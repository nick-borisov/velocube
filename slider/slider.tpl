{get_slides var=slide}






{if $smarty.session.admin == 'admin'}{if $slide}	

<div class="main-slider-wrapper">
	<div class="main-slider">
		<ul class="slider">
		{foreach $slide as $s}
				{if $s->image}
					<li>{if $s->url}<a href="{$s->url}">{/if}
					<img src="{$s->image}" alt="" {if $s->description}title="#slide_{$s->id}"{/if} />
					{if $s->url}</a>{/if}</li>
				{/if}
		{/foreach}
		</ul>
	</div>
	<i class="icon control-prev"></i>
	<i class="icon control-next"></i>
</div>
{/if}
{/if}

{literal}
<script type="text/javascript">
$(document).ready(function(){
	var slider = $('.slider').bxSlider({
		mode: 'horizontal',
		auto: true,
		autoHover: true,
		pause: 5000,
		pager: false,
		controls: false
	});
	$('.control-prev').click(function(){
		slider.goToPrevSlide();
		return false;
	});
	$('.control-next').click(function(){
		slider.goToNextSlide();
		return false;
	});
	var slider_cat = $('.catalog').bxSlider({
		mode: 'horizontal',
		slideWidth: 240,
		slideMargin: 0,
		maxSlides: 4,
		moveSlides: 1,
		infiniteLoop: false,
		pager: false,
		controls: false
	});
	$('.rec_prev').click(function(){
		slider_cat.goToPrevSlide();
		return false;
	});
	$('.rec_next').click(function(){
		slider_cat.goToNextSlide();
		$(window).trigger("scroll");
		return false;
	});
});	
</script>
{/literal}