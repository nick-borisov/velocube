;<? exit(); ?>

license = ace8gghahh lfkplqprmj rqwsxuryx2 9488bbbf9b cjgjhldlif ppnrqtrmnm vsuay46ca3 9bf7cge9 ijijmgiknk qmrtquvwpp w84dadb848 gjajihemgd mkfkjiqlru qvrqp9s1v4 bf8gcbaabh blagemkjhq lroorqnmmn r4q7y8wa1c 8b8dbjblgh fijmhoekot innwoqt6p9 w6xd179d9a ae7gdhfgep lmjtfngupn p8s5s9p6x4 4c2bbbdjal 8finhhilhi jokshspvra sbvd

[database]

;Сервер базы данных
db_server = "localhost"

;Пользователь базы данных
db_user = "root"

;Пароль к базе
db_password = ""

;Имя базы
db_name = "velocube"

;Префикс для таблиц
db_prefix = s_;

;Кодировка базы данных
db_charset = UTF8;

;Режим SQL
db_sql_mode =;

[php]
error_reporting = E_ALL;
php_charset = UTF8;
php_locale_collate = ru_RU;
php_locale_ctype = ru_RU;
php_locale_monetary = ru_RU;
php_locale_numeric = ru_RU;
php_locale_time = ru_RU;

logfile = admin/log/log.txt;

[smarty]

smarty_compile_check = true;
smarty_caching = false;
smarty_cache_lifetime = 30;
smarty_debugging = false;
smarty_html_minify = true;
 
[images]
;Использовать imagemagick для обработки изображений (вместо gd)
use_imagick = false

;Директория оригиналов изображений
original_images_dir = files/originals/;

;Директория миниатюр
resized_images_dir = files/products/;

;Изображения категорий
categories_images_dir = files/categories/;

;Ресайзы категорий
categories_resized_dir = files/categories_resized/;

;Изображения брендов
brands_images_dir = files/brands/;

;Ресайзы брендов
brands_resized_dir = files/brands_resized/;

;Файл изображения с водяным знаком
watermark_file = simpla/files/watermark/watermark.png;

[files]

;Директория хранения цифровых товаров
downloads_dir = files/downloads/;
;Изображения слайдов
slides_images_dir = files/slides/;