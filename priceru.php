<?php
//ini_set('display_errors',1);
require_once('api/Simpla.php');
$simpla = new Simpla();

header("Content-type: text/xml; charset=UTF-8");
print (pack('CCC', 0xef, 0xbb, 0xbf));
// Заголовок
print
"<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE yml_catalog SYSTEM 'shops.dtd'>
<priceru_feed date='".date('Y-m-d H:i')."'>
<shop>
<name>".$simpla->settings->site_name."</name>
<company>".$simpla->settings->company_name."</company>
<url>".$simpla->config->root_url."</url>
<platform>SimplaCMS</platform>
<version>".$simpla->config->version."</version>
";

// Валюты
$currencies = $simpla->money->get_currencies(array('enabled'=>1));
$main_currency = reset($currencies);
print "<currencies>
";
foreach($currencies as $c)
if($c->enabled)
print "
    <currency id='".$c->code."' rate='".$c->rate_to/$c->rate_from*$main_currency->rate_from/$main_currency->rate_to."'/>
";
print "</currencies>
";

// Категории
print "<categories>";
$simpla->db->query("SELECT c.id, c.name, c.parent_id, c.market_category
                    FROM s_categories c
                        LEFT JOIN s_products_categories pc ON c.id = pc.category_id
                        LEFT JOIN s_products p ON pc.product_id = p.id
                    WHERE p.priceru = 1 GROUP BY c.id ORDER BY c.id");
$categories = $simpla->db->results();
foreach($categories as $c){
print "
    <category id='$c->id'";
    if($c->parent_id>0)
    	print " parentId='$c->parent_id'";
    print ">".htmlspecialchars($c->name)."</category>
";
    }
    print "</categories>
";
   /* print "<delivery-options>
            <option cost='".$simpla->settings->yandex_delivery_cost."' days='2' order-before='18'/>
        </delivery-options>
        ";*/
$currency_code = reset($currencies)->code;

$check_instock = ($simpla->settings->yandex_vnalichii)?"AND IF(p.sklad_id < 1, (v.stock > 0 OR v.stock is NULL), '1') AND p.sklad_id IS NOT NULL":"AND (v.stock > 0 OR v.stock is NULL) AND p.sklad_id = 0";
print "<offers>
";
//$categories = $simpla->categories->get_categories();
foreach($categories as $c){
    $simpla->db->query("SET SQL_BIG_SELECTS=1");
    // Товары
    $query = $simpla->db->placehold("SELECT v.price, NULLIF(v.compare_price, 0) as compare_price, v.id as variant_id, p.name as product_name, v.name as variant_name, v.position as variant_position, i.filename as vimage, p.id as product_id, p.url, p.annotation, pc.category_id, v.stock, v.sku, b.name as brand_name, p.type_prefix, p.sklad_id, p.sales_notes
    					FROM __variants v LEFT JOIN __products p ON v.product_id=p.id
    					LEFT JOIN __brands b ON p.brand_id=b.id
                        LEFT JOIN __images i ON v.image_id=i.id
    					LEFT JOIN __products_categories pc ON p.id = pc.product_id AND pc.position=(SELECT MIN(position) FROM __products_categories WHERE product_id=p.id LIMIT 1) AND pc.category_id=?	
    					WHERE p.visible $check_instock AND pc.category_id=? AND p.priceru=1 AND v.price>0 AND v.price IS NOT NULL AND p.brand_id > 0 AND p.brand_id IS NOT NULL GROUP BY v.id ORDER BY p.id, v.position ", $c->id, $c->id);
    
    //print_r($query);
    $simpla->db->query($query);
    $products = $simpla->db->results();
    
    // Товары
    foreach($products as $p){
        $product_category = reset($simpla->categories->get_product_categories($p->product_id));
        if($product_category->category_id != $c->id)
            continue;
        $variant_url = '';
        $variant_url = '?variant='.$p->variant_id;
        if($simpla->settings->yandex_vnalichii){
            if(($p->stock || $p->stock==null) && $p->sklad_id<1){
                $available = "available='true'";
            }else{
                $available = "available='false'";
            }
        }else{
            $available = "available='true'";
        }
        
        $prod = new StdClass();
        $prod->product_id = $p->product_id;
        $prod->variant_id = $p->variant_id;
        $prod->price = $p->price;
        
        $ready_price = $simpla->orders->calc_auto_discount_priceru($prod);
        $price = $ready_price->price;//round($simpla->money->convert($p->price, $main_currency->id, false),2);
        if(!$p->compare_price && $ready_price->dirty_price){
            $p->compare_price = $ready_price->dirty_price;
        }
        
    print
    "
    <offer id='$p->variant_id' type='vendor.model' $available>
        <url>".$simpla->config->root_url.'/products/'.$p->url.$variant_url."</url>";
        print "
        <price>$price</price>";
        if($p->compare_price && $simpla->settings->yandex_oldprice){
        print "
        <oldprice>$p->compare_price</oldprice>
        ";    
        }
        print "
        <currencyId>".$currency_code."</currencyId>
        <categoryId>".$p->category_id."</categoryId>
        ";
        
        if($c->market_category)
            print "<market_category>".htmlspecialchars($c->market_category)."</market_category>
        ";
        if($price > $simpla->settings->interval_tom_price || $c->delivery_price){
            
            if($price > $simpla->settings->interval_tom_price){
                $delivery_price = 0; 
                $delivery_days = 1;
            }elseif($price > $simpla->settings->interval_price){
                $delivery_price = intval($c->delivery_price); 
                $delivery_days = 2;
            }else{
                $delivery_price = intval($c->delivery_price) * intval($simpla->settings->interval_delivery_mult); 
                $delivery_days = 2;
            }
        print "
        <local_delivery_cost>".$delivery_price."</local_delivery_cost>
        ";
        }
        
        if($simpla->settings->yandex_store && $available == "available='true'"){
            print "<store>true</store>
        ";
        }else{
            print "<store>false</store>
        ";
        }
        
        if($simpla->settings->yandex_pickup && $available == "available='true'"){
            print "<pickup>true</pickup>
        ";
        }else{
            print "<pickup>false</pickup>
        ";
        }
        
        if(count($simpla->delivery->get_deliveries(array('enabled'=>1)))){
            print "<delivery>true</delivery>
        ";
        }else{
            print "<delivery>false</delivery>
        ";
        }
        
        if($p->sku)
            print "<vendorCode>".htmlspecialchars($p->sku)."</vendorCode>
        ";
        
        if($p->sales_notes)
            print "<sales_notes>".htmlspecialchars($p->sales_notes)."</sales_notes>
        ";
        
        print "<vendor>".htmlspecialchars($p->brand_name)."</vendor>
        ";
        
        print "<model>".htmlspecialchars($p->product_name).($p->variant_name?' '.htmlspecialchars($p->variant_name):'')."</model>
        ";
        
        
        if($p->vimage){
            print "<picture>".$simpla->design->resize_modifier($p->vimage, 200, 200)."</picture>
        ";
        }else{
            $query1 = $simpla->db->query("SELECT filename FROM __images WHERE product_id=? ORDER BY position LIMIT 10", $p->product_id);
            foreach($simpla->db->results('filename') as $i) {
            print "<picture>".$simpla->design->resize_modifier($i, 200, 200)."</picture>
        ";
            }
        
        }
        
        if($p->type_prefix){
            print "<typePrefix>".htmlspecialchars($p->type_prefix)."</typePrefix>
        ";
        }
        
        if($simpla->settings->yandex_full_description){
            print "<description>".htmlspecialchars(strip_tags($p->body))."</description>
        ";
        }else{
            print "<description>".htmlspecialchars(strip_tags($p->annotation))."</description>
        ";
        }

        foreach($simpla->features->get_options(array('product_id'=>$p->product_id, 'yandex'=>1)) as $o) {
            $f = $simpla->features->get_features(array('id'=>$o->feature_id));
            print "<param name='".htmlspecialchars($f[0]->name)."'>".htmlspecialchars($o->value)."</param>
        ";
           
        }
        
print "
    </offer>
";
    }
}

print "</offers>
";
print "</shop>
</priceru_feed>
";