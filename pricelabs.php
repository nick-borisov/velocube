<?php
$fp = fopen('ipaccess.txt', 'a+');
fwrite($fp, date('d.m.Y H:i:s')." {$_SERVER['HTTP_X_REAL_IP']} {$_SERVER['HTTP_USER_AGENT']} {$_SERVER['REQUEST_URI']}\n");

if ($_SERVER['HTTP_USER_AGENT']=='YandexMarket/1.9-2 (compatible; http://market.yandex.ru)') require('set_prices2.php');

require_once('api/Simpla.php');
$simpla = new Simpla();

header("Content-type: text/xml; charset=UTF-8");

// Заголовок
print
"<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE yml_catalog SYSTEM 'shops.dtd'>
<yml_catalog date='".date('Y-m-d H:m')."'>
<shop>
<name>".$simpla->settings->site_name."</name>
<company>".$simpla->settings->company_name."</company>
<url>".$simpla->config->root_url."</url>
";

// Валюты
$currencies = $simpla->money->get_currencies(array('enabled'=>1));
$main_currency = reset($currencies);
print "<currencies>
";
foreach($currencies as $c)
if($c->enabled)
print "<currency id='".$c->code."' rate='".$c->rate_to/$c->rate_from*$main_currency->rate_from/$main_currency->rate_to."'/>
";
print "</currencies>
";


// Категории
$categories = $simpla->categories->get_categories();
print "<categories>
";
foreach($categories as $c)
{
print "<category id='$c->id'";
if($c->parent_id>0)
	print " parentId='$c->parent_id'";
print ">".htmlspecialchars($c->name)."</category>
";
}
print "</categories>
";

// Товары
$simpla->db->query("SET SQL_BIG_SELECTS=1");

    $images1 = $simpla->products->get_images();
	if(is_array($images1))foreach ($images1 as $image) {
		$images[$image->product_id][]=$image->filename;
	}

	$features=array();
	$temps = $simpla->features->get_features();
	if(is_array($temps))foreach ($temps as $temp) {
		$features[$temp->id]=$temp->name;
	}

	$options=array();
	$temps = $simpla->features->get_options_all();
	if(is_array($temps))foreach ($temps as $temp) {
		$options[$temp->product_id][$temp->feature_id]=$temp->value;
	}




// Товары
$simpla->db->query("SELECT p.warranty as warranty, p.sklad_id, v.stock, v.price, v.stockprice, v.mprice, v.compare_price, v.id as variant_id, p.name as product_name, v.name as variant_name, v.position as variant_position, p.id as product_id, p.url, p.annotation, pc.category_id, i.filename as image
					FROM __variants v LEFT JOIN __products p ON v.product_id=p.id
					
					LEFT JOIN __products_categories pc ON p.id = pc.product_id AND pc.position=(SELECT MIN(position) FROM __products_categories WHERE product_id=p.id LIMIT 1)	
					LEFT JOIN __images i ON p.id = i.product_id AND i.position=(SELECT MIN(position) FROM __images WHERE product_id=p.id LIMIT 1)	
					WHERE p.yandex=1 AND p.visible AND (v.stock >0 OR v.stock is NULL or p.sklad_id > 0) GROUP BY v.id ORDER BY p.id, v.position ");
print "<offers>
";
 

$currency_code = reset($currencies)->code;

// В цикле мы используем не results(), a result(), то есть выбираем из базы товары по одному,
// так они нам одновременно не нужны - мы всё равно сразу же отправляем товар на вывод.
// Таким образом используется памяти только под один товар
$prev_product_id = null;
while($p = $simpla->db->result())
{
$variant_url = '';
if ($prev_product_id === $p->product_id)
	$variant_url = '?variant='.$p->variant_id;
$prev_product_id = $p->product_id;
$stockprice = round($simpla->money->convert($p->stockprice, $main_currency->id, false),2);
$price = round($simpla->money->convert($p->price, $main_currency->id, false),2);
$oldprice = round($simpla->money->convert($p->compare_price, $main_currency->id, false),2);

if ($oldprice == $price)
	unset($oldprice);
 
$aval = "true";
if ($p->sklad_id > 1)
$aval = "false";
print
"
<offer id='$p->variant_id' available='$aval'>
<url>".$simpla->config->root_url.'/products/'.$p->url.$variant_url."</url>";
if ($oldprice > 0)
	print "<oldprice>$oldprice</oldprice>";
print "<price>$price</price>";
if ($stockprice > 0)
print "<stockprice>$stockprice</stockprice>";

if ($p->warranty)
	{
		print "<manufacturer_warranty>true</manufacturer_warranty>";
	}
print "
<currencyId>".$currency_code."</currencyId>
<categoryId>".$p->category_id."</categoryId>
";

/*
if($p->image)
print "<picture>".$simpla->design->resize_modifier($p->image, 200, 200)."</picture>
";
*/
$it = 0;
	if(is_array($images[$p->product_id]))foreach ($images[$p->product_id] as $image) {
print "<picture>".$simpla->design->resize_modifier($image, 800, 600)."</picture>
";
$it = $it + 1;
if ($it == 10) break;
	}




print "<name>".htmlspecialchars($p->product_name).($p->variant_name?' '.htmlspecialchars($p->variant_name):'')."</name>
<description>".htmlspecialchars(strip_tags($p->annotation))."</description>
";

	if(is_array($options[$p->product_id]))foreach ($options[$p->product_id] as  $id => $value) {
print '<param name="'.$features[$id].'">'.htmlspecialchars($value).'</param>
';
	}




print "
</offer>
";



}

print "</offers>
";
print "</shop>
</yml_catalog>
";