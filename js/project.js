// Звёздный рейтинг товаров
$.fn.rater = function (options) {
    var opts = $.extend({}, $.fn.rater.defaults, options);
    return this.each(function () {
        var $this = $(this);
        var $on = $this.find('.rater-starsOn');
        var $off = $this.find('.rater-starsOff');
		
        opts.size = $on.height();
        if (opts.rating == undefined) opts.rating = $on.width() / opts.size;
        if (opts.id == undefined) opts.id = $this.attr('id');

        $off.mousemove(function (e) {
            var left = e.clientX - $off.offset().left;
            var width = $off.width() - ($off.width() - left);
            width = Math.ceil(width / (opts.size / opts.step)) * opts.size / opts.step;
            $on.width(width);
        }).hover(function (e) { $on.removeClass('rater-starsHover'); }, function (e) {
            $on.removeClass('rater-starsHover'); $on.width(opts.rating * opts.size);
        }).click(function (e) {
		    $on.addClass('rater-starsHover');
		    var $rt = document.getElementById('rt');
            var r = Math.round($on.width() / $off.width() * (opts.units * opts.step)) / opts.step;
           
            $off.css('cursor', 'default'); $on.css('cursor', 'default');
            $rt.value = r;
        }).mouseleave(function (e) {  

        var $rt = document.getElementById('rt');
        $on.addClass('rater-starsHover');
		$on.width($rt.value*32);            }).css('cursor', 'pointer'); $on.css('cursor', 'pointer');
    });
};

$.fn.rater.defaults = {
    postHref: location.href,
    units: 5,
    step: 1
};


// end