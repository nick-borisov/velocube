﻿$(document).ready(function(){	
    //  Автозаполнитель поиска
    $(".input_search").autocomplete({
        serviceUrl:'/ajax/search_products.php',
        minChars:1,
        noCache: false, 
		width:450,
		maxHeight:375,
        onSelect:
            function(value, data){
                 //$(".input_search").closest('form').submit();
                 location.href=data.url;
            },
        fnFormatResult:
            function(value, data, currentValue){
                var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
                var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
                  return '<div class="suggest-box">'+(data.image?"<div class='suggest-image'><img src='"+data.image+"' alt=''></div>":'') +'<a href="'+data.url+'" title="">'+ value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')+'</a>'+(data.price?"<div class='suggest-price'>"+data.price+" грн.</div>":'')+(data.thiss?"<div class='suggest-category'>Перейти в категорию</div>":'')+'</div>';
            }    
    });
    
    //Подстановка в поиск ключевых слов
    $(".search p span").click(function(){
    	$(".input_search").val($(this).html()).focus().setCursorPosition($(this).html().length);
    	
    });
});

//Функция подстановки курсора в конец текста
new function($) {
	$.fn.setCursorPosition = function(pos) {
	    if ($(this).get(0).setSelectionRange) {
	      $(this).get(0).setSelectionRange(pos, pos);
	    } else if ($(this).get(0).createTextRange) {
	      var range = $(this).get(0).createTextRange();
	      range.collapse(true);
	      range.moveEnd('character', pos);
	      range.moveStart('character', pos);
	      range.select();
	    }
  }
}(jQuery);