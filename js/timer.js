$(document).ready(function(){

	
	var nowTime = new Date();
	weekcounter = typeof(weekcounter) == "undefined" ? 0 : weekcounter;
	var targetTime = weekcounter || new Date('31 August 2020 00:00');
	var diffSecs = Math.floor((targetTime.valueOf() - nowTime.valueOf()));
	dif(Math.floor(diffSecs / 1000));

	
});

function dif(diffSecs) {
	sec = Math.floor(diffSecs % 60);
	mins = Math.floor(diffSecs / 60) % 60;
	hours = Math.floor(diffSecs / 60 / 60);
	if (weekcounter == 0){
		hours %= 24;
	}
	//days = Math.floor(diffSecs / 60 / 60 / 24);

	var temp = convert(sec, ['', '', '']);
	$('.timer .minutes').html(temp[0]);
	$('.timer .measure-minutes').html(temp[1]);
	var temp = convert(mins, [':', ':', ':']);
	$('.timer .hours').html(temp[0]);
	$('.timer .measure-hours').html(temp[1]);
	var temp = convert(hours, [':', ':', ':']);
	$('.timer .days').html(temp[0]);
	$('.timer .measure-days').html(temp[1]);
	//var temp = convert(days, ['день', 'дня', 'дней']);
	

	if (diffSecs > 0)
	{
		t = setTimeout(function() {
			dif(diffSecs - 1)
		}, 1000);
	}
}

function convert(n, ar) {
	var o = n % 10;
	var l, g;
	switch (o) {
		case 1:
			l = 0;
			break;
		case 2:
		case 3:
		case 4:
			l = 1;
			break;
		default:
			l = 2;
			break;
	}

	var g = n % 100;
	if (g == 10 || g == 11 || g == 12 || g == 13 || g == 14) {
		l = 2;
	}
	return [n, ar[l]];
}
