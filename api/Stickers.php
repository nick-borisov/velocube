<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Stickers extends Simpla {

	public function get_stickers($filter = array()) {
		$visible_filter = "";
		$brand_filter = "";

		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold("AND visible = ?", $filter['visible']);

		if(isset($filter['type']))
			$type_filter = $this->db->placehold("AND (type = ? OR type = '*')", $filter['type']);

		if(isset($filter['brand_id']))
			$brand_filter = $this->db->placehold("AND (brand_id = ? OR brand_id = '*')", $filter['brand_id']);

		$query = $this->db->placehold("SELECT id, name, text, visible, type FROM __stickers WHERE 1 $visible_filter $type_filter $brand_filter");

		$this->db->query($query);
		return $this->db->results();
	}

	public function get_sticker($id) {
		$this->db->query("SELECT * FROM __stickers WHERE id = ?", $id);
		return $this->db->result();
	}

	public function add_sticker($sticker) {
		$this->db->query("INSERT INTO __stickers SET ?%", $sticker);
		return $this->db->insert_id();
	}

	public function update_sticker($id, $sticker) {
		$this->db->query("UPDATE __stickers SET ?% WHERE id = ?", $sticker, $id);
	}

	public function delete_sticker($id) {
		$this->db->query("DELETE FROM __stickers WHERE id = ?", $id);
	}

}