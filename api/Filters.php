<?php

/**
 * Simpla CMS
 *
 * @copyright	2016 Slava Polyakov
 * @link		http://simplacms.ru
 * @author		Slava Polyakov
 *
 */
 
require_once('Simpla.php');

class Filters extends Simpla
{	
	
	function get_filters($filter = array())
	{
		$category_id_filter = '';	
		if(isset($filter['category_id']))
			$category_id_filter = $this->db->placehold('AND id in(SELECT filter_id FROM __categories AS c WHERE c.category_id in(?@))', (array)$filter['category_id']);
		
		$visible_filter = '';	
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND f.visible=?', intval($filter['in_filter']));
		
		$id_filter = '';	
		if(!empty($filter['id']))
			$id_filter = $this->db->placehold('AND f.id in(?@)', (array)$filter['id']);
		
		// Выбираем свойства
		$query = $this->db->placehold("SELECT id, name, position, visible FROM __filters AS f
									WHERE 1
									$category_id_filter $visible_filter $id_filter ORDER BY f.position");
		$this->db->query($query);
		return $this->db->results();
	}
		
	function get_filter($id)
	{
		// Выбираем свойство
		$query = $this->db->placehold("SELECT id, name, position, visible FROM __filters WHERE id=? LIMIT 1", $id);
		$this->db->query($query);
		$feature = $this->db->result();

		return $feature;
	}
	
	public function add_filter($object)
	{
		$query = $this->db->placehold("INSERT INTO __filters SET ?%", $object);
		$this->db->query($query);
		$id = $this->db->insert_id();
		$query = $this->db->placehold("UPDATE __filters SET position=id WHERE id=? LIMIT 1", $id);
		$this->db->query($query);
		return $id;
	}
		
	public function update_filter($id, $filter)
	{
		$query = $this->db->placehold("UPDATE __filters SET ?% WHERE id in(?@) LIMIT ?", (array)$filter, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}
	
	public function delete_filter($id = array())
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __filters WHERE id=? LIMIT 1", intval($id));
			$this->db->query($query);
			//$query = $this->db->placehold("DELETE FROM __options WHERE filter_id=?", intval($id));
			//$this->db->query($query);	
			$query = $this->db->placehold("DELETE FROM __filter_features WHERE filter_id=?", intval($id));
			$this->db->query($query);	
		}
	}
    
   	function get_filter_features($id)
	{
		$query = $this->db->placehold("SELECT ff.feature_id as feature_id, expand FROM __filter_features ff
										WHERE ff.filter_id = ? ORDER BY position", $id);
		$this->db->query($query);
		return $this->db->results();	
	}
	
	public function add_filter_features($id, $feature_id)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __filter_features SET filter_id=?, feature_id=?", $id, $feature_id);
		$this->db->query($query);
	}
			
	public function update_filter_features($id, $features)
	{
		$id = intval($id);
		$query = $this->db->placehold("DELETE FROM __filter_features WHERE filter_id=?", $id);
		$this->db->query($query);
		
		
		if(is_array($features))
		{
            foreach($features as $feature){
                $query = $this->db->placehold("INSERT INTO __filter_features SET ?%", $feature);
                $this->db->query($query);
            }
		}
		else
		{
			// Удалим значения из options 
			$query = $this->db->placehold("DELETE o FROM __options o WHERE o.feature_id=?", $id);
			$this->db->query($query);
		}
	}
}
