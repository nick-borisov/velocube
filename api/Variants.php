<?php
//ini_set('display_errors',1);
/**
 * Работа с вариантами товаров
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simplacms.ru
 * @author 		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Variants extends Simpla
{
	/**
	* Функция возвращает варианты товара
	* @param	$filter
	* @retval	array
	*/
	public function get_variants($filter = array())
	{		
		$product_id_filter = '';
		$variant_id_filter = '';
		$instock_filter = '';
        $sku_filter = '';
        $order = 'v.position';
		
		if(!empty($filter['product_id']))
			$product_id_filter = $this->db->placehold('AND v.product_id in(?@)', (array)$filter['product_id']);
		
		if(!empty($filter['id']))
			$variant_id_filter = $this->db->placehold('AND v.id in(?@)', (array)$filter['id']);
        
        if(!empty($filter['sku']))
			$sku_filter = $this->db->placehold('AND v.sku in(?@)', (array)$filter['sku']);    

		if(!empty($filter['in_stock']) && $filter['in_stock'])
			$variant_id_filter = $this->db->placehold('AND (v.stock>=0 OR v.stock IS NULL)');

		if(!$product_id_filter && !$variant_id_filter && !$sku_filter)
			return array();

        if(!empty($filter['order']))
            switch($filter['order']){
                case 'stock':
                $order = $this->db->placehold(" (v.stock > 0 OR v.stock IS NULL) DESC ");
                break;
            }
		
		$query = $this->db->placehold("SELECT p.brand_id, p.vendor, v.mprice, v.stockprice, v.murl, v.id, v.product_id , v.price, NULLIF(v.compare_price, 0) as compare_price, v.sku, IFNULL(v.stock, ?) as stock, (v.stock IS NULL) as infinity, v.name, v.color_code, v.image_id, v.attachment, v.position, v.viewed, v.store
					FROM __variants AS v
					LEFT JOIN __products p ON p.id = v.product_id 
					WHERE 
					1
					$product_id_filter          
					$variant_id_filter
                    $sku_filter   
					ORDER BY $order       
					", $this->settings->max_order_amount);
//if($_SESSION['admin']) print($query);		
		$this->db->query($query);	
		$res = $this->db->results();
		
		if (is_array($res) and 1 == 1)
		foreach ($res as $r)
		{
				 
				$stop = 0;
				$delta = '';
				$delta = $this->parsers->pro_strat($r);		

				if ( isset($delta["minprice"]) and (int)$delta["minprice"] > $r->price)
					$stop = 1;
				if ( isset($delta["maxprice"]) and(int)$delta["maxprice"] < $r->price)
					$stop = 1;
				
				$r->admprice = $r->price;
				if (empty($r->compare_price) and (is_array($delta)) and ($stop != 1))			
					$r->compare_price = $r->price;
				

				
				if ($stop == 0) 
					{
						if(is_int($delta["a"]) AND $delta["a"] <> 0)
						{
							$r->price = $r->price + (int)$delta["a"];					 
							$r->strat = 1;
						}
						if(is_int($delta["p"]) AND $delta["p"] <> 0)
						{
							$r->price = $r->price / 100 * (100+(int)$delta["p"]);							
							$r->strat = 1;
						}
						
					}
				//if ($r->compare_price <= $r->price)
					//unset($r->compare_price);
		}
		return $res;
	}
	
	
	public function get_variant($id)
	{	
		if(empty($id))
			return false;
			
		$query = $this->db->placehold("SELECT v.id, v.product_id , v.price, v.stockprice, v.mprice, v.murl, NULLIF(v.compare_price, 0) as compare_price, v.sku, IFNULL(v.stock, ?) as stock, (v.stock IS NULL) as infinity, v.name, v.color_code, v.image_id, v.attachment, v.store FROM __variants AS v 
					 
					WHERE id=?
					LIMIT 1", $this->settings->max_order_amount, $id);
		 
		$this->db->query($query);	
		$variant = $this->db->result();
 
 		$this->db->query("SELECT vendor, brand_id FROM __products WHERE id = ?", $variant->product_id);	
		$temprod = $this->db->result();
		$variant->brand_id = $temprod->brand_id;
		$variant->vendor = $temprod->vendor;
 
 
		$delta = '';
		$delta = $this->parsers->pro_strat($variant);		
		if (empty($variant->compare_price) and (is_array($delta)))			
			$variant->compare_price = $variant->price;
		if(is_int($delta["a"]) AND $delta["a"] <> 0)
			$variant->price = $variant->price + (int)$delta["a"];					 
		if(is_int($delta["p"]) AND $delta["p"] <> 0)
			$variant->price = $variant->price / 100 * (100+(int)$delta["p"]);	
 
 				if ($variant->compare_price <= $variant->price)
					unset($variant->compare_price);
 
		return $variant;
	}
	
	public function update_variant($id, $variant)
	{
		$query = $this->db->placehold("UPDATE __variants SET ?% WHERE id=? LIMIT 1", $variant, intval($id));
		$this->db->query($query);

		//Добавляем/удаляем категории связанные с ценой
		$this->check_highlowcost_category($id);
		return $id;
	}
	
	public function add_variant($variant)
	{
		$query = $this->db->placehold("INSERT INTO __variants SET ?%", $variant);
		$this->db->query($query);
		return $this->db->insert_id();
	}

	public function delete_variant($id)
	{
		if(!empty($id))
		{
			$this->delete_attachment($id);
			$query = $this->db->placehold("DELETE FROM __variants WHERE id = ? LIMIT 1", intval($id));
			$this->db->query($query);
			$this->db->query('UPDATE __purchases SET variant_id=NULL WHERE variant_id=?', intval($id));
		}
	}
	
	public function delete_attachment($id)
	{
		$query = $this->db->placehold("SELECT attachment FROM __variants WHERE id=?", $id);
		$this->db->query($query);
		$filename = $this->db->result('attachment');
		$query = $this->db->placehold("SELECT 1 FROM __variants WHERE attachment=? AND id!=?", $filename, $id);
		$this->db->query($query);
		$exists = $this->db->num_rows();
		if(!empty($filename) && $exists == 0)
			@unlink($this->config->root_dir.'/'.$this->config->downloads_dir.$filename);
		$this->update_variant($id, array('attachment'=>null));
	}
    
    private function check_highlowcost_category($vid){
        $variant = $this->variants->get_variant($vid);
        $price = (float)$variant->price;
        $product_id = $variant->product_id;

		//Добавляем/удаляем категорию Распродажа
		$this->check_sale_category($product_id);

        if($product_id){
            foreach($this->categories->get_categories(array('product_id'=>$product_id)) as $category){
                if($category->lowcost_limit && $category->lowcost_category){
                    if(floatval($variant->price) && $price <= (float)$category->lowcost_limit){
                        $this->categories->add_product_category($product_id, $category->lowcost_category, 100);
                    }else{
						$this->categories->delete_product_category($product_id, $category->lowcost_category);
					}
                }
                if($category->highcost_limit && $category->highcost_category){
                    if(floatval($variant->price) && $price >= (float)$category->highcost_limit){
                        $this->categories->add_product_category($product_id, $category->highcost_category, 100);
                    }else{
						$this->categories->delete_product_category($product_id, $category->highcost_category);
					}
                }
            }
        }
		return true;
    }

	public function check_sale_category($product_id){
		$add = false;
		foreach($this->get_variants(array('product_id'=>$product_id)) as $variant) {
			if ($variant->compare_price && $variant->compare_price > $variant->price) {
				$add = true;
			}
		}
		$product_categories = $this->categories->get_product_categories($product_id);
		if (is_array($product_categories)) {
			$first_product_category = $this->categories->get_category(intval(reset($product_categories)->category_id));
			if($add && $first_product_category->sale_category){
				$this->categories->add_product_category($product_id, $first_product_category->sale_category, "110");
			}elseif($first_product_category->sale_category){
				$this->categories->delete_product_category($product_id, $first_product_category->sale_category);
			}
		}
		return true;
	}
    
}