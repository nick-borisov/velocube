<?php
//ini_set('display_errors',1);
/**
 * Р Р°Р±РѕС‚Р° СЃ С‚РѕРІР°СЂР°РјРё
 * 
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simplacms.ru
 * @author 		Denis Pikusov
 *
 */
      
require_once('Simpla.php');

class Products extends Simpla
{
	/**
	* Р¤СѓРЅРєС†РёСЏ РІРѕР·РІСЂР°С‰Р°РµС‚ С‚РѕРІР°СЂС‹
	* Р’РѕР·РјРѕР¶РЅС‹Рµ Р·РЅР°С‡РµРЅРёСЏ С„РёР»СЊС‚СЂР°:
	* id - id С‚РѕРІР°СЂР° РёР»Рё РёС… РјР°СЃСЃРёРІ
	* category_id - id РєР°С‚РµРіРѕСЂРёРё РёР»Рё РёС… РјР°СЃСЃРёРІ
	* brand_id - id Р±СЂРµРЅРґР° РёР»Рё РёС… РјР°СЃСЃРёРІ
	* page - С‚РµРєСѓС‰Р°СЏ СЃС‚СЂР°РЅРёС†Р°, integer
	* limit - РєРѕР»РёС‡РµСЃС‚РІРѕ С‚РѕРІР°СЂРѕРІ РЅР° СЃС‚СЂР°РЅРёС†Рµ, integer
	* sort - РїРѕСЂСЏРґРѕРє С‚РѕРІР°СЂРѕРІ, РІРѕР·РјРѕР¶РЅС‹Рµ Р·РЅР°С‡РµРЅРёСЏ: position(РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ), name, price
	* keyword - РєР»СЋС‡РµРІРѕРµ СЃР»РѕРІРѕ РґР»СЏ РїРѕРёСЃРєР°
	* features - С„РёР»СЊС‚СЂ РїРѕ СЃРІРѕР№СЃС‚РІР°Рј С‚РѕРІР°СЂР°, РјР°СЃСЃРёРІ (id СЃРІРѕР№СЃС‚РІР° => Р·РЅР°С‡РµРЅРёРµ СЃРІРѕР№СЃС‚РІР°)
	*/
	public function get_products($filter = array())
	{		
		// РџРѕ СѓРјРѕР»С‡Р°РЅРёСЋ
		$limit = 100;
		$page = 1;
		$category_id_filter = '';
		$brand_id_filter = '';
		$vendor_id_filter = '';
		$mark_id_filter = '';
		$product_id_filter = '';
		$features_filter = '';
		$keyword_filter = '';
		$visible_filter = '';
		//$min_price = '';
		//$max_price = '';
		$is_featured_filter = '';
		$discounted_filter = '';
        $discounted_rev_filter = '';
		$in_stock_filter = '';
		$date_filter = '';
		$manager_filter = '';
        $pricenotnull_filter = '';
        $sklad_filter = '';
        $variant_join = '';
        $smartbikes_filter = '';
        $sales_notes_filter  = '';
		$group_by = '';
		$order = 'p.position DESC';
        $is_featured_filter_d = '';
        
        $fields_for_front = 'p.id,
        					p.date,
        					p.url,
        					p.brand_id,
        					p.name,
                            p.fullname,
        					p.annotation,
                            p.sklad, 
                            p.sklad_id,                                         
        					p.meta_title, 
        					p.meta_keywords, 
        					p.meta_description,
                            p.ttoday,
        					p.own_text,
        					p.rating,
        					p.votes,
        					p.vendor,
        					b.name as brand,
        					b.url as brand_url,
                            p.hits,
                            p.delivery_description,
                            b.name as brand,
        					b.url as brand_url';
        
        $default_fields = 'p.id,
        					p.date,
        					p.url,
        					p.brand_id,
        					p.name,
                            p.fullname,
        					p.annotation,
        					p.body,
        					p.position,
        					p.created,
        					p.manager,
        					p.visible, 
        					p.featured,
        					p.new,
        					p.yandex,
        					p.warranty,
        					p.byrequest,	
        					p.atribut,
        					p.video,
        					p.sklad, p.sklad_id,                                         
        					p.meta_title, 
        					p.meta_keywords, 
        					p.meta_description,
        					p.ttoday,
                            p.own_text,
        					p.rating,
        					p.votes,
        					p.vendor,
        					b.name as brand,
        					b.url as brand_url,
                            p.yandex,
                            p.type_prefix, p.seller_warranty, p.sales_notes,
                            p.free_delivery,
                            p.hits,
                            p.smartbikes,
                            p.last_parse,
                            p.generate_meta,
                            p.k50,
                            p.priceru,
                            p.generate_description,
                            p.delivery_description';
        
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		if(!empty($filter['id']))
			$product_id_filter = $this->db->placehold('AND p.id in(?@)', (array)$filter['id']);

		if(!empty($filter['category_id']))
		{
			$category_id_filter = $this->db->placehold('INNER JOIN __products_categories pc ON pc.product_id = p.id AND pc.category_id in(?@)', (array)$filter['category_id']);
			$group_by = "GROUP BY p.id";
		}elseif(empty($filter['keyword'])){
		    //Для скрытия импортируемых их XML товаров при выводе в админке по умолчанию 
            $category_id_filter = $this->db->placehold('INNER JOIN __products_categories pc ON pc.product_id = p.id AND pc.category_id NOT in(493)');
		}

		if(!empty($filter['vendor_id']))
			$vendor_id_filter = $this->db->placehold('AND p.vendor in(?@)', (array)$filter['vendor_id']);

		if(!empty($filter['brand_id']))
		$brand_id_filter = $this->db->placehold('AND p.brand_id in(?@)', (array)$filter['brand_id']);

		
		if(!empty($filter['mark_id']))
			$mark_id_filter = $this->db->placehold('INNER JOIN __products_marks pm ON pm.product_id = p.id AND pm.mark_id in(?@)', (array)$filter['mark_id']);
			
if(isset($filter['date_from']) && !isset($filter['date_to'])){
$date_filter = $this->db->placehold('AND p.created > ?', $filter['date_from']);
}
elseif(isset($filter['date_to']) && !isset($filter['date_from'])){
$date_filter = $this->db->placehold('AND p.created < ?', $filter['date_to']);
}
elseif(isset($filter['date_to']) && isset($filter['date_from'])){
$date_filter = $this->db->placehold('AND (p.created BETWEEN ? AND ?)', $filter['date_from'], $filter['date_to']);
}

		if(!empty($filter['featured']))
			$is_featured_filter = $this->db->placehold('AND p.featured=?', intval($filter['featured']));

		if(!empty($filter['new']))
			$is_featured_filter = $this->db->placehold('AND p.new=?', intval($filter['new']));
            
        if(!empty($filter['smartbikes']))
			$smartbikes_filter = $this->db->placehold('AND p.smartbikes=?', intval($filter['smartbikes']));    
			
		if(!empty($filter['ttoday']))
			$is_featured_filter = $this->db->placehold('AND p.ttoday=?', intval($filter['ttoday']));

        if(!empty($filter['own_text']))
            $is_featured_filter = $this->db->placehold('AND p.own_text=?', intval($filter['own_text']));
 
		if(!empty($filter['yandex']))
			$is_featured_filter = $this->db->placehold('AND p.yandex=?', intval($filter['yandex']));
        
        if(!empty($filter['k50']))
			$is_featured_filter = $this->db->placehold('AND p.k50=?', intval($filter['k50']));    

        if(!empty($filter['priceru']))
			$is_featured_filter = $this->db->placehold('AND p.priceru=?', intval($filter['priceru'])); 
        
        if(!empty($filter['disc_less5'])){
			$is_featured_filter_d = $this->db->placehold('AND v.compare_price > 0 AND v.price > 0 AND ((v.compare_price - v.price)*100/v.price) < 5');  
            $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');      
        }
        if(!empty($filter['disc_more50'])){
			$is_featured_filter_d = $this->db->placehold('AND v.compare_price > 0 AND v.price > 0 AND ((v.compare_price - v.price)*100/v.price) > 50');  
            $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');      
        }
        if(!empty($filter['sales_note'])){
            if($filter['sales_note'] == 'empty'){
                $sales_notes_filter = $this->db->placehold("AND p.sales_notes=''");    
            }else{
                $sl = mysql_real_escape_string(trim($filter['sales_note']));
                $sales_notes_filter = $this->db->placehold("AND p.sales_notes LIKE '%$sl%'");    
            }
        }
        
		if(!empty($filter['card']))
			$is_featured_filter = $this->db->placehold('AND p.`id` IN (SELECT `product_id` FROM __variants pv WHERE pv.murl <> "") ' );
            
        if(!empty($filter['sklad'])){
            $sklad_filter = $this->db->placehold('AND v.store=?', $filter['sklad']);
            $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');
        }
		
		if(!empty($filter['no_id']))
			$keyword_filter = $this->db->placehold('AND p.id!=?', intval($filter['no_id']));            

		$price_filter = $min_price = $max_price = '';
        if(!empty($filter['min_price']))
			$min_price = $this->db->placehold('AND pv.price>=?', intval($filter['min_price']));;
		if(!empty($filter['max_price']))
			$max_price = $this->db->placehold('AND pv.price<=?', intval($filter['max_price']));
        if(!empty($min_price) || $max_price)
            $price_filter = $this->db->placehold("AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id $min_price $max_price /*AND (pv.stock IS NULL OR pv.stock>0)*/ LIMIT 1)", intval($filter['max_price']));
        /*if(!empty($filter['min_price']))
			$min_price = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price>=? AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1)', intval($filter['min_price']));;

		if(!empty($filter['max_price']))
			$max_price = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price<=? AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1)', intval($filter['max_price']));*/

		if(!empty($filter['discounted']))
			$discounted_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.compare_price>0 LIMIT 1) = ?', intval($filter['discounted']));
            
        if(!empty($filter['discounted_rev']))
			$discounted_rev_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.compare_price < pv.price AND pv.compare_price IS NOT NULL AND pv.compare_price>0 LIMIT 1) = ?', intval($filter['discounted_rev']));

		if(!empty($filter['in_stock']))
			$in_stock_filter = $this->db->placehold('AND ((SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price>0 AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1) = ?)', intval($filter['in_stock']));
            
        if(!empty($filter['pricenotnull']))
			$pricenotnull_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price>0 LIMIT 1)');

		if(!empty($filter['out_of_stock']))
			$in_stock_filter = $this->db->placehold('AND ((SELECT COUNT(*) FROM __variants pv WHERE pv.product_id=p.id AND (pv.stock IS NULL OR pv.stock>0)) = 0)');

		if(!empty($filter['visible']))
			$visible_filter = $this->db->placehold('AND p.visible=?', intval($filter['visible']));

 		if(!empty($filter['sort'])){
            $group_by = "GROUP BY p.id";
            //if($this->settings->view_in_stock)
                $in_stock = "(stock>0 OR stock IS NULL)";
            //else
            //    $in_stock = '1';
                
			switch ($filter['sort'])
			{
				case 'position':
                $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');
                $order_sklad = '';
                $order_instock_first = "IF((SELECT COUNT(*) FROM __variants WHERE $in_stock AND product_id=p.id LIMIT 1), 1, 0) DESC";
                if($this->settings->order_sklad)
                    $order_sklad = "(SELECT pv.store FROM s_variants pv WHERE 1 AND p.id = pv.product_id AND pv.store=1 LIMIT 1) DESC, (SELECT pv.store FROM s_variants pv WHERE 1 AND p.id = pv.product_id AND pv.store=3 LIMIT 1) DESC, (SELECT pv.store FROM s_variants pv WHERE 1 AND p.id = pv.product_id AND pv.store=2 LIMIT 1) DESC, ";
                    //$order_sklad = 'v.store=1 DESC,v.store=3 DESC,v.store=2 DESC,';
                if($this->settings->order_hits){
                    //
                    $order = $order_instock_first.', '.$order_sklad.'p.hits DESC, v.stock DESC';
                }else{
				    $order = $order_sklad.'IF((SELECT COUNT(*) FROM __variants WHERE '.$in_stock.' AND p.sklad_id=0 AND product_id=p.id LIMIT 1), 1, 0) DESC, p.sklad_id>0 DESC, p.position DESC';
                }
				break;
                case 'position_adm':
                $order = ' p.position DESC';
                break;
				case 'name':
				//$order = 'p.name';
				$order = 'IF((SELECT COUNT(*) FROM __variants WHERE '.$in_stock.' AND product_id=p.id LIMIT 1), 1, 0) DESC, p.name ASC';
				break;
                case 'position_r':
				$order = 'IF((SELECT COUNT(*) FROM __variants WHERE '.$in_stock.' AND product_id=p.id LIMIT 1), 1, 0) DESC, p.position ASC';
				break;
				case 'name_r':
				//$order = 'p.name';
				$order = 'IF((SELECT COUNT(*) FROM __variants WHERE '.$in_stock.' AND product_id=p.id LIMIT 1), 1, 0) DESC, p.name DESC';
				break;
				case 'created':
				$order = 'p.created DESC';
				break;
                case 'priceadm':
				    $order = 'v.price';
                    $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');
                    break;
                case 'priceadm_r':
				    $order = 'v.price DESC';
                    $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');
                    break;
				case 'price':
				//$order = 'p.price';
				//$order = '(SELECT pv.price FROM __variants pv WHERE (pv.stock IS NULL OR pv.stock>0) AND p.id = pv.product_id AND pv.price=(SELECT MIN(price) FROM __variants WHERE (stock>0 OR stock IS NULL) AND product_id=p.id LIMIT 1) LIMIT 1)';
				$order = '(SELECT -pv.price FROM __variants pv WHERE (pv.stock IS NULL OR pv.stock>0) AND p.id = pv.product_id LIMIT 1) DESC';
				break;
                case 'price_r':
				$order = '(SELECT pv.price FROM __variants pv WHERE (pv.stock IS NULL OR pv.stock>0) AND p.id = pv.product_id LIMIT 1) DESC';
				break;
				case 'viewed':
				$order = '(SELECT viewed FROM __variants WHERE (stock>0 OR stock IS NULL) AND product_id=p.id LIMIT 1) DESC';
				break;
				case 'random':
				$order = 'RAND()';
				break;
 				case 'rating':
				$order = 'p.rating DESC, p.position DESC';
				break; 
                case 'rating_r':
				$order = 'p.rating DESC, p.position ASC';
				break;               
			}
        }
        
        if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
			{
				$kw = mysql_real_escape_string(trim($keyword));
				if($kw!=='')
					$keyword_filter .= $this->db->placehold("AND (p.fullname LIKE '%$kw%' OR p.meta_keywords LIKE '%$kw%' OR p.id in (SELECT product_id FROM __variants WHERE sku LIKE '%$kw%'))");
			}
		}
		
        if(!empty($filter['manager']))
		{
			
			$manager_filter .= $this->db->placehold("AND (p.manager LIKE '%".mysql_real_escape_string(trim($filter['manager']))."%')");
			$group_by = "GROUP BY p.id";
		}

		/*if(!empty($filter['features']) && !empty($filter['features']))
			foreach($filter['features'] as $feature=>$value)
				$features_filter .= $this->db->placehold('AND p.id in (SELECT product_id FROM __options WHERE feature_id=? AND value in (?@) ) ', $feature, $value);*/
        /* chpu_filter_extended */
        /*if(!empty($filter['features']) && !empty($filter['features']))
			foreach($filter['features'] as $feature=>$value)
				$features_filter .= $this->db->placehold('AND p.id in (SELECT product_id FROM __options WHERE feature_id=? AND value=? ) ', $feature, $value);*/
        if(!empty($filter['features']) && !empty($filter['features'])){
            $i = 0;
            foreach($filter['features'] as $feature=>$value){
                $query = $this->db->placehold("SELECT product_id FROM __options WHERE feature_id=? AND translit in(?@)", $feature, (array)$value);
                //var_dump($query);
                $this->db->query($query);
                $products_ids_array[$i] = $this->db->results('product_id');
                $products_count[$i] = count($products_ids_array[$i]);
                $i++;
            }
            arsort($products_count);
            if(count($products_count)>1){
                foreach($products_count as $key=>$pc){
                    $ii[] =  $products_ids_array[$key];
                }
                $result_array = call_user_func_array('array_intersect', $ii);
            }else{
                $result_array = $products_ids_array[0];
            }
            
           	$features_filter = $this->db->placehold('AND p.id in(?@)', (array)$result_array);
            
            //if($_SESSION['admin']){ var_dump($products_ids_array[0]);die;}
        }
        //$price_filter = '';
        //$variant_join = '';
        /*if(isset($filter['price'])){
        	if(!empty($filter['price']['min']))
                $price_filter .= $this->db->placehold(' AND pv.price>= ? ', $this->db->escape(trim($filter['price']['min'])));
        	if(!empty($filter['price']['max']))
                $price_filter .= $this->db->placehold(' AND pv.price<= ? ', $this->db->escape(trim($filter['price']['max'])));
        	$variant_join = 'LEFT JOIN __variants pv ON pv.product_id = p.id';
        }*/
        /* chpu_filter_extended /*/
        if($filter['field_for_front']){
            $fields = $fields_for_front;
        }else{
            $fields = $default_fields;
        }
        
		$query = "SELECT  
				    $fields	
				FROM __products p		
				$category_id_filter 
				$mark_id_filter
                $variant_join	
				LEFT JOIN __brands b ON p.brand_id = b.id
				WHERE 
					1
					$product_id_filter
					$brand_id_filter
					$vendor_id_filter
					$features_filter
					$keyword_filter
					$manager_filter
					$date_filter
					$is_featured_filter
                    $is_featured_filter_d
					$discounted_filter
					$in_stock_filter
					$visible_filter
					$price_filter
                    $pricenotnull_filter
                    $sklad_filter
                    $smartbikes_filter
                    $discounted_rev_filter
                    $sales_notes_filter
				$group_by
				ORDER BY $order
					$sql_limit";

		$query = $this->db->placehold($query);
print('<!--');print($query);print('-->');
		$this->db->query($query);

		return $this->db->results();
	}

	/**
	* Р¤СѓРЅРєС†РёСЏ РІРѕР·РІСЂР°С‰Р°РµС‚ РєРѕР»РёС‡РµСЃС‚РІРѕ С‚РѕРІР°СЂРѕРІ
	* Р’РѕР·РјРѕР¶РЅС‹Рµ Р·РЅР°С‡РµРЅРёСЏ С„РёР»СЊС‚СЂР°:
	* category_id - id РєР°С‚РµРіРѕСЂРёРё РёР»Рё РёС… РјР°СЃСЃРёРІ
	* brand_id - id Р±СЂРµРЅРґР° РёР»Рё РёС… РјР°СЃСЃРёРІ
	* keyword - РєР»СЋС‡РµРІРѕРµ СЃР»РѕРІРѕ РґР»СЏ РїРѕРёСЃРєР°
	* features - С„РёР»СЊС‚СЂ РїРѕ СЃРІРѕР№СЃС‚РІР°Рј С‚РѕРІР°СЂР°, РјР°СЃСЃРёРІ (id СЃРІРѕР№СЃС‚РІР° => Р·РЅР°С‡РµРЅРёРµ СЃРІРѕР№СЃС‚РІР°)
	*/
	public function count_products($filter = array())
	{		
		$category_id_filter = '';
		$brand_id_filter = '';
		$mark_id_filter = '';
		$keyword_filter = '';
		$vendor_id_filter = '';
		$date_filter = '';
		$manager_filter = '';
		$visible_filter = '';
		$is_featured_filter = '';
        $in_stock_filter = '';
		//$min_price = '';
		//$max_price = '';
		$discounted_filter = '';
		$features_filter = '';
        $sklad_filter = '';
        $smartbikes_filter = '';
        $discounted_rev_filter = '';
        $variant_join = '';
        $sales_notes_filter = '';
        $is_featured_filter_d = '';
        
        
        if(!empty($filter['sklad'])){
            $sklad_filter = $this->db->placehold('AND v.store=?', $filter['sklad']);
            $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');
        }
		
		if(!empty($filter['category_id'])){
			$category_id_filter = $this->db->placehold('INNER JOIN __products_categories pc ON pc.product_id = p.id AND pc.category_id in(?@)', (array)$filter['category_id']);
        }elseif(empty($filter['keyword'])){
		    //Для скрытия импортируемых из XML товаров при выводе в админке по умолчанию 
            $category_id_filter = $this->db->placehold('INNER JOIN __products_categories pc ON pc.product_id = p.id AND pc.category_id NOT in(493)');
		}

		if(!empty($filter['brand_id']))
			$brand_id_filter = $this->db->placehold('AND p.brand_id in(?@)', (array)$filter['brand_id']);
        /* sshop_2016_08_17 */
        $brand_select = '';
        $brand_group = '';
        $brand_join = '';
        if(isset($filter['check_brands']) && !empty($filter['brand_id'])){
            $brand_select = ', b.id';
            $brand_group = 'GROUP BY b.id';
            $brand_join = 'LEFT JOIN __brands b ON p.brand_id = b.id';
        }
        /* sshop_2016_08_17 /*/
        
        if(!empty($filter['sales_note'])){
            if($filter['sales_note'] == 'empty'){
                $sales_notes_filter = $this->db->placehold("AND p.sales_notes=''");    
            }else{
                $sl = mysql_real_escape_string(trim($filter['sales_note']));
                $sales_notes_filter = $this->db->placehold("AND p.sales_notes LIKE '%$sl%'");    
            }
        }
		
        if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
			{
				$kw = mysql_real_escape_string(trim($keyword));
				if($kw!=='')
					$keyword_filter .= $this->db->placehold("AND (p.fullname LIKE '%$kw%' OR p.meta_keywords LIKE '%$kw%' OR p.id in (SELECT product_id FROM __variants WHERE sku LIKE '%$kw%'))");
			}
		}
		
        
		if(!empty($filter['vendor_id']))
			$vendor_id_filter = $this->db->placehold('AND p.vendor in(?@)', (array)$filter['vendor_id']);
		
		if(!empty($filter['manager']))
		{
			
				$man = trim($filter['manager']);
				$manager_filter .= $this->db->placehold("AND (p.manager LIKE '%$man%')");
			
		}
        
        if(!empty($filter['smartbikes']))
			$smartbikes_filter = $this->db->placehold('AND p.smartbikes=?', intval($filter['smartbikes']));   
		
		if(isset($filter['date_from']) && !isset($filter['date_to'])){
            $date_filter = $this->db->placehold('AND p.created > ?', $filter['date_from']);
        }
        elseif(isset($filter['date_to']) && !isset($filter['date_from'])){
            $date_filter = $this->db->placehold('AND p.created < ?', $filter['date_to']);
        }
        elseif(isset($filter['date_to']) && isset($filter['date_from'])){
            $date_filter = $this->db->placehold('AND (p.created BETWEEN ? AND ?)', $filter['date_from'], $filter['date_to']);
        }
		
		if(!empty($filter['mark_id']))
			$mark_id_filter = $this->db->placehold('INNER JOIN __products_marks pm ON pm.product_id = p.id AND pm.mark_id in(?@)', (array)$filter['mark_id']);

		if(!empty($filter['featured']))
			$is_featured_filter = $this->db->placehold('AND p.featured=?', intval($filter['featured']));

		if(!empty($filter['in_stock']))
			$in_stock_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price>0 AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1) = ?', intval($filter['in_stock']));

		if(!empty($filter['out_of_stock']))
			$in_stock_filter = $this->db->placehold('AND ((SELECT COUNT(*) FROM __variants pv WHERE pv.product_id=p.id AND (pv.stock IS NULL OR pv.stock>0)) = 0)');

		if(!empty($filter['discounted']))
			$discounted_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.compare_price>0 LIMIT 1) = ?', intval($filter['discounted']));
        
        if(!empty($filter['discounted_rev']))
			$discounted_rev_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.compare_price < pv.price AND pv.compare_price IS NOT NULL AND pv.compare_price>0 LIMIT 1) = ?', intval($filter['discounted_rev']));
        
		if(!empty($filter['visible']))
			$visible_filter = $this->db->placehold('AND p.visible=?', intval($filter['visible']));

		if(!empty($filter['new']))
			$is_featured_filter = $this->db->placehold('AND p.new=?', intval($filter['new']));
			
			
		if(!empty($filter['ttoday']))
            $is_featured_filter = $this->db->placehold('AND p.ttoday=?', intval($filter['ttoday']));

        if(!empty($filter['own_text']))
			$is_featured_filter = $this->db->placehold('AND p.own_text=?', intval($filter['own_text']));
      	  
	  	if(!empty($filter['card']))
			$is_featured_filter = $this->db->placehold('AND p.`id` IN (SELECT `product_id` FROM __variants pv WHERE pv.murl <> "") ' );
	  
		if(!empty($filter['yandex']))
			$is_featured_filter = $this->db->placehold('AND p.yandex=?', intval($filter['yandex']));  
            
        if(!empty($filter['k50']))
			$is_featured_filter = $this->db->placehold('AND p.k50=?', intval($filter['k50'])); 
        
        if(!empty($filter['priceru']))
			$is_featured_filter = $this->db->placehold('AND p.priceru=?', intval($filter['priceru'])); 
            
        if(!empty($filter['disc_less5'])){
			$is_featured_filter_d = $this->db->placehold('AND v.compare_price > 0 AND v.price > 0 AND ((v.compare_price - v.price)*100/v.price) < 5');  
            $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');      
        }         		
        if(!empty($filter['disc_more50'])){
			$is_featured_filter_d = $this->db->placehold('AND v.compare_price > 0 AND v.price > 0 AND ((v.compare_price - v.price)*100/v.price) > 50');  
            $variant_join = $this->db->placehold('LEFT JOIN __variants v ON v.product_id=p.id');      
        }
		$price_filter = $min_price = $max_price = '';
        if(!empty($filter['min_price']))
			$min_price = $this->db->placehold('AND pv.price>=?', intval($filter['min_price']));;
		if(!empty($filter['max_price']))
			$max_price = $this->db->placehold('AND pv.price<=?', intval($filter['max_price']));
        if(!empty($min_price) || $max_price)
            $price_filter = $this->db->placehold("AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id $min_price $max_price AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1)", intval($filter['max_price']));
        /*if(!empty($filter['min_price']))
			$min_price = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price>=? AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1)', intval($filter['min_price']));;

		if(!empty($filter['max_price']))
			$max_price = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price<=? AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1)', intval($filter['max_price']));*/ 
		
		/*if(!empty($filter['features']) && !empty($filter['features']))
			foreach($filter['features'] as $feature=>$value)
				$features_filter .= $this->db->placehold('AND p.id in (SELECT product_id FROM __options WHERE feature_id=? AND value in (?@) ) ', $feature, $value);*/
        if(!empty($filter['features']) && !empty($filter['features'])){
            $i = 0;
            foreach($filter['features'] as $feature=>$value){
                $query = $this->db->placehold("SELECT product_id FROM __options WHERE feature_id=? AND translit in(?@)", $feature, (array)$value);
                //var_dump($query);
                $this->db->query($query);
                $products_ids_array[$i] = $this->db->results('product_id');
                $products_count[$i] = count($products_ids_array[$i]);
                $i++;
            }
            arsort($products_count);
            if(count($products_count)>1){
                foreach($products_count as $key=>$pc){
                    $ii[] =  $products_ids_array[$key];
                }
                $result_array = call_user_func_array('array_intersect', $ii);
            }else{
                $result_array = $products_ids_array[0];
            }
            
           	$features_filter = $this->db->placehold('AND p.id in(?@)', (array)$result_array);
            
            //if($_SESSION['admin']){ var_dump($products_ids_array[0]);die;}
        }
		
		$query = "SELECT count(distinct p.id) as count $brand_select
				FROM __products AS p
				$category_id_filter
				$mark_id_filter	
                $variant_join
                $brand_join
				WHERE 1
					$brand_id_filter
					$date_filter
					$keyword_filter
					$vendor_id_filter
					$manager_filter
					$is_featured_filter
					$in_stock_filter
					$discounted_filter
					$visible_filter
					$price_filter
					$features_filter
                    $sklad_filter
                    $discounted_rev_filter
                    $is_featured_filter_d
                    $smartbikes_filter 
                    $brand_group
                    $sales_notes_filter";

		$this->db->query($query); 
//if($_SESSION['admin']){var_dump($query);}	
        if(isset($filter['check_brands']) && !empty($filter['brand_id']))
            return $this->db->results();
		return $this->db->result('count');
	}
	
	public function max_min_products($filter = array())
	{		
		$category_id_filter = '';
		$brand_id_filter = '';
		$keyword_filter = '';
		$visible_filter = '';
		$is_featured_filter = '';
		$in_stock_filter = '';
		$discounted_filter = '';
		$features_filter = '';
		
		if(!empty($filter['category_id']))
			$category_id_filter = $this->db->placehold('INNER JOIN __products_categories pc ON pc.product_id = p.id AND pc.category_id in(?@)', (array)$filter['category_id']);

		if(!empty($filter['brand_id']))
			$brand_id_filter = $this->db->placehold('AND p.brand_id in(?@)', (array)$filter['brand_id']);
		
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (p.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR p.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		if(!empty($filter['featured']))
			$is_featured_filter = $this->db->placehold('AND p.featured=?', intval($filter['featured']));

		if(!empty($filter['discounted']))
			$discounted_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.compare_price>0 LIMIT 1) = ?', intval($filter['discounted']));

		if(!empty($filter['visible']))
			$visible_filter = $this->db->placehold('AND p.visible=?', intval($filter['visible']));

		if(!empty($filter['in_stock']))
			$in_stock_filter = $this->db->placehold('AND (SELECT 1 FROM __variants pv WHERE pv.product_id=p.id AND pv.price>0 AND (pv.stock IS NULL OR pv.stock>0) LIMIT 1) = ?', intval($filter['in_stock']));

		if(!empty($filter['features']) && !empty($filter['features'])){
            $i = 0;
            foreach($filter['features'] as $feature=>$value){
                $query = $this->db->placehold("SELECT product_id FROM __options WHERE feature_id=? AND translit in(?@)", $feature, (array)$value);
                //var_dump($query);
                $this->db->query($query);
                $products_ids_array[$i] = $this->db->results('product_id');
                $products_count[$i] = count($products_ids_array[$i]);
                $i++;
            }
            arsort($products_count);
            if(count($products_count)>1){
                foreach($products_count as $key=>$pc){
                    $ii[] =  $products_ids_array[$key];
                }
                $result_array = call_user_func_array('array_intersect', $ii);
            }else{
                $result_array = $products_ids_array[0];
            }
            
           	$features_filter = $this->db->placehold('AND p.id in(?@)', (array)$result_array);
            
            //if($_SESSION['admin']){ var_dump($products_ids_array[0]);die;}
        }

		$query = "SELECT  MIN(pv.price) as min_price, MAX(pv.price) as max_price
				FROM __products AS p
				INNER JOIN __variants pv ON pv.product_id=p.id
				$category_id_filter
				WHERE 1
					$brand_id_filter
					$keyword_filter
					$is_featured_filter
					$discounted_filter
					$in_stock_filter
					$visible_filter
					$features_filter ";

		$this->db->query($query);	
		return $this->db->result();
	}


	public function get_price($filter = array())
	{		
		$product_id_filter = '';
		$order_filter = 'v.price';
		
		if(!empty($filter['product_id']))
			$product_id_filter = $this->db->placehold('AND v.product_id in(?@)', (array)$filter['product_id']);
		
			$variant_id_filter = $this->db->placehold('AND (v.stock>0 OR v.stock IS NULL)');
			
		if(!empty($filter['order']))
			$order_filter = 'v.price DESC';			
			
		if(!$product_id_filter)
			return false;
		
		$query = $this->db->placehold("SELECT v.product_id , v.price,v.stock
					FROM __variants AS v
					WHERE 
					1
					$product_id_filter
          $variant_id_filter             
					ORDER BY $order_filter       
					LIMIT 1");
		
		$this->db->query($query);	
		return $this->db->result();
	}

	/**
	* Р¤СѓРЅРєС†РёСЏ РІРѕР·РІСЂР°С‰Р°РµС‚ С‚РѕРІР°СЂ РїРѕ id
	* @param	$id
	* @retval	object
	*/
	public function get_product($id)
	{
		if(is_int($id))
			$filter = $this->db->placehold('p.id = ?', $id);
		else
			$filter = $this->db->placehold('p.url = ?', $id);
			
		$query = "SELECT DISTINCT
					p.id,
					p.date,
					p.url,
					p.brand_id,
					p.name,
                    p.fullname,
					p.annotation,
					p.body,
					p.position,
					p.created as created,
					p.manager,
                    p.visible, 
					p.own_text, 
					p.byrequest,
					p.featured,
					p.warranty,
					p.new,
					p.yandex,
					p.atribut,
					p.video,
					p.sklad, 
					p.sklad_id,                     
					p.meta_title, 
					p.meta_keywords, 
					p.meta_description,
					p.ttoday,
					p.rating,
					p.votes,
					count(cm.id) as comcon,
					sum(cm.rate) as comrate,
					p.vendor,
                    p.yandex,
                    p.type_prefix, p.seller_warranty, p.sales_notes,
                    p.free_delivery,
                    p.hits,
                    p.smartbikes,
                    p.last_parse,
                    p.generate_meta,
                    p.k50,
                    p.priceru,
                    p.generate_description,
                    p.delivery_description
				FROM __products AS p
                LEFT JOIN __brands b ON p.brand_id = b.id
				LEFT JOIN __comments cm ON cm.object_id = p.id
                WHERE $filter
                GROUP BY p.id
                LIMIT 1";
		$this->db->query($query);
		$product = $this->db->result();
		return $product;
	}

	public function update_product($id, $product)
	{
		$query = $this->db->placehold("UPDATE __products SET ?% WHERE id in (?@) LIMIT ?", $product, (array)$id, count((array)$id));
//var_dump($query);
		if($this->db->query($query))
			return $id;
		else
			return false;
	}
	
	public function add_product($product)
	{	
		$product = (array) $product;
		
		if(empty($product['url']))
		{
			$product['url'] = preg_replace("/[\s]+/ui", '-', $product['name']);
			$product['url'] = strtolower(preg_replace("/[^0-9a-zР°-СЏ\-]+/ui", '', $product['url']));
		}

		$man = $this->managers->get_manager();
		$product['manager'] =  $man->login;
		
		
		// Р•СЃР»Рё РµСЃС‚СЊ С‚РѕРІР°СЂ СЃ С‚Р°РєРёРј URL, РґРѕР±Р°РІР»СЏРµРј Рє РЅРµРјСѓ С‡РёСЃР»Рѕ
		while($this->get_product((string)$product['url']))
		{
			if(preg_match('/(.+)_([0-9]+)$/', $product['url'], $parts))
				$product['url'] = $parts[1].'_'.($parts[2]+1);
			else
				$product['url'] = $product['url'].'_2';
		}

		if($this->db->query("INSERT INTO __products SET ?%", $product))
		{
			$id = $this->db->insert_id();
			$this->db->query("UPDATE __products SET position=id WHERE id=?", $id);		
			return $id;
		}
		else
			return false;
	}
	
	
	
	/*
	*
	* РЈРґР°Р»РёС‚СЊ С‚РѕРІР°СЂ
	*
	*/	
	public function delete_product($id)
	{
		if(!empty($id))
		{
			// РЈРґР°Р»СЏРµРј РІР°СЂРёР°РЅС‚С‹
			$variants = $this->variants->get_variants(array('product_id'=>$id));
			foreach($variants as $v)
				$this->variants->delete_variant($v->id);
			
			// РЈРґР°Р»СЏРµРј РёР·РѕР±СЂР°Р¶РµРЅРёСЏ
			$images = $this->get_images(array('product_id'=>$id));
			foreach($images as $i)
				$this->delete_image($i->id);
			
            /* product_videos */
			// Удаляем видео
			$query = $this->db->placehold('DELETE FROM __products_videos WHERE product_id=?', $id);
			$this->db->query($query);
			/* product_videos **/
			
			// РЈРґР°Р»СЏРµРј РєР°С‚РµРіРѕСЂРёРё
			$categories = $this->categories->get_categories(array('product_id'=>$id));
			foreach($categories as $c)
				$this->categories->delete_product_category($id, $c->id);

			// РЈРґР°Р»СЏРµРј СЃРІРѕР№СЃС‚РІР°
			$options = $this->features->get_options(array('product_id'=>$id));
			foreach($options as $o)
				$this->features->delete_option($id, $o->feature_id);
			
			// РЈРґР°Р»СЏРµРј СЃРІСЏР·Р°РЅРЅС‹Рµ С‚РѕРІР°СЂС‹
			$related = $this->get_related_products($id);
			foreach($related as $r)
				$this->delete_related_product($id, $r->related_id);
			
			// РЈРґР°Р»СЏРµРј РѕС‚Р·С‹РІС‹
			$comments = $this->comments->get_comments(array('object_id'=>$id, 'type'=>'product'));
			foreach($comments as $c)
				$this->comments->delete_comment($c->id);
			
			// РЈРґР°Р»СЏРµРј РёР· РїРѕРєСѓРїРѕРє
			$this->db->query('UPDATE __purchases SET product_id=NULL WHERE product_id=?', intval($id));
			
			// РЈРґР°Р»СЏРµРј С‚РѕРІР°СЂ
			$query = $this->db->placehold("DELETE FROM __products WHERE id=? LIMIT 1", intval($id));
			if($this->db->query($query))
				return true;			
		}
		return false;
	}	
	
	public function duplicate_product($id)
	{
    	$product = $this->get_product($id);
    	$product->id = null;
    	$product->created = null;

		// РЎРґРІРёРіР°РµРј С‚РѕРІР°СЂС‹ РІРїРµСЂРµРґ Рё РІСЃС‚Р°РІР»СЏРµРј РєРѕРїРёСЋ РЅР° СЃРѕСЃРµРґРЅСЋСЋ РїРѕР·РёС†РёСЋ
    	$this->db->query('UPDATE __products SET position=position+1 WHERE position>?', $product->position);
    	$new_id = $this->products->add_product($product);
    	$this->db->query('UPDATE __products SET position=? WHERE id=?', $product->position+1, $new_id);
    	
    	// РћС‡РёС‰Р°РµРј url
    	$this->db->query('UPDATE __products SET url="" WHERE id=?', $new_id);
    	
		// Р”СѓР±Р»РёСЂСѓРµРј РєР°С‚РµРіРѕСЂРёРё
		$categories = $this->categories->get_product_categories($id);
		foreach($categories as $c)
			$this->categories->add_product_category($new_id, $c->category_id);
    	
    	// Р”СѓР±Р»РёСЂСѓРµРј РёР·РѕР±СЂР°Р¶РµРЅРёСЏ
    	$images = $this->get_images(array('product_id'=>$id));
    	foreach($images as $image)
    		$this->add_image($new_id, $image->filename);
    	
        /* product_videos */
    	$videos = $this->get_videos(array('product_id'=>$id));
    	foreach($videos as $video)
    		$this->add_product_video($new_id, $video->link);
		/* product_videos **/
        	
    	// Р”СѓР±Р»РёСЂСѓРµРј РІР°СЂРёР°РЅС‚С‹
    	$variants = $this->variants->get_variants(array('product_id'=>$id));
    	foreach($variants as $variant)
    	{
			unset($variant->brand_id);
			unset($variant->vendor);
			unset($variant->admprice);
    		$variant->product_id = $new_id;
    		unset($variant->id);
    		if($variant->infinity)
    			$variant->stock = null;
    		unset($variant->infinity);
    		$this->variants->add_variant($variant);
    	}
    	
    	// Р”СѓР±Р»РёСЂСѓРµРј СЃРІРѕР№СЃС‚РІР°
		$options = $this->features->get_options(array('product_id'=>$id));
		foreach($options as $o)
			$this->features->update_option($new_id, $o->feature_id, $o->value);
			
		// Р”СѓР±Р»РёСЂСѓРµРј СЃРІСЏР·Р°РЅРЅС‹Рµ С‚РѕРІР°СЂС‹
		$related = $this->get_related_products($id);
		foreach($related as $r)
			$this->add_related_product($new_id, $r->related_id);
			
    		
    	return $new_id;
	}
	

	
	function get_related_products($product_id = array())
	{
		if(empty($product_id))
			return array();

		$product_id_filter = $this->db->placehold('AND product_id in(?@)', (array)$product_id);
				
		$query = $this->db->placehold("SELECT product_id, related_id, position
					FROM __related_products
					WHERE 
					1
					$product_id_filter   
					ORDER BY position       
					");
		
		$this->db->query($query);
		$res = $this->db->results();
		
		if (!$res AND is_int((int)$product_id) AND (int)$product_id > 0 AND 1 == 2) 
		{
				$temprod = $this->get_product((int)$product_id);
				$pc = $this->categories->get_product_categories($product_id);
				//die(print_r($temprod));
				$tname = $temprod->name;
				$tbrand_id = $temprod->brand_id;
				$query = $this->db->placehold("SELECT ? as product_id, p.id as related_id, 1 as position FROM __products p LEFT JOIN __variants v ON v.product_id = p.id WHERE p.visible = 1 AND (v.stock > 0 OR v.stock is NULL)  AND p.id NOT IN (?) ORDER BY p.name LIMIT 4;",   (int)$product_id, (int)$product_id);
	 	//print($query);
		 	$this->db->query($query);
		    	$res = $this->db->results();
 
		}
		return $res;
	}
	
	// Р¤СѓРЅРєС†РёСЏ РІРѕР·РІСЂР°С‰Р°РµС‚ СЃРІСЏР·Р°РЅРЅС‹Рµ С‚РѕРІР°СЂС‹
	public function add_related_product($product_id, $related_id, $position=0)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __related_products SET product_id=?, related_id=?, position=?", $product_id, $related_id, $position);
		$this->db->query($query);
		return $related_id;
	}
	
	// РЈРґР°Р»РµРЅРёРµ СЃРІСЏР·Р°РЅРЅРѕРіРѕ С‚РѕРІР°СЂР°
	public function delete_related_product($product_id, $related_id)
	{
		$query = $this->db->placehold("DELETE FROM __related_products WHERE product_id=? AND related_id=? LIMIT 1", intval($product_id), intval($related_id));
		$this->db->query($query);
	}
	
	
	function get_images($filter = array())
	{		
		$product_id_filter = '';
        $id_filter = '';
		$group_by = '';

		if(!empty($filter['product_id']))
			$product_id_filter = $this->db->placehold('AND i.product_id in(?@)', (array)$filter['product_id']);

        if(!empty($filter['id']))
            $id_filter = $this->db->placehold('AND i.id in(?@)', (array)$filter['id']);

		// images
		$query = $this->db->placehold("SELECT i.id, i.product_id, i.name, i.filename, i.position
									FROM __images AS i WHERE 1 $product_id_filter $id_filter $group_by ORDER BY i.product_id, i.position");
		$this->db->query($query);
	//@author S.A
    //change palitra + photo
	//	return $this->db->results();
    //start
        $images = array();
        foreach($this->db->results() as $image)
            $images[$image->id] = $image;

        return $images;
    //end
    }
	
	public function add_image($product_id, $filename, $name = '')
	{
		$query = $this->db->placehold("SELECT id FROM __images WHERE product_id=? AND filename=?", $product_id, $filename);
		$this->db->query($query);
		$id = $this->db->result('id');
		if(empty($id))
		{
			$query = $this->db->placehold("INSERT INTO __images SET product_id=?, filename=?", $product_id, $filename);
			$this->db->query($query);
			$id = $this->db->insert_id();
			$query = $this->db->placehold("UPDATE __images SET position=id WHERE id=?", $id);
			$this->db->query($query);
		}
		return($id);
	}
	
	public function update_image($id, $image)
	{
	
		$query = $this->db->placehold("UPDATE __images SET ?% WHERE id=?", $image, $id);
		$this->db->query($query);
		
		return($id);
	}
	
	public function delete_image($id)
	{
		$query = $this->db->placehold("SELECT filename FROM __images WHERE id=?", $id);
		$this->db->query($query);
		$filename = $this->db->result('filename');
		$query = $this->db->placehold("DELETE FROM __images WHERE id=? LIMIT 1", $id);
		$this->db->query($query);
		$query = $this->db->placehold("SELECT count(*) as count FROM __images WHERE filename=? LIMIT 1", $filename);
		$this->db->query($query);
		$count = $this->db->result('count');
		if($count == 0)
		{			
			$file = pathinfo($filename, PATHINFO_FILENAME);
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			
			// РЈРґР°Р»РёС‚СЊ РІСЃРµ СЂРµСЃР°Р№Р·С‹
			$rezised_images = glob($this->config->root_dir.$this->config->resized_images_dir.$file."*.".$ext);
			if(is_array($rezised_images))
			foreach (glob($this->config->root_dir.$this->config->resized_images_dir.$file."*.".$ext) as $f)
				@unlink($f);

			@unlink($this->config->root_dir.$this->config->original_images_dir.$filename);		
		}
	}
		
	/*
	*
	* РЎР»РµРґСѓСЋС‰РёР№ С‚РѕРІР°СЂ
	*
	*/	
	public function get_next_product($id)
	{
		$this->db->query("SELECT position FROM __products WHERE id=? LIMIT 1", $id);
		$position = $this->db->result('position');
		
		$this->db->query("SELECT pc.category_id FROM __products_categories pc WHERE product_id=? ORDER BY position LIMIT 1", $id);
		$category_id = $this->db->result('category_id');
        
        $is_in_stock_order = '';
        //if($this->settings->order_hits)
        //    $is_in_stock_order = $this->db->placehold('( (SELECT count(pv.id) FROM __variants pv WHERE (pv.stock IS NULL OR pv.stock>0) AND p.id = pv.product_id)>0 ) DESC, ');
        
		$query = $this->db->placehold("SELECT id FROM __products p, __products_categories pc
										WHERE pc.product_id=p.id AND p.position>? 
										AND pc.position=(SELECT MIN(pc2.position) FROM __products_categories pc2 WHERE pc.product_id=pc2.product_id)
										AND pc.category_id=? 
										AND p.visible ORDER BY $is_in_stock_order p.position limit 1", $position, $category_id);
		$this->db->query($query);

		return $this->get_product((integer)$this->db->result('id'));
	}
	
	/*
	*
	* РџСЂРµРґС‹РґСѓС‰РёР№ С‚РѕРІР°СЂ
	*
	*/	
	public function get_prev_product($id)
	{
		$this->db->query("SELECT position FROM __products WHERE id=? LIMIT 1", $id);
		$position = $this->db->result('position');
		
		$this->db->query("SELECT pc.category_id FROM __products_categories pc WHERE product_id=? ORDER BY position LIMIT 1", $id);
		$category_id = $this->db->result('category_id');
        
        $is_in_stock_order = '';
        //if($this->settings->order_hits)
        //    $is_in_stock_order = $this->db->placehold('( (SELECT count(pv.id) FROM __variants pv WHERE (pv.stock IS NULL OR pv.stock>0) AND p.id = pv.product_id)>0 ) DESC, ');
        
		$query = $this->db->placehold("SELECT id FROM __products p, __products_categories pc
										WHERE pc.product_id=p.id AND p.position<? 
										AND pc.position=(SELECT MIN(pc2.position) FROM __products_categories pc2 WHERE pc.product_id=pc2.product_id)
										AND pc.category_id=? 
										AND p.visible ORDER BY $is_in_stock_order p.position DESC limit 1", $position, $category_id);
		$this->db->query($query);
 
		return $this->get_product((integer)$this->db->result('id'));	
    }
    
    public function check_store_status($store){
        $query = $this->db->query('UPDATE s_variants v 
                                    LEFT JOIN s_products p ON v.product_id=p.id
                                    SET v.store = NULL
                                    WHERE v.stock=0 AND v.store=?', $store);
                                    
        if($query)
            return 'Успешно обновили';
        
        return 'Обновить не получилось';    
    }
	
	
	/* product_videos */
	function get_videos($filter = array())
	{		
		$product_id_filter = '';
		$group_by = '';
		$videos = array();

		if(!empty($filter['product_id']))
			$product_id_filter = $this->db->placehold('AND product_id in(?@)', (array)$filter['product_id']);

		// images
		$query = $this->db->placehold("SELECT *
									FROM __products_videos WHERE 1 $product_id_filter $group_by ORDER BY product_id, position");
		$this->db->query($query);
		$results = $this->db->results();
		foreach($results as &$v){
			$tmp = explode('/',$v->link);
			$tmp = $tmp[count($tmp)-1];
			$tmp = stristr($tmp,'v=');
			$b_p = strpos($tmp,'&');
			if($b_p){
				$tmp = substr($tmp,0,$b_p);
			}
			$tmp = substr($tmp,2);
			$v->vid = $tmp;
			$videos[] = $v;
		}
		return $videos;
	}
	
	public function add_product_video($product_id, $link, $position)
	{
		$query = $this->db->placehold("SELECT id FROM __products_videos WHERE product_id=? AND link=?", $product_id, $link);
		$this->db->query($query);
		$id = $this->db->result('id');
		if(empty($id))
		{
			$query = $this->db->placehold("INSERT INTO __products_videos SET product_id=?, link=?", $product_id, $link);
			$this->db->query($query);
			$id = $this->db->insert_id();
			$query = $this->db->placehold("UPDATE __products_videos SET position=id WHERE id=?", $id);
			$this->db->query($query);
		}
		return($id);
	}
	/* product_videos **/
    
    /*auto_discount*/
    public function calc_auto_discount($product){
        $coupons = $this->coupons->calc_product_coupon($product->id);
        //auto_discount
        //$products->categories = $this->categories->get_categories(array('product_id'=>$product->id)); 
//        $category = reset($products->categories);
//        
//        //Получаем стразу список стоп-товаров
//        $ad_stop_products = $this->coupons->get_coupons(array('stop_related_product_id'=>$product->id, 'auto_discount'=>1, 'schedule'=>1));
//        foreach($products->categories as $pc){
//            $product_category_ids[] = $pc->id;
//        }
//        if(!$ad_stop_products){
//            if($auto_discount_products = $this->coupons->get_coupons(array('related_product_id'=>$product->id, 'auto_discount'=>1, 'schedule'=>1))){
//                //рассчитываем скидку для каждого купона, чтобы выбрать самую большую
//                foreach($auto_discount_products as $ad_prod){
//                    if($ad_prod->type == 'percentage')
//                        $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
//                    elseif($ad_prod->type == 'absolute')
//                        $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                }
//            }elseif($auto_discount_brands = $this->coupons->get_coupons(array('brand_id'=>$product->brand_id, 'auto_discount'=>1, 'schedule'=>1))){
//                if($auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$product_category_ids, 'auto_discount'=>1, 'schedule'=>1))){
//                    foreach($auto_discount_brands as $ad_prod){
//                        if($ad_prod->type == 'percentage')
//                            $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
//                        elseif($ad_prod->type == 'absolute')
//                            $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                    }
//                }
//            }/*elseif(!array_sum($auto_discount_brands) && $auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$product_category_ids, 'auto_discount'=>1))){
           //     foreach($auto_discount_categories as $ad_prod){
//                    if($ad_prod->type == 'percentage')
//                        $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
//                    elseif($ad_prod->type == 'absolute')
//                        $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                }
//            }*/
        //выбираем наибольшую скидку, получаем выбранный купон
        //arsort($coupons);
        $selected_coupon_id = key($coupons);
        $coupon_discount_price = reset($coupons);
        
        //применяем купон для вариантов
        foreach($product->variants as $variant){
            $variant->auto_coupon_price = max(0, $variant->price - $coupon_discount_price);
            $variant->coupon_discount_price = $coupon_discount_price;
            $variant->coupon_id = $selected_coupon_id;
        }
        $product->variant->coupon_discount_price = $coupon_discount_price;
        $product->variant->coupon_id = $selected_coupon_id;
        $product->variant->auto_coupon_price = max(0, $product->variant->price - $coupon_discount_price); 
        //}
        return $product;     
    }
	/*/auto_discount/*/	
    
    public function get_sales_notes($filter = array()){
        
        $product_id_filter = '';
        
        if(!empty($filter['product_id']))
			$product_id_filter = $this->db->placehold('AND p.id in(?@)', (array)$filter['id']);
            
        $this->db->query("SELECT DISTINCT(sales_notes) FROM s_products WHERE sales_notes!='' $product_id_filter");
        
        return $this->db->results('sales_notes');
    }

    public function get_product_id_today()
    {       
        $this->db->query("SELECT id FROM s_products WHERE ttoday=1 AND visible=1 ORDER BY rand() LIMIT 1");
        
        return $this->db->result('id');
    }
}