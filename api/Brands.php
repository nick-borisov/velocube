<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Brands extends Simpla
{
	/*
	*
	* Функция возвращает массив брендов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function get_brands($filter = array())
	{
		$order_filter = "b.position";
		$id_filter = '';
        $where = 'WHERE 1';
        
		$brands = array();
		$category_id_filter = '';
		if(!empty($filter['category_id'])){
			$category_id_filter = $this->db->placehold('LEFT JOIN __products p ON p.brand_id=b.id LEFT JOIN __products_categories pc ON p.id = pc.product_id WHERE pc.category_id in(?@)', (array)$filter['category_id']);
            $where = '';
        }
		if(!empty($filter['order']))
			$order_filter = "b.name";

        if(!empty($filter['id']))
			$id_filter = $this->db->placehold("AND b.id IN (?@)", (array)$filter['id']);

		// Выбираем все бренды
		$query = $this->db->placehold("SELECT DISTINCT b.id, b.name, b.position, b.url, b.meta_title, b.meta_keywords, b.meta_description, b.description, b.image, b.in_filter, b.nisale, b.meta_generation, b.generate_description, b.rus_name
								 		FROM __brands b $category_id_filter $where $id_filter ORDER BY $order_filter");
		$this->db->query($query);
//if($_SESSION['admin']) print($query);
		$br = $this->db->results();

		//foreach($br as $b){
//			if($b->position == 0){
//				$b->position = $b->id;
//				$this->db->query("UPDATE __brands SET position=id WHERE id=?", $b->position);
//			}
//		}
		return $br;
	}

	/*
	*
	* Функция возвращает бренд по его id или url
	* (в зависимости от типа аргумента, int - id, string - url)
	* @param $id id или url поста
	*
	*/
	public function get_brand($id)
	{
		if(is_int($id))			
			$filter = $this->db->placehold('id = ?', $id);
		else
			$filter = $this->db->placehold('url = ?', $id);
		$query = "SELECT id, name, url, position, meta_title, meta_keywords, meta_description, description, description_in_categories, image, in_filter, nisale, meta_generation, generate_description, rus_name
								 FROM __brands WHERE $filter LIMIT 1";
		$this->db->query($query);
		$brand = $this->db->result();
        if($brand){
            $brand->description_in_categories = unserialize($brand->description_in_categories);
            return $brand;
        }else
            return false;
	}

	/*
	*
	* Добавление бренда
	* @param $brand
	*
	*/
	public function add_brand($brand)
	{
		$brand = (array)$brand;
		if(empty($brand['url']))
		{
			$brand['url'] = preg_replace("/[\s]+/ui", '_', $brand['name']);
			$brand['url'] = strtolower(preg_replace("/[^0-9a-zа-я_]+/ui", '', $brand['url']));
		}
	
		$this->db->query("INSERT INTO __brands SET ?%", $brand);
		$id = $this->db->insert_id();
		$this->db->query("UPDATE __brands SET position=id WHERE id=?", $id);
		return $id;
	}

	/*
	*
	* Обновление бренда(ов)
	* @param $brand
	*
	*/		
	public function update_brand($id, $brand)
	{
		$query = $this->db->placehold("UPDATE __brands SET ?% WHERE id=? LIMIT 1", $brand, intval($id));
		$this->db->query($query);
		return $id;
	}
	
	/*
	*
	* Удаление бренда
	* @param $id
	*
	*/	
	public function delete_brand($id)
	{
		if(!empty($id))
		{
			$this->delete_image($id);	
			$query = $this->db->placehold("DELETE FROM __brands WHERE id=? LIMIT 1", $id);
			$this->db->query($query);		
			$query = $this->db->placehold("UPDATE __products SET brand_id=NULL WHERE brand_id=?", $id);
			$this->db->query($query);	
		}
	}
	
	/*
	*
	* Удаление изображения бренда
	* @param $id
	*
	*/
	public function delete_image($brand_id)
	{
		$query = $this->db->placehold("SELECT image FROM __brands WHERE id=?", intval($brand_id));
		$this->db->query($query);
		$filename = $this->db->result('image');
		if(!empty($filename))
		{
			$query = $this->db->placehold("UPDATE __brands SET image=NULL WHERE id=?", $brand_id);
			$this->db->query($query);
			$query = $this->db->placehold("SELECT count(*) as count FROM __brands WHERE image=? LIMIT 1", $filename);
			$this->db->query($query);
			$count = $this->db->result('count');
			if($count == 0)
			{			
				@unlink($this->config->root_dir.$this->config->brands_images_dir.$filename);		
			}
		}
	}
    
    public function yandex_brand($id, $yandex){
        $query = $this->db->placehold("UPDATE __products p SET yandex=? WHERE p.brand_id = ?", $yandex['to_yandex'], $id);
        $this->db->query($query);
        return is_int($id);
    }
	
	//Выбираем все категории, товары которых относятся к выбранному БРЕНДу
	public function get_brand_categories($brand_id) 
	{
		//Производим выборку всех категорий в которых есть товары этого Бренда
		$this->db->query("SELECT c.id, c.name, c.url, b.url AS brand, count(p.id) as `products` 
							FROM s_categories AS c
							INNER JOIN s_products_categories AS pc
							ON pc.category_id = c.id
							INNER JOIN s_products AS p
							ON p.id = pc.product_id
							INNER JOIN s_brands AS b
							ON b.id = p.brand_id
							WHERE b.id=?
							AND c.visible = 1
							GROUP BY c.id
							ORDER BY c.name ASC", (int)$brand_id); 
		$categories = $this->db->results();
		
		foreach($categories as $key => $category)
		{
			$categoryInfo = $this->categories->get_category($categories[$key]->url);
			
			foreach($categoryInfo->path as $path)
			{
				$categories[$key]->urlPath .= $path->url."/";
			}
			$categories[$key]->urlPath = substr($categories[$key]->urlPath,0,(strlen($categories[$key]->urlPath)-1));
		}
		
		return $categories;
	}
    
    public function get_brand_categories_4_seo($brand_id) 
	{
		//Производим выборку всех категорий в которых есть товары этого Бренда
		$this->db->query("SELECT c.id, c.name, c.url, c.skl, b.name as brand_name
							FROM s_categories AS c
							INNER JOIN s_products_categories AS pc
							ON pc.category_id = c.id
							INNER JOIN s_products AS p
							ON p.id = pc.product_id
							INNER JOIN s_brands AS b
							ON b.id = p.brand_id
							WHERE b.id=?
							AND c.visible = 1
							GROUP BY c.id
							ORDER BY c.name ASC", (int)$brand_id); 
		$categories = $this->db->results();
		
		return $categories;
	}
}