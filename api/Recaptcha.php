<?php

require_once('Simpla.php');

class Recaptcha extends Simpla
{
	public function send($response,$remoteip)
	{
	
		$ch = curl_init("https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array(
    		"secret"		=>	"6Le3YhUUAAAAAOekY3C0niQMfXuduqTD1Wu8CWaL",
    		"response"		=>	$response,
            "remoteip"      =>  $remoteip
        ));
        $body = curl_exec($ch);
        curl_close($ch); 
        //var_dump($body);    
        return $body;

	}
}