<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Callbacks extends Simpla
{

    public function email_callback_admin($callback_id)
	{
        //$callback = $this->get_callback(intval($callback_id));

		if(!($callback = $this->get_callback(intval($callback_id)))){
			return false;
		}
        $this->design->assign('callback', $callback);
		// Отправляем письмо
        
		$email_template = $this->design->fetch($this->config->root_dir.'simpla/design/html/email_callback_admin.tpl');
		$subject = $this->design->get_var('subject');
		$this->notify->email($this->settings->callback_email, $subject, $email_template, "$callback->name <$callback->phone>", "$callback->name <$callback->phone>");

	}    
    
	public function get_callback($id)
	{
		$query = $this->db->placehold("SELECT c.id, c.name, c.phone, c.message, c.date, c.status, c.time_from, c.time_to, c.call_me_intime, c.url FROM __callbacks c WHERE id=? LIMIT 1", intval($id));

		if($this->db->query($query))
			return $this->db->result();
		else
			return false; 
	}
	
	public function get_callbacks($filter = array(), $new_on_top = false)
	{	
		// По умолчанию
		$limit = 1000;
		$page = 1;
        $keyword_filter = '';
        $status = '';
 

		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));
        
        if(isset($filter['status'])){
            switch($filter['status']){
                case 1: {
                    $status = $this->db->placehold('AND status=?', $filter['status']);
                    break;
                }
                case 2: {
                    $status = $this->db->placehold('AND status=?', $filter['status']);
                    break;
                }
                case 3: {
                    $status = $this->db->placehold('AND status=?', $filter['status']);
                    break;
                }
            }
        }
        
        if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
			{
				$kw = $this->db->escape(trim($keyword));
				$keyword_filter .= $this->db->placehold("AND (c.name LIKE '%$kw%' OR c.phone LIKE '%$kw%')");
			}
		}
		
		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		if($new_on_top)
			$sort='DESC';
		else
			$sort='ASC';

		$query = $this->db->placehold("SELECT c.id,
                                              c.name,
                                              c.phone, 
                                              c.date, 
                                              c.message, 
                                              c.time_from, 
                                              c.time_to, 
                                              c.call_me_intime, 
                                              c.url, 
                                              c.status
                                        FROM __callbacks c 
                                        WHERE 1 
                                        $status
                                        $keyword_filter 
                                        ORDER BY c.id $sort $sql_limit");
	
		$this->db->query($query);
		return $this->db->results();
	}
	
	
	
	public function add_callback($callback)
	{	
		$query = $this->db->placehold('INSERT INTO __callbacks
		SET ?%,
		date = NOW()',
		$callback);
       		
		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();
		return $id;
	}
	
	
	public function update_callback($id, $callback)
	{
		$date_query = '';
		/*if(isset($fedback->date))
		{
			$date = $callback->date;
			unset($callback->date);
			$date_query = $this->db->placehold(', date=STR_TO_DATE(?, ?)', $date, $this->settings->date_format);
		}*/
		$query = $this->db->placehold("UPDATE __callbacks SET ?% $date_query WHERE id in(?@) LIMIT ?", $callback, (array)$id, count($id));
		$this->db->query($query);
		return $id;
	}


	public function delete_callback($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __callbacks WHERE id=? LIMIT 1", intval($id));
			$this->db->query($query);
		}
	}
    
    // Количество комментариев, удовлетворяющих фильтру
	public function count_callbacks($filter = array())
	{	

		$approved_filter = '';
		$keyword_filter = '';

		if(isset($filter['status']))
			$approved_filter = $this->db->placehold('AND c.status=?', intval($filter['status']));

		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND c.name LIKE "%'.$this->db->escape(trim($keyword)).'%" OR c.phone LIKE "%'.$this->db->escape(trim($keyword)).'%" ');
		}

		$query = $this->db->placehold("SELECT count(distinct c.id) as count
										FROM __callbacks c WHERE 1 $keyword_filter $approved_filter", $this->settings->date_format);
	
		$this->db->query($query);	
		return $this->db->result('count');

	}		
}
