<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Orders extends Simpla
{

	public function get_order($id)
	{
		if(is_int($id))
			$where = $this->db->placehold(' WHERE o.id=? ', intval($id));
		else
			$where = $this->db->placehold(' WHERE o.url=? ', $id);
		
		$query = $this->db->placehold("SELECT  o.id, o.delivery_id, o.delivery_price,o.delivery_date, o.delivery_time, o.separate_delivery, o.passport,
										o.payment_method_id, o.paid, o.payment_date, o.closed, o.discount, o.coupon_code, o.coupon_discount,
										o.date, o.user_id, o.name, o.address, o.phone, o.phone2, o.email, o.email2, o.comment, o.status, o.manager,
										o.url, o.total_price, o.note, o.ip, o.trnomer, o.discount_type, o.smartbikes, o.allow_to_pay, o.isfastorder
										FROM __orders o $where LIMIT 1");

		if($this->db->query($query))
			return $this->db->result();
		else
			return false; 
	}
	
	function get_orders($filter = array())
	{
		// По умолчанию
		$limit = 100;
		$page = 1;
		$keyword_filter = '';	
		$label_filter = '';	
		$status_filter = '';
		$user_filter = '';	
		$modified_from_filter = '';	
		$id_filter = '';
		$date_filter = '';
		$delivery_filter = '';
        $coupon_filter = '';
		
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);
		
			
		if(isset($filter['status']))
			$status_filter = $this->db->placehold('AND o.status = ?', intval($filter['status']));
		
		if(isset($filter['id']))
			$id_filter = $this->db->placehold('AND o.id in(?@)', (array)$filter['id']);
		
		if(isset($filter['user_id']))
			$user_filter = $this->db->placehold('AND o.user_id = ?', intval($filter['user_id']));
        
        if(isset($filter['coupon_code']))
			$coupon_filter = $this->db->placehold('AND o.coupon_code = ?', strval($filter['coupon_code']));    
		
		if(isset($filter['modified_from']))
			$modified_from_filter = $this->db->placehold('AND o.modified > ?', $filter['modified_from']);
		
		if(isset($filter['label']))
			$label_filter = $this->db->placehold('AND ol.label_id = ?', $filter['label']);
			
        if(isset($filter['date_from']) && !isset($filter['date_to'])){
        $date_filter = $this->db->placehold('AND o.date > ?', $filter['date_from']);
        }
        elseif(isset($filter['date_to']) && !isset($filter['date_from'])){
        $date_filter = $this->db->placehold('AND o.date < ?', $filter['date_to']);
        }
        elseif(isset($filter['date_to']) && isset($filter['date_from'])){
        $date_filter = $this->db->placehold('AND (o.date BETWEEN ? AND ?)', $filter['date_from'], $filter['date_to']);
        }
        if(isset($filter['delivery_from']) && !isset($filter['delivery_to'])){
        $date_filter = $this->db->placehold('AND o.delivery_date >= DATE(?)', $filter['delivery_from']);
        }
        elseif(isset($filter['delivery_to']) && !isset($filter['delivery_from'])){
        $date_filter = $this->db->placehold('AND o.delivery_date <= ?', $filter['delivery_to']);
        }
        elseif(isset($filter['delivery_to']) && isset($filter['delivery_from'])){
        $date_filter = $this->db->placehold("AND o.delivery_date BETWEEN DATE(?) 
          AND DATE(?)", $filter['delivery_from'], $filter['delivery_to']);
        }			
		
		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (o.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR REPLACE(o.phone, "-", "")  LIKE "%'.mysql_real_escape_string(str_replace('-', '', trim($keyword))).'%" OR o.address LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR o.id LIKE "'.mysql_real_escape_string(trim($keyword)).'")');
		}
		
		// Выбираем заказы
		$query = $this->db->placehold("SELECT o.id, o.delivery_id, o.delivery_price,o.delivery_date, o.delivery_time, o.separate_delivery, o.passport,
										o.payment_method_id, o.paid, o.payment_date, o.closed, o.discount, o.coupon_code, o.coupon_discount,
										o.date, o.user_id, o.name, o.address, o.phone, o.phone2, o.email, o.email2, o.comment, o.status, o.manager,
										o.url, o.total_price, o.note, o.ip, o.trnomer, o.discount_type, o.smartbikes, o.allow_to_pay, o.isfastorder
									FROM __orders AS o 
									LEFT JOIN __orders_labels AS ol ON o.id=ol.order_id 
									WHERE 1
									$id_filter $status_filter $user_filter $keyword_filter $date_filter $delivery_filter $label_filter $modified_from_filter $coupon_filter GROUP BY o.id ORDER BY status, id DESC $sql_limit", "%Y-%m-%d");
		$this->db->query($query);
        //if($_SESSION['admin']) print($query);
		$orders = array();
		foreach($this->db->results() as $order)
			$orders[$order->id] = $order;
		return $orders;
	}

	function count_orders($filter = array())
	{
		$keyword_filter = '';	
		$label_filter = '';	
		$status_filter = '';
		$user_filter = '';
		$date_filter = '';	
		
		if(isset($filter['status']))
			$status_filter = $this->db->placehold('AND o.status = ?', intval($filter['status']));
		
		if(isset($filter['user_id']))
			$user_filter = $this->db->placehold('AND o.user_id = ?', intval($filter['user_id']));

		if(isset($filter['label']))
			$label_filter = $this->db->placehold('AND ol.label_id = ?', $filter['label']);
		
		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (o.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR REPLACE(o.phone, "-", "")  LIKE "%'.mysql_real_escape_string(str_replace('-', '', trim($keyword))).'%" OR o.address LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" )');
		}
		
if(isset($filter['date_from']) && !isset($filter['date_to'])){
$date_filter = $this->db->placehold('AND o.date > ?', $filter['date_from']);
}
elseif(isset($filter['date_to']) && !isset($filter['date_from'])){
$date_filter = $this->db->placehold('AND o.date < ?', $filter['date_to']);
}
elseif(isset($filter['date_to']) && isset($filter['date_from'])){
$date_filter = $this->db->placehold('AND (o.date BETWEEN ? AND ?)', $filter['date_from'], $filter['date_to']);
}			
		
		// Выбираем заказы
		$query = $this->db->placehold("SELECT COUNT(DISTINCT id) as count
									FROM __orders AS o 
									LEFT JOIN __orders_labels AS ol ON o.id=ol.order_id 
									WHERE 1
									$status_filter $date_filter $user_filter $label_filter $keyword_filter");
		$this->db->query($query);
		return $this->db->result('count');
	}

	public function update_order($id, $order)
	{
		$query = $this->db->placehold("UPDATE __orders SET ?%, modified=now() WHERE id=? LIMIT 1", $order, intval($id));
		$this->db->query($query);
		$this->update_total_price(intval($id));
		return $id;
	}

	public function delete_order($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __purchases WHERE order_id=?", $id);
			$this->db->query($query);
			
			$query = $this->db->placehold("DELETE FROM __order_comments WHERE object_id=?", intval($id));
			$this->db->query($query); 			
			
			$query = $this->db->placehold("DELETE FROM __orders WHERE id=? LIMIT 1", $id);
			$this->db->query($query);
			
		}
	}
	
	public function add_order($order)
	{
		$order = (object)$order;
		$order->url = md5(uniqid($this->config->salt, true));
		$set_curr_date = '';
		if(empty($order->date))
			$set_curr_date = ', date=now()';
		$query = $this->db->placehold("INSERT INTO __orders SET ?%$set_curr_date", $order);
		$this->db->query($query);
		$id = $this->db->insert_id();		
		return $id;
	}

	public function get_label($id)
	{
		$query = $this->db->placehold("SELECT * FROM __labels WHERE id=? LIMIT 1", intval($id));
		$this->db->query($query);
		return $this->db->result();
	}

	public function get_labels()
	{
		$query = $this->db->placehold("SELECT * FROM __labels ORDER BY position");
		$this->db->query($query);
		return $this->db->results();
	}

	/*
	*
	* Создание метки заказов
	* @param $label
	*
	*/	
	public function add_label($label)
	{	
		$query = $this->db->placehold('INSERT INTO __labels SET ?%', $label);
		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();
		$this->db->query("UPDATE __labels SET position=id WHERE id=?", $id);	
		return $id;
	}
	
	
	/*
	*
	* Обновить метку
	* @param $id, $label
	*
	*/	
	public function update_label($id, $label)
	{
		$query = $this->db->placehold("UPDATE __labels SET ?% WHERE id in(?@) LIMIT ?", $label, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}

	/*
	*
	* Удалить метку
	* @param $id
	*
	*/	
	public function delete_label($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __orders_labels WHERE label_id=?", intval($id));
			if($this->db->query($query))
			{
				$query = $this->db->placehold("DELETE FROM __labels WHERE id=? LIMIT 1", intval($id));
				return $this->db->query($query);
			}
			else
			{
				return false;
			}
		}
	}	
	
	function get_order_labels($order_id = array())
	{
		if(empty($order_id))
			return array();

		$label_id_filter = $this->db->placehold('AND order_id in(?@)', (array)$order_id);
				
		$query = $this->db->placehold("SELECT ol.order_id, l.id, l.name, l.color, l.position
					FROM __labels l LEFT JOIN __orders_labels ol ON ol.label_id = l.id
					WHERE 
					1
					$label_id_filter   
					ORDER BY position       
					");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
	public function update_order_labels($id, $labels_ids)
	{
		$labels_ids = (array)$labels_ids;
		$query = $this->db->placehold("DELETE FROM __orders_labels WHERE order_id=?", intval($id));
		$this->db->query($query);
		if(is_array($labels_ids))
		foreach($labels_ids as $l_id)
			$this->db->query("INSERT INTO __orders_labels SET order_id=?, label_id=?", $id, $l_id);
	}

	public function add_order_labels($id, $labels_ids)
	{
		$labels_ids = (array)$labels_ids;
		if(is_array($labels_ids))
		foreach($labels_ids as $l_id)
		{
			$this->db->query("INSERT IGNORE INTO __orders_labels SET order_id=?, label_id=?", $id, $l_id);
		}
	}

	public function delete_order_labels($id, $labels_ids)
	{
		$labels_ids = (array)$labels_ids;
		if(is_array($labels_ids))
		foreach($labels_ids as $l_id)
			$this->db->query("DELETE FROM __orders_labels WHERE order_id=? AND label_id=?", $id, $l_id);
	}


	public function get_purchase($id)
	{
		$query = $this->db->placehold("SELECT * FROM __purchases WHERE id=? LIMIT 1", intval($id));
		$this->db->query($query);
		return $this->db->result();
	}

	public function get_purchases($filter = array())
	{
		$order_id_filter = '';
		if(!empty($filter['order_id']))
			$order_id_filter = $this->db->placehold('AND order_id in(?@)', (array)$filter['order_id']);

		$query = $this->db->placehold("SELECT * FROM __purchases WHERE 1 $order_id_filter ORDER BY id");
		$this->db->query($query);
		return $this->db->results();
	}
	
	public function update_purchase($id, $purchase)
	{	
		$purchase = (object)$purchase;
		$old_purchase = $this->get_purchase($id);
		if(!$old_purchase)
			return false;
			
		$order = $this->get_order(intval($old_purchase->order_id));
		if(!$order)
			return false;
			
		// Если заказ закрыт, нужно обновить склад при изменении покупки
		if($order->closed && !empty($purchase->amount))
		{
			if($old_purchase->variant_id != $purchase->variant_id)
			{
				if(!empty($old_purchase->variant_id))
				{
					$query = $this->db->placehold("UPDATE __variants SET stock=stock+? WHERE id=? AND stock IS NOT NULL LIMIT 1", $old_purchase->amount, $old_purchase->variant_id);
					$this->db->query($query);
				}
				if(!empty($purchase->variant_id))
				{
					$query = $this->db->placehold("UPDATE __variants SET stock=stock-? WHERE id=? AND stock IS NOT NULL LIMIT 1", $purchase->amount, $purchase->variant_id);
					$this->db->query($query);
				}
			}
			elseif(!empty($purchase->variant_id))
			{
				$query = $this->db->placehold("UPDATE __variants SET stock=stock+(?) WHERE id=? AND stock IS NOT NULL LIMIT 1", $old_purchase->amount - $purchase->amount, $purchase->variant_id);
				$this->db->query($query);
			}
              
		}
		/*auto_discount*/
        if(!empty($purchase->apply_auto_coupon)){
            $purchase = $this->calc_auto_discount($purchase);
        }
        /*/auto_discount/*/
        
		$query = $this->db->placehold("UPDATE __purchases SET ?% WHERE id=? LIMIT 1", $purchase, intval($id));
		$this->db->query($query);
		$this->update_total_price($order->id);		
		return $id;
	}
	
	public function add_purchase($purchase)
	{
		$purchase = (object)$purchase;
		if(!empty($purchase->variant_id))
		{
			$variant = $this->variants->get_variant($purchase->variant_id);
			if(empty($variant))
				return false;
			$product = $this->products->get_product(intval($variant->product_id));
			if(empty($product))
				return false;
		}			

		$order = $this->get_order(intval($purchase->order_id));
		if(empty($order))
			return false;				
	
		$brand = $this->brands->get_brand(intval($product->brand_id));
		if(!isset($purchase->product_id) && isset($variant))
			$purchase->product_id = $variant->product_id;
				
		if(!isset($purchase->product_name)  && !empty($product))
			$purchase->product_name = $product->type_prefix.' '.$brand->name.' '.$product->name;
			
		if(!isset($purchase->sku) && !empty($variant))
			$purchase->sku = $variant->sku;

		if(!isset($purchase->stockprice) && !empty($variant))
			$purchase->stockprice = $variant->stockprice;
			
		if(!isset($purchase->variant_name) && !empty($variant))
			$purchase->variant_name = $variant->name;
		
      
        
		if(!isset($purchase->price) && !empty($variant))
            if($purchase->auto_coupon){
                $coupon = $this->coupons->get_coupon(intval($purchase->auto_coupon));
                //если есть интервалы для цены, работаем с ними
                if($coupon->active_margin){
                    $margins = unserialize($coupon->margin);
    				if($margins['min'][0] && $margins['max'][0]){  
                        foreach($margins['min'] as $i=>$min){
                            if($variant->price > intval($margins['min'][$i]) && $variant->price < intval($margins['max'][$i]))
                            {
                                if(intval($margins['mode'][$i])==1){
                                    $purchase->price = max(0, $variant->price - $margins['price'][$i]);
                                }elseif(intval($margins['mode'][$i])==2){
                                    $purchase->price = round($variant->price * (100 - $margins['price'][$i]) / 100, 0); 
                                }
                            }
                        }
    				}
                }else{//если интервалов нет
                    if($coupon->type=="percentage")
                        $purchase->price = round($variant->price*(100-$coupon->value)/100, 2);
                    elseif($coupon->type=="absolute")
                        $purchase->price = max(0, $variant->price-$coupon->value);
                }
                    
                $purchase->dirty_price = $variant->price;
			}else
                $purchase->price = $variant->price;
			
		if(!isset($purchase->amount))
			$purchase->amount = 1;

		// Если заказ закрыт, нужно обновить склад при добавлении покупки
		if($order->closed && !empty($purchase->amount) && !empty($variant->id))
		{
			$stock_diff = $purchase->amount;
			$query = $this->db->placehold("UPDATE __variants SET stock=stock-? WHERE id=? AND stock IS NOT NULL LIMIT 1", $stock_diff, $variant->id);
			$this->db->query($query);
		}

		$query = $this->db->placehold("INSERT INTO __purchases SET ?%", $purchase);

		$this->db->query($query);
		$purchase_id = $this->db->insert_id();
		
		$this->update_total_price($order->id);		
		return $purchase_id;
	}

	public function delete_purchase($id)
	{
		$purchase = $this->get_purchase($id);
		if(!$purchase)
			return false;
			
		$order = $this->get_order(intval($purchase->order_id));
		if(!$order)
			return false;

		// Если заказ закрыт, нужно обновить склад при изменении покупки
		if($order->closed && !empty($purchase->amount))
		{
			$stock_diff = $purchase->amount;
			$query = $this->db->placehold("UPDATE __variants SET stock=stock+? WHERE id=? AND stock IS NOT NULL LIMIT 1", $stock_diff, $purchase->variant_id);
			$this->db->query($query);
		}
		
		$query = $this->db->placehold("DELETE FROM __purchases WHERE id=? LIMIT 1", intval($id));
		$this->db->query($query);
		$this->update_total_price($order->id);				
		return true;
	}

	
	public function close($order_id)
	{
		$order = $this->get_order(intval($order_id));
		if(empty($order))
			return false;
		
		if(!$order->closed)
		{
			$variants_amounts = array();
			$purchases = $this->get_purchases(array('order_id'=>$order->id));
			foreach($purchases as $purchase)
			{
				if(isset($variants_amounts[$purchase->variant_id]))
					$variants_amounts[$purchase->variant_id] += $purchase->amount;
				else
					$variants_amounts[$purchase->variant_id] = $purchase->amount;
			}

			foreach($variants_amounts as $id=>$amount)
			{
				$variant = $this->variants->get_variant($id);
				if(empty($variant) || ($variant->stock<$amount))
					return false;
			}
			foreach($purchases as $purchase)
			{
			
                
                $query = $this->db->query("UPDATE s_products p SET p.hits=IFNULL(p.hits, 0)+? WHERE id=? LIMIT 1", $purchase->amount, $purchase->product_id);
                //print_r($query);
				$variant = $this->variants->get_variant($purchase->variant_id);
				if(!$variant->infinity)
				{
					$new_stock = $variant->stock-$purchase->amount;
					$this->variants->update_variant($variant->id, array('stock'=>$new_stock));
				}
			}				
			$query = $this->db->placehold("UPDATE __orders SET closed=1, modified=NOW() WHERE id=? LIMIT 1", $order->id);
			$this->db->query($query);
		}
		return $order->id;
	}

	public function open($order_id)
	{
		$order = $this->get_order(intval($order_id));
		if(empty($order))
			return false;
		
		if($order->closed)
		{
			$purchases = $this->get_purchases(array('order_id'=>$order->id));
			foreach($purchases as $purchase)
			{
                $query = $this->db->query("UPDATE s_products p SET p.hits=IFNULL(p.hits, 0)-? WHERE id=? LIMIT 1", $purchase->amount, $purchase->product_id);
                //print($query);
				$variant = $this->variants->get_variant($purchase->variant_id);				
				if($variant && !$variant->infinity)
				{
					$new_stock = $variant->stock+$purchase->amount;
					$this->variants->update_variant($variant->id, array('stock'=>$new_stock));
				}
			}				
			$query = $this->db->placehold("UPDATE __orders SET closed=0, modified=NOW() WHERE id=? LIMIT 1", $order->id);
			$this->db->query($query);
		}
		return $order->id;
	}
	
	public function pay($order_id)
	{
		$order = $this->get_order(intval($order_id));
		if(empty($order))
			return false;
		
		if(!$this->close($order->id))
		{
			return false;
		}
		$query = $this->db->placehold("UPDATE __orders SET payment_status=1, payment_date=NOW(), modified=NOW() WHERE id=? LIMIT 1", $order->id);
		$this->db->query($query);
		return $order->id;
	}
	
	private function update_total_price($order_id)
	{
		$order = $this->get_order(intval($order_id));
		if(empty($order))
			return false;
		if($order->discount_type==1){
            $query = $this->db->placehold("UPDATE __orders o SET o.total_price=IFNULL(
                (SELECT SUM( IF( p.auto_coupon, (p.dirty_price * p.amount), ( p.price * p.amount ))
                 * ( 100 - o.discount) / 100
                  + IF(p.auto_coupon, - (( p.dirty_price - p.price ) * p.amount ) , 0) ) 
                FROM s_purchases p
                WHERE p.order_id=o.id), 0)
                +o.delivery_price*(1-o.separate_delivery)-o.coupon_discount, modified=NOW() WHERE o.id=? LIMIT 1", $order->id);
        }else{
            $query = $this->db->placehold("UPDATE __orders o SET o.total_price=IFNULL((SELECT SUM(p.price*p.amount) - o.discount FROM __purchases p WHERE p.order_id=o.id), 0)+o.delivery_price*(1-o.separate_delivery)-o.coupon_discount, modified=NOW() WHERE o.id=? LIMIT 1", $order->id);    
        }
//if($_SESSION['admin']) var_dump($query);
		$this->db->query($query);
		return $order->id;
	}
	

	public function get_next_order($id, $status = null)
	{
		$f = '';
		if($status!==null)
			$f = $this->db->placehold('AND status=?', $status);
		$this->db->query("SELECT MIN(id) as id FROM __orders WHERE id>? $f LIMIT 1", $id);
		$next_id = $this->db->result('id');
		if($next_id)
			return $this->get_order(intval($next_id));
		else
			return false; 
	}
	
	public function get_prev_order($id, $status = null)
	{
		$f = '';
		if($status !== null)
			$f = $this->db->placehold('AND status=?', $status);
		$this->db->query("SELECT MAX(id) as id FROM __orders WHERE id<? $f LIMIT 1", $id);
		$prev_id = $this->db->result('id');
		if($prev_id)
			return $this->get_order(intval($prev_id));
		else
			return false; 
	}
	
	// Возвращает комментарии, удовлетворяющие фильтру
	public function get_comments($filter = array())
	{	
		// По умолчанию
		$limit = 0;
		$object_id_filter = '';
		$sort='DESC';
		
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));
			
		if($limit)
			$sql_limit = $this->db->placehold(' LIMIT ? ', $limit);
		else
			$sql_limit = '';					

		if(!empty($filter['object_id']))
			$object_id_filter = $this->db->placehold('AND c.object_id in(?@)', (array)$filter['object_id']);
		
		$query = $this->db->placehold("SELECT c.id, c.object_id,c.name, c.text, c.date
										FROM __order_comments c WHERE 1 $object_id_filter  ORDER BY id $sort $sql_limit ");
	
		$this->db->query($query);
		return $this->db->results();
	}	
	
	// Добавление комментария
	public function add_comment($comment)
	{	
		$query = $this->db->placehold('INSERT INTO __order_comments
		SET ?%,
		date = NOW()',
		$comment);

		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();
		return $id;
	}	
	
	// Удаление комментария
	public function delete_comment($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __order_comments WHERE id=? LIMIT 1", intval($id));
			$this->db->query($query);    
		}
	}
    	
    
    /*auto_discount*/
    public function calc_auto_discount($purchase){
        
        $coupons = array();
        $selected_coupon_id = 0;
        
        if(!empty($purchase->product_id)){
            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule');
        }else{
            $variant = $this->variants->get_variant(intval($purchase->variant_id));
            $purchase->product_id = $variant->product_id;
            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule');
        }
        
//        if(!empty($purchase->product_id)){
//            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule_priceru');
//        }else{
//            $variant = $this->variants->get_variant(intval($purchase->variant_id));
//            $purchase->product_id = $variant->product_id;
//            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule_priceru');
//        }       
//       
//        //auto_discount
//        $products->categories = $this->categories->get_categories(array('product_id'=>$purchase->product_id)); 
//        $category = reset($products->categories);
//        foreach($category->path as $cp){
//            if($cp->parent_id == 0){
//                $root_category_children = $cp->children;
//            }
//        }
//        if(!empty($purchase->product_id)){
//            $product = $this->products->get_product(intval($purchase->product_id));
//        }else{
//            $variant = $this->variants->get_variant(intval($purchase->variant_id));
//            $purchase->product_id = $variant->product_id;
//            $product = $this->products->get_product(intval($purchase->product_id));
//        }
//       
//        //Получаем стразу список стоп-товаров
//        $ad_stop_products = $this->coupons->get_coupons(array('stop_related_product_id'=>$purchase->product_id, 'auto_discount'=>1, 'schedule'=>1));
//        /*foreach($products->categories as $pc){
//            $product_category_ids[] = $pc->id;
//        }*/
//        if(!$ad_stop_products){
//            if($auto_discount_products = $this->coupons->get_coupons(array('related_product_id'=>$purchase->product_id, 'auto_discount'=>1, 'schedule'=>1))){
//                //рассчитываем скидку для каждого купона, чтобы выбрать самую большую
//                foreach($auto_discount_products as $ad_prod){
//                    //если есть интервалы для цены, работаем с ними
//                    if($ad_prod->active_margin){
//                        $margins = unserialize($ad_prod->margin);
//        				if($margins['min'][0] && $margins['max'][0]){  
//                            foreach($margins['min'] as $i=>$min){
//                                if($purchase->price > intval($margins['min'][$i]) && $purchase->price < intval($margins['max'][$i])){
//                                    if(intval($margins['mode'][$i])==1){
//                                        $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
//                                    }elseif(intval($margins['mode'][$i])==2){
//                                        $coupons[$ad_prod->id] = round($purchase->price * $margins['price'][$i] / 100, 2); 
//                                    }
//                                }
//                            }
//        				}
//                    }else{//если интервалов нет
//                        if($ad_prod->type == 'percentage')
//                            $coupons[$ad_prod->id] = round($purchase->price * $ad_prod->value/100, 0);
//                        elseif($ad_prod->type == 'absolute')
//                            $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                    }
//                }
//            }elseif($product->brand_id && $auto_discount_brands = $this->coupons->get_coupons(array('brand_id'=>$product->brand_id, 'auto_discount'=>1, 'schedule'=>1))){
//                if($auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$root_category_children, 'auto_discount'=>1, 'schedule'=>1))){
//                    foreach($auto_discount_brands as $ad_prod){
//                        //если есть интервалы для цены, работаем с ними
//                        if($ad_prod->active_margin){
//                            $margins = unserialize($ad_prod->margin);
//            				if($margins['min'][0] && $margins['max'][0]){  
//                                foreach($margins['min'] as $i=>$min){
//                                    if($purchase->price > intval($margins['min'][$i]) && $purchase->price < intval($margins['max'][$i])){
//                                        if(intval($margins['mode'][$i])==1){
//                                            $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
//                                        }elseif(intval($margins['mode'][$i])==2){
//                                            $coupons[$ad_prod->id] = round($purchase->price * $margins['price'][$i] / 100, 2); 
//                                        }
//                                    }
//                                }
//            				}
//                        }else{//если интервалов нет
//                            if($ad_prod->type == 'percentage')
//                                $coupons[$ad_prod->id] = round($purchase->price * $ad_prod->value/100, 0);
//                            elseif($ad_prod->type == 'absolute')
//                                $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                        }
//                    }
//                }
//            }/*elseif(!array_sum($auto_discount_brands) && $auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$product_category_ids, 'auto_discount'=>1))){
//                foreach($auto_discount_categories as $ad_prod){
//                    if($ad_prod->type == 'percentage')
//                        $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
//                    elseif($ad_prod->type == 'absolute')
//                        $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                }
//            }*/
            //выбираем наибольшую скидку, получаем выбранный купон
        if(!empty($coupons)){
            //arsort($coupons);
            $selected_coupon_id = key($coupons);
        
            $coupon_discount_price = reset($coupons);
            
            $variant = $this->variants->get_variant(intval($purchase->variant_id));
        
            $purchase->dirty_price = $variant->price;
            $purchase->auto_coupon = $selected_coupon_id;
            $purchase->price = max(0, $variant->price - $coupon_discount_price);
            $purchase->apply_auto_coupon = 0;
        }
        //}

        return $purchase;      
    }
    
     public function calc_auto_discount_yandex($purchase){
        
        $coupons = array();
        $selected_coupon_id = 0;
        if(!empty($purchase->product_id)){
            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule_yandex');
        }else{
            $variant = $this->variants->get_variant(intval($purchase->variant_id));
            $purchase->product_id = $variant->product_id;
            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule_yandex');
        }
      
        //auto_discount
//        $products->categories = $this->categories->get_categories(array('product_id'=>$purchase->product_id)); 
//        $category = reset($products->categories);
//        foreach($category->path as $cp){
//            if($cp->parent_id == 0){
//                $root_category_children = $cp->children;
//            }
//        }
//        if(!empty($purchase->product_id)){
//            $product = $this->products->get_product(intval($purchase->product_id));
//        }else{
//            $variant = $this->variants->get_variant(intval($purchase->variant_id));
//            $purchase->product_id = $variant->product_id;
//            $product = $this->products->get_product(intval($purchase->product_id));
//        }
//       
        //Получаем стразу список стоп-товаров
//        $ad_stop_products = $this->coupons->get_coupons(array('stop_related_product_id'=>$purchase->product_id, 'auto_discount'=>1, 'schedule_yandex'=>1));
//        /*foreach($products->categories as $pc){
//            $product_category_ids[] = $pc->id;
//        }*/
//        if(!$ad_stop_products){
//            if($auto_discount_products = $this->coupons->get_coupons(array('related_product_id'=>$purchase->product_id, 'auto_discount'=>1, 'schedule_yandex'=>1))){
//                //рассчитываем скидку для каждого купона, чтобы выбрать самую большую
//                foreach($auto_discount_products as $ad_prod){
//                    //если есть интервалы для цены, работаем с ними
//                    if($ad_prod->active_margin){
//                        $margins = unserialize($ad_prod->margin);
//        				if($margins['min'][0] && $margins['max'][0]){  
//                            foreach($margins['min'] as $i=>$min){
//                                if($purchase->price > intval($margins['min'][$i]) && $purchase->price < intval($margins['max'][$i])){
//                                    if(intval($margins['mode'][$i])==1){
//                                        $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
//                                    }elseif(intval($margins['mode'][$i])==2){
//                                        $coupons[$ad_prod->id] = round($purchase->price * $margins['price'][$i] / 100, 2); 
//                                    }
//                                }
//                            }
//        				}
//                    }else{//если интервалов нет
//                        if($ad_prod->type == 'percentage')
//                            $coupons[$ad_prod->id] = round($purchase->price * $ad_prod->value/100, 0);
//                        elseif($ad_prod->type == 'absolute')
//                            $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                    }
//                }
//            }elseif($product->brand_id && $auto_discount_brands = $this->coupons->get_coupons(array('brand_id'=>$product->brand_id, 'auto_discount'=>1, 'schedule_yandex'=>1))){
//                if($auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$root_category_children, 'auto_discount'=>1, 'schedule_yandex'=>1))){
//                    foreach($auto_discount_brands as $ad_prod){
//                        //если есть интервалы для цены, работаем с ними
//                        if($ad_prod->active_margin){
//                            $margins = unserialize($ad_prod->margin);
//            				if($margins['min'][0] && $margins['max'][0]){  
//                                foreach($margins['min'] as $i=>$min){
//                                    if($purchase->price > intval($margins['min'][$i]) && $purchase->price < intval($margins['max'][$i])){
//                                        if(intval($margins['mode'][$i])==1){
//                                            $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
//                                        }elseif(intval($margins['mode'][$i])==2){
//                                            $coupons[$ad_prod->id] = round($purchase->price * $margins['price'][$i] / 100, 2); 
//                                        }
//                                    }
//                                }
//            				}
//                        }else{//если интервалов нет
//                            if($ad_prod->type == 'percentage')
//                                $coupons[$ad_prod->id] = round($purchase->price * $ad_prod->value/100, 0);
//                            elseif($ad_prod->type == 'absolute')
//                                $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                        }
//                    }
//                }
//            }/*elseif(!array_sum($auto_discount_brands) && $auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$product_category_ids, 'auto_discount'=>1))){
//                foreach($auto_discount_categories as $ad_prod){
//                    if($ad_prod->type == 'percentage')
//                        $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
//                    elseif($ad_prod->type == 'absolute')
//                        $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                }
//            }*/
            //выбираем наибольшую скидку, получаем выбранный купон
        if(!empty($coupons)){
            //arsort($coupons);
            $selected_coupon_id = key($coupons);
        
            $coupon_discount_price = reset($coupons);
            
            $variant = $this->variants->get_variant(intval($purchase->variant_id));
        
            $purchase->dirty_price = $variant->price;
            $purchase->auto_coupon = $selected_coupon_id;
            $purchase->price = max(0, $variant->price - $coupon_discount_price);
            $purchase->apply_auto_coupon = 0;
        }
        //}

        return $purchase;      
    }
    
    public function calc_auto_discount_priceru($purchase){
        
        $coupons = array();
        $selected_coupon_id = 0;
        
        if(!empty($purchase->product_id)){
            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule_priceru');
        }else{
            $variant = $this->variants->get_variant(intval($purchase->variant_id));
            $purchase->product_id = $variant->product_id;
            $coupons = $this->coupons->calc_product_coupon($purchase->product_id, $purchase->variant_id, 'schedule_priceru');
        }
//var_dump($coupons);        
        //auto_discount
//        $products->categories = $this->categories->get_categories(array('product_id'=>$purchase->product_id)); 
//        $category = reset($products->categories);
//        foreach($category->path as $cp){
//            if($cp->parent_id == 0){
//                $root_category_children = $cp->children;
//            }
//        }
//        if(!empty($purchase->product_id)){
//            $product = $this->products->get_product(intval($purchase->product_id));
//        }else{
//            $variant = $this->variants->get_variant(intval($purchase->variant_id));
//            $purchase->product_id = $variant->product_id;
//            $product = $this->products->get_product(intval($purchase->product_id));
//        }
//       
//        //Получаем стразу список стоп-товаров
//        $ad_stop_products = $this->coupons->get_coupons(array('stop_related_product_id'=>$purchase->product_id, 'auto_discount'=>1, 'schedule_priceru'=>1));
//        /*foreach($products->categories as $pc){
//            $product_category_ids[] = $pc->id;
//        }*/
//        if(!$ad_stop_products){
//            if($auto_discount_products = $this->coupons->get_coupons(array('related_product_id'=>$purchase->product_id, 'auto_discount'=>1, 'schedule_priceru'=>1))){
//                //рассчитываем скидку для каждого купона, чтобы выбрать самую большую
//                foreach($auto_discount_products as $ad_prod){
//                    //если есть интервалы для цены, работаем с ними
//                    if($ad_prod->active_margin){
//                        $margins = unserialize($ad_prod->margin);
//        				if($margins['min'][0] && $margins['max'][0]){  
//                            foreach($margins['min'] as $i=>$min){
//                                if($purchase->price > intval($margins['min'][$i]) && $purchase->price < intval($margins['max'][$i])){
//                                    if(intval($margins['mode'][$i])==1){
//                                        $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
//                                    }elseif(intval($margins['mode'][$i])==2){
//                                        $coupons[$ad_prod->id] = round($purchase->price * $margins['price'][$i] / 100, 2); 
//                                    }
//                                }
//                            }
//        				}
//                    }else{//если интервалов нет
//                        if($ad_prod->type == 'percentage')
//                            $coupons[$ad_prod->id] = round($purchase->price * $ad_prod->value/100, 0);
//                        elseif($ad_prod->type == 'absolute')
//                            $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                    }
//                }
//            }elseif($product->brand_id && $auto_discount_brands = $this->coupons->get_coupons(array('brand_id'=>$product->brand_id, 'auto_discount'=>1, 'schedule_priceru'=>1))){
//                if($auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$root_category_children, 'auto_discount'=>1, 'schedule_priceru'=>1))){
//                    foreach($auto_discount_brands as $ad_prod){
//                        //если есть интервалы для цены, работаем с ними
//                        if($ad_prod->active_margin){
//                            $margins = unserialize($ad_prod->margin);
//            				if($margins['min'][0] && $margins['max'][0]){  
//                                foreach($margins['min'] as $i=>$min){
//                                    if($purchase->price > intval($margins['min'][$i]) && $purchase->price < intval($margins['max'][$i])){
//                                        if(intval($margins['mode'][$i])==1){
//                                            $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
//                                        }elseif(intval($margins['mode'][$i])==2){
//                                            $coupons[$ad_prod->id] = round($purchase->price * $margins['price'][$i] / 100, 2); 
//                                        }
//                                    }
//                                }
//            				}
//                        }else{//если интервалов нет
//                            if($ad_prod->type == 'percentage')
//                                $coupons[$ad_prod->id] = round($purchase->price * $ad_prod->value/100, 0);
//                            elseif($ad_prod->type == 'absolute')
//                                $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                        }
//                    }
//                }
//            }/*elseif(!array_sum($auto_discount_brands) && $auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$product_category_ids, 'auto_discount'=>1))){
//                foreach($auto_discount_categories as $ad_prod){
//                    if($ad_prod->type == 'percentage')
//                        $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
//                    elseif($ad_prod->type == 'absolute')
//                        $coupons[$ad_prod->id] = floatval($ad_prod->value);
//                }
//            }*/
        //выбираем наибольшую скидку, получаем выбранный купон
        if(!empty($coupons)){
            //arsort($coupons);
            $selected_coupon_id = key($coupons);
        
            $coupon_discount_price = reset($coupons);
            
            $variant = $this->variants->get_variant(intval($purchase->variant_id));
        
            $purchase->dirty_price = $variant->price;
            $purchase->auto_coupon = $selected_coupon_id;
            $purchase->price = max(0, $variant->price - $coupon_discount_price);
            $purchase->apply_auto_coupon = 0;
        }
        //}

        return $purchase;      
    }
	/*/auto_discount/*/
	
}
