<?php

/**
 * Simple CORE CMS
 *
 * @copyright	2015 Sheeft
 * @link		http://simplecore.ru
 * @author		Ismatulaev G.
 *
 */

require_once('Simpla.php');

class Exports extends Simpla
{
 
	// ������� ���������� ������ ���������
	public function get_exports($filter = array())
	{
		 
		$exports = array();
		$active_filter = "";
		if(isset($filter['active']))
		$active_filter = "AND active = 1";
		$query = $this->db->placehold("SELECT * FROM __exports WHERE 1 $active_filter");		 
		$this->db->query($query);
		return $this->db->results();
	}	
 
	// ������� ���������� �������� ���������
	public function get_export($id)
	{
		$query = "SELECT * FROM __exports WHERE id = $id";
		$this->db->query($query);
		return $this->db->result();
	}
	
	// ���������� ���������
	public function add_export($export)
	{
		$export = (array)$export;
		$this->db->query("INSERT INTO __exports SET ?%", $export);
		return $this->db->insert_id();
		
	}
	
	// ��������� ���������
	public function update_export($id, $export)
	{
		$query = $this->db->placehold("UPDATE __exports SET ?% WHERE id=? LIMIT 1", $export, intval($id));
		$this->db->query($query);
		return $id;
	}
	
	// �������� ���������
	public function delete_export($id)
	{
 
			if(!empty($id))
			{
				$query = $this->db->placehold("DELETE FROM __export WHERE id=? LIMIT 1", $id);
				$this->db->query($query);		
			}
  
		return true;
	}

///        
	   
}