<?php
//ini_set('display_errors',1);
/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */
 
require_once('Simpla.php');

class Cart extends Simpla
{

	/*
	*
	* Функция возвращает корзину
	*
	*/
	public function get_cart()
	{
		$cart = new stdClass();
		$cart->purchases = array();
		$cart->total_price = 0;
		$cart->total_products = 0;
		$cart->coupon = null;
		$cart->discount = 0;
		$cart->coupon_discount = 0;
 
		// Берем из сессии список variant_id=>amount
		if(!empty($_SESSION['shopping_cart']))
		{
			$session_items = $_SESSION['shopping_cart'];
            $auto_coupons = $_SESSION['auto_coupon'];
		
			$variants = $this->variants->get_variants(array('id'=>array_keys($session_items)));
			if(!empty($variants))
			{
				foreach($variants as &$variant)
				{
					$items[$variant->id] = new stdClass();
                    
                    if(in_array($variant->id, array_keys($auto_coupons))){
                        $coupon = $this->coupons->get_coupon(intval($auto_coupons[$variant->id]));
                        //если есть интервалы для цены, работаем с ними
                        if($coupon->active_margin){
                            $margins = unserialize($coupon->margin);
            				if($margins['min'][0] && $margins['max'][0]){  
                                foreach($margins['min'] as $i=>$min){
                                    if($variant->price > intval($margins['min'][$i]) && $variant->price < intval($margins['max'][$i]))
                                    {
                                        if(intval($margins['mode'][$i])==1){
                                            $variant->auto_coupon_price = max(0, $variant->price - $margins['price'][$i]);
                                            $variant->coupon_discount_price = $margins['price'][$i];
                                        }elseif(intval($margins['mode'][$i])==2){
                                            $variant->auto_coupon_price = round($variant->price * (100 - $margins['price'][$i]) / 100, 0); 
                                            $variant->coupon_discount_price = round($variant->price * $margins['price'][$i]/100, 0);
                                        }
                                    }
                                }
            				}
                        }else{//если интервалов нет
                            if($coupon->type=='percentage'){
                                $variant->auto_coupon_price = round($variant->price * (100 - $coupon->value)/100, 0);
                                $variant->coupon_discount_price = round($variant->price * $coupon->value/100, 0);
                            }elseif($coupon->type=='absolute'){
                                $variant->auto_coupon_price =  max(0, $variant->price-$coupon->value);
                                $variant->coupon_discount_price = $coupon->value;
                            }
                        }
                        $variant->auto_coupon_id = $coupon->id;
                    }
					$items[$variant->id]->variant = $variant;
					$items[$variant->id]->amount = $session_items[$variant->id];
					$products_ids[] = $variant->product_id;
                    //var_dump($variant);
				}
	
				$products = array();
				foreach($this->products->get_products(array('id'=>$products_ids)) as $p){
                    $brand = $this->brands->get_brand(intval($p->brand_id));
    				$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
                    $products[$p->id] = $p;
				}
				$images = $this->products->get_images(array('product_id'=>$products_ids));
				foreach($images as $image)
					$products[$image->product_id]->images[$image->id] = $image;
			
				foreach($items as $variant_id=>$item)
				{	
					$purchase = null;
					if(!empty($products[$item->variant->product_id]))
					{
						$purchase = new stdClass();
						$purchase->product = $products[$item->variant->product_id];					
						$purchase->variant = $item->variant;
                        $purchase->amount = $item->amount;
                        if(!empty($item->variant->image_id) && isset($images[$item->variant->image_id]))  $purchase->image = $images[$item->variant->image_id];
                        elseif(isset($product->images[0]))  $purchase->image = $products[$item->variant->product_id]->images[0];

                        $cart->purchases[] = $purchase;
                        if($purchase->variant->auto_coupon_price){
                            $cart->total_price += $item->variant->auto_coupon_price*$item->amount;
                            
                            $auto_discount_price += $coupon_discount_price;
                        }else
                            $cart->total_price += $item->variant->price*$item->amount;
						$cart->total_products += $item->amount;
                        
                        //Подсчитаем общую сумму без учета автокупона, чтобы правильно вычислить групповую скидку
                        $cart->dirty_total_price += $item->variant->price*$item->amount;
                        
					}
				}
				// Пользовательская скидка
				$cart->discount = 0;

            if($this->settings->sum_1 && $this->settings->proc_1 && $cart->total_price > $this->settings->sum_1)
              $cart->discount = $this->settings->proc_1;
            if($this->settings->sum_2 && $this->settings->proc_2 && $cart->total_price > $this->settings->sum_2)
              $cart->discount = $this->settings->proc_2;
            if($this->settings->sum_3 && $this->settings->proc_3 && $cart->total_price > $this->settings->sum_3)
              $cart->discount = $this->settings->proc_3;
            $user = $this->users->get_user(intval($_SESSION['user_id']));  
            //if($_SESSION['admin']){print('<pre>');print_r($_SESSION);print('</pre>');}
            if(isset($_SESSION['user_id']) && $user && !$user->auto)
                if($user->discount > $cart->discount)
                    $cart->discount = $user->discount;

                if($cart->dirty_total_price){
                    $cart->total_price_t = $cart->dirty_total_price * (100-$cart->discount)/100;

                    $cart->total_price_delta = $cart->dirty_total_price - $cart->total_price;
                    $cart->total_price = $cart->total_price_t - $cart->total_price_delta;
                }else
				    $cart->total_price *= (100-$cart->discount)/100;
				
				// Скидка по купону
				if(isset($_SESSION['coupon_code']))
				{
					$cart->coupon = $this->coupons->get_coupon($_SESSION['coupon_code']);
					if($cart->coupon && $cart->coupon->valid && $cart->total_price>=$cart->coupon->min_order_price)
					{
                        
                        //if($_SESSION['admin']){print('<pre>');print_r($coupon_categories);print_r($coupon_brands);print_r($rel_prod_stop_ids);print_r($related_products);print('</pre>');} 
                        $coupons = array();
                        foreach ($cart->purchases as $purchase){
                            $coupons[] = $this->coupons->calc_coupon($purchase->product->id, $purchase->variant->id, $purchase->amount);
                        }
                        $sum_coupon_discount = array();
                        $selected = array();
                        if(count($coupons)){
                            foreach($coupons as $coupon){
                                if(is_array($coupon)){
                                    foreach($coupon as $cid=>$discount){
                                        $selected[$cid] = $discount;
                                        if($cid==$cart->coupon->id){
                                            $sum_coupon_discount[$cid] += $discount;
                                        }
                                    }
                                }
                            }
                            //if($_SESSION['admin']){var_dump($selected);}
                        
                            if(in_array($cart->coupon->id, array_keys($selected))){ 
                                unset($_SESSION['bad_coupon']);
                                $cart->coupon_discount = $sum_coupon_discount[$cart->coupon->id];
       							$cart->total_price = $cart->total_price-$cart->coupon_discount;
                            }else{
                                $_SESSION['bad_coupon']=true;
                                unset($_SESSION['coupon_code']);
                            }
                        }    
						//if($_SESSION['admin']){print('<pre>');print_r($_SESSION);print('</pre>');}
					}
					else
					{
						unset($_SESSION['coupon_code']);
					}
				}
				
			}
		}
			
		return $cart;
	}
	
	/*
	*
	* Добавление варианта товара в корзину
	*
	*/
	public function add_item($variant_id, $amount = 1)
	{ 
		$amount = max(1, $amount);
	
		if(isset($_SESSION['shopping_cart'][$variant_id]))
      		$amount = max(1, $amount+$_SESSION['shopping_cart'][$variant_id]);

		// Выберем товар из базы, заодно убедившись в его существовании
		$variant = $this->variants->get_variant($variant_id);

		// Если товар существует, добавим его в корзину
		//if(!empty($variant) && ($variant->stock>0) )
		if(!empty($variant))
		{
			// Не дадим больше чем на складе
			$amount = min($amount, $variant->stock);
			// если нет, то под заказ
			if($variant->stock == 0){
				$amount = 1;
			}
	     
			$_SESSION['shopping_cart'][$variant_id] = intval($amount); 
		}
	}
	
	/*
	*
	* Обновление количества товара
	*
	*/
	public function update_item($variant_id, $amount = 1)
	{
		$amount = max(1, $amount);
		
		// Выберем товар из базы, заодно убедившись в его существовании
		$variant = $this->variants->get_variant($variant_id);

		// Если товар существует, добавим его в корзину
		//if(!empty($variant) && $variant->stock>0)
		if(!empty($variant))
		{
			// Не дадим больше чем на складе
			$amount = min($amount, $variant->stock);
			// если нет, то под заказ
			if($variant->stock == 0){
				$amount = max(1, $amount+$_SESSION['shopping_cart'][$variant_id]);
			}
	     
			$_SESSION['shopping_cart'][$variant_id] = intval($amount); 
		}
 
	}
	
	
	/*
	*
	* Удаление товара из корзины
	*
	*/
	public function delete_item($variant_id)
	{
		unset($_SESSION['shopping_cart'][$variant_id]);
        unset($_SESSION['auto_coupon'][$variant_id]);  
	}
	
	/*
	*
	* Очистка корзины
	*
	*/
	public function empty_cart()
	{
		unset($_SESSION['shopping_cart']);
		unset($_SESSION['coupon_code']);
        unset($_SESSION['auto_coupon']);
        unset($_SESSION['bad_coupon']);
	}
 
	/*
	*
	* Применить купон
	*
	*/
	public function apply_coupon($coupon_code)
	{
		$coupon = $this->coupons->get_coupon((string)$coupon_code);
		if($coupon && $coupon->valid)
		{
			$_SESSION['coupon_code'] = $coupon->code;
		}
		else
		{
			unset($_SESSION['coupon_code']);
		}		
	} 
}