<?php
/**
 * Simple CORE CMS
 *
 * @copyright	2015 Sheeft
 * @link		http://simplecore.ru
 * @author		Ismatulaev G.
 *
 */
//ini_set('display_errors', true);
require_once('Simpla.php');

class Parsers extends Simpla
{
	//	
	// Список указателей на категории в дереве категорий (ключ = id категории)
	private $all_Parsers;
	// Дерево категорий
	private $Parsers_tree;
	
///////////////////////////////
//////// Стратегия цен ////////
///////////////////////////////

	public function get_strategs($filter = array())
	{		 
		$active_filter = "";
		if(isset($filter['active']))
		$active_filter = "AND active = 1";

		// Выбираем все
		$query = $this->db->placehold("SELECT * FROM __strat WHERE 1 $active_filter ORDER BY position");
		$this->db->query($query);
		return $this->db->results();
	}	
	
		// Функция возвращает заданную категорию
	public function get_strat($id)
	{
		$query = "SELECT * FROM __strat WHERE id = $id";
		$this->db->query($query);
		return $this->db->result();
	}
	
	
		// Добавление категории
	public function add_strat($strat)
	{
		$strat = (array)$strat;
		$this->db->query("INSERT INTO __strat SET ?%", $strat);
		$id = $this->db->insert_id();
		$this->db->query("UPDATE __strat SET position=id WHERE id=?", $id);	
		return $id;		
	}
	
	// Изменение категории
	public function update_strat($id, $strat)
	{
		$query = $this->db->placehold("UPDATE __strat SET ?% WHERE id=? LIMIT 1", $strat, intval($id));
		$this->db->query($query);
		return $id;
	}
	
	// Удаление категории
	public function delete_strat($id)
	{
 
			if(!empty($id))
			{
				$query = $this->db->placehold("DELETE FROM __strat WHERE id=? LIMIT 1", $id);
				$this->db->query($query);		
			}
  
		return true;
	}

		function get_strat_related_products($strat_id = array())
	{
		if(empty($strat_id))
			return array();

		$product_id_filter = $this->db->placehold('AND strat_id in(?@)', (array)$strat_id);
				
		$query = $this->db->placehold("SELECT strat_id, related_id, position
					FROM __strat_related_products
					WHERE 
					1
					$product_id_filter   
					ORDER BY position       
					");
		
		$this->db->query($query);
		$res = $this->db->results();
		
		if (!$res AND is_int((int)$strat_id) AND (int)$strat_id > 0 AND 1 == 2) 
		{
 
				$tname = $temprod->name;
				$tbrand_id = $temprod->brand_id;
				$query = $this->db->placehold("SELECT ? as strat_id, p.id as related_id, 1 as position FROM __products p LEFT JOIN __variants v ON v.strat_id = p.id WHERE p.visible = 1 AND (v.stock > 0 OR v.stock is NULL)  AND p.id NOT IN (?) ORDER BY p.name LIMIT 4;",   (int)$strat_id, (int)$product_id);
				$this->db->query($query);
		    	$res = $this->db->results();
 
		}
		return $res;
	}
	
	// Р¤СѓРЅРєС†РёСЏ РІРѕР·РІСЂР°С‰Р°РµС‚ СЃРІСЏР·Р°РЅРЅС‹Рµ С‚РѕРІР°СЂС‹
	public function add_strat_related_product($strat_id, $related_id, $position=0)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __strat_related_products SET strat_id=?, related_id=?, position=?", $strat_id, $related_id, $position);
		$this->db->query($query);
		return $related_id;
	}
	
	// РЈРґР°Р»РµРЅРёРµ СЃРІСЏР·Р°РЅРЅРѕРіРѕ С‚РѕРІР°СЂР°
	public function delete_strat_related_product($strat_id, $related_id)
	{
		$query = $this->db->placehold("DELETE FROM __strat_related_products WHERE strat_id=? AND related_id=? LIMIT 1", intval($strat_id), intval($related_id));
		$this->db->query($query);
	}
	
	
		// Изменение категории
	public function pro_strat($product)
	{
		$pid = $product->product_id;
		if ($pid == 0) return false;
		
		if ($product->brand_id > 0)
		$brand_fil = "OR `brand` = $product->brand_id";
	 
		if ($product->vendor > 0)
		$ven_fil = "OR `ven` = $product->vendor";
	
		$pid_fil = "`id` IN (SELECT `strat_id` FROM __strat_related_products WHERE `related_id` = $pid)";
		$query = $this->db->placehold("SELECT * FROM __strat WHERE `active` = 1 AND ( $pid_fil $ven_fil $brand_fil ) LIMIT 1");
		$this->db->query($query);
		
		 if ($_GET["debug"] == 2) print $query;
		$d = getdate();
		$tday = $d["wday"];
		if ($tday == 0) $tday = 7;
		$th	  = $d["hours"]; 		
		$tm	  = $d["minutes"];
		$tr  = $this->db->result();
		//print_r($tday);
		$sh  = (int)$tr->starth;
		$sm  = (int)$tr->startm;
		$eh  = (int)$tr->endh;
		$em  = (int)$tr->endm;
 	
	if (!isset($sm))
		$sm = 00;
	if (!isset($em))
		$em = 00;
		
		// Начало акции //
		$next = 0;
		
		if ($tday == 1 AND $tr->day1 == "on") $next = 1;
		if ($tday == 2 AND $tr->day2 == "on") $next = 1;
		if ($tday == 3 AND $tr->day3 == "on") $next = 1;
		if ($tday == 4 AND $tr->day4 == "on") $next = 1;
		if ($tday == 5 AND $tr->day5 == "on") $next = 1;
		if ($tday == 6 AND $tr->day6 == "on") $next = 1;
		if ($tday == 0 AND $tr->day7 == "on") $next = 1;
	 
		if (($eh >= $sh) and ($th < $sh)) $next = 0;
		if (($eh >= $sh) and ($th > $eh)) $next = 0;
		
		if (($eh >= $sh) and ($th == $sh) and ($tm <= $sm)) $next = 0;
		if (($eh >= $sh) and ($th == $eh) and ($tm >= $em)) $next = 0;
		
		if (($eh < $sh) and ($th < $sh)) $next = 0;
	 	if (($eh < $sh) and ($th > $eh) and ($th < $sh)) $next = 0;
		
	
	
	if ($_GET["debug"] == 1) print "th $th sh $sh sm $sm eh $eh em $em next $next // $tr->day1 $tr->day2 $tr->day3 $tr->day4 $tr->day5 $tr->day6 $tr->day7"; 
	
		if ($next == "1")
		{
			$res["a"] = (int)$tr->delta;
			$res["p"] = (int)$tr->deltap;
			
			if ((isset($tr->minprice)) and ($tr->minprice > 0))
				$res["minprice"] = (int)$tr->minprice;
			if ((isset($tr->maxprice)) and ($tr->maxprice > 0))
				$res["maxprice"] = (int)$tr->maxprice;
		}
		
		return $res;
	}
	
///////////////////////////////
///////////////////////////////
///////////////////////////////
///////////////////////////////

	
	// Функция возвращает массив категорий
	public function get_parsers($filter = array())
	{
		 
		$parsers = array();
		$active_filter = "";
		if(isset($filter['active']))
		$active_filter = "AND active = 1";

		// Выбираем все вендоры
		$query = $this->db->placehold("SELECT * FROM __parsers WHERE 1 $active_filter ORDER BY name");
		//$query = $this->db->placehold("ALTER TABLE  `s_parsers` ADD  `notinstockname` varchar( 255 ) NOT NULL");
		$this->db->query($query);
		return $this->db->results();
	}	
 
	// Функция возвращает заданную категорию
	public function get_parser($id)
	{
		$query = "SELECT * FROM __parsers WHERE id = $id";
		$this->db->query($query);
		return $this->db->result();
	}
	
	// Добавление категории
	public function add_parser($parser)
	{
		$parser = (array)$parser;
		$this->db->query("INSERT INTO __parsers SET ?%", $parser);
		return $this->db->insert_id();
		
	}
	
	// Изменение категории
	public function update_parser($id, $parser)
	{
		$query = $this->db->placehold("UPDATE __parsers SET ?% WHERE id=? LIMIT 1", $parser, intval($id));
		$this->db->query($query);
		return $id;
	}
	
	// Удаление категории
	public function delete_parser($id)
	{
 
			if(!empty($id))
			{
				$query = $this->db->placehold("DELETE FROM __parsers WHERE id=? LIMIT 1", $id);
				$this->db->query($query);		
			}
  
		return true;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function ReadExcel($filepath){
	$filepath = "simpla/files/prices/".$filepath;
		require_once "PHPExcel.php"; //подключаем наш фреймворк
		$ar=array(); // инициализируем массив
		$inputFileType = PHPExcel_IOFactory::identify($filepath);  // узнаем тип файла, excel может хранить файлы в разных форматах, xls, xlsx и другие
		$objReader = PHPExcel_IOFactory::createReader($inputFileType); // создаем объект для чтения файла
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($filepath); // загружаем данные файла в объект
		$lc = $objPHPExcel->getSheetCount();
		for ($x=0; $x<$lc; $x++)
		{
			 $objPHPExcel->setActiveSheetIndex($x);
			 $tar = $objPHPExcel->getActiveSheet()->toArray(NULL,true); // выгружаем данные из объекта в массив
			foreach($tar as $ta)
				$ar[] = $ta;
		}
	
		return $ar; //возвращаем массив
	}

	public function get_files()
	{
	
		$ndirct = "simpla/files/prices/"; 
		$nhdl=opendir($ndirct); 
		while ($nfile = readdir($nhdl)) 
		{
		
		//$na[] = $nfile; 
	if (is_file($ndirct.$nfile)) 
			{ 
					$na[] = $nfile; 
			} 
	} 
	closedir($nhdl); 
	if (sizeof($na)!=0) 
	{ 
	rsort($na); 
	$vsego=sizeof($na); 
	} 
	 
	return $na;
	}
	
	public function upd_date($id)
	{
 
			if(!empty($id))
			{
				$data = date("j F Y, H:i:s");				 
				$query = $this->db->placehold("UPDATE __parsers SET lastrun=? WHERE id=? LIMIT 1", $data, $id);
				$this->db->query($query);		
			}
  
		return true;
	}
	
	public function go_parse($id = NULL)
	{
	setlocale(LC_TIME, "ru_RU");
	if (empty($id))
		$parsers = $this->get_parsers(array("active"=>"1"));
		else
		$parsers[] = $this->get_parser($id);
		
	//
    
		foreach($parsers as $parser)
		{
		  
            if($parser->restock){
    			$query = $this->db->placehold("UPDATE  `__variants` v INNER JOIN `__products` p ON v.product_id = p.id SET  `stock` = 0  WHERE p.vendor = ?", $parser->id);
                @$this->db->query($query);
                //print($query);
    
    			$query = $this->db->placehold("UPDATE  `__products` p SET p.`sklad_id` = 0 , p.`byrequest` = ''  WHERE  p.vendor = ? ", $parser->id);
                @$this->db->query($query);
                //print($query);
            }
							
			$this->parsers->upd_date($id);
            
            ///////////////////////////////////||||||EXCEL PARSER||||||||\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            
            if($parser->parser_type == 'excel'){
    			$pricel = $this->readexcel($parser->filename);
      	
                $variants_not_exists = array();
    			foreach($pricel as $line)
    			{
    				  
    				$art =  trim($line[$parser->art-1]);
    				//$art = mb_strtolower($art);
    				//print($art);
    			
    				$price =  str_replace(" ","", $line[$parser->prices-1]);				
    				$price =  str_replace(",","", $price);
    				$price =  str_replace("$","", $price);
    				$price =  str_replace("","", $price);				
    				$price = (float)$price;
                  //var_dump($price);  
                    if ($parser->val) { $price = $price * (float)$parser->val; }
                    //var_dump($price);
                    if($parser->active_margin){
                        $margins = unserialize($parser->margin);
                        //var_dump($margins);
        				if($margins['min'][0] && $margins['max'][0]){  
        				    //print_r($margins);
                            foreach($margins['min'] as $i=>$min){
                                if($price > intval($margins['min'][$i]) && $price < intval($margins['max'][$i])){
                                    if(intval($margins['mode'][$i])==1){
                                        $price = $price + $margins['price'][$i];
                                    }elseif(intval($margins['mode'][$i])==2){
                                        $price = $price + ($price * $margins['price'][$i] / 100); 
                                    }
                                }
                            }
        				}
                    }
    				 
    				$stockprice =  str_replace(" ","", $line[$parser->stockprice-1]);				
    				$stockprice =  str_replace(",","", $stockprice);
    				$stockprice =  str_replace("$","", $stockprice);
    				$stockprice =  str_replace("","", $stockprice);				
    				$stockprice = (float)$stockprice;
    				//var_dump($stockprice);
    				
                    
                    if($parser->active_stockmargin){
                        $stockmargins = unserialize($parser->stockmargin);
                        //var_dump($stockmargins);
        				if($stockmargins['min'][0] && $stockmargins['max'][0]){  
        				    //print_r($stockmargins);
                            foreach($stockmargins['min'] as $i=>$smin){
                                if($stockprice > intval($stockmargins['min'][$i]) && $stockprice < intval($stockmargins['max'][$i])){
                                    if(intval($stockmargins['mode'][$i])==1){
                                        $price = $stockprice + $stockmargins['price'][$i];
                                    }elseif(intval($stockmargins['mode'][$i])==2){
                                        $price = $stockprice + ($stockprice * $stockmargins['price'][$i] / 100); 
                                    }
                                }
                            }
        				}
                    }
                    
    				$stock = trim($line[$parser->instock-1]);
    				
    				$oji = trim($line[$parser->byrequest - 1]);
    				 //if (!empty($oji)) die($oji);
    				
    				$stock_arr = (array)explode(";", $parser->instockname);
    				$not_in_stock_arr = (array)explode(";", $parser->notinstockname);
    				
    					if ( in_array($stock, $stock_arr)) {$instock = "NULL";} else {$instock = 0;}
    					if ( in_array($stock, $not_in_stock_arr)) {$instock = "0";}
    					
    					if (!is_int($stock))
    					$stock = preg_replace("|[^\d]+|", "", $stock);
    					
    					if ( (is_int((int)$stock)) AND ((int)$stock > 0)) {$instock = (int)$stock;}
    				 //print($art. " цена: ". $price. " наличие: ". $stock ."<br>");
    				if ($parser->reprice) 
    					$pfr = $price;
    				else $pfr = 1;
 				   
    				if (!empty($art) AND $pfr > 0)
    				{
                        $query = $this->db->placehold('SELECT id as variant_id, name FROM __variants WHERE sku=?', $art);
                        
                        $this->db->query($query);
                        $exists_variants = $this->db->results();
    
                        if(!count($exists_variants)){
                            $variants_not_exists[] = $art;
                            echo "<pre style='background: green; font-weight: 500'>Новой товар c артикулом ".$art."</pre>";
                            //print_r($variants_not_exists);
                        }
                        
    					if ($parser->reprice) $reprice = "`price` =  $price"; else $reprice = '';
                        if ($parser->restock) $restock = " `stock` =  $instock"; else $restock = '';
    					if ($parser->stockprice) $resprice = " `stockprice` = $stockprice"; else $resprice = '';
    					$data = date("j F Y H:i:s", time());	
                        $last_parse = ', `last_parse`="'.$data.'"';
                        
    					if(!empty($reprice) AND !empty($resprice) AND !empty($restock)){
                            $restock = ", ". $restock.", ";
    					}elseif(!empty($reprice) AND !empty($restock)){
                            $restock = ", ".$restock;	
                        }elseif(!empty($reprice) AND !empty($resprice)){
                            $resprice = ", ".$resprice;  
                        }elseif(!empty($restock) AND !empty($resprice))
                            $resprice = ", ".$resprice;       
    					
    					if (($restock) OR !empty($reprice)) {
                            
                            if($parser->id == 22 || $parser->id == 64 || $parser->id == 65){
                                
                                $xls_vendor = trim($line[$parser->xls_vendor-1]);
                                //var_dump($xls_vendor);
                                if($xls_vendor){
                                    if($parser->id==22) $store = ', `store` = 1';
                                    elseif($parser->id==64) $store = ', `store` = 2';
                                    elseif($parser->id==65) $store = ', `store` = 3';
                                    else $store = '';
                                    // если Velocube_sklad, то ставим 1, если Magazin то 2
                                    //var_dump($store); var_dump($parser->id);
                                    $queryx = $this->db->placehold("UPDATE  `__variants` v INNER JOIN `__products` p ON v.product_id = p.id  SET  $reprice $restock $resprice  $store $last_parse WHERE `sku` = ? AND p.vendor = ?", $art, $xls_vendor);
        							$this->db->query($queryx);
        							//$result = $this->db->result;
                                    //print($queryx);
                                    if (!empty($oji))
        							{	
        								$queryx = $this->db->placehold("UPDATE  `__products` SET `sklad_id` = 3, `byrequest` = ? $last_parse WHERE `id` IN (SELECT product_id FROM __variants WHERE sku = ? AND vendor = ?)", $oji, $art, $xls_vendor);
        								$this->db->query($queryx);
        								//print "<b>$queryx</b><br>";
                                    }
    							}else{
    							     print("Не указан поставщик, обновление товара с арт. $art не произошло");
                                     continue; 
    							}
                            }else{
    							$queryx = $this->db->placehold("UPDATE  `__variants` v INNER JOIN `__products` p ON v.product_id = p.id  SET  $reprice $restock $resprice $last_parse , v.store=NULL WHERE `sku` = ? AND p.vendor = ?", $art, $parser->id);
    							$this->db->query($queryx);
    							//$result = $this->db->result;
                                //print($queryx);
                                
    							if (!empty($oji))
    							{	
    								$queryx = $this->db->placehold("UPDATE  `__products` SET `sklad_id` = 3, `byrequest` = ? $last_parse WHERE `id` IN (SELECT product_id FROM __variants WHERE sku = ? AND vendor = ?)", $oji, $art, $parser->id);
    								$this->db->query($queryx);
    								//print "<b>$queryx</b><br>";
    							}
                            }
    						
//print_r($result);
    						
    						if ($parser->reprice){
                                $np = " . Новая цена: " .$price . " Опт: ".$stockprice;
                                $variants = $this->variants->get_variants(array('sku'=>$art));
                                $product_id = 0;
                                foreach($variants as $v){
                                    $product_id = $v->product_id;
                                }
                                if($product_id){
                                    $catogories = array();                            
                                    $categories = $this->categories->get_categories(array('product_id'=>$product_id));
                                    //var_dump($product_id);
                                    foreach($categories as $cat){
                                        if(!empty($cat->lowcost_limit) && !empty($cat->lowcost_category)){
                                            if($price <= $cat->lowcost_limit){
                                                $this->categories->add_product_category($product_id, $cat->lowcost_category,100);
                                                $np .= '. Добавили дешевую катерогию с id '. $cat->lowcost_category;
												//var_dump($cat->name);
                                            }
                                        }
                                        if(!empty($cat->highcost_limit) && !empty($cat->highcost_category)){
                                            if($price >= $cat->highcost_limit){
                                                $this->categories->add_product_category($product_id, $cat->highcost_category,100);
                                                $np .= '. Добавили дорогую катерогию с id '. $cat->highcost_category;
                                            }
                                        }
                                        
                                    }
                                } 
                            }else 
                                $np = '';
    						if ($parser->restock) $ns = " Наличие: ".$instock; else $ns = '';
    						if (!empty($oji)) $brd = " Поставка: ".$oji; else $brd = '';
    						//if ($result) 
                            print("Обновили товар с артикулом: "  .$art . $np . $ns . $brd . "  <br>");
    					}
    				}
    				flush();
    				//ob_flush();
    			}
            }
            
            ///////////////////////////XML PARSER//////////////////////////////////
            if($parser->parser_type == 'xml'){
                if($parser->xmlurl){
                    $string = file_get_contents($parser->xmlurl);
                    //var_dump($string);
                    $xml = simplexml_load_string($string);
                    
                    $products = $xml->xpath("//".$parser->xmlproduct);
                    
                    //$parser->xmlcurrency = ($parser->xmlcurrency_attr) ? $parser->xmlcurrency.'[@'.$parser->xmlcurrency_attr.']' :  $parser->xmlcurrency;
                    $obj_currency = $xml->xpath("//".$parser->xmlcurrency);
                    if($obj_currency)
                        $currency = $obj_currency[0]->attributes()[$parser->xmlcurrency_attr];
                    
                    foreach($products as $product){
                        if($parser->xmlart == 'xpath'){
                            $art = trim(strval($product->xpath($parser->xmlart_attr)[0]));
                        }elseif($parser->xmlart_attr){
                            //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                            if($product->{$parser->xmlart}->attributes)
                                $art = trim(strval($product->{$parser->xmlart}->attributes()[$parser->xmlart_attr]));
                            else
                                $art = trim(strval($product->attributes()[$parser->xmlart_attr]));
                        }else{
                            $art = trim(strval($product->{$parser->xmlart}));
                        }

                        if($parser->xmlstockprice == 'xpath'){
                            $stockprice = strval($product->xpath($parser->xmlstockprice_attr)[0]);
                        }elseif($parser->xmlstockprice_attr){
                            if($product->{$parser->xmlstockprice}->attributes)//проверим, не является ли этот самим объектом продукта
                                $stockprice = $product->{$parser->xmlstockprice}->attributes()[$parser->xmlstockprice_attr];
                            else
                                $stockprice = $product->attributes()[$parser->xmlstockprice_attr];
                        }else{
                            $stockprice = $product->{$parser->xmlstockprice};
                        }
                        $stockprice =  str_replace(",","", $stockprice);
        				$stockprice =  str_replace("$","", $stockprice);
        				$stockprice =  str_replace("","", $stockprice);	
                        $stockprice = floatval(preg_replace("/[^\d,.]/","",$stockprice));			
        				$stockprice = (float)trim($stockprice);
                        if ($currency) { $stockprice = $stockprice * (float)$currency; }
                        
                        //print_r($stockprice);
                        if($parser->xmlprice == 'xpath'){
                            $price = strval($product->xpath($parser->xmlprice_attr)[0]);
                        }elseif($parser->xmlprice_attr){
                            //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                            if($product->{$parser->xmlprice}->attributes)
                                $price = $product->{$parser->xmlprice}->attributes()[$parser->xmlprice_attr];
                            else
                                $price = $product->attributes()[$parser->xmlprice_attr];
                        }else{
                            $price = $product->{$parser->xmlprice};
                        }
                        //var_dump($price);
                        $price = (string)$price;
                        
                        $price =  str_replace(" ","", $price);
                        $price =  str_replace("&nbsp;","", $price);
                        $price =  str_replace(",",".", $price);
        				$price =  str_replace("$","", $price);
        				$price =  str_replace("","", $price);
                        /*if(!preg_match("[\.]", $price)){
                            $price =  preg_replace ("[\D]","",$price);
                        }else*/
                        $price = floatval(preg_replace("/[^\d,.]/","",$price));
                        //var_dump($price);				
        				$price = floatval($price);
                        //var_dump($price);
                        if ($currency) { $price = $price * (float)$currency; }
                        
                        if($parser->active_margin){
                            $margins = unserialize($parser->margin);
                            //var_dump($margins);
            				if($margins['min'][0] && $margins['max'][0]){  
            				    //print_r($margins);
                                foreach($margins['min'] as $i=>$min){
                                    if($price > intval($margins['min'][$i]) && $price < intval($margins['max'][$i])){
                                        if(intval($margins['mode'][$i])==1){
                                            $price = $price + $margins['price'][$i];
                                        }elseif(intval($margins['mode'][$i])==2){
                                            $price = $price + ($price * $margins['price'][$i] / 100); 
                                        }
                                    }
                                }
            				}
                        }
                        
                        if($parser->active_stockmargin){
                            $stockmargins = unserialize($parser->stockmargin);
                            //var_dump($stockmargins);
            				if($stockmargins['min'][0] && $stockmargins['max'][0]){  
            				    //print_r($stockmargins);
                                foreach($stockmargins['min'] as $i=>$smin){
                                    if($stockprice > intval($stockmargins['min'][$i]) && $stockprice < intval($stockmargins['max'][$i])){
                                        if(intval($stockmargins['mode'][$i])==1){
                                            $price = $stockprice + $stockmargins['price'][$i];
                                        }elseif(intval($stockmargins['mode'][$i])==2){
                                            $price = $stockprice + ($stockprice * $stockmargins['price'][$i] / 100); 
                                        }
                                    }
                                }
            				}
                        }

                        if($parser->xmlstock == 'xpath'){
                            $stock = strval($product->xpath($parser->xmlstock_attr)[0]);
                        }elseif($parser->xmlstock_attr){
                            if($product->{$parser->xmlstock}->attributes)
                                $stock = $product->{$parser->xmlstock}->attributes()[$parser->xmlstock_attr];
                            else
                                $stock = $product->attributes()[$parser->xmlstock_attr];
                        }else{
                            $stock = $product->{$parser->xmlstock};
                        }                        
                                              
                        $stock_arr = (array)explode(";", $parser->xmlvnalname);
                        
    				    $not_in_stock_arr = (array)explode(";", $parser->xmlnenalname);
                        
                        if($stock_arr[0]){    
                            if(in_array($stock, $stock_arr)) {$instock = "NULL";} else {$instock = 0;}
                        }else{
                            $stock = preg_replace("|[^\d-]+|", "", $stock);
                            if (!is_int($stock))
                                if ( (is_int((int)$stock)) AND ((int)$stock > 0)) {$instock = (int)$stock;}
                        }
                        //var_dump($not_in_stock_arr[0]);var_dump($not_in_stock_arr[1]);var_dump($stock);
                        if($not_in_stock_arr[0] || $not_in_stock_arr[1]){
                            
                            if(in_array($stock, $not_in_stock_arr)){
                                //var_dump($stock);
                                $instock = "0";
                            }
                        }
                        
                        if($parser->xmlwaiting_attr){
                            if($product->{$parser->xmlwaiting}->attributes)
                                $oji_value = $product->{$parser->xmlwaiting}->attributes()[$parser->xmlwaiting_attr];
                            else
                                $oji_value = $product->attributes()[$parser->xmlwaiting_attr];
                        }else{
                            $oji_value = $product->{$parser->xmlwaiting};
                        } 
                        
                        $oji = 0;
                        $ojistock_arr = (array)explode(";", $parser->xmlojiname);
                        foreach($ojistock_arr as $ojiword){
                            //var_dump($ojiword);
                            if(strpos($oji_value, $ojiword)!==false){
                                $oji = $oji_value;
                            }
                        };
                        
                        
                        if ($parser->reprice) 
        					$pfr = $price;
        				else $pfr = 1;
                 
                        if (!empty($art) AND $pfr > 0)
        				{
                            $query = $this->db->placehold('SELECT id as variant_id, name FROM __variants WHERE sku=?', $art);
                            
                            $this->db->query($query);
                            $exists_variants = $this->db->results();
        
                            if(!count($exists_variants)){
                                $variants_not_exists[] = $art;
                                echo "<pre style='background: green; font-weight: 500'>Новой товар c артикулом ".$art."</pre>";
                            } else {
                                if ($parser->reprice) $reprice = "`price` =  $price"; else $reprice = '';
                                if ($parser->restock) $restock = " `stock` =  $instock"; else $restock = '';
            					if ($parser->xmlstockprice) $resprice = " `stockprice` = $stockprice"; else $resprice = '';
            					$data = date("j F Y H:i:s", time());	
                                $last_parse = ', `last_parse`="'.$data.'"';
                                
            					if(!empty($reprice) AND !empty($resprice) AND !empty($restock)){
                                    $restock = ", ". $restock.", ";
            					}elseif(!empty($reprice) AND !empty($restock)){
                                    $restock = ", ".$restock;	
                                }elseif(!empty($reprice) AND !empty($resprice)){
                                    $resprice = ", ".$resprice;  
                                }elseif(!empty($restock) AND !empty($resprice))
                                    $resprice = ", ".$resprice;       
            					
            					if (($restock) OR !empty($reprice)) {
            					   
        							$queryx = $this->db->placehold("UPDATE  `__variants` v INNER JOIN `__products` p ON v.product_id = p.id  SET  $reprice $restock $resprice $last_parse , v.store=NULL WHERE `sku` = ? AND p.vendor = ?", $art, $parser->id);
        							$this->db->query($queryx);
        							//$result = $this->db->result;
                                    //print($queryx);
                                    
        							if (!empty($oji))
        							{	
        								$queryx = $this->db->placehold("UPDATE  `__products` SET `sklad_id` = 3, `byrequest` = ? $last_parse WHERE `id` IN (SELECT product_id FROM __variants WHERE sku = ? AND vendor = ?)", $oji, $art, $parser->id);
        								$this->db->query($queryx);
        								//print "<b>$queryx</b><br>";
        							}
            						
            						if ($parser->reprice){
                                        $np = " . Новая цена: " .$price . " Опт: ".$stockprice;
                                        $variants = $this->variants->get_variants(array('sku'=>$art));
                                        //var_dump($variants);
                                        $product_id = 0;
                                        foreach($variants as $v){
                                            $product_id = $v->product_id;
                                        }
                                        if($product_id){
                                            $catogories = array();                            
                                            $categories = $this->categories->get_categories(array('product_id'=>$product_id));
                                            //var_dump($product_id);
                                            foreach($categories as $cat){
                                                if(!empty($cat->lowcost_limit) && !empty($cat->lowcost_category)){
                                                    if($price <= $cat->lowcost_limit){
                                                        $this->categories->add_product_category($product_id, $cat->lowcost_category,100);
                                                        $np .= '. Добавили дешевую катерогию с id '. $cat->lowcost_category;
                                                    }
                                                }
                                                if(!empty($cat->highcost_limit) && !empty($cat->highcost_category)){
                                                    if($price >= $cat->highcost_limit){
                                                        $this->categories->add_product_category($product_id, $cat->highcost_category,100);
                                                        $np .= '. Добавили дорогую катерогию с id '. $cat->highcost_category;
                                                    }
                                                }
                                            }
                                        } 
                                    }else $np = '';
                                    
            						if ($parser->restock) $ns = " Наличие: ".$instock; else $ns = '';
            						if (!empty($oji)) $brd = " Поставка: ".$oji; else $brd = ''; 
                                    print("Обновили товар с артикулом: "  .$art . $np . $ns . $brd . "  <br>");
            					}
                            }
        				}
                        flush();
                        //var_dump($instock);
                    }
                    //print_r($products);die;
                }
            }
            if(count($variants_not_exists)){
                unlink('simpla/files/export/new_articules.csv');
                file_put_contents('simpla/files/export/new_articules.csv', implode("\n", $variants_not_exists));
                echo "<a href='http://velocube.ru/simpla/files/export/new_articules.csv?".rand(0,100)."'>Загрузить файл</a>  Всего новых ~ ".count($variants_not_exists);
            }
		}
	}
 
	public function go_parser($id)
	{
		//
		return true;
	}
	
	public function go_clear()
	{
	
		$ndirct = "simpla/files/prices/"; 
		$nhdl=opendir($ndirct); 
		while ($nfile = readdir($nhdl)) 
		{
		
		//$na[] = $nfile; 
		if (is_file($ndirct.$nfile)) 
			{ 
					unlink($ndirct.$nfile);
					//$na[] = $nfile; 
			} 
	} 
	closedir($nhdl); 

	 
	return true;
	}
	
	public function mk_op()
	{
			$opname = "simpla/files/opins/velocube.xml";
	 		$xml =  simplexml_load_file($opname);
			
			if ($xml) {
				foreach ($xml->xpath('//opinion') as $ops) 
				{
					 	// print_r($ops);
						$date = $ops->date;
						$fio = $ops->author;
						$comment = $ops->text;
						$dost = $ops->pro;
						$nedost = $ops->contra;
						$rate =  $ops->attributes();
						 $rate = (int)$rate->grade[0] + 3;						 
						 $query = $this->db->placehold("INSERT INTO __comments (`id`, `date`, `ip`, `object_id`, `name`, `text`, `type`, `approved`, `parent_id`, `admin`, `rating`, `dost`, `nedost`, `rate`) VALUES (NULL, ?, '127.0.0.1', '0', ?, ?, 'shop', '0', '0', '1', '0', ?, ?, ?);",$date,$fio,$comment,$dost,$nedost,$rate);
						 @$this->db->query($query);
				}
				 
			}
	}
	
	public function mk_pop()
	{
		//print "pop";
			$products = $this->products->get_products(array("limit"=>10000));
			//print_r($products);
			foreach ($products as $prod)
			{
				if (!file_exists("simpla/files/opins/search/".$prod->name.".xml"))
				$res = file_put_contents("simpla/files/opins/search/".$prod->name.".xml", file_get_contents("http://market.icsystem.ru/v1/search.xml?geo_id=213&text=".urlencode($prod->name)));				 
			
		////	
			
	$filename = "simpla/files/opins/search/".$prod->name.".xml";
	if (file_exists($filename))
		$xml =  simplexml_load_file($filename);
		if ($xml) 
		{ 
				foreach($xml->xpath('//search-item') as $si)
				{				
					foreach($si->model as $model)
					{							
						$modelid = $model->attributes()->id;						 		
						if (!file_exists("simpla/files/opins/products/".$modelid.".xml"))						
							$res = file_put_contents("simpla/files/opins/products/$modelid.xml", file_get_contents("http://market.icsystem.ru/v1/model/$modelid/opinion.xml?count=30"));
						
						//////////////////
						
			$opname = "simpla/files/opins/products/".$modelid.".xml";
	 		$xml2 =  simplexml_load_file($opname);
			
			if ($xml2) {
				foreach ($xml2->xpath('//opinion') as $ops) 
				{
					 	// print_r($ops);
						$date = $ops->date;
						$fio = $ops->author;
						$comment = $ops->text;
						$dost = $ops->pro;
						$nedost = $ops->contra;
						$rate =  $ops->attributes();
						 $rate = (int)$rate->grade[0] + 3;						 
						 $query = $this->db->placehold("INSERT INTO __comments (`id`, `date`, `ip`, `object_id`, `name`, `text`, `type`, `approved`, `parent_id`, `admin`, `rating`, `dost`, `nedost`, `rate`) VALUES (NULL, ?, '127.0.0.1', '$prod->id', ?, ?, 'product', '0', '0', '1', '0', ?, ?, ?);",$date,$fio,$comment,$dost,$nedost,$rate);
						 @$this->db->query($query);
				}
			}
						
						//////////////////
						
					}
				}
		}
		/////////////
		}

	}
	
}