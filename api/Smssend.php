<?php
 
require_once('Simpla.php');

class SMSSend extends Simpla
{
	public function send($phone,$sms)
	{
    if($this->settings->sms_convert)
		$sms		=	iconv("windows-1251","utf-8",$sms);
		$ch = curl_init("http://sms.ru/sms/send");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array(

		"api_id"		=>	$this->settings->sms_api,
		"to"			=>	$phone,
		"partner_id"    =>  "6583",
		"text"		=>	$sms, 
		"from" => "Velocube.ru"
));
$body = curl_exec($ch);
curl_close($ch); 

return false;

	}
}