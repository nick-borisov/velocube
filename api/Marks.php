<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Marks extends Simpla
{
	// Список указателей на метки 
	private $all_marks;
	


	// Функция возвращает массив меток
	public function get_marks($filter = array())
	{
          if(!isset($this->all_marks))
			$this->init_marks();
		if(!empty($filter['category_id']))
		{
            $visible_filter = '';
            if(!empty($filter['visible']))
                $visible_filter = 'AND visible=1';
                
			$query = $this->db->placehold("SELECT id FROM __marks WHERE category in(?@) $visible_filter ORDER BY position", $filter['category_id']);
   //print($query);
			$this->db->query($query);
			$marks_ids = $this->db->results('id');
			$result = array();
			foreach($marks_ids as $id)
				if(isset($this->all_marks[$id]))
					$result[$id] = $this->all_marks[$id];
			return $result;
		}
		
		elseif(!empty($filter))
		{
            $visible_filter = '';
            if(!empty($filter['visible']))
                $visible_filter = 'AND visible=1';
                
			$query = $this->db->placehold("SELECT mark_id FROM __products_marks WHERE product_id in(?@) $visible_filter ORDER BY position", (array)$filter);
			$this->db->query($query);
			$marks_ids = $this->db->results('mark_id');
			$result = array();
			foreach($marks_ids as $id)
				if(isset($this->all_marks[$id]))
					$result[$id] = $this->all_marks[$id];
			return $result;
		}
		
		return $this->all_marks;
	}	

	// Функция возвращает id меток для заданного товара
	public function get_product_marks($product_id)
	{
		$query = $this->db->placehold("SELECT product_id, mark_id  FROM __products_marks WHERE product_id in(?@) ORDER BY position", (array)$product_id);
		$this->db->query($query);
		return $this->db->results();
	}	

	// Функция возвращает id меток для всех товаров
	public function get_products_marks()
	{
		$query = $this->db->placehold("SELECT product_id, mark_id FROM __products_marks ORDER BY position");
		$this->db->query($query);
		return $this->db->results();
	}	

	

	// Функция возвращает заданную метку
	public function get_mark($id)
	{
		if(!isset($this->all_marks))
			$this->init_marks();
		if(is_int($id) && array_key_exists(intval($id), $this->all_marks))
			return $mark = $this->all_marks[intval($id)];
		elseif(is_string($id))
			foreach ($this->all_marks as $mark)
				if ($mark->url == $id)
					return $this->get_mark((int)$mark->id);	
		
		return false;
	}
	
	// Добавление метки
	public function add_mark($mark)
	{
		$mark = (array)$mark;
		if(empty($mark['url']))
		{
			$mark['url'] = preg_replace("/[\s]+/ui", '_', $mark['name']);
			$mark['url'] = strtolower(preg_replace("/[^0-9a-zа-я_]+/ui", '', $mark['url']));
		}	

		// Если есть категория с таким URL, добавляем к нему число
		while($this->get_mark((string)$mark['url']))
		{
			if(preg_match('/(.+)_([0-9]+)$/', $mark['url'], $parts))
				$mark['url'] = $parts[1].'_'.($parts[2]+1);
			else
				$mark['url'] = $mark['url'].'_2';
		}

		$this->db->query("INSERT INTO __marks SET ?%", $mark);
		$id = $this->db->insert_id();
		$this->db->query("UPDATE __marks SET position=id WHERE id=?", $id);		
		
		unset($this->all_marks);	
		return $id;
	}
	
	// Изменение категории
	public function update_mark($id, $mark)
	{
		$query = $this->db->placehold("UPDATE __marks SET ?% WHERE id=? LIMIT 1", $mark, intval($id));
		$this->db->query($query);
			
		unset($this->all_marks);	
		return intval($id);
	}
	
	// Удаление метки
	public function delete_mark($ids)
	{
	//	die(print_r($ids));
		$ids = (array)$ids;
		foreach($ids as $id)
		{
			//die($id);
				$query = $this->db->placehold("DELETE FROM __marks WHERE id = ?", $id);
				@$this->db->query($query);
				$query = $this->db->placehold("DELETE FROM __products_marks WHERE mark_id = ?", $id);
				@$this->db->query($query);
		}
		
		unset($this->all_marks);	
		return $id;
	}
	
	// Добавить метку к заданному товару
	public function add_product_mark($product_id, $mark_id)
	{
		//die(print("pid: $product_id mid: $mark_id"));
		//		$cpid = $this->categories->get_product_categories($product_id);
		//		if (is_array($cpid))
		//			$cpid = (int)$cpid[0]->category_id;
		//die(print($cpid));
		//die(print_r($this->marks->get_mark($mark_id)));
		
		$query = $this->db->placehold("INSERT IGNORE INTO __products_marks SET product_id=?, mark_id=?", intval($product_id), intval($mark_id));
		$this->db->query($query);
	}

	// Удалить метку заданного товара
	public function delete_product_mark($product_id, $mark_id)
	{
		$query = $this->db->placehold("DELETE FROM __products_marks WHERE product_id=? AND mark_id=? LIMIT 1", intval($product_id), intval($mark_id));
		$this->db->query($query);
	}
	



	// Инициализация меток, после которой метки будем выбирать из локальной переменной
	private function init_marks()
	{
		
		
		// Указатели на метки
		$pointers = array();
		
		
		// Выбираем все метки
		$query = $this->db->placehold("SELECT c.id, c.name, c.category, c.description, c.url, c.meta_title, c.meta_keywords, c.meta_description, c.visible, c.position
										FROM __marks c ORDER BY c.position");
											
		
		$this->db->query($query);
		$marks = $this->db->results();
				
		$finish = false;
		// Не кончаем, пока не кончатся категории, или пока ниодну из оставшихся некуда приткнуть
		while(!empty($marks)  && !$finish)
		{
			$flag = false;
			// Проходим все выбранные категории
			foreach($marks as $mark)
			{
				
				$pointers[$mark->id] = $mark;
				
				
			}
			if(!$flag) $finish = true;
		}
		
		
		

	
		$this->all_marks = $pointers;	
	}
}