<?php

require_once('Simpla.php');

class ReportStat extends Simpla
{
    function get_stat($filter = array()) { //Выборка товара
        $weekdays = array('вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб');
        
        if(isset($filter['is_delivery']))
            $date_field = 'o.delivery_date';
        else
            $date_field = 'o.date';
        
        $all_filters = $this->make_filter($filter);
        
        $query = $this->db->placehold("SELECT 
                o.total_price, 
                (SELECT SUM(IFNULL((pv.stockprice),0)*pv.amount) FROM __purchases AS pv WHERE pv.order_id=o.id) as total_stockprice, 
                DATE_FORMAT($date_field,'%d.%m.%y') date, 
                DATE_FORMAT($date_field,'%w') weekday, 
                DATE_FORMAT($date_field,'%H') hour, 
                DATE_FORMAT($date_field,'%d') day, 
                DATE_FORMAT($date_field,'%m') month, 
                DATE_FORMAT($date_field,'%Y') year, 
                o.status 
            FROM __orders o
            LEFT JOIN __orders_labels AS ol ON o.id=ol.order_id
            WHERE 1 $all_filters ORDER BY $date_field");
        $this->db->query($query);
        $data = $this->db->results();


        $group = 'day';
        if(isset($filter['date_filter']))
        {
            switch ($filter['date_filter']){
                case 'today':       $group = 'hour';    break;
                case 'yesterday':   $group = 'hour';    break;
                case 'last_24hour': $group = 'hour';    break;
                case 'this_year':   $group = 'month';   break;
                case 'last_year':   $group = 'month';   break;
                case 'all':         $group = 'month';   break;
            }
        }

        $results = array();

        foreach($data as $d)
        {
            switch($group) {
                case 'hour':
                    $date = $d->year.$d->month.$d->day.$d->hour;
                    $results[$date]['title'] = $d->day.'.'.$d->month.' '.$d->hour.':00';
                    break;
                case 'day':
                    $date = $d->year.$d->month.$d->day;
                    $results[$date]['title'] = $d->date.' '.$weekdays[$d->weekday];
                    break;
                case 'month':
                    $date = $d->year.$d->month;
                    $results[$date]['title'] = $d->month.'.'.$d->year;
                    break;
            }

            if(!isset($results[$date]['new']))
                $results[$date]['new'] = $results[$date]['confirm'] = $results[$date]['complite'] = $results[$date]['delete'] = $results[$date]['cancel'] =$results[$date]['new_total_stockprice'] = $results[$date]['confirm_total_stockprice'] = $results[$date]['complite_total_stockprice'] = $results[$date]['delete_total_stockprice'] = $results[$date]['cancel_total_stockprice'] = 0;

            switch($d->status) {
                case 0: $results[$date]['new'] += $d->total_price;
                        $results[$date]['new_total_stockprice'] += $d->total_stockprice;
                        break;
                case 1: $results[$date]['confirm'] += $d->total_price;
                        $results[$date]['confirm_total_stockprice'] += $d->total_stockprice;
                        break;
                case 2: $results[$date]['complite'] += $d->total_price;
                        $results[$date]['complite_total_stockprice'] += $d->total_stockprice;
                        break;
                case 3: $results[$date]['delete'] += $d->total_price;
                        $results[$date]['delete_total_stockprice'] += $d->total_stockprice;
                        break;
                case 4: $results[$date]['cancel'] += $d->total_price;
                        $results[$date]['cancel_total_stockprice'] += $d->total_stockprice;
                        break;                
            }
        }
        return $results;
    }

    function get_stat_orders($filter = array()) { //Выборка товара
        $weekdays = array('вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб');
        
        if(isset($filter['is_delivery']))
            $date_field = 'o.delivery_date';
        else
            $date_field = 'o.date';
        
        $all_filters = $this->make_filter($filter);

        $query = $this->db->placehold("SELECT
                o.id,
                DATE_FORMAT($date_field,'%d.%m.%y') date,
                DATE_FORMAT($date_field,'%w') weekday,
                DATE_FORMAT($date_field,'%H') hour,
                DATE_FORMAT($date_field,'%d') day,
                DATE_FORMAT($date_field,'%m') month,
                DATE_FORMAT($date_field,'%Y') year,
                o.status
            FROM __orders o

            WHERE 1 $all_filters ORDER BY $date_field");
        $this->db->query($query);
        $data = $this->db->results();

        $group = 'day';
        if(isset($filter['date_filter']))
        {
            switch ($filter['date_filter']){
                case 'today':       $group = 'hour';    break;
                case 'yesterday':   $group = 'hour';    break;
                case 'last_24hour': $group = 'hour';    break;
                case 'this_year':   $group = 'month';   break;
                case 'last_year':   $group = 'month';   break;
                case 'all':         $group = 'month';   break;
            }
        }

        $results = array();

        foreach($data as $d)
        {
            switch($group) {
                case 'hour':
                    $date = $d->year.$d->month.$d->day.$d->hour;
                    $results[$date]['title'] = $d->day.'.'.$d->month.' '.$d->hour.':00';
                    break;
                case 'day':
                    $date = $d->year.$d->month.$d->day;
                    $results[$date]['title'] = $d->date.' '.$weekdays[$d->weekday];
                    break;
                case 'month':
                    $date = $d->year.$d->month;
                    $results[$date]['title'] = $d->month.'.'.$d->year;
                    break;
            }

            if(!isset($results[$date]['new']))
                $results[$date]['new'] = $results[$date]['confirm'] = $results[$date]['complite'] = $results[$date]['delete'] = $results[$date]['cancel'] = 0;

            switch($d->status) {
                case 0:
                    $results[$date]['new']++;
                    break;
                case 1:
                    $results[$date]['confirm']++;
                    break;
                case 2:
                    $results[$date]['complite']++;
                    break;
                case 3:
                    $results[$date]['delete']++;
                    break;
                case 4:
                    $results[$date]['cancel']++;
                    break;                    
            }
        }
        
        return $results;
    }

    function get_report_purchases($filter = array()) { //Выборка товара
        // По умолчанию
        $sort_prod = 'sum_price DESC';

        if(isset($filter['sort_prod'])){
            switch($filter['sort_prod']){
                case 'price':
                    $sort_prod = $this->db->placehold('sum_price DESC');
                break;
                case 'price_in':
                    $sort_prod = $this->db->placehold('sum_price ASC');
                break;
                case 'stockprice':
                    $sort_prod = $this->db->placehold('sum_stockprice DESC');
                break;
                case 'stockprice_in':
                    $sort_prod = $this->db->placehold('sum_stockprice ASC');
                break;
                case 'amount':
                    $sort_prod = $this->db->placehold('amount DESC');
                break;
                case 'amount_in':
                    $sort_prod = $this->db->placehold('amount ASC');
                break;
            }
        }

        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                o.id,
                p.product_id,
                p.variant_id,
                p.product_name,
                p.variant_name,
                SUM(p.price * p.amount) as sum_price,
                SUM(p.stockprice * p.amount) as sum_stockprice,
                SUM(p.amount) as amount,
                p.sku FROM __purchases AS p
            LEFT JOIN __orders AS o ON o.id = p.order_id

            WHERE 1 $all_filters
            GROUP BY p.variant_id
            ORDER BY $sort_prod");

        $this->db->query($query);
        return $this->db->results();
    }

    function get_report_purchase($filter = array()) { //Связанный товар

        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                p.product_id,
                o.id, p.variant_id,
                p.product_name,
                p.variant_name,
                p.price,
                p.amount,
                p.sku
            FROM __orders AS o
            LEFT JOIN __purchases AS p ON o.id = p.order_id

            WHERE 1 $all_filters");

        $this->db->query($query);
        return $this->db->results();
    }

    function get_report_purchases_all($filter = array()) { //Сумма продаж

        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                SUM(p.price * p.amount) as sum_price,
                SUM(p.amount) as amount
            FROM __orders AS o
            LEFT JOIN __purchases AS p ON o.id = p.order_id

            WHERE 1 $all_filters");

        $this->db->query($query);
        $report_purchases = array();
        foreach($this->db->results() as $report_purchase)
            $report_purchases[] = $report_purchase;
        return $report_purchases;
    }

    function get_products(){ // Товар суммарно

        // Выбираем товар
        $query = $this->db->placehold("SELECT COUNT(id) as count_id FROM __products WHERE 1");

        $this->db->query($query);
        foreach($this->db->results() as $counts)
            $count = $counts->count_id;
        return $count;
    }

    function get_variants_product($filter = array()){ //Вариаты товара, наличие

        $in_stock = '';

        if(isset($filter['in_stock']))
            $in_stock = $this->db->placehold('AND stock = 0');

        // Считаем товар по вариантам
        $query = $this->db->placehold("SELECT COUNT(id) as count_id FROM __variants WHERE 1 $in_stock");

        $this->db->query($query);
        foreach($this->db->results() as $counts)
            $count = $counts->count_id;
        return $count;
    }

    function get_user_count($filter = array()){ // Пользователи, не активые

        if(isset($filter['enabled']))
            $enabled = $this->db->placehold('AND enabled = 0');

        // Считаем пользователей
        $query = $this->db->placehold("SELECT COUNT(id) as count_id FROM __users WHERE 1 $enabled");

        $this->db->query($query);
        foreach($this->db->results() as $counts)
            $count = $counts->count_id;
        return $count;
    }

    function get_order_count($filter = array()){ //Заказы, период, статусы

        $all_filters = $this->make_filter($filter);

        // Считаем заказы
        $query = $this->db->placehold("SELECT
                COUNT(id) as count_id
            FROM __orders as o

            WHERE 1 $all_filters");

        $this->db->query($query);
        foreach($this->db->results() as $counts)
            $count = $counts->count_id;
        return $count;
    }

    function get_report_product($filter = array(), $id) { //Выборка товара для страицы товара
        // По умолчанию
        $variant_id = '';

        if(isset($filter['variant_id']))
            $variant_id = $this->db->placehold('AND p.variant_id = ?', intval($filter['variant_id']));
        
        if(isset($filter['is_delivery']))
            $date_field = 'o.delivery_date';
        else
            $date_field = 'o.date';
        
        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                o.id,
                DATE($date_field) as date,
                p.product_id,
                p.variant_id,
                p.product_name,
                p.variant_name,
                SUM(p.price * p.amount) as price,
                SUM(p.amount) as amount,
                p.sku
            FROM __orders AS o
            LEFT JOIN __purchases AS p ON o.id = p.order_id

            WHERE 1 AND p.product_id=? $variant_id $all_filters
            GROUP BY DATE($date_field)
            ORDER BY $date_field", $id);

        $this->db->query($query);
        return $this->db->results();
    }


    function get_report_purchase_product($filter = array(),$id) { //Связанный товар
        // По умолчанию
        $variant_id = '';

        if(isset($filter['variant_id']))
            $variant_id = $this->db->placehold('AND variant_id = ?', intval($filter['variant_id']));
        
        if(isset($filter['is_delivery']))
            $date_field = 'o.delivery_date';
        else
            $date_field = 'o.date';
        
        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                p.product_id,
                o.id,
                o.total_price,
                DATE($date_field) as date,
                p.variant_id,
                p.product_name,
                p.variant_name,
                p.price,
                p.amount,
                p.sku
            FROM __orders AS o
            LEFT JOIN __purchases AS p ON o.id = p.order_id

            WHERE p.order_id IN (SELECT order_id FROM s_purchases WHERE product_id=? $variant_id) AND p.product_id!=? $all_filters", $id, $id);

        $this->db->query($query);
        return $this->db->results();
    }

    function get_report_purchase_product_summ($filter = array(), $id) { //Связанный товар
        // По умолчанию
        $variant_id = '';

        if(isset($filter['variant_id']))
            $variant_id = $this->db->placehold('AND variant_id = ?', intval($filter['variant_id']));

        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                p.product_id,
                p.variant_id,
                p.product_name,
                SUM(p.amount) as amount,
                p.variant_name
            FROM __orders AS o
            LEFT JOIN __purchases AS p ON o.id = p.order_id

            WHERE 1 AND p.order_id IN (SELECT order_id FROM s_purchases WHERE product_id=? $variant_id) AND p.product_id!=? $all_filters
            GROUP BY p.variant_id
            ORDER BY amount DESC", $id, $id);

        $this->db->query($query);
        return $this->db->results();
    }

    function get_report_purchase_product_total($filter = array(),$id) { //Связанный товар
        // По умолчанию
        $variant_id = '';

        if(isset($filter['variant_id']))
            $variant_id = $this->db->placehold('AND p.variant_id = ?', intval($filter['variant_id']));

        $all_filters = $this->make_filter($filter);

        // Выбираем заказы
        $query = $this->db->placehold("SELECT
                p.product_name,
                SUM(p.price * p.amount) as price,
                SUM(p.amount) as amount
            FROM __orders AS o
            LEFT JOIN __purchases AS p ON o.id = p.order_id

            WHERE 1 AND p.product_id = ? $variant_id $all_filters", $id);    
    
        $this->db->query($query);
        return $this->db->results();
    }
    
    private function make_filter($filter = array()) { //Связанный товар
        // По умолчанию
        $label_filter = '';        
        $period_filter = '';
        $date_filter = '';
        $status_filter = '';
        
        if(isset($filter['is_delivery']))
            $date_field = 'o.delivery_date';
        else
            $date_field = 'o.date';
        
        if(isset($filter['status']))
            $status_filter = $this->db->placehold('AND o.status = ?', intval($filter['status']));

        if(isset($filter['label']))
            $label_filter = $this->db->placehold('AND o.id in (SELECT order_id FROM __orders_labels WHERE label_id=?)', $filter['label']);
        
        if(isset($filter['date_from']) && !isset($filter['date_to'])){
            $period_filter = $this->db->placehold("AND $date_field > ?", $filter['date_from']);
        }
        elseif(isset($filter['date_to']) && !isset($filter['date_from'])){
            $period_filter = $this->db->placehold("AND $date_field < ?", $filter['date_to']);
        }
        elseif(isset($filter['date_to']) && isset($filter['date_from'])){
            $period_filter = $this->db->placehold("AND ($date_field BETWEEN ? AND ?)", $filter['date_from'], $filter['date_to']);
        }
        
        if(isset($filter['date_filter']))
        {
            switch ($filter['date_filter']){
                case 'today':
                    $date_filter = "AND DATE($date_field) = DATE(NOW())";
                    break;
                case 'this_week':
                    $date_filter = "AND WEEK($date_field - INTERVAL 1 DAY) = WEEK(now()) AND YEAR($date_field - INTERVAL 1 DAY) = YEAR(now())";
                    break;
                case 'this_month':
                    $date_filter = "AND MONTH($date_field) = MONTH(now()) AND YEAR($date_field) = YEAR(now())";
                    break;
                case 'this_year':
                    $date_filter = "AND YEAR($date_field) = YEAR(now())";
                    break;
                case 'yesterday':
                    $date_filter = "AND DATE($date_field) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY))";
                    break;
                case 'last_week':
                    $date_filter = "AND WEEK($date_field - INTERVAL 1 DAY) = WEEK(DATE_SUB(NOW(),INTERVAL 1 WEEK)) AND YEAR($date_field - INTERVAL 1 DAY) = YEAR(DATE_SUB(NOW(),INTERVAL 1 WEEK))";
                    break;
                case 'last_month':
                    $date_filter = "AND MONTH($date_field) = MONTH(DATE_SUB(NOW(),INTERVAL 1 MONTH)) AND YEAR($date_field) = YEAR(DATE_SUB(NOW(),INTERVAL 1 MONTH)) ";
                    break;
                case 'last_year':
                    $date_filter = "AND YEAR($date_field) = YEAR(DATE_SUB(NOW(),INTERVAL 1 YEAR))";
                    break;
                case 'last_24hour':
                    $date_filter = "AND $date_field >= DATE_SUB(NOW(),INTERVAL 24 HOUR)";
                    break;
                case 'last_7day':
                    $date_filter = "AND DATE($date_field) >= DATE(DATE_SUB(NOW(),INTERVAL 6 DAY))";
                    break;
                case 'last_30day':
                    $date_filter = "AND DATE($date_field) >= DATE(DATE_SUB(NOW(),INTERVAL 29 DAY))";
                    break;                                                                                                                                                                
            }
        }
        
        return "$status_filter $date_filter $period_filter $label_filter";
    }
        
}
