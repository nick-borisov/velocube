<?php
//ini_set('display_errors',1);
/**
 * Simple CORE CMS
 *
 * @copyright	2015 Sheeft
 * @link		http://simplecore.ru
 * @author		Ismatulaev G.
 *
 */

require_once('Simpla.php');

class Parser_XML extends Simpla
{

	// Список указателей на категории в дереве категорий (ключ = id категории)
	private $all_Parsers;
	// Дерево категорий
	private $Parsers_tree;
	
	// Функция возвращает массив категорий
	public function get_parsers($filter = array())
	{
		 
		$parsers = array();
		$active_filter = "";
		if(isset($filter['active']))
		$active_filter = "AND active = 1";

		// Выбираем все вендоры
		$query = $this->db->placehold("SELECT * FROM __parsers_xml WHERE 1 $active_filter ORDER BY name");
		//$query = $this->db->placehold("ALTER TABLE  `s_parsers` ADD  `notinstockname` varchar( 255 ) NOT NULL");
		$this->db->query($query);
		return $this->db->results();
	}	
 
	// Функция возвращает заданную категорию
	public function get_parser($id)
	{
		$query = "SELECT * FROM __parsers_xml WHERE id = $id";
		$this->db->query($query);
		return $this->db->result();
	}
	
	// Добавление категории
	public function add_parser($parser)
	{
		$parser = (array)$parser;
		$query = $this->db->placehold("INSERT INTO __parsers_xml SET ?%", $parser);

        $this->db->query($query);
		return $this->db->insert_id();
		
	}
	
	// Изменение категории
	public function update_parser($id, $parser)
	{
		$query = $this->db->placehold("UPDATE __parsers_xml SET ?% WHERE id=? LIMIT 1", $parser, intval($id));   
        //print($query);     
		$this->db->query($query);
		return $id;
	}
	
	// Удаление категории
	public function delete_parser($id)
	{
 
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __parsers_xml WHERE id=? LIMIT 1", $id);
			$this->db->query($query);		
		}
  
		return true;
	}
    
    public function clear_xml_category($category_id){
        $products = $this->products->get_products(array('category_id'=>$category_id, 'limit'=>2000));
        foreach($products as $product){
            $this->products->delete_product($product->id);
        }
    }
    
    public function translit_ru2en($filename)
	{
		$ru = explode('-', "А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я"); 
		$en = explode('-', "A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch---Y-y---E-e-YU-yu-YA-ya");

	 	$res = str_replace($ru, $en, $filename);
		$res = preg_replace("/[\s]+/ui", '-', $res);
		$res = preg_replace("/[^a-zA-Z0-9\.\-\_]+/ui", '', $res);
	 	$res = strtolower($res);
	    return $res;  
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function upd_date($id)
	{
 
		if(!empty($id))
		{
			$data = date("j F Y, H:i:s");				 
			$query = $this->db->placehold("UPDATE __parsers_xml SET lastrun=? WHERE id=? LIMIT 1", $data, $id);
			$this->db->query($query);		
		}
  
		return true;
	}
	
	public function go_parse($id = NULL)
	{
        //куда ложим картинки физически
        $path = 'files/originals/';
        $opts = array(
          'http' => array('ignore_errors' => true)
        );
        $context = stream_context_create($opts);
                
    	setlocale(LC_TIME, "ru_RU");
    	if (empty($id))
    		$parsers = $this->get_parsers(array("active"=>"1"));
		else
		$parsers[] = $this->get_parser($id);
    
		foreach($parsers as $parser)
		{
							
			$this->upd_date($id);
            
            ///////////////////////////XML IMPORT PARSER//////////////////////////////////
            
            if($parser->xmlurl){
                $string = file_get_contents($parser->xmlurl);
                //var_dump($string);
                $xml = simplexml_load_string($string);
                        
                $category_id = $parser->category_id;
                $vendor_id = $parser->vendor_id;
                $brand_id = $parser->brand_id;
                
                $products = $xml->xpath("//".$parser->xmlproduct);

                $obj_currency = $xml->xpath("//".$parser->xmlcurrency);
                if($obj_currency)
                    $currency = $obj_currency[0]->attributes()[$parser->xmlcurrency_attr];
                
                foreach($products as $product){

                    if($parser->xmlart == 'xpath'){
                        $art = html_entity_decode(trim(strval($product->xpath($parser->xmlart_attr)[0])));
                    }elseif($parser->xmlart_attr){
                        //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                        if($product->{$parser->xmlart}->attributes())
                            $art = html_entity_decode(trim(strval($product->{$parser->xmlart}->attributes()[$parser->xmlart_attr])));
                        else
                            $art = html_entity_decode(trim(strval($product->attributes()[$parser->xmlart_attr])));
                    }else{
                        $art = html_entity_decode(trim(strval($product->{$parser->xmlart})));
                    }
                    //Проверяем по артикулу и поставщику, есть ли такой вариант, 
                    $q = $this->db->query("SELECT v.id FROM s_variants v 
                                        LEFT JOIN s_products p ON p.id=v.product_id
                                        WHERE v.sku=? AND p.vendor=? LIMIT 1", $art, $vendor_id);
                    $exists = $this->db->result('id');
                    
                    $count_added_images = array();
                    $count_added_features = array();
                    $brand = $type_prefix = $description = $name = $images = $images2 = $stock = $price = $stockprice = $xmlold_price = $value = null;

                                   
                    //если нет, то можем добавлять
                    if(!$exists){ 
                        if($parser->limit_parse && (count($count_not_exists) >= $parser->limit_parse)){
                            echo "<b>Остановлено по лимиту  $parser->limit_parse. Добавлено ".count($count_added_product_ids)." товаров</b>";
                            break 2;
                        }else{
                            
                            //Счетчик обнаруженных новых продуктов
                            $count_not_exists[] = $art;                       
                            //$type_prefix = html_entity_decode(trim(strval($product->{$parser->xmltypeprefix})));
                            //Получаем type_prefix
                            if($parser->xmltypeprefix == 'xpath'){
                                $type_prefix = html_entity_decode(trim(strval($product->xpath('.//'.$parser->xmltypeprefix_attr)[0])));
                            }elseif($parser->xmltypeprefix_attr){
                                //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                if($product->{$parser->xmltypeprefix}->attributes())
                                    $type_prefix = html_entity_decode(trim(strval($product->{$parser->xmltypeprefix}->attributes()[$parser->xmltypeprefix_attr])));
                                else
                                    $type_prefix = html_entity_decode(trim(strval($product->attributes()[$parser->xmltypeprefix_attr])));
                            }else{
                                $type_prefix = html_entity_decode(trim(strval($product->{$parser->xmltypeprefix})));
                            }
                            
                            //$name = html_entity_decode(trim(strval($product->{$parser->xmlname})));
                            //Получаем имя 
                            if($parser->xmlname == 'xpath'){
                                $name = html_entity_decode(trim(strval($product->xpath($parser->xmlname_attr)[0])));
                            }elseif($parser->xmlname_attr){
                                //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                if($product->{$parser->xmlname}->attributes())
                                    $name = html_entity_decode(trim(strval($product->{$parser->xmlname}->attributes()[$parser->xmlname_attr])));
                                else
                                    $name = html_entity_decode(trim(strval($product->attributes()[$parser->xmlname_attr])));
                            }else{
                                $name = html_entity_decode(trim(strval($product->{$parser->xmlname})));
                            }
                            
                            //$brand = html_entity_decode(trim(strval($product->{$parser->xmlbrand})));
                            //Получаем бренд
                            if($parser->xmlbrand == 'xpath'){
                                $brand = html_entity_decode(trim(strval($product->xpath($parser->xmlbrand_attr)[0])));
                            }elseif($parser->xmlbrand_attr){
                                //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                if($product->{$parser->xmlbrand}->attributes())
                                    $brand = html_entity_decode(trim(strval($product->{$parser->xmlbrand}->attributes()[$parser->xmlbrand_attr])));
                                else
                                    $brand = html_entity_decode(trim(strval($product->attributes()[$parser->xmlbrand_attr])));
                            }else{
                                $brand = html_entity_decode(trim(strval($product->{$parser->xmlbrand})));
                            }
//if($art == 'T1X-04'){var_dump($brand);print('  ');var_dump($art);die;}
                            //Описание 
                            if($parser->xmldesctiption == 'xpath'){
                                $description = html_entity_decode(strval($product->xpath($parser->xmldesctiption_attr)[0]));
                            }elseif($parser->xmldesctiption_attr){
                                if($product->{$parser->xmldesctiption}->attributes())
                                    $description = html_entity_decode(trim(strval($product->{$parser->xmldesctiption}->attributes()[$parser->xmldesctiption_attr])));
                                else
                                    $description = html_entity_decode(trim(strval($product->attributes()[$parser->xmldesctiption_attr])));
                            }else{
                                $description = html_entity_decode(trim(strval($product->{$parser->xmldesctiption})));
                            }  
                            
                            //Если не указан "статический" бренд, то попытаемся взять из тега (если он указан)
                            if(!$brand_id && !empty($brand)){
                                $query = $this->db->placehold('SELECT id FROM __brands WHERE name LIKE "%'.$brand.'%" LIMIT 1');
                                $this->db->query($query);
                                $brand_id_parse = $this->db->result('id');
                            }
                            if($brand_id_parse)
                                $obj_brand = $this->brands->get_brand(intval($brand_id_parse));
                            else
                                $obj_brand = $this->brands->get_brand(intval($brand_id));

                            $fullname = $type_prefix.' '.$obj_brand->name.' '.$name;
                            $product_url = $this->translit_ru2en($fullname);
                            
                            //Добавляем продукт в базу
                            $product_id = $this->products->add_product(array('type_prefix'=>$type_prefix, 'name'=>$name, 'vendor'=>$vendor_id, 'brand_id'=>$obj_brand->id, 'fullname'=>$fullname, 'url'=>$product_url, 'body'=>$description));
                            
                            if($product_id){
                                
                                //Счетчик добавленных продуктов
                                $count_added_product_ids[] = $product_id;
                                
                                //Получаем картинки
                                if($parser->xmlimage == 'xpath'){
                                    $images = $product->xpath($parser->xmlimage_attr);
                                }elseif($parser->xmlimage_attr){
                                    //проверим, есть ли у этого тега атрибуты, если есть, то берем их
                                    if($product->{$parser->xmlimage}->attributes)
                                        $images = $product->{$parser->xmlimage}->attributes()[$parser->xmlimage_attr];
                                }else{
                                    $images = $product->{$parser->xmlimage};
                                    //Проверим в потомках (если вложенный)
                                    if(!$images){
                                        $images = $product->xpath('.//'.$parser->xmlimage);
                                    } 
                                }
//var_dump($images);
                                foreach($images as $image){
                                    $filename = trim(strval($image));
                                    if(substr($filename, -4)=='.png'){
                                        $file = file_get_contents($filename, false, $context);
                                        $newname = pathinfo($filename, PATHINFO_BASENAME);
                                        $newname = str_replace(" ","", $newname);
                                        $newname = str_replace("&nbsp;","", $newname);
                                        $newname = str_replace(",",".", $newname);
            				            $newname = str_replace("","", $newname);
                                        $newname = $this->translit_ru2en($newname);
                                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                        $image = $newname;
                                        file_put_contents($path.$newname, $file);
                                    }
                                    $image_id = $this->products->add_image($product_id, trim(strval($image)));
                                    $count_added_images[] = $image_id;
                                }
                                
                                //Получаем дополнительные картинки
                                if($parser->xmlimage_2){
                                    if($parser->xmlimage_2 == 'xpath'){
                                        $images2 = $product->xpath($parser->xmlimage_2_attr);
                                    }elseif($parser->xmlimage_2_attr){
                                        //проверим, есть ли у этого тега атрибуты, если есть, то берем их
                                        if($product->{$parser->xmlimage_2}->attributes)
                                            $images2 = $product->{$parser->xmlimage_2}->attributes()[$parser->xmlimage_2_attr];
                                    }else{
                                        $images2 = $product->{$parser->xmlimage_2};
                                        //Проверим в потомках (если вложенный)
                                        if(!$images2){
                                            $images2 = $product->xpath('.//'.$parser->xmlimage_2);
                                        } 
                                    }
     
                                    foreach($images2 as $image){
                                        $filename = trim(strval($image));
                                        if(substr($filename, -4)=='.png'){
                                            $file = file_get_contents($filename, false, $context);
                                            $newname = pathinfo($filename, PATHINFO_BASENAME);
                                            $newname = str_replace(" ","", $newname);
                                            $newname = str_replace("&nbsp;","", $newname);
                                            $newname = str_replace(",",".", $newname);
                				            $newname = str_replace("","", $newname);
                                            $newname = $this->translit_ru2en($newname);
                                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                            $image = $newname;
                                            file_put_contents($path.$newname, $file);
                                        }
                                        $image_id = $this->products->add_image($product_id, trim(strval($image)));
                                        $count_added_images[] = $image_id;
                                    }
                                }
                                
                                //Получаем цену
                                if($parser->xmlprice == 'xpath'){
                                    $price = strval($product->xpath($parser->xmlprice_attr)[0]);
                                }elseif($parser->xmlprice_attr){
                                    //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                    if($product->{$parser->xmlprice}->attributes)
                                        $price = $product->{$parser->xmlprice}->attributes()[$parser->xmlprice_attr];
                                    else
                                        $price = $product->attributes()[$parser->xmlprice_attr];
                                }else{
                                    $price = $product->{$parser->xmlprice};
                                }
        
                                $price = (string)$price;
                                $price =  str_replace(" ","", $price);
                                $price =  str_replace("&nbsp;","", $price);
                                $price =  str_replace(",",".", $price);
                				$price =  str_replace("$","", $price);
                				$price =  str_replace("","", $price);
                                $price = floatval(preg_replace("/[^\d,.]/","",$price));			
                				$price = floatval($price);
                                if ($currency) { $price = $price * (float)$currency; }
                                
                                //Получаем закупочную цену
                                if($parser->xmlstock_price == 'xpath'){
                                    $stockprice = strval($product->xpath($parser->xmlstock_price_attr)[0]);
                                }elseif($parser->xmlstock_price_attr){
                                    //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                    if($product->{$parser->xmlstock_price}->attributes)
                                        $stockprice = $product->{$parser->xmlstock_price}->attributes()[$parser->xmlstock_price_attr];
                                    else
                                        $stockprice = $product->attributes()[$parser->xmlstock_price_attr];
                                }else{
                                    $stockprice = $product->{$parser->xmlstock_price};
                                }
        
                                $stockprice = (string)$stockprice;
                                $stockprice =  str_replace(" ","", $stockprice);
                                $stockprice =  str_replace("&nbsp;","", $stockprice);
                                $stockprice =  str_replace(",",".", $stockprice);
                				$stockprice =  str_replace("$","", $stockprice);
                				$stockprice =  str_replace("","", $stockprice);
                                $stockprice = floatval(preg_replace("/[^\d,.]/","",$stockprice));			
                				$stockprice = floatval($stockprice);
                                
                                //Получаем старую цену
                                if($parser->xmlold_price == 'xpath'){
                                    $xmlold_price = strval($product->xpath($parser->xmlold_price_attr)[0]);
                                }elseif($parser->xmlold_price_attr){
                                    //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                    if($product->{$parser->xmlold_price}->attributes)
                                        $xmlold_price = $product->{$parser->xmlold_price}->attributes()[$parser->xmlold_price_attr];
                                    else
                                        $xmlold_price = $product->attributes()[$parser->xmlold_price_attr];
                                }else{
                                    $xmlold_price = $product->{$parser->xmlold_price};
                                }
        
                                $xmlold_price = (string)$xmlold_price;
                                $xmlold_price =  str_replace(" ","", $xmlold_price);
                                $xmlold_price =  str_replace("&nbsp;","", $xmlold_price);
                                $xmlold_price =  str_replace(",",".", $xmlold_price);
                				$xmlold_price =  str_replace("$","", $xmlold_price);
                				$xmlold_price =  str_replace("","", $xmlold_price);
                                $xmlold_price = floatval(preg_replace("/[^\d,.]/","",$xmlold_price));			
                				$xmlold_price = floatval($xmlold_price);
                                
                                
                                
                                //Наценка на price
                                if($parser->active_margin){
                                    $margins = unserialize($parser->margin);
                                    //var_dump($margins);
                    				if($margins['min'][0] && $margins['max'][0]){  
                    				    //print_r($margins);
                                        foreach($margins['min'] as $i=>$min){
                                            if($price > intval($margins['min'][$i]) && $price < intval($margins['max'][$i])){
                                                if(intval($margins['mode'][$i])==1){
                                                    $price = $price + $margins['price'][$i];
                                                }elseif(intval($margins['mode'][$i])==2){
                                                    $price = round($price + ($price * $margins['price'][$i] / 100), 0); 
                                                }
                                            }
                                        }
                    				}
                                }
                                //Вывод наценки для лога
                                if($stockprice)
                                    $print_margin =  "Наценка: ".$price - $stockprice." руб.";
                                else
                                    $print_margin = '';
                                
                                //наличие 
                                if($parser->xmlstock == 'xpath'){
                                    $stock = strval($product->xpath($parser->xmlstock_attr)[0]);
                                }elseif($parser->xmlstock_attr){
                                    if($product->{$parser->xmlstock}->attributes())
                                        $stock = trim(strval($product->{$parser->xmlstock}->attributes()[$parser->xmlstock_attr]));
                                    else
                                        $stock = trim(strval($product->attributes()[$parser->xmlstock_attr]));
                                }else{
                                    $stock = trim(strval($product->{$parser->xmlstock}));
                                }                        
                                                      
                                $stock_arr = (array)explode(";", $parser->xmlvnalname);
                                
            				    $not_in_stock_arr = (array)explode(";", $parser->xmlnenalname);
                                
                                if($stock_arr[0]){    
                                    if(in_array($stock, $stock_arr)) {$instock = NULL;} else {$instock = 0;}
                                }else{
                                    $stock = preg_replace("|[^\d-]+|", "", $stock);
                                    if (!is_int($stock))
                                        if ( (is_int((int)$stock)) AND ((int)$stock > 0)) {$instock = (int)$stock;}
                                }
                                if($not_in_stock_arr[0] || $not_in_stock_arr[1]){
                                    
                                    if(in_array($stock, $not_in_stock_arr)){
                                        //var_dump($stock);
                                        $instock = "0";
                                    }
                                }
                                //Вывод наличия для лога
                                if($instock === NULL)
                                    $print_instock = 'В наличии';
                                else
                                    $print_instock = 'Не в наличии';
                                
                                //Добавляем вариант
                                $variant_id = $this->variants->add_variant(array('product_id'=>$product_id, 'price'=>$price, 'stockprice'=>$stockprice, 'compare_price'=>$xmlold_price, 'sku'=>$art, 'stock'=>$instock));
                                
                                //Добавляем в категорию
                                if(empty($category_id))
                                    $category_id = 493;
                                $this->categories->add_product_category($product_id, $category_id);
                                
                                //Добавляем в товар свойства
                                if($parser->xmlfeatures){
                                    $features = unserialize($parser->xmlfeatures);
                                    if($features['feature'][0] && $features['tag'][0]){
                                        foreach($features['feature'] as $i=>$f){
    //print("\n XPATH");var_dump(strval($product->xpath("param[@name='Игровое поле']")[0]));
    
                                            if($features['tag'][$i] == 'xpath'){
                                                $value = html_entity_decode(trim(strval($product->xpath('.//'.$features['xpath'][$i])[0])));
                                            }elseif($features['attr'][$i]){
                                                //проверим, есть ли у этого тега атрибуты, если есть, то берем их, если нет то попытаемся взять у объекта продукта    
                                                if($product->{$features['tag'][$i]}->attributes()){
                                                    
                                                    $value = html_entity_decode(trim(strval($product->{$features['tag'][$i]}->attributes()[$features['attr'][$i]])));
                                                }else{
                                                    $value = html_entity_decode(trim(strval($product->attributes()[$features['attr'][$i]])));
                                                }
                                            }else{
                                                $value = html_entity_decode(trim(strval($product->{$features['tag'][$i]})));
                                                //Проверим в потомках (если вложенный)
                                                if(!$value){
                                                    $value = html_entity_decode(trim(strval($product->xpath('.//'.$features['attr'][$i])[0])));
                                                }
                                            }
                                            
                                            if($value){
                                                $this->features->update_option(intval($product_id), intval($features['feature'][$i]), $value);
                                                $count_added_features[] = $value;
                                            }
                                        }
                                    }
                                }
                                $color_array = array('lightgreen', 'pink', 'yellow', 'lightyellow', 'lightgrey', 'white', 'lightblue', 'lightseagreen', 'cyan', 'magenta');
                                echo "<div style='font-size: 13px; background-color: ".$color_array[rand(0, 9)]."'>Добавлен продукт <a href='/simpla/index.php?module=ProductAdmin&category_id=493&id=$product_id' target='_blank'>$fullname</a>. Цена: $price руб. $print_margin Наличие: '$print_instock'. Кол-во картинок: ".count($count_added_images).". Кол-во свойств: ".count($count_added_features)."</div>";
                            }
                        }
                    }   
                }
            }
            
            if(count($count_not_exists)){
                echo "<b>Обнаружено новых ".count($count_not_exists).". Добавлено ".count($count_added_product_ids)." товаров</b>";
            }
		}
	}
 
	public function go_parser($id)
	{
		//
		return true;
	}
	
	public function go_clear()
	{
	
		$ndirct = "simpla/files/prices/"; 
		$nhdl=opendir($ndirct); 
		while ($nfile = readdir($nhdl)) 
		{
		
		//$na[] = $nfile; 
		if (is_file($ndirct.$nfile)) 
			{ 
					unlink($ndirct.$nfile);
					//$na[] = $nfile; 
			} 
	} 
	closedir($nhdl); 

	 
	return true;
	}
	
	public function mk_op()
	{
			$opname = "simpla/files/opins/velocube.xml";
	 		$xml =  simplexml_load_file($opname);
			
			if ($xml) {
				foreach ($xml->xpath('//opinion') as $ops) 
				{
					 	// print_r($ops);
						$date = $ops->date;
						$fio = $ops->author;
						$comment = $ops->text;
						$dost = $ops->pro;
						$nedost = $ops->contra;
						$rate =  $ops->attributes();
						 $rate = (int)$rate->grade[0] + 3;						 
						 $query = $this->db->placehold("INSERT INTO __comments (`id`, `date`, `ip`, `object_id`, `name`, `text`, `type`, `approved`, `parent_id`, `admin`, `rating`, `dost`, `nedost`, `rate`) VALUES (NULL, ?, '127.0.0.1', '0', ?, ?, 'shop', '0', '0', '1', '0', ?, ?, ?);",$date,$fio,$comment,$dost,$nedost,$rate);
						 @$this->db->query($query);
				}
				 
			}
	}
	
	public function mk_pop()
	{
		//print "pop";
			$products = $this->products->get_products(array("limit"=>10000));
			//print_r($products);
			foreach ($products as $prod)
			{
				if (!file_exists("simpla/files/opins/search/".$prod->name.".xml"))
				$res = file_put_contents("simpla/files/opins/search/".$prod->name.".xml", file_get_contents("http://market.icsystem.ru/v1/search.xml?geo_id=213&text=".urlencode($prod->name)));				 
			
		////	
			
	$filename = "simpla/files/opins/search/".$prod->name.".xml";
	if (file_exists($filename))
		$xml =  simplexml_load_file($filename);
		if ($xml) 
		{ 
				foreach($xml->xpath('//search-item') as $si)
				{				
					foreach($si->model as $model)
					{							
						$modelid = $model->attributes()->id;						 		
						if (!file_exists("simpla/files/opins/products/".$modelid.".xml"))						
							$res = file_put_contents("simpla/files/opins/products/$modelid.xml", file_get_contents("http://market.icsystem.ru/v1/model/$modelid/opinion.xml?count=30"));
						
						//////////////////
						
			$opname = "simpla/files/opins/products/".$modelid.".xml";
	 		$xml2 =  simplexml_load_file($opname);
			
			if ($xml2) {
				foreach ($xml2->xpath('//opinion') as $ops) 
				{
					 	// print_r($ops);
						$date = $ops->date;
						$fio = $ops->author;
						$comment = $ops->text;
						$dost = $ops->pro;
						$nedost = $ops->contra;
						$rate =  $ops->attributes();
						 $rate = (int)$rate->grade[0] + 3;						 
						 $query = $this->db->placehold("INSERT INTO __comments (`id`, `date`, `ip`, `object_id`, `name`, `text`, `type`, `approved`, `parent_id`, `admin`, `rating`, `dost`, `nedost`, `rate`) VALUES (NULL, ?, '127.0.0.1', '$prod->id', ?, ?, 'product', '0', '0', '1', '0', ?, ?, ?);",$date,$fio,$comment,$dost,$nedost,$rate);
						 @$this->db->query($query);
				}
			}
						
						//////////////////
						
					}
				}
		}
		/////////////
		}

	}
	
}