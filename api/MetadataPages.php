<?php


require_once('Simpla.php');

class MetadataPages extends Simpla
{
	/*
	*
	* Функция возвращает массив линий, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function get_metadata_pages($filter = array())
	{
		$metadata_pages = array();
		// Выбираем все линии

		$query = $this->db->placehold("
			SELECT DISTINCT 
				l.id,
                                l.h1_title,
				l.name, 
				l.url, 
				l.meta_title, 
				l.meta_keywords, 
				l.meta_description, 
				l.description
			FROM 
				__metadata_pages l
                                ORDER BY l.name
		");                
                
		$this->db->query($query);
		return $this->db->results();
	}

	/*
	*
	* Функция возвращает линию по его id или url
	* (в зависимости от типа аргумента, int - id, string - url)
	* @param $id id или url поста
	*
	*/
	public function get_metadata_page($id)
	{
		if(is_int($id))			
			$filter = $this->db->placehold('l.id = ?', $id);
		else
                {
                        $id = urldecode($id);
			$filter = $this->db->placehold('l.url = ?', $id);
                }
		
                $query = "
                        SELECT 
                                l.id, 
                                l.h1_title,
                                l.name,
                                l.url,
                                l.meta_title,
                                l.meta_keywords, 
                                l.meta_description, 
                                l.description
                        FROM 
                                __metadata_pages l
                        WHERE 
                                $filter 
                        ORDER BY 
                                name 
                        LIMIT 1
                        ";                
                
		$this->db->query($query);
		return $this->db->result();
	}

	/*
	*
	* Добавление бренда
	* @param $metadata_page
	*
	*/
	public function add_metadata_page($metadata_page)
	{
                $this->settings->lastModifyLines=date("Y-m-d H:i:s");     
                $this->settings->lastModifyCategories=date("Y-m-d H:i:s");
                $this->settings->lastModifyProducts=date("Y-m-d H:i:s");
                $this->settings->lastModifyPages =date("Y-m-d H:i:s");  
                
		$metadata_page = (array)$metadata_page;
                $metadata_page['url'] = urldecode($metadata_page['url']);
		$this->db->query("INSERT INTO __metadata_pages SET ?%", $metadata_page);
		return $this->db->insert_id();
	}

	/*
	*
	* Обновление бренда(ов)
	* @param $metadata_page
	*
	*/		
	public function update_metadata_page($id, $metadata_page)
	{
                $this->settings->lastModifyLines=date("Y-m-d H:i:s");     
                $this->settings->lastModifyCategories=date("Y-m-d H:i:s");
                $this->settings->lastModifyProducts=date("Y-m-d H:i:s");
                $this->settings->lastModifyPages =date("Y-m-d H:i:s"); 
                
                $metadata_page->url = urldecode($metadata_page->url);
		$query = $this->db->placehold("UPDATE __metadata_pages SET ?% WHERE id=? LIMIT 1", $metadata_page, intval($id));
		$this->db->query($query);
		return $id;
	}
	
	/*
	*
	* Удаление бренда
	* @param $id
	*
	*/	
	public function delete_metadata_page($id)
	{
		if(!empty($id))
		{
                        $this->settings->lastModifyLines=date("Y-m-d H:i:s");     
                        $this->settings->lastModifyCategories=date("Y-m-d H:i:s");
                        $this->settings->lastModifyProducts=date("Y-m-d H:i:s");
                        $this->settings->lastModifyPages =date("Y-m-d H:i:s");    
	
			$query = $this->db->placehold("DELETE FROM __metadata_pages WHERE id=? LIMIT 1", $id);
			$this->db->query($query);			
		}
	}
	


}