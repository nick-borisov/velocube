<?php

/**
 * Simpla CMS
 *
 * @copyright	2013 Kirill Tikhomirov
 * @link		http://chocolatemol.es
 * @author		Kirill Tikhomirov
 *
 */

require_once('Simpla.php');

class Cslides extends Simpla
{
	/*
	*
	* ������� ���������� ������ �������
	*
	*/
	public function get_cslides($param)
	{
		
		$res = false;
		$brand_f = '';
		$cat_f = '';
		$pos_f = '';
 
		//print_r($param);
		
		if (empty($param["pos"]))
			$param["pos"] = "NULL";

		if (empty($param["brand"]))
			$param["brand"] = "NULL";	
		
		$cat_f = $this->db->placehold(' AND `cat` = ?',$param["cat"]);		
		$brand_f = $this->db->placehold(' AND `brand` = ?',$param["brand"]);
		$pos_f = $this->db->placehold(' AND `pos` = ?',$param["pos"]);

		if (($param["brand"]) == "ALL")
		{
			$brand_f = '';
			$pos_f = '';
		}
		
		$cslides = array();
		
		// �������� ��� ������
		$query = $this->db->placehold("SELECT DISTINCT id, name, url, description, image, position, brand, cat, pos
								 		FROM __cslides WHERE 1 $cat_f $brand_f $pos_f ORDER BY position");
										
		$this->db->query($query);
		//print($query);
		$res = $this->db->results();
		//print_r($res);
		return $res;
	}

	/*
	*
	* ������� ���������� ����� �� ��� id ��� url
	*
	*/
	public function get_cslide($id)
	{
		if(is_int($id))			
			$filter = $this->db->placehold('id = ?', $id);
		else
			$filter = $this->db->placehold('url = ?', $id);
		$query = "SELECT id, name, url, description, image, position, brand, cat, pos
								 FROM __cslides WHERE $filter ORDER BY position LIMIT 1";
		$this->db->query($query);
		return $this->db->result();
	}

	/*
	*
	* ���������� ������
	*
	*/
	public function add_cslide($slide)
	{
		$query = $this->db->placehold("INSERT INTO __cslides SET ?%", $slide);
		$this->db->query($query);
		$id = $this->db->insert_id();
		$query = $this->db->placehold("UPDATE __cslides SET position=id WHERE id=? LIMIT 1", $id);
		$this->db->query($query);
		return $id;
	}

	/*
	*
	* ���������� ������(��)
	*
	*/		
	public function update_cslide($id, $slide)
	{
		$query = $this->db->placehold("UPDATE __cslides SET ?% WHERE id in(?@) LIMIT ?", (array)$slide, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}
	
	/*
	*
	* �������� ������
	*
	*/	
	public function delete_cslide($id)
	{
		if(!empty($id))
		{
			$this->delete_image($id);	
			$query = $this->db->placehold("DELETE FROM __cslides WHERE id=? LIMIT 1", $id);
			$this->db->query($query);		
		}
	}
	
	/*
	*
	* �������� ����������� ������
	*
	*/
	public function delete_image($slide_id)
	{
		$query = $this->db->placehold("SELECT image FROM __cslides WHERE id=?", intval($slide_id));
		$this->db->query($query);
		$filename = $this->db->result('image');
		if(!empty($filename))
		{
			$query = $this->db->placehold("UPDATE __cslides SET image=NULL WHERE id=?", $slide_id);
			$this->db->query($query);
			$query = $this->db->placehold("SELECT count(*) as count FROM __cslides WHERE image=? LIMIT 1", $filename);
			$this->db->query($query);
			$count = $this->db->result('count');
			if($count == 0)
			{			
				@unlink($this->config->root_dir.$filename);		
			}
		}
	}

}