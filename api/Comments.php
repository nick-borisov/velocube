<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Comments extends Simpla
{

	// Возвращает комментарий по id
	public function get_comment($id)
	{
		$query = $this->db->placehold("SELECT c.id, c.dost, c.nedost, c.rate, c.object_id, c.name, c.ip, c.type, c.text, c.date, c.approved, c.parent_id, c.admin, c.rating, c.on_main_page FROM __comments c WHERE id=? LIMIT 1", intval($id));

		if($this->db->query($query))
			return $this->db->result();
		else
			return false; 
	}
	
	// Возвращает комментарии, удовлетворяющие фильтру
	public function get_comments($filter = array())
	{	
		// По умолчанию
		$limit = 0;
		$page = 1;
		$object_id_filter = '';
		$parent_id_filter = '';
		$type_filter = '';
		$keyword_filter = '';
		$approved_filter = '';
		$rateme_filter = '';
        $main_page_filter = '';

		
		if(isset($filter['rateme']))
			$rateme_filter  = $this->db->placehold("AND (c.rate=?)", intval($filter['rateme']));
		
		
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		if(isset($filter['ip']))
			$ip = $this->db->placehold("OR c.ip=?", $filter['ip']);
		if(isset($filter['approved']))
			$approved_filter = $this->db->placehold("AND (c.approved=? $ip)", intval($filter['approved']));
			
		if($limit)
			$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);
		else
			$sql_limit = '';

		if(!empty($filter['object_id']))
			$object_id_filter = $this->db->placehold('AND c.object_id in(?@)', (array)$filter['object_id']);

		if(!empty($filter['parent_id']))
			$parent_id_filter = $this->db->placehold('AND c.parent_id=?', $filter['parent_id']);

		if(!empty($filter['type']))
			$type_filter = $this->db->placehold('AND c.type=?', $filter['type']);
            
        if(!empty($filter['main_page']))
			$main_page_filter = $this->db->placehold('AND c.on_main_page=?', $filter['main_page']);

		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND c.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR c.text LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" ');
		}

			
		$sort='DESC';
		
		$query = $this->db->placehold("SELECT * FROM __comments c WHERE 1 $object_id_filter $parent_id_filter $type_filter $rateme_filter $keyword_filter $approved_filter $main_page_filter ORDER BY id $sort $sql_limit");
	
		$this->db->query($query);
		return $this->db->results();
	}
  
	// Возвращает комментарии, удовлетворяющие фильтру
	public function get_comments_tree($filter = array())
	{	
		// По умолчанию
		$limit = 0;
		$page = 1;
		$object_id_filter = '';
        $parent_id_filter = '';
		$type_filter = '';
		$keyword_filter = '';
		$approved_filter = '';
        $rateme_filter = '';
        $main_page_filter = '';

		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		if(isset($filter['rateme']))
			$rateme_filter  = $this->db->placehold("AND (c.rate=?)", intval($filter['rateme']));
		
		if(isset($filter['ip']))
			$ip = $this->db->placehold("OR c.ip=?", $filter['ip']);
		if(isset($filter['approved']))
			$approved_filter = $this->db->placehold("AND (c.approved=? $ip)", intval($filter['approved']));
			
		if($limit)
			$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);
		else
			$sql_limit = '';

		if(!empty($filter['object_id']))
			$object_id_filter = $this->db->placehold('AND c.object_id in(?@)', (array)$filter['object_id']);

		if(!empty($filter['parent_id']))
			$parent_id_filter = $this->db->placehold('AND c.parent_id=?', $filter['parent_id']);

		if(!empty($filter['type']))
			$type_filter = $this->db->placehold('AND c.type=?', $filter['type']);
            
        if(!empty($filter['main_page']))
			$main_page_filter = $this->db->placehold('AND c.on_main_page=?', $filter['main_page']);    

		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND c.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR c.text LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" ');
		}

			
		$sort='DESC';
		
		$query = $this->db->placehold("SELECT * FROM __comments c WHERE 1 $object_id_filter $parent_id_filter $type_filter $rateme_filter $keyword_filter $approved_filter $main_page_filter ORDER BY c.date $sort $sql_limit");
	
		$this->db->query($query);
		$comments = $this->db->results();

		$tree = new stdClass();
		$tree->subcomments = array();
		
		// Указатели на узлы дерева
		$pointers = array();
		$pointers[0] = &$tree;
		$pointers[0]->path = array();
    
		$finish = false;
		// Не кончаем, пока не кончатся категории, или пока ниодну из оставшихся некуда приткнуть
		while(!empty($comments)  && !$finish)
		{
			$flag = false;
			// Проходим все выбранные категории
			foreach($comments as $k=>$category)
			{
				if(isset($pointers[$category->parent_id]))
				{
					// В дерево категорий (через указатель) добавляем текущую категорию
					$pointers[$category->id] = $pointers[$category->parent_id]->subcomments[] = $category;
					
					// Путь к текущей категории
					$curr = $pointers[$category->id];

					// Убираем использованную категорию из массива категорий
					unset($comments[$k]);
					$flag = true;
				}
			}
			if(!$flag) $finish = true;
		}
		
		// Для каждой категории id всех ее деток узнаем
		$ids = array_reverse(array_keys($pointers));
		foreach($ids as $id)
		{
			if($id>0)
			{
				$pointers[$id]->children[] = $id;

				if(isset($pointers[$pointers[$id]->parent_id]->children))
					$pointers[$pointers[$id]->parent_id]->children = array_merge($pointers[$id]->children, $pointers[$pointers[$id]->parent_id]->children);
				else
					$pointers[$pointers[$id]->parent_id]->children = $pointers[$id]->children;
			}
		}
		unset($pointers[0]);
		unset($ids);

		$this->comments_tree = $tree->subcomments;
		$this->all_comments = $pointers;        
    
     return  $this->comments_tree;
	}  
	
	// Количество комментариев, удовлетворяющих фильтру
	public function count_comments($filter = array())
	{	
		$object_id_filter = '';
		$type_filter = '';
		$approved_filter = '';
		$keyword_filter = '';
        $parent_id_filter = '';
        $main_page_filter = '';
		$rateme_filter = '';

		
				if(isset($filter['rateme']))
			$rateme_filter  = $this->db->placehold("AND (c.rate=?)", intval($filter['rateme']));
	
		if(!empty($filter['object_id']))
			$object_id_filter = $this->db->placehold('AND c.object_id in(?@)', (array)$filter['object_id']);

		if(!empty($filter['type']))
			$type_filter = $this->db->placehold('AND c.type=?', $filter['type']);

		if(isset($filter['approved']))
			$approved_filter = $this->db->placehold('AND c.approved=?', intval($filter['approved']));
            
        if(!empty($filter['main_page']))
			$main_page_filter = $this->db->placehold('AND c.on_main_page=?', $filter['main_page']);    

		if(!empty($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND c.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR c.text LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" ');
		}

		$query = $this->db->placehold("SELECT count(distinct c.id) as count
										FROM __comments c WHERE 1 $object_id_filter $parent_id_filter $type_filter $keyword_filter $rateme_filter $approved_filter $main_page_filter", $this->settings->date_format);
	
		$this->db->query($query);	
		return $this->db->result('count');

	}
	
	// Добавление комментария
	public function add_comment($comment)
	{	
		$query = $this->db->placehold('INSERT INTO __comments
		SET ?%,
		date = NOW()',
		$comment);

		if(!$this->db->query($query))
			return false;

		$id = $this->db->insert_id();
		return $id;
	}
	
	// Изменение комментария
	public function update_comment($id, $comment)
	{
		$date_query = '';
		//if(isset($comment->date))
		//{
		//	$date = $comment->date;
		//	unset($comment->date);
		//	$date_query = $this->db->placehold(', date=STR_TO_DATE(?, ?)', $date, $this->settings->date_format);
		//}
		$query = $this->db->placehold("UPDATE __comments SET ?% $date_query WHERE id in(?@) LIMIT 1", $comment, (array)$id);
		$this->db->query($query);
		return $id;
	}

	// Удаление комментария
	public function delete_comment($id)
	{
		if(!empty($id))
		{
			$comment = $this->comments->get_comment(intval($id));
			$product = $this->products->get_product(intval($comment->object_id)); 
			$product_id = intval($product->id);				
			$rate = ($product->rating * $product->votes - $comment->rating) / ($product->votes - 1);
            $query = $this->db->placehold("UPDATE __products SET rating = ?, votes = votes - 1 WHERE id = ?", $rate, $product_id);
            $this->db->query($query);
		
			$query = $this->db->placehold("DELETE FROM __comments WHERE id=? LIMIT 1", intval($id));
			$this->db->query($query);
			$query = $this->db->placehold("DELETE FROM __comments WHERE parent_id=?", intval($id));
			$this->db->query($query);      
		}
	}	
}
