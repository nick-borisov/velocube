<?php
//ini_set('display_errors',1);
/**
 * Simpla CMS
 *
 * @copyright	2012 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Coupons extends Simpla
{

	/*
	*
	* Функция возвращает купон по его id или url
	* (в зависимости от типа аргумента, int - id, string - code)
	* @param $id id или code купона
	*
	*/
	public function get_coupon($id)
	{
		if(gettype($id) == 'string')
			$where = $this->db->placehold('WHERE c.code=? AND c.auto_discount!=1', $id);
		else
			$where = $this->db->placehold('WHERE c.id=? ', $id);
		
		$query = $this->db->placehold("SELECT c.id, c.code, c.name, c.value, c.type, c.expire, min_order_price, c.single, c.usages,	((DATE(NOW()) <= DATE(c.expire) OR c.expire IS NULL) AND (c.usages=0 OR NOT c.single)) AS valid, c.auto_discount, c.annotation, c.position, c.ad_active, c.active_margin, c.margin, c.active
		                               FROM __coupons c $where LIMIT 1");
		if($this->db->query($query))
			return $this->db->result();
		else
			return false; 
	}
	
	/*
	*
	* Функция возвращает массив купонов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function get_coupons($filter = array())
	{	
		// По умолчанию
		$limit = 100;
		$page = 1;
		$coupon_id_filter = '';
		$valid_filter = '';
		$keyword_filter = '';
        $category_id_filter = '';
        $brand_id_filter = '';
        $group_by = '';
        $auto_discount_filter = '';
        $related_product_id_filter = '';
        $stop_related_product_id_filter = '';
        $ad_active_filter = '';

		
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		if(!empty($filter['id']))
			$coupon_id_filter = $this->db->placehold('AND c.id in(?@)', (array)$filter['id']);
        
        if(!empty($filter['active']))
			$active_filter = $this->db->placehold('AND c.active=1');    
            
        if(isset($filter['auto_discount']))
			$auto_discount_filter = $this->db->placehold('AND c.auto_discount=?', (integer)$filter['auto_discount']);
            
        if(!empty($filter['schedule']) || !empty($filter['schedule_yandex']) || !empty($filter['schedule_priceru']) || !empty($filter['ad_active'])){
            $ad_active_filter = $this->db->placehold("AND c.ad_active=1");
        }
        
		if(isset($filter['valid']))
			if($filter['valid'])
				$valid_filter = $this->db->placehold('AND ((DATE(NOW()) <= DATE(c.expire) OR c.expire IS NULL) AND (c.usages=0 OR NOT c.single))');		
			else
				$valid_filter = $this->db->placehold('AND NOT ((DATE(NOW()) <= DATE(c.expire) OR c.expire IS NULL) AND (c.usages=0 OR NOT c.single))');		
        
        if(!empty($filter['category_id']))
		{
			$category_id_filter = $this->db->placehold('INNER JOIN __categories_coupons cc ON cc.coupon_id = c.id AND cc.category_id in(?@)', (array)$filter['category_id']);
			$group_by = "GROUP BY c.id";
		}
        
        if(!empty($filter['brand_id']))
            $brand_id_filter = $this->db->placehold('INNER JOIN __brands_coupons bc ON bc.coupon_id = c.id AND bc.brand_id=?', $filter['brand_id']);
            
        if(!empty($filter['related_product_id']))
            $related_product_id_filter = $this->db->placehold('INNER JOIN __related_products_coupons rpc ON rpc.coupon_id = c.id AND rpc.product_id in (?@)', (array)$filter['related_product_id']);
        
        if(!empty($filter['stop_related_product_id']))    
            $stop_related_product_id_filter =     $this->db->placehold('INNER JOIN __related_products_coupons_stop rpcs ON rpcs.coupon_id = c.id AND rpcs.product_id in (?@)', (array)$filter['stop_related_product_id']);
        
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		$query = $this->db->placehold("SELECT c.id, c.code, c.name, c.value, c.type, c.expire, min_order_price, c.single, c.usages,
										((DATE(NOW()) <= DATE(c.expire) OR c.expire IS NULL) AND (c.usages=0 OR NOT c.single)) AS valid, c.auto_discount, c.annotation, c.position, c.ad_active, c.active_margin, c.margin, c.active
		                                      FROM __coupons c
                                              $category_id_filter
                                              $brand_id_filter
                                              $related_product_id_filter
                                              $stop_related_product_id_filter
                                              WHERE 1 
                                              $coupon_id_filter 
                                              $valid_filter 
                                              $keyword_filter
		                                      $auto_discount_filter                      
                                              $ad_active_filter
                                              $yandex_filter
                                              $active_filter
                                              
                                              $group_by ORDER BY valid, c.position DESC, id DESC $sql_limit",
		                                      $this->settings->date_format);
//if($_SESSION['admin'])print($query);
		$this->db->query($query);
        
        $result = $this->db->results();
        
        if(!empty($filter['schedule'])){
            $coupons_result = $this->check_timeset($result);
                return $coupons_result;
        }
        
        elseif(!empty($filter['schedule_yandex'])){
            $coupons_result = $this->check_timeset_yandex($result);
                return $coupons_result;
        }
        
        elseif(!empty($filter['schedule_priceru'])){
            $coupons_result = $this->check_timeset_priceru($result);
                return $coupons_result;
        }
        
		return $result;
	}
	
	
	/*
	*
	* Функция вычисляет количество постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function count_coupons($filter = array())
	{	
		$coupon_id_filter = '';
		$valid_filter = '';
        $category_id_filter = '';
        $brand_id_filter = '';
        $auto_discount_filter = '';
        $related_product_id_filter = '';
        $stop_related_product_id_filter = '';
		
		if(!empty($filter['id']))
			$coupon_id_filter = $this->db->placehold('AND c.id in(?@)', (array)$filter['id']);
            
        if(isset($filter['auto_discount']))
			$auto_discount_filter = $this->db->placehold('AND c.auto_discount=?', (integer)$filter['auto_discount']);
			
		if(isset($filter['valid']))
			$valid_filter = $this->db->placehold('AND ((DATE(NOW()) <= DATE(c.expire) OR c.expire IS NULL) AND (c.usages=0 OR NOT c.single))');	
            
        if(!empty($filter['category_id']))
		{
			$category_id_filter = $this->db->placehold('INNER JOIN __categories_coupons cc ON cc.coupon_id = c.id AND cc.category_id in(?@)', (array)$filter['category_id']);
		}
        
        if(!empty($filter['brand_id']))
            $brand_id_filter = $this->db->placehold('INNER JOIN __brands_coupons bc ON bc.coupon_id = c.id AND bc.brand_id=?', $filter['brand_id']);
        
        if(!empty($filter['related_product_id']))
            $related_product_id_filter = $this->db->placehold('INNER JOIN __related_products_coupons rpc ON rpc.coupon_id = c.id AND rpc.product_id in (?@)', (array)$filter['related_product_id']);
        
        if(!empty($filter['stop_related_product_id']))    
            $stop_related_product_id_filter = $this->db->placehold('INNER JOIN __related_products_coupons_stop rpcs ON rpcs.coupon_id = c.id AND rpcs.product_id in (?@)', (array)$filter['stop_related_product_id']);
        
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}
		
		$query = "SELECT COUNT(distinct c.id) as count
		          FROM __coupons c
                  $category_id_filter
                  $brand_id_filter
                  $related_product_id_filter
                  $stop_related_product_id_filter
                  WHERE 1 
                  $coupon_id_filter $valid_filter $auto_discount_filter ";

		if($this->db->query($query))
			return $this->db->result('count');
		else
			return false;
	}
	
	/*
	*
	* Создание купона
	* @param $coupon
	*
	*/	
	public function add_coupon($coupon)
	{	
		if(empty($coupon->single))
			$coupon->single = 0;
        
		$date = date("Y-m-d H:i:s", time());
		$date_query = $this->db->placehold(', created=NOW()');

		$query = $this->db->placehold("INSERT INTO __coupons SET ?% $date_query", $coupon);
		
		if(!$this->db->query($query))
			return false;
		else
			return $this->db->insert_id();
	}
	
	
	/*
	*
	* Обновить купон(ы)
	* @param $id, $coupon
	*
	*/	
	public function update_coupon($id, $coupon)
	{
		$query = $this->db->placehold("UPDATE __coupons SET ?% WHERE id in(?@) LIMIT ?", $coupon, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}


	/*
	*
	* Удалить купон
	* @param $id
	*
	*/	
	public function delete_coupon($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __coupons WHERE id=? LIMIT 1", intval($id));
			$this->db->query($query);
            $query = $this->db->placehold("DELETE FROM __categories_coupons WHERE coupon_id=?", intval($id));
			$this->db->query($query);
            $query = $this->db->placehold("DELETE FROM __brands_coupons WHERE coupon_id=?", intval($id));
			$this->db->query($query);
            return $id;
		}
	}
    
    function get_coupon_categories($id)
	{
		$query = $this->db->placehold("SELECT cc.category_id as category_id FROM __categories_coupons cc
										WHERE cc.coupon_id = ?", $id);
                                        //print($query);
		$this->db->query($query);
		$categories = $this->db->results('category_id');
        
        //print_r($this->categories->get_categories());
        return 	$categories;
	}
    
    public function add_coupon_category($id, $category_id)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __categories_coupons SET coupon_id=?, category_id=?", $id, $category_id);
		$this->db->query($query);
	}
			
	public function update_coupon_categories($id, $categories)
	{
		$id = intval($id);
		$query = $this->db->placehold("DELETE FROM __categories_coupons WHERE coupon_id=?", $id);
		$this->db->query($query);
		
		
		if(is_array($categories))
		{
			$values = array();
			foreach($categories as $category)
				$values[] = "($id , ".intval($category).")";
	
			$query = $this->db->placehold("INSERT INTO __categories_coupons (coupon_id, category_id) VALUES ".implode(', ', $values));
			$this->db->query($query);
		}
	}	
	
    function get_coupon_brands($id)
	{
		$query = $this->db->placehold("SELECT cb.brand_id as brand_id FROM __brands_coupons cb
										WHERE cb.coupon_id = ?", $id);
		$this->db->query($query);
		return $this->db->results('brand_id');	
	}
    
    public function add_coupon_brand($id, $brand_id)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __brands_coupons SET coupon_id=?, brand_id=?", $id, $brand_id);
		$this->db->query($query);
	}
			
	public function update_coupon_brands($id, $brands)
	{
		$id = intval($id);
		$query = $this->db->placehold("DELETE FROM __brands_coupons WHERE coupon_id=?", $id);
		$this->db->query($query);
		
		
		if(is_array($brands))
		{
			$values = array();
			foreach($brands as $brand)
				$values[] = "($id , ".intval($brand).")";
	
			$query = $this->db->placehold("INSERT INTO __brands_coupons (coupon_id, brand_id) VALUES ".implode(', ', $values));
			$this->db->query($query);
		}
	}
    
   	function get_related_products($product_id = array())
	{
		if(empty($product_id))
			return array();

		$product_id_filter = $this->db->placehold('AND coupon_id in(?@)', (array)$product_id);
				
		$query = $this->db->placehold("SELECT product_id, coupon_id
					FROM __related_products_coupons
					WHERE 
					1
					$product_id_filter   
					ORDER BY product_id     
					");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
	// 
	public function add_related_product($product_id, $related_id)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __related_products_coupons SET coupon_id=?, product_id=?", $product_id, $related_id);
		$this->db->query($query);
		return $related_id;
	}
	
	// 
	public function delete_related_product($product_id, $related_id)
	{
		$query = $this->db->placehold("DELETE FROM __related_products_coupons WHERE coupon_id=? AND product_id=? LIMIT 1", intval($product_id), intval($related_id));
		$this->db->query($query);
	}
    
   	function get_related_products_stop($product_id = array())
	{
		if(empty($product_id))
			return array();

		$product_id_filter = $this->db->placehold('AND coupon_id in(?@)', (array)$product_id);
				
		$query = $this->db->placehold("SELECT product_id, coupon_id
					FROM __related_products_coupons_stop
					WHERE 
					1
					$product_id_filter   
					ORDER BY product_id     
					");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
	// 
	public function add_related_product_stop($product_id, $related_id)
	{
		$query = $this->db->placehold("INSERT IGNORE INTO __related_products_coupons_stop SET coupon_id=?, product_id=?", $product_id, $related_id);
		$this->db->query($query);
		return $related_id;
	}
    
    //для автокупона
    public function calc_product_coupon($product_id, $variant_id=null, $shedule = 'schedule'){
        //auto_discount
        $product = $this->products->get_product(intval($product_id));
        if($variant_id){
            $variant = $this->variants->get_variant(intval($variant_id));
            $product->variant = $variant;
        }else{
            $variants = $this->variants->get_variants(array('product_id'=>$product_id));
            $product->variant = reset($variants);
        }
        
        $categories = $this->categories->get_product_categories($product_id); 
        foreach($categories as $cat){
            $all_prod_categories[] = $cat->category_id;
        }
        //if($_SESSION['admin']) var_dump($all_prod_categories);
        //$category = reset($categories);
//        foreach($category->path as $cp){
//            if($cp->parent_id == 0){
//                $root_category_children = $cp->children;
//            }
//        }
        

        $cat_coupons = array();
        $brand_coupons = array();
        $no_brand_coupons = array();

        if($product->brand_id){
            $all_cat_coupons = $this->get_coupons(array('auto_discount'=>1, 'ad_active'=>1, $shedule=>1, 'category_id'=>$all_prod_categories));
            
            foreach($all_cat_coupons as $c_coupon){
                $kb_coupons = $this->get_coupon_brands($c_coupon->id);
                //if($_SESSION['admin']) print("<!--");var_dump($all_prod_categories);print("-->");
                if(empty($kb_coupons[0])){
                    $cat_coupons[$c_coupon->id] = $c_coupon;
                }else{
                    if(in_array($product->brand_id, $kb_coupons)){
                        $cat_coupons[$c_coupon->id] = $c_coupon;
                    }
                }
            }
            
            $all_brand_coupons = $this->get_coupons(array('auto_discount'=>1, 'ad_active'=>1, $shedule=>1, 'brand_id'=>$product->brand_id));
            foreach($all_brand_coupons as $abc){
                $is_have_cats = $this->get_coupon_categories($abc->id);
                if(empty($is_have_cats[0])){
                    $brand_coupons[$abc->id] = $abc;
                }
            }
        }else{
            $no_brand_coupons = $this->get_coupons(array('auto_discount'=>1, 'ad_active'=>1, $shedule=>1, 'category_id'=>$all_prod_categories));
        } 
        //$ad_coupons_products = array();
        //print('<!--');var_dump($all_coupons);print('-->');
        $ad_coupons_products = $this->get_coupons(array('auto_discount'=>1, 'ad_active'=>1, $shedule=>1, 'related_product_id'=>$product->id));

        $selected = $ad_coupons_products+$cat_coupons+$brand_coupons+$no_brand_coupons;
        
        $coupons_to_unset = array();
        foreach($selected as $scoupon){
            $stop_related_product = $this->get_related_products_stop($scoupon->id);
            foreach($stop_related_product as $stprod){
                if($product->id == $stprod->product_id){
                    $coupons_to_unset[$stprod->coupon_id] = $stprod->coupon_id;
                }
            }
            if(in_array($product->id, $stop_related_product)){
                $coupons_to_unset[] = $scoupon->id;
            }
        }
        foreach($coupons_to_unset as $ctu){
            unset($selected[$ctu]);
        }
        foreach($selected as $ad_prod){
            //если есть интервалы для цены, работаем с ними
            if($ad_prod->active_margin){
                $margins = unserialize($ad_prod->margin);
				if($margins['min'][0] && $margins['max'][0]){  
                    foreach($margins['min'] as $i=>$min){
                        if($product->variant->price > intval($margins['min'][$i]) && $product->variant->price < intval($margins['max'][$i])){
                            if(intval($margins['mode'][$i])==1){
                                $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
                            }elseif(intval($margins['mode'][$i])==2){
                                $coupons[$ad_prod->id] = round($product->variant->price * $margins['price'][$i] / 100, 2); 
                            }
                        }
                    }
				}
            }else{//если интервалов нет
                if($ad_prod->type == 'percentage')
                    $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
                elseif($ad_prod->type == 'absolute')
                    $coupons[$ad_prod->id] = floatval($ad_prod->value);
            }
        }
        //выбираем наибольшую скидку, получаем выбранный купон
        arsort($coupons);
            
        return $coupons;
    }
    
    /*получение массива всех подходящих ОБЫЧНЫХ купонов к товару*/
    public function calc_coupon($product_id, $variant_id=null, $amount=1){
        
        $product = $this->products->get_product(intval($product_id));
        if($variant_id){
            $variant = $this->variants->get_variant(intval($variant_id));
            $product->variant = $variant;
        }else{
            $variants = $this->variants->get_variants(array('product_id'=>$product_id));
            $product->variant = reset($variants);
        }
            
        
        $categories = $this->categories->get_product_categories($product_id); 
        foreach($categories as $cat){
            $all_prod_categories[] = $cat->category_id;
        }

        $all_coupons = array();
        $cat_coupons = array();
        $brand_coupons = array();
        if($product->brand_id){
            $all_cat_coupons = $this->get_coupons(array('auto_discount'=>0, 'active'=>1, 'category_id'=>$all_prod_categories));
            
            foreach($all_cat_coupons as $c_coupon){
                $kb_coupons = $this->get_coupon_brands($c_coupon->id);
                
                if(empty($kb_coupons[0])){
                    $cat_coupons[$c_coupon->id] = $c_coupon;
                }else{
                    if(in_array($product->brand_id, $kb_coupons)){
                        $cat_coupons[$c_coupon->id] = $c_coupon;
                    }
                }
            }
            
            $all_brand_coupons = $this->get_coupons(array('auto_discount'=>0, 'active'=>1, 'brand_id'=>$product->brand_id));
            foreach($all_brand_coupons as $abc){
                $is_have_cats = $this->get_coupon_categories($abc->id);
                if(empty($is_have_cats[0])){
                    $brand_coupons[$abc->id] = $abc;
                }
            }
        }else{
            $all_coupons = $this->get_coupons(array('auto_discount'=>0, 'active'=>1, 'category_id'=>$all_prod_categories));
        } 
        $ad_coupons_products = array();   
        $ad_coupons_products = $this->get_coupons(array('auto_discount'=>0, 'active'=>1, 'related_product_id'=>$product->id));

        $selected = $ad_coupons_products+$cat_coupons+$brand_coupons;
//if($_SESSION['admin']){var_dump($product->id);}
        $coupons_to_unset = array();
        foreach($selected as $scoupon){
            $stop_related_product = $this->get_related_products_stop($scoupon->id);
            foreach($stop_related_product as $stprod){
                if($product->id == $stprod->product_id){
                    $coupons_to_unset[$stprod->coupon_id] = $stprod->coupon_id;
                }
            }
            if(in_array($product->id, $stop_related_product)){
                $coupons_to_unset[] = $scoupon->id;
            }
        }
        foreach($coupons_to_unset as $ctu){
            unset($selected[$ctu]);
        }
        foreach($selected as $ad_prod){
            if($ad_prod->type == 'percentage')
                    $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0)*$amount;
                elseif($ad_prod->type == 'absolute')
                    $coupons[$ad_prod->id] = floatval($ad_prod->value)*$amount; 
        }
        //arsort($coupons);
        return $coupons;
    }
	
	// 
	public function delete_related_product_stop($product_id, $related_id)
	{
		$query = $this->db->placehold("DELETE FROM __related_products_coupons_stop WHERE coupon_id=? AND product_id=? LIMIT 1", intval($product_id), intval($related_id));
		$this->db->query($query);
	}
    
    //Get coupon timeset
    function get_coupon_timeset($coupon_id, $yandex=null, $priceru=null)
	{
		if(empty($coupon_id))
			return array();
        
        $yandex_filter = $this->db->placehold("AND yandex IS NULL AND priceru IS NULL");;
        
        if($yandex){
            $yandex_filter = $this->db->placehold("AND yandex=1");
        }
        if($priceru){
            $yandex_filter = $this->db->placehold("AND priceru=1");
        }
        
		$coupon_id_filter = $this->db->placehold('AND coupon_id in(?@)', (array)$coupon_id);
				
		$query = $this->db->placehold("SELECT *
					FROM __coupon_timesets
					WHERE 
					1
					$coupon_id_filter
                    $yandex_filter   
					ORDER BY coupon_id     
					");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
    //Add coupon timeset
    public function add_coupon_timeset($timeset)
	{
        if(is_object($timeset)){
    		$query = $this->db->placehold("INSERT IGNORE INTO __coupon_timesets SET ?%", $timeset);
    		$this->db->query($query);
    		return true;
        }
        return false;
	}
    
    //Delete coupon timeset
    public function delete_coupon_timeset($coupon_id)
	{
		$query = $this->db->placehold("DELETE FROM __coupon_timesets WHERE coupon_id=?", intval($coupon_id));
		$this->db->query($query);
	}
    
    private function check_timeset($coupons){
        if(is_array($coupons)){
            $result = array();
            foreach($coupons as $coupon){
                $this->db->query('SELECT * FROM s_coupon_timesets WHERE coupon_id = ? AND yandex IS NULL AND priceru IS NULL', $coupon->id);
                $timesets[$coupon->id] = $this->db->results();
            }

            foreach($coupons as $coupon)
                $coupons_result[$coupon->id] = $coupon;
            
            foreach($timesets as $k=>$coupon_id){
                foreach($coupon_id as $timeset){
                    if(in_array(strtoupper(date(l, mktime())), explode(',', $timeset->week_days))){
                        if(intval($timeset->from_hour) < intval($timeset->to_hour)){
                            if(intval(date(Hi, mktime())) >= intval($timeset->from_hour.$timeset->from_minute) && intval(date(Hi, mktime())) <= intval($timeset->to_hour.$timeset->to_minute)){
                                $result[$k] = $coupons_result[$timeset->coupon_id];
                            }
                         }else{
                            if(intval(date(Hi, mktime())) >= intval($timeset->from_hour.$timeset->from_minute) || intval(date(Hi, mktime())) <= intval($timeset->to_hour.$timeset->to_minute)){
                                $result[$k] = $coupons_result[$timeset->coupon_id];
                            }
                         }   
                    }
                }
            }
            return $result;
        }
        return false;
    }	
    
    private function check_timeset_yandex($coupons){
        if(is_array($coupons)){
            $result = array();
            foreach($coupons as $coupon){
                $this->db->query('SELECT * FROM s_coupon_timesets WHERE coupon_id = ? AND yandex = 1', $coupon->id);
                $timesets[$coupon->id] = $this->db->results();
            }

            foreach($coupons as $coupon)
                $coupons_result[$coupon->id] = $coupon;
            
            foreach($timesets as $k=>$coupon_id){
                foreach($coupon_id as $timeset){
                    if(in_array(strtoupper(date(l, mktime())), explode(',', $timeset->week_days))){
                        if(intval($timeset->from_hour) <= intval($timeset->to_hour)){
                            if(intval(date(Hi, mktime())) >= intval($timeset->from_hour.$timeset->from_minute) && intval(date(Hi, mktime())) <= intval($timeset->to_hour.$timeset->to_minute)){
                                $result[$k] = $coupons_result[$timeset->coupon_id];
                            }
                         }else{
                            if(intval(date(Hi, mktime())) >= intval($timeset->from_hour.$timeset->from_minute) || intval(date(Hi, mktime())) <= intval($timeset->to_hour.$timeset->to_minute)){
                                $result[$k] = $coupons_result[$timeset->coupon_id];
                            }
                         }   
                    }
                }
            }
            return $result;
        }
        return false;
    }	
    
    private function check_timeset_priceru($coupons){
        if(is_array($coupons)){
            $result = array();
            foreach($coupons as $coupon){
                $this->db->query('SELECT * FROM s_coupon_timesets WHERE coupon_id = ? AND priceru = 1', $coupon->id);
                $timesets[$coupon->id] = $this->db->results();
            }

            foreach($coupons as $coupon)
                $coupons_result[$coupon->id] = $coupon;
            
            foreach($timesets as $k=>$coupon_id){
                foreach($coupon_id as $timeset){
                    if(in_array(strtoupper(date(l, mktime())), explode(',', $timeset->week_days))){
                        if(intval($timeset->from_hour) < intval($timeset->to_hour)){
                            if(intval(date(Hi, mktime())) >= intval($timeset->from_hour.$timeset->from_minute) && intval(date(Hi, mktime())) <= intval($timeset->to_hour.$timeset->to_minute)){
                                $result[$k] = $coupons_result[$timeset->coupon_id];
                            }
                         }else{
                            if(intval(date(Hi, mktime())) >= intval($timeset->from_hour.$timeset->from_minute) || intval(date(Hi, mktime())) <= intval($timeset->to_hour.$timeset->to_minute)){
                                $result[$k] = $coupons_result[$timeset->coupon_id];
                            }
                         }   
                    }
                }
            }
            return $result;
        }
        return false;
    }	
}
