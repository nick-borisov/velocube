<?php
	require_once('api/Simpla.php');
	$simpla = new Simpla();
  
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'http://manager.marketmonster.ru/api/21165908/eKvWsdJmEP/get_prices');
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$x = curl_exec($ch);
	curl_close($ch);
	unset($ch);

	$x = json_decode($x, true);
	//print_r($x);
	if(!$x) die('Fail to decode json'."\n");

	if(isset($x['err'])) die('Marketmonster returned error: '.$x['err']."\n");

	foreach($x['res'] as $product_id=>$price){
		//set $price for $product_id
		$simpla->variants->update_variant($product_id, array('price'=>$price));
	}
?>