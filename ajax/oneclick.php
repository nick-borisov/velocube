<?php
	session_start();
	chdir('..');
    require_once('api/Simpla.php');
    $simpla = new Simpla();
	
	$variant_id = $simpla->request->post('variant', 'integer');
	$amount = $simpla->request->post('amount', 'integer');

	$order = new StdClass;
	$order->name = $simpla->request->post('name', 'string');
	$order->phone = $simpla->request->post('phone', 'string');
	$order->comment = $simpla->request->post('comment', 'string');
	$order->ip = $_SERVER['REMOTE_ADDR'];
	
	// добавляем заказ
	$order_id = $simpla->orders->add_order($order);
	
	// добавляем товар в заказ
	$simpla->orders->add_purchase(array('order_id'=>$order_id, 'variant_id'=>intval($variant_id), 'amount'=>intval($amount)));

	// Выбираем заказ
	$order = $simpla->orders->get_order($order_id);

	// отправляем письмо администратору
	$simpla->notify->email_order_admin($order_id);	

	// Отправка смс уведомления пользователю если он этого запросил
	$simpla->smssend->send($order->phone, 'Ваш заказ №'.$order->id.' на сумму '.$order->total_price.'. В ближайшее время с Вами свяжется менеджер. Спасибо за покупку! Velocube.ru');

	// Отправка смс уведомления администратору о поступившем заказе 
	//$simpla->smssend->send('+номерадмина', 'На Velocube.ru поступил быстрый заказ №'.$order->id.' на сумму '.$order->total_price.' руб');