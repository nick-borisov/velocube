<?php
	session_start();
	chdir('..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();
  if($simpla->request->get('mode', 'string')=='update')
	$simpla->cart->update_item($simpla->request->get('variant', 'integer'), $simpla->request->get('amount', 'integer'));
  if($simpla->request->get('mode', 'string')=='remove')
	$simpla->cart->delete_item($simpla->request->get('variant', 'integer'));
  if($simpla->request->get('mode', 'string')=='add' || !$simpla->request->get('mode')){
        $_SESSION['auto_coupon'][$simpla->request->get('variant', 'integer')] = $simpla->request->get('auto_coupon', 'integer');
        //if($_SESSION['admin']) var_dump($_SESSION); 
        $simpla->cart->add_item($simpla->request->get('variant', 'integer'), $simpla->request->get('amount', 'integer'));
    }     
	$cart = $simpla->cart->get_cart();
	$simpla->design->assign('cart', $cart);
	
	$currencies = $simpla->money->get_currencies(array('enabled'=>1));
	if(isset($_SESSION['currency_id']))
		$currency = $simpla->money->get_currency($_SESSION['currency_id']);
	else
		$currency = reset($currencies);

	$simpla->design->assign('currency',	$currency);
	
	$result = $simpla->design->fetch('cart_informer.tpl');
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
