<?php
chdir('/var/www/velocube.ru/');
include_once('api/Simpla.php');

$simpla = new Simpla();

$status = 'status negative '.date('d.m.Y H:i:s', mktime())."\n";
$dir = 'simpla/files/backup/';
$filename = $dir.'simpla_'.date("Y_m_d_G_i_s").'.sql';
if($simpla->db->dump($filename)){
    $status = 'status Success '.date('d.m.Y H:i:s', mktime())."\n";
    $unlink = '';
    $files = scandir($dir);

    foreach($files as $file){
        if((time()-filemtime($dir.$file)) > 60*60*24*7){
            $unlink = "\n delete file ".$dir.$file;
            unlink($dir.$file);
        }
    }
}
file_put_contents('auto_backup_log.txt', $status.$filename."\n----------------------------------------\n\n", FILE_APPEND);