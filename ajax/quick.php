<?php

	session_start();
	chdir('..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();
	$product_id = $simpla->request->get('id', 'integer');
	
	
	
	$product = $simpla->products->get_product($product_id);
	
	$product->images = $simpla->products->get_images(array('product_id'=>$product->id));
		$product->image = reset($product->images);

		$variants = array();
		foreach($simpla->variants->get_variants(array('product_id'=>$product->id, 'in_stock'=>true, 'order'=>'stock')) as $v)
			$variants[$v->id] = $v;
		
		$product->variants = $variants;
		
		// ������� �� ���������
		if(($v_id = $simpla->request->get('variant', 'integer'))>0 && isset($variants[$v_id]))
			$product->variant = $variants[$v_id];
		else
			$product->variant = reset($variants);
					
		$product->features = $simpla->features->get_product_options(array('product_id'=>$product->id));
        
        /*auto_discount*/
        $product = $simpla->products->calc_auto_discount($product);
        /*auto_discount*/
	
	$simpla->design->assign('product', $product);
	
    /*user_groups*/
    $user = $simpla->users->get_user(intval($_SESSION['user_id']));
    $simpla->design->assign('user', $user);
    if($user->group_id)
        $simpla->design->assign('user_group', $simpla->users->get_group($user->group_id));
    
	$currencies = $simpla->money->get_currencies(array('enabled'=>1));
	if(isset($_SESSION['currency_id']))
		$currency = $simpla->money->get_currency($_SESSION['currency_id']);
	else
		$currency = reset($currencies);

	$simpla->design->assign('currency',	$currency);
	
	$result = $simpla->design->fetch('quick.tpl');
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
