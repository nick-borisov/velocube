<?php
ini_set('display_errors',1);
	chdir('..');
    require_once('api/Simpla.php');
	$simpla = new Simpla();
    
    $callback = new StdClass();

    $callback->name = $simpla->request->post('name', 'string');
    $callback->phone = $simpla->request->post('phone', 'string');
    $callback->message = $simpla->request->post('message', 'string');
    $callback->call_me_intime = $simpla->request->post('call_me_intime', 'boolean');
    $callback->time_from = $simpla->request->post('time_from', 'integer');
    $callback->time_to = $simpla->request->post('time_to', 'integer');
    $callback->url = $_SERVER['HTTP_REFERER'];
    $callback_id = $simpla->callbacks->add_callback($callback);
    
    /*RECAPTCHA*/
	// $recaptha_response = $simpla->request->post('g-recaptcha-response');
 //    $remoteip = $_SERVER['REMOTE_ADDR'];
 //    $recaptcha_status = json_decode($simpla->recaptcha->send($recaptha_response, $remoteip));
 //    //var_dump(!$recaptcha_status->success);die;

	// if(empty($recaptha_response) || !$recaptcha_status->success)
	// {
	// 	$simpla->design->assign('error', 'captcha');
	// }
    /*RECAPTCHA*/
    
    $simpla->callbacks->email_callback_admin($callback_id);
	
	$simpla->design->assign('callback_id',	$callback_id);
	$result = $simpla->design->fetch('callback_inform.tpl');
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		  
	print json_encode($result);
?>    