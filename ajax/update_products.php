<?php
	session_start();
	chdir('..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();
	// POST-Параметры
	 
		$category_id = $simpla->request->post('cat_id', 'integer');
		$brand_id	  = $simpla->request->post('brand_id');
		$mark_id    = $simpla->request->post('mark_id', 'integer');
        
        $text =  "$category_id";
		$filter = array();
		$filter['visible'] = 1;	
		//$filter['in_stock'] = 1;	
		$filter['brand_id'] = array();
	
		
	
		// Если задан бренд, выберем его из базы
		if (is_array($brand_id)) 
		{
			if (!empty($brand_id)) {
			$filter['brand_id'] = array_merge($filter['brand_id'], $brand_id);
			}
		}
		
	    else 
		{
		
		if (!empty($brand_id)) {
		
			$br = array();
			$br[] = $brand_id;
		
			$filter['brand_id'] = array_merge($filter['brand_id'], $br);
		}
		}
		
	    // Выберем текущую категорию
		if (!empty($category_id))
		{
			$category = $simpla->categories->get_category(intval($category_id));
			if (empty($category) || (!$category->visible && empty($_SESSION['admin'])))
				return false;
			//$simpla->design->assign('category', $category);
			$filter['category_id'] = $category->children;
		}
	
	

	    // Если задана метка, выберем ее из базы
		if (!empty($mark_id))
		{
		    
			$mark = $simpla->marks->get_mark(intval($mark_id));
			if (empty($mark))
				return false;
			//$simpla->design->assign('mark', $mark);
			$filter['mark_id'] = $mark->id;
			if ($mark->category != $category->id)return false;
		}
		
			// Если задано ключевое слово
		$keyword = $simpla->request->post('keyword');
		if (!empty($keyword))
		{
			$simpla->design->assign('keyword', $keyword);
			$filter['keyword'] = $keyword;
		}
		
		
		
		if ($simpla->request->post('featured', 'integer'))
			$filter['featured'] = 1;
			
		if ($simpla->request->post('discounted', 'integer'))
			$filter['discounted'] = 1;

		if ($simpla->request->post('min_price', 'integer'))
			$filter['min_price'] = $simpla->request->post('min_price', 'integer');
				
		if ($simpla->request->post('max_price'))
			$filter['max_price'] = $simpla->request->post('max_price', 'integer');
			
		// Сортировка товаров, сохраняем в сесси, чтобы текущая сортировка оставалась для всего сайта
		if($sort = $simpla->request->post('sort', 'string'))
			$_SESSION['sort'] = $sort;		
		if (!empty($_SESSION['sort']))
			$filter['sort'] = $_SESSION['sort'];			
		else
			$filter['sort'] = 'position';			
		$simpla->design->assign('sort', $filter['sort']);	
		
		// Свойства товаров
		if(!empty($category))
		{
			$features = array();
			$filter['features'] = array();
			foreach($simpla->features->get_features(array('category_id'=>$category->id, 'in_filter'=>1)) as $feature)
			{ 
				$features[$feature->id] = $feature;
				//if(($val = strval($simpla->request->get($feature->id)))!='')
				if(($val = $simpla->request->post($feature->id))!='')
				{
					if($val[0] != '')
						$filter['features'][$feature->id] = (array)$val;
								
					$features[$feature->id]->active = true;
				}
				else
				{
					$features[$feature->id]->active = false;
				}
			}
			
			$options_filter['visible'] = 1;
			$options_filter['mark_id'] = $filter['mark_id'];
			$features_ids = array_keys($features);
			if(!empty($features_ids))
				$options_filter['feature_id'] = $features_ids;
			$options_filter['category_id'] = $category->children;
			if(isset($filter['features']))
				$options_filter['features'] = $filter['features'];
						
			$options = $simpla->features->get_options($options_filter);

			if(isset($options)) {
				foreach($options as &$option) {
					// в этой групе есть чекнутый фильтер
					if($features[$option->feature_id]->active) {

						if( in_array($option->value, $filter['features'][$option->feature_id])){
							$option->checked = true;
							$option->disabled = false;
							$option->count = 0;
						}else{
							$temp_filter = $filter;
							$temp_filter['features'][$option->feature_id] = (array)$option->value;
							$option->count = '+'.$simpla->products->count_products($temp_filter);
								if((int)$option->count > 0){
									$option->disabled = false;
								}else{
									$option->disabled = true;
									$option->count = 0;
								}
									
							unset($temp_filter);
						}
					} else {
						$temp_filter = $filter;
						$temp_filter['features'][$option->feature_id] = (array)$option->value;
						$option->count = $simpla->products->count_products($temp_filter);
							if((int)$option->count > 0)
								$option->disabled = false;
							else
								$option->disabled = true;
						unset($temp_filter);
						$option->checked = false;
					}
						
					$features[$option->feature_id]->options[] = $option;
				}
			}
			
			
			
			foreach($features as $i=>&$feature)
			{ 
				if(empty($feature->options))
					unset($features[$i]);
			}

			//$simpla->design->assign('features', $features);
			
			//Минимальная и максимальная допустимая цена
			//$simpla->design->assign('max_min_price', $simpla->products->max_min_products($filter));	
			
			//Минимальная и максимальная допустимая цена
			//$simpla->design->assign('slider_max_min_price', $simpla->products->max_min_products(array('category_id'=>$filter['category_id'], 'mark_id'=>$filter['mark_id'],'visible'=>1)));	
 		}
	    
		// Постраничная навигация
		$items_per_page = $simpla->settings->products_num;		
		// Текущая страница в постраничном выводе
		$current_page = $simpla->request->post('page', 'int');	
		// Если не задана, то равна 1
		$current_page = max(1, $current_page);
		$simpla->design->assign('current_page_num', $current_page);
		// Вычисляем количество страниц
		$products_count = $simpla->products->count_products($filter);
		
		// Показать все страницы сразу
		if($simpla->request->post('page') == 'all')
			$items_per_page = $products_count;	
		
		$pages_num = ceil($products_count/$items_per_page);
		$simpla->design->assign('total_pages_num', $pages_num);
		$simpla->design->assign('total_products_num', $products_count);

		$filter['page'] = $current_page;
		$filter['limit'] = $items_per_page;
		
		///////////////////////////////////////////////
		// Постраничная навигация END
		///////////////////////////////////////////////
		
		$discount = 0;
		if(isset($_SESSION['user_id']) && $user = $simpla->users->get_user(intval($_SESSION['user_id'])))
			$discount = $user->discount;
			
		// Товары 
		$products = array();
		foreach($simpla->products->get_products($filter) as $p)
			$products[$p->id] = $p;
        	
		if(!empty($products))
		{
			$products_ids = array_keys($products);
			foreach($products as &$product)
			{
				$product->variants = array();
				$product->images = array();
				$product->properties = array();
			}
	
			$variants = $simpla->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));
			
			foreach($variants as &$variant)
			{
				//$variant->price *= (100-$discount)/100;
				$products[$variant->product_id]->variants[] = $variant;
			}
	
			$images = $simpla->products->get_images(array('product_id'=>$products_ids));
			foreach($images as $image)
				$products[$image->product_id]->images[] = $image;

			foreach($products as &$product)
			{
				if(isset($product->variants[0]))
					$product->variant = $product->variants[0];
				if(isset($product->images[0]))
					$product->image = $product->images[0];
			}
				
	
			/*
			$properties = $simpla->features->get_options(array('product_id'=>$products_ids));
			foreach($properties as $property)
				$products[$property->product_id]->options[] = $property;
			*/
	
			$simpla->design->assign('products', $products);
 		}
		
		
	
	
	$currencies = $simpla->money->get_currencies(array('enabled'=>1));
	if(isset($_SESSION['currency_id']))
		$currency = $simpla->money->get_currency($_SESSION['currency_id']);
	else
		$currency = reset($currencies);

	$simpla->design->assign('currency',	$currency);
	
	$result = $simpla->design->fetch('prods.tpl');
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
