<?php
	session_start();
	chdir('..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();

	$category_id = $simpla->request->post('cat', 'integer');
	$brand_id[]	  = $simpla->request->post('brand', 'integer');
	$mark_id    = $simpla->request->post('mark', 'integer');
	$filter['brand_id'] = array();
	
 
	
// Если задан бренд, выберем его из базы
		if ($brand_id[0] != '')
		if (!empty($brand_id)) {
			$filter['brand_id'] = array_merge($filter['brand_id'], $brand_id);
		}
	    
		
	    // Выберем текущую категорию
		if (!empty($category_id))
		{
			$category = $simpla->categories->get_category(intval($category_id));
			if (empty($category) || (!$category->visible && empty($_SESSION['admin'])))
				return false;
			$simpla->design->assign('category', $category);
			$filter['category_id'] = $category->children;
		}
	
			$filter['visible'] = 1;	
		$filter['in_stock'] = 1;	

	    // Если задана метка, выберем ее из базы
		if (!empty($mark_id))
		{
		    
			$mark = $simpla->marks->get_mark(intval($mark_id));
			if (empty($mark))
				return false;
			$simpla->design->assign('mark', $mark);
			$filter['mark_id'] = $mark->id;
			if ($mark->category != $category->id)return false;
		}
		
		// Свойства товаров
		if(!empty($category))
		{
			$features = array();
			$filter['features'] = array();
			foreach($simpla->features->get_features(array('category_id'=>$category->id, 'in_filter'=>1)) as $feature)
			{ 
				$features[$feature->id] = $feature;
				//if(($val = strval($simpla->request->get($feature->id)))!='')
				if(($val = $simpla->request->post($feature->id)) != '')
				{
					if($val[0] != '')
						$filter['features'][$feature->id] = (array)$val;
								
					$features[$feature->id]->active = true;
				}
				else
				{
					$features[$feature->id]->active = false;
				}
			}
			
			$options_filter['visible'] = 1;
			$options_filter['mark_id'] = $filter['mark_id'];
			$features_ids = array_keys($features);
			if(!empty($features_ids))
				$options_filter['feature_id'] = $features_ids;
			$options_filter['category_id'] = $category->children;
			if(isset($filter['features']))
				$options_filter['features'] = $filter['features'];
						
			$options = $simpla->features->get_options($options_filter);

			if(isset($options)) {
				foreach($options as &$option) {
					// в этой групе есть чекнутый фильтер
					if($features[$option->feature_id]->active) {

						if( in_array($option->value, $filter['features'][$option->feature_id])){
							$option->checked = true;
							$features[$option->feature_id]->options[] = $option;
							$option->count = 0;
						}else{
							$temp_filter = $filter;
							$temp_filter['features'][$option->feature_id] = (array)$option->value;
							$option->count = '+'.$simpla->products->count_products($temp_filter);
								if((int)$option->count > 0){
									$features[$option->feature_id]->options[] = $option;
								}else{
									$option->disabled = true;
									$option->count = 0;
								}
									
							unset($temp_filter);
						}
					} else {
						$temp_filter = $filter;
						$temp_filter['features'][$option->feature_id] = (array)$option->value;
						$option->count = $simpla->products->count_products($temp_filter);
							if((int)$option->count > 0)
								$features[$option->feature_id]->options[] = $option;
							else
								$option->disabled = true;
						unset($temp_filter);
						$option->checked = false;
					}
						
					
				}
			}
			
			
			
			foreach($features as $i=>&$feature)
			{ 
				if(empty($feature->options))
					unset($features[$i]);
			}

			$simpla->design->assign('features', $features);
			
			//Минимальная и максимальная допустимая цена
			$simpla->design->assign('max_min_price', $simpla->products->max_min_products($filter));	
			
			//Минимальная и максимальная допустимая цена
			$simpla->design->assign('slider_max_min_price', $simpla->products->max_min_products(array('category_id'=>$filter['category_id'], 'mark_id'=>$filter['mark_id'],'visible'=>1)));	
 		}
		
		
		$brands = $simpla->brands->get_brands(array('category_id'=>$category->children));
		
				//Есть ли активный бренд в фильтре
				if(!empty($filter['brand_id'])){
					$brands_active = true;
					$count_prefix = '+';
				}else{
					$count_prefix = '';
				}
		    foreach($brands as $key => $brand_temp){
						$temp_filter = $filter;
						$temp_filter['brand_id'] = (array)$brand_temp->id;
						$brand_temp->count = $count_prefix.$simpla->products->count_products($temp_filter);
						unset($temp_filter);
						if((int)$brand_temp->count <= 0){
							unset($brands[$key]);
						}
			     
			}
			
			foreach($brands as &$brand_temp){
			
				if(isset($brands_active)){
					if(in_array($brand_temp->id, $filter['brand_id'])){ //если активен текущий
						$brand_temp->checked = true;
						$brand_temp->disabled = false;
						$brand_temp->count = 0;
					} else {
						$brand_temp->checked = false;
						$temp_filter = $filter;
						$temp_filter['brand_id'] = (array)$brand_temp->id;
						$brand_temp->count = $count_prefix.$simpla->products->count_products($temp_filter);
						unset($temp_filter);
						if((int)$brand_temp->count > 0){
							$brand_temp->disabled = false;
						}else{
							$brand_temp->disabled = true;
							$brand_temp->count = 0;
						}
					}
				
				}else{
					$brand_temp->checked = false;
					$temp_filter = $filter;
					$temp_filter['brand_id'] = (array)$brand_temp->id;
					$brand_temp->count = $count_prefix.$simpla->products->count_products($temp_filter);
					unset($temp_filter);
						if((int)$brand_temp->count > 0)
							$brand_temp->disabled = false;
						else
							$brand_temp->disabled = true;
				}
			}
				
				$simpla->design->assign('brands',	$brands);

			$in_stock = new stdClass(); 
			if ($simpla->request->get('in_stock', 'integer')){
				$in_stock->checked = true;
				$in_stock->disabled = false;
				$in_stock->count = 0;
			}else{
				$temp_filter = $filter;
				$temp_filter['in_stock'] = 1;
				$in_stock->count = $simpla->products->count_products($temp_filter);
				if((int)$in_stock->count > 0){
					$in_stock->disabled = false;
				}else{
					$in_stock->disabled = true;
					$in_stock->count = 0;
				}
				unset($temp_filter);
			}
			
			$simpla->design->assign('in_stock',	$in_stock);				
			
			$featured = new stdClass(); 
			if ($simpla->request->get('featured', 'integer')){
				$featured->checked = true;
				$featured->disabled = false;
				$featured->count = 0;
			}else{
				$temp_filter = $filter;
				$temp_filter['featured'] = 1;
				$featured->count = $simpla->products->count_products($temp_filter);
				if((int)$featured->count > 0){
					$featured->disabled = false;
				}else{
					$featured->disabled = true;
					$featured->count = 0;
				}
				unset($temp_filter);
			}
			
			$simpla->design->assign('featured',	$featured);
			
			$discounted = new stdClass(); 
			if ($simpla->request->get('discounted', 'integer')){
				$discounted->checked = true;
				$discounted->disabled = false;
				$discounted->count = 0;
			}else{
				$temp_filter = $filter;
				$temp_filter['discounted'] = 1;
				$discounted->count = $simpla->products->count_products($temp_filter);
				if((int)$discounted->count > 0){
					$discounted->disabled = false;
				}else{
					$discounted->disabled = true;
					$discounted->count = 0;
				}
				unset($temp_filter);
			}
			$simpla->design->assign('discounted',	$discounted);
			
		$currencies = $simpla->money->get_currencies(array('enabled'=>1));
	if(isset($_SESSION['currency_id']))
		$currency = $simpla->money->get_currency($_SESSION['currency_id']);
	else
		$currency = reset($currencies);

	$simpla->design->assign('currency',	$currency);
	
	$result = $simpla->design->fetch('filter.tpl');
		
		
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
