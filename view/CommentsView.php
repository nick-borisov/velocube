<?PHP 
ini_set('display_errors',1);
require_once('View.php');

class CommentsView extends View
{

  function fetch()
  {
  
 	$filter = array();
  	//$filter['page'] = max(1, $this->request->get('page', 'integer'));
  		
  	//$filter['limit'] = $this->settings->limit_comments;
  	$filter['type'] = 'shop';
	$filter['approv'] = 1; 

		// Автозаполнение имени для формы комментария
		if(!empty($this->user))
			$this->design->assign('comment_name', $this->user->name);

		
		// Принимаем комментарий
		if ($this->request->method('post') && $this->request->post('comment'))
		{
			$comment->name = $this->request->post('name');
			$comment->text = $this->request->post('text');
			$comment->parent_id = $this->request->post('parent_id');
			$comment->admin = $this->request->post('admin');
			
            /*RECAPTCHA*/
    		$recaptha_response = $this->request->post('g-recaptcha-response');
            $remoteip = $_SERVER['REMOTE_ADDR'];
            $recaptcha_status = json_decode($this->recaptcha->send($recaptha_response, $remoteip));
            /*/RECAPTCHA*/
			
			// Передадим комментарий обратно в шаблон - при ошибке нужно будет заполнить форму
			$this->design->assign('comment_text', $comment->text);
			$this->design->assign('comment_name', $comment->name);
			$this->design->assign('parent_id', $comment->parent_id);
			
			// Проверяем капчу и заполнение формы
			if (empty($comment->name))
			{
				$this->design->assign('error', 'empty_name');
			}
			elseif (empty($comment->text))
			{
				$this->design->assign('error', 'empty_comment');
			}
            /*RECAPTCHA*/
			elseif(empty($recaptha_response) || !$recaptcha_status->success)
			{
				$this->design->assign('error', 'captcha');
			}
            /*RECAPTCHA*/
			else
			{
				// Создаем комментарий
				$comment->object_id = 000;
				$comment->type      = 'shop';
				$comment->ip        = $_SERVER['REMOTE_ADDR'];
				
				// Если были одобренные комментарии от текущего ip, одобряем сразу
				//$this->db->query("SELECT 1 FROM __comments WHERE approved=1 AND ip=? LIMIT 1", $comment->ip);
				//if($this->db->num_rows()>0)
				//	$comment->approved = 1;
				
				// Добавляем комментарий в базу
				$comment_id = $this->comments->add_comment($comment);
				
				// Отправляем email
				$this->notify->email_comment_admin($comment_id);				
				
				// Приберем сохраненную капчу, иначе можно отключить загрузку рисунков и постить старую
				unset($_SESSION['captcha_code']);
				header('location: '.$_SERVER['REQUEST_URI'].'#comment_'.$comment_id);
			}			
		}
	// Постраничная навигация
		$items_per_page = $this->settings->limit_comments;		
		// Текущая страница в постраничном выводе
		$current_page = $this->request->get('page', 'integer');	
		// Если не задана, то равна 1
		$current_page = max(1, $current_page);
		$this->design->assign('current_page_num', $current_page);
		// Вычисляем количество страниц

		$comments_count = $this->comments->count_comments(array('type'=>'shop', 'approved'=>1, 'ip'=>$_SERVER['REMOTE_ADDR']));
		
		// Показать все страницы сразу
		if($this->request->get('page') == 'all')
			$items_per_page = $comments_count;	
		
		$pages_num = ceil($comments_count/$items_per_page);
		$this->design->assign('total_pages_num', $pages_num);
        
        if($pages_num > 1 && $current_page < $pages_num){
            $this->design->assign('is_more', 1);
        }

		$filter['page'] = $current_page;
		$filter['limit'] = $items_per_page;

	// Отображение
  	if(isset($_SESSION['admin']) && $_SESSION['admin'] == 'admin')
		$comments = $this->comments->get_comments_tree($filter);
	  else
    $comments = $this->comments->get_comments_tree(array('type'=>'shop', 'approved'=>1, 'ip'=>$_SERVER['REMOTE_ADDR'], 'page'=>$current_page, 'limit'=>$items_per_page));
  	//$comments_count = $this->comments->count_comments($filter);
  	

		// Текущая страница в постраничном выводе
		//$current_page = $this->request->get('page', 'integer');
		
		// Если не задана, то равна 1
		//$current_page = max(1, $current_page);
		//$this->design->assign('current_page_num', $current_page);

		// Вычисляем количество страниц
		//$pages_num = ceil($comments_count/$filter['limit']);
		//$this->design->assign('total_pages_num', $pages_num);  	
//var_dump($comments);

 	$this->design->assign('comments', $comments);
 	//$this->design->assign('comments_count', $comments_count);

		// Метатеги
		if($this->page)
		{
			$this->design->assign('meta_title', $this->page->meta_title);
			$this->design->assign('meta_keywords', $this->page->meta_keywords);
			$this->design->assign('meta_description', $this->page->meta_description);
		}
		
		$body = $this->design->fetch('comments.tpl');
		
		return $body;

  }
}


?>