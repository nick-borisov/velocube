<?PHP
//ini_set('display_errors',true);
/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Базовый класс для всех View
 *
 */

require_once('api/Simpla.php');

class View extends Simpla
{
	/* Смысл класса в доступности следующих переменных в любом View */
	public $currency;
	public $currencies;
	public $user;
	public $group;
	public $page;
	
	/* Класс View похож на синглтон, храним статически его инстанс */
	private static $view_instance;
	
	public function __construct()
	{
		parent::__construct();
		
		// Если инстанс класса уже существует - просто используем уже существующие переменные
		if(self::$view_instance)
		{
			$this->currency     = &self::$view_instance->currency;
			$this->currencies   = &self::$view_instance->currencies;
			$this->user         = &self::$view_instance->user;
			$this->group        = &self::$view_instance->group;	
			$this->page         = &self::$view_instance->page;	
		}
		else
		{
			// Сохраняем свой инстанс в статической переменной,
			// чтобы в следующий раз использовать его
			self::$view_instance = $this;

			// Все валюты
			$this->currencies = $this->money->get_currencies(array('enabled'=>1));
	
			// Выбор текущей валюты
			if($currency_id = $this->request->get('currency_id', 'integer'))
			{
				$_SESSION['currency_id'] = $currency_id;
				header("Location: ".$this->request->url(array('currency_id'=>null)));
			}
			
			// Берем валюту из сессии
			if(isset($_SESSION['currency_id']))
				$this->currency = $this->money->get_currency($_SESSION['currency_id']);
			// Или первую из списка
			else
				$this->currency = reset($this->currencies);
	
			// Пользователь, если залогинен
			if(isset($_SESSION['user_id']))
			{
				$u = $this->users->get_user(intval($_SESSION['user_id']));
				if($u && $u->enabled)
				{
					$this->user = $u;
					$this->group = $this->users->get_group($this->user->group_id);
				
				}
			}
    
	  if(isset($_SESSION['compared_products'])){
		$compare_products = count($_SESSION['compared_products']);
		}
    else $compare_products = 0;
    $this->design->assign('compare_products',	$compare_products);

		if(!empty($_COOKIE['wishlist_products']))
		{
			$wishlist_products = explode(',', $_COOKIE['wishlist_products']);
      $this->design->assign('wishlist_products',	$wishlist_products);
    }      
    
			// Текущая страница (если есть)
			$subdir = substr(dirname(dirname(__FILE__)), strlen($_SERVER['DOCUMENT_ROOT']));
			$page_url = trim(substr($_SERVER['REQUEST_URI'], strlen($subdir)),"/");
			if(strpos($page_url, '?') > 0)
				$page_url = substr($page_url, 0, strpos($page_url, '?'));
			$this->page = $this->pages->get_page((string)$page_url);
			$this->design->assign('page', $this->page);		
			
			// Передаем в дизайн то, что может понадобиться в нем
			$this->design->assign('currencies',	$this->currencies);
			$this->design->assign('currency',	$this->currency);
			$this->design->assign('user',       $this->user);
			$this->design->assign('group',      $this->group);
			
			$this->design->assign('config',		$this->config);
			$this->design->assign('settings',	$this->settings);

			// Настраиваем плагины для смарти
			$this->design->smarty->registerPlugin("function", "get_posts",               array($this, 'get_posts_plugin'));
			$this->design->smarty->registerPlugin("function", "get_articles",            array($this, 'get_articles_plugin'));
			$this->design->smarty->registerPlugin("function", "get_brands",              array($this, 'get_brands_plugin'));
			$this->design->smarty->registerPlugin("function", "get_browsed_products",    array($this, 'get_browsed_products'));
			$this->design->smarty->registerPlugin("function", "get_products",            array($this, 'get_products_plugin'));
			$this->design->smarty->registerPlugin("function", "get_featured_products",        array($this, 'get_featured_products_plugin'));
			$this->design->smarty->registerPlugin("function", "get_pop_products",        array($this, 'get_pop_products_plugin'));
			$this->design->smarty->registerPlugin("function", "get_new_products",        array($this, 'get_new_products_plugin'));
			$this->design->smarty->registerPlugin("function", "get_discounted_products", array($this, 'get_discounted_products_plugin'));
			$this->design->smarty->registerPlugin("function", "get_slides",				 array($this, 'get_slides_plugin'));
			$this->design->smarty->registerPlugin("function", "get_cslides",			 array($this, 'get_cslides_plugin'));
			
			$this->design->smarty->registerPlugin("function", "get_ttoday_products_plugin", array($this, 'get_ttoday_products_plugin'));
			
				 // плагины для листа сравнения
			$this->design->smarty->registerPlugin("function", "get_prods",            array($this, 'get_prods_plugin'));
			$this->design->smarty->registerPlugin("function", "get_session_products",    array($this, 'get_session_products_plugin'));
			$this->design->smarty->registerPlugin("function", "remove_product_session",  array($this, 'remove_product_session_plugin'));
			$this->design->smarty->registerPlugin("function", "add_product_session",     array($this, 'add_product_session_plugin'));
            // плагины для листа сравнения
      
            $this->design->smarty->registerPlugin("function", "get_comments", array($this, 'get_comments_plugin'));
			$this->design->smarty->registerPlugin("function", "get_categories_in_brand", array($this, 'get_categories_in_brand_plugin'));
		}
	}
		
	/**
	 *
	 * Отображение
	 *
	 */
	function fetch()
	{
		return false;
	}
	
	
	 // плагины для листа сравнения
	public function add_product_session_plugin($params, &$smarty)
	{
	    $list = array();
        $key  = $params['key'];
        $id   = $params['id'];
        if(empty($key) && empty($id))
            return false;
        if(!$this->design->smarty->templateExists('products_session_'.$key.'_informer.tpl'))
            return false;
        $product = $this->products->get_product((int)$id);
        if(empty($product))
            return false;
		
		if(!empty($_COOKIE[$key]))
        {
		    
		    $list = explode(";",$_COOKIE[$key]);
           
		}
		if (!in_array($id, $list)) $list[]=$id;
		
		
		setcookie($key, implode(";",$list), time()+60*60*24*180, "/");
	}
	public function remove_product_session_plugin($params, &$smarty)
	{
	   $list = array();
       $id   = $params['id'];
		if(!empty($params['key']) && $params['remove'])
        {
		   
            $list = explode(";",$_COOKIE[$params['key']]);
			if (in_array($id, $list)) {
			$n = array_search($id, $list);
			unset($list[$n]) ; //Удаляем элемент массива
			}
	

			setcookie($params['key'], implode(";",$list), time()+60*60*24*180, "/");
		}
	}
	public function get_session_products_plugin($params, &$smarty)
	{
		
					
			
		
		
		if(!empty($_COOKIE[$params['key']]))
        {
            $result = new stdClass();
		    $list = array();
		    $list = explode(";",$_COOKIE[$params['key']]);
            $result->key   = $params['key']; // ключ
            $result->count = count($list);  // количество
            $result->ids = 	$list;		
            $smarty->assign($params['key'], $result);
		}
		
	}
	public function get_prods_plugin($params, &$smarty)
	{
	
	
	
		if(!empty($params['var']))
		{
            if(!empty($params['get_session_products'])){
                if(!empty($_COOKIE[$params['get_session_products']])){
                    
					 $list = array();
					$list = explode(";",$_COOKIE[$params['get_session_products']]);
					$params['id'] = $list;
                    $params['visible'] = 1;
                }
                else{
                    return false;
                }
            }
            foreach($this->products->get_products($params) as $p){
                //$brand = $this->brands->get_brand(intval($p->brand_id));
				//$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
                $products[$p->id] = $p;
            }
			if(!empty($products))
			{
				$products_ids = array_keys($products);
				$variants = $this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));
				foreach($variants as &$variant)
				{
					$products[$variant->product_id]->variants[] = $variant;
				}
				$images = $this->products->get_images(array('product_id'=>$products_ids));
				foreach($images as $image)
					$products[$image->product_id]->images[] = $image;

                if(!empty($params['data_features'])){
    			    $properties = $this->features->get_product_options($products_ids);
    			    foreach($properties as $property)
    				    $products[$property->product_id]->options[] = $property;
                }
                if(!empty($params['data_categories'])){
    			    $categories = $this->categories->get_categories(array('product_id'=>$products_ids));
                    $product_categories = $this->categories->get_product_categories($products_ids);
    			    foreach($product_categories as $category)
    				    $products[$category->product_id]->categories[$category->category_id] = $categories[$category->category_id];
                }
				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
					if(!empty($product->categories))
						$product->category = reset($product->categories);
                        
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    /*auto_discount*/
                        
				}
			}
			$smarty->assign($params['var'], $products);

		}
		
	}
    // плагины для листа сравнения
	/**
	 *
	 * Плагины для смарти
	 *
	 */	
	public function get_posts_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		if(!empty($params['var']))
			$smarty->assign($params['var'], $this->blog->get_posts($params));
	}
	
	public function get_articles_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		if(!isset($params['sort']))
			$params['sort'] = 'date';			
		if(!empty($params['var']))
			$smarty->assign($params['var'], $this->articles->get_articles($params));
	} 
	public function get_slides_plugin($params, &$smarty)
	{
		if(!empty($params['var']))
			$smarty->assign($params['var'], $this->slides->get_slides($params));
	}  
	
	public function get_cslides_plugin($params, &$smarty)
	{
		if(!empty($params['var']))
			$smarty->assign($params['var'], $this->cslides->get_cslides($params));
	}  
	/**
	 *
	 * Плагины для смарти
	 *
	 */	
	public function get_brands_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		if(!empty($params['var']))
			$smarty->assign($params['var'], $this->brands->get_brands($params));
	}
	
	public function get_browsed_products($params, &$smarty)
	{
		if(!empty($_COOKIE['browsed_products']))
		{
			$browsed_products_ids = explode(',', $_COOKIE['browsed_products']);
			$browsed_products_ids = array_reverse($browsed_products_ids);
			if(isset($params['limit']))
				$browsed_products_ids = array_slice($browsed_products_ids, 0, $params['limit']);

			$products = array();
            $params['id'] = $browsed_products_ids;
                       
            foreach($this->products->get_products($params) as $p)
                $products[$p->id] = $p;
            
            unset($params['id']);
            if($products){
                // id выбраных товаров
    			$products_ids = array_keys($products);
                
                // Выбираем варианты товаров
    			$variants = $this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));
    			
    			// Для каждого варианта
    			foreach($variants as &$variant)
    			{
    				// добавляем вариант в соответствующий товар
    				$products[$variant->product_id]->variants[] = $variant;
    			}
            }
          
			$browsed_products_images = $this->products->get_images(array('product_id'=>$browsed_products_ids));
			foreach($browsed_products_images as $browsed_product_image)
				if(isset($products[$browsed_product_image->product_id]))
					$products[$browsed_product_image->product_id]->images[] = $browsed_product_image;
            
            $result = array();
			foreach($browsed_products_ids as $id)
			{	
				if(is_object($products[$id]))
				{
				    if(isset($products[$id]->variants[0]))
						$products[$id]->variant = $products[$id]->variants[0];
					if(isset($products[$id]->images[0]))
						$products[$id]->image = $products[$id]->images[0];
                        
                    /*auto_discount*/
                    $products[$id] = $this->calc_auto_discount($products[$id]);
                    /*auto_discount*/
                    
					$result[] = $products[$id];
				}
			}	
			$smarty->assign($params['var'], $result);
            //if($_SESSION['admin']) var_dump($result);
		}
	}
	
	public function get_products_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;

		if(!empty($params['var']))
		{
            foreach($this->products->get_products($params) as $p){
                //$brand = $this->brands->get_brand(intval($p->brand_id));
				//$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
                $products[$p->id] = $p;
            }
			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);
                
				// Выбираем варианты товаров
				$variants = $this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));
				
				// Для каждого варианта
				foreach($variants as &$variant)
				{
					// добавляем вариант в соответствующий товар
					$products[$variant->product_id]->variants[] = $variant;
				}
				
				// Выбираем изображения товаров
				$images = $this->products->get_images(array('product_id'=>$products_ids));
				foreach($images as $image)
					$products[$image->product_id]->images[] = $image;
	
				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
                        
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    /*auto_discount*/
				}				
			}

			$smarty->assign($params['var'], $products);
			
		}
	}

	public function get_featured_products_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		$params['featured'] = 1;
		$params['in_stock'] = 1;
        $params['sort'] = 'position_adm';
		if(!empty($params['var']))
		{
			foreach($this->products->get_products($params) as $p)
                $products[$p->id] = $p;

			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);

                // Выбираем изображения товаров
                $images = $this->products->get_images(array('product_id'=>$products_ids));
                foreach($images as $image)
                    $products[$image->product_id]->images[] = $image;

                // Выбираем варианты товаров
                foreach($this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true)) as $variant)
                {
                    if(!empty($variant->image_id) && isset($images[$variant->image_id]))
                        $variant->image = $images[$variant->image_id];
                    // добавляем вариант в соответствующий товар
                    $products[$variant->product_id]->variants[] = $variant;
                }
                // Для каждого варианта
//				foreach($variants as &$variant)
//				{
//					// добавляем вариант в соответствующий товар
//					$products[$variant->product_id]->variants[] = $variant;
//				}

				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
                    
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    /*auto_discount*/                        
				}				
			}

			$smarty->assign($params['var'], $products);
			
		}
	}
		
	public function get_pop_products_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		//$params['in_stock'] = 1;
		 
		$params['sort'] = 'rating';
		if(!empty($params['var']))
		{
			
			foreach($this->products->get_products($params) as $p)
                $products[$p->id] = $p;

			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);
		
				// Выбираем варианты товаров
                // Выбираем изображения товаров
                $images = $this->products->get_images(array('product_id'=>$products_ids));
                foreach($images as $image)
                    $products[$image->product_id]->images[] = $image;

                // Выбираем варианты товаров
                foreach($this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true)) as $variant)
                {
                    if(!empty($variant->image_id) && isset($images[$variant->image_id]))
                        $variant->image = $images[$variant->image_id];
                    // добавляем вариант в соответствующий товар
                    $products[$variant->product_id]->variants[] = $variant;
                }

                // Для каждого варианта
//				foreach($variants as &$variant)
//				{
//					// добавляем вариант в соответствующий товар
//					$products[$variant->product_id]->variants[] = $variant;
//				}

				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
                        
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    /*auto_discount*/
				}				
			}

			$smarty->assign($params['var'], $products);
			
		}
	}
	
	public function get_new_products_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		if(!isset($params['sort']))
			$params['sort'] = 'created';
		if(!empty($params['var']))
		{
			foreach($this->products->get_products($params) as $p){
                //$brand = $this->brands->get_brand(intval($p->brand_id));
				//$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
                $products[$p->id] = $p;
            }

			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);
		
				// Выбираем варианты товаров
                // Выбираем изображения товаров
                $images = $this->products->get_images(array('product_id'=>$products_ids));
                foreach($images as $image)
                    $products[$image->product_id]->images[] = $image;

                // Выбираем варианты товаров
                foreach($this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true)) as $variant)
                {
                    if(!empty($variant->image_id) && isset($images[$variant->image_id]))
                        $variant->image = $images[$variant->image_id];
                    // добавляем вариант в соответствующий товар
                    $products[$variant->product_id]->variants[] = $variant;
                }


                // Для каждого варианта
				foreach($variants as &$variant)
				{
					// добавляем вариант в соответствующий товар
					$products[$variant->product_id]->variants[] = $variant;
				}
				
				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    /*auto_discount*/
				}				
			}

			$smarty->assign($params['var'], $products);
			
		}
	}
	
	
	public function get_discounted_products_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
		$params['discounted'] = 1;
		if(!empty($params['var']))
		{
			foreach($this->products->get_products($params) as $p){
                //$brand = $this->brands->get_brand(intval($p->brand_id));
				//$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
                $products[$p->id] = $p;
            }

			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);
		
				// Выбираем варианты товаров
                // Выбираем варианты товаров
                $variants = $this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));

                // Для каждого варианта
                foreach($variants as &$variant)
                {
                    // добавляем вариант в соответствующий товар
                    $products[$variant->product_id]->variants[] = $variant;
                }

                // Выбираем изображения товаров
                $images = $this->products->get_images(array('product_id'=>$products_ids));
                foreach($images as $image)
                    $products[$image->product_id]->images[] = $image;


                // Для каждого варианта
				foreach($variants as &$variant)
				{
					// добавляем вариант в соответствующий товар
					$products[$variant->product_id]->variants[] = $variant;
				}
	
				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
                    
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    /*auto_discount*/
				}				
			}

			$smarty->assign($params['var'], $products);
			
		}
	}
	
	public function get_ttoday_products_plugin($params, &$smarty)
	{
		if(!isset($params['visible']))
			$params['visible'] = 1;
			$params['ttoday'] = 1;

		if(!empty($params['var']))
		{
			foreach($this->products->get_products($params) as $p)
                $products[$p->id] = $p; 

			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);
		
				// Выбираем варианты товаров
				$variants = $this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));
				
				// Для каждого варианта
				foreach($variants as &$variant)
				{
					// добавляем вариант в соответствующий товар
					$products[$variant->product_id]->variants[] = $variant;
				}
				
				// Выбираем изображения товаров
				$images = $this->products->get_images(array('product_id'=>$products_ids));

                if(count($images)){
    				foreach($images as $image)
    					$products[$image->product_id]->images[] = $image;
                }
                
				foreach($products as &$product)
				{
					if(isset($product->variants[0]))
						$product->variant = $product->variants[0];
					if(isset($product->images[0]))
						$product->image = $product->images[0];
                        
                    /*auto_discount*/
                    $product = $this->calc_auto_discount($product);
                    //if($_SESSION['admin'])print_r($product);
                    /*auto_discount*/
				}				
			}

			$smarty->assign($params['var'], $products);
			
		}
	}
    
    public function get_comments_plugin($params, &$smarty)
    {
		if(!isset($params['main_page']))
			$params['main_page'] = 1;
 		if(!isset($params['approved']))
			$params['approved'] = 1;

        if(!empty($params['var'])){
            $comments = $this->comments->get_comments($params);
        
            $products_ids = array();
          	$posts_ids = array();    
          	
            foreach($comments as $comment)
          	{
          		if($comment->type == 'product')
          			$products_ids[] = $comment->object_id;
          		if($comment->type == 'blog')
          			$posts_ids[] = $comment->object_id;
          	}
            
        	$products = array();
        	foreach($this->products->get_products(array('id'=>$products_ids)) as $p){
        		$products[$p->id] = $p;
            }   
            $products_images = $this->products->get_images(array('product_id'=>array_keys($products)));
    			foreach($products_images as $image)
					$products[$image->product_id]->image = $image;
                        
          	$posts = array();
          	foreach($this->blog->get_posts(array('id'=>$posts_ids)) as $p)
          		$posts[$p->id] = $p;
          		
          	foreach($comments as &$comment)
          	{
          		if($comment->type == 'product' && isset($products[$comment->object_id]))
          			$comment->product = $products[$comment->object_id];
          		if($comment->type == 'blog' && isset($posts[$comment->object_id]))
          			$comment->post = $posts[$comment->object_id];
          	}    
    		//print_r($comments);
   			$smarty->assign($params['var'], $comments);
        }
	}
    
    /*auto_discount*/
    private function calc_auto_discount($product){
        $coupons = $this->coupons->calc_product_coupon($product->id);
        //auto_discount
        // $products->categories = $this->categories->get_categories(array('product_id'=>$product->id)); 
        // $category = reset($products->categories);
        // foreach($category->path as $cp){
        //     if($cp->parent_id == 0){
        //         $root_category_children = $cp->children;
        //     }
        // }
        
        //Получаем стразу список стоп-товаров
        //$ad_stop_products = $this->coupons->get_coupons(array('stop_related_product_id'=>$product->id, 'auto_discount'=>1, 'schedule'=>1));
        /*foreach($products->categories as $pc){
            $product_category_ids[] = $pc->id;
        }*/
        // if(!$ad_stop_products){
        //     if($auto_discount_products = $this->coupons->get_coupons(array('related_product_id'=>$product->id, 'auto_discount'=>1, 'schedule'=>1))){
        //         //рассчитываем скидку для каждого купона, чтобы выбрать самую большую
        //         foreach($auto_discount_products as $ad_prod){
        //             if($ad_prod->active_margin){
        //                 $margins = unserialize($ad_prod->margin);
        // 				if($margins['min'][0] && $margins['max'][0]){  
        //                     foreach($margins['min'] as $i=>$min){
        //                         if($product->variant->price > intval($margins['min'][$i]) && $product->variant->price < intval($margins['max'][$i])){
        //                             if(intval($margins['mode'][$i])==1){
        //                                 $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
        //                             }elseif(intval($margins['mode'][$i])==2){
        //                                 $coupons[$ad_prod->id] = round($product->variant->price * $margins['price'][$i] / 100, 2); 
        //                             }
        //                         }
        //                     }
        // 				}
        //             }else{//если интервалов нет
        //                 if($ad_prod->type == 'percentage')
        //                     $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
        //                 elseif($ad_prod->type == 'absolute')
        //                     $coupons[$ad_prod->id] = floatval($ad_prod->value);
        //             }
        //         }
        //     }elseif($auto_discount_brands = $this->coupons->get_coupons(array('brand_id'=>$product->brand_id, 'auto_discount'=>1, 'schedule'=>1))){
        //         if($auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$root_category_children, 'auto_discount'=>1, 'schedule'=>1))){
        //             foreach($auto_discount_brands as $ad_prod){
        //                 //если есть интервалы для цены, работаем с ними
        //                 if($ad_prod->active_margin){
        //                     $margins = unserialize($ad_prod->margin);
        //     				if($margins['min'][0] && $margins['max'][0]){  
        //                         foreach($margins['min'] as $i=>$min){
        //                             if($product->variant->price > intval($margins['min'][$i]) && $product->variant->price < intval($margins['max'][$i])){
        //                                 if(intval($margins['mode'][$i])==1){
        //                                     $coupons[$ad_prod->id] = floatval($margins['price'][$i]);
        //                                 }elseif(intval($margins['mode'][$i])==2){
        //                                     $coupons[$ad_prod->id] = round($product->variant->price * $margins['price'][$i] / 100, 2); 
        //                                 }
        //                             }
        //                         }
        //     				}
        //                 }else{//если интервалов нет
        //                     if($ad_prod->type == 'percentage')
        //                         $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
        //                     elseif($ad_prod->type == 'absolute')
        //                         $coupons[$ad_prod->id] = floatval($ad_prod->value);
        //                 }
        //             }
        //         }
        //     }/*elseif(!array_sum($auto_discount_brands) && $auto_discount_categories = $this->coupons->get_coupons(array('category_id'=>$product_category_ids, 'auto_discount'=>1))){
        //         foreach($auto_discount_categories as $ad_prod){
        //             if($ad_prod->type == 'percentage')
        //                 $coupons[$ad_prod->id] = round($product->variant->price * $ad_prod->value/100, 0);
        //             elseif($ad_prod->type == 'absolute')
        //                 $coupons[$ad_prod->id] = floatval($ad_prod->value);
        //         }
        //     }*/
            //выбираем наибольшую скидку, получаем выбранный купон
            arsort($coupons);
            $selected_coupon_id = key($coupons);
            $coupon_discount_price = reset($coupons);
            $selected_coupon = $this->coupons->get_coupon(intval($selected_coupon_id));
//print('coupons VIEW--');var_dump($coupons);          
            //применяем купон для вариантов
            foreach($product->variants as $variant3){
                if($selected_coupon->type == 'percentage'){
                    $variant3->auto_coupon_price = max(0, round($variant3->price * (100-$selected_coupon->value)/100, 0));
                    $variant3->coupon_discount_price = round($variant3->price * $selected_coupon->value / 100, 0);
                }elseif($selected_coupon->type == 'absolute'){
                    $variant3->auto_coupon_price = max(0, ($variant3->price - $selected_coupon->value));
                    $variant3->coupon_discount_price = $selected_coupon->value;
                }
                $variant3->coupon_id = $selected_coupon_id;
            }
            $product->variant->coupon_discount_price = $coupon_discount_price;
            $product->variant->coupon_id = $selected_coupon_id;
            $product->variant->auto_coupon_price = max(0, $product->variant->price - $coupon_discount_price);
       // }
        return $product;     
    }
	/*/auto_discount/*/


	public function get_categories_in_brand_plugin($params, &$smarty){
		if(!isset($params['visible']))
			$params['visible'] = 1;
		if(!isset($params['limit']))
			$params['limit'] = 10000;
		if(!empty($params['brand_id']))
		{
			//$brand = $this->brands->get_brand(intval($params['brand_id']));
			foreach($this->products->get_products($params) as $p){
				//
				//$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
				$products[$p->id] = $p;
			}
			if(!empty($products))
			{
				// id выбраных товаров
				$products_ids = array_keys($products);

				$categories = $this->categories->get_categories(array('product_id'=>$products_ids));
				if($_SESSION['admin']) var_dump($categories);

			}

			$smarty->assign($params['var'], $products);

		}
	}
}