<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон index.tpl,
 * который содержит всю страницу кроме центрального блока
 * По get-параметру module мы определяем что сожержится в центральном блоке
 *
 */

require_once('View.php');

class IndexView extends View
{	
	public $modules_dir = 'view/';

	public function __construct()
	{
		parent::__construct();
	}

		
	/**
	 *
	 * Отображение
	 *
	 */
	function fetch()
	{
		
		        //Модуль работы с метаданными
        $currentURL=$_SERVER['REQUEST_URI'];
        $metadata_page=$this->metadatapages->get_metadata_page($currentURL);
        if(!empty($metadata_page) )
                $this->design->assign('metadata_page',$metadata_page);
        /*/metatitle-data*/
		
		// Содержимое корзины
		$this->design->assign('cart',		$this->cart->get_cart());
	
        // Категории товаров
		$this->design->assign('categories', $this->categories->get_categories_tree());
    
        // Категории статей
		$this->design->assign('articles_categories', $this->articles_categories->get_articles_categories_tree());		
		    		
		// Страницы
		$pages = $this->pages->get_pages(array('visible'=>1));		
		$this->design->assign('pages', $pages);

		/* callbacks_cheap */
		$item_spam = $this->request->post('name_asbf');
		if($this->request->method('post') && $this->request->post('callback') && empty($item_spam)) {
			$callback = new StdClass();
			$callback->phone        = $this->request->post('phone');
			$callback->name         = $this->request->post('name');
			$callback->message      = $this->request->post('message');

			//$callback->message    = "___";
			$callback->product_id   = $this->request->post('product_id');
			//$callback->url = $_SERVER['REQUEST_URI'];
			$this->design->assign('call_sent', true);

			$callback_id = $this->callbackcheap->add_callback($callback);
			// Отправляем email
			$this->callbackcheap->email_callback_admin($callback_id);
		}
		/*/ callbacks_cheap /*/

		// Текущий модуль (для отображения центрального блока)
		$module = $this->request->get('module', 'string');
		$module = preg_replace("/[^A-Za-z0-9]+/", "", $module);

		$stickers = $this->stickers->get_stickers(array("visible" => 1, "type" => $module));
		if($stickers) {
			$sticker = $stickers[array_rand($stickers)];
			$this->design->assign('sticker', $sticker);
		}

		

        /*user_groups*/
        if($this->user->group_id)
            $this->design->assign('user_group', $this->users->get_group($this->user->group_id));

		// Если не задан - берем из настроек
		if(empty($module))
			return false;
		//$module = $this->settings->main_module;

		// ttoday
		if ($module == 'MainView') {
			if (!$this->settings->product_today_id) {
				$this->settings->product_today_id = $this->products->get_product_id_today();
			}

			$product_today = $this->products->get_product(intval($this->settings->product_today_id));
			if(!empty($product_today))
				{
					$product_today->images = $this->products->get_images(array('product_id'=>$product_today->id));
	        		$product_today->image = reset($product->images);

			
					// Выбираем варианты товаров
					$product_today->variants[] = $this->variants->get_variants(array('product_id'=>$product_today->id, 'in_stock'=>true));

	                if (isset($product_today->variants[0])) {
	                	$product_today->variant = $product_today->variants[0];
	                }
					/*auto_discount*/
	                $product_today = $this->calc_auto_discount($product_today);
	                // var_dump($product_today);
	                $this->design->assign('product_today', $product_today);
				}
		}
		// ttoday end

		// Создаем соответствующий класс
		if (is_file($this->modules_dir."$module.php"))
		{
				include_once($this->modules_dir."$module.php");
				if (class_exists($module))
				{
					$this->main = new $module($this);
				} else return false;
		} else return false;

		// Создаем основной блок страницы
		if (!$content = $this->main->fetch())
		{
			return false;
		}		

		// Передаем основной блок в шаблон
		$this->design->assign('content', $content);		
		
		// Передаем название модуля в шаблон, это может пригодиться
		$this->design->assign('module', $module);
				
		// Создаем текущую обертку сайта (обычно index.tpl)
		$wrapper = $this->design->smarty->getTemplateVars('wrapper');
		if(is_null($wrapper))
			$wrapper = 'index.tpl';
		
		
		
		$can=preg_replace("/\?.*/", '', $_SERVER['REQUEST_URI']);
		if($module=='ProductsView' && $keyword=$this->request->get('keyword') )
			$can.='?keyword='.$keyword;
		else if($module=='ProductsView' || $module=='BlogView' || $module=='ArticlesView')
			$can.='';
		$can='http://'.$_SERVER['SERVER_NAME'].$can;
		$this->design->assign('canonical', $can);

		
		
		if(!empty($wrapper))
			return $this->body = $this->design->fetch($wrapper);
		else
			return $this->body = $content;

	}
	private function calc_auto_discount($product)
	{
        $coupons = $this->coupons->calc_product_coupon($product->id);
        arsort($coupons);
        $selected_coupon_id = key($coupons);
        $coupon_discount_price = reset($coupons);
        $selected_coupon = $this->coupons->get_coupon(intval($selected_coupon_id));
        //применяем купон для вариантов
        foreach ($product->variants as $variant3) {
            if ($selected_coupon->type == 'percentage') {
                $variant3->auto_coupon_price = max(0, round($variant3->price * (100-$selected_coupon->value)/100, 0));
                $variant3->coupon_discount_price = round($variant3->price * $selected_coupon->value / 100, 0);
            } elseif ($selected_coupon->type == 'absolute') {
                $variant3->auto_coupon_price = max(0, ($variant3->price - $selected_coupon->value));
                $variant3->coupon_discount_price = $selected_coupon->value;
            }
            $variant3->coupon_id = $selected_coupon_id;
        }
        $product->variant->coupon_discount_price = $coupon_discount_price;
        $product->variant->coupon_id = $selected_coupon_id;
        $product->variant->auto_coupon_price = max(0, $product->variant->price - $coupon_discount_price);

        return $product;     
    }
}
