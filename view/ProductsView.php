<?PHP
//ini_set('display_errors',1);
/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simplacms.ru
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон products.tpl
 *
 */
 
require_once('View.php');
//ini_set('display_errors', 1);
class ProductsView extends View
{
    
 	/* chpu_filter_extended */
    private $meta_array = array();
    private $set_canonical = false;
    private $meta = array('h1'=>'','title'=>'','keywords'=>'','description'=>'');
    private $meta_delimiter = ', ';
    
    public function __construct()
	{
        parent::__construct();
        
        /**
         * 
         * внешний вид параметров:
         * brand-brandUrl1_brandUrl2... - фильтр по брендам
         * paramUrl-paramValue1_paramValue2... - фильтр по мультисвойствам
         * page-pageNumber - постраничная навигация
         * sort-sortParam - параметры сортировки
         * 
         */
        $is_brands_page = 0;
        
        //определение текущего положения и выставленных параметров
        $uri = @parse_url($_SERVER["REQUEST_URI"]);
        //убираем модификатор каталога
        if(strpos($uri['path'], '/catalog/')===0){
            $uri = preg_replace("~/?catalog/~",'',$uri['path']);
        }elseif(strpos($uri['path'], '/brands/')===0){
            $uri = preg_replace("~/?brands~",'',$uri['path']);
            $is_brands_page = 1;
        }
        $this->design->assign('is_brands_page', $is_brands_page);
        
        $uri_array = explode('/',$uri);
        array_shift($uri_array);

        //обнуляем бренд (непонятна для меня причина почему в ГЕТ уже есть бренд)
        $_GET['brand'] = '';

        foreach($uri_array as $k=>$v){
            if(empty($v)) continue;
            if(!$k && $brand=$this->brands->get_brand((string)$v)){
                $_GET['brand'] = $brand->url;
            }else{
                //list($param_name, $param_values) = explode('-',$v);
                
                $url_array = explode('-',$v);
                $param_name = $url_array[0];
                unset($url_array[0]);
                $param_values = implode('-', $url_array);

                switch($param_name){
                    case 'brand':
                        foreach(explode('_',$param_values) as $bv)
                            if($brand = $this->brands->get_brand((string)$bv)){
                                $_GET['b'][] = $brand->id;
                                if(!$is_brands_page)
                                $this->meta_array['brand'][] = 'Бренд '. $brand->name;
                            }
                        break;
                    case 'page':
                        $_GET['page'] = $param_values;
                        break;
                    case 'sort':
                        $_GET['sort'] = strval($param_values);
                        break;
                    case 'v_nalichii':
                        $_GET['in_stock'] = 1;
                        $this->meta_array['status'][] = 'в наличии';
                        break;
                    case 'so_skidkoi':
                        $_GET['discounted'] = 1;
                        $this->meta_array['status'][] = 'со скидкой';
                        break;
                    case 'hit_prodaj':
                        $_GET['featured'] = 1;
                        $this->meta_array['status'][] = 'хит продаж';
                        break;
                    case 'min_price':
                        $_GET['min_price'] = floatval($param_values);
                        break;
                    case 'max_price':
                        $_GET['max_price'] = floatval($param_values);
                        break;        
                    default:
                        if($feature = $this->features->get_feature($param_name)){
                            $_GET[$feature->id] = explode('_',$param_values);
                            foreach($this->features->get_options(array('feature_id'=>$feature->id,'features'=>$_GET[$feature->id])) as $fo){
                                if(in_array($fo->translit,$_GET[$feature->id])){
                                    $this->meta_array['options'][$feature->id][] = $feature->name . ' '. $fo->value;
                                }
                            }
                        }                  
                }
            }
        }
//if($_SESSION['admin']){var_dump($_GET);}
        if(!empty($this->meta_array)){
            foreach($this->meta_array as $type=>$_meta_array){
                switch($type){
                    case 'brand':
                        if(count($_meta_array) > 1 )
                            $this->set_canonical = true;
                        $this->meta['h1'] = $this->meta['title'] = $this->meta['keywords'] = $this->meta['description'] = implode($this->meta_delimiter,$_meta_array); 
                        break;
                    case 'options':
                        foreach($_meta_array as $f_id=>$f_array){
                            if(count($f_array) > 1)
                                $this->set_canonical = true;
                            $this->meta['h1']           .= (!empty($this->meta['h1'])           ? $this->meta_delimiter : '') . implode($this->meta_delimiter,$f_array);
                            $this->meta['title']        .= (!empty($this->meta['title'])        ? $this->meta_delimiter : '') . implode($this->meta_delimiter,$f_array);
                            $this->meta['keywords']     .= (!empty($this->meta['keywords'])     ? $this->meta_delimiter : '') . implode($this->meta_delimiter,$f_array);
                            $this->meta['description']  .= (!empty($this->meta['description'])  ? $this->meta_delimiter : '') . implode($this->meta_delimiter,$f_array);
                        }
                        break;
                    case 'status':
                        if(count($_meta_array) > 1)
                            $this->set_canonical = true;
                        $this->meta['h1'] = $this->meta['title'] = $this->meta['keywords'] = $this->meta['description'] = implode($this->meta_delimiter,$_meta_array); 
                        break;    
                }
            }
        }
        
        if(!empty($this->meta['h1']) && !$is_brands_page)
            $this->meta['h1']           = ' c характеристиками ' . $this->meta['h1']          . ' в магазине ' . $this->settings->site_name;
        if(!empty($this->meta['title']))
            $this->meta['title']        = ' c характеристиками ' . $this->meta['title']       . ' в магазине ' . $this->settings->site_name;
        if(!empty($this->meta['keywords']))
            $this->meta['keywords']     = ' c характеристиками ' . $this->meta['keywords']    . ' в магазине ' . $this->settings->site_name;
        if(!empty($this->meta['description']))
            $this->meta['description']  = ' c характеристиками ' . $this->meta['description'] . ' в магазине ' . $this->settings->site_name;
        
        if($this->set_canonical)
            $this->meta['h1'] = $this->meta['title'] = $this->meta['keywords'] = $this->meta['description'] = '';
        
        $this->design->assign('set_canonical',$this->set_canonical);
        $this->design->assign('filter_meta',(object)$this->meta);

        $this->design->smarty->registerPlugin('function', 'furl', array($this, 'filter_chpu_url'));
        
    }
    public function filter_chpu_url($params)
    {
        
		if(is_array(reset($params)))
			$params = reset($params);
        
        $result_array = array('brand'=>array(),'features'=>array(),'sort'=>null,'page'=>null);
        //Определяем, что у нас уже есть в строке
        $uri = @parse_url($_SERVER["REQUEST_URI"]);
        
        if(strpos($uri['path'], '/catalog/')===0){
            $uri = preg_replace("~/?catalog/~",'',$uri['path']);
            $result_string = '/catalog';
        }elseif(strpos($uri['path'], '/brands/')===0){
            $uri = preg_replace("~/?brands~",'',$uri['path']);
             $result_string = '/brands';
        }
        
        //$uri = preg_replace("~/?catalog/~",'',$uri['path']);
        $uri_array = explode('/',$uri);
        array_shift($uri_array);
        foreach($uri_array as $k=>$v){
            if($k>0 || !($brand=$this->brands->get_brand((string)$v))){
                list($param_name, $param_values) = explode('-',$v);
                switch($param_name){
                    case 'brand':
                        $result_array['brand'] = explode('_',$param_values);
                        break;
                    case 'sort':
                        $result_array['sort'] = strval($param_values);
                        break;
                    case 'page':
                        $result_array['page'] = $param_values;
                        break;
                    default:
                        $result_array['features'][$param_name] = explode('_',$param_values);                   
                }
            }
        }
        //Определяем переданные параметры для ссылки
        foreach($params as $k=>$v){
            switch($k){
                case 'brand':
                    if(is_null($v))
                        unset($result_array['brand']);
                    elseif(in_array($v,$result_array['brand']))
                        unset($result_array['brand'][array_search($v,$result_array['brand'])]);
                    else
                        $result_array['brand'][] = $v;
                    break;
                case 'sort':
                    $result_array['sort'] = strval($v);
                    break;
                case 'page':
                    $result_array['page'] = $v;
                    break;
                default:
                    if(is_null($v))
                        unset($result_array['features'][$k]);
                    elseif(!empty($result_array['features']) && in_array($k,array_keys($result_array['features'])) && in_array($v,$result_array['features'][$k]))
                        unset($result_array['features'][$k][array_search($v,$result_array['features'][$k])]);
                    else
                        $result_array['features'][$k][] = $v;
                    break;
            }
        }
        //формируем ссылку
        
        if($_GET['keyword']){
            $result_string = "/products";
        }
        
        if(!empty($_GET['category']))
            $result_string .= '/' . $_GET['category']; 
        if(!empty($_GET['brand']))
            $result_string .= '/' . $_GET['brand'];
        
        if(!empty($result_array['brand']))
            $result_string .= '/brand-' . implode('_',$this->filter_chpu_sort_brands($result_array['brand'])); // - это с сортировкой по брендам
            //$result_string .= '/brand-' . implode('_',$result_array['brand']); // - это без сортировки по брендам
        foreach($result_array['features'] as $k=>$v){
            if(empty($result_array['features'][$k]))
                unset($result_array['features'][$k]);
        }
        if(!empty($result_array['features']))
            $result_string .= $this->filter_chpu_sort_features($result_array['features']);
        if(!empty($result_array['sort']))
            $result_string .= '/sort-' . $result_array['sort'];
        if($result_array['page'] > 1 || $result_array['page'] == 'all')
            $result_string .= '/page-' . $result_array['page'];
        
        if($_GET['keyword']){    
            $result_string .= '?keyword='.$_GET['keyword'];
        }
            
        //отдаем сформированную ссылку
        return $result_string;
    }
    private function filter_chpu_sort_brands($brands_urls = array()){
        if(empty($brands_urls))
            return false;
        $this->db->query("SELECT url FROM __brands WHERE url in(?@) ORDER BY name", (array)$brands_urls);
        return $this->db->results('url');
    }
    private function filter_chpu_sort_features($features = array()){
        if(empty($features))
            return false;
        $this->db->query("SELECT url FROM __features WHERE url in(?@) ORDER BY position", (array)array_keys($features));
        $result_string = '';
        foreach($this->db->results('url') as $v){
            if(in_array($v,array_keys($features))){
                $result_string .= '/' . $v . '-' . implode('_',$features[$v]);
            }
        }
        return $result_string;
    }
    /* chpu_filter_extended /*/   
 	/**
	 *
	 * Отображение списка товаров
	 *
	 */	
	function fetch()
	{
		// GET-Параметры
		$category_url = $this->request->get('category', 'string');
		$brand_url    = $this->request->get('brand', 'string');
		$brand_id	  = $this->request->get('brand_id');
   		$mode    = $this->request->get('mode', 'string');
		$mark_url    = $this->request->get('mark', 'string');

		$filter = array();
		$filter['visible'] = 1;	
		$filter['brand_id'] = array();
        $filter['pricenotnull'] = 1;
        if($this->settings->view_in_stock){
            $filter['in_stock'] = 1;
        }elseif($this->request->get('in_stock')){
            $filter['in_stock'] = 1;
        }
		if ($mode == 'hits')
		{
			$filter['featured'] = 1;
		}

		if ($mode == 'new')
		{
			$filter['new'] = 1;
		}
		if ($mode == 'sale')
		{
			$filter['discounted'] = 1;
		}    

		// Если задан бренд, выберем его из базы
		/* chpu_filter_extended */
        //$prices = array();
        //$prices['current'] = $this->request->get('p');
        ///if (!empty($prices['current']['min']) && !empty($prices['current']['max']))
        //    $filter['price'] = $prices['current'];
        //else
        //    unset($prices['current']);
        
        if ($val = $this->request->get('b')){
            $filter['brand_id'] = $val;
            $brand = $this->brands->get_brand(intval($val[0]));
            $this->design->assign('brand', $brand);
        }else/* chpu_filter_extended /*/if (!empty($brand_url)){

			$brand = $this->brands->get_brand((string)$brand_url);
			if (empty($brand))
				return false;
			$stickers = $this->stickers->get_stickers(array("visible" => 1, "brand_id" => $brand->id));
			if($stickers) {
				$sticker = $stickers[array_rand($stickers)];
				$this->design->assign('sticker', $sticker);
			}
			$this->design->assign('brand', $brand);
			$this->design->assign('brandix', $brand->id);
			$filter['brand_id'][] = $brand->id;
		}
		else
		{
			$this->design->assign('brandix', "0");
		}
		
		if (!empty($brand_id)) {
			$filter['brand_id'] = array_merge($filter['brand_id'], $this->request->get('brand_id'));
		}
		
		// Выберем текущую категорию
		if (!empty($category_url))
		{
			$category = $this->categories->get_category((string)$category_url);
			if (empty($category) || (!$category->visible && empty($_SESSION['admin'])))
				return false;
			$this->design->assign('category', $category);
			$filter['category_id'] = $category->children;
		}elseif(!empty($filter['brand_id'])){
                $categories_in_brand = $this->categories->get_brand_categories($filter['brand_id']);
                $all_path_cids = array();
                foreach($categories_in_brand as $cib){
                    foreach($cib->path as $cid_path){
                        $all_path_cids[$cid_path->id] = $cid_path->id;
                    }
                }
                //if($_SESSION['admin']) var_dump($all_path_cids);
            $this->design->assign('categories_in_brand', $all_path_cids);
        }
		
		// Если задана метка, выберем ее из базы
		if (!empty($mark_url))
		{

			$mark = $this->marks->get_mark((string)$mark_url);
			if (empty($mark))
				return false;
			$this->design->assign('mark', $mark);
			$filter['mark_id'] = $mark->id;

			if ($mark->category != $category->id)return false;
		}
		
		// Если задано ключевое слово
		$keyword = $this->request->get('keyword');
		if (!empty($keyword))
		{
			$this->design->assign('keyword', $keyword);
			$filter['keyword'] = $keyword;
            unset($filter['in_stock']);
		}

		if ($this->request->get('featured', 'integer'))
			$filter['featured'] = 1;
			
		if ($this->request->get('discounted', 'integer'))
			$filter['discounted'] = 1;

		if ($this->request->get('min_price', 'integer'))
			$filter['min_price'] = $this->request->get('min_price', 'integer') * $this->currency->rate_to/$this->currency->rate_from;
				
		if ($this->request->get('max_price'))
			$filter['max_price'] = $this->request->get('max_price', 'integer') * $this->currency->rate_to/$this->currency->rate_from;

        //Выбираем дочерние категории
		if(!empty($category)){
    		$cat_child = $this->categories->get_child_categories($category->id);
    		$this->design->assign('cat_child', $cat_child); 
		}
		
		// Выбираем метки, они нужны нам в шаблоне	
		$fil['category_id'][0] = $category->id;
        $fil['visible']=1;
		$marks = $this->marks->get_marks($fil);
		$this->design->assign('marks', $marks);	
        
		// Сортировка товаров, сохраняем в сесси, чтобы текущая сортировка оставалась для всего сайта
		if($sort = $this->request->get('sort', 'string'))
			$_SESSION['sort'] = $sort;		
		if (!empty($_SESSION['sort']))
			$filter['sort'] = $_SESSION['sort'];			
		else{
            if($category->id == 5){
                $filter['sort'] = 'position';
                $filter["field_for_front"] = 1;
                $filter['category_id'] = 5;
            }else
                $filter['sort'] = 'position';
        }			
		$this->design->assign('sort', $filter['sort']);
        
//Скрываем продукты в больших категориях        
if(!$category->hide_products || $brand) {
    
		// Свойства товаров
		if(!empty($category))
		{
			$features = array();
			$filter['features'] = array();
            if($category->filter_id){
                $the_features = $this->features->get_features(array('filter_id'=>$category->filter_id));    
            }else{
                $the_features = $this->features->get_features(array('category_id'=>$category->id, 'in_filter'=>1));
            }

			foreach($the_features as $feature)
			{ 
				$features[$feature->id] = $feature;
				//if(($val = strval($this->request->get($feature->id)))!='')
				/* chpu_filter_extended */
                /*if(($val = strval($this->request->get($feature->id)))!='')*/
                if($val = $this->request->get($feature->id))
                /* chpu_filter_extended /*/
				{
					if($val[0] != '')
						$filter['features'][$feature->id] = (array)$val;
								
					$features[$feature->id]->active = true;
				}
				else
				{
					$features[$feature->id]->active = false;
				}
			}
			
			$options_filter['visible'] = 1;
            $options_filter['in_stock'] = 1;
			
			$features_ids = array_keys($features);
			if(!empty($features_ids))
				$options_filter['feature_id'] = $features_ids;

            if($category->filter_id){
                $options_filter['filter_id'] = $category->filter_id;
                $options_filter['category_id'] = $category->children;
            }else{
                $options_filter['category_id'] = $category->children;
            }
            if(!empty($brand))
				$options_filter['brand_id'] = $brand->id;
            /* chpu_filter_extended */
            elseif($filter['brand_id'])
                $options_filter['brand_id'] = $filter['brand_id'];
            /* chpu_filter_extended /*/
			if(isset($filter['features']))
				$options_filter['features'] = $filter['features'];
			if(isset($filter['min_price']))
                $options_filter['min_price'] = $filter['min_price'];
            if(isset($filter['max_price']))
                $options_filter['max_price'] = $filter['max_price'];
            
            $options = $this->features->get_options($options_filter);
			
			if(isset($options)) {
				foreach($options as &$option) {
					// в этой групе есть чекнутый фильтер
					if($features[$option->feature_id]->active) {

						if( in_array($option->translit, $filter['features'][$option->feature_id])){
							$option->checked = true;
							$option->disabled = false;
							$option->count = 0;
						}else{
							//$temp_filter = $filter;
							//$temp_filter['features'][$option->feature_id] = (array)$option->value;
							//$option->count = '+'.$this->products->count_products($temp_filter);
                            if((int)$option->count > 0){
                                $option->disabled = false;
                            }else{
                                $option->disabled = true;
                                $option->count = 0;
                            }
						}
					} else {
						//$temp_filter = $filter;
						//$temp_filter['features'][$option->feature_id] = (array)$option->value;
						//$option->count = $this->products->count_products($temp_filter);
							if((int)$option->count > 0)
								$option->disabled = false;
							else
								$option->disabled = true;
						//unset($temp_filter);
						$option->checked = false;
					}
						
					$features[$option->feature_id]->options[] = $option;
				}
			}
			

			
			foreach($features as $i=>&$feature)
			{ 
				if(empty($feature->options))
					unset($features[$i]);
			}

			$this->design->assign('features', $features);
	        
			//Минимальная и максимальная допустимая цена
			$this->design->assign('max_min_price', $this->products->max_min_products($filter));	
			
			//Минимальная и максимальная допустимая цена
			$this->design->assign('slider_max_min_price', $this->products->max_min_products(array('category_id'=>$filter['category_id'], 'visible'=>1)));	
 		}
     
        /*Кнопка загрузить больше*/
        if($this->request->get('load_more')){
            $this->design->assign('load_more', 1);
            $current_page = $this->request->get('page', 'integer');
            $items_per_page = $this->settings->products_num;
            $count_loaded_items = $items_per_page * $current_page;
            $this->design->assign('current_page_num', $current_page);
            
            $products_count = $this->products->count_products($filter);
           
            $filter['limit'] = $count_loaded_items;
//var_dump($filter);             
      		// Показать все страницы сразу
    		if($this->request->get('page') == 'all')
    			$items_per_page = $products_count;	
    		
    		$pages_num = ceil($products_count/$items_per_page);
    		$this->design->assign('total_pages_num', $pages_num);
           
            if($pages_num > $current_page){
                $this->design->assign('is_more', 1);
                $this->design->assign('next_page', $current_page+1);
            }
        }else{
    		// Постраничная навигация
    		$items_per_page = $this->settings->products_num;		
    		// Текущая страница в постраничном выводе
    		$current_page = $this->request->get('page', 'integer');	
    		// Если не задана, то равна 1
    		$current_page = max(1, $current_page);
    		$this->design->assign('current_page_num', $current_page);
    		// Вычисляем количество страниц
    
    		$products_count = $this->products->count_products($filter);
    		
    		// Показать все страницы сразу
    		if($this->request->get('page') == 'all')
    			$items_per_page = $products_count;	
    		
    		$pages_num = ceil($products_count/$items_per_page);
    		$this->design->assign('total_pages_num', $pages_num);
            
            if($pages_num > 1 && $current_page < $pages_num){
                $this->design->assign('is_more', 1);
            }
    
    		$filter['page'] = $current_page;
    		$filter['limit'] = $items_per_page;

    		///////////////////////////////////////////////
    		// Постраничная навигация END
    		///////////////////////////////////////////////
        }

		

		$discount = 0;
		if(isset($_SESSION['user_id']) && $user = $this->users->get_user(intval($_SESSION['user_id'])))
			$discount = $user->discount;
			
		// Товары 
		$products = array();
        
		foreach($this->products->get_products($filter) as $p){
            //$brand = $this->brands->get_brand(intval($p->brand_id));
            //$p->name = $p->type_prefix.' '.$brand->name.' '.$p->name;
			$products[$p->id] = $p;
        }
		
		// Если искали товар и найден ровно один - перенаправляем на него
		if(!empty($keyword) && $products_count == 1)
			header('Location: '.$this->config->root_url.'/products/'.$p->url);
		
		if(!empty($products))
		{
			$products_ids = array_keys($products);
			foreach($products as &$product)
			{
				$product->variants = array();
				$product->images = array();
				$product->properties = array();
			}
            //@author S.A
            //change palitra + photo
            //	return $this->db->results();
            //start
//			$variants = $this->variants->get_variants(array('product_id'=>$products_ids ));
//
//			foreach($variants as &$variant)
//			{
//				//$variant->price *= (100-$discount)/100;
//				$products[$variant->product_id]->variants[] = $variant;
//			}
//
//			$images = $this->products->get_images(array('product_id'=>$products_ids));
//			foreach($images as $image)
//				$products[$image->product_id]->images[] = $image;

            $images = $this->products->get_images(array('product_id'=>$products_ids));
            foreach($images as $image)
                $products[$image->product_id]->images[] = $image;

            $variants = $this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));

            foreach($variants as &$variant)
            {
 
                if(!empty($variant->image_id) && isset($images[$variant->image_id]))
                    $variant->image = $images[$variant->image_id];
                $products[$variant->product_id]->variants[] = $variant;
            }

//end
			foreach($products as &$product)
			{			
				if(isset($product->variants[0]))
					$product->variant = $product->variants[0];
				if(isset($product->images[0]))
					$product->image = $product->images[0];
                    
                /*auto_discount*/
                //$product = $this->calc_auto_discount($product);
                /*auto_discount*/
                
                /////////////////////////////////////////////////////////////////////
                //Определяем лучший купон для этого товара
                $coupons = $this->coupons->calc_product_coupon($product->id);
                $selected_coupon_id = key($coupons);
                $coupon_discount_price = reset($coupons);
                $selected_coupon = $this->coupons->get_coupon(intval($selected_coupon_id));        
                /////////////////////////////////////////////////////////////////////
                 
                //применяем купон для вариантов
                foreach($product->variants as &$variant3){
                    if($selected_coupon->type == 'percentage'){
                        $variant3->auto_coupon_price = max(0, round($variant3->price * (100-$selected_coupon->value)/100, 0));
                        $variant3->coupon_discount_price = round($variant3->price * $selected_coupon->value / 100, 0);
                    }elseif($selected_coupon->type == 'absolute'){
                        $variant3->auto_coupon_price = max(0, ($variant3->price - $selected_coupon->value));
                        $variant3->coupon_discount_price = $selected_coupon->value;
                    }
                    $variant3->coupon_id = $selected_coupon_id;
                }
                $product->variant->coupon_discount_price = $coupon_discount_price;
                $product->variant->coupon_id = $selected_coupon_id;
                $product->variant->auto_coupon_price = max(0, $product->variant->price - $coupon_discount_price);
			}
				
	
			//$ofeatures = $this->features->get_features(array('product_id'));
			$properties = $this->features->get_product_options($products_ids);
//print_r($properties);            
			foreach($properties as $property){
                //$property->feature_name = 
				$products[$property->product_id]->options[] = $property;
			}
			
			 
	
			$this->design->assign('products', $products);
 		}
//print_r($properties);		
		
		

			
		  
		// Выбираем бренды, они нужны нам в шаблоне	

    //if($this->request->get('ajax','boolean')){
    if(!empty($category))
    {
        /* sshop_2016_08_17 */
        /*$brands = $this->brands->get_brands(array('category_id'=>$category->children));
        $category->brands = $brands;
            //Есть ли активный бренд в фильтре
            if(!empty($filter['brand_id'])){
                $brands_active = true;
                $count_prefix = '+';
            }else{
                $count_prefix = '';
            }*/
        //////////////////



        /*foreach($category->brands as &$brand_temp){

            if(isset($brands_active)){
                if(in_array($brand_temp->id, $filter['brand_id'])){ //если активен текущий
                    $brand_temp->checked = true;
                    $brand_temp->disabled = false;
                    $brand_temp->count = 0;
                } else {
                    $brand_temp->checked = false;
                    $temp_filter = $filter;
                    $temp_filter['brand_id'] = (array)$brand_temp->id;
                    $brand_temp->count = $count_prefix.$this->products->count_products($temp_filter);
                    unset($temp_filter);
                    if((int)$brand_temp->count > 0){
                        $brand_temp->disabled = false;
                    }else{
                        $brand_temp->disabled = true;
                        $brand_temp->count = 0;
                    }
                }

            }else{
                $brand_temp->checked = false;
                $temp_filter = $filter;
                $temp_filter['brand_id'] = (array)$brand_temp->id;
                $brand_temp->count = $count_prefix.$this->products->count_products($temp_filter);
                unset($temp_filter);
                    if((int)$brand_temp->count > 0)
                        $brand_temp->disabled = false;
                    else
                        $brand_temp->disabled = true;
            }
        }*/
        $brands = array();
        foreach($this->brands->get_brands(array('category_id'=>$category->children)) as $b){
            $brands[$b->id] = $b;
        }
        if(!empty($brands)){
            $category->brands = $brands;
            //Есть ли активный бренд в фильтре
            if(!empty($filter['brand_id'])){
                $brands_active = true;
                $count_prefix = '+';
            }else{
                $count_prefix = '';
            }
            $brands_filter = $filter;
            $brands_filter['brand_id'] = array_keys($brands);
            $brands_filter['check_brands'] = 1;
            $brands_count = array();
            foreach($this->products->count_products($brands_filter) as $bc){
                $brands_count[$bc->id] = $bc->count;
            }

            foreach($category->brands as &$brand_temp){
                if(isset($brands_active)){
                    if(in_array($brand_temp->id, $filter['brand_id'])){ //если активен текущий
                        $brand_temp->checked = true;
                        $brand_temp->disabled = false;
                        $brand_temp->count = 0;
                    } else {
                        $brand_temp->checked = false;
                        $brand_temp->count = $count_prefix.intval($brands_count[$brand_temp->id]);
                        unset($temp_filter);
                        if((int)$brand_temp->count > 0){
                            $brand_temp->disabled = false;
                        }else{
                            $brand_temp->disabled = true;
                            $brand_temp->count = 0;
                        }
                    }

                }else{
                    $brand_temp->checked = false;
                    $brand_temp->count = $count_prefix.intval($brands_count[$brand_temp->id]);
                    unset($temp_filter);
                    if((int)$brand_temp->count > 0)
                        $brand_temp->disabled = false;
                    else
                        $brand_temp->disabled = true;
                }
            }
        }
        /* sshop_2016_08_17 /*/

        $in_stock = new stdClass();
        if ($this->request->get('in_stock', 'integer'))
        {
            $in_stock->checked = true;
            $in_stock->disabled = false;
            $in_stock->count = 0;
        }
        else
        {
            $temp_filter = $filter;
            $temp_filter['in_stock'] = 1;
            $in_stock->count = $this->products->count_products($temp_filter);
            if((int)$in_stock->count > 0)
            {
                $in_stock->disabled = false;
            }
            else
            {
                $in_stock->disabled = true;
                $in_stock->count = 0;
            }
            unset($temp_filter);
        }
        $this->design->assign('in_stock', $in_stock);

        $featured = new stdClass();
        if ($this->request->get('featured', 'integer'))
        {
            $featured->checked = true;
            $featured->disabled = false;
            $featured->count = 0;
        }
        else
        {
            $temp_filter = $filter;
            $temp_filter['featured'] = 1;
            $featured->count = $this->products->count_products($temp_filter);
            if((int)$featured->count > 0)
            {
                $featured->disabled = false;
            }
            else
            {
                $featured->disabled = true;
                $featured->count = 0;
            }
            unset($temp_filter);
        }
        $this->design->assign('featured', $featured);

        $discounted = new stdClass();
        if ($this->request->get('discounted', 'integer'))
        {
            $discounted->checked = true;
            $discounted->disabled = false;
            $discounted->count = 0;
        }
        else
        {
            $temp_filter = $filter;
            $temp_filter['discounted'] = 1;
            $discounted->count = $this->products->count_products($temp_filter);
            if((int)$discounted->count > 0)
            {
                $discounted->disabled = false;
            }
            else
            {
                $discounted->disabled = true;
                $discounted->count = 0;
            }
            unset($temp_filter);
        }
        $this->design->assign('discounted', $discounted);

        if($category->filter_id){
            $product_filter = $this->filters->get_filter(intval($category->filter_id));
            $product_filter_features = $this->filters->get_filter_features($product_filter->id);
            foreach($product_filter_features as $pff){
                $product_filter->features[$pff->feature_id] = $pff;
            }
            $this->design->assign('filter', $product_filter);
        }

    }
//            $this->design->assign('ajax', 1);
//            $result = new StdClass;
//            $result->products_content = $this->design->fetch('products_content.tpl');
//            $result->products_pagination = $this->design->fetch('chpu_pagination.tpl');
//            print json_encode($result);
//            die;
//        }
        
        //
 }
        $category_ = new stdClass();
        $category_->skl = unserialize($category->skl);
        //if($_SESSION['admin']) var_dump($category->skl);
        $replaces = ARRAY(
            '{l-category}'=>mb_strtolower($category->name, 'UTF-8'),
            '{category}'=>$category->name,
            '{l-skl}'=>mb_strtolower($category_->skl['skl']),
            '{l-skl_mn}'=>mb_strtolower($category_->skl['skl_mn']),
            '{l-skl_ov}'=>mb_strtolower($category_->skl['skl_ov']),
            '{skl}'=>$category_->skl['skl'],
            '{skl_mn}'=>$category_->skl['skl_mn'],
            '{skl_ov}'=>$category_->skl['skl_ov'],
            '{rus_brand}'=>$brand->rus_name
        );

              
/*SEO*/	if(isset($brand) AND isset($category))
/*SEO*/	{
    
            //$category->meta_title = $brand->meta_title = str_replace(array_keys($replaces),array_values($replaces), $category->meta_title);
            $category->meta_title = $brand->meta_title = str_replace(array_keys($replaces),array_values($replaces), $this->settings->title_category_brand); 
            $category->meta_keywords = $brand->meta_keywords = str_replace(array_keys($replaces),array_values($replaces), $this->settings->keywords_category_brand);
            //$category->meta_description = $brand->meta_description = str_replace(array_keys($replaces),array_values($replaces), $category->meta_description);
            $category->meta_description = $brand->meta_description = str_replace(array_keys($replaces),array_values($replaces), $this->settings->meta_description_category_brand); 
    
/*SEO*/		$category->meta_title = $brand->meta_title = 			 str_replace("{brand}","".$brand->name,$category->meta_title);
/*SEO*/		$category->meta_keywords = $brand->meta_keywords = 		 str_replace("{brand}","".$brand->name,$category->meta_keywords);
/*SEO*/		$category->meta_description = $brand->meta_description = str_replace("{brand}","".$brand->name,$category->meta_description);
/*SEO*/		$category->description_footer = 						 str_replace("{brand}","".$brand->name,$category->description_footer);
/*SEO*/		$category->description = (trim($brand->description_in_categories[$category->id])!='')?$brand->description_in_categories[$category->id]:$category->description;
/*SEO*/		$brand->description = '';

/*SEO*/	}
/*SEO*/	elseif(empty($brand) AND isset($category))
/*SEO*/	{

            $category->meta_title = str_replace(array_keys($replaces),array_values($replaces), $category->meta_title);
            $category->meta_keywords = str_replace(array_keys($replaces),array_values($replaces), $category->meta_keywords);
            $category->meta_description = str_replace(array_keys($replaces),array_values($replaces), $category->meta_description);

/*SEO*/		$category->meta_title =        str_replace("{brand}","",$category->meta_title);
/*SEO*/		$category->meta_keywords =     str_replace("{brand}","",$category->meta_keywords);
/*SEO*/		$category->meta_description =  str_replace("{brand}","",$category->meta_description);
/*SEO*/		$category->description_footer = str_replace("{brand}","",$category->description_footer);
/*SEO*/	}

		// Устанавливаем мета-теги в зависимости от запроса
		if($this->page)
		{
		    
			$this->design->assign('meta_title', $this->page->meta_title);
			$this->design->assign('meta_keywords', $this->page->meta_keywords);
			$this->design->assign('meta_description', $this->page->meta_description);
		}
		elseif(isset($mark))
		{
		
			$this->design->assign('meta_title', $mark->meta_title);
			$this->design->assign('meta_keywords', $mark->meta_keywords);
			$this->design->assign('meta_description', $mark->meta_description);
		}
		elseif(isset($category))
		{
            //if($_SESSION['admin']){var_dump($category->description);}
			$this->design->assign('meta_title', $category->meta_title);
			$this->design->assign('meta_keywords', $category->meta_keywords);
			$this->design->assign('meta_description', $category->meta_description);
		}
		elseif(isset($brand))
		{   
			$this->design->assign('meta_title', $brand->meta_title);
			$this->design->assign('meta_keywords', $brand->meta_keywords);
			$this->design->assign('meta_description', $brand->meta_description);
		}
		elseif(isset($keyword))
		{
			$this->design->assign('meta_title', $keyword);
		}  
        
        if($this->request->get('load_more')){
            $result = $this->design->fetch('load_more.tpl');
            print($result);
            die;
        }
		//if($_SESSION['admin']) var_dump($category->description); 
		$this->body = $this->design->fetch('products.tpl');
		return $this->body;
	}
}
