<?php

/**
 * Simpla CMS
 *
 * @copyright 	2009 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Корзина покупок
 * Этот класс использует шаблон cart.tpl
 *
 */
//ini_set('display_errors',1);
require_once('View.php');

class CartView extends View
{
  //////////////////////////////////////////
  // Изменения товаров в корзине
  //////////////////////////////////////////
  public function __construct()
  {
	parent::__construct();

    // Если передан id варианта, добавим его в корзину
    if($variant_id = $this->request->get('variant', 'integer'))
    {
		$this->cart->add_item($variant_id, $this->request->get('amount', 'integer'));
		if($this->request->get('credit', 'integer')==1)
	    header('location: '.$this->config->root_url.'/cart?credit=1');
		else
		header('location: '.$this->config->root_url.'/cart/');
    }

    // Удаление товара из корзины
    if($delete_variant_id = intval($this->request->get('delete_variant')))
    {
      $this->cart->delete_item($delete_variant_id);
      if(!isset($_POST['submit_order']) || $_POST['submit_order']!=1)
			header('location: '.$this->config->root_url.'/cart/');
	}
	
    //var_dump($_POST);die; 
    if($_POST['IsFastOrder'])
    {
        $order->name = $this->request->post('name');
        $order->phone = preg_replace('/\W/', '', $this->request->post('phone'));
        $order->comment = "Заказ в 1 клик";
        $order->ip    	= $_SERVER['REMOTE_ADDR'];

        //$this->design->assign('delivery_id', $order->delivery_id);
        $this->design->assign('name', $order->name);
        //$this->design->assign('email', $order->email);
        $this->design->assign('phone', $order->phone);
        $this->design->assign('address', $order->address);

        $order->email="_______";
        $order->address = "_______";
        $order->comment = "Быстрый заказ";    
        $order->isfastorder = 1; 
        $order->discount_type = 1;                   
        
        //$captcha_code =  $this->request->post('captcha_code', 'string');

        // Скидка
        $cart = $this->cart->get_cart();
        $order->discount = $cart->discount;

        if($cart->coupon){
            $order->coupon_discount = $cart->coupon_discount;
            $order->coupon_code = $cart->coupon->code;
        }                      
        
        if(!empty($this->user->id))
            $order->user_id = $this->user->id;

        /*if(empty($order->name)){
            $this->design->assign('error', 'empty_name');
        }else*/if (empty($order->phone)) {
            $this->design->assign('error', 'empty_phone');
        /*} elseif($_SESSION['captcha_code'] != $captcha_code || empty($captcha_code)){
            $this->design->assign('error', 'captcha');
         */
        }else{
                // Добавляем заказ в базу
            $order_id = $this->orders->add_order($order);
            $_SESSION['order_id'] = $order_id;
            
            //Получаем все автокупоны
            $auto_coupons = $this->request->post('auto_coupon'); 

            // Добавляем товары к заказу
	    	foreach($this->request->post('amounts') as $variant_id=>$amount)
	    	{
                //var_dump($variant_id);die;
	    		$this->orders->add_purchase(array('order_id'=>$order_id, 'variant_id'=>intval($variant_id), 'amount'=>intval($amount), 'auto_coupon'=>$auto_coupons[$variant_id]));
	    	}
	    	$order = $this->orders->get_order($order_id);

            // Отправляем письмо пользователю
            //$this->notify->email_order_user($order->id);
            // Отправляем письмо администратору
            $this->notify->email_order_admin($order->id);
            
            // Очищаем корзину (сессию)
			$this->cart->empty_cart();
            // Перенаправляем на страницу заказа
            header('Location: '.$this->config->root_url.'/order/'.$order->url);
        }                                
    /*/fastorder*/
    }

    // Если нажали оформить заказ
    elseif($_POST['checkout'] == 1)
    {
    	$order->payment_method_id = $this->request->post('payment_method_id', 'integer');
    	$order->delivery_id = $this->request->post('delivery_id', 'integer');
    	$order->name        = $this->request->post('name');
        $order->surname     = $this->request->post('surname');
    	$order->email       = $this->request->post('email');
    	$order->address     = $this->request->post('address');
    	$order->phone       = preg_replace('/\W/', '', $this->request->post('phone'));
        $order->phone2      = preg_replace('/\W/', '', $this->request->post('phone2'));
    	$order->comment     = $this->request->post('comment');
		$order->passport    = $this->request->post('passport');
		$order->ip      	= getenv(HTTP_X_FORWARDED_FOR);
		$order->discount_type = 1;
		//getenv(HTTP_X_FORWARDED_FOR)
    	
		//die(print_r($order));
		$this->design->assign('delivery_id', $order->delivery_id);
		$this->design->assign('name', $order->name);
        $this->design->assign('surname', $order->surname);
		$this->design->assign('email', $order->email);
		$this->design->assign('phone', $order->phone);
        $this->design->assign('phone2', $order->phone2);
		$this->design->assign('address', $order->address);
        
        /*RECAPTCHA*/
		$recaptha_response = $this->request->post('g-recaptcha-response');
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $recaptcha_status = json_decode($this->recaptcha->send($recaptha_response, $remoteip));
        /*/RECAPTCHA*/
    
		// Скидка
		$cart = $this->cart->get_cart();
		$order->discount = $cart->discount;
		
		if($cart->coupon)
		{
			$order->coupon_discount = $cart->coupon_discount;
			$order->coupon_code = $cart->coupon->code;
		}
		//
    	
    	if(!empty($this->user->id))
	    	$order->user_id = $this->user->id;
    	
    	if(empty($order->name))
    	{
    		$this->design->assign('error', 'empty_name');
    	}
    	elseif(empty($order->email))
    	{
    		$this->design->assign('error', 'empty_email');
    	}
        elseif(empty($order->phone))
    	{
    		$this->design->assign('error', 'empty_phone');
    	}
        /*RECAPTCHA* /
		elseif(empty($recaptha_response) || !$recaptcha_status->success)
		{
			$this->design->assign('error', 'captcha');
		}
        /*RECAPTCHA*/
    	else
    	{
	    	// Добавляем заказ в базу
	    	$order_id = $this->orders->add_order($order);
	    	$_SESSION['order_id'] = $order_id;
	    	
	    	// Если использовали купон, увеличим количество его использований
	    	if($cart->coupon)
	    		$this->coupons->update_coupon($cart->coupon->id, array('usages'=>$cart->coupon->usages+1));
	    	
            //Получаем все автокупоны
            $auto_coupons = $this->request->post('auto_coupon'); 
            
	    	// Добавляем товары к заказу
	    	foreach($this->request->post('amounts') as $variant_id=>$amount)
	    	{
	    		$this->orders->add_purchase(array('order_id'=>$order_id, 'variant_id'=>intval($variant_id), 'amount'=>intval($amount), 'auto_coupon'=>$auto_coupons[$variant_id]));
	    	}
	    	$order = $this->orders->get_order($order_id);
	    	
	    	// Стоимость доставки
			$delivery = $this->delivery->get_delivery($order->delivery_id);
	    	if(!empty($delivery) && $delivery->free_from > $order->total_price)
	    	{
	    		$this->orders->update_order($order->id, array('delivery_price'=>$delivery->price, 'separate_delivery'=>$delivery->separate_payment));
	    	}
			
            /*autoregistration*/
            //Определяем ip пользователя
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip=$_SERVER['HTTP_CLIENT_IP']; 
            }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
            }else{ 
                $ip=$_SERVER['REMOTE_ADDR']; 
            }
            	
            	//Если мы не знаем юзера
            if(!$this->user) {
            	//Смотрим есть ли такой email в базе
            	$this->db->query('SELECT count(*) as count FROM s_users WHERE email=?', $order->email);
            	$user_exists = $this->db->result('count');
            	if($user_exists) { //Если нашли, то добавляем заказ к найденому пользователю
            		$this->db->query('SELECT * FROM s_users WHERE email=?', $order->email);
            		$user_exists_id = $this->db->result('id');
                    $this->orders->update_order($order->id, array('user_id'=>$user_exists_id));
                } else { 
                    //Если не нешли, то генерируем пароль, согдаем пользователя, отправляем ему письмо и добавляем заказ к созданому пользователю
                    $password=$order->phone;
                    
                    while($max--) $password.=$chars[rand(0,$size)]; 
                    
                    $user_id = $this->users->add_user(array('email'=>$order->email, 'password'=>$password, 'name'=>$order->name, 'surname'=>$order->surname, 'phone'=>$order->phone, 'phone2'=>$order->phone2, 'enabled'=>'1', 'last_ip'=>$ip, 'auto'=>1));
                    
                    $this->orders->update_order($order->id, array('user_id'=>$user_id));
                    //$this->notify->email_registration($user_id, $password);
                    //$_SESSION['user_id'] = $user_id;
                } 
              }
            /*autoregistration/*/
			// Отправляем письмо пользователю
			$this->notify->email_order_user($order->id);
	    	
			// Отправляем письмо администратору
			$this->notify->email_order_admin($order->id);

			// Отправка смс уведомления пользователю если он этого запросил
			if ($order->phone && $this->settings->sms_order_user)
			$this->smssend->send($order->phone, 'Ваш заказ №'.$order->id.' на сумму '.$order->total_price.'. В ближайшее время с Вами свяжется менеджер. Спасибо за покупку! Velocube.ru');
            if ($order->phone2 && $this->settings->sms_order_user)
			$this->smssend->send($order->phone2, 'Ваш заказ №'.$order->id.' на сумму '.$order->total_price.'. В ближайшее время с Вами свяжется менеджер. Спасибо за покупку! Velocube.ru');
			// Отправка смс уведомления администратору о поступившем заказе
            if ($order->phone && $this->settings->sms_order_admin){
                foreach($this->settings->sms_phone as $sms_phone){
        			$this->smssend->send($sms_phone, 'На сайт поступил заказ №'.$order->id.' на сумму '.$order->total_price.' руб.');
                }
            }	
	    	// Очищаем корзину (сессию)
			$this->cart->empty_cart();
						
			// Перенаправляем на страницу заказа
			header('Location: '.$this->config->root_url.'/order/'.$order->url);
		}
        
    }   
    else
    {

	    // Если нам запостили amounts, обновляем их
	    if($amounts = $this->request->post('amounts'))
	    {
			foreach($amounts as $variant_id=>$amount)
			{
				$this->cart->update_item($variant_id, $amount);         
			}

	    	$coupon_code = trim($this->request->post('coupon_code', 'string'));
	    	if(empty($coupon_code))
	    	{
	    		$this->cart->apply_coupon('');
				header('location: '.$this->config->root_url.'/cart/');	    		
	    	}
	    	else
	    	{
				$coupon = $this->coupons->get_coupon((string)$coupon_code);

				if(empty($coupon) || !$coupon->valid)
				{
		    		$this->cart->apply_coupon($coupon_code);
					$this->design->assign('coupon_error', 'invalid');
				}
				else
				{
					$this->cart->apply_coupon($coupon_code);
					header('location: '.$this->config->root_url.'/cart/');
				}
	    	}
		}
	    
	}
              
  }

  
	//////////////////////////////////////////
	// Основная функция
	//////////////////////////////////////////
	function fetch()
	{  
		// Способы доставки
		$deliveries = $this->delivery->get_deliveries(array('enabled'=>1));
		/*order_on_one_page*/
        foreach($deliveries as $delivery)
            $delivery->payment_methods = $this->payment->get_payment_methods(array('delivery_id'=>$delivery->id, 'enabled'=>1));
        $this->design->assign('all_currencies', $this->money->get_currencies());
        /*/order_on_one_page*/
		$this->design->assign('deliveries', $deliveries);
		$query = $this->db->placehold("SELECT id
									FROM __orders ORDER BY  id DESC");
		$this->db->query($query);
		$this->design->assign('last_order',$this->db->result('id')+1);		
		// Данные пользователя
		if($this->user)
		{
			$last_order = reset($this->orders->get_orders(array('user_id'=>$this->user->id, 'limit'=>1)));
			if($last_order)
			{
				$this->design->assign('name', $last_order->name);
                $this->design->assign('surname', $last_order->surname);
				$this->design->assign('email', $last_order->email);
				$this->design->assign('phone', $last_order->phone);
                $this->design->assign('phone2', $last_order->phone2);
				$this->design->assign('address', $last_order->address);
			}
			else
			{
				$this->design->assign('name', $this->user->name);
                $this->design->assign('surname', $this->user->surname);
				$this->design->assign('email', $this->user->email);
                $this->design->assign('phone', $this->user->phone);		
                $this->design->assign('phone2', $this->user->phone2);		
			}
		}
		
		// Если существуют валидные купоны, нужно вывести инпут для купона
		if($this->coupons->count_coupons(array('valid'=>1))>0)
			$this->design->assign('coupon_request', true);

		// Выводим корзину
		return $this->design->fetch('cart.tpl');
	}
	
}