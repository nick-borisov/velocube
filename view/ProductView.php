<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simplacms.ru
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон product.tpl
 *
 */
//ini_set('display_errors',1);
require_once('View.php');


class ProductView extends View
{

	function fetch()
	{   

		$product_url = $this->request->get('product_url', 'string');
		
		if(empty($product_url))
			return false;

		// Выбираем товар из базы
		$product = $this->products->get_product((string)$product_url);
		if(empty($product) || (!$product->visible && empty($_SESSION['admin'])))
			return false;
		
		$product->images = $this->products->get_images(array('product_id'=>$product->id));
	    //$product->image = &$product->images[0];
        $product->image = reset($product->images);

//	     $product->image = &$product->images[array_shift($product->images)->id];
//@author S.A
        //change palitra + photo
        //	return $this->db->results();
        //start
//		$variants = array();
//		foreach($this->variants->get_variants(array('product_id'=>$product->id, 'in_stock'=>true)) as $v)
//			$variants[$v->id] = $v;

        $variants = array();
        foreach($this->variants->get_variants(array('product_id'=>$product->id, 'in_stock'=>true, 'order'=>'stock')) as $v) {
            if(!empty($v->image_id) && isset($product->images[$v->image_id]))
                $v->image = $product->images[$v->image_id];
			/* Проверка наличия всех вариантов */
            if($v->stock == null || $v->stock > 0)
              $this->design->assign('available', 1);
            $variants[$v->id] = $v;
        } 

//end
		if(!$variants){
			$variants2 = $this->variants->get_variants(array('product_id'=>$product->id));
			$product->variant2 = $variants2[0];
		}
		

		$product->variants = $variants;
		$product->variants2 = $variants2;
        
        /////////////////////////////////////////////////////////////////////
        //Определяем лучший купон для этого товара
        $coupons = $this->coupons->calc_product_coupon($product->id);
        
        $selected_coupon_id = key($coupons);
        $coupon_discount_price = reset($coupons);
        $selected_coupon = $this->coupons->get_coupon(intval($selected_coupon_id));        
        /////////////////////////////////////////////////////////////////////
        
        //применяем купон для вариантов
        foreach($product->variants as &$variant3){
            //если есть интервалы для цены, работаем с ними
            if($selected_coupon->active_margin){
                $margins = unserialize($selected_coupon->margin);
				if($margins['min'][0] && $margins['max'][0]){  
                    foreach($margins['min'] as $i=>$min){
                        if($variant3->price > intval($margins['min'][$i]) && $variant3->price < intval($margins['max'][$i])){
                            if(intval($margins['mode'][$i])==1){
                                $variant3->auto_coupon_price = max(0, ($variant3->price - floatval($margins['price'][$i])));
                                $variant3->coupon_discount_price = floatval($margins['price'][$i]);
                            }elseif(intval($margins['mode'][$i])==2){
                                $variant3->coupon_discount_price = max(0, round($variant3->price * $margins['price'][$i] / 100, 2));
                                $variant3->auto_coupon_price = max(0, round($variant3->price * (100-$margins['price'][$i])/100, 0));
                            }
                        }
                    }
				}
            }else{//если интервалов нет
                if($selected_coupon->type == 'percentage'){
                    $variant3->auto_coupon_price = max(0, round($variant3->price * (100-$selected_coupon->value)/100, 0));
                    $variant3->coupon_discount_price = round($variant3->price * $selected_coupon->value / 100, 0);
                }elseif($selected_coupon->type == 'absolute'){
                    $variant3->auto_coupon_price = max(0, ($variant3->price - $selected_coupon->value));
                    $variant3->coupon_discount_price = $selected_coupon->value;
                }
            }    
            $variant3->coupon_id = $selected_coupon_id;
        }
        //$product->variant->coupon_discount_price = $coupon_discount_price;
        //$product->variant->coupon_id = $selected_coupon_id;
        //$product->variant->auto_coupon_price = max(0, $product->variant->price - $coupon_discount_price);
//if($_SESSION['admin']) {var_dump($product);}         
		
		// Вариант по умолчанию
		if(($v_id = $this->request->get('variant', 'integer'))>0 && isset($variants[$v_id]))
			$product->variant = $variants[$v_id];
		else
			$product->variant = reset($variants);
            			
		$product->features = $this->features->get_product_options(array('product_id'=>$product->id));
	
		// Автозаполнение имени для формы комментария
		if(!empty($this->user))
			$this->design->assign('comment_name', $this->user->name);
	 
		// Принимаем комментарий
		if ($this->request->method('post') && $this->request->post('comment'))
		{
		 
			$comment = new stdClass;
			$comment->name = $this->request->post('name');
			$comment->text = $this->request->post('text');
            $comment->dost = $this->request->post('dost');
            $comment->nedost = $this->request->post('nedost');
			
            /*RECAPTCHA*/
    		$recaptha_response = $this->request->post('g-recaptcha-response');
            $remoteip = $_SERVER['REMOTE_ADDR'];
            $recaptcha_status = json_decode($this->recaptcha->send($recaptha_response, $remoteip));
            /*/RECAPTCHA*/
            
			$rating = $this->request->post('rating');
			$comment->rating = $this->request->post('rating');
			$product_id = $product->id;
			
			$comment->parent_id = $this->request->post('parent_id');
			$comment->admin = $this->request->post('admin');
			//$captcha_code =  $this->request->post('captcha_code', 'string');
		
			// Передадим комментарий обратно в шаблон - при ошибке нужно будет заполнить форму
			$this->design->assign('comment_text', $comment->text);
			$this->design->assign('comment_name', $comment->name);
            $this->design->assign('comment_dost', $comment->dost);
            $this->design->assign('comment_nedost', $comment->nedost);
            $this->design->assign('parent_id', $comment->parent_id);
			  	
			// Проверяем капчу и заполнение формы
			/*if ($_SESSION['captcha_code'] != $captcha_code || empty($captcha_code))
			{
				$this->design->assign('error', 'captcha');
			}
			else*/
            
            if (empty($comment->name))
			{
				$this->design->assign('error', 'empty_name');
			}
			elseif (empty($comment->text))
			{
				$this->design->assign('error', 'empty_comment');
			}
            /*RECAPTCHA*/
			elseif(empty($recaptha_response) || !$recaptcha_status->success)
			{
				$this->design->assign('error', 'captcha');
			}
            /*RECAPTCHA*/
			else
			{
			
				// Создаем комментарий
				$comment->object_id = $product->id;
				$comment->type      = 'product';
				$comment->ip        = $_SERVER['REMOTE_ADDR'];
				
				// Если были одобренные комментарии от текущего ip, одобряем сразу
				//$this->db->query("SELECT 1 FROM __comments WHERE approved=1 AND ip=? LIMIT 1", $comment->ip);
				//if($this->db->num_rows()>0)
				//	$comment->approved = 1;
				 
				// Добавляем комментарий в базу
				$comment_id = $this->comments->add_comment($comment);
				
				// Добавляем рейтинг
				$rate = ($product->rating * $product->votes + $rating) / ($product->votes + 1);
                $query = $this->db->placehold("UPDATE __products SET rating = ?, votes = votes + 1 WHERE id = ?", $rate, $product_id);
                $this->db->query($query);
				 
				// Отправляем email
				$this->notify->email_comment_admin($comment_id);				

				// Приберем сохраненную капчу, иначе можно отключить загрузку рисунков и постить старую
				//unset($_SESSION['captcha_code']);
				header('location: '.$_SERVER['REQUEST_URI'].'#comment_'.$comment_id);
			}			
		}
		
		
		
		// Похожие товары

	
    $features = array();
    $similar_products = array();
    $feats = array(352, 390, 490, 636, 371, 392, 609);
	foreach($product->features as $f)
	{
	if(in_array($f->feature_id,$feats))
	{
	
	$features[$f->feature_id]->value = $f->value;
	
	}
	}
	$this->design->assign('feats', $features);

	
	if (count($features) > 1) {
	
	$v = reset($product->variants);
	$max_price = $v->price + $v->price * 0.2;
	$min_price = $v->price - $v->price * 0.2;
    $products = $this->products->get_products(array('category_id' => $category->id, 'limit' => 100, 'in_stock'=>1, 'visible'=>1, 'features'=>$features));
    

	foreach($products as $pro)
    {
        $brand = $this->brands->get_brand(intval($pro->brand_id));
		$pro->name = $pro->type_prefix.' '.$brand->name.' '.$pro->name;
		foreach($this->variants->get_variants(array('product_id'=>$pro->id, 'in_stock'=>true)) as $v)
		if ($v->price >= $min_price and $v->price <= $max_price)
		{
            $similar_products[$v->product_id] = $pro;
            $similar_products[$v->product_id]->variants[] = $v;
		}
        
    }


   
 
    $similar_products_images = $this->products->get_images(array('product_id'=>array_keys($similar_products)));
    foreach($similar_products_images as $similar_product_image)
        if(isset($similar_products[$similar_product_image->product_id]))
            $similar_products[$similar_product_image->product_id]->images[] = $similar_product_image;
    
    
    foreach($similar_products as $s)
    {
        
		$s->image = &$s->images[0];
        $s->variant = &$s->variants[0];
    }
	unset($similar_products[$product->id]);
	if (count($similar_products) > 10) array_splice($similar_products,10);
	
    $this->design->assign('similar_products', $similar_products);
		
		
	}	
		
		
		// Связанные товары
		$related_ids = array();
		$related_products = array();
		foreach($this->products->get_related_products($product->id) as $p)
		{
			$related_ids[] = $p->related_id;
			$related_products[$p->related_id] = null;
		}
		if(!empty($related_ids))
		{
			foreach($this->products->get_products(array('id'=>$related_ids, 'in_stock'=>1, 'visible'=>1)) as $p){
				$related_products[$p->id] = $p;
			}
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($related_products)));
			foreach($related_products_images as $related_product_image)
				if(isset($related_products[$related_product_image->product_id]))
					$related_products[$related_product_image->product_id]->images[] = $related_product_image;
			$related_products_variants = $this->variants->get_variants(array('product_id'=>array_keys($related_products), 'instock'=>true));
			foreach($related_products_variants as $related_product_variant)
			{
				if(isset($related_products[$related_product_variant->product_id]))
				{
					$related_products[$related_product_variant->product_id]->variants[] = $related_product_variant;
				}
			}
			foreach($related_products as $id=>$r)
			{
				$brand = $this->brands->get_brand(intval($r->brand_id));
				$r->name = $r->type_prefix.' '.$brand->name.' '.$r->name;
				if(is_object($r))
				{
					$r->image = &$r->images[0];
					$r->variant = &$r->variants[0];
				}
				else
				{
					unset($related_products[$id]);
				}
			}
			$this->design->assign('related_products', $related_products);
		}
		
		/* product_videos */
		$product->videos = $this->products->get_videos(array('product_id'=>$product->id));
		/* product_videos **/

		// Отзывы о товаре
    if($_SESSION['admin'] == 'admin')
		$comments = $this->comments->get_comments_tree(array('type'=>'product', 'object_id'=>$product->id, 'ip'=>$_SERVER['REMOTE_ADDR']));
	  else
    $comments = $this->comments->get_comments_tree(array('type'=>'product', 'object_id'=>$product->id, 'approved'=>1, 'ip'=>$_SERVER['REMOTE_ADDR']));
	
	$next_product = $this->products->get_next_product($product->id);
	$brand1 = $this->brands->get_brand(intval($next_product->brand_id));
	$next_product->name = $next_product->type_prefix.' '.$brand1->name.' '.$next_product->name;
	
	$prev_product = $this->products->get_prev_product($product->id);
	$brand1 = $this->brands->get_brand(intval($prev_product->brand_id));
	$prev_product->name = $prev_product->type_prefix.' '.$brand1->name.' '.$prev_product->name;
	
	// Соседние товары
	$this->design->assign('next_product', $next_product);
	$this->design->assign('prev_product', $prev_product);

	// И передаем его в шаблон
//        echo '<pre>';
//        print_r($product);
    $brand = $this->brands->get_brand(intval($product->brand_id));
    //$product->name = $product->type_prefix.' '.$brand->name.' '.$product->name;
    //var_dump($product->name);
    
	
	$this->design->assign('comments', $comments);
    
    /*comments_rate_count*/
    $comments_rate_count = array();
    foreach($comments as $com){
        $comments_rate_count[$com->rating] = $comments_rate_count[$com->rating]+1;
    }
//var_dump($comments_rate_count);
	$this->design->assign('comments_rate_count', $comments_rate_count);   
    /*/comments_rate_count*/
    
	// Категория и бренд товара
	$product->categories = $this->categories->get_categories(array('product_id'=>$product->id));
	$this->design->assign('brand', $this->brands->get_brand(intval($product->brand_id)));		
	$this->design->assign('category', reset($product->categories));


	// Количество просмотров
	if(empty($_SESSION['view_products'])){
		$_SESSION['view_products'][] = $product->id;
		$this->db->query("UPDATE __variants SET viewed=viewed+1 WHERE product_id=?", $product->id);
	}else{
		if(($exists = array_search($product->id, $_SESSION['view_products'])) == false){
			$_SESSION['view_products'][] = $product->id;
			$this->db->query("UPDATE __variants SET viewed=viewed+1 WHERE product_id=?", $product->id);
		}
	}		


	// Добавление в историю просмотров товаров
	$max_visited_products = 100; // Максимальное число хранимых товаров в истории
	$expire = time()+60*60*24*30; // Время жизни - 30 дней
	if(!empty($_COOKIE['browsed_products']))
	{
		$browsed_products = explode(',', $_COOKIE['browsed_products']);
		// Удалим текущий товар, если он был
		if(($exists = array_search($product->id, $browsed_products)) !== false)
			unset($browsed_products[$exists]);
	}
	// Добавим текущий товар
	$browsed_products[] = $product->id;
	$cookie_val = implode(',', array_slice($browsed_products, -$max_visited_products, $max_visited_products));
	setcookie("browsed_products", $cookie_val, $expire, "/");
	
    /*user_groups*/
    $this->design->assign('user_group', $this->users->get_group($this->user->group_id));
    
    //определяем наименьшую цену для замены тега {price} в описании и мета
    $v_prices = array();
    foreach($product->variants as $v){
        if($v->auto_coupon_price)
            $v_prices[] = $v->auto_coupon_price;
        else
            $v_prices[] = $v->price;
    } 
    sort($v_prices);
    $variant_low_price = reset($v_prices);
    
    $replaces = ARRAY(
        '{rus_brand}'=>$brand->rus_name,
        '{price}' => intval($variant_low_price)
    );	
    
    $product->meta_title = str_replace(array_keys($replaces),array_values($replaces), $product->meta_title);
    $product->meta_keywords = str_replace(array_keys($replaces),array_values($replaces), $product->meta_keywords);
    $product->meta_description = str_replace(array_keys($replaces),array_values($replaces), $product->meta_description);
    $product->body = str_replace(array_keys($replaces),array_values($replaces), $product->body);
    
    //$product->meta_title = str_replace('{price}', $variant_low_price, $product->meta_title);
//    $product->meta_keywords = str_replace('{price}', , $product->meta_keywords);
//    $product->meta_description = str_replace('{price}', $variant_low_price, $product->meta_description);
//    $product->body = str_replace('{price}', $variant_low_price, $product->body);
    
	$this->design->assign('meta_title', $product->meta_title);
	$this->design->assign('meta_keywords', $product->meta_keywords);
	$this->design->assign('meta_description', $product->meta_description);
    $this->design->assign('product', $product);
	
	return $this->design->fetch('product.tpl');
  }
	


}
