<?PHP

require_once('View.php');

class RegisterView extends View
{
	function fetch()
	{
		$default_status = 1; // Активен ли пользователь сразу после регистрации (0 или 1)
		
		if($this->request->method('post') && $this->request->post('register'))
		{
			$name			= $this->request->post('name');
			$email			= $this->request->post('email');
			$password		= $this->request->post('password');
			$surname		= $this->request->post('surname');
			$phone			= $this->request->post('phone');
			/* $captcha_code   = $this->request->post('captcha_code');  */
			
            /*RECAPTCHA*/
    		$recaptha_response = $this->request->post('g-recaptcha-response');
            $remoteip = $_SERVER['REMOTE_ADDR'];
            $recaptcha_status = json_decode($this->recaptcha->send($recaptha_response, $remoteip));
            /*/RECAPTCHA*/
			
			$this->design->assign('name', $name);
            $this->design->assign('surname', $surname);
			$this->design->assign('email', $email);
			
			$this->db->query('SELECT count(*) as count FROM __users WHERE email=?', $email);
			$user_exists = $this->db->result('count');
            
            $this->db->query('SELECT id FROM s_users WHERE email=? AND auto=1 LIMIT 1', $email);
            $user_exists_auto = $this->db->result('id');
            
			if($user_exists && !$user_exists_auto)
				$this->design->assign('error', 'user_exists');
			elseif(empty($name))
				$this->design->assign('error', 'empty_name');
			elseif(empty($email))
				$this->design->assign('error', 'empty_email');
			elseif(empty($password))
				$this->design->assign('error', 'empty_password');		
            /*RECAPTCHA*/
			elseif(empty($recaptha_response) || !$recaptcha_status->success)
			{
				$this->design->assign('error', 'captcha');
			}
            /*RECAPTCHA*/
            elseif($user_exists_auto){
                $user_id = $this->users->update_user($user_exists_auto, array('name'=>$name, 'email'=>$email, 'surname'=>$surname, 'phone'=>$phone, 'password'=>$password, 'enabled'=>$default_status, 'last_ip'=>$_SERVER['REMOTE_ADDR'], 'auto'=>0));
                $_SESSION['user_id'] = $user_id;
				if(!empty($_SESSION['last_visited_page']))
					header('Location: '.$_SESSION['last_visited_page']);				
				else
					header('Location: '.$this->config->root_url);
            }
			elseif($user_id = $this->users->add_user(array('name'=>$name, 'email'=>$email, 'surname'=>$surname, 'phone'=>$phone, 'password'=>$password, 'enabled'=>$default_status, 'last_ip'=>$_SERVER['REMOTE_ADDR'])))
			{
				$_SESSION['user_id'] = $user_id;
				if(!empty($_SESSION['last_visited_page']))
					header('Location: '.$_SESSION['last_visited_page']);				
				else
					header('Location: '.$this->config->root_url);
			}
			else
				$this->design->assign('error', 'unknown error');
	
		}
		/*/social-registration*/
        elseif($this->request->post('form_type')==="ulogin")
        {
            $name = $this->request->post('name');
            $email = $this->request->post('email');
            $password = $this->request->post('password');
            $password = md5($password.'9d6e1c88e1c559e682b86a2713b15484');
            
            $this->db->query('SELECT id FROM __users WHERE email=?', $email);
            $user_id = $this->db->result('id');
            
            if($user_id)
            {
                $this->users->update_user($user_id, array('name'=>$name, 'password'=>$password, 'last_ip'=>$_SERVER['REMOTE_ADDR'], 'enabled'=>'1'));
            }
            else
            {
                $user_id = $this->users->add_user(array('name'=>$name, 'email'=>$email, 'password'=>$password, 'enabled'=>'1', 'last_ip'=>$_SERVER['REMOTE_ADDR']));
            }
            $this->design->assign('email', $email);
            $_SESSION['user_id'] = $user_id;

            // Перенаправляем пользователя на прошлую страницу, если она известна
            if(!empty($_SESSION['last_visited_page']))
                    header('Location: '.$_SESSION['last_visited_page']);				
            else
                    header('Location: '.$this->config->root_url);
        }
        /*/social-registration*/
		return $this->design->fetch('register.tpl');
	}	
}
