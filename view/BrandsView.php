<?PHP
require_once('View.php');
class BrandsView extends View
{
    function fetch()
    {   
        // Выбираем товар из базы
        $this->design->assign('meta_title', 'Все бренды');
        $this->design->assign('meta_keywords', 'Все бренды');
        $this->design->assign('meta_description', 'Все бренды');
 
        $brands = $this->brands->get_brands();
        foreach($brands as $k=>$b)

           $brands[$k]->letter = mb_strtoupper(mb_substr($b->name, 0, 1,'UTF-8'),'UTF-8');    
        
        $this->design->assign('brands', $brands);
 //print_r($brands);
        return $this->design->fetch('brands.tpl');
    }
}