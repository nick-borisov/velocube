<?php
	chdir('../..');
    
    //ini_set('display_errors',1);
	require_once('api/Simpla.php');
	$simpla = new Simpla();
	$limit = 100;
	
	if(!$simpla->managers->access('orders'))
		return false;
	
	
	
    $keyword = $simpla->request->get('query', 'string');
	
	$keywords = explode(' ', $keyword);
	$keyword_sql = '';
	foreach($keywords as $keyword1)
	{
		$kw = $simpla->db->escape(trim($keyword1));
		$keyword_sql .= $simpla->db->placehold(" AND (p.fullname LIKE '%$kw%' OR p.meta_keywords LIKE '%$kw%' OR p.id in (SELECT product_id FROM __variants WHERE sku LIKE '%$kw%'))");
	}

	$order = '( (SELECT count(pv.id) FROM __variants pv WHERE (pv.stock IS NULL OR pv.stock>0) AND p.id = pv.product_id)>0 ) DESC, p.fullname';

    
	$query = $simpla->db->placehold("SELECT p.id, p.fullname, b.name as brand, p.name, p.type_prefix, i.filename as image FROM __products p
	                    LEFT JOIN __images i ON i.product_id=p.id AND i.position=(SELECT MIN(position) FROM __images WHERE product_id=p.id LIMIT 1)
                        LEFT JOIN __brands b ON p.brand_id=b.id
	                    LEFT JOIN __variants pv ON pv.product_id=p.id /*AND (pv.stock IS NULL OR pv.stock>0)*/ AND pv.price>0
	                    WHERE 1 $keyword_sql /*AND (pv.stock>0 OR pv.stock is NULL  OR p.sklad_id > 1)*/ AND visible=1 AND pv.price > 0
	                    ORDER BY $order LIMIT ?", $limit);
	//var_dump($query);
    $simpla->db->query($query);
    //print($query);                    
	$products = array();
	foreach($simpla->db->results() as $product)
		$products[$product->id] = $product;
	
	$simpla->db->query('SELECT v.id, v.name, v.price, IFNULL(v.stock, ?) as stock, (v.stock IS NULL) as infinity, v.product_id FROM __variants v WHERE v.product_id in(?@)/* AND (v.stock IS NULL OR v.stock>0)*/ AND v.price>0 ORDER BY v.position', $simpla->settings->max_order_amount, array_keys($products));
	$variants = $simpla->db->results();
	
	foreach($variants as $variant)
		if(isset($products[$variant->product_id]))
			$products[$variant->product_id]->variants[] = $variant;
	
	
	foreach($products as $product)
	{
		if(!empty($product->variants))
		{
			if(!empty($product->image))
				$product->image = $simpla->design->resize_modifier($product->image, 35, 35);
		    if(!empty($product->fullname))  
			    $products_names[] = $product->fullname;
            else
                $products_names[] = $product->type_prefix.' '.$product->brand.' '.$product->name;
                		
			$products_data[] = $product;		
		}
	}
//var_dump($products);
	$res->query = $keyword;
	$res->suggestions = $products_names;
	$res->data = $products_data;
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($res);
