<?php

chdir('../..');
require_once('api/Simpla.php');

class ExportAjax extends Simpla
{
private $columns_names = array(
			'created' 			=>	'����',
			'name'				=>  '�����',
			'url'				=>  '�����',
			'brand'				=>  '�����',
			
);

private $column_delimiter = ';';
private $products_count = 5;
private $export_files_dir = 'simpla/files/export_csvproducts/';
private $filename = 'products.csv';
	private $subcategory_delimiter = '/';

public function fetch()
{
// ������ ������ ������ 1251
		setlocale(LC_ALL, 'ru_RU.1251');
		$this->db->query('SET NAMES cp1251');

// ��������, ������� ������������
		$page = $this->request->get('page');
		if(empty($page) || $page==1)
		{
			$page = 1;
			// ���� ������ ������� - ������ ������ ���� ��������
			if(is_writable($this->export_files_dir.$this->filename))
				unlink($this->export_files_dir.$this->filename);
		}

// ��������� ���� �������� �� ����������
		$f = fopen($this->export_files_dir.$this->filename, 'ab');
				
		// ���� ������ ������� - ������� � ������ ������ �������� �������
		if($page == 1)
		{
			fputcsv($f, $this->columns_names, $this->column_delimiter);
		}

$filter = array();
// ������
$man = $this->request->get('manager');
if( $man != 'all') $filter['manager'] = $man;

$date_from = $this->request->get('date_from');
if($date_from)
$filter['date_from'] = date("Y-m-d 00:00:01",strtotime($date_from));

$date_to = $this->request->get('date_to');
if($date_to)
$filter['date_to'] = date("Y-m-d 23:59:00",strtotime($date_to));

$filter['page'] = $page;
		
$filter['limit'] = $this->products_count;


// �������� ������
$products = array();
$products = $this->products->get_products($filter);

 		// ��������� �������
 		foreach($products as $p_id=>&$product)
 		{
		   $categories = array();
	 		$cats = $this->categories->get_product_categories($p_id);
	 		foreach($cats as $category)
	 		{
	 			$path = array();
	 			$cat = $this->categories->get_category((int)$category->category_id);
	 			if(!empty($cat))
 				{
	 				// ��������� ������������ ���������
	 				foreach($cat->path as $p)
	 					$path[] = str_replace($this->subcategory_delimiter, '\\'.$this->subcategory_delimiter, $p->name);
	 				// ��������� ��������� � ������ 
	 				$categories[] = implode('/', $path);
 				}
	 		}
	 		$product->category = implode(', ', $categories);
 		}

foreach($products as $p)
{
 			$str = array();
 			foreach($this->columns_names as $n=>$c)
 				$str[] = $p->$n;
 				
 			fputcsv($f, $str, $this->column_delimiter);
}

$total_products = $this->products->count_products($filter);

if($this->products_count*$page < $total_products)
return array('end'=>false, 'page'=>$page, 'totalpages'=>$total_products/$this->products_count);
else
return array('end'=>true, 'page'=>$page, 'totalpages'=>$total_products/$this->products_count);

fclose($f);

}

}

$export_ajax = new ExportAjax();
$json = json_encode($export_ajax->fetch());
header("Content-type: application/json; charset=utf-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");		
print $json;