<?php
//ini_set('display_errors',1);
	chdir('../..');
	require_once('api/Simpla.php');
	$simpla = new Simpla();
	$limit = 20;
	
    $keyword = $simpla->request->get('query', 'string');
	
	$keywords = explode(' ', $keyword);
	$keyword_sql = '';
	foreach($keywords as $keyword1)
	{
		$kw = $simpla->db->escape(trim($keyword1));
		$keyword_sql .= $simpla->db->placehold("AND (p.fullname LIKE '%$kw%' OR p.meta_keywords LIKE '%$kw%' OR p.id in (SELECT product_id FROM __variants WHERE sku LIKE '%$kw%'))");
	}
	
	$query = $simpla->db->placehold('SELECT p.id, p.fullname, i.filename as image FROM __products p
	                    LEFT JOIN __images i ON i.product_id=p.id AND i.position=(SELECT MIN(position) FROM __images WHERE product_id=p.id LIMIT 1)
                        LEFT JOIN __variants v ON v.product_id=p.id
	                    WHERE 1 '.$keyword_sql.' AND (v.stock>0 OR v.stock is NULL  OR p.sklad_id > 1) AND visible=1 AND v.price > 0  GROUP BY p.id ORDER BY IF((SELECT COUNT(*) FROM s_variants WHERE (stock>0 OR stock IS NULL) AND product_id=p.id LIMIT 1), 1, 0) DESC, p.position DESC LIMIT ?', $limit);
    
/*	$fullkeyword = $simpla->request->get('query', 'string');
	$keywords = explode(' ', $fullkeyword);
    $keyword_filter = '';
    $word = '';
    $keyword_filter = $simpla->db->placehold('AND p.name LIKE "%'.mysql_real_escape_string($fullkeyword).'%"');
    if(count($keywords)>2){
    	for($i=2; $i<count($keywords); $i++){
    	   $word .= $keywords[$i].' ';
    	} 
    }
    if(count($keywords)>1){
    	for($i=1; $i<count($keywords); $i++){
    	   $word2 .= $keywords[$i].' ';
    	} 
    }
    $keyword_filter .= $simpla->db->placehold(' OR (b.name LIKE "%'.mysql_real_escape_string($keywords[0]).'%" AND p.name LIKE "%'.mysql_real_escape_string($word2).'%")'); 
    $keyword_filter .= $simpla->db->placehold(' OR (p.type_prefix LIKE "%'.mysql_real_escape_string($keywords[0]).'%" AND b.name LIKE "%'.mysql_real_escape_string($keywords[1]).'%" AND p.name LIKE "%'.mysql_real_escape_string($word).'%")');
	
	$products = array();
	
	$query = $simpla->db->placehold("SELECT p.id, p.name, p.type_prefix, b.name as brand, i.filename as image FROM __products p
	                    LEFT JOIN __images i ON i.product_id=p.id AND i.position=(SELECT MIN(position) FROM __images WHERE product_id=p.id LIMIT 1)
                        LEFT JOIN __brands b ON p.brand_id=b.id
						LEFT JOIN __variants v ON v.product_id=p.id
	                    WHERE 1 $keyword_filter AND (v.stock>0 OR v.stock is NULL  OR p.sklad_id > 1) AND visible=1 AND v.price > 0 
                        GROUP BY p.id ORDER BY IF((SELECT COUNT(*) FROM s_variants WHERE (stock>0 OR stock IS NULL) AND product_id=p.id LIMIT 1), 1, 0) DESC, p.position DESC LIMIT ?", $limit);
    */
    //print_r($query);
    $simpla->db->query($query);
	$products = $simpla->db->results();

foreach($products as $product)
	{
	if(!empty($product->image))
		{
				$product->image = $simpla->design->resize_modifier($product->image, 35, 35);
				$products_names[] = $product->fullname;
				$product->url = "/products/".$product->url;
				$products_data[] = $product;
		}
	else
		{
			$product->image = "noproductimage.jpg";
			$products_names[] = $product->fullname;	
			$product->url = "/products/".$product->url;
			$products_data[] = $product;
	}

	}

	$res->query = $keyword;
	$res->suggestions = $products_names;
	$res->data = $products_data;
	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($res);
