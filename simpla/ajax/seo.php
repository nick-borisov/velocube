<?php
//ini_set('display_errors',1);
	chdir('../..');
	require_once('api/Simpla.php'); 
    
	$simpla = new Simpla();
	$limit = 30;
	
	$seo = (object)read_cfg();
	
	$seo->regionCity = str_replace(' ','',explode(",",$seo->regionCity));
	$seo->regions = str_replace(' ','',explode(",",$seo->regions));
	foreach($seo->descriptions as $key => $description)
		if(trim(strip_tags($description)) == '')
			unset($seo->descriptions[$key]);
	
	shuffle($seo->regionCity);
	shuffle($seo->regions);
	shuffle($seo->descriptions);
    shuffle($seo->brand_descriptions);
    shuffle($seo->product_descriptions);
	
	$seo->regionCity = implode(", ",array_slice($seo->regionCity, 0, rand(5,7)));
	$seo->regions = implode(", ",array_slice($seo->regions, 0, rand(5,7)));
	$seo->description = $seo->descriptions[0];
    $seo->brand_description = $seo->brand_descriptions[0];
    $seo->product_description = $seo->product_descriptions[0];

	unset($seo->descriptions);
    unset($seo->brand_descriptions);
    unset($seo->product_descriptions);
	$res = new StdClass();
    
    if($simpla->request->get('action', 'string') == 'brand')
	{
        
		$b_id = $simpla->request->get('brand_id', 'integer');
        $brand = $simpla->brands->get_brand(intval($b_id));
		$b_name = $simpla->request->get('brand', 'string');
		
		$replaces = ARRAY(
			'{brand}'=>$b_name,
			'{shopname}'=>$seo->name,
            '{rus_brand}'=>$brand->rus_name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
		);

		
		$res->title = str_replace(array_keys($replaces),array_values($replaces),$seo->title_brand);
		$res->description = str_replace(array_keys($replaces),array_values($replaces),$seo->description_brand);
		$res->keywords = str_replace(array_keys($replaces),array_values($replaces),$seo->keywords_brand);
		//$res->description_footer = str_replace(array_keys($replaces),array_values($replaces), $seo->description);

	}
    elseif($simpla->request->get('action', 'string') == 'delivery_description')
	{
		$b->id = $simpla->request->get('brand_id', 'integer');
        $brand = $simpla->brands->get_brand(intval($b->id));
		$b_name = $simpla->request->get('brand', 'string');
		
		$replaces = ARRAY(
			'{brand}'=>$b_name,
            '{rus_brand}'=>$brand->rus_name,
			'{shopname}'=>$seo->name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
            '{name}'=>$product->name,
            '{type_prefix}'=>$product->type_prefix,
            '{fullname}'=>$product->fullname,
            '{l-name}'=>mb_strtolower($product->name, 'UTF-8'),
            '{l-type_prefix}'=>mb_strtolower($product->type_prefix, 'UTF-8'),
            '{l-category}'=>mb_strtolower($product->category, 'UTF-8'),
		);

        
        $res->description = str_replace(array_keys($replaces),array_values($replaces),$seo->brand_description);
        
        
	}
    elseif($simpla->request->get('action', 'string') == 'brand_description')
	{
		$b->id = $simpla->request->get('brand_id', 'integer');
        $brand = $simpla->brands->get_brand(intval($b->id));
		$b_name = $simpla->request->get('brand', 'string');
		
		$replaces = ARRAY(
			'{brand}'=>$b_name,
            '{rus_brand}'=>$brand->rus_name,
			'{shopname}'=>$seo->name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
		);

        
        $res->description = str_replace(array_keys($replaces),array_values($replaces),$seo->brand_description);
        
        
	}
	elseif($simpla->request->get('action', 'string') == 'category')
	{
        
		$cat->id = $simpla->request->get('cat_id', 'integer');
        $category = $simpla->categories->get_category(intval($cat->id));
		$cat->skl = $simpla->request->get('cat_skl', 'string');
		$cat->skl_mn = $simpla->request->get('cat_skl_mn', 'string');
		$cat->skl_ov = $simpla->request->get('cat_skl_ov', 'string');
		
		$replaces = ARRAY(
			'{skl}'=>$cat->skl,
			'{skl_mn}'=>$cat->skl_mn,
			'{skl_ov}'=>$cat->skl_ov,
			'{l-skl}'=>mb_lcfirst_($cat->skl),
			'{l-skl_mn}'=>mb_lcfirst_($cat->skl_mn),
			'{l-skl_ov}'=>mb_lcfirst_($cat->skl_ov),
			'{shopname}'=>$seo->name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
            '{category}'=>$category->name,
            '{l-category}'=>mb_strtolower($category->name, 'UTF-8')
		);

		
		$res->title = str_replace(array_keys($replaces),array_values($replaces),$seo->title_category);
		$res->description = str_replace(array_keys($replaces),array_values($replaces),$seo->description_category);
		$res->keywords = str_replace(array_keys($replaces),array_values($replaces),$seo->keywords_category);
		//$res->description_footer = str_replace(array_keys($replaces),array_values($replaces), $seo->description);

	}
    elseif($simpla->request->get('action', 'string') == 'category_description')
	{
		$cat->id = $simpla->request->get('cat_id', 'integer');
		$cat->skl = $simpla->request->get('cat_skl', 'string');
		$cat->skl_mn = $simpla->request->get('cat_skl_mn', 'string');
		$cat->skl_ov = $simpla->request->get('cat_skl_ov', 'string');
		$category = $simpla->categories->get_category(intval($cat->id));
		$replaces = ARRAY(
        	'{skl}'=>$cat->skl,
			'{skl_mn}'=>$cat->skl_mn,
			'{skl_ov}'=>$cat->skl_ov,
			'{l-skl}'=>mb_lcfirst_($cat->skl),
			'{l-skl_mn}'=>mb_lcfirst_($cat->skl_mn),
			'{l-skl_ov}'=>mb_lcfirst_($cat->skl_ov),
			'{shopname}'=>$seo->name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
            '{category}'=>$category->name,
            '{l-category}'=>mb_strtolower($category->name, 'UTF-8')
		);

        $cat_templates_ids = unserialize($seo->categories_templates);
        $cat_templates_des = unserialize($seo->des_categories_templates);
        
        $ctemplate_id = -1;
        foreach($cat_templates_ids as $ckey=>$ct){
            $c_category = $simpla->categories->get_category(intval($ct));
            $c_children = $c_category->children;

            if(in_array($category->id, $c_children)){
                $ctemplate_id = $ckey;
            }
        }
        
        if($ctemplate_id>-1){
            $res->description = str_replace(array_keys($replaces),array_values($replaces), $cat_templates_des[$ctemplate_id]);
        }else{
            $res->description = str_replace(array_keys($replaces),array_values($replaces), $seo->description);
        } 
        
        //var_dump($res);
	}
	elseif($simpla->request->get('action', 'string') == 'product')
	{
        $product = new StdClass();
		$product->name = $simpla->request->get('name');
        $product->type_prefix = $simpla->request->get('type_prefix');
        $product->fullname = $simpla->request->get('fullname');
		$product->title = $simpla->request->get('title');
		$product->brand = $simpla->request->get('brand');
        $product->rus_brand = $simpla->request->get('rus_brand');
		$product->category = $simpla->request->get('category');
		$product->variants = str_replace('_',', ',$simpla->request->get('variants'));
		$product->variants_array = explode('_',$simpla->request->get('variants'));
        $low_variant_price = $simpla->request->get('low_variant_price', 'integer');
		//var_dump($low_variant_price);
		$replaces = ARRAY(
			'{name}'=>$product->name,
            '{type_prefix}'=>$product->type_prefix,

            '{fullname}'=>$product->fullname,
			//'{name}'=>$product->title,
			'{brand}'=>$product->brand,
            '{rus_brand}'=>$product->rus_brand,
			'{category}'=>$product->category,
			'{variant}'=>$product->variants,
			'{shopname}'=>$seo->name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
            '{l-name}'=>mb_strtolower($product->name, 'UTF-8'),
            '{l-type_prefix}'=>mb_strtolower($product->type_prefix, 'UTF-8'),
            '{l-category}'=>mb_strtolower($product->category, 'UTF-8')
            //,'{price}'=>$low_variant_price
		);
		
		$res->title = str_replace(array_keys($replaces),array_values($replaces),$seo->title_product);
		$res->description = str_replace(array_keys($replaces),array_values($replaces),$seo->description_product);
		$res->keywords = str_replace(array_keys($replaces),array_values($replaces),$seo->keywords_product);
	}elseif($simpla->request->get('action', 'string') == 'product_description')
	{
        $product = new StdClass();
        $product->id = $simpla->request->get('pid', 'integer');
		$product->name = $simpla->request->get('name');
        $product->type_prefix = $simpla->request->get('type_prefix', 'string');
        $product->fullname = $simpla->request->get('fullname');
		$product->title = $simpla->request->get('title', 'string');
		$product->brand = $simpla->request->get('brand', 'string');
		$product->category = $simpla->request->get('category', 'string');
		$product->variants = str_replace('_',', ',$simpla->request->get('variants', 'string'));
		$product->variants_array = explode('_',$simpla->request->get('variants', 'string'));
        $product->rus_brand = $simpla->request->get('rus_brand', 'string');
        $low_variant_price = $simpla->request->get('low_variant_price', 'string');
		//var_dump($low_variant_price);
		$replaces = ARRAY(
			'{name}'=>$product->name,
            '{type_prefix}'=>$product->type_prefix,

            '{fullname}'=>$product->fullname,
			//'{name}'=>$product->title,
			'{brand}'=>$product->brand,
			'{category}'=>$product->category,
			'{variant}'=>$product->variants,
			'{shopname}'=>$seo->name,
			'{shopcity}'=>$seo->officeCity,
			'{region_cities}'=>$seo->regionCity,
			'{regions}'=>$seo->regions,
			'{phones}'=>$seo->contactTel,
            '{l-name}'=>mb_strtolower($product->name, 'UTF-8'),
            '{l-type_prefix}'=>mb_strtolower($product->type_prefix, 'UTF-8'),
            '{l-category}'=>mb_strtolower($product->category, 'UTF-8'),
            '{rus_brand}'=>$product->rus_brand
            //,'{price}'=>$low_variant_price
		);
		$replace_f = array();
        foreach($simpla->categories->get_product_categories(intval($product->id)) as $pc){
            $categories[] = $pc->category_id;
        }
        
        $features = $simpla->features->get_features(array('category_id'=>$categories));
        foreach($features as $f){
            $options = $simpla->features->get_options(array('product_id'=>$product->id, 'feature_id'=>$f->id));
            if(count($options)){
                $replace_f['{f_'.$f->id.'}'] = $options[0]->value;
            }
        }
        $replaces = array_merge($replaces, $replace_f);
       
        $template_id = -1;
        foreach(unserialize($seo->products_templates) as $key=>$pt){
            $t_category = $simpla->categories->get_category(intval($pt));
            $t_children = $t_category->children;
            
            foreach($t_children as $tc){
                if(in_array($tc, $categories)){
                    $template_id = $key;
                    $template_to_product_ = unserialize($seo->des_products_templates);
                    $template_to_product = $template_to_product_[$key];                    
                }
            }
        }

        if($template_id>-1){
            $res->description = str_replace(array_keys($replaces),array_values($replaces), $template_to_product);
        }else{
            $res->description = str_replace(array_keys($replaces),array_values($replaces),$seo->product_description);
        }
	}

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");
	print json_encode($res);
	
	
	
	function read_cfg()
	{
		$vars = is_file('config/seo.cfg')?unserialize(file_get_contents('config/seo.cfg')):array();
		return $vars;
	}
	function mb_ucfirst_($str, $enc = 'utf8') { 
			return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
	}
    function mb_lcfirst_($str, $enc = 'utf8') { 
			return mb_strtolower(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
	}
	
	
