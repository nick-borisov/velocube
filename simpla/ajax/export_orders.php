<?php

chdir('../..');
require_once('api/Simpla.php');

class ExportAjax extends Simpla
{
private $columns_names = array(
'id'=> '����� N',
'name'=> '��� ������������',
'phone'=> '����� ��������',
'address'=> '����� �������',
'email'=> '����� �������',
'comment'=> '����������� �������',
'discount'=> '������ �� �����(���� ����)',
'total_price'=> '����� ����� ������',
'date'=> '���� ������',
'delivery_date'=> '���� ��������',
'products'=> '������'
);

private $column_delimiter = ';';
private $orders_count = 5;
private $export_files_dir = 'simpla/files/export_orders/';
private $filename = 'orders.csv';

public function fetch()
{
// ������ ������ ������ 1251
setlocale(LC_ALL, 'ru_RU.1251');
$this->db->query('SET NAMES cp1251');

// ��������, ������� ������������
$page = $this->request->get('page');
if(empty($page) || $page==1)
{
$page = 1;
// ���� ������ ������� - ������ ������ ���� ��������
if(is_writable($this->export_files_dir.$this->filename))
unlink($this->export_files_dir.$this->filename);
}

// ��������� ���� �������� �� ����������
$f = fopen($this->export_files_dir.$this->filename, 'ab');

// ���� ������ ������� - ������� � ������ ������ �������� �������
if($page == 1)
{
fputcsv($f, $this->columns_names, $this->column_delimiter);
}

$filter = array();
$filter['page'] = $page;
//if($this->request->get('status') && $this->request->get('status') != 'all')
			$filter['status'] = intval($this->request->get('status'));
$filter['limit'] = $this->orders_count;


// �������� ������
$orders = array();
foreach($this->orders->get_orders($filter) as $u)
{

$u->purchases = $this->orders->get_purchases(array('order_id'=>$u->id));
	
$str = array();
foreach($this->columns_names as $n=>$c)
{
	if($n == 'delivery_date' && ($u->$n == '0000-00-00' || $u->$n == '1970-01-01'))
	$u->$n = '';
	if($n == 'products')
	{
	foreach($u->purchases as $p)
	$u->$n .= $p->product_name.' '.$p->variant_name.' - '.$p->amount.' ��.';
	}		
$str[] = $u->$n;
}

fputcsv($f, $str, $this->column_delimiter);
}

$total_orders = $this->orders->count_orders();

if($this->orders_count*$page < $total_orders)
return array('end'=>false, 'page'=>$page, 'totalpages'=>$total_orders/$this->orders_count);
else
return array('end'=>true, 'page'=>$page, 'totalpages'=>$total_orders/$this->orders_count);

fclose($f);

}

}

$export_ajax = new ExportAjax();
$json = json_encode($export_ajax->fetch());
header("Content-type: application/json; charset=utf-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print $json;