<?PHP 

require_once('api/Simpla.php');

########################################
class CallbacksAdmin extends Simpla
{


  function fetch()
  {
  
  
  	// Обработка действий 	
  	if($this->request->method('post'))
  	{
		// Действия с выбранными
		$ids = $this->request->post('check');

		if(!empty($ids))
		switch($this->request->post('action'))
		{
			case 2:
            {
				$this->callbacks->update_callback($ids, array('status'=>2));    
		        break;
            }
            case 3:
            {
				$this->callbacks->update_callback($ids, array('status'=>3));    
		        break;
            }
            case 4:
		    {
				foreach($ids as $id)
					$this->callbacks->delete_callback($id);    
		        break;
		    }
		}		
		
 	}

  	// Отображение
	$filter = array();
	$filter['page'] = max(1, $this->request->get('page', 'integer')); 		
	$filter['limit'] = 40;


  	//$callbacks_count = $this->callbacks->count_callbacks($filter);
	// Показать все страницы сразу
	if($this->request->get('page') == 'all')
		$filter['limit'] = $callbacks_count;
        
	// Текущий фильтр
	if($f = $this->request->get('status', 'string'))
	{
        $filter['status'] = $f;
		$this->design->assign('status', $f);
	}

	// Поиск
	$keyword = $this->request->get('keyword');
	if(!empty($keyword))
	{
  		$filter['keyword'] = $keyword;
		$this->design->assign('keyword', $keyword);
	}  	
  	
  	$callbacks = $this->callbacks->get_callbacks($filter, true);

 	$this->design->assign('pages_count', ceil($callbacks_count/$filter['limit']));
 	$this->design->assign('current_page', $filter['page']);

 	$this->design->assign('callbacks', $callbacks);
 	$this->design->assign('callbacks_count', $callbacks_count);

	return $this->design->fetch('callbacks.tpl');
  }
}


?>