<?PHP
//ini_set('display_eroors',1);
require_once('api/Simpla.php');

########################################
class CallbacksCheapAdmin extends Simpla
{


  function fetch()
  {
  

  	// Обработка действий 	
  	if($this->request->method('post'))
  	{
		// Действия с выбранными
		$ids = $this->request->post('check');
		if(!empty($ids))
		switch($this->request->post('action'))
		{
		    case 'delete':
		    {
				foreach($ids as $id)
					$this->callbackcheap->delete_callback($id);    
		        break;
		    }
			case 'processed':
            {
				$this->callbackcheap->update_callback($ids,array('processed'=>1));    
		        break;
            }
		}		
		
 	}

  	// Отображение
	$filter = array();
	$filter['page'] = max(1, $this->request->get('page', 'integer')); 		
	$filter['limit'] = 40;

	if(isset($_GET['processed'])){
		$filter['processed'] = $this->request->get('processed');
		$this->design->assign('processed', $filter['processed']);
	}
  	$callbacks_count = $this->callbackcheap->count_callbacks($filter);
	// Показать все страницы сразу
	if($this->request->get('page') == 'all')
		$filter['limit'] = $callbacks_count;	
  	
  	$callbacks = $this->callbackcheap->get_callbacks($filter, true);
    foreach($callbacks as &$callback)
        if($callback->product_id)
            $callback->product = $this->products->get_product(intval($callback->product_id));

 	$this->design->assign('pages_count', ceil($callbacks_count/$filter['limit']));
 	$this->design->assign('current_page', $filter['page']);

 	$this->design->assign('callbacks', $callbacks);
 	$this->design->assign('callbacks_count', $callbacks_count);

	return $this->design->fetch('callbacks_cheap.tpl');
  }
}


?>