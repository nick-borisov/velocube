<?PHP
//ini_set('display_errors',1);
require_once('api/Simpla.php');

class SeoAdmin extends Simpla
{
	private $config_file = 'config/seo.cfg';
	private $vars = ARRAY();
	private $errors = ARRAY();
	
	public function __construct()
	{
        
		if($this->request->post('session_id'))
		{
			$vars = $_POST;
			unset($vars['session_id']);
			$this->vars = $vars;
			
			$this->vars['name'] = $this->skip($vars['name']);
			$this->vars['officeCity'] = $this->skip($vars['officeCity']);
			$this->vars['contactTel'] = $this->skip($vars['contactTel']);
			$this->vars['regionCity'] = $this->skip($vars['regionCity']);
			$this->vars['regions'] = $this->skip($vars['regions']);
			
			$this->vars['title_category'] = $this->skip($vars['title_category']);
			$this->vars['description_category'] = $this->skip($vars['description_category']);
			$this->vars['keywords_category'] = $this->skip($vars['keywords_category']);

			$this->vars['title_product'] = $this->skip($vars['title_product']);
			$this->vars['description_product'] = $this->skip($vars['description_product']);
			$this->vars['keywords_product'] = $this->skip($vars['keywords_product']);
            $this->vars['full_description_product'] = $vars['full_description_product'];
            $this->vars['delivery_description'] = $vars['delivery_description'];
            
            $this->vars['categories_templates'] = serialize($vars['categories_templates']);
            $this->vars['des_categories_templates'] = serialize($vars['des_categories_templates']);
            
            $this->settings->title_category_brand = $this->vars['title_category_brand'];
            $this->settings->keywords_category_brand = $this->vars['keywords_category_brand'];
            $this->settings->meta_description_category_brand = $this->vars['description_category_brand'];

            $this->vars['categories_brand_templates'] = serialize($vars['categories_brand_templates']);
            $this->vars['des_categories_brand_templates'] = serialize($vars['des_categories_brand_templates']);
            //$this->vars['full_description_category_brand'] = $vars['full_description_category_brand'];
            
            $this->vars['products_templates'] = serialize($vars['products_templates']);
            $this->vars['des_products_templates'] = serialize($vars['des_products_templates']);
            
            $this->vars['title_brand'] = $this->skip($vars['title_brand']);
			$this->vars['description_brand'] = $this->skip($vars['description_brand']);
			$this->vars['keywords_brand'] = $this->skip($vars['keywords_brand']);
            
            foreach($this->vars['descriptions'] as $key => $description)
        		if(trim(strip_tags($description)) == '')
        			unset($this->vars['descriptions'][$key]);
            
            foreach($this->vars['product_descriptions'] as $key => $description)
        		if(trim(strip_tags($description)) == '')
        			unset($this->vars['product_descriptions'][$key]);        
            
            foreach($this->vars['descriptions_brand'] as $key => $description_brand)
        		if(trim(strip_tags($description_brand)) == '')
        			unset($this->vars['descriptions_brand'][$key]);   
                    
            foreach($this->vars['brand_descriptions'] as $key => $description)
        		if(trim(strip_tags($description)) == '')
        			unset($this->vars['brand_descriptions'][$key]);             
			
			if(trim($_POST['name']) == '' or strlen($_POST['name']) < 3)
				$this->errors[] = (trim($_POST['name']) == '')?'Заполните поле "Название интернет-магазина"':'Cлишком короткое "Название интернет-магазина"';
			
			if(trim($_POST['officeCity']) == '' or strlen($_POST['officeCity']) < 3)
				$this->errors[] = (trim($_POST['officeCity']) == '')?'Заполните поле "Город в котором находится магазин"':'Cлишком короткое "Название Города"';
			
			if(trim($_POST['contactTel']) == '' or strlen($_POST['contactTel']) < 3)
				$this->errors[] = (trim($_POST['contactTel']) == '')?'Заполните поле "Контактные телефоны"':'Cлишком короткий "Контактный телефон"';
			
			if(trim($_POST['regionCity']) == '' or strlen($_POST['regionCity']) < 3)
				$this->errors[] = (trim($_POST['regionCity']) == '')?'Заполните поле "Города вашего региона"':'Cлишком короткое значение поля "Города вашего региона"';
			
			if(trim($_POST['regions']) == '' or strlen($_POST['regions']) < 3)
				$this->errors[] = (trim($_POST['regions']) == '')?'Заполните поле "Основные регионы..."':'Cлишком короткое значение поля "Основные регионы"';
			
			/*if(
				trim($_POST['descriptions'][0]) == '' or strlen($_POST['descriptions'][0]) < 3 OR
				trim($_POST['descriptions'][1]) == '' or strlen($_POST['descriptions'][1]) < 3 OR
				trim($_POST['descriptions'][2]) == '' or strlen($_POST['descriptions'][2]) < 3
			  ){
				$this->errors[] = 'Заполните поле "RANDOM Шаблон описания в категориях", <br>первые <strong style="color:red">ТРИ</strong> шаблона обязательно должны быть заполнены';
			}elseif(
                trim($_POST['descriptions_brand'][0]) == '' or strlen($_POST['descriptions_brand'][0]) < 3 OR
				trim($_POST['descriptions_brand'][1]) == '' or strlen($_POST['descriptions_brand'][1]) < 3 OR
				trim($_POST['descriptions_brand'][2]) == '' or strlen($_POST['descriptions_brand'][2]) < 3
            ){
				$this->errors[] = 'Заполните поле "RANDOM Шаблон описания в категории+бренд", <br>первые <strong style="color:red">ТРИ</strong> шаблона обязательно должны быть заполнены';
			}elseif(
                trim($_POST['brand_descriptions'][0]) == '' or strlen($_POST['brand_descriptions'][0]) < 3 OR
				trim($_POST['brand_descriptions'][1]) == '' or strlen($_POST['descriptions_brand'][1]) < 3 OR
				trim($_POST['brand_descriptions'][2]) == '' or strlen($_POST['brand_descriptions'][2]) < 3
            ){
				$this->errors[] = 'Заполните поле "RANDOM Шаблон описания в брендах", <br>первые <strong style="color:red">ТРИ</strong> шаблона обязательно должны быть заполнены';
			}elseif(
                trim($_POST['product_descriptions'][0]) == '' or strlen($_POST['product_descriptions'][0]) < 3 OR
				trim($_POST['product_descriptions'][1]) == '' or strlen($_POST['product_descriptions'][1]) < 3 OR
				trim($_POST['product_descriptions'][2]) == '' or strlen($_POST['product_descriptions'][2]) < 3
            ){
				$this->errors[] = 'Заполните поле "RANDOM Шаблон описания в продукте", <br>первые <strong style="color:red">ТРИ</strong> шаблона обязательно должны быть заполнены';
			}
            */
			if(count($this->errors) == 0)
				$this->saveConfig($vars);
            
            $regionCity = str_replace(' ','',explode(",",$this->vars['regionCity']));
	        $regions = str_replace(' ','',explode(",",$this->vars['regions']));

            shuffle($regionCity);
	        shuffle($regions);
           	$regionCity = implode(", ",array_slice($regionCity, 0, rand(5,7)));
	        $regions = implode(", ",array_slice($regions, 0, rand(5,7)));
//var_dump($regions);die;                
            if($this->request->post('go_update_meta')){
                set_time_limit(500);
                
                if($_POST['tab_type'] == 'brand'){
                    foreach($this->brands->get_brands() as $brand){
                        if($brand->meta_generation){
                            $replaces = ARRAY(
                        			'{brand}'=>$brand->name,
                                    '{rus_brand}'=>$brand->rus_name,
                        			'{shopname}'=>$this->vars['name'],
                        			'{shopcity}'=>$this->vars['officeCity'],
                        			'{region_cities}'=>$regionCity,
                        			'{regions}'=>$regions,
                        			'{phones}'=>$this->vars['contactTel']
                        		);
                            $b = new StdClass();
                            $b->meta_title = str_replace(array_keys($replaces),array_values($replaces),$this->vars['title_brand']);
                            $b->meta_description = str_replace(array_keys($replaces),array_values($replaces),$this->vars['description_brand']);
                            $b->meta_keywords = str_replace(array_keys($replaces),array_values($replaces),$this->vars['keywords_brand']); 
                            
                            $this->brands->update_brand($brand->id, $b);
                            echo('<span style="background:#'.rand(10,99).'ca'.rand(10,99).'">Обновили бренд <b>'.$brand->name."</b></span><br/>");
                        }
                    }
                }
                
                if($_POST['tab_type'] == 'category' || $_POST['tab_type'] == 'cat_brand' || $_POST['tab_type'] == 'product'){    
                    $categories = $this->categories->get_categories();
                
                    foreach($categories as $category){
                        if($this->request->post('tab_type') == 'category' && $category->meta_generation){
                            $category->skl = unserialize($category->skl);
                            $replaces = ARRAY(
                    			'{skl}'=>$category->skl['skl'],
                    			'{skl_mn}'=>$category->skl['skl_mn'],
                    			'{skl_ov}'=>$category->skl['skl_ov'],
                                '{l-skl}'=>mb_strtolower($category->skl['skl']),
                                '{l-skl_mn}'=>mb_strtolower($category->skl['skl_mn']),
                                '{l-skl_ov}'=>mb_strtolower($category->skl['skl_ov']),
                    			'{shopname}'=>$this->vars['name'],
                    			'{shopcity}'=>$this->vars['officeCity'],
                    			'{region_cities}'=>$regionCity,
                    			'{regions}'=>$regions,
                    			'{phones}'=>$this->vars['contactTel']
                    		);
                            $cat = new StdClass();
                            $cat->meta_title = str_replace(array_keys($replaces),array_values($replaces),$this->vars['title_category']);
                            $cat->meta_description = str_replace(array_keys($replaces),array_values($replaces),$this->vars['description_category']);
                            $cat->meta_keywords = str_replace(array_keys($replaces),array_values($replaces),$this->vars['keywords_category']);
    
                            $this->categories->update_category($category->id, $cat);
                            echo('<span style="background:#'.rand(10,99).'ee'.rand(10,99).'">Обновили категорию <b>'.$category->name."</b></span><br/>");
                        }
                        if($this->request->post('tab_type') == 'product'){
                            $products = $this->products->get_products(array('category_id'=>$category->id, 'limit'=>5000));
                            foreach($products as $product){
                                //if($product->id==18749)
                                
                                if($product->generate_meta){
                                    $brand = $this->brands->get_brand(intval($product->brand_id));
                                    $variants = array();
                                    $variants = $this->variants->get_variants(array('product_id'=>$product->id));
                                    $variant = reset($variants);
                                    $v_prices = array();
                                    foreach($variants as $v){
                                        $v_prices[] = $v->price;
                                    } 
                                    sort($v_prices);
                                    $variant_low_price = reset($v_prices);
        //if($product->id==18749){var_dump($v_prices);var_dump($variant_low_price);}
                                    $replaces = ARRAY(
                            			'{name}'=>$product->name,
                                        '{type_prefix}'=>$product->type_prefix,
                                        '{fullname}'=>$product->fullname,
                            			'{brand}'=>$brand->name,
                                        '{rus_brand}'=>$brand->rus_name,
                            			'{category}'=>$category->name,
                            			'{variant}'=>$variant->name,
                            			'{shopname}'=>$this->vars['name'],
                            			'{shopcity}'=>$this->vars['officeCity'],
                            			'{region_cities}'=>$regionCity,
                    			        '{regions}'=>$regions,
                            			'{phones}'=>$this->vars['contactTel'],
                                        '{l-name}'=>strtolower($product->name),
                                        '{l-type_prefix}'=>strtolower($product->type_prefix),
                                        '{l-category}'=>strtolower($category->name)
                                        //,'{price}'=>intval($variant_low_price)
                            		);
                                    $prod = new StdClass();
                                    $prod->meta_title = str_replace(array_keys($replaces),array_values($replaces),$this->vars['title_product']);
                            		$prod->meta_description = str_replace(array_keys($replaces),array_values($replaces),$this->vars['description_product']);
                            		$prod->meta_keywords = str_replace(array_keys($replaces),array_values($replaces),$this->vars['keywords_product']);
                                    //if($product->id==18749){
                                        //print_r($prod);die;
                                    $this->products->update_product($product->id, $prod);
                                    //print_r($prod);
                                }
                            }
                            echo('<i style="background:#EC'.rand(10,99).rand(10,99).'; color: white;">Обновили все товары из категории <b>'.$category->name."</b>   </i><br/> ");
                        }
                    }
                }
            }
            
            if($this->request->post('go_update_description')){   
                set_time_limit(500);

/////////////////////////////////////////////////// BRAND \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

                if($_POST['tab_type'] == 'brand'){
                    foreach($this->brands->get_brands() as $brand){
                        if($brand->generate_description){
                            $replaces = ARRAY(
                        			'{brand}'=>$brand->name,
                                    '{rus_brand}'=>$brand->rus_name,
                        			'{shopname}'=>$this->vars['name'],
                        			'{shopcity}'=>$this->vars['officeCity'],
                        			'{region_cities}'=>$regionCity,
                        			'{regions}'=>$regions,
                        			'{phones}'=>$this->vars['contactTel']
                        		);
                            shuffle($this->vars['brand_descriptions']);
                            
                            $b = new StdClass();
                            $b->description = str_replace(array_keys($replaces),array_values($replaces), $this->vars['brand_descriptions'][0]);
                            
                            $this->brands->update_brand($brand->id, $b);
                            echo('<span style="background:#'.rand(10,99).'ca'.rand(10,99).'">Обновили бренд <b>'.$brand->name."</b></span><br/>");
                        }
                    }
                }
                

///////////////////////////////////////////////////CATEGORY + BRAND\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 
               
                if($this->request->post('tab_type') == 'cat_brand'){                         
                    $brands = $this->brands->get_brands();
                    foreach($brands as $brand){
                        $cats_in_brand = $this->brands->get_brand_categories_4_seo($brand->id);
                        $cats_descipts = array();
                        foreach($cats_in_brand as $cat){
                            //var_dump($cat);die;
                            $cat->skl = unserialize($cat->skl);
                            
                            $replaces = ARRAY(
                    			'{skl}'=>$cat->skl['skl'],
                    			'{skl_mn}'=>$cat->skl['skl_mn'],
                    			'{skl_ov}'=>$cat->skl['skl_ov'],
                                '{l-skl}'=>mb_strtolower($category->skl['skl']),
                                '{l-skl_mn}'=>mb_strtolower($category->skl['skl_mn']),
                                '{l-skl_ov}'=>mb_strtolower($category->skl['skl_ov']),
                    			'{shopname}'=>$this->vars['name'],
                    			'{shopcity}'=>$this->vars['officeCity'],
                    			'{region_cities}'=>$regionCity,
                			    '{regions}'=>$regions,
                    			'{phones}'=>$this->vars['contactTel'],
                                '{category}'=>$cat->name,
                                '{l-category}'=>strtolower($cat->name),
                                '{rus_brand}'=>$brand->rus_name,
                                '{brand}'=>$cat->brand_name
                    		);
                            
                            $dc_template_id = -1;
                            foreach(unserialize($this->vars['categories_brand_templates']) as $ckey=>$ct){
                                $dcc_category = $this->categories->get_category(intval($ct));
                                $dcc_children = $dcc_category->children;
                                   
                                if(in_array($cat->id, $dcc_children)){
                                    $dc_template_id = $ckey;
                                }
                            }
                             
                            if($dc_template_id>-1){
                                $description = str_replace(array_keys($replaces),array_values($replaces), unserialize($this->vars['des_categories_brand_templates'])[$dc_template_id]);
                            }else{
                                shuffle($this->vars['descriptions_brand']);
                                $description = str_replace(array_keys($replaces),array_values($replaces), $this->vars['descriptions_brand'][0]);
                            }
                            
                            $cats_descipts[$cat->id] = $description;
                            
                        }
                        
                        $b = new StdClass();
                        $b->description_in_categories = serialize($cats_descipts);
                        $this->brands->update_brand($brand->id, $b);
                        echo('<i style="background:#AE'.rand(10,99).rand(10,99).'; color: white;">Обновили все описания для категорий в бренде <b>'.$brand->name."</b>   </i><br/> ");
                    }
                }
                
///////////////////////////////////////////////////CATEGORY + BRAND END\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 

///////////////////////////////////////////////////CATEGORY\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                if($this->request->post('tab_type') == 'category' ){
                    //var_dump($_POST);die; 
                    $categories = $this->categories->get_categories();
                    foreach($categories as $category){
                        if($category->generate_description){
                            $category->skl = unserialize($category->skl);
                            //var_dump($category->skl);."{brand}
                            $replaces = ARRAY(
                    			'{skl}'=>$category->skl['skl'],
                    			'{skl_mn}'=>$category->skl['skl_mn'],
                    			'{skl_ov}'=>$category->skl['skl_ov'],
                                '{l-skl}'=>mb_strtolower($category->skl['skl']),
                                '{l-skl_mn}'=>mb_strtolower($category->skl['skl_mn']),
                                '{l-skl_ov}'=>mb_strtolower($category->skl['skl_ov']),
                    			'{shopname}'=>$this->vars['name'],
                    			'{shopcity}'=>$this->vars['officeCity'],
                    			'{region_cities}'=>$regionCity,
                			    '{regions}'=>$regions,
                    			'{phones}'=>$this->vars['contactTel'],
                                '{category}'=>$category->name,
                                '{l-category}'=>strtolower($category->name),
                                
                    		);
                            
                            $ctemplate_id = -1;
                            foreach(unserialize($this->vars['categories_templates']) as $ckey=>$ct){
                                $c_category = $this->categories->get_category(intval($ct));
                                $c_children = $c_category->children;
        
                                if(in_array($category->id, $c_children)){
                                    $ctemplate_id = $ckey;
                                }
                            }
                            
                            $cat = new StdClass();
                        
                            if($ctemplate_id>-1){
                                $cat->description = str_replace(array_keys($replaces),array_values($replaces), unserialize($this->vars['des_categories_templates'])[$ctemplate_id]);
                            }else{
                                shuffle($this->vars['descriptions']);
                                $cat->description = str_replace(array_keys($replaces),array_values($replaces), $this->vars['descriptions'][0]);
                            }
                        
                            $this->categories->update_category($category->id, $cat);
                            echo('<span style="background:#'.rand(10,99).'ee'.rand(10,99).'">Обновили категорию <b>'.$category->name."</b></span><br/>");
                        }  
                    }
                     
                    if($this->request->post('tab_type') == 'product'){
                        if(empty($this->vars['full_description_product'])){
                            if(in_array($category->id, unserialize($this->vars['products_templates']))){
                                //$this->update_product_description($category);
                            }
                        }else{
                            //$this->update_product_description($category);
                        }
                    
                    }
                }
///////////////////////////////////////////////////CATEGORY END\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                
                if($this->request->post('tab_type') == 'product'){
                    $this->update_product_description_fast();
                }
            }
                
		}
		else{
			$this->config();
		}
	}
	
	function fetch()
	{
		if(count($this->errors) != 0)
			$this->design->assign('message_error', $this->errors);
		$this->design->assign('seo', (object)$this->vars);
        
        //print_r($this->vars);
        $this->design->assign('categories', $this->categories->get_categories_tree());
        
        $features = array();
        foreach(unserialize($this->vars['products_templates']) as $pt){
            $category = $this->categories->get_category(intval($pt));
            $features[$pt] = $this->features->get_features(array('category_id'=>$category->children));
        }        
        $this->design->assign('features', $features);
        //var_dump($features);
        
        $this->design->assign('tab_type', $this->request->post('tab_type'));
		return $this->design->fetch('seo.tpl');
        
	}
    
    private function update_product_description_fast(){
        //$products = $this->products->get_products(array('category_id'=>$category->id, 'limit'=>1000));//'id'=>array(23400, 23499), 'category_id'=>$category->id));

        
        $products_query = $this->db->placehold("SELECT p.id, p.name, p.fullname, p.type_prefix, b.name as brand_name, b.rus_name as brand_rus_name, v.name as variant_name
                                            FROM  s_products p 
                                            LEFT JOIN s_variants v ON v.product_id=p.id
                                            LEFT JOIN s_brands b ON b.id=p.brand_id
                                            WHERE p.generate_description = 1
                                            GROUP BY p.id");// AND p.id=24215
        $this->db->query($products_query);
        //$products = $this->db->results();
        //var_dump($products);die;
        $count = 0;
        foreach($this->db->results() as $product){
            
            $regionCity = str_replace(' ','',explode(",",$this->vars['regionCity']));
            $regions = str_replace(' ','',explode(",",$this->vars['regions']));
  
            shuffle($regionCity);
            shuffle($regions);
           	$regionCity = implode(", ",array_slice($regionCity, 0, rand(5,7)));
            $regions = implode(", ",array_slice($regions, 0, rand(5,7)));

            //if($product->generate_description){
            $product_categories = $this->categories->get_product_categories($product->id);
            $product_category = reset($product_categories);
            //var_dump($product_category);die;
            $category = $this->categories->get_category(intval($product_category->category_id));
            //$brand = $this->brands->get_brand(intval($product->brand_id));
            $variants = $this->variants->get_variants(array('product_id'=>$product->id));
            $variant = reset($variants);
            foreach($variants as $v){
                $v_prices[] = $v->price;
            } 
            sort($v_prices);
            $variant_low_price = reset($v_prices);
            
            $replaces = ARRAY(
    			'{name}'=>$product->name,
                '{type_prefix}'=>$product->type_prefix,
                '{fullname}'=>$product->fullname,
    			'{brand}'=>$product->brand_name,
                '{rus_brand}'=>$product->brand_rus_name,
    			'{category}'=>$category->name,
    			'{variant}'=>$variant->name,
    			'{shopname}'=>$this->vars['name'],
    			'{shopcity}'=>$this->vars['officeCity'],
    			'{region_cities}'=>$regionCity,
    			'{regions}'=>$regions,
    			'{phones}'=>$this->vars['contactTel'],
                '{l-name}'=>strtolower($product->name),
                '{l-type_prefix}'=>strtolower($product->type_prefix),
                '{l-category}'=>strtolower($category->name),
                '{all_cities}'=>$this->vars['regionCity']
                //,'{price}'=>$variant_low_price
    		);
            
            $replace_f = array();
            $pcategories = array();
            foreach($product_categories as $pc){
                $pcategories[] = $pc->category_id;
            }
            
            $features = $this->features->get_features(array('category_id'=>$pcategories));
            foreach($features as $f){
                $options = $this->features->get_options(array('product_id'=>$product->id, 'feature_id'=>$f->id));
                if(count($options)){
                    $replace_f['{f_'.$f->id.'}'] = $options[0]->value;
                }else{
                    $replace_f['{f_'.$f->id.'}'] = '';
                }
            }
            $replaces = array_merge($replaces, $replace_f);
            //var_dump($replaces);die;
            //var_dump($features);
            $template_id = -1;
            foreach(unserialize($this->vars['products_templates']) as $key=>$pt){
                $t_category = $this->categories->get_category(intval($pt));
                $t_children = $t_category->children;
               
                foreach($t_children as $tc){
                     
                    if(in_array($tc, $pcategories)){
                        $template_id = $key;
                        $template_to_product_ = unserialize($this->vars['des_products_templates']);
                        $template_to_product = $template_to_product_[$key];
                    }
                }
            }
            $prod = new StdClass();

            if($template_id>-1){
                $prod->body = str_replace(array_keys($replaces),array_values($replaces), $template_to_product);
            }else{
                shuffle($this->vars['product_descriptions']);
                $prod->body = str_replace(array_keys($replaces),array_values($replaces), $this->vars['product_descriptions'][0]);
            }
            $prod->delivery_description = str_replace(array_keys($replaces),array_values($replaces), $this->vars['delivery_description']);
    		//var_dump($prod->body);die;
            
            $this->products->update_product($product->id, $prod);
            $count++;
            print($product->name.', ');
            if($count%100==0){
                echo('<i style="background:#EC'.rand(10,99).rand(10,99).'; color: white;">Обновили <b>'.$count."</b> товаров</i><br/> ");
            }
            //}
        }
        echo('<i style="background:#EC'.rand(10,99).rand(10,99).'; color: white;">Обновление описания успешно закончилось, обновлено <b>'.$count."</b> товаров</i><br/> "); 
    }
	
	private function config()
	{
		//Читаем файл конфига
		if(is_file($this->config_file))
		{
			$this->vars = unserialize(file_get_contents($this->config_file));
		}
	}
	
	private function saveConfig()
	{
		return file_put_contents($this->config_file,serialize($this->vars));
	}
	
	private function skip($text)
	{
		$quotes = array ("\t", '\n', '\r', "\n", "\r", '\\', "/", "¬", "#", "@", "[", "]", "=", "*", "^", "%", "$", "<", ">", "?");
		return htmlspecialchars(strip_tags(str_replace($quotes,'',$text)));
	}
}
