<?php 

require_once('api/Simpla.php');

########################################
class OrdersAdmin extends Simpla
{
	public function fetch()
	{
	 	$filter = array();
	  	$filter['page'] = max(1, $this->request->get('page', 'integer'));
	  		
	  	$filter['limit'] = 40;
	  	
	    // Поиск
	  	$keyword = $this->request->get('keyword', 'string');
	  	if(!empty($keyword))
	  	{
		  	$filter['keyword'] = $keyword;
	 		$this->design->assign('keyword', $keyword);
		}

// Фильтр
$get_filter = $this->request->get('filter');
if($get_filter['date_from'])
$filter['date_from'] = date("Y-m-d 00:00:01",strtotime($get_filter['date_from']));

if($get_filter['date_to'])
$filter['date_to'] = date("Y-m-d 23:59:00",strtotime($get_filter['date_to']));

if($get_filter['delivery_from'])
$filter['delivery_from'] = date("Y-m-d 00:00:01",strtotime($get_filter['delivery_from']));

if($get_filter['delivery_to'])
$filter['delivery_to'] = date("Y-m-d 23:59:00",strtotime($get_filter['delivery_to']));

		// Фильтр по метке
	  	$label = $this->orders->get_label($this->request->get('label'));	  	
	  	if(!empty($label))
	  	{
		  	$filter['label'] = $label->id;
		 	$this->design->assign('label', $label);
		}


		// Обработка действий
		if($this->request->method('post'))
		{

			// Действия с выбранными
			$ids = $this->request->post('check');
			if(is_array($ids))
			switch($this->request->post('action'))
			{
				case 'delete':
				{
					foreach($ids as $id)
					{
						$o = $this->orders->get_order(intval($id));
						if($o->status<3)
						{
							$this->orders->update_order($id, array('status'=>3));
							$this->orders->open($id);							
						}
						else
							$this->orders->delete_order($id);
					}
					break;
				}
				case(preg_match('/^set_label_([0-9]+)/', $this->request->post('action'), $a) ? true : false):
				{
					$l_id = intval($a[1]);
					if($l_id>0)
					foreach($ids as $id)
					{
						$this->orders->add_order_labels($id, $l_id);
					}
					break;
				}
				case(preg_match('/^unset_label_([0-9]+)/', $this->request->post('action'), $a) ? true : false):
				{
					$l_id = intval($a[1]);
					if($l_id>0)
					foreach($ids as $id)
					{
						$this->orders->delete_order_labels($id, $l_id);
					}
					break;
				}
			}
		}		

		if(empty($keyword) && $this->request->get('status', 'string')!='all')
		{
			$status = $this->request->get('status', 'integer');
			$filter['status'] = $status;
		 	$this->design->assign('status', $status);
		}
		elseif($this->request->get('status', 'string')=='all') {
			$status = $this->request->get('status', 'string');
		 	$this->design->assign('status', $status);			
		}
				  	
	  	$orders_count = $this->orders->count_orders($filter);
		// Показать все страницы сразу
		if($this->request->get('page') == 'all')
			$filter['limit'] = $orders_count;	

		// Отображение
		$orders = array();
		foreach($this->orders->get_orders($filter) as $o)
			$orders[$o->id] = $o;
foreach ($orders as $order)
{
$order->purchases = $this->orders->get_purchases(array('order_id'=>$order->id));
$order->delivery = $this->delivery->get_delivery($order->delivery_id);
$order->payment = $this->payment->get_payment_method($order->payment_method_id);
$order->comments = $this->orders->get_comments(array('object_id'=>array($order->id),'limit'=>1));
}	 	
		// Метки заказов
		$orders_labels = array();
	  	foreach($this->orders->get_order_labels(array_keys($orders)) as $ol)
	  		$orders[$ol->order_id]->labels[] = $ol;
	  	
	 	$this->design->assign('pages_count', ceil($orders_count/$filter['limit']));
	 	$this->design->assign('current_page', $filter['page']);
	  	
	 	$this->design->assign('orders_count', $orders_count);
	
	 	$this->design->assign('orders', $orders);
	
		// Метки заказов
	  	$labels = $this->orders->get_labels();
	 	$this->design->assign('labels', $labels);
	  	
		return $this->design->fetch('orders.tpl');
	}
}
