<?php
//ini_set('display_errors', 1); 
//var_dump(ini_get('post_max_size'));
//var_dump(ini_get('max_input_vars'));
require_once('api/Simpla.php');
//print('<!--');print_r($_POST);print('-->');
class ProductsAdmin extends Simpla
{
	function fetch()
	{		

		$filter = array();
		$filter['page'] = max(1, $this->request->get('page', 'integer'));
			
		$filter['limit'] = $this->settings->products_num_admin;
	
		// Категории
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		
		// Текущая категория
		$category_id = $this->request->get('category_id', 'integer'); 
		if($category_id && $category = $this->categories->get_category($category_id))
	  		$filter['category_id'] = $category->children;
		    
		// Манагеры
		$managers = $this->managers->get_managers(); 
		$this->design->assign('managers', $managers);
			
		// Бренды категории
		$brands = $this->brands->get_brands(array('category_id'=>$category_id,"order"=>1));
		$this->design->assign('brands', $brands);  
		
		// Все бренды
		$all_brands = $this->brands->get_brands(array("order"=>1));
		$this->design->assign('all_brands', $all_brands);
        foreach($all_brands as $b){
            $bbrands[$b->id] = $b;
        }
        $this->design->assign('bbrands', $bbrands);
		
		$vendors = $this->parsers->get_parsers();
		$this->design->assign('vendors', $vendors);
        

		$marks = $this->marks->get_marks();
		$this->design->assign('marks', $marks);
		
		// Текущий бренд
		$brand_id = $this->request->get('brand_id', 'integer'); 
		if($brand_id && $brand = $this->brands->get_brand($brand_id))
			$filter['brand_id'] = $brand->id;
	
		// Текущий бренд
		$vendor_id = $this->request->get('vendor_id', 'integer'); 
		if($vendor_id && $vendor = $this->parsers->get_parser($vendor_id))
			$filter['vendor_id'] = $vendor->id;
            
        $sales_note = $this->request->get('sales_note');
        if($sales_note){
            $filter['sales_note'] = $sales_note;
            $this->design->assign('sales_note', $sales_note);
        } 
        
        if($f1 = $this->request->get('filter_d', 'string')){
            if($f1 == 'disc_more50')
				$filter['disc_more50'] = 1; 
            elseif($f1 == 'disc_less5')
				$filter['disc_less5'] = 1;      
        }
        $this->design->assign('filter_d', $f1);
		// Текущий фильтр
		if($f = $this->request->get('filter', 'string'))
		{
			if($f == 'featured')
				$filter['featured'] = 1; 
			elseif($f == 'discounted')
				$filter['discounted'] = 1;
            elseif($f == 'discounted_rev')
				$filter['discounted_rev'] = 1;
      		elseif($f == 'new')
				$filter['new'] = 1;
			elseif($f == 'in_stock')
				$filter['in_stock'] = 1;
			elseif($f == 'out_of_stock')
				$filter['out_of_stock'] = 1;
			elseif($f == 'ttoday')
				$filter['ttoday'] = 1;
			elseif($f == 'own_text')
				$filter['own_text'] = 1;
			elseif($f == 'viewed')
				$filter['viewed'] = 1; 
			elseif($f == 'yandex')
				$filter['yandex'] = 1; 
            elseif($f == 'k50')
				$filter['k50'] = 1;        
			elseif($f == 'card')
				$filter['card'] = 1; 
            elseif($f == 'sklad')
				$filter['sklad'] = 1;
            elseif($f == 'store')
				$filter['sklad'] = 2;
            elseif($f == 'rezerv')
				$filter['sklad'] = 3; 
            elseif($f == 'smartbikes')
				$filter['smartbikes'] = 1; 
            elseif($f == 'priceru')
				$filter['priceru'] = 1;  
          
			$this->design->assign('filter', $f);
		}
        //var_dump($filter);
	// Сортировка товаров
		if($sort = $this->request->get('sort', 'string'))
		$_SESSION['sort_admin'] = $sort;		
		if (!empty($_SESSION['sort_admin']))
		$filter['sort'] = $_SESSION['sort_admin'];			
		else
		$filter['sort'] = 'position_adm';			
		$this->design->assign('sort', $filter['sort']);	
			
			
			
			
		// Поиск
		$keyword = $this->request->get('keyword');
		if(!empty($keyword))
		{
	  		$filter['keyword'] = $keyword;
			$this->design->assign('keyword', $keyword);
		}
			
		// Обработка действий 	
		if($this->request->method('post'))
		{
			// Сохранение цен и наличия
			$prices = $this->request->post('price');
			$stocks = $this->request->post('stock');
            $compare_prices = $this->request->post('compare_price');
            $stockprices = $this->request->post('stockprice');
            $skus = $this->request->post('sku');
		
			foreach($prices as $id=>$price)
			{
                $compare_price = $compare_prices[$id];
                $sku = $skus[$id];
                $stockprice = $stockprices[$id];
				$stock = $stocks[$id];
                
				if($stock == '∞' || $stock == '')
					$stock = null;
					
				$this->variants->update_variant($id, array('price'=>$price, 'stock'=>$stock, 'compare_price'=>$compare_price, 'stockprice'=>$stockprice, 'sku'=>$sku));
			}
		
			// Сортировка
			$positions = $this->request->post('positions'); 		
				$ids = array_keys($positions);
			sort($positions);
			$positions = array_reverse($positions);
			foreach($positions as $i=>$position)
				$this->products->update_product($ids[$i], array('position'=>$position)); 
		
			
			// Действия с выбранными
			$ids = $this->request->post('check');
         
			if(!empty($ids))
			switch($this->request->post('action'))
			{
			    case 'disable':
			    {
			    	$this->products->update_product($ids, array('visible'=>0));
					break;
			    }
			    case 'enable':
			    {
			    	$this->products->update_product($ids, array('visible'=>1));
			        break;
			    }
			    case 'set_featured':
			    {
			    	$this->products->update_product($ids, array('featured'=>1));
					break;
			    }
			    case 'unset_featured':
			    {
			    	$this->products->update_product($ids, array('featured'=>0));
					break;
			    }
			    case 'set_new':
			    {
			    	$this->products->update_product($ids, array('new'=>1));
					break;
			    }
			    case 'unset_new':
			    {
			    	$this->products->update_product($ids, array('new'=>0));
					break;
			    }
				case 'set_ttoday':
			    {
			    	$this->products->update_product($ids, array('ttoday'=>1));
					break;
			    }
			    case 'unset_ttoday':
			    {
			    	$this->products->update_product($ids, array('ttoday'=>0));
					break;
			    }
				
				case 'set_warranty':
			    {
			    	$this->products->update_product($ids, array('warranty'=>1));
					break;
			    }
			    case 'unset_warranty':
			    {
			    	$this->products->update_product($ids, array('warranty'=>0));
					break;
			    }      
				
                case 'set_seller_warranty':
			    {
			    	$this->products->update_product($ids, array('seller_warranty'=>1));
					break;
			    }
			    case 'unset_seller_warranty':
			    {
			    	$this->products->update_product($ids, array('seller_warranty'=>0));
					break;
			    }
                
				case 'smartbikes':
			    {
			    	$this->products->update_product($ids, array('smartbikes'=>1));
					break;
			    }
                case 'unsmartbikes':
			    {
			    	$this->products->update_product($ids, array('smartbikes'=>0));
					break;
			    }
                
			    case 'set_yandex':
			    {
			    	$this->products->update_product($ids, array('yandex'=>1));
					break;
			    }
			    case 'unset_yandex':
			    {
			    	$this->products->update_product($ids, array('yandex'=>0));
					break;
			    }      
                case 'set_k50':
			    {
			    	$this->products->update_product($ids, array('k50'=>1));
					break;
			    }
			    case 'unset_k50':
			    {
			    	$this->products->update_product($ids, array('k50'=>0));
					break;
			    }  
                case 'set_priceru':
			    {
			    	$this->products->update_product($ids, array('priceru'=>1));
					break;
			    }
			    case 'unset_priceru':
			    {
			    	$this->products->update_product($ids, array('priceru'=>0));
					break;
			    }                  
			    case 'delete':
			    {
                    foreach($ids as $id)
						$this->products->delete_product($id);    
			        break;
			    }
			    case 'duplicate':
			    {
				    foreach($ids as $id)
				    	$this->products->duplicate_product(intval($id));
			        break;
			    }
			    case 'move_to_page':
			    {
		
			    	$target_page = $this->request->post('target_page', 'integer');
			    	
			    	// Сразу потом откроем эту страницу
			    	$filter['page'] = $target_page;
		
				    // До какого товара перемещать
				    $limit = $filter['limit']*($target_page-1);
				    if($target_page > $this->request->get('page', 'integer'))
				    	$limit += count($ids)-1;
				    else
				    	$ids = array_reverse($ids, true);
		

					$temp_filter = $filter;
					$temp_filter['page'] = $limit+1;
					$temp_filter['limit'] = 1;
					$target_product = array_pop($this->products->get_products($temp_filter));
					$target_position = $target_product->position;
				   	
				   	// Если вылезли за последний товар - берем позицию последнего товара в качестве цели перемещения
					if($target_page > $this->request->get('page', 'integer') && !$target_position)
					{
				    	$query = $this->db->placehold("SELECT distinct p.position AS target FROM __products p LEFT JOIN __products_categories AS pc ON pc.product_id = p.id WHERE 1 $category_id_filter $brand_id_filter ORDER BY p.position DESC LIMIT 1", count($ids));	
				   		$this->db->query($query);
				   		$target_position = $this->db->result('target');
					}
				   	
			    	foreach($ids as $id)
			    	{		    	
				    	$query = $this->db->placehold("SELECT position FROM __products WHERE id=? LIMIT 1", $id);	
				    	$this->db->query($query);	      
				    	$initial_position = $this->db->result('position');
		
				    	if($target_position > $initial_position)
				    		$query = $this->db->placehold("	UPDATE __products set position=position-1 WHERE position>? AND position<=?", $initial_position, $target_position);	
				    	else
				    		$query = $this->db->placehold("	UPDATE __products set position=position+1 WHERE position<? AND position>=?", $initial_position, $target_position);	
				    		
			    		$this->db->query($query);	      			    	
			    		$query = $this->db->placehold("UPDATE __products SET __products.position = ? WHERE __products.id = ?", $target_position, $id);	
			    		$this->db->query($query);	
				    }
			        break;
				}
			    case 'move_to_category':
			    {
			    	$category_id = $this->request->post('target_category_c', 'integer');
                   
			    	$filter['page'] = 1;
					$category = $this->categories->get_category($category_id);
	  				$filter['category_id'] = $category->children;
			    	
			    	foreach($ids as $id)
			    	{
			    		$query = $this->db->placehold("DELETE FROM __products_categories WHERE category_id=? AND product_id=? LIMIT 1", $category_id, $id);	
			    		$this->db->query($query);	      			    	
			    		$query = $this->db->placehold("UPDATE IGNORE __products_categories set category_id=? WHERE product_id=? ORDER BY position DESC LIMIT 1", $category_id, $id);	
			    		$this->db->query($query);
			    		if($this->db->affected_rows() == 0)
							$query = $this->db->query("INSERT IGNORE INTO __products_categories set category_id=?, product_id=?", $category_id, $id);	

				    }
			        break;
				}
				
				case 'move_to_subcategory':
			    {
			    	$category_id = $this->request->post('target_category_s', 'integer');
                   
			    	$filter['page'] = 1;
					$category = $this->categories->get_category($category_id);
	  				$filter['category_id'] = $category->children;
			    	
			    	foreach($ids as $id)
			    	{
//			    		$query = $this->db->placehold("UPDATE IGNORE __products_categories set category_id=? WHERE product_id=? ORDER BY position DESC LIMIT 1", $category_id, $id);	
//			    		$this->db->query($query);
//			    		if($this->db->affected_rows() == 0)
							$query = $this->db->query("INSERT IGNORE INTO __products_categories set category_id=?, product_id=?", $category_id, $id);	
				    }
			        break;
				}
				
				case 'make_mark':
			    {
			    	$mark_id = $this->request->post('target_mark', 'integer');			    
			    	foreach($ids as $id)
			    	{
							$this->marks->add_product_mark($id, $mark_id);	
				    }
			        break;
				}
                
                case 'sales_notes':
			    {
			    	$p->sales_notes = $this->request->post('sales_notes', 'string');			    
			    	foreach($ids as $id)
			    	{
							$this->products->update_product($id, $p);	
				    }
			        break;
				}
                
                case 'free_delivery':
                {
			    	$this->products->update_product($ids, array('free_delivery'=>1));
					break;
				}
				
			    case 'move_to_brand':
			    {
			    	$brand_id = $this->request->post('target_brand', 'integer');
			    	$brand = $this->brands->get_brand($brand_id);
			    	$filter['page'] = 1;
	  				$filter['brand_id'] = $brand_id;
			    	$query = $this->db->placehold("UPDATE __products set brand_id=? WHERE id in (?@)", $brand_id, $ids);	
			    	$this->db->query($query);	

					// Заново выберем бренды категории
					$brands = $this->brands->get_brands(array('category_id'=>$category_id));
					$this->design->assign('brands', $brands);
			    	      			    	
			        break;
				}
				
				case 'move_to_vendor':
			    {
			    	$vendor_id = $this->request->post('target_vendor', 'integer');
			    	$vendor = $this->parsers->get_parser($vendor_id);
			    	$query = $this->db->placehold("UPDATE __products set vendor=? WHERE id in (?@)", $vendor->id, $ids);	
			    	$this->db->query($query);	    	      			    	
			        break;
				}

			    case 'change_price':
			    {
					$percent = $this->request->post("how_many", "float");
                    $how_change = $this->request->post("how_change", "integer");
					$round = (int)$this->request->post("round");
					$check = $this->request->post("check");



					if(!$percent || !is_array($check) || count($check)==0){

						$this->design->assign('message_error', 'Не выполнено');
                        
					}else{
                        if($how_change==2){					   
                            $q=$this->db->placehold("UPDATE __variants SET price=price*(1+$percent/100) WHERE product_id IN (?@) ", $check);
                                          
                        }elseif($how_change==1){
                            $q=$this->db->placehold("UPDATE __variants SET price=price+$percent WHERE product_id IN (?@) ", $check);
                        }
                        
						$this->db->query($q);
                        
						if($round!='none'){
							$round=(int)$round;
							if($round>2) $round=2;
							if($round<-3) $round=-3;
							$q=$this->db->placehold("UPDATE __variants SET price=round(price,$round)  WHERE product_id IN (?@) ", $check);
							$this->db->query($q);
						}

						$this->design->assign('message_success',  'Выполнено');
					}	
			        break;
				}
				
				case 'change_oldprice':
			    {
					$percent = (float)$this->request->post("percent");
					$percent2 = (float)$this->request->post("percent2");
					$check = $this->request->post("check");

					if((!$percent AND !$percent2) || !is_array($check) || count($check)==0){

						$this->design->assign('message_error', 'Не выполнено');
					}else{

						$q=$this->db->placehold("SELECT compare_price FROM __variants WHERE product_id IN (?@) ", $check);
						$this->db->query($q);
						$r = $this->db->result("compare_price");
					
					if ($percent)
					{

						if ($r > 0) 
						$q=$this->db->placehold("UPDATE __variants SET compare_price=compare_price*(1+$percent/100) WHERE product_id IN (?@) ", $check);					
						else
						$q=$this->db->placehold("UPDATE __variants SET compare_price=price*(1+$percent/100) WHERE product_id IN (?@) ", $check);						
						$this->db->query($q);
					}
					if ($percent2)
					{
						if ($r > 0) 
						$q=$this->db->placehold("UPDATE __variants SET compare_price=compare_price+$percent2 WHERE product_id IN (?@) ", $check);
						else
						$q=$this->db->placehold("UPDATE __variants SET compare_price=price+$percent2 WHERE product_id IN (?@) ", $check);
					
						$this->db->query($q);
					}
 
		 
						$this->design->assign('message_success',  'Выполнено');
					};	

			        break;
				}

				 case 'del_oldprice':
			    {
 
					$check = $this->request->post("check");

					if(!is_array($check) || count($check)==0){

						$this->design->assign('message_error', 'Не выполнено');
					}else{



						$q=$this->db->placehold("UPDATE __variants SET compare_price = 0 WHERE product_id IN (?@) ", $check);
						$this->db->query($q);
				

						$this->design->assign('message_success',  'Выполнено');
					};	

			        break;
				}

			    case 'change_sklad':
			    {

					$sklad_id = (int)$this->request->post("sklad_id");
					$check = $this->request->post("check");

					if(!is_array($check) || count($check)==0){

						$this->design->assign('message_error', 'Не выполнено');
					}else{

		switch ($sklad_id) {
			case 1 :
				$sklad="В наличии";
				break;
			case 0 :
				$sklad="Нет в наличии";
				break;
			case 2 :
				$sklad="Под заказ";
				break;
			case 3 :
				$sklad="Ожидается";
				break;
			default :
				$sklad="В наличии";
				break;
		}

						$q=$this->db->placehold("UPDATE __products SET sklad_id=?, sklad=? WHERE id IN (?@) ", $sklad_id, $sklad, $check);
						$this->db->query($q);

						$this->design->assign('message_success',  'Выполнено');
					};	

			        break;
				}



			}			
		}

		// Отображение
		if(isset($brand))
			$this->design->assign('brand', $brand);
		
		if(isset($vendor))
			$this->design->assign('vendor', $vendor);
		
				if($this->request->get('pricelab') == "1")
		{
			
			setlocale(LC_ALL, 'ru_RU.UTF-8');
			file_put_contents("simpla/files/pricelab/pricelab.csv", file_get_contents("https://velocube:qweqweqwe@pricelabs.ru/export/velocube.ru/prices.csv"));
			$f = fopen("simpla/files/pricelab/pricelab.csv", 'r');					 
			$columns = fgetcsv($f, null, ";");
			
					$query = $this->db->placehold("SELECT v.id, concat(p.name, ' ', v.name) as pname FROM __products p LEFT JOIN __variants v ON p.id = v.product_id WHERE p.yandex = 1");
					$this->db->query($query);
					$insite = $this->db->results();
						foreach($insite as $ins)
						$comp_id[$ins->pname] = $ins->id;

					
			for($k=0; !feof($f); $k++)
			{ 
				$line = fgetcsv($f, 0, ";");
				
				if ($line[0] > 10 and $line[5] > 0)
				{
				if ($comp_id[iconv("WINDOWS-1251", "UTF-8", $line[1])] > 0)
					{
						$query = $this->db->placehold("UPDATE __variants SET `mprice` = ?, `murl` = ? WHERE id = ? LIMIT 1", intval($line[5]), $line[2], $comp_id[iconv("WINDOWS-1251", "UTF-8", $line[1])]);
						$this->db->query($query);
					}
				}
			}
		}
		
		if(isset($category))
			$this->design->assign('category', $category);
		
	  	$products_count = $this->products->count_products($filter);
		// Показать все страницы сразу
		if($this->request->get('page') == 'all')
			$filter['limit'] = $products_count;	
	  	
	  	$pages_count = ceil($products_count/$filter['limit']);
	  	$filter['page'] = min($filter['page'], $pages_count);
	 	$this->design->assign('products_count', $products_count);
	 	$this->design->assign('pages_count', $pages_count);
	 	$this->design->assign('current_page', $filter['page']);
	 	
		$products = array();
		foreach($this->products->get_products($filter) as $p)
			$products[$p->id] = $p;
	 	
        $sales_notes = array();
		if(!empty($products))
		{
		  	
			// Товары 
			$products_ids = array_keys($products);
			foreach($products as &$product)
			{
				$product->variants = array();
				$product->images = array();
				$product->properties = array();
                //$sales_notes[] = $product->sales_notes;
			}
		
			$variants = $this->variants->get_variants(array('product_id'=>$products_ids));
		
		 
			foreach($variants as &$variant)
			{
				$products[$variant->product_id]->variants[] = $variant;
			}
		
			$images = $this->products->get_images(array('product_id'=>$products_ids));
			foreach($images as $image)
				$products[$image->product_id]->images[$image->id] = $image;
		}
        
        $this->design->assign('sales_notes', $this->products->get_sales_notes());	 
		$this->design->assign('products', $products);
	
		return $this->design->fetch('products.tpl');
	}
}
