<?PHP
//ini_set('display_errors',1);
require_once('api/Simpla.php');

############################################
# Class Product - edit the static section
############################################
class ProductAdmin extends Simpla
{
	public function fetch()
	{
		$tostock = 0;
		$options = array();
		$product_categories = array();
		$variants = array();
		$images = array();
		$product_features = array();
		$related_products = array();
		$marks = array();
		$product_marks = array();
		/* product_videos */
		$product_videos = array();
		/* /product_videos */

		if($this->request->method('post') && !empty($_POST))
		{
			$product->id = $this->request->post('id', 'integer');
						
			$configss['date'] = '%I:%M %p';
			$configss['time'] = '%H:%M:%S';
			$product->date = $this->request->post('date', $configss);
			$product->name = $this->request->post('name');
			$product->visible = $this->request->post('visible', 'boolean');
			$product->own_text = $this->request->post('own_text', 'boolean');
			$product->featured = $this->request->post('featured');
			$product->brand_id = $this->request->post('brand_id', 'integer');
			$product->vendor = $this->request->post('vendor');
			$product->url = $this->request->post('url', 'string');
			$product->meta_title = $this->request->post('meta_title');
			$product->meta_keywords = $this->request->post('meta_keywords');
			$product->meta_description = $this->request->post('meta_description');
			$product->warranty = $this->request->post('warranty', 'boolean');
			$product->byrequest = $this->request->post('byrequest');
			
			$product->annotation = $this->request->post('annotation');
			$product->body = $this->request->post('body');
            
            /*yandex ext*/
            $product->yandex = $this->request->post('yandex');
            $product->type_prefix = $this->request->post('type_prefix');
            $product->smartbikes = $this->request->post('smartbikes');

            $product->seller_warranty = $this->request->post('seller_warranty');
            $product->sales_notes = $this->request->post('sales_notes');
            /*yandex ext/*/
            $product->free_delivery = $this->request->post('free_delivery');
      
            $product->new = $this->request->post('new');
            //$product->yandex = $this->request->post('yandex');
            $product->rating = $this->request->post('rating');
            $product->votes = $this->request->post('votes');
            $product->atribut = $this->request->post('atribut');
            $product->video = $this->request->post('video');
            //$product->sklad = $this->request->post('sklad');            
            $product->sklad_id = $this->request->post('sklad_id');
            $product->generate_meta = ($this->request->post('generate_meta'))?1:2;
            $product->generate_description = $this->request->post('generate_description', 'boolean');
            $product->k50 = $this->request->post('k50');
            $product->priceru = $this->request->post('priceru');
            $product->delivery_description = $this->request->post('delivery_description');
            
            if($product->brand_id){
                $bbrand = $this->brands->get_brand(intval($product->brand_id));	
                $product->fullname = $product->type_prefix." ".$bbrand->name." ".$product->name;
            }else{
                $product->fullname = $product->type_prefix." ".$product->name;
            }        
                
            switch ($product->sklad_id) {
            	case 1 :
            		$product->sklad="В наличии";
            		break;
            	case 0 :
            		$product->sklad="Нет в наличии";
            		break;
            	case 2 :
            		$product->sklad="Под заказ";
            		break;
            	case 3 :
            		$product->sklad="Ожидается";
            		break;
            	default :
            		$product->sklad="В наличии";
            		break;
            }

			
			
			// Варианты товара
			if($this->request->post('variants'))
			foreach($this->request->post('variants') as $n=>$va) 
				foreach($va as $i=>$v)
					$variants[$i]->$n = $v;		
			
			// Категории товара
			$product_categories = $this->request->post('categories');
			if(is_array($product_categories))
			{
				foreach($product_categories as $c)
					$pc[]->id = $c;
				$product_categories = $pc;
			}

			// Свойства товара
   	    	$options = $this->request->post('options');
			if(is_array($options))
			{
				foreach($options as $f_id=>$val)
				{
					$po[$f_id]->feature_id = $f_id;
					$po[$f_id]->value = $val;
				}
				$options = $po;
			}

			// Связанные товары
			if(is_array($this->request->post('related_products')))
			{
				foreach($this->request->post('related_products') as $p)
				{
					$rp[$p]->product_id = $product->id;
					$rp[$p]->related_id = $p;
				}
				$related_products = $rp;
			}
			
			/* product_videos */
			if(is_array($this->request->post('videos')))
				foreach($this->request->post('videos') as $k=>$v)
					if(!empty($v)){
						$product_videos[$k]->link = $v;
						$tmp = explode('/',$v);
						$tmp = $tmp[count($tmp)-1];
						$tmp = stristr($tmp,'v=');
						$b_p = strpos($tmp,'&');
						if($b_p){
							$tmp = substr($tmp,0,$b_p);
						}
						$tmp = substr($tmp,2);
						$product_videos[$k]->vid = $tmp;
					}
			/* /product_videos */
		
			if(empty($product->date)){
			
			$product->date = date("d.m.Y");
			}
			
			 //Метки товара
			if(explode('/',$this->request->post('marks')))
			$ids_marks = explode('/',$this->request->post('marks'));	
			
			// Не допустить пустое название товара.
			if(empty($product->name))
			{			
				$this->design->assign('message_error', 'empty_name');
				if(!empty($product->id))
					$images = $this->products->get_images(array('product_id'=>$product->id));
			}
			// Не допустить одинаковые URL разделов.
			elseif(($p = $this->products->get_product($product->url)) && $p->id!=$product->id)
			{			
				$this->design->assign('message_error', 'url_exists');
				if(!empty($product->id))
					$images = $this->products->get_images(array('product_id'=>$product->id));
			}
			else
			{
				if(empty($product->id))
				{
	  				$product->id = $this->products->add_product($product);
	  				$product = $this->products->get_product($product->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->products->update_product($product->id, $product);
  	    			$product = $this->products->get_product($product->id);
					$this->design->assign('message_success', 'updated');
  	    		}	
   	    		
   	    		if($product->id)
   	    		{
	   	    		// Категории товара
	   	    		$query = $this->db->placehold('DELETE FROM __products_categories WHERE product_id=?', $product->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($product_categories))
		  		    {
		  		    	foreach($product_categories as $i=>$category)
	   	    				$this->categories->add_product_category($product->id, $category->id, $i);
	  	    		}
	
	   	    		// Варианты
		  		    if(is_array($variants))
		  		    { 
	 					$variants_ids = array();
						foreach($variants as $index=>&$variant)
						{
							if($variant->stock == '∞' || $variant->stock == '')
								$variant->stock = null;
                            
							if($variant->sku)
							{
								$variant->sku = str_replace("     "," ", $variant->sku);
								$variant->sku = str_replace("    "," ", $variant->sku);
								$variant->sku = str_replace("   "," ", $variant->sku);
								$variant->sku = str_replace("  "," ", $variant->sku);
							}
							// Удалить файл
							if(!empty($_POST['delete_attachment'][$index]))
							{
								$this->variants->delete_attachment($variant->id);
							}
	
		 					// Загрузить файлы
		 					if(!empty($_FILES['attachment']['tmp_name'][$index]) && !empty($_FILES['attachment']['name'][$index]))
		 					{
			 					$attachment_tmp_name = $_FILES['attachment']['tmp_name'][$index];					
			 					$attachment_name = $_FILES['attachment']['name'][$index];
		 						move_uploaded_file($attachment_tmp_name, $this->config->root_dir.'/'.$this->config->downloads_dir.$attachment_name);
		 						$variant->attachment = $attachment_name;
		 					}
	
								
							if($variant->id)
								$this->variants->update_variant($variant->id, $variant);
							else
							{
								$variant->product_id = $product->id;
								$variant->id = $this->variants->add_variant($variant);
							}
							$variant = $this->variants->get_variant($variant->id);
							
							if ($variant->compare_price > 0)
								$tostock = 1;
							
					 		$variants_ids[] = $variant->id;
						}
						
	
						// Удалить непереданные варианты
						$current_variants = $this->variants->get_variants(array('product_id'=>$product->id));
						foreach($current_variants as $current_variant)
							if(!in_array($current_variant->id, $variants_ids))
	 							$this->variants->delete_variant($current_variant->id);
	 							 					
	 					//if(!empty($))
						
						// Отсортировать  варианты
						asort($variants_ids);
						$i = 0;
						foreach($variants_ids as $variant_id)
						{ 
							$this->variants->update_variant($variants_ids[$i], array('position'=>$variant_id));
							$i++;
						}
					}
	
					// Удаление изображений
					$images = (array)$this->request->post('images');
					$current_images = $this->products->get_images(array('product_id'=>$product->id));
					foreach($current_images as $image)
					{
						if(!in_array($image->id, $images))
	 						$this->products->delete_image($image->id);
						}
	
					// Порядок изображений
					if($images = $this->request->post('images'))
					{
	 					$i=0;
						foreach($images as $id)
						{
							$this->products->update_image($id, array('position'=>$i));
							$i++;
						}
					}
	   	    		// Загрузка изображений
		  		    if($images = $this->request->files('images'))
		  		    {
						for($i=0; $i<count($images['name']); $i++)
						{
				 			if ($image_name = $this->image->upload_image($images['tmp_name'][$i], $images['name'][$i]))
				 			{
			  	   				$this->products->add_image($product->id, $image_name);
			  	   			}
							else
							{
								$this->design->assign('error', 'error uploading image');
							}
						}
					}
	   	    		// Загрузка изображений из интернета
		  		    if($images = $this->request->post('images_urls'))
		  		    {
						foreach($images as $url)
						{
							if(!empty($url) && $url != 'http://')
					 			$this->products->add_image($product->id, $url);
						}
					}
					$images = $this->products->get_images(array('product_id'=>$product->id));
	
	   	    		// Характеристики товара
	   	    		
	   	    		// Удалим все из товара
					foreach($this->features->get_product_options($product->id) as $po)
						$this->features->delete_option($product->id, $po->feature_id);
						
					// Свойства текущей категории
					$category_features = array();
					foreach($this->features->get_features(array('category_id'=>$product_categories[0])) as $f)
						$category_features[] = $f->id;
	
	  	    		if(is_array($options))
					foreach($options as $option)
					{
						if(in_array($option->feature_id, $category_features))
							$this->features->update_option($product->id, $option->feature_id, $option->value);
					}
					
					// Новые характеристики
					$new_features_names = $this->request->post('new_features_names');
					$new_features_values = $this->request->post('new_features_values');
					if(is_array($new_features_names) && is_array($new_features_values))
					{
						foreach($new_features_names as $i=>$name)
						{
							$value = trim($new_features_values[$i]);
							if(!empty($name) && !empty($value))
							{
								$query = $this->db->placehold("SELECT * FROM __features WHERE name=? LIMIT 1", trim($name));
								$this->db->query($query);
								$feature_id = $this->db->result('id');
								if(empty($feature_id))
								{
									$feature_id = $this->features->add_feature(array('name'=>trim($name)));
								}
								$this->features->add_feature_category($feature_id, reset($product_categories)->id);
								$this->features->update_option($product->id, $feature_id, $value);
							}
						}
						// Свойства товара
						$options = $this->features->get_product_options($product->id);
					}
					
					// Связанные товары
	   	    		$query = $this->db->placehold('DELETE FROM __related_products WHERE product_id=?', $product->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($related_products))
		  		    {
		  		    	$pos = 0;
		  		    	foreach($related_products  as $i=>$related_product)
	   	    				$this->products->add_related_product($product->id, $related_product->related_id, $pos++);
	  	    		}
					/* product_videos */
					$query = $this->db->placehold('DELETE FROM __products_videos WHERE product_id=?', $product->id);
					$this->db->query($query);
					if(is_array($product_videos))
					{
						  $pos = 0;
						foreach($product_videos  as $i=>$video)
							$this->products->add_product_video($product->id, $video->link, $pos++);
					}
					/* /product_videos */
					//Метки товара
					$query = $this->db->placehold('DELETE FROM __products_marks WHERE product_id=?', $product->id);
	   	    		$this->db->query($query);
					if(is_array($ids_marks))
					{
					     foreach($ids_marks  as $i)
						 if ($i != 0)
						 $this->marks->add_product_mark($product->id, $i);
					
					}

  	    		}
			}
			
			//header('Location: '.$this->request->url(array('message_success'=>'updated')));
		}
		else
		{
			$product->id = $this->request->get('id', 'integer');
			$product = $this->products->get_product(intval($product->id));

			if($product && $product->id)
			{
				
				// Категории товара
				$product_categories = $this->categories->get_categories(array('product_id'=>$product->id));
				
				// Варианты товара
				$variants = $this->variants->get_variants(array('product_id'=>$product->id));
				
				// Изображения товара
				$images = $this->products->get_images(array('product_id'=>$product->id));
				
				// Свойства товара
				$options = $this->features->get_options(array('product_id'=>$product->id));
				
				// Связанные товары
				$related_products = $this->products->get_related_products(array('product_id'=>$product->id));
				
				/* product_videos */
				$product_videos = $this->products->get_videos(array('product_id'=>$product->id));
				/* .product_videos */
			}
			else
			{
				// Сразу активен
				$product->visible = 1;			
			}
		}
		
		
		if(empty($variants))
			$variants = array(1);
			
		if(empty($product_categories))
		{
			if($category_id = $this->request->get('category_id'))
				$product_categories[0]->id = $category_id;		
			else
				$product_categories = array(1);
		}
		if(empty($product->brand_id) && $brand_id=$this->request->get('brand_id'))
		{
			$product->brand_id = $brand_id;
		}
			
		if(!empty($related_products))
		{
			foreach($related_products as &$r_p)
				$r_products[$r_p->related_id] = &$r_p;
			$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
			foreach($temp_products as $temp_product)
				$r_products[$temp_product->id] = $temp_product;
		
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
			foreach($related_products_images as $image)
			{
				$r_products[$image->product_id]->images[] = $image;
			}
		}
			
		if(is_array($options))
		{
			$temp_options = array();
			foreach($options as $option)
				$temp_options[$option->feature_id] = $option;
			$options = $temp_options;
		}
		$bbrand = $this->brands->get_brand(intval($product->brand_id));	
        $this->design->assign('bbrand', $bbrand);
        $product->fullname = $product->type_prefix." ".$bbrand->name." ".$product->name;
        
		$this->design->assign('product', $product);
		
		$vendors = $this->parsers->get_parsers();
		$this->design->assign('vendors', $vendors);

		if($product && $product->id) {
			// Категории товара
			$product_categories = $this->categories->get_categories(array('product_id' => $product->id));
		}
		
		$this->design->assign('product_categories', $product_categories);
		$this->design->assign('product_variants', $variants);
		$this->design->assign('product_images', $images);
		$this->design->assign('options', $options);
		$this->design->assign('related_products', $related_products);
		/* product_videos */
		$this->design->assign('product_videos', $product_videos);
		/* /product_videos */
		
		
		// Все бренды
		$brands = $this->brands->get_brands(array('order'=>1));
		$this->design->assign('brands', $brands);
		
		//Все метки
		foreach($product_categories as $pc){
			$filter['category_id'][] = $pc->id;
		}
		$marks = $this->marks->get_marks($filter);
		$this->design->assign('marks', $marks);
		
		//Метки товара
		if ($product->id){
		$product_marks = $this->marks->get_product_marks($product->id);
		$this->design->assign('product_marks', $product_marks);
		}
		// Все категории
		
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);


		// Все свойства
		$features = $this->features->get_features();
		$this->design->assign('features', $features);
		
		// Связка категорий со свойствами
		$categories_features = array();
		$query = $this->db->placehold('SELECT category_id, feature_id FROM __categories_features');
		$this->db->query($query);
		$c_f = $this->db->results();
		foreach($c_f as $i)
		{
			$categories_features[$i->category_id][] = $i->feature_id;
		}
		$this->design->assign('categories_features', $categories_features);
		
		//$this->design->assign('message_success', $this->request->get('message_success', 'string'));
		//$this->design->assign('message_error',   $this->request->get('message_error', 'string'));

 	  	return $this->design->fetch('product.tpl');
	}
}