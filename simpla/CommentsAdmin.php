<?PHP 

require_once('api/Simpla.php');

########################################
class CommentsAdmin extends Simpla
{


  function fetch()
  {
  
     if ($this->request->get('gop', 'integer') == 1)
	 {
		 $this->parsers->mk_op();
	 }
  
 	$filter = array();
  	$filter['page'] = max(1, $this->request->get('page', 'integer'));
  		
  	$filter['limit'] = 40;
 
    $item_id = $this->request->get('item_id', 'integer');
    if($item_id)
    $filter['object_id'] = $item_id;
    
    // Тип
    $type = $this->request->get('type', 'string');
    if($type)
    {
    	$filter['type'] = $type;
 		$this->design->assign('type', $type);
 	}
	
	// Тип
    $rateme = $this->request->get('rateme', 'integer');
    if($rateme)
    {
    	$filter['rateme'] = $rateme;
 		$this->design->assign('rateme', $rateme);
 	}
    
    // На главной
    $main_page = $this->request->get('main_page', 'integer');
    if($main_page)
    {
    	$filter['main_page'] = $main_page;
 		$this->design->assign('main_page', $main_page);
 	}
    
    // Поиск
  	$keyword = $this->request->get('keyword', 'string');
  	if(!empty($keyword))
  	{
	  	$filter['keyword'] = $keyword;
 		$this->design->assign('keyword', $keyword);
	}

  
  	// Обработка действий 	
  	if($this->request->method('post'))
  	{
		
		// Действия с выбранными
		$ids = $this->request->post('check');
		if(!empty($ids) && is_array($ids))
		switch($this->request->post('action'))
		{
		    case 'approve':
		    {
				foreach($ids as $id)
					$this->comments->update_comment($id, array('approved'=>1));    
		        break;
		    }
		    case 'delete':
		    {
				foreach($ids as $id)
					$this->comments->delete_comment($id);    
		        break;
		    }
		}		

		// Принимаем комментарий
		if ($this->request->method('post') && $this->request->post('comment'))
		{
			$comment->name = $this->request->post('name');
			$comment->text = $this->request->post('text');
      $comment->parent_id = $this->request->post('parent_id');
      $comment->admin = $this->request->post('admin');
			
			// Передадим комментарий обратно в шаблон - при ошибке нужно будет заполнить форму
			$this->design->assign('comment_text', $comment->text);
			$this->design->assign('comment_name', $comment->name);
      $this->design->assign('parent_id', $comment->parent_id);
			
			// Проверяем капчу и заполнение формы
      if (empty($comment->name))
			{
				$this->design->assign('error', 'empty_name');
			}
			elseif (empty($comment->text))
			{
				$this->design->assign('error', 'empty_comment');
			}
			else
			{
				// Создаем комментарий
				$comment->object_id = $this->request->post('object_id');
				$comment->type      = $this->request->post('type');;
				$comment->ip        = $_SERVER['REMOTE_ADDR'];
				
					$comment->approved = 1;
				
				// Добавляем комментарий в базу
				$comment_id = $this->comments->add_comment($comment);
				

			}			
		}


		
 	}

  

	// Отображение
  	$comments_count = $this->comments->count_comments($filter);
	// Показать все страницы сразу
	if($this->request->get('page') == 'all' || $item_id || $type == 'shop')
		$filter['limit'] = $comments_count;
    if($item_id || $type == 'shop')
    $comments = $this->comments->get_comments_tree($filter, true);
    else	
  	$comments = $this->comments->get_comments($filter, true);
  	
  	// Выбирает объекты, которые прокомментированы:
  	$products_ids = array();
  	$posts_ids = array();
    $articles_ids = array();
  	foreach($comments as $comment)
  	{
  		if($comment->type == 'product')
  			$products_ids[] = $comment->object_id;
  		if($comment->type == 'blog')
  			$posts_ids[] = $comment->object_id;
  		if($comment->type == 'article')
  			$articles_ids[] = $comment->object_id;	
  	}
	$products = array();
	foreach($this->products->get_products(array('id'=>$products_ids)) as $p)
		$products[$p->id] = $p;

  	$posts = array();
  	foreach($this->blog->get_posts(array('id'=>$posts_ids)) as $p)
  		$posts[$p->id] = $p;
  		
    $articles = array();
  	foreach($this->articles->get_articles(array('id'=>$articles_ids)) as $p)
  		$articles[$p->id] = $p; //print_r($articles); 		
  		
  	foreach($comments as &$comment)
  	{
  		if($comment->type == 'product' && isset($products[$comment->object_id]))
  			$comment->product = $products[$comment->object_id];
  		if($comment->type == 'blog' && isset($posts[$comment->object_id]))
  			$comment->post = $posts[$comment->object_id];
  		if($comment->type == 'article' && isset($articles[$comment->object_id]))
  			$comment->article = $articles[$comment->object_id];	
  	}
  	
  	
 	$this->design->assign('pages_count', ceil($comments_count/$filter['limit']));
 	$this->design->assign('current_page', $filter['page']);

 	$this->design->assign('comments', $comments);
 	$this->design->assign('comments_count', $comments_count);
 
   if($item_id) 
   {
  		if($type == 'product')
  			$object = $this->products->get_product((integer)$item_id);
  		if($type == 'blog')
  			$object = $this->blog->get_post((integer)$item_id);
  		if($type == 'article')
  			$object = $this->articles->get_article((integer)$item_id);
        
      $this->design->assign('object', $object);  
   }         
  
  
  if($item_id || $type == 'shop')
  return $this->design->fetch('comments_tree.tpl');
  else
	return $this->design->fetch('comments.tpl');
  }
}


?>