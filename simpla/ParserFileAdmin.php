 <?php

require_once('api/Simpla.php');


############################################
# Class Category - Edit the good gategory
############################################
class ParserFileAdmin extends Simpla
{



  function fetch()
  {
		if($this->request->method('post'))
		{
			   if($_FILES["filename"]["size"] > 1024*3*1024)
		{
			echo ("Размер файла превышает три мегабайта");
			exit;
		}
		// Проверяем загружен ли файл
				if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
		{
			// Если файл загружен успешно, перемещаем его
			// из временной директории в конечную
			move_uploaded_file($_FILES["filename"]["tmp_name"], "simpla/files/prices/".$_FILES["filename"]["name"]);
			
			$ok = "Файл ".$_FILES["filename"]["name"]." успешно загружен";
		} else {
			$ok = "Ошибка загрузки файла";
		}
		}
		
		
		$pfiles = $this->parsers->get_files();
		$this->design->assign('pfiles', $pfiles);
		$this->design->assign('suk', $ok);
 
		return  $this->design->fetch('parserfile.tpl');
	}
}