<?PHP
require_once('api/Simpla.php');

class FeatureAdmin extends Simpla
{

	function fetch()
	{
		if($this->request->method('post'))
		{
			$feature->id = $this->request->post('id', 'integer');
			$feature->name = $this->request->post('name');
			$feature->in_filter = intval($this->request->post('in_filter'));
			$feature->separ = intval($this->request->post('separ'));
			$feature_categories = $this->request->post('feature_categories');
            
            $feature->prefix = $this->request->post('prefix');
            $feature->postfix = $this->request->post('postfix');
            $feature->mode = $this->request->post('mode');
            $feature->description = $this->request->post('description');
            $feature->strict = $this->request->post('strict');
            $feature->allowed_values = $this->request->post('allowed_values');

			if(empty($feature->id))
			{
  				$feature->id = $this->features->add_feature($feature);
  				$feature = $this->features->get_feature($feature->id);
				$this->design->assign('message_success', 'added');
  			}
			else
			{
				$this->features->update_feature($feature->id, $feature);
				$feature = $this->features->get_feature($feature->id);
				$this->design->assign('message_success', 'updated');
                
                /*transfer_values*/
                $tranfer_feature = $this->request->post('transfer_feature', 'integer');
                $transfer_from_category = $this->request->post('transfer_from_category', 'integer');
    
                
                /*/transfer_values/*/
			}
            //var_dump($feature_categories);
			$this->features->update_feature_categories($feature->id, $feature_categories);

			
			if( $tranfer_feature && $transfer_from_category ){
                $all_category_product = $this->products->get_products(array('category_id'=>$transfer_from_category, 'limit'=>10000));
                foreach($all_category_product as $p){
                    $product_options = $this->features->get_product_options($p->id);
                    
                    foreach($product_options as $po){
                        if($po->feature_id == $tranfer_feature)
                            $options[$po->product_id] = $po->value;
                    }
                }

                foreach($options as $pid=>$value){
                    $this->features->update_option($pid, $feature->id, $value);
                    $this->features->update_option($pid, $tranfer_feature, '');
                }
                
                $this->db->query("DELETE FROM s_categories_features WHERE category_id=? AND feature_id=?", $transfer_from_category, $tranfer_feature);
                $this->features->add_feature_category($feature->id, $transfer_from_category);
            }
			
		}
		else
		{
			$feature->id = $this->request->get('id', 'integer');
			$feature = $this->features->get_feature($feature->id);
		}

		$feature_categories = array();	
		if($feature)
		{	
			$feature_categories = $this->features->get_feature_categories($feature->id);
            $feature_options = $this->features->get_options(array('feature_id'=>$feature->id));
            foreach($feature_options as $fo){
                $feature_values[] = $fo->value;
            }
		}
		
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		$this->design->assign('feature', $feature);
		$this->design->assign('feature_categories', $feature_categories);
        $this->design->assign('feature_values', $feature_values);
        
        $this->design->assign('features', $this->features->get_features());
        
		return $this->body = $this->design->fetch('feature.tpl');
	}
}




