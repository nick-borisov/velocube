<?php

/**
 * Simpla CMS
 *
 * @copyright	2012 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */
 
require_once('api/Simpla.php');

class CouponsAdmin extends Simpla
{
	public function fetch()
	{
		// Обработка действий
		if($this->request->method('post'))
		{
            $positions = $this->request->post('positions'); 		
				$ids = array_keys($positions);
			sort($positions);
			$positions = array_reverse($positions);
//var_dump($positions);
			foreach($positions as $i=>$position)
				$this->coupons->update_coupon($ids[$i], array('position'=>$position)); 
                
			// Действия с выбранными
			$ids = $this->request->post('check');
			if(is_array($ids) && count($ids)>0)
			switch($this->request->post('action'))
			{
			    case 'delete':
			    {
				    foreach($ids as $id)
						$this->coupons->delete_coupon($id);    
			        break;
			    }
			}				
		}
        
        // Категории
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		
        $filter = array();
        
		// Текущая категория
		$category_id = $this->request->get('category_id', 'integer'); 
		if($category_id && $category = $this->categories->get_category($category_id))
	  		$filter['category_id'] = $category->children;
        
		
		$filter['page'] = max(1, $this->request->get('page', 'integer')); 		
		$filter['limit'] = 20;
  	  	
		// Бренды категории
		$brands = $this->brands->get_brands(array('category_id'=>$category_id,"order"=>1));
		$this->design->assign('brands', $brands);  
		
		// Все бренды
		$all_brands = $this->brands->get_brands(array("order"=>1));
		$this->design->assign('all_brands', $all_brands);
        foreach($all_brands as $b){
            $bbrands[$b->id] = $b;
        }
        $this->design->assign('bbrands', $bbrands);
        
		// Текущий бренд
		$brand_id = $this->request->get('brand_id', 'integer'); 
		if($brand_id && $brand = $this->brands->get_brand($brand_id))
			$filter['brand_id'] = $brand->id;
        
		// Поиск
		$keyword = $this->request->get('keyword', 'string');
		if(!empty($keyword))
		{
			$filter['keyword'] = $keyword;
			$this->design->assign('keyword', $keyword);
		}
        
        $filter['auto_discount'] = 0;
        $auto_discount = $this->request->get('auto_discount');
        if($auto_discount){
            $filter['auto_discount'] = 1;
        }		
		
	  	$coupons_count = $this->coupons->count_coupons($filter);
	  	
        // Показать все страницы сразу
		if($this->request->get('page') == 'all')
			$filter['limit'] = $coupons_count;	
        $pages_count = ceil($coupons_count/$filter['limit']);
	  	$filter['page'] = min($filter['page'], $pages_count);
	 	$this->design->assign('coupons_count', $coupons_count);
	 	$this->design->assign('pages_count', $pages_count);
	 	$this->design->assign('current_page', $filter['page']);


		$coupons = $this->coupons->get_coupons($filter);
		
        if(isset($category))
			$this->design->assign('category', $category);
        
       	// Отображение
		if(isset($brand))
			$this->design->assign('brand', $brand);
        		
		$this->design->assign('coupons', $coupons);
		return $this->design->fetch('coupons.tpl');
	}
}
