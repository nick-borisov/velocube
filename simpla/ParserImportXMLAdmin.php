<?php
//ini_set('display_errors',1);
require_once('api/Simpla.php');

############################################
# Class Category - Edit the good gategory
############################################
class ParserImportXMLAdmin extends Simpla
{



  function fetch()
  {
		if($this->request->method('post'))
		{
            $pareser = new StdClass();
            $parser->id = 				$this->request->post('id');
            $parser->name = 			$this->request->post('name');
            $parser->active = 			$this->request->post('active');
            $parser->xmlurl =           $this->request->post('xmlurl');
            $parser->limit_parse =      intval($this->request->post('limit_parse'));
            $parser->description =		$this->request->post('description');
            
            if($parser->active_margin = $this->request->post('active_margin'))
                $parser->margin = serialize($this->request->post('marja'));
            
            $parser->brand_id = $this->request->post('brand_id');
            $parser->vendor_id = $this->request->post('vendor_id');
            $parser->category_id = $this->request->post('category_id');
        
            if(!isset($_POST['get_tags'])){

                $parser->xmlname =		    $this->request->post('xmlname');
                $parser->xmlname_attr =		$this->request->post('xmlname_attr');
                if($parser->xmlname=='xpath'){
                    $parser->xmlname_attr = $this->request->post('xmlname_xpath');
                }
                
                $parser->xmltypeprefix =			$this->request->post('xmltypeprefix');
                $parser->xmltypeprefix_attr =	$this->request->post('xmltypeprefix_attr');
                if($parser->xmltypeprefix=='xpath'){
                    $parser->xmltypeprefix_attr = $this->request->post('xmltypeprefix_xpath');
                }
                
                $parser->xmlbrand =			$this->request->post('xmlbrand');
                $parser->xmlbrand_attr =	$this->request->post('xmlbrand_attr');
                if($parser->xmlbrand=='xpath'){
                    $parser->xmlbrand_attr = $this->request->post('xmlbrand_xpath');
                }
                
                $parser->xmlimage =			$this->request->post('xmlimage');
                $parser->xmlimage_attr =	$this->request->post('xmlimage_attr');
                if($parser->xmlimage=='xpath'){
                    $parser->xmlimage_attr = $this->request->post('xmlimage_xpath');
                }
                
                $parser->xmlimage_2 =			$this->request->post('xmlimage_2');
                $parser->xmlimage_2_attr =	$this->request->post('xmlimage_2_attr');
                if($parser->xmlimage_2=='xpath'){
                    $parser->xmlimage_2_attr = $this->request->post('xmlimage_2_xpath');
                }
                
                $parser->xmldesctiption =		$this->request->post('xmldesctiption');
                $parser->xmldesctiption_attr =		$this->request->post('xmldesctiption_attr');
                if($parser->xmldesctiption=='xpath'){
                    $parser->xmldesctiption_attr = $this->request->post('xmldesctiption_xpath');
                }
                                
                $parser->xmlproduct = $this->request->post('xmlproduct');
                $parser->xmlproduct_attr = $this->request->post('xmlproduct_attr');
                
                $parser->xmlart = $this->request->post('xmlart');
                $parser->xmlart_attr = $this->request->post('xmlart_attr');
                if($parser->xmlart=='xpath'){
                    $parser->xmlart_attr = $this->request->post('xmlart_xpath');
                }
                
                $parser->xmlprice = $this->request->post('xmlprice');
                $parser->xmlprice_attr = $this->request->post('xmlprice_attr');
                if($parser->xmlprice=='xpath'){
                    $parser->xmlprice_attr = $this->request->post('xmlprice_xpath');
                }
                
                $parser->xmlstock_price = 		$this->request->post('xmlstock_price');
                $parser->xmlstock_price_attr = 		$this->request->post('xmlstock_price_attr');
                if($parser->xmlstock_price=='xpath'){
                    $parser->xmlprice_attr = $this->request->post('xmlstock_price_xpath');
                }
                
                $parser->xmlold_price = 		$this->request->post('xmlold_price');
                $parser->xmlold_price_attr = 		$this->request->post('xmlold_price_attr');
                if($parser->xmlold_price=='xpath'){
                    $parser->xmlold_price_attr = $this->request->post('xmlold_price_xpath');
                }

                $parser->xmlstock = $this->request->post('xmlstock');
                $parser->xmlstock_attr = $this->request->post('xmlstock_attr');
                if($parser->xmlstock=='xpath'){
                    $parser->xmlstock_attr = $this->request->post('xmlstock_xpath');
                }
                $parser->xmlwaiting = $this->request->post('xmlwaiting');
                $parser->xmlwaiting_attr = $this->request->post('xmlwaiting_attr');
                if($parser->xmlwaiting=='xpath'){
                    $parser->xmlwaiting_attr = $this->request->post('xmlwaiting_xpath');
                }
                $parser->xmlcurrency = $this->request->post('xmlcurrency');
                $parser->xmlcurrency_attr = $this->request->post('xmlcurrency_attr');
                $parser->xmlvnalname = $this->request->post('xmlvnalname');
                $parser->xmlojiname = $this->request->post('xmlojiname');
                $parser->xmlnenalname = $this->request->post('xmlnenalname');
                
                //Features
                $parser->xmlfeatures = serialize($this->request->post('xmlfeatures'));
            }
            
			if(empty($parser->id))
			{
				if (empty($parser->name)) $parser->name = "Не указан";
  				$parser->id = $this->parser_xml->add_parser($parser);
				$this->design->assign('message_success', 'added');
  			}
    		else
    		{
    			$this->parser_xml->update_parser($parser->id, $parser);
				$this->design->assign('message_success', 'updated');
    		}
            
            if($parser->xmlurl && !empty($_POST['get_tags'])){
                $string = file_get_contents($parser->xmlurl);
                $xml = simplexml_load_string($string); 
                if($xml){
                    $tags = array_unique(array_map(function ($e) { return $e->getName(); }, $xml->xpath('//*')));
                    $parser->tags = serialize($tags);
                
                    $tags_w_attr = array_unique(array_map(function ($e) { return $e->getName(); }, $xml->xpath('//*[@*]')));
                    $parser->tags_w_attr = serialize($tags_w_attr);
        
                    $attributes = array_unique(array_map(function ($e) { return $e->getName(); }, $xml->xpath('//@*')));
                    $parser->attributes = serialize($attributes);
                    $this->parser_xml->update_parser($parser->id, $parser);
                }
            }	

	 		$parser = $this->parser_xml->get_parser($parser->id); 	
		}
		else
		{
			if ($pid = $this->request->get('id')) {
				$parser = $this->parser_xml->get_parser($pid);
			}
		}   
        
        $this->design->assign('categories', $this->categories->get_categories_tree());
        $this->design->assign('brands', $this->brands->get_brands());
		$this->design->assign('vendors', $this->parsers->get_parsers());
        $this->design->assign('features', $this->features->get_features());
        
        $this->design->assign('tags', unserialize($parser->tags));
        $this->design->assign('tags_w_attr', unserialize($parser->tags_w_attr));
        $this->design->assign('attributes', unserialize($parser->attributes));
        
 		$this->design->assign('parser', $parser);
 
		return  $this->design->fetch('parser_xml.tpl');
	}
}