<?PHP

require_once('api/Simpla.php');

class CSlidesAdmin extends Simpla
{
	function fetch()
	{	
	
		if($this->request->method('post'))
		{  	
			// �������� � ����������
			$ids = $this->request->post('check');
			if(is_array($ids))
			switch($this->request->post('action'))
			{
			    case 'delete':
			    {
			    	foreach($ids as $id)
			    	{
			    		$this->cslides->delete_cslide($id); 
					}
			        break;
			    }
			}		
	  	
			// ����������
			$positions = $this->request->post('positions');
	 		$ids = array_keys($positions);
			sort($positions);
			foreach($positions as $i=>$position)
				$this->cslides->update_cslide($ids[$i], array('position'=>$position)); 

		} 

		$filter = array();
		$filter["brand"] = "ALL";
		$cslides = $this->cslides->get_cslides($filter);
		$this->design->assign('cslides', $cslides);
		return $this->body = $this->design->fetch('cslides.tpl');
	}
}
