<?PHP 

require_once('api/Simpla.php');

########################################
class StickersAdmin extends Simpla
{

  public function fetch()
  {
  
  	// Обработка действий
  	if($this->request->method('post'))
  	{
		// Действия с выбранными
		$ids = $this->request->post('check');
		if(is_array($ids))
		switch($this->request->post('action'))
		{
		    case 'disable':
		    {
		    	foreach($ids as $id)
					$this->stickers->update_sticker($id, array('visible'=>0));
				break;
		    }
		    case 'enable':
		    {
				foreach($ids as $id)
					$this->stickers->update_sticker($id, array('visible'=>1));
		        break;
		    }
		    case 'delete':
		    {
			    foreach($ids as $id)
					$this->stickers->delete_sticker($id);    
		        break;
		    }
		}		
		
 	}

  

	// Отображение
  	$stickers = $this->stickers->get_stickers();

 	$this->design->assign('stickers', $stickers);
	return $this->design->fetch('stickers.tpl');
  }
}


?>