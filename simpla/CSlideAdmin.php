<?php

require_once('api/Simpla.php');

class CSlideAdmin extends Simpla
{
  private $allowed_image_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');

  function fetch()
  {
		if($this->request->method('post'))
		{
			$slide->id = $this->request->post('id', 'integer');
			$slide->name = $this->request->post('name');
			$slide->cat = $this->request->post('cat');
			$slide->brand = $this->request->post('brand');
			$slide->description = $this->request->post('description');
			$slide->url = $this->request->post('url');
			$slide->pos = $this->request->post('pos');

				if(empty($slide->id))
				{
	  				$slide->id = $this->cslides->add_cslide($slide);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->cslides->update_cslide($slide->id, $slide);
					$this->design->assign('message_success', 'updated');
  	    		}	
  	    		// �������� �����������
  	    		if($this->request->post('delete_image'))
  	    		{
  	    			$this->cslides->delete_image($slide->id);
  	    		}
  	    		// �������� �����������
				$image = preg_replace("/\s+/", '_', $this->request->files('image'));

  	    		if(!empty($image['name']) && in_array(strtolower(pathinfo($image['name'], PATHINFO_EXTENSION)), $this->allowed_image_extentions))
  	    		{
  	    			$this->cslides->delete_image($slide->id);   	    			
  	    			move_uploaded_file($image['tmp_name'], $this->root_dir.$this->config->slides_images_dir.$image['name']);
  	    			$this->cslides->update_cslide($slide->id, array('image'=>$this->config->slides_images_dir.$image['name']));
  	    		}
	  			$slide = $this->cslides->get_cslide($slide->id);
			
		}
		else
		{
			$slide->id = $this->request->get('id', 'integer');
			$slide = $this->cslides->get_cslide($slide->id);
		}
		
		// ��� ������ � ���������
		$brands = $this->brands->get_brands();
		$this->design->assign('brands', $brands);
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		
		
 		$this->design->assign('slide', $slide);
		return  $this->design->fetch('cslide.tpl');
	}
}