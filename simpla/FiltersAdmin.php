<?PHP
//ini_set('display_errors',1);
require_once('api/Simpla.php');

############################################
# Class Properties displays a list of product parameters
############################################
class FiltersAdmin extends Simpla
{
	function fetch()
	{	
	
		if($this->request->method('post'))
		{  	
			// Действия с выбранными
			$ids = $this->request->post('check');
			if(is_array($ids))
			switch($this->request->post('action'))
			{
			    case 'set_in_filter':
			    {
			    	$this->features->update_feature($ids, array('in_filter'=>1));    
					break;
			    }
			    case 'unset_in_filter':
			    {
			    	$this->features->update_feature($ids, array('in_filter'=>0));    
					break;
			    }
			    case 'delete':
			    {

			    	foreach($ids as $id)
			    	{
			    		
		    			$this->filters->delete_filter($id); 
					}
			        break;
			    }
                case 'in_yandex':
			    {
			    	$this->features->update_feature($ids, array('yandex'=>1));    
					break;
			    }
                case 'out_yandex':
			    {
			    	$this->features->update_feature($ids, array('yandex'=>0));    
					break;
			    }
			}		
	  	
			// Сортировка
			$positions = $this->request->post('positions');
	 		$ids = array_keys($positions);
			sort($positions);
			foreach($positions as $i=>$position)
				$this->features->update_feature($ids[$i], array('position'=>$position)); 

		} 
	
		$categories = $this->categories->get_categories_tree();
		$category = null;
		
		$filter = array();
		$category_id = $this->request->get('category_id', 'integer');
		if($category_id)
		{
			$category = $this->categories->get_category($category_id);
			$filter['category_id'] = $category->id;
		}
		
		$filters = $this->filters->get_filters();
		
		//$this->design->assign('categories', $categories);
		//$this->design->assign('category', $category);
		$this->design->assign('filters', $filters);
		return $this->body = $this->design->fetch('filters.tpl');
	}
}
