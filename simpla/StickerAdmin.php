<?PHP
require_once('api/Simpla.php');

class StickerAdmin extends Simpla
{	
	public function fetch()
	{	
		if($this->request->method('POST'))
		{
			$sticker->id = $this->request->post('id', 'integer');
			$sticker->name = $this->request->post('name');
			$sticker->text = $this->request->post('text');
			$sticker->brand_id = $this->request->post('brand_id');
			$sticker->visible = $this->request->post('visible');
			$sticker->type = $this->request->post('type');
	
			if(empty($sticker->id))
			{
	  			$sticker->id = $this->stickers->add_sticker($sticker);
	  			$sticker = $this->stickers->get_sticker($sticker->id);
	  			$this->design->assign('message_success', 'added');
  	    	}
  	    	else
  	    	{
  	    		$this->stickers->update_sticker($sticker->id, $sticker);
	  			$sticker = $this->stickers->get_sticker($sticker->id);
	  			$this->design->assign('message_success', 'updated');
   	    	}		
		}
		else
		{
			$id = $this->request->get('id', 'integer');
			if(!empty($id))
				$sticker = $this->stickers->get_sticker($id);
			else
			{
				$sticker->visible = 1;
			}
		}	

		$this->design->assign('sticker', $sticker);
		$this->design->assign('brands', $this->brands->get_brands());
		
 	  	return $this->design->fetch('sticker.tpl');
	}
	
}

