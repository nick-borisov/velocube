<?PHP

require_once('api/Simpla.php');

class BrandsAdmin extends Simpla
{
	function fetch()
	{

		// Обработка действий 	
		if($this->request->method('post'))
		{

			// Действия с выбранными
			$ids = $this->request->post('check');

			if(is_array($ids))
			switch($this->request->post('action'))
			{
				case 'delete':
				{
					foreach($ids as $id)
						$this->brands->delete_brand($id);    
		        break;
				}
			}
			
			
			// Сортировка
			$positions = $this->request->post('positions');
			$ids = array_keys($positions);
			
			sort($positions);
			
			foreach($positions as $i=>$position)
				$this->brands->update_brand($ids[$i], array('position'=>$position));
		}	
        
        // Текущая категория
		$category_id = $this->request->get('category_id', 'integer'); 
		if($category_id && $category = $this->categories->get_category($category_id))
	  		$filter['category_id'] = $category->children;
        
        
		//
        if($sort = $this->request->get('sort', 'string'))
            $_SESSION['sort_brand_admin'] = $sort;		
		if (!empty($_SESSION['sort_brand_admin']))
            $filter['sort'] = $_SESSION['sort_brand_admin'];			
		else
            $filter['sort'] = 'position';
          			
		$this->design->assign('sort', $filter['sort']);	
		if ($filter['sort'] == 'byname')
		{
            $filter["order"] = 1;
			//$brands = $this->brands->get_brands(array());
		}
		
		$brands = $this->brands->get_brands($filter);
        
        
        $this->design->assign('categories', $this->categories->get_categories_tree());
        $this->design->assign('category', $category);
		$this->design->assign('brands', $brands);
		return $this->body = $this->design->fetch('brands.tpl');
	}
}

