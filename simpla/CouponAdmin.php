<?PHP
//ini_set('display_errors',1);
require_once('api/Simpla.php');

class CouponAdmin extends Simpla
{
	public function fetch()
	{
        $related_products = array();
        $related_products_stop = array();
        $coupon_categories = array();
        $coupon_brands = array();
        $coupon = new stdClass();
		if($this->request->method('post'))
		{
			$coupon->id = $this->request->post('id', 'integer');
            
			$coupon->code = $this->request->post('code', 'string');
            $coupon->annotation = $this->request->post('annotation', 'string');
            $coupon->name = $this->request->post('name', 'string');
			if($this->request->post('expires'))
				$coupon->expire = date('Y-m-d', strtotime($this->request->post('expire')));
			else
				$coupon->expire = null;
            
            if($coupon->active_margin = $this->request->post('active_margin'))
                $coupon->margin = serialize($this->request->post('marja'));    
                
			$coupon->value = $this->request->post('value', 'float');			
			$coupon->type = $this->request->post('type', 'string');
			$coupon->min_order_price = $this->request->post('min_order_price', 'float');
			$coupon->single = $this->request->post('single', 'float');
            $coupon->auto_discount = $this->request->post('auto_discount', 'integer');
            $coupon->ad_active = $this->request->post('ad_active', 'integer');
            $coupon->active = $this->request->post('active', 'integer');
            
            if($coupon->auto_discount || !$coupon->code)
                $coupon->code = '--AUTO_COUPON_'.$coupon->id;
            
            // Категории купона
			$coupon_categories = $this->request->post('categories');
            $coupon_subcat = $this->request->post('subcat');
            
			if(is_array($coupon_categories))
			{
				foreach($coupon_categories as $c){
					$pc[]->id = $c;
                }
				$coupon_categories = $pc;
			}
            
            $coupon_brands = $this->request->post('brands');
			if(is_array($coupon_brands))
			{
				foreach($coupon_brands as $b)
					$bc[]->id = $b;
				$coupon_brands = $bc;
			}
            
            // Связанные товары
			if(is_array($this->request->post('related_products')))
			{
				foreach($this->request->post('related_products') as $p)
				{
					$rp[$p]->coupon_id = $coupon->id;
					$rp[$p]->product_id = $p;
				}
				$related_products = $rp;
			}
            
            // Исключенные товары
			if(is_array($this->request->post('related_products_stop')))
			{
				foreach($this->request->post('related_products_stop') as $ps)
				{
					$rps[$ps]->coupon_id = $coupon->id;
					$rps[$ps]->product_id = $ps;
				}
				$related_products_stop = $rps;
			}
            
            //Coupon timesets
            if($this->request->post('timeset'))
			foreach($this->request->post('timeset') as $n=>$va) {
				foreach($va as $i=>$v){
				    
					$timesets[$i]->$n = $v;
                    $timesets[$i]->coupon_id = $coupon->id;
                }
            }
            if($this->request->post('timeset_yandex'))
			foreach($this->request->post('timeset_yandex') as $n=>$va) {
				foreach($va as $i=>$v){
				    
					$timesets_yandex[$i]->$n = $v;
                    $timesets_yandex[$i]->coupon_id = $coupon->id;
                    $timesets_yandex[$i]->yandex = 1;
                }
            }
            if($this->request->post('timeset_priceru'))
			foreach($this->request->post('timeset_priceru') as $n=>$va) {
				foreach($va as $i=>$v){
				    
					$timesets_priceru[$i]->$n = $v;
                    $timesets_priceru[$i]->coupon_id = $coupon->id;
                    $timesets_priceru[$i]->priceru = 1;
                }
            }
            
            //Add range days to DB
            $range_days = array();
            $flag_add_day = 0;
            foreach($timesets as $i=>$ts){
                foreach([MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY] as $day){
                    if($ts->from_day == $day)
                        $flag_add_day = 1;
                    
                    if($flag_add_day){
                        $range_days[$i][] = $day;
                    }
                    
                    if($ts->to_day == $day)
                        $flag_add_day = 0;
                }
                $timesets[$i]->week_days = implode(',', $range_days[$i]);
            }
            
            $range_days = array();
            $flag_add_day = 0;
            foreach($timesets_yandex as $i=>$ts){
                foreach([MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY] as $day){
                    if($ts->from_day == $day)
                        $flag_add_day = 1;
                    
                    if($flag_add_day){
                        $range_days[$i][] = $day;
                    }
                    
                    if($ts->to_day == $day)
                        $flag_add_day = 0;
                }
                $timesets_yandex[$i]->week_days = implode(',', $range_days[$i]);
            }
            
            $range_days = array();
            $flag_add_day = 0;
            foreach($timesets_priceru as $i=>$ts){
                foreach([MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY] as $day){
                    if($ts->from_day == $day)
                        $flag_add_day = 1;
                    
                    if($flag_add_day){
                        $range_days[$i][] = $day;
                    }
                    
                    if($ts->to_day == $day)
                        $flag_add_day = 0;
                }
                $timesets_priceru[$i]->week_days = implode(',', $range_days[$i]);
            }

 			// Не допустить одинаковые URL разделов.
			if(($a = $this->coupons->get_coupon((string)$coupon->code)) && $a->id!=$coupon->id)
			{	
				$this->design->assign('message_error', 'code_exists');
			}
			else
			{
				if(empty($coupon->id))
				{ 
	  				$coupon->id = $this->coupons->add_coupon($coupon);
	  				$coupon = $this->coupons->get_coupon($coupon->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
                    // Категории купона
	   	    		$query = $this->db->placehold('DELETE FROM __categories_coupons WHERE coupon_id=?', $coupon->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($coupon_categories))
		  		    {
		  		    	foreach($coupon_categories as $i=>$category){
                            if(in_array($category->id, $coupon_subcat)){
                                $category = $this->categories->get_category(intval($category->id));
                                foreach($category->children as $cid){
                                    //var_dump($category->name.' children '. $cid);print("\n");
                                    $this->coupons->add_coupon_category($coupon->id, $cid);
                                }
                            }else{
                                //var_dump('no sub'.$category->id);print("\n");
                                $this->coupons->add_coupon_category($coupon->id, $category->id);
                                
                            }
                        }
	  	    		}
                    
                    // Бренды купона
	   	    		$query = $this->db->placehold('DELETE FROM __brands_coupons WHERE coupon_id=?', $coupon->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($coupon_brands))
		  		    {
		  		    	foreach($coupon_brands as $i=>$brand)
                            if($brand)
	   	    				   $this->coupons->add_coupon_brand($coupon->id, $brand->id);
	  	    		}
                    
                    // Связанные товары
	   	    		$query = $this->db->placehold('DELETE FROM __related_products_coupons WHERE coupon_id=?', $coupon->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($related_products))
		  		    {
		  		    	$pos = 0;
		  		    	foreach($related_products  as $i=>$related_product)
	   	    				$this->coupons->add_related_product($coupon->id, $related_product->product_id);
	  	    		}
                    
                    // Исключенные товары
	   	    		$query = $this->db->placehold('DELETE FROM __related_products_coupons_stop WHERE coupon_id=?', $coupon->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($related_products_stop))
		  		    {
		  		    	$pos = 0;
		  		    	foreach($related_products_stop as $i=>$related_product)
	   	    				$this->coupons->add_related_product_stop($coupon->id, $related_product->product_id);
	  	    		}
                    
                    $this->coupons->delete_coupon_timeset($coupon->id);
                    if(is_array($timesets))
		  		    {
                        foreach($timesets as $timeset)
                            if($timeset)
	   	    				   $this->coupons->add_coupon_timeset($timeset);
                    }
                    if(is_array($timesets_yandex))
		  		    {
                        foreach($timesets_yandex as $timeset)
                            if($timeset)
	   	    				   $this->coupons->add_coupon_timeset($timeset);
                    }
                    if(is_array($timesets_priceru))
		  		    {
                        foreach($timesets_priceru as $timeset)
                            if($timeset)
	   	    				   $this->coupons->add_coupon_timeset($timeset);
                    }
                    
  	    			$this->coupons->update_coupon($coupon->id, $coupon);
  	    			$coupon = $this->coupons->get_coupon($coupon->id);
					$this->design->assign('message_success', 'updated');
  	    		}	
                //$this->coupons->update_coupon_categories($coupon->id, $coupon_categories);
   	    		
			}
            
		}
		else
		{
			$coupon->id = $this->request->get('id', 'integer');
			$coupon = $this->coupons->get_coupon($coupon->id);
		}

//		if(empty($coupon->id))
//			$coupon->expire = date($this->settings->date_format, time());
        $coupon_categories = array();
        $coupon_brands = array();		
		if($coupon)
		{	
			$coupon_categories = $this->coupons->get_coupon_categories($coupon->id);
            $coupon_brands = $this->coupons->get_coupon_brands($coupon->id);
            // Связанные товары
			$related_products = $this->coupons->get_related_products(array('product_id'=>$coupon->id));
            $related_products_stop = $this->coupons->get_related_products_stop(array('product_id'=>$coupon->id));
            $timesets = $this->coupons->get_coupon_timeset($coupon->id);
            $timesets_yandex = $this->coupons->get_coupon_timeset($coupon->id, 1);
            $timesets_priceru = $this->coupons->get_coupon_timeset($coupon->id, 0, 1);
//var_dump($timesets);
		}
		//var_dump($coupon_brands);
        if(empty($coupon_categories))
		{
			if($category_id = $this->request->get('category_id'))
				$coupon_categories[0]->id = $category_id;		
			else
				$coupon_categories = array(1);
		}
        if(empty($coupon_brands))
		{
				$coupon_brands = array(1);
		}
        
        if(!empty($related_products))
		{
			foreach($related_products as &$r_p)
				$r_products[$r_p->product_id] = &$r_p;
			$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
			foreach($temp_products as $temp_product)
				$r_products[$temp_product->id] = $temp_product;
		
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
			foreach($related_products_images as $image)
			{
				$r_products[$image->product_id]->images[] = $image;
			}
		}
        
        if(!empty($related_products_stop))
		{
			foreach($related_products_stop as &$r_p)
				$r_products[$r_p->product_id] = &$r_p;
			$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
			foreach($temp_products as $temp_product)
				$r_products[$temp_product->id] = $temp_product;
            $related_products_images = array();
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
			foreach($related_products_images as $image)
			{
				$r_products[$image->product_id]->images[] = $image;
			}
		}
        //var_dump($coupon->code);
        $orders = $this->orders->get_orders(array('coupon_code'=>$coupon->code));
        if($coupon->code)
            $this->design->assign('orders', $orders);
        
		$categories = $this->categories->get_categories_tree();
        
        $categories_4_brands2 = $categories_4_brands = $categories_4_brands3 = array();
        $this->db->query("SELECT id FROM s_categories WHERE parent_id in (?@)", $coupon_categories);
        if($res = $this->db->results('id'))
            $categories_4_brands = $res;

        $this->db->query("SELECT id FROM s_categories WHERE parent_id in (?@)", $categories_4_brands);
        if($res2 = $this->db->results('id'))
            $categories_4_brands2 = $res2;


        $this->db->query("SELECT id FROM s_categories WHERE parent_id in (?@)", $categories_4_brands2);
        if($res3 = $this->db->results('id'))
            $categories_4_brands3 = $res3;
        
        $all_categories_ids = array_merge($categories_4_brands, $categories_4_brands2, $categories_4_brands3);    
            
        $brands = $this->brands->get_brands(array('category_id'=>$all_categories_ids, 'order'=>1));

		$this->design->assign('categories', $categories);
        $this->design->assign('brands', $brands);
        if(count($related_products))
            $this->design->assign('related_products', $related_products); 
        if(count($related_products_stop))
            $this->design->assign('related_products_stop', $related_products_stop);       
//print_r($related_products_stop);        
		$this->design->assign('coupon_categories', $coupon_categories);
        $this->design->assign('coupon_brands', $coupon_brands);
        $this->design->assign('timesets', $timesets);
        $this->design->assign('timesets_yandex', $timesets_yandex);
        $this->design->assign('timesets_priceru', $timesets_priceru);
        
		$this->design->assign('coupon', $coupon);
 	  	return $this->design->fetch('coupon.tpl');
	}
    
    public function cat_tree($categories){
        foreach($categories AS $k=>$v) {
            if(isset($v->subcategories)) $this->cat_tree($v->subcategories);
            $brands[$v->id] = $this->brands->get_brands(array('category_id'=>$v->children));
        }
        return $brands;
    }
}