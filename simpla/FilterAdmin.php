<?PHP
require_once('api/Simpla.php');
//ini_set('display_errors',1);
class FilterAdmin extends Simpla
{

	function fetch()
	{
		if($this->request->method('post'))
		{
            $filter = new StdClass();
			$filter->id = $this->request->post('id', 'integer');
			$filter->name = $this->request->post('name');
			$filter->visible = intval($this->request->post('visible'));
            $expand = $this->request->post('expand');
			$filter_features = $this->request->post('filter_features');
            foreach($filter_features as $ff){
                foreach($ff as $k=>$id){
                    $ffeatures[$k] = new StdClass();
                    $ffeatures[$k]->filter_id = $filter->id;
                    $ffeatures[$k]->feature_id = $id; 
                    $ffeatures[$k]->position = $k;
                    foreach($expand as $j=>$e){
                        if($j==$id)
                            $ffeatures[$k]->expand = $e;
                    }
                }
            }

			if(empty($filter->id))
			{
  				$filter->id = $this->filters->add_filter($filter);
  				$filter = $this->filters->get_filter($filter->id);
				$this->design->assign('message_success', 'added');
                $this->filters->add_filter_features($filter->id, $filter_features);
  			}
			else
			{
				$this->filters->update_filter($filter->id, $filter);
				$filter = $this->filters->get_filter(intval($filter->id));
				$this->design->assign('message_success', 'updated');
			}
			$this->filters->update_filter_features($filter->id, $ffeatures);
		}
		else
		{
            if($id = $this->request->get('id', 'integer')){
                $filter = $this->filters->get_filter($id);
            }
		}
        
        $temp_features = $this->features->get_features();
        foreach($temp_features as $tf){
            $features[$tf->id] = $tf;
        }

		$filter_features = array();	
		if($filter)
		{	
			$filter_features = $this->filters->get_filter_features($filter->id);
		}
//print_r($filter_features);
		$this->design->assign('filter', $filter);
		$this->design->assign('features', $features);
		$this->design->assign('filter_features', $filter_features);
		return $this->body = $this->design->fetch('filter.tpl');
	}
}