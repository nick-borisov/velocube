<?PHP

require_once('api/Simpla.php');

############################################
# Class Properties displays a list of product parameters
############################################
class MarksAdmin extends Simpla
{
	function fetch()
	{	
	    // Категории
		$categories = $this->categories->get_categories_tree();
		$this->design->assign('categories', $categories);
		
		// Текущая категория
		$category_id = $this->request->get('category_id', 'integer'); 
		if($category_id && $category = $this->categories->get_category($category_id))
	  		$filter['category_id'][0] = $category->id;
	
	
		if($this->request->method('post'))
		{  	
			// Действия с выбранными
			$ids = $this->request->post('check');
			if(is_array($ids))
			switch($this->request->post('action'))
			{
			    case 'disable':
			    {
			    	foreach($ids as $id)
						$this->marks->update_mark($id, array('visible'=>0));    
					break;
			    }
			    case 'enable':
			    {
			    	foreach($ids as $id)
						$this->marks->update_mark($id, array('visible'=>1));    
			        break;
			    }
			    case 'delete':
			    {
			    			$this->marks->delete_mark($ids); 
			        break;
			    }
			}		
	  	
			// Сортировка
			$positions = $this->request->post('positions');
	 		$ids = array_keys($positions);
			sort($positions);
			foreach($positions as $i=>$position)
				$this->marks->update_mark($ids[$i], array('position'=>$position)); 

		} 
	
		
		
		if(isset($category))
			$this->design->assign('category', $category);
		
		
		$marks = $this->marks->get_marks($filter);
		

		$this->design->assign('marks', $marks);
		return $this->body = $this->design->fetch('marks.tpl');
	}
}
