<?PHP
require_once('api/Simpla.php');

class SettingsAdmin extends Simpla
{	
	private $allowed_image_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');
	
	public function fetch()
	{	
		$this->passwd_file = $this->config->root_dir.'/simpla/.passwd';
		$this->htaccess_file = $this->config->root_dir.'/simpla/.htaccess';
	
		$managers = $this->managers->get_managers();
		$this->design->assign('managers', $managers);
		
		if($this->request->method('POST'))
		{
			$this->settings->site_name = $this->request->post('site_name');
			$this->settings->company_name = $this->request->post('company_name');
			$this->settings->date_format = $this->request->post('date_format');
			$this->settings->admin_email = $this->request->post('admin_email');
			$this->settings->limit_comments = $this->request->post('limit_comments');
			
            /*callback*/			
            $this->settings->callback_email = $this->request->post('callback_email');
            
            if($this->request->post('callback_mask'))
                $this->settings->callback_mask = $this->request->post('callback_mask');
            else
                $this->settings->callback_mask = 0;
            if($this->request->post('callback_message'))
                $this->settings->callback_message = $this->request->post('callback_message');
            else
                $this->settings->callback_message = 0;
            if($this->request->post('callback_calltime'))   
                $this->settings->callback_calltime = $this->request->post('callback_calltime');
            else
                $this->settings->callback_calltime = 0;
            
            $this->settings->calltime_from = $this->request->post('calltime_from');
            $this->settings->calltime_to = $this->request->post('calltime_to');
            /*callback end*/     
            
			$this->settings->order_email = $this->request->post('order_email');
			$this->settings->comment_email = $this->request->post('comment_email');
			$this->settings->notify_from_email = $this->request->post('notify_from_email');
			
			$this->settings->decimals_point = $this->request->post('decimals_point');
			$this->settings->thousands_separator = $this->request->post('thousands_separator');
			
			$this->settings->products_num = $this->request->post('products_num');
			$this->settings->products_num_admin = $this->request->post('products_num_admin');
			$this->settings->max_order_amount = $this->request->post('max_order_amount');	
			$this->settings->units = $this->request->post('units');
            if($this->request->post('view_in_stock'))
                $this->settings->view_in_stock = $this->request->post('view_in_stock');
            else $this->settings->view_in_stock = 0;
            
            $this->settings->order_sklad = $this->request->post('order_sklad', 'boolean');
            $this->settings->order_hits = $this->request->post('order_hits', 'boolean');
            
            $this->settings->interval_min = $this->request->post('interval_min', 'integer');
            $this->settings->interval_max = $this->request->post('interval_max', 'integer');
            $this->settings->interval_price = $this->request->post('interval_price', 'integer');
            $this->settings->interval_tom_price = $this->request->post('interval_tom_price', 'integer');
            $this->settings->interval_delivery_mult = $this->request->post('interval_delivery_mult', 'integer');
            $this->settings->yandex_delivery_cost = $this->request->post('yandex_delivery_cost', 'integer');
            $this->settings->calendar_days = $this->request->post('calendar_days');
            if(substr($this->settings->calendar_days,-1)==','){
                $this->settings->calendar_days = substr($this->settings->calendar_days,0,-1);
                print($this->settings->calendar_days);
            }
            //Yandex ext
            if($this->request->post('yandex_vnalichii'))
                $this->settings->yandex_vnalichii = $this->request->post('yandex_vnalichii');
            else $this->settings->yandex_vnalichii = 0;
            if($this->request->post('yandex_store'))
                $this->settings->yandex_store = $this->request->post('yandex_store');
            else $this->settings->yandex_store = 0;
            if($this->request->post('yandex_pickup'))
                $this->settings->yandex_pickup = $this->request->post('yandex_pickup');
            else $this->settings->yandex_pickup = 0;
            if($this->request->post('yandex_full_description'))
                $this->settings->yandex_full_description = $this->request->post('yandex_full_description');
            else $this->settings->yandex_full_description = 0;
            
            if($this->request->post('yandex_oldprice'))
                $this->settings->yandex_oldprice = $this->request->post('yandex_oldprice');
            else $this->settings->yandex_oldprice = 0;
            
 
      if($this->request->post('category_count')==1)
      $this->settings->category_count = 1;
      else
      $this->settings->category_count = 0;
      
      $this->settings->sms_api = $this->request->post('sms_api');
      $this->settings->sms_phone = $this->request->post('sms_phone');
      //print_r($this->settings->sms_phone);
      if($this->request->post('sms_order_admin')==1)
      $this->settings->sms_order_admin = 1;
      else
      $this->settings->sms_order_admin = 0;
      if($this->request->post('sms_order_user')==1)
      $this->settings->sms_order_user = 1;
      else
      $this->settings->sms_order_user = 0;
      if($this->request->post('sms_convert')==1)
      $this->settings->sms_convert = 1;
      else
      $this->settings->sms_convert = 0;
      
      $this->settings->sum_1 = $this->request->post('sum_1');
      $this->settings->proc_1 = $this->request->post('proc_1');
      $this->settings->sum_2 = $this->request->post('sum_2');
      $this->settings->proc_2 = $this->request->post('proc_2');
      $this->settings->sum_3 = $this->request->post('sum_3');
      $this->settings->proc_3 = $this->request->post('proc_3');                   	
			
			// Простые звонки
			$this->settings->pz_server = $this->request->post('pz_server');
			$this->settings->pz_password = $this->request->post('pz_password');
			$this->settings->pz_phones = $this->request->post('pz_phones');
			
			
			// Водяной знак
			$clear_image_cache = false;
			$watermark = $this->request->files('watermark_file', 'tmp_name');
			if(!empty($watermark) && in_array(pathinfo($this->request->files('watermark_file', 'name'), PATHINFO_EXTENSION), $this->allowed_image_extentions))
			{
				if(@move_uploaded_file($watermark, $this->config->root_dir.$this->config->watermark_file))
					$clear_image_cache = true;
				else
					$this->design->assign('message_error', 'watermark_is_not_writable');
			}
			
			if($this->settings->watermark_offset_x != $this->request->post('watermark_offset_x'))
			{
				$this->settings->watermark_offset_x = $this->request->post('watermark_offset_x');
				$clear_image_cache = true;
			}
			if($this->settings->watermark_offset_y != $this->request->post('watermark_offset_y'))
			{
				$this->settings->watermark_offset_y = $this->request->post('watermark_offset_y');
				$clear_image_cache = true;
			}
			if($this->settings->watermark_transparency != $this->request->post('watermark_transparency'))
			{
				$this->settings->watermark_transparency = $this->request->post('watermark_transparency');
				$clear_image_cache = true;
			}
			if($this->settings->images_sharpen != $this->request->post('images_sharpen'))
			{
				$this->settings->images_sharpen = $this->request->post('images_sharpen');
				$clear_image_cache = true;
			}
			
			
			// Удаление заресайзеных изображений
			if($clear_image_cache)
			{
				$dir = $this->config->resized_images_dir;
				if($handle = opendir($dir))
				{
					while(false !== ($file = readdir($handle)))
					{
						if($file != "." && $file != "..")
						{
							@unlink($dir."/".$file);
						}
					}
					closedir($handle);
				}			
			}			
			$this->design->assign('message_success', 'saved');
		}
 	  	return $this->design->fetch('settings.tpl');
	}
	
}

