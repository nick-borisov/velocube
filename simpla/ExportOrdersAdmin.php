<?PHP
require_once('api/Simpla.php');

class ExportOrdersAdmin extends Simpla
{	
	private $export_files_dir = 'simpla/files/export_orders/';

	public function fetch()
	{
		$this->design->assign('export_files_dir', $this->export_files_dir);
		if(!is_writable($this->export_files_dir))
			$this->design->assign('message_error', 'no_permission');
			$this->design->assign('status', $this->request->get('status'));
  	  	return $this->design->fetch('export_orders.tpl');
	}
	
}

