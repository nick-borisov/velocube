<?php

require_once('api/Simpla.php');


############################################
# Class Category - Edit the good gategory
############################################
class CategoryAdmin extends Simpla
{
  private	$allowed_image_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');
  
  function fetch()
  {
		if($this->request->method('post'))
		{
            $category = new StdClass();
			$category->id = $this->request->post('id', 'integer');
			$category->parent_id = $this->request->post('parent_id', 'integer');
			$category->name = $this->request->post('name');
			$category->short_name = $this->request->post('short_name');
			$category->visible = $this->request->post('visible', 'boolean');
			$category->metka = $this->request->post('metka', 'boolean');
            
            $category->market_category = trim($this->request->post('market_category', 'string'));

			$category->url = $this->request->post('url', 'string');
			$category->meta_title = $this->request->post('meta_title');
			$category->meta_keywords = $this->request->post('meta_keywords');
			$category->meta_description = $this->request->post('meta_description');
			
			$category->description = $this->request->post('description');
			
			$category->skl['skl'] = $this->request->post('skl');
			$category->skl['skl_mn'] = $this->request->post('skl_mn');
			$category->skl['skl_ov'] = $this->request->post('skl_ov');
			$category->skl = serialize($category->skl);
            
            $category->meta_generation = $this->request->post('meta_generation', 'integer');
            $category->generate_description = $this->request->post('generate_description', 'integer');
            
            $category->delivery_price = $this->request->post('delivery_price', 'integer');
            
            $category->lowcost_limit = $this->request->post('lowcost_limit', 'integer');
            $category->highcost_limit = $this->request->post('highcost_limit', 'integer');
            
            $category->filter_id = $this->request->post('filter_id', 'integer');
            
            $category->hide_products = $this->request->post('hide_products', 'integer');
            $category->sale_category = $this->request->post('sale_category', 'integer');
            
			// Не допустить одинаковые URL разделов.
			if(($c = $this->categories->get_category($category->url)) && $c->id!=$category->id)
			{			
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{
				if(empty($category->id))
				{
	  				$category->id = $this->categories->add_category($category);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
                    if($this->request->post('pricetochild')){
                        foreach($c->children as $cid){
                            $cat_dp = new StdClass();
                            $cat_dp->delivery_price = $category->delivery_price;
                            $this->categories->update_category($cid, $cat_dp);
                        }
                        //print_r($c);
                    }
                    if($fid = $this->request->post('filter_id', 'integer')){
                        foreach($c->children as $cid){
                            if($cid != $category->id){
                                
                                $temp_cat = $this->categories->get_category(intval($cid));
                                
                                if(!$temp_cat->filter_id){
                                    $cat_lc = new StdClass();
                                    $cat_lc->filter_id = $fid;
                                    $this->categories->update_category($cid, $cat_lc);
                                } 
                            }else{
                                $cat_lc = new StdClass();
                                $cat_lc->filter_id = $fid;
                                $this->categories->update_category($category->id, $cat_lc);
                            }
                        }
                    }
                    
                    
                    //if($this->request->post('lowcost_limit') || $this->request->post('lowcost_category')){
                        foreach($c->children as $cid){
                            if($cid != $category->id){
                                
                                $temp_cat = $this->categories->get_category(intval($cid));
                                
                                if(!$temp_cat->lowcost_limit && !$temp_cat->lowcost_category){
                                    $cat_lc = new StdClass();
                                    $cat_lc->lowcost_limit = $this->request->post('lowcost_limit', 'integer');
                                    $cat_lc->lowcost_category = $this->request->post('lowcost_category', 'integer');
                                    $this->categories->update_category($cid, $cat_lc);
                                } 
                            }else{
                                $cat_lc = new StdClass();
                                $cat_lc->lowcost_limit = $this->request->post('lowcost_limit', 'integer');
                                $cat_lc->lowcost_category = $this->request->post('lowcost_category', 'integer');
                                $this->categories->update_category($category->id, $cat_lc);
                            }
                        }
                    //}
                    
                   // if($this->request->post('highcost_limit') && $this->request->post('highcost_category')){
                        foreach($c->children as $cid){
                            if($cid != $category->id){
                                
                                $temp_cat = $this->categories->get_category(intval($cid));
                                
                                if(!$temp_cat->highcost_limit && !$temp_cat->highcost_category){
                                    $cat_lc = new StdClass();
                                    $cat_lc->highcost_limit = $this->request->post('highcost_limit', 'integer');
                                    $cat_lc->highcost_category = $this->request->post('highcost_category', 'integer');
                                    $this->categories->update_category($cid, $cat_lc);
                                } 
                            }else{
                                $cat_lc = new StdClass();
                                $cat_lc->highcost_limit = $this->request->post('highcost_limit', 'integer');
                                $cat_lc->highcost_category = $this->request->post('highcost_category', 'integer');
                                $this->categories->update_category($category->id, $cat_lc);
                            }
                        }
                   // }
                    
  	    			$this->categories->update_category($category->id, $category);
					$this->design->assign('message_success', 'updated');
  	    		}
  	    		// Удаление изображения
  	    		if($this->request->post('delete_image'))
  	    		{
  	    			$this->categories->delete_image($category->id);
  	    		}
  	    		// Загрузка изображения
  	    		$image = $this->request->files('image');
  	    		if(!empty($image['name']) && in_array(strtolower(pathinfo($image['name'], PATHINFO_EXTENSION)), $this->allowed_image_extentions))
  	    		{
  	    			$this->categories->delete_image($category->id);
  	    			move_uploaded_file($image['tmp_name'], $this->root_dir.$this->config->categories_images_dir.$image['name']);
  	    			$this->categories->update_category($category->id, array('image'=>$image['name']));
  	    		}
  	    		$category = $this->categories->get_category(intval($category->id));
                $category->skl = unserialize($category->skl);
			}
		}
		else
		{
			$category->id = $this->request->get('id', 'integer');
			$category = $this->categories->get_category($category->id);
            $category->skl = unserialize($category->skl);
            //print_r($category);
		}
		
        $filters = $this->filters->get_filters();
        $this->design->assign('filters', $filters);

		$categories = $this->categories->get_categories_tree();
        
		$this->design->assign('category', $category);
		$this->design->assign('categories', $categories);
		return  $this->design->fetch('category.tpl');
	}
}