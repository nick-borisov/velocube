{* Вкладки *}
{capture name=tabs}
		{if in_array('comments', $manager->permissions)}<li><a href="index.php?module=CommentsAdmin">Комментарии
				{if $new_comments_counter}<span class="rounded_counter_tab">{$new_comments_counter}</span>{/if}
			</a></li>{/if}
		{if in_array('feedbacks', $manager->permissions)}<li><a href="index.php?module=FeedbacksAdmin">Обратная связь</a></li>{/if}
        <li><a href="index.php?module=YapAdmin">Отзывы с Я.Маркета</a></li>
		<li><a href="index.php?module=CallbacksAdmin">Заказ обратного звонка
				{if $new_callbacks_counter}<span class="rounded_counter_tab">{$new_callbacks_counter}</span>{/if}
			</a></li>
		<li class="active"><a href="index.php?module=CallbacksCheapAdmin">Нашли дешевле
				{if $new_callbackcheap_counter}<span class="rounded_counter_tab">{$new_callbackcheap_counter}</span>{/if}
			</a></li>
{/capture}

{* Title *}
{$meta_title='Нашли дешевле' scope=parent}

{* Заголовок *}
<div id="header">
	{if $callbacks_count}
	<h1>{$callbacks_count} {$callbacks_count|plural:'сообщение':'сообщения':'сообщений'}</h1> 
	{else}
	<h1>Нет сообщений</h1> 
	{/if}
</div>	

<div id="main_list">
	
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->

	{if $callbacks}
		<form id="list_form" method="post">
		<input type="hidden" name="session_id" value="{$smarty.session.id}">
		
			<div id="list" style="width:100%;">
				
				{foreach $callbacks as $callback}
				<div class="row{if $callback->processed} active{/if}">
			 		<div class="checkbox cell">
						<input type="checkbox" name="check[]" value="{$callback->id}" />				
					</div>
					<div class="name cell">
						<div class='comment_name'>
                            {$callback->name|escape}
						</div>
						<div class='comment_text'>
                            Телефон: {$callback->phone|escape|nl2br}
						</div>
						<div class='comment_text'>
                            Ссылка: <a href="{$callback->message}" target="_blank">{$callback->message}</a>
						</div>
                        <div class='comment_text'>
                            Товар: <a href="{$config->root_url}/products/{$callback->product->url}" target="_blank">{$callback->product->fullname}</a>
						</div>
						<div class='comment_info'>
                            Заявка отправлена {$callback->date|date} в {$callback->date|time}
						</div>
					</div>
					<div class="select_status">
                        <select name="processed">
                            <option value="0" {if !$callback->processed}selected{/if}>Новый</option>
                            <option value="1" {if $callback->processed == 1}selected{/if}>Обработан</option>
                        </select>
                    </div>
					<div class="icons cell">
						<a href='#' title='Обработать' class="processed"></a>
						<a href='#' title='Удалить' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
				{/foreach}
			</div>
		
			<div id="action">
			<label id='check_all' class='dash_link'>Выбрать все</label>
		
			<span id=select>
			<select name="action">
				<option value="processed">Отметить как обработанные</option>
				<option value="delete">Удалить</option>
			</select>
			</span>
		
			<input id='apply_action' class="button_green" type=submit value="Применить">
		
			
		</div>
		</form>
		
	{else}
	Нет сообщений
	{/if}
		
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->
			
</div>

<!-- Меню -->
<div id="right_menu">
	<ul>
		<li {if $processed==null}class="selected"{/if}><a href="{url keyword=null page=null processed=null}">Все</a></li>
		<li {if $processed=='0'}class="selected"{/if}><a href="{url keyword=null page=null processed='0'}">Новые</a></li>
		<li {if $processed==1}class="selected"{/if}><a href="{url keyword=null page=null processed=1}">Обработанные</a></li>	
	</ul>
</div>
<!-- Меню  (The End) -->

{literal}
<script>
$(function() {

	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();
	
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', $('#list input[type="checkbox"][name*="check"]:not(:checked)').length>0);
	});	

	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest(".row").find('input[type="checkbox"][name*="check"]').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').prop('selected', true);
		$(this).closest("form").submit();
	});
	
	// Обработать
	$("a.processed").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('active')?null:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'callbackcheap', 'id': id, 'values': {'processed': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('active');
				else
					line.removeClass('active');				
			},
			dataType: 'json'
		});	
		return false;	
	});

	// Обработать
	$("select[name='processed']").change(function() {
		var icon        = $(this);        
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = $(this).val();
		icon.parent().addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'callbackcheap', 'id': id, 'values': {'processed': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.parent().removeClass('loading_icon');
				if(state)
					line.addClass('active');
				else
					line.removeClass('active');				
			},
			dataType: 'json'
		});	
		return false;	
	});
	
	// Подтверждение удаления
	$("form#list_form").submit(function() {
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});

});

</script>
{/literal}
