{* Вкладки *}
{capture name=tabs}
		{if in_array('comments', $manager->permissions)}<li><a href="index.php?module=CommentsAdmin">Комментарии
				{if $new_comments_counter}<span class="rounded_counter_tab">{$new_comments_counter}</span>{/if}
			</a></li>{/if}
		{if in_array('feedbacks', $manager->permissions)}<li><a href="index.php?module=FeedbacksAdmin">Обратная связь</a></li>{/if}
        <li><a href="index.php?module=YapAdmin">Отзывы с Я.Маркета</a></li>
		<li class="active"><a href="index.php?module=CallbacksAdmin">Заказ обратного звонка 
				{if $new_callbacks_counter}<span class="rounded_counter_tab">{$new_callbacks_counter}</span>{/if}
			</a></li>
		<li><a href="index.php?module=CallbacksCheapAdmin">Нашли дешевле
				{if $new_callbackcheap_counter}<span class="rounded_counter_tab">{$new_callbackcheap_counter}</span>{/if}
			</a></li>
{/capture}

{* Title *}
{$meta_title='Заказ обратного звонка' scope=parent}
{* Поиск *}
<form method="get">
<div id="search">
	<input type="hidden" name="module" value="CallbacksAdmin">
	<input class="search" type="text" name="keyword" value="{$keyword|escape}" />
	<input class="search_button" type="submit" value=""/>
</div>
</form>

{* Заголовок *}
<div id="header">
	{if count($callbacks)>0}
	<h1>{count($callbacks)} {count($callbacks)|plural:'заказ':'заказов':'заказа'}</h1> 
	{else}
	<h1>Нет заказов</h1> 
	{/if}
</div>	

<div id="main_list">
	
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->

	{if $callbacks}
		<form id="list_form" method="post">
		<input type="hidden" name="session_id" value="{$smarty.session.id}">
		
			<div id="list" style="width:100%;">
				
				{foreach $callbacks as $callback}
				<div class="row{if $callback->processed} active{/if}">
			 		<div class="checkbox cell">
						<input type="checkbox" name="check[]" value="{$callback->id}" />				
					</div>
					<div class="name cell">
						<div class='comment_name'>
						{$callback->name|escape}
						</div>
						<div class='comment_text'>
						Телефон: +{$callback->phone|escape|nl2br}
						</div>
						<div class='comment_text'>
						Сообщение: {$callback->message|escape|nl2br}
						</div>
						<div class='comment_info'>
						Заявка отправлена {$callback->date|date} в {$callback->date|time} 
                        <br />
                        Cо страницы <a href="{$callback->url}">{$callback->url}</a>
						</div>
					</div>
					{if $callback->time_from}
					<div>
						Назначеное время {$callback->time_from}:00 - {$callback->time_to}:00
					</div>
					{/if}
                    <div class="select_status">
                        <select name="status">
                            <option value="1" {if $callback->status == 1}selected{/if}>Новый</option>
                            <option value="2" {if $callback->status == 2}selected{/if}>Обработан</option>
                            <option value="3" {if $callback->status == 3}selected{/if}>Нет ответа</option>
                        </select>
                    </div>
                    {if $callback->call_me_intime}
                    <div class="time_to_call">
                        Удобно позвонить с {$callback->time_from}:00 до {$callback->time_to}:00
                    </div>
                    {/if}
					<div class="icons cell">
						<a href='#' title='Удалить' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
				{/foreach}
			</div>
		
			<div id="action">
			<label id='check_all' class='dash_link'>Выбрать все</label>
		
			<span id=select>
			<select name="action">
				<option value="2">Отметить как обработанные</option>
                <option value="3">Отметить как нет ответа</option>
				<option value="4">Удалить</option>
			</select>
			</span>
		
			<input id='apply_action' class="button_green" type=submit value="Применить">
		
			
		</div>
		</form>
		
	{else}
	Нет сообщений
	{/if}
		
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->
			
</div>

<!-- Меню -->
<div id="right_menu">
	
	<!-- Фильтры -->
	<ul>
		<li {if !$status}class="selected"{/if}><a href="{url keyword=null page=null status=null}">Все</a></li>
		<li {if $status==1}class="selected"{/if}><a href="{url keyword=null page=null status=1}">Новые</a></li>
		<li {if $status==2}class="selected"{/if}><a href="{url keyword=null page=null status=2}">Обработанные</a></li>
        <li {if $status==3}class="selected"{/if}><a href="{url keyword=null page=null status=3}">Нет ответа</a></li>
	
	</ul>
	<!-- Фильтры -->
	
</div>
<!-- Меню  (The End) -->

{literal}
<script>
$(function() {

	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();
	
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', $('#list input[type="checkbox"][name*="check"]:not(:checked)').length>0);
	});	

	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest(".row").find('input[type="checkbox"][name*="check"]').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=4]').prop('selected', true);
		$(this).closest("form").submit();
	});
	
	// Обработать
	$("select[name='status']").change(function() {
		var icon        = $(this);        
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = $(this).val();
		icon.parent().addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'callback', 'id': id, 'values': {'status': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.parent().removeClass('loading_icon');
				if(state)
					line.addClass('active');
				else
					line.removeClass('active');				
			},
			dataType: 'json'
		});	
		return false;	
	});
	
	// Подтверждение удаления
	$("form#list_form").submit(function() {
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});

});

</script>
{/literal}
