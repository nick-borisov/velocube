{capture name=tabs}
	<li class="active"><a href="index.php?module=SettingsAdmin">Настройки</a></li>
	{if in_array('currency', $manager->permissions)}<li><a href="index.php?module=CurrencyAdmin">Валюты</a></li>{/if}
	{if in_array('delivery', $manager->permissions)}<li><a href="index.php?module=DeliveriesAdmin">Доставка</a></li>{/if}
	{if in_array('payment', $manager->permissions)}<li><a href="index.php?module=PaymentMethodsAdmin">Оплата</a></li>{/if}
	{if in_array('managers', $manager->permissions)}<li><a href="index.php?module=ManagersAdmin">Менеджеры</a></li>{/if}
{/capture}
 
{$meta_title = "Настройки" scope=parent}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success == 'saved'}Настройки сохранены{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error == 'watermark_is_not_writable'}Установите права на запись для файла {$config->watermark_file}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}
			

<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
			
		<!-- Параметры -->
		<div class="block">
			<h2>Настройки сайта</h2>
			<ul>
				<li><label class=property>Имя сайта</label><input name="site_name" class="simpla_inp" type="text" value="{$settings->site_name|escape}" /></li>
				<li><label class=property>Имя компании</label><input name="company_name" class="simpla_inp" type="text" value="{$settings->company_name|escape}" /></li>
				<li><label class=property>Формат даты</label><input name="date_format" class="simpla_inp" type="text" value="{$settings->date_format|escape}" /></li>
				<li><label class=property>Email для восстановления пароля</label><input name="admin_email" class="simpla_inp" type="text" value="{$settings->admin_email|escape}" /></li>
				<li><label class=property>Количество отзывов на странице</label><input name="limit_comments" class="simpla_inp" type="text" value="{$settings->limit_comments|escape}" /></li>
			</ul>
		</div>
        
        
		<div class="block layer">
			<h2>Оповещения</h2>
			<ul>
				<li><label class=property>Оповещение о заказах</label><input name="order_email" class="simpla_inp" type="text" value="{$settings->order_email|escape}" /></li>
				<li><label class=property>Оповещение о комментариях</label><input name="comment_email" class="simpla_inp" type="text" value="{$settings->comment_email|escape}" /></li>
				<li><label class=property>Обратный адрес оповещений</label><input name="notify_from_email" class="simpla_inp" type="text" value="{$settings->notify_from_email|escape}" /></li>
			</ul>
		</div>
		<!-- Параметры (The End)-->
        
		<!-- Обратный звонок-->
        <div class="block layer">
			<h2>Обратный звонок</h2>
			<ul>
				<li><label class=property>Оповещение на email</label><input name="callback_email" {*class="simpla_inp"*} type="text" value="{$settings->callback_email|escape}" /></li>
                <li style="display: none;"><label class=property>Маска для телефона</label><input name="callback_mask" class="simpla_inp" type="checkbox" value="1" {if $settings->callback_mask==1} checked="checked"{/if}/></li>
				<li><label class=property>Поле для сообщения</label><input name="callback_message" class="simpla_inp" type="checkbox" value="1" {if $settings->callback_message==1} checked="checked"{/if}/></li>
				<li class="calltime"><label class=property>Временной интервал</label><input name="callback_calltime" class="simpla_inp" type="checkbox" value="1" {if $settings->callback_calltime} checked="checked"{/if} />
                C:
                <select name="calltime_from">
                    {section name=calltime_from loop=24 step=1}
                        <option value="{$smarty.section.calltime_from.index}" {if $settings->calltime_from == $smarty.section.calltime_from.index} selected="selected"{/if}>{$smarty.section.calltime_from.index}:00</option>
                    {/section}
                    
                </select>
                До:
                <select name="calltime_to">
                    {section name=calltime_to loop=24 step=1}
                        <option value="{$smarty.section.calltime_to.index}" {if $settings->calltime_to == $smarty.section.calltime_to.index} selected="selected"{/if}>{$smarty.section.calltime_to.index}:00</option>
                    {/section}
                    
                </select>
                </li>
			</ul>
		</div>
		<!-- Обратный звонок (The End)-->
		
		<!-- Параметры -->
		<div class="block layer">
			<h2>Формат цены</h2>
			<ul>
				<li><label class=property>Разделитель копеек</label>
					<select name="decimals_point" class="simpla_inp">
						<option value='.' {if $settings->decimals_point == '.'}selected{/if}>точка: 12.45 рублей</option>
						<option value=',' {if $settings->decimals_point == ','}selected{/if}>запятая: 12,45 рублей</option>
					</select>
				</li>
				<li><label class=property>Разделитель тысяч</label>
					<select name="thousands_separator" class="simpla_inp">
						<option value='' {if $settings->thousands_separator == ''}selected{/if}>без разделителя: 1245678 рублей</option>
						<option value=' ' {if $settings->thousands_separator == ' '}selected{/if}>пробел: 1 245 678 рублей</option>
						<option value=',' {if $settings->thousands_separator == ','}selected{/if}>запятая: 1,245,678 рублей</option>
					</select>
				
				
				</li>
                
                <li><label class=property>Граница наличия</label><input class="set_interval" name="interval_min" class="simpla_inp" type="number" value="{$settings->interval_min|escape}" min="1"/> Если товаров меньше, то покажет <span style="color: red">"заканчивается"</span></li>
				{*
                <li><label class=property>Граница заканчивается/много</label><input class="set_interval" name="interval_max" class="simpla_inp" type="number" value="{$settings->interval_max|escape}" min="2" /> Больше этого светофор покажет "в наличии"</li>
				*}
                <li><label class=property>Доставка бесплатна от </label><input class="set_interval" name="interval_price" class="simpla_inp" type="number" value="{$settings->interval_price|escape}" min="1000" step="100" /> руб. </li>
                <li><label class=property>Доставка ЗАВТРА бесплатна от </label><input class="set_interval" name="interval_tom_price" class="simpla_inp" type="number" value="{$settings->interval_tom_price|escape}" min="2000" step="100" /> руб. </li>
                <li><label class=property>Стоимость доставки в яндексе по умолчанию</label><input class="set_interval" name="yandex_delivery_cost" class="simpla_inp" type="number" value="{$settings->yandex_delivery_cost|escape}" min="100" step="10" />руб.</li>
                <li><label class=property>Множитель доставки X</label><input class="set_interval" name="interval_delivery_mult" class="simpla_inp" type="number" value="{$settings->interval_delivery_mult|escape}" min="1" step="1" /></li>
                
                <li>
                    <textarea class="calendar_days" name="calendar_days">{rtrim($settings->calendar_days)}</textarea>
                    <label class=property>Нерабочие дни доставки</label><input type="text" id="delivery_calendar" />
                    
                </li>
            </ul>
            
		</div>
		<!-- Параметры (The End)-->
		
		<!-- Параметры -->
		<div class="block layer">
			<h2>Настройки каталога</h2>
			<ul>
				<li><label class=property>Товаров на странице сайта</label><input name="products_num" class="simpla_inp" type="text" value="{$settings->products_num|escape}" /></li>
				<li><label class=property>Товаров на странице админки</label><input name="products_num_admin" class="simpla_inp" type="text" value="{$settings->products_num_admin|escape}" /></li>
				<li><label class=property>Максимум товаров в заказе</label><input name="max_order_amount" class="simpla_inp" type="text" value="{$settings->max_order_amount|escape}" /></li>
				<li><label class=property>Единицы измерения товаров</label><input name="units" class="simpla_inp" type="text" value="{$settings->units|escape}" /></li>
                <li><label class=property>Скрыть товары не в наличии</label><input name="view_in_stock"  value='1' type="checkbox" {if $settings->view_in_stock}checked{/if} /></li>
                <li><label class=property for="order_hits">Учитывать заказы при сортировке по умолчанию </label><input name="order_hits" id="order_hits" class="simpla_inp" value='1' type="checkbox" {if $settings->order_hits}checked{/if} /></li>
                <li><label class=property for="order_sklad">Учитывать склад/магазин/резерв при сортировке по умолчанию </label><input name="order_sklad" id="order_sklad" class="simpla_inp" value='1' type="checkbox" {if $settings->order_sklad}checked{/if} /></li>
			</ul>
		</div>
		<!-- Параметры (The End)-->
		
		<!-- Параметры -->
		<div class="block layer">
			<h2>Изображения товаров</h2>
			
			<ul>
				<li><label class=property>Водяной знак</label>
				<input name="watermark_file" class="simpla_inp" type="file" />

				<img style='display:block; border:1px solid #d0d0d0; margin:10px 0 10px 0;' src="{$config->root_url}/{$config->watermark_file}?{math equation='rand(10,10000)'}">
				</li>
				<li><label class=property>Горизонтальное положение водяного знака</label><input name="watermark_offset_x" class="simpla_inp" type="text" value="{$settings->watermark_offset_x|escape}" /> %</li>
				<li><label class=property>Вертикальное положение водяного знака</label><input name="watermark_offset_y" class="simpla_inp" type="text" value="{$settings->watermark_offset_y|escape}" /> %</li>
				<li><label class=property>Прозрачность знака (больше &mdash; прозрачней)</label><input name="watermark_transparency" class="simpla_inp" type="text" value="{$settings->watermark_transparency|escape}" /> %</li>
				<li><label class=property>Резкость изображений (рекомендуется 20%)</label><input name="images_sharpen" class="simpla_inp" type="text" value="{$settings->images_sharpen|escape}" /> %</li>
			</ul>
		</div>
		<!-- Параметры (The End)-->

		<!-- Параметры -->
		<div class="block layer">
			<h2>Дополнительные настройки</h2>
			<ul>
				<li><label class=property>Подсчет товаров в категории</label><input name="category_count"  value='1' type="checkbox" {if $settings->category_count}checked{/if} /></li>
                <li><input type="button" value="Очистить склад" id="check_store_status" /><span class="icons" style="display:inline-block"><a class="icon" style="width: 100px;height: 30px;"></a></span></li>
                <li><input type="button" value="Очистить магазин" id="check_store_status2" /><span class="icons2" style="display:inline-block"><a class="icon2" style="width: 100px;height: 30px;"></a></span></li>
			</ul>
		</div>
		<!-- Параметры (The End)-->

		<!-- Параметры -->
		<div class="block layer">
			<h2>Hастройки <a href="http://kosjak76.sms.ru" >SMS.RU</a> (-10%)</h2>
			<ul>
				<li><label class=property><a href="http://kosjak76.sms.ru" >SMS.RU</a> Ваш api_id:</label><input name="sms_api" class="simpla_inp" type="text" value="{$settings->sms_api|escape}" /></li>
				<li class="sms_settings"><label class=property>Teлефон администратора:<br> (+380670000000)</label>
                <input id=new_feature type=text style="float: right;margin-right: 145px;"/>
                {foreach $settings->sms_phone as $phone}
                    <span class="item_phone">
                        <input name="sms_phone[]" class="simpla_inp" type="text" value="{$phone|escape}" {if $phone@iteration>2}style="margin-left: 170px"{/if}/>
                        <span class="add"{if $phone@iteration!=1}style="display:none"{/if}><i class="dash_link" id="add_new_feature">Добавить телефон</i></span>
                        <span {if $phone@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                    </span>
                {/foreach}
                
                </li>
				<li><label class=property>SMS о новом заказе администратору</label><input name="sms_order_admin"  value='1' type="checkbox" {if $settings->sms_order_admin}checked{/if} /></li>
				<li><label class=property>SMS о новом заказе пользователю</label><input name="sms_order_user"  value='1' type="checkbox" {if $settings->sms_order_user}checked{/if} /></li>
				<li><label class=property>Сменить кодировку</label><input name="sms_convert"  value='1' type="checkbox" {if $settings->convert}checked{/if} /></li>
			</ul>
		</div>
		<!-- Параметры (The End)-->

		<!-- Параметры -->
		<div class="block layer">
			<h2>Скидки от суммы заказа</h2>
			<ul>
				<li style="width:40%;display:inline-block;clear:none;"><label style="width:80px;" class=property>Сумма 1</label><input name="sum_1" class="simpla_inp" type="text" value="{$settings->sum_1|escape}" /></li>
				<li style="width:40%;display:inline-block;clear:none;"><label style="width:80px;" class=property>Процент 1</label><input name="proc_1" class="simpla_inp" type="text" value="{$settings->proc_1|escape}" /></li>
				<li style="width:40%;display:inline-block;clear:none;"><label style="width:80px;" class=property>Сумма 2</label><input name="sum_2" class="simpla_inp" type="text" value="{$settings->sum_2|escape}" /></li>
				<li style="width:40%;display:inline-block;clear:none;"><label style="width:80px;" class=property>Процент 2</label><input name="proc_2" class="simpla_inp" type="text" value="{$settings->proc_2|escape}" /></li>
				<li style="width:40%;display:inline-block;clear:none;"><label style="width:80px;" class=property>Сумма 3</label><input name="sum_3" class="simpla_inp" type="text" value="{$settings->sum_3|escape}" /></li>
				<li style="width:40%;display:inline-block;clear:none;"><label style="width:80px;" class=property>Процент 3</label><input name="proc_3" class="simpla_inp" type="text" value="{$settings->proc_3|escape}" /></li>								
			</ul>
		</div>
		<!-- Параметры (The End)-->
		
		<!-- Параметры -->
		<div class="block layer">
			<h2>Интеграция с <a href="http://prostiezvonki.ru">простыми звонками</a></h2>
			<ul>
				<li><label class=property>Сервер</label><input name="pz_server" class="simpla_inp" type="text" value="{$settings->pz_server|escape}" /></li>
				<li><label class=property>Пароль</label><input name="pz_password" class="simpla_inp" type="text" value="{$settings->pz_password|escape}" /></li>
				<li><label class=property>Телефоны менеджеров:</label></li>
				{foreach $managers as $manager}
				<li><label class=property>{$manager->login}</label><input name="pz_phones[{$manager->login}]" class="simpla_inp" type="text" value="{$settings->pz_phones[$manager->login]|escape}" /></li>
				{/foreach}
			</ul>
		</div>
		<!-- Параметры (The End)-->
        
         <!-- Параметры Экспорт YandexMarket-->
        <div class="block layer">
			<h2>Экспорт в YandexMarket</h2>
			<ul>
                <li><input id="yandex_vnalichii" name="yandex_vnalichii" type="checkbox" {if $settings->yandex_vnalichii}checked{/if} /><label for="yandex_vnalichii" class="property yandex">Экспортировать со статусом "под заказ", товары со статусом "Под заказ, Пердзаказ, Ожидается" и кол-вом > 0</label></li>
                <li><input id="yandex_store" name="yandex_store" type="checkbox" {if $settings->yandex_store}checked{/if} /><label for="yandex_store" class="property yandex">Можно купить в розничном магазине</label></li>
                <li><input id="yandex_pickup" name="yandex_pickup" type="checkbox" {if $settings->yandex_pickup}checked{/if} /><label for="yandex_pickup" class="property yandex">Можно зарезервировать выбранный товар и забрать его самостоятельно</label></li>
                <li><input id="yandex_full_description" name="yandex_full_description" type="checkbox" {if $settings->yandex_full_description}checked{/if} /><label for="yandex_full_description" class="property yandex">Выводить в ЯндекМаркет полное описание товара (если не включено, выводится краткое описание)</label></li>
                <li><input id="yandex_oldprice" name="yandex_oldprice" type="checkbox" {if $settings->yandex_oldprice}checked{/if} /><label for="yandex_oldprice" class="property yandex">Выгружать oldprice</label></li>
                {*<li><input id="yandex_seller_warranty" name="yandex_seller_warranty" type="checkbox" {if $settings->yandex_seller_warranty}checked{/if} /><label for="yandex_seller_warranty" class="property yandex">У товаров есть гарантия продавца</label></li>
                <li><label class="property sales">sales notes</label><input name="yandex_sales_notes" class="simpla_inp" type="text" value="{$settings->yandex_sales_notes|escape}" /></li> *}
            </ul>
        </div>    
        <!-- Параметры Экспорт YandexMarket (The End)-->
		
		<input class="button_green button_save" type="submit" name="save" value="Сохранить" />
			
	<!-- Левая колонка свойств товара (The End)--> 
	
</form>
<!-- Основная форма (The End) -->

{literal}
<script>
$(function() {
	$('#change_password_form').hide();
	$('#change_password').click(function() {
		$('#change_password_form').show();
	});
    var text = $('textarea.calendar_days').html().replace(/\"/gi, '').split(', ');
     $('#delivery_calendar').datetimepicker({
        timepicker: false,
        lang: 'ru',
        inline: true,
        format: '\'d.m.Y\'',
        closeOnDateSelect: false,
        opened: true,
        dayOfWeekStart: 1, 
        formatDate:'d.m.Y',
        onGenerate: function(ct){
            $('td.xdsoft_date').removeClass('xdsoft_weekend');
            $('td.xdsoft_date').each(function(index, elem){
                var ed = $(this).data('date');
                if(ed<=9) ed='0'+ed;
                var em = $(this).data('month')+1;
                if(em<=9) em='0'+em;
                var ey = $(this).data('year');
                var edate = '\''+ed+'.'+em+'.'+ey+'\'';

                if($.inArray(edate, text)!=-1){
                    $(this).addClass('xdsoft_weekend');
                }
            });
        },
        onChangeDateTime:function(ct){
            callme(ct); 
        }
    });
    function callme(date){
        var day = date.getDate();
        if(day<=9) day = '0'+day;
        var month = date.getMonth()+1;
        if(month<=9) month = '0'+month;
        var year = date.getFullYear();
        var itday = '\''+day+'.'+month+'.'+year+'\'';

        if(itday != ''){
            if($.inArray(itday, text)==-1){
                text.push(itday);
            }else{
                text.splice($.inArray(itday, text), 1);                
            }
        }
        
        $('textarea.calendar_days').html('');
        text.sort();
        console.log(text);
        for(var i=0;i<=text.length;i++){
            if(text[i]){
                $('textarea.calendar_days').append(text[i]);
                if(i!=(text.length-1)) $('textarea.calendar_days').append(', ');
            }    
        }
    }
    
    	// 
	$("#check_store_status").click(function() {
		$('.icon').addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'check_store', 'values': 1, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				$('.icon').removeClass('loading_icon');
                console.log(data);
				$('.icon').text(data);
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    $("#check_store_status2").click(function() {
		$('.icon2').addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'check_store', 'values': 2, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				$('.icon2').removeClass('loading_icon');
                console.log(data);
				$('.icon2').text(data);
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    // Добавление нового свойства товара
	var feature = $('#new_feature').clone(true);
	$('#new_feature').remove().removeAttr('id');
	$('#add_new_feature').click(function() {
		$(feature).clone(true).appendTo('li.sms_settings').fadeIn('slow').attr('name', 'sms_phone[]').find("input[name*=new_feature_name]").focus();
		return false;		
	});
    
    // Удаление категории
	$(".sms_settings .delete").bind('click', function() {
		$(this).closest(".item_phone").fadeOut(200, function() { $(this).remove(); });
		return false;
	});
});
</script>
{/literal}
