{* Вкладки *}
{capture name=tabs}
	{if in_array('orders', $manager->permissions)}
	<li {if $status==='all'}class="active"{/if}><a href="{url module=OrdersAdmin status=all keyword=null id=null page=null  filter=null label=null}">Все</a></li>
	<li {if $status===0}class="active"{/if}><a href="{url module=OrdersAdmin status=0 keyword=null id=null page=null filter=null label=null}">Новые</a></li>
	<li {if $status==1}class="active"{/if}><a href="{url module=OrdersAdmin status=1 keyword=null id=null page=null filter=null label=null}">Приняты</a></li>
	<li {if $status==2}class="active"{/if}><a href="{url module=OrdersAdmin status=2 keyword=null id=null page=null filter=null label=null}">Выполнены</a></li>
	<li {if $status==4}class="active"{/if}><a href="{url module=OrdersAdmin status=4 keyword=null id=null page=null filter=null label=null}">Отменены</a></li>
	<li {if $status==5}class="active"{/if}><a href="{url module=OrdersAdmin status=5 keyword=null id=null page=null filter=null label=null}">Проблемы</a></li>
	<li {if $status==3}class="active"{/if}><a href="{url module=OrdersAdmin status=3 keyword=null id=null page=null filter=null label=null}">Удалены</a></li>
	{if $keyword}
	<li class="active"><a href="{url module=OrdersAdmin keyword=$keyword id=null label=null}">Поиск</a></li>
	{/if}
	{/if}
	{if in_array('labels', $manager->permissions)}
	<li><a href="{url module=OrdersLabelsAdmin keyword=null id=null page=null label=null}">Метки</a></li>
	{/if}
{/capture}
{* On document load *}
{literal}
<script src="design/js/jquery/datepicker/jquery.ui.datepicker-ru.js"></script>

<script>
$(function() {

$('input[name="filter[date_from]"]').datepicker({
regional:'ru'
});

$('input[name="filter[date_to]"]').datepicker({
regional:'ru'
});

$('input[name="filter[delivery_from]"]').datepicker({
regional:'ru'
});

$('input[name="filter[delivery_to]"]').datepicker({
regional:'ru'
});

});
</script>
{/literal}

{* Title *}
{$meta_title='Заказы' scope=parent}

{* Поиск *}
<div id="search">
<form method="get">

	<input type="hidden" name="module" value="OrdersAdmin">
	<input class="search" type="text" name="keyword" value="{$keyword|escape}"/>
	<input class="search_button" type="submit" value=""/>

</form>
{if $orders_count>0}
	<form method="post" action="{url module=ExportOrdersAdmin}" target="_blank">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
	<input type="image" src="./design/images/export_excel.png" name="export" title="Экспортировать заказы">
	</form>
	{/if}
<!-- Меню -->
<div id="right_menu">
	
	{if $labels}
	<!-- Метки -->
	<ul id="labels">
	{if !$label}
		<li {if !$label}class="selected"{/if}><span class="label"></span> <a href="javascript: void(0);" onclick="$('#labs').toggle();">Все заказы</a></li>
	{else}	
		{foreach $labels as $l}
		{if $label->id==$l->id}
		<li data-label-id="{$l->id}" {if $label->id==$l->id}class="selected"{/if}>
		<span style="background-color:#{$l->color};" class="order_label"></span>
		<a href="javascript: void(0);" onclick="$('#labs').toggle();">{$l->name}</a></li>
		{/if}
		{/foreach}
	{/if}
	</ul>
	<div id="labs" style="display:none;position:absolute;border:1px solid #ddd;background-color:#fff;">	
	<ul id="labels">
		<li {if !$label}class="selected"{/if}><span class="label"></span> <a href="{url label=null}">Все заказы</a></li>
		{foreach $labels as $l}
		<li data-label-id="{$l->id}" {if $label->id==$l->id}class="selected"{/if}>
		<span style="background-color:#{$l->color};" class="order_label"></span>
		<a href="{url label=$l->id}">{$l->name}</a></li>
		{/foreach}
	</ul>
	<!-- Метки -->
	</div>
	{/if}
	
</div>
<!-- Меню  (The End) -->

</div>	
{* Заголовок *}
<div id="header">
	<h1>{if $orders_count}{$orders_count}{else}Нет{/if} заказ{$orders_count|plural:'':'ов':'а'}</h1>		
	<a class="add" href="{url module=OrderAdmin}">Добавить заказ</a>
<div style="clear:both;"></div>	
<form method="get">


<div id='filter_fields'>
<input type="hidden" name="module" value="OrdersAdmin">
<input type="hidden" name="status" value="{$status}">
<div style="margin: 10px 0">
<label>Дата с:&nbsp;</label><input type=text style="width:100px;" name=filter[date_from] value='{$smarty.get.filter.date_from}'>&nbsp;
<label>По:&nbsp;</label><input type=text style="width:100px;" name=filter[date_to] value='{$smarty.get.filter.date_to}'>&nbsp;
<label>Доставка с:&nbsp;</label><input type=text style="width:100px;" name=filter[delivery_from] value='{$smarty.get.filter.delivery_from}'>&nbsp;
<label>По:&nbsp;</label><input type=text style="width:100px;" name=filter[delivery_to] value='{$smarty.get.filter.delivery_to}'>
</div>
<input id="apply_action" class="button_green" type="submit" value="Применить">
</div>
</form>	
</div>	
<div style="clear:both;"></div>
{if $orders}
<div id="main_list2" style="overflow:auto;width:100%;">
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->
	<form id="form_list" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">	
		<div id="list">		
		<table id="zakazi">
  <tr>
    <td></td>
    <td style="width: 40px"><b>№</b></td>
	<td width="90"><b>Дата</b></td>
		<td width="100"><b>Дата/время доставки
</b></td>
	<td width="250"><b>ФИО</b><div style="width: 200px">&nbsp;</div></td>


	<td><b> Корзина</b></td>

	<td width="70"><b>Стоимость</b></td>
	<td width="150"><b>Адрес</b></td>	
	<td width="45"><b>Статус</b></td>
  </tr>

			{foreach $orders as $order}
		
  <tr class="{if $order->paid}green{/if} row">
	<td>
	<input type="checkbox" name="check[]" value="{$order->id}"/>
	</td>
	<td>
  	 	<a href="{url module=OrderAdmin id=$order->id return=$smarty.server.REQUEST_URI}">{$order->id}</a> 
        <br />
    {if $order->smartbikes}<b>SmartBikes</b>{/if} 
	</td>
  <td>{$order->date|date} <br>в {$order->date|time}
    {if $order->manager}
    <br><b>{$order->manager}</b>
    {/if} 
    
  </td>

  <td>
 {if $order->delivery_date && $order->delivery_date != '0000-00-00' && $order->delivery_date != '1970-01-01'}
{$order->delivery_date|date}
{/if}
{if $order->delivery_time}
<br />{$order->delivery_time}
{/if} 
  </td>
  <td><b>{$order->name|escape}</b><br>+{$order->phone|escape}{if $order->phone2}<br>+{$order->phone2|escape}<br>{/if}
 {$order->comment|escape|nl2br}
	 				{if $order->note}
	 				<div class="note">{$order->note|escape}</div>
	 				{/if}
			{foreach $order->comments as $comment}
			<div class="note">
				<b>
					{$comment->name|escape}, {$comment->date|date} в {$comment->date|time}
					</b>
					<div class="comment_text">
					{$comment->text|nl2br}
					</div>					
				</div>

				<div class="clear"></div>
			</div>
			{/foreach}	 				
	 				 
 </td>
  
  <td>
<div id="purchase_list">
<div class="post">
	<div class="title"></div>
	<div class="entry">
		{foreach $order->purchases as $p}
			<br>{$p->product_name} {$p->variant_name} - {$p->amount} шт.<br>
		{/foreach}
	</div>
	</div>
</div>
  </td>

<td>
	{$order->total_price}
</td>
  <td>{$order->address|escape}</td>
<td>		
<div class="icons cell" style="width: 66px;">
    <a class="preview"   title="Предпросмотр в новом окне" href="../order/{$order->url}" target="_blank"></a>						
	{if $order->paid}
	<a href='#' style="background-image: url(./design/images/cash_stack.png)" alt='Оплачен' title='Оплачен'></a>
	{else}
	<a href='#' style="background-image: url(./design/images/cash_stack_gray.png)" alt='Не оплачен' title='Не оплачен'></a>				
	{/if}			 	

	{if $order->status == 0}
	<a href='#' style="background-image: url(./design/images/new.png)" alt='Новый' title='Новый'></a>
	{/if}
	{if $order->status == 1}
	<a href='#' style="background-image: url(./design/images/time.png)" alt='Принят' title='Принят'></a>
	{/if}
	{if $order->status == 2}
	<a href='#' style="background-image: url(./design/images/tick.png)" alt='Выполнен' title='Выполнен'></a>
	{/if}
	{if $order->status == 3}
	<a href='#' style="background-image: url(./design/images/cross.png)" alt='Удалён' title='Удалён'></a>
	{/if}
	{if $order->status == 5}
	<a href='#' style="background-image: url(./design/images/crap.png)" alt='Проблемный' title='Проблемный'></a>
	{/if}	

					{foreach $order->labels as $l}
					<span class="order_label" style="background-color:#{$l->color};" title="{$l->name}"></span>
					{/foreach}
	
	<a href='{url module=OrderAdmin id=$order->id view=print}'  target="_blank" class="print" title="Печать заказа"></a>
	<a href='#' class=delete title="Удалить"></a>
	</div>
</td>			
</tr>
{/foreach}
</table>
</div>

	
		<div id="action">
		<label id='check_all' class="dash_link">Выбрать все</label>
	
		<span id="select">
		<select name="action">
			{foreach $labels as $l}
			<option value="set_label_{$l->id}">Отметить &laquo;{$l->name}&raquo;</option>
			{/foreach}
			{foreach $labels as $l}
			<option value="unset_label_{$l->id}">Снять &laquo;{$l->name}&raquo;</option>
			{/foreach}
			<option value="delete">Удалить выбранные заказы</option>
		</select>
		</span>
	
		<input id="apply_action" class="button_green" type="submit" value="Применить">
		
		</div>
	</form>
	
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->
		
</div>
{/if}





{* On document load *}
{literal}
<script>

$(function() {

		$("#expand").show();
	// Показать все варианты
	$("#expand_all").click(function() {
		$("a#expand_all").hide();
		$("a#roll_up_all").show();
		$("a.expand_variant").hide();
		$("a.roll_up_variant").show();
		$(".info").fadeIn('fast');
		return false;
	});


	// Свернуть все варианты
	$("#roll_up_all").click(function() {
		$("a#roll_up_all").hide();
		$("a#expand_all").show();
		$("a.roll_up_variant").hide();
		$("a.expand_variant").show();
		$(".info").fadeOut('fast');
		return false;
	});

 
	// Показать вариант
	$("a.expand_variant").click(function() {
		$(this).closest("div.cell").find(".info").fadeIn('fast');
		$(this).closest("div.cell").find("a.expand_variant").hide();
		$(this).closest("div.cell").find("a.roll_up_variant").show();
		return false;
	});

	// Свернуть вариант
	$("a.roll_up_variant").click(function() {
		$(this).closest("div.cell").find(".info").fadeOut('fast');
		$(this).closest("div.cell").find("a.roll_up_variant").hide();
		$(this).closest("div.cell").find("a.expand_variant").show();
		return false;
	});

	// Сортировка списка
	$("#labels").sortable({
		items:             "li",
		tolerance:         "pointer",
		scrollSensitivity: 40,
		opacity:           0.7
	});
	

	$("#main_list #list .row").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			label_id = $(ui.helper).attr('data-label-id');
			$(this).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(this).closest("form").find('select[name="action"] option[value=set_label_'+label_id+']').attr("selected", "selected");		
			$(this).closest("form").submit();
			return false;	
		}		
	});
	
	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();

	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', $('#list input[type="checkbox"][name*="check"]:not(:checked)').length>0);
	});	

	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest(".row").find('input[type="checkbox"][name*="check"]').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').prop('selected', true);
		$(this).closest("form").submit();
	});

	// Подтверждение удаления
	$("form").submit(function() {
		if($('#list input[type="checkbox"][name*="check"]:checked').length>0)
			if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
				return false;	
	});
});

</script>
{/literal}
