{* Вкладки *}
{capture name=tabs}
	{if in_array('users', $manager->permissions)}<li><a href="index.php?module=UsersAdmin">Покупатели</a></li>{/if}
	{if in_array('groups', $manager->permissions)}<li><a href="index.php?module=GroupsAdmin">Группы</a></li>{/if}
	<li {if !$smarty.get.auto_discount}class="active"{/if}><a href="index.php?module=CouponsAdmin">Купоны</a></li>
    <li {if $smarty.get.auto_discount}class="active"{/if}><a href="index.php?module=CouponsAdmin&auto_discount=1">Скидки</a></li>
{/capture}

{if $coupon->code}
{$meta_title = $coupon->code scope=parent}
{else}
{$meta_title = 'Новый купон' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}
<script src="design/js/autocomplete/jquery.autocomplete-min.js"></script>
{*<script src="design/js/jquery/datepicker/jquery.ui.datepicker-ru.js"></script>*}
{literal}
<script>
$(function() {

	$('input[name="expire"]').datepicker({
		regional:'ru'
	});
	$('input[name="end"]').datepicker({
		regional:'ru'
	});

	// On change date
	$('input[name="expire"]').focus(function() {
 
    	$('input[name="expires"]').attr('checked', true);

	});
    
  	// Добавление категории
	$('#product_categories .add').click(function() {
		$("#product_categories ul li:last").clone(false).appendTo('#product_categories ul').fadeIn('slow').find("select[name*=categories]:last").focus();
		$("#product_categories ul li:last span.add").hide();
		$("#product_categories ul li:last span.delete").show();
		return false;		
	});

	// Удаление категории
	$(document).on('click', "#product_categories .delete", function() {
		$(this).closest("li").fadeOut(200, function() { $(this).remove(); });
		return false;
	});
    
    //Галочка для подкатегории
    $(document).on('change', "#product_categories ul li select", function(){
        $(this).parent().find('span.subcat input').val($(this).val());
        console.log($(this).val());
    });
    
    	// Добавление бренда
	$('#product_categories_brands .add').click(function() {
		$("#product_categories_brands ul li:last").clone(false).appendTo('#product_categories_brands ul').fadeIn('slow').find("select[name*=categories]:last").focus();
		$("#product_categories_brands ul li:last span.add").hide();
		$("#product_categories_brands ul li:last span.delete").show();
		return false;		
	});

	// Удаление бренда
	$("#product_categories_brands .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function() { $(this).remove(); });
		return false;
	});
    
    // Удаление связанного товара
	$(document).on('click', ".related_products a.delete", function() {
		 $(this).closest("div.row").fadeOut(200, function() { $(this).remove(); });
		 return false;
	});
 

	// Добавление связанного товара 
	var new_related_product = $('#new_related_product').clone(true);
	$('#new_related_product').remove().removeAttr('id');
 
	$("#related_products").autocomplete({
		serviceUrl:'ajax/search_products.php',
		minChars:3,
		noCache: true, 
		onSelect:
			function(value, data){
				new_item = new_related_product.clone().appendTo('.related_products');
				new_item.removeAttr('id');
				new_item.find('a.related_product_name').html(data.fullname);
				new_item.find('a.related_product_name').attr('href', 'index.php?module=ProductAdmin&id='+data.id);
				new_item.find('input[name*="related_products"]').val(data.id);
				if(data.image)
					new_item.find('img.product_icon').attr("src", data.image);
				else
					new_item.find('img.product_icon').remove();
				$("#related_products").val(''); 
				new_item.show();
			},
		fnFormatResult:
			function(value, data, currentValue){
				var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
				var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
			}

	});
    // Добавление исключенного товара 
	var new_related_product_stop = $('#new_related_product_stop').clone(true);
	$('#new_related_product_stop').remove().removeAttr('id');
    
    // Удаление исключенного товара
	$(".related_products_stop a.delete").bind('click', function() {
		 $(this).closest("div.row").fadeOut(200, function() { $(this).remove(); });
		 return false;
	});
    
    $("#related_products_stop").autocomplete({
		serviceUrl:'ajax/search_products.php',
		minChars:3,
		noCache: true, 
		onSelect:
			function(value, data){
				new_item = new_related_product_stop.clone().appendTo('.related_products_stop');
                console.log(new_item);
				new_item.removeAttr('id');
				new_item.find('a.related_product_name').html(data.fullname);
				new_item.find('a.related_product_name').attr('href', 'index.php?module=ProductAdmin&id='+data.id);
				new_item.find('input[name*="related_products_stop"]').val(data.id);
				if(data.image)
					new_item.find('img.product_icon').attr("src", data.image);
				else
					new_item.find('img.product_icon').remove();
				$("#related_products_stop").val(''); 
				new_item.show();
			},
		fnFormatResult:
			function(value, data, currentValue){
				var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
				var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
			}

	});

    //Add Schedule timeset
    $(document).on("click", ".js-add_timeset", function(){
        var new_timeset = $(this).closest('li.coupon-timeset').clone();
        $(this).closest('.coupon-schedule').append(new_timeset).fadeIn(200);
    });
    
    //Delete Schedule timeset
    $(document).on("click", ".js-delete_timeset", function(){
        $(this).closest("li").fadeOut(200, function() { $(this).remove(); });
		return false;
    });
    
    //Переключение наценки
    $('#column_right .activator span#check').click(function(){
        $(this).parents('.block.layer').find('.margin-body').toggle();
        $('#column_left .total_discount').toggle();
    });
    
    // Добавление нового интервала наценки
   	$('#add_new_feature').click(function() {
		$('.marga-settings li:last').clone(true).appendTo('ul.marga-settings').fadeIn('slow');//.find("input[name*=new_feature_name]").focus();
        $('.marga-settings li:last .add').hide();
        $('.marga-settings li:last .delete').show();
		return false;		
	});
    
    // Удаление наценки
	$(".marga-settings li .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function(){ $(this).remove(); });
		return false;
	});
    
    // Добавление нового интервала наценки на закупку
	
	$('#add_new_interval_s').click(function() {
		$('.marga-settings_stock li:last').clone(true).appendTo('ul.marga-settings_stock').fadeIn('slow');
        $('.marga-settings_stock li:last .add').hide();
        $('.marga-settings_stock li:last .delete').show();
		return false;		
	});
    
    // Удаление наценки на закупку
	$(".marga-settings_stock li .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function(){ $(this).remove(); });
		return false;
	});
});
</script>
<script src="../../js/autocomplete/jquery.autocomplete-min.js" type="text/javascript"></script>
	<style>
	.autocomplete-suggestion { solid; background: #FFF !important; padding: 5px;}
	.autocomplete-w1 { position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; width: 500px; }
	.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:hidden;  overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
	.autocomplete .selected { background:#F0F0F0; }
	.autocomplete div { padding:2px 5px; white-space:nowrap; }
	.autocomplete strong { font-weight:normal; color:#3399FF; }
	</style>	
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success == 'added'}Купон добавлен{elseif $message_success == 'updated'}Купон изменен{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error == 'code_exists'}Купон с таким кодом уже существует{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
    <div style="display: table; margin-left: auto;">
        {if $coupon->auto_discount}
            <input class="simpla_inp" id="ad_active" name="ad_active" type="checkbox" value="1" {if $coupon->ad_active}checked="checked"{/if}/>
            <label for="ad_active" style="margin-right: 20px">Активен</label>
        {else}
            <input class="simpla_inp" id="active" name="active" type="checkbox" value="1" {if $coupon->active}checked="checked"{/if}/>
            <label for="active" style="margin-right: 20px">Активен</label>
        {/if}
        <label class="ios7-switch">           
            <input type="checkbox" name="auto_discount" {if $coupon->auto_discount}checked{/if} value="1"/>
            <span id="check" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
        </label>
        <span style="font-size: 16px;font-weight: 600;">Автоматический купон-скидка</span>
    </div>
	<div class="name">
        Имя
		<input class="name" name="name" type="text" value="{$coupon->name|escape}"/>	
	</div> 
    {if $coupon->auto_discount}
    <div id="name">
        Аннотация
		<input class="name" name="annotation" type="text" value="{$coupon->annotation|escape}"/>
		<input name="id" class="name" type="hidden" value="{$coupon->id|escape}"/>		
	</div> 
    {else}
    <div id="name">
        Кодовое слово
		<input class="name" name="code" type="text" value="{$coupon->code|escape}"/>
		<input name="id" class="name" type="hidden" value="{$coupon->id|escape}"/>		
	</div> 
    {/if}
	<!-- Левая колонка свойств товара -->
	<div id="column_left" style="width: 500px">
			
		<div class="block layer">
			<ul class="total_discount" {if $coupon->active_margin}style="display:none"{/if}>
				<li>
					<label class=property>Скидка</label><input name="value" class="coupon_value" type="text" value="{$coupon->value|escape}" />
					<select class="coupon_type" name="type">
						<option value="percentage" {if $coupon->type=='percentage'}selected{/if}>%</option>
						<option value="absolute" {if $coupon->type=='absolute'}selected{/if}>{$currency->sign}</option>
					</select>
				</li>
				<li>
					<label class=property>Для заказов от</label>
					<input class="coupon_value" type="text" name="min_order_price" value="{$coupon->min_order_price|escape}"> {$currency->sign}		
				</li>
				<li>
					<label class=property for="single"></label>
					<input type="checkbox" name="single" id="single" value="1" {if $coupon->single==1}checked{/if}> <label for="single">одноразовый</label>					
				</li>
			</ul>
		</div>
		
     	<div id="product_categories" {if !$categories}style='display:none;'{/if}>
    		<label>Категория</label>
    		<div>
    			<ul>
    				{foreach name=categories from=$coupon_categories item=product_category}
    				<li>
    					<select name="categories[]">
                            <option value="">Выбрать категорию</option>
    						{function name=category_select level=0}
    						{foreach from=$categories item=category}
    								<option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
    								{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
    						{/foreach}
    						{/function}
    						{category_select categories=$categories selected_id=$product_category}
    					</select>
                        <span class="subcat"><input type="checkbox" name="subcat[]" value="{$product_category}" /></span>
    					<span {if not $smarty.foreach.categories.first}style='display:none;'{/if} class="add"><i class="dash_link">Добавить</i></span>
    					<span {if $smarty.foreach.categories.first}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
    				</li>
    				{/foreach}		
    			</ul>
    		</div>
    	</div>
        <br />
        <div id="product_categories_brands" {if !$brands}style='display:none;'{/if}>
    		<label>Бренд</label>
    		<div>
    			<ul>
    				{foreach name=brands from=$coupon_brands item=coupon_brand}
    				<li>
    					<select name="brands[]">
                            
    						{function name=brand_select level=0}
    						{foreach from=$brands item=brand}
    								<option value='{$brand->id}' {if $brand->id == $selected_id}selected{/if} brand_name='{$brand->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$brand->name|escape}</option>
    								{brand_select brands=$brand->subbrands selected_id=$selected_id  level=$level+1}
    						{/foreach}
    						{/function}
                            <option value="0">Выбрать бренд</option>
    						{brand_select brands=$brands selected_id=$coupon_brand}
    					</select>
    					<span {if not $smarty.foreach.brands.first}style='display:none;'{/if} class="add"><i class="dash_link">Добавить бренд</i></span>
    					<span {if $smarty.foreach.brands.first}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
    				</li>
    				{/foreach}		
    			</ul>
    		</div>
    	</div>
        <br />
        
   		<div class="block layer">
			<h2>Связанные товары</h2>
			<div id=list class="sortable related_products">
				{foreach from=$related_products item=related_product}
				<div class="row">
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products[] value='{$related_product->id}'>
					<a href="{url id=$related_product->id}">
					<img class=product_icon src='{$related_product->images[0]->filename|resize:35:35}'>
					</a>
					</div>
					<div class="name cell">
					<a href="{url id=$related_product->id}">{$related_product->fullname}</a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
				{/foreach}
				<div id="new_related_product" class="row" style='display:none;'>
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products[] value=''>
					<img class=product_icon src=''>
					</div>
					<div class="name cell">
					<a class="related_product_name" href=""></a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<input type=text name=related id='related_products' class="input_autocomplete" placeholder='Выберите товар чтобы добавить его'>
		</div>
        
        <div class="block layer">
			<h2>Исключенные товары</h2>
			<div id=list class="sortable related_products_stop">
				{foreach from=$related_products_stop item=related_product_s}
				<div class="row">
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products_stop[] value='{$related_product_s->id}'>
					<a href="{url id=$related_product_s->id}">
					<img class=product_icon src='{$related_product_s->images[0]->filename|resize:35:35}'>
					</a>
					</div>
					<div class="name cell">
					<a href="{url id=$related_product_s->id}">{$related_product_s->fullname}</a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
				{/foreach}
				<div id="new_related_product_stop" class="row" style='display:none;'>
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products_stop[] value=''>
					<img class=product_icon src=''>
					</div>
					<div class="name cell">
					<a class="related_product_name" href=""></a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<input type=text name=related id='related_products_stop' class="input_autocomplete" placeholder='Выберите товар чтобы добавить его'>
		</div>
        
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right" style="width: 450px">
        
    {*auto_discount schedule*}
    {if $coupon->auto_discount}
        
        {include 'autocoupons_margins.tpl' timeset_title="Ценовые диапазоны" inner_timesets=$margins}
        
		{include 'coupon_timeset.tpl' timeset_title="График для сайта" inner_timesets=$timesets save_name="timeset"}
        
        {include 'coupon_timeset.tpl' timeset_title="График для Яндекс.Маркета" inner_timesets=$timesets_yandex save_name="timeset_yandex"}
        
        {include 'coupon_timeset.tpl' timeset_title="График для Price.ru" inner_timesets=$timesets_priceru save_name="timeset_priceru"}
     
    {/if}
    {*/auto_discount schedule/*}

        {if $orders}
       	<div class="block layer orders">
            <h2>Заказы с использованием купона</h2>
            <div id="list">		
            	{foreach $orders as $order}
            	<div class="{if $order->paid}green{/if} row">
            		<div class="order_date cell">
            			{$order->date|date} {$order->date|time}
            		</div>
            		<div class="name cell">
            			<a href="{url module=OrderAdmin id=$order->id return=$smarty.server.REQUEST_URI}">Заказ №{$order->id}</a>
            		</div>
            		<div class="name cell">
            			{$order->total_price}&nbsp;{$currency->sign}
            		</div>
            		<div class="icons cell">
            			{if $order->paid}
            				<img src='design/images/cash_stack.png' alt='Оплачен' title='Оплачен'>
            			{else}
            				<img src='design/images/cash_stack_gray.png' alt='Не оплачен' title='Не оплачен'>				
            			{/if}	
            		</div>
            		<div class="clear"></div>
            	</div>
            	{/foreach}
            </div>
        </div>
        {/if}
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	
</form>
<!-- Основная форма (The End) -->
