{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	<li class="active"><a href="index.php?module=BrandsAdmin">Бренды</a></li>
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{* Title *}
{$meta_title='Бренды' scope=parent}

{* Заголовок *}
<div id="header">
	<h1>Бренды</h1> 
	<a class="add" href="{url module=BrandAdmin return=$smarty.server.REQUEST_URI}">Добавить бренд</a>
</div>	

{if $brands}
<div id="main_list" class="brands">

	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
		
		<div id="list" class="brands">	
			{foreach $brands as $brand}
			<div class="{if !$brand->meta_generation}meta{/if} {if !$brand->generate_description}description{/if} row">
				{if $filter!='byname'}
				<input type="hidden" name="positions[{$brand->id}]" value="{$brand->position}">
				<div class="move cell"><div class="move_zone"></div></div>
				{/if}
		 		<div class="checkbox cell">
					<input type="checkbox" name="check[]" value="{$brand->id}" />				
				</div>
                <div class="image cell brand_image">
    				
    				{if $brand->image}
    				<a href="{url module=BrandAdmin id=$brand->id return=$smarty.server.REQUEST_URI}"><img src="../{$config->brands_images_dir}{$brand->image}" alt="" width="50px" /></a>
    				{/if}
                </div>
				<div class="cell">
					<a href="{url module=BrandAdmin id=$brand->id return=$smarty.server.REQUEST_URI}">{$brand->name|escape}</a> 	 			
				</div>
				<div class="icons cell">
					<a class="preview" title="Предпросмотр в новом окне" href="../brands/{$brand->url}" target="_blank"></a>				
					<a class="delete"  title="Удалить" href="#"></a>
                    <a class="meta" title="Автогенерация метаданных" href="#"></a>
                    <a class="description" title="Автогенерация описания" href="#"></a>
				</div>
                
                <div class="icons cell">
        			<a class="yandex_c" data-to_yandex="1" href="javascript:;">В яндекс</a>
                    <a class="yandex_c" data-to_yandex="0" href="javascript:;">Из яндекса</a>
        		</div>
				<div class="clear"></div>
			</div>
			{/foreach}
		</div>
		
		<div id="action">
			<label id="check_all" class="dash_link">Выбрать все</label>
			
			<span id="select">
			<select name="action">
				<option value="delete">Удалить</option>
			</select>
			</span>
			<input id="apply_action" class="button_green" type="submit" value="Применить">
		</div>
		
	</form>
</div>
{else}
Нет брендов
{/if}

<!-- Меню -->
<div id="right_menu">
	
	<!-- Фильтры -->
	<ul>
		<li {if !$sort || $sort == 'position'}class="selected"{/if}><a href="{url brand_id=null category_id=null keyword=null page=null filter=null sort='position'}">По позиции</a></li>
		<li {if $sort=='byname'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null sort='byname'}">По алфавиту</a></li>
	</ul>
    
    <!-- Категории товаров -->
	{function name=categories_tree level=1}
	{if $categories}
		<ul id="my_list1" class="list-right level-{$level}{if $level > 1} drop{/if}">
			{if $categories[0]->parent_id == 0}
				<li class="{if !$category->id}selected{/if} no-border" style="margin-left:0;padding-left:0;"><a href="{url category_id=null brand_id=null}" style="border:0;">Все категории</a></li>	
			{/if}
			{foreach $categories as $c}
				<li category_id="{$c->id}" class="{if $category->id == $c->id}selected{else}droppable category{/if}{if $c->subcategories} dropdown{/if}">
					<a href='{url keyword=null brand_id=null page=null category_id={$c->id}}'>{$c->name}</a>
					{if $c->subcategories}
						<span class="icon icon-plus"></span>
					{/if}
					{categories_tree categories=$c->subcategories level=$level+1}
				</li>
				
			{/foreach}
		</ul>	 
	{/if}
	{/function}
	{categories_tree categories=$categories}
    
</div>

{literal}
<script>
$(function() {

	// Сортировка списка
	$("#main_list").sortable({
		items:".row",
		handle: ".move_zone",
		tolerance:"pointer",
		scrollSensitivity:40,
		opacity:0.7,
		axis: "y",
		update:function(){
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit();
			colorize();
		}
	});

	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();	
	
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', $('#list input[type="checkbox"][name*="check"]:not(:checked)').length>0);
	});	

	// Удалить
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').attr('selected', true);
		$(this).closest("form").submit();
	});
	
    // Автогенерация мета данных категории
	$("a.meta").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('meta')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'brands', 'id': id, 'values': {'meta_generation': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('meta');
				else
					line.addClass('meta');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    // Автогенерация описания категории
	$("a.description").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('description')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'brands', 'id': id, 'values': {'generate_description': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('description');
				else
					line.addClass('description');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    /*yandex ext*/
    $("a.yandex_c").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
        var state = $(this).data('to_yandex');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'brand_yandex', 'id': id, 'values': {'to_yandex': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
                line.find('a.yandex_c.success_yandex').removeClass('success_yandex');
                line.find('a.yandex_c.fail_yandex').removeClass('fail_yandex');
				if (data == -1) {
                    line.find('a.yandex_c[data-to_yandex="' + state + '"]').addClass('fail_yandex');
                } else if (data) {
                    line.find('a.yandex_c[data-to_yandex="' + state + '"]').addClass('success_yandex');
				} else {
                    line.find('a.yandex_c[data-to_yandex="' + state + '"]').removeClass('success_yandex');
				}
			},
			dataType: 'json'
		});	
		return false;	
	});
    /*/yandex ext*/
    
	// Подтверждение удаления
	$("form").submit(function() {
		if($('#list input[type="checkbox"][name*="check"]:checked').length>0)
			if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
				return false;	
	});
 	
});
</script>
{/literal}
