<!-- jQuery -->
{*<script src="//yandex.st/jquery/1.9.1/jquery.min.js"></script>*}

<!-- arcticModal -->
<script src="design/js/am/jquery.arcticmodal-0.3.min.js"></script>
<link rel="stylesheet" href="design/js/am/jquery.arcticmodal-0.3.css">

<!-- arcticModal theme -->
<link rel="stylesheet" href="design/js/am/themes/simple.css">


{literal}
<script>
function pushit(){
    $('#exampleModal').arcticmodal();
};

$(function(){
    $( "#tabs" ).tabs();
    
    // Добавление нового интервала наценки
	
	$('.add_new_feature').click(function() {
		var ul = $(this).closest('ul');
 console.log(ul);
        var li = $(this).closest('li');
        ul.find('li:last').clone(true).appendTo(ul).fadeIn('slow');//.find("input[name*=new_feature_name]").focus();
        ul.find('li:last .add').hide();
        ul.find('li:last .delete').show();
		return false;		
	});
    
    // Удаление наценки
	$("ul li .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function(){ $(this).remove(); });
		return false;
	});
    
    // Добавление нового интервала наценки на закупку
	
	$('#add_new_interval_s').click(function() {
		$('.marga-settings_stock li:last').clone(true).appendTo('ul.marga-settings_stock').fadeIn('slow');
        $('.marga-settings_stock li:last .add').hide();
        $('.marga-settings_stock li:last .delete').show();
		return false;		
	});
    
    // Удаление наценки на закупку
	$(".marga-settings_stock li .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function(){ $(this).remove(); });
		return false;
	});
});

</script>
{/literal}
<style>
#upload{
margin:30px 200px; padding:15px;
font-weight:bold; font-size:1.3em;
font-family:Arial, Helvetica, sans-serif;
text-align:center;
background:#f2f2f2;
color:#3366cc;
border:1px solid #ccc;
width:150px;
cursor:pointer !important;
-moz-border-radius:5px; -webkit-border-radius:5px;
}
</style>

{* Вкладки *}
{capture name=tabs}
	<li><a href="index.php?module=ParsersImportXMLAdmin">Все парсеры XML</a></li>
	{if ($parser->name)}
	<li class="active"><a href="index.php?module=ParserImportXMLAdmin&id={$parser->id}">{$parser->name|escape}</a></li>
	{else}
	<li class="active"><a href="index.php?module=ParserImportXMLAdmin">Новый парсер XML</a></li>
	{/if}
{/capture}
{if $parser->id}
{$meta_title = $parser->name scope=parent}
{else}
{$meta_title = 'Новый парсер' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}


 

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Парсер цен добавлен{elseif $message_success=='updated'}Парсер обновлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Парсер с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$parser->name|escape}"/> 
		<input name=id type="hidden" value="{$parser->id|escape}"/>
		<div class="checkbox">
			<input name=active value='1' type="checkbox" id="active_checkbox" {if $parser->active}checked{/if}/> <label for="active_checkbox">Активен</label><br>			
		</div>
	</div> 
    
    
    
 	<div>Последний запуск {$parser->lastrun}</div>
    <div><label class=property>Лимит для загрузки:</label> <input class="simpla_inp" type="text" name="limit_parse" value="{$parser->limit_parse}" class=""/> продуктов</div>
	<div id="column_left">
		<div class="block layer">
 
		
			<h2>Параметры парсера</h2>
				<li><label class=property>Комментарии</label><textarea name="description" class="simpla_inp"/>{$parser->description|escape}</textarea></li>
			<hr>
            <div>
              <div id="tabs-2">
                <ul class="xml-parse-settings">
                    <li>
                        <label class=property>Ссылка</label>
                        <input type="text" name="xmlurl" value="{$parser->xmlurl|escape}" class="xmlurl" />
                        <input type="submit" name="get_tags" value="Получить теги" class="save_button"/>
                    </li>
                    <hr/>
    				    <h3>Поля</h3>
                    <hr/>
                    <li>
                        <label class=property><b style="text-decoration: underline;">Продукт</b></label>
                        <select name="xmlproduct" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlproduct}selected{/if}>{$tag}</option>
                            {/foreach}
                        </select> Обязательно!
                    </li>
                    <li>    
                        <label class=property>Артикул</label>
                        <select name="xmlart" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlart}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlart=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlart_attr" class="xml-attr {if $parser->xmlart_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlart_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmlart_xpath" class="xpath_input" type="text" value="{$parser->xmlart_attr}"/>
                    </li>
                    
                    
                    <li>
                        <label class=property>Цена</label>					
                        <select name="xmlprice">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlprice}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlprice=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlprice_attr" class="xml-attr {if $parser->xmlprice_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlprice_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlprice_xpath" class="xpath_input" type="text" value="{$parser->xmlprice_attr}"/>
                    </li>
                    
                    <li>
                        <label class=property>Закупочная цена</label>					
                        <select name="xmlstock_price">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlstock_price}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlstock_price=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlstock_price_attr" class="xml-attr {if $parser->xmlstock_price_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlstock_price_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlstock_price_xpath" class="xpath_input" type="text" value="{$parser->xmlstock_price_attr}"/>
                    </li>
                    
                    <li>
                        <label class=property>Старая цена</label>					
                        <select name="xmlold_price">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlold_price}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlold_price=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlold_price_attr" class="xml-attr {if $parser->xmlold_price_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlold_price_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlold_price_xpath" class="xpath_input" type="text" value="{$parser->xmlold_price_attr}"/>
                    </li>
                    
                    <li>    
                        <label class=property>Type_prefix</label>
                        <select name="xmltypeprefix" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmltypeprefix}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmltypeprefix=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmltypeprefix_attr" class="xml-attr {if $parser->xmltypeprefix_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmltypeprefix_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmltypeprefix_xpath" class="xpath_input" type="text" value="{$parser->xmltypeprefix_attr}"/>
                    </li>
                    
                    <li>    
                        <label class=property>Model/Name</label>
                        <select name="xmlname" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlname}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlname=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlname_attr" class="xml-attr {if $parser->xmlname_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlname_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmlname_xpath" class="xpath_input" type="text" value="{$parser->xmlname_attr}"/>
                    </li>
                    
                    <li>    
                        <label class=property>Бренд</label>
                        <select name="xmlbrand" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlbrand}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlbrand=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlbrand_attr" class="xml-attr {if $parser->xmlbrand_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlbrand_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmlbrand_xpath" class="xpath_input" type="text" value="{$parser->xmlbrand_attr}"/>
                    </li>
                    
                    <li>    
                        <label class=property>Описание</label>
                        <select name="xmldesctiption" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmldesctiption}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmldesctiption=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmldesctiption_attr" class="xml-attr {if $parser->xmldesctiption_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmldesctiption_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmldesctiption_xpath" class="xpath_input" type="text" value="{$parser->xmldesctiption_attr}"/>
                    </li>
                    
                    <li>    
                        <label class=property>Изображение</label>
                        <select name="xmlimage" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlimage}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlimage=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlimage_attr" class="xml-attr {if $parser->xmlimage_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlimage_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmlimage_xpath" class="xpath_input" type="text" value="{$parser->xmlimage_attr}"/>
                    </li>
                    
                    <li>    
                        <label class=property>Дополнительное изображение</label>
                        <select name="xmlimage_2" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlimage_2}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlimage_2=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlimage_2_attr" class="xml-attr {if $parser->xmlimage_2_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlimage_2_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmlimage_2_xpath" class="xpath_input" type="text" value="{$parser->xmlimage_2_attr}"/>
                    </li>
                    
                    <li>
                        <label class=property>Наличие</label>					
                        <select name="xmlstock">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlstock}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlstock=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlstock_attr" class="xml-attr {if $parser->xmlstock_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlstock_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlstock_xpath" class="xpath_input" type="text" value="{$parser->xmlstock_attr}"/>
                    </li>
                    
    				{*<li>
                        <label class=property>Ожидается</label>					
                        <select name="xmlwaiting">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlwaiting}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlwaiting=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlwaiting_attr" class="xml-attr {if $parser->xmlwaiting_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlwaiting_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlwaiting_xpath" class="xpath_input" type="text" value="{$parser->xmlwaiting_attr}"/>
                    </li>*}
                   	
    				<h3>Синонимы</h3>
    			    <hr/>
    				<li>
                        <label class=property>Синоним наличия:</label>					
                        <input name="xmlvnalname" class="simpla_inp" type="text" value="{$parser->xmlvnalname|escape}" />
                    </li>
                    {*<li>
                        <label class=property>Синоним ожидается:</label>					
                        <input name="xmlojiname" class="simpla_inp" type="text" value="{$parser->xmlojiname|escape}" />
                    </li>*}
    				<li>
                        <label class=property>Синоним отсутствия:</label>				
                        <input name="xmlnenalname" class="simpla_inp" type="text" value="{$parser->xmlnenalname|escape}" />
                    </li>
                    <h3>Статические параметры</h3>
                    <hr />
                    <li>
                        <div id="">
                            <label class=property>Картегория: </label>
                			<select name="category_id">
                				<option value='0'>Выбрать категорию</option>
                				{function name=category_select level=0}
                				{foreach from=$cats item=cat}
                					{if $category->id != $cat->id}
                						<option value='{$cat->id}' {if !$parser->category_id && $cat->id == 493}selected{elseif $parser->category_id == $cat->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$cat->name}</option>
                						{category_select cats=$cat->subcategories level=$level+1}
                					{/if}
                				{/foreach}
                				{/function}
                				{category_select cats=$categories}
                			</select>
                	   </div>
                    </li>
                    
                    <li>
                        <div id="" {if !$brands}style='display:none;'{/if}>
                    		<label class=property>Бренд</label>
                    		<select name="brand_id">
                                <option value='0' {if !$parser->brand_id}selected{/if} brand_name=''>Не указан</option>
                           		{foreach from=$brands item=brand}
                                	<option value='{$brand->id}' {if $parser->brand_id == $brand->id}selected{/if} brand_name='{$brand->name|escape}'>{$brand->name|escape}</option>
                            	{/foreach}
                    		</select>
                    	</div>
                    </li>
                    <li> 
            			<label class=property>Поставщик</label>
            			<select name="vendor_id">
            				<option value="">Не указан</option>
            			{foreach $vendors as $ven}
            				<option {if $ven->id == $parser->vendor_id}selected{/if} value="{$ven->id}">{$ven->name}</option>
            			{/foreach}
            			</select>
                        {if $parser->last_parse}<span><b>Последнее обновление: </b>{$parser->last_parse}</span>{/if}
            		</li>
                    <h3>Свойства</h3>
                    <hr />
                    
                    
                    {assign 'xmlfeatures' unserialize($parser->xmlfeatures)}	
                    
                    {if $xmlfeatures}
                        {foreach $xmlfeatures['feature'] as $i=>$f}
                        <li>
                            <label class=property>
                                <select name="xmlfeatures[feature][]" {if count( $xmlfeatures['feature'])>1}required{/if}>
                                    <option value="">Не указан</option>
                        			{foreach $features as $feature}
                       				<option {if $feature->id == $f}selected{/if} value="{$feature->id}">{$feature->name}</option>
                        			{/foreach}
                                </select>
                            </label>
                            
                            <select name="xmlfeatures[tag][]">
                                <option value="0">Выбрать тег</option>
                                {foreach $tags as $tag}
                                <option value="{$tag}" {if $tag == $xmlfeatures['tag'][$i]}selected{/if}>{$tag}</option>
                                {/foreach}
                                <option value="xpath" {if $xmlfeatures['tag'][$i] == 'xpath'}selected{/if}>XPATH</option>
                            </select>
                            
                            <select name="xmlfeatures[attr][]" class="xml-attr {if $xmlfeatures['attr'][$i]}display{/if}">
                                <option value="0">Выбрать атрибут</option>
                                {foreach $attributes as $attribute}
                                <option value="{$attribute}" {if $attribute == $xmlfeatures['attr'][$i]}selected{/if}>{$attribute}</option>
                                {/foreach}
                            </select>
                            <input name="xmlfeatures[xpath][]" class="xpath_input" type="text" value="{$xmlfeatures['xpath'][$i]}"/>
                            <span class="add add_new_feature"{if $f@iteration!=1}style="display:none"{/if}><i class="dash_link">Добавить свойство</i></span>
                            <span {if $f@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                        </li>    
                        {/foreach}
                    {else}
                        <li> 
                			<label class=property>
                                <select name="xmlfeatures[feature][]">
                                    <option value="">Не указан</option>
                        			{foreach $features as $feature}
                       				<option value="{$feature->id}">{$feature->name}</option>
                        			{/foreach}
                                </select>
                            </label>
                			
                            <select name="xmlfeatures[tag][]">
                                <option value="0">Выбрать тег</option>
                                {foreach $tags as $tag}
                                <option value="{$tag}">{$tag}</option>
                                {/foreach}
                                <option value="xpath">XPATH</option>
                            </select>
                            
                            <select name="xmlfeatures[attr][]" class="xml-attr">
                                <option value="0">Выбрать атрибут</option>
                                {foreach $attributes as $attribute}
                                <option value="{$attribute}">{$attribute}</option>
                                {/foreach}
                            </select>
                            <input name="xmlfeatures[xpath][]" class="xpath_input" type="text" value=""/>
                            <span class="add add_new_feature"><i class="dash_link">Добавить интервал</i></span>
                            <span style='display:none;' class="delete"><i class="dash_link">Удалить</i></span>
                		</li>
                    {/if}
                    
                </ul> 
              </div>
            </div>
		</div>
	</div>
	<div id="column_right"> 
        <div class="block layer marga">
            <div class="activator">
                <label class="ios7-switch">           
                    <input type="checkbox" name="active_margin" {if $parser->active_margin}checked="checked"{/if} value="1"/>
                    <span id="check" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
                </label>
                <span style="font-size: 18px;font-weight: 600;">Наценка (+ к цене товара)</span>
            </div>
            
            <div class="margin-body {if $parser->active_margin}display{/if}">
                <ul class="marga-settings">
                    <h3></h3>
                    {assign 'margins' unserialize($parser->margin)}	
                    
                    {if $margins}
                        {foreach $margins['min'] as $i=>$min}
                        <li>
                            От <input name="marja[min][]" class="simpla_inp" type="text" value="{$min}" {if count( $margins['min'])>1}required{/if} /> 
                            до <input name="marja[max][]" class="simpla_inp" type="text" value="{$margins['max'][$i]}" {if count( $margins['min'])>1}required{/if} />
                            добавить <input name="marja[price][]" class="simpla_inp" type="text" value="{$margins['price'][$i]}" />
                            <select name="marja[mode][]">
                                <option value="1" {if $margins['mode'][$i]==1}selected{/if}>руб.</option>
                                <option value="2" {if $margins['mode'][$i]==2}selected{/if}>%</option>
                            </select>
                            <span class="add add_new_feature"{if $min@iteration!=1}style="display:none"{/if}><i class="dash_link">Добавить интервал</i></span>
                            <span {if $min@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                        {/foreach}
                    {else}
                        <li>
                            От <input name="marja[min][]" class="simpla_inp" type="text" value="" /> 
                            до <input name="marja[max][]" class="simpla_inp" type="text" value="" />                                                          добавить <input name="marja[price][]" class="simpla_inp" type="text" value="" />
                            <select name="marja[mode][]">
                                <option value="1">руб.</option>
                                <option value="2">%</option>
                            </select>
                            <span class="add"><i class="dash_link add_new_feature">Добавить интервал</i></span>
                            <span style='display:none;' class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                    {/if}
                </ul>
            </div>
        </div>
        
        {*<div class="block layer stockmarga">
            <div class="activator">
                <label class="ios7-switch">           
                    <input type="checkbox" name="active_stockmargin" {if $parser->active_stockmargin}checked="checked"{/if} value="1"/>
                    <span id="check" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
                </label>
                <span style="font-size: 16px;font-weight: 600;">Закупочная наценка (+ к закупочной цене товара)</span>
            </div>
            
            <div class="margin-body {if $parser->active_stockmargin}display{/if}">
                {*<h2>Закупочная наценка (+ к закупочной цене товара)</h2>*}{*}
                <ul class="marga-settings_stock">
                    {assign 'stockmargins' unserialize($parser->stockmargin)}	
                    
                    {if $stockmargins}
                        {foreach $stockmargins['min'] as $i=>$smin}
                        <li>
                            От <input name="stockmarja[min][]" class="simpla_inp" type="text" value="{$smin}" {if count( $stockmargins['min'])>1}required{/if} /> 
                            до <input name="stockmarja[max][]" class="simpla_inp" type="text" value="{$stockmargins['max'][$i]}" {if count( $stockmargins['min'])>1}required{/if} />
                            добавить <input name="stockmarja[price][]" class="simpla_inp" type="text" value="{$stockmargins['price'][$i]}" />
                            <select name="stockmarja[mode][]">
                                <option value="1" {if $stockmargins['mode'][$i]==1}selected{/if}>руб.</option>
                                <option value="2" {if $stockmargins['mode'][$i]==2}selected{/if}>%</option>
                            </select>
                            <span class="add"{if $smin@iteration!=1}style="display:none"{/if}><i class="dash_link" id="add_new_interval_s">Добавить интервал</i></span>
                            <span {if $smin@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                        {/foreach}
                    {else}
                        <li>
                            От <input name="stockmarja[min][]" class="simpla_inp" type="text" value="" /> 
                            до <input name="stockmarja[max][]" class="simpla_inp" type="text" value="" />                                                          добавить <input name="stockmarja[price][]" class="simpla_inp" type="text" value="" />
                            <select name="stockmarja[mode][]">
                                <option value="1">руб.</option>
                                <option value="2">%</option>
                            </select>
                            <span class="add"><i class="dash_link" id="add_new_interval_s">Добавить интервал</i></span>
                            <span style='display:none;' class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                    {/if}
                </ul>
            </div>        
        </div>	
	</div>*}
	
	 
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
</form>
<form id="go" method="get">
		<input hidden name="module" value="ParsersImportXMLAdmin">
		<input hidden name="goparse" id="chg" value="1">
		<input hidden name="prs" id="chg" value="{$parser->id}">
	<input class="button_green button_save" type="submit" name="" value="Запустить" />
</form>
<script>
$(function(){
    var tags = [{foreach $tags_w_attr as $tag}'{$tag}',{/foreach}];
    var selects = $('.xml-parse-settings select');
    selects.each(function(item, i){
        if($(this).val()=='xpath'){
            $(this).next().hide();
            $(this).siblings('.xpath_input').show();
        }
    });
    $(document).on('change', '.xml-parse-settings select', function(){
        var this_select = $(this);
        if(this_select.val()=='xpath'){
            $(this_select).siblings('.xpath_input').show();
            //console.log($(this_select).siblings('.xpath_input'));
        }else{
            this_select.siblings('.xpath_input').hide();
        }
        console.log(this_select.val());
        if($.inArray(this_select.context.value, tags)==-1){
            this_select.next().find('option:first').attr('selected',1);
            this_select.next().hide();
        }else{
            this_select.next().show();
        }
    });
    
    
    
    $('#tabs').find('div input, select').attr('disabled', 1);
    var checked_tab = $('#tabs li.ui-state-default input:checked').parent().attr('aria-controls');
    
    $('#tabs').find('div#'+checked_tab+' input, div#'+checked_tab+' select').removeAttr('disabled');
    if($('#tabs .xml-active').length){ 
        $( "#tabs" ).tabs("option", "active", 1);
    }
    $('#tabs li.ui-state-default').on('click', function(){
        $(this).find('input').attr('checked', 1);
        $(this).parents('#tabs').find('div input, select').attr('disabled', 1);
        var tabname = $(this).attr('aria-controls');
        $(this).parents('#tabs').find('div#'+tabname+' input, div#'+tabname+' select').removeAttr('disabled');
    });
    
   $('#column_right .activator span#check').click(function(){
        $(this).parents('.block.layer').find('.margin-body').toggle();
    });
   
});
</script>