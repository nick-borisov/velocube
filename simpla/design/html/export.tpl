{capture name=tabs}
	{if in_array('import', $manager->permissions)}<li><a href="index.php?module=ImportAdmin">Импорт</a></li>{/if}
	<li class="active"><a href="index.php?module=ExportAdmin">Экспорт</a></li>
	{if in_array('backup', $manager->permissions)}<li><a href="index.php?module=BackupAdmin">Бекап</a></li>{/if}
{/capture}
{$meta_title='Экспорт товаров' scope=parent}

<script src="{$config->root_url}/simpla/design/js/piecon/piecon.js"></script>
<script>
{literal}
	
var in_process=false;

$(function() {

	// On document load
	$('input#start').click(function() {
 
 		Piecon.setOptions({fallback: 'force'});
 		Piecon.setProgress(0);
    	$("#progressbar").progressbar({ value: 0 });

    	$("#start").hide('fast');
		do_export();
    
	});
    
    $('input#get_ids').click(function() {
        get_ids();    
   	});    
  
	function do_export(page)
	{
		page = typeof(page) != 'undefined' ? page : 1;

		brand_id=$('#brand_id').val()
        vendor_id=$('#vendor_id').val()
		category_id=$('#category_id').val()
		if($('#sub').is(":checked"))
			sub=1
		else
			sub=0
        
		$.ajax({
 			 url: "ajax/export.php?1&brand_id="+brand_id+"&category_id="+category_id+"&sub="+sub,
 			 	data: {page:page, vendor_id: vendor_id},
 			 	dataType: 'json',
  				success: function(data){
  				  //console.log(data);
    				if(data && !data.end)
    				{
    					Piecon.setProgress(Math.round(100*data.page/data.totalpages));
    					$("#progressbar").progressbar({ value: 100*data.page/data.totalpages });
    					do_export(data.page*1+1);
    				}
    				else
    				{	
	    				if(data && data.end)
	    				{
	    					Piecon.setProgress(100);
	    					$("#progressbar").hide('fast');
	    					window.location.href = 'files/export/export.csv?'+Math.random() * (100 - 1) + 1;
    					}
    				}
  				},
				error:function(xhr, status, errorThrown) {
					alert(errorThrown+'\n'+xhr.responseText);
        		}  				
  				
		});
	
	} 
    
    function get_ids(){
        brand_id=$('#brand_id').val()
        vendor_id=$('#vendor_id').val()
		category_id=$('#category_id').val()
		if($('#sub').is(":checked"))
			sub=1
		else
			sub=0
            
        $.ajax({
 			url: "ajax/export.php?1&brand_id="+brand_id+"&category_id="+category_id+"&sub="+sub,
		 	data: {get_ids:1},
		 	dataType: 'json',
			success: function(data){
                window.location.href = 'files/products_ids.txt?'+Math.random() * (1000 - 1) + 1;
			}
        });
            
    }
	
});
{/literal}
</script>

<style>
	.ui-progressbar-value { background-image: url(design/images/progress.gif); background-position:left; border-color: #009ae2;}
	#progressbar{ clear: both; height:29px; }
	#result{ clear: both; width:100%;}
	#download{ display:none;  clear: both; }
</style>


{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>
	{if $message_error == 'no_permission'}Установите права на запись в папку {$export_files_dir}
	{else}{$message_error}{/if}
	</span>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<div>
	<h1>Экспорт товаров</h1>
	{if $message_error != 'no_permission'}
	<div id='progressbar'></div>

	<div id="product_categories">
		<label>Категория</label>
		<div>
			<select name="category_id" ID="category_id">
				<option value='0'>Не указанo</option>
				{function name=category_select level=0}
				{foreach from=$cats item=cat}
						<option value='{$cat->id}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$cat->name}</option>
						{category_select cats=$cat->subcategories level=$level+1}
				{/foreach}
				{/function}
				{category_select cats=$categories}
			</select>
			
			<BR><INPUT TYPE="checkbox" ID="sub" NAME="sub" selected> Включать подкатегории
		</div>
	</div>

	<div id="product_categories">
		<label>Бренд</label>
		<div>
		<select name="brand_id" ID="brand_id">
	            <option value='0' >Не указанo</option>
       		{foreach from=$brands item=brand}
            	<option value='{$brand->id}' brand_name='{$brand->name|escape}'>{$brand->name|escape}</option>
        	{/foreach}
		</select>
		</div>
	</div>
    
    <div id="product_categories">
		<label>Поставщик</label>
		<div>
		<select name="vendor_id" id="vendor_id">
	            <option value='0' >Не указан</option>
       		{foreach $vendors as $vendor}
            	<option value='{$vendor->id}'>{$vendor->name|escape}</option>
        	{/foreach}
		</select>
		</div>
	</div>

	<div style="clear:both"></div>

    <input class="button_green" id="get_ids" type="button" name="" value="Получить ID для K50" />
    


	<input class="button_green" id="start" type="button" name="" value="Экспортировать" />	
	{/if}
</div>
 
