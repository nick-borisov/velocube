<!-- jQuery -->
{*<script src="//yandex.st/jquery/1.9.1/jquery.min.js"></script>*}

<!-- arcticModal -->
<script src="design/js/am/jquery.arcticmodal-0.3.min.js"></script>
<link rel="stylesheet" href="design/js/am/jquery.arcticmodal-0.3.css">

<!-- arcticModal theme -->
<link rel="stylesheet" href="design/js/am/themes/simple.css">


<script src="design/js/am/ajax_upload.js"></script>
{literal}
<script>
function pushit(){
    $('#exampleModal').arcticmodal();
};

$(function(){
    $( "#tabs" ).tabs();
    
    var btnUpload=$('#upload');
    var status=$('#status');
    new AjaxUpload(btnUpload, {
    action: 'ajax/upload_price.php',
    //Имя файлового поля ввода
    name: 'uploadfile',
    onSubmit: function(file, ext){
    if (! (ext && /^(xls|xlsx|csv|txt)$/.test(ext))){
    // Валидация расширений файлов
    status.text('Только Excel файлы');
    return false;
    }
    status.text('Загружаем...');
    },
    
    onComplete: function(file, response){
    //Очищаем текст статуса
    status.text('');
    //Добавляем загруженные файлы в лист
    if(response==="success"){
    $('<li></li>').appendTo('#files').html('<br />'+file).addClass('success');
    } else{
    $('<li></li>').appendTo('#files').text("Error: "+file+" Resp: "+response).addClass('error');
    }
    }
    });
    
    // Добавление нового интервала наценки
	
	$('#add_new_feature').click(function() {
		$('.marga-settings li:last').clone(true).appendTo('ul.marga-settings').fadeIn('slow');//.find("input[name*=new_feature_name]").focus();
        $('.marga-settings li:last .add').hide();
        $('.marga-settings li:last .delete').show();
		return false;		
	});
    
    // Удаление наценки
	$(".marga-settings li .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function(){ $(this).remove(); });
		return false;
	});
    
    // Добавление нового интервала наценки на закупку
	
	$('#add_new_interval_s').click(function() {
		$('.marga-settings_stock li:last').clone(true).appendTo('ul.marga-settings_stock').fadeIn('slow');
        $('.marga-settings_stock li:last .add').hide();
        $('.marga-settings_stock li:last .delete').show();
		return false;		
	});
    
    // Удаление наценки на закупку
	$(".marga-settings_stock li .delete").on('click', function() {
		$(this).closest("li").fadeOut(200, function(){ $(this).remove(); });
		return false;
	});
});

</script>
{/literal}
<style>
#upload{
margin:30px 200px; padding:15px;
font-weight:bold; font-size:1.3em;
font-family:Arial, Helvetica, sans-serif;
text-align:center;
background:#f2f2f2;
color:#3366cc;
border:1px solid #ccc;
width:150px;
cursor:pointer !important;
-moz-border-radius:5px; -webkit-border-radius:5px;
}
</style>

<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close">закрыть</div>
			<div id="upload" >Выбрать файл</div>
			<span id="status" ></span>
			<ul id="files" ></ul>
    </div>
</div>

{* Вкладки *}
{capture name=tabs}
<!--	<li><a href="index.php?module=ProductsAdmin">Товары</a></li> -->
	<li><a href="index.php?module=ParsersAdmin">Все парсеры</a></li>
	{if ($parser->name)}
	<li class="active"><a href="index.php?module=ParserAdmin&id={$parser->id}">{$parser->name|escape}</a></li>
	{else}
	<li class="active"><a href="index.php?module=ParserAdmin">Новый парсер</a></li>
	{/if}
    <li><a href="index.php?module=ParsersImportXMLAdmin">Парсеры XML</a></li>
{/capture}
{if $parser->id}
{$meta_title = $parser->name scope=parent}
{else}
{$meta_title = 'Новый парсер' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}


 

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Парсер цен добавлен{elseif $message_success=='updated'}Парсер обновлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Парсер с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$parser->name|escape}"/> 
		<input name=id type="hidden" value="{$parser->id|escape}"/>
		<div class="checkbox">
			<input name=active value='1' type="checkbox" id="active_checkbox" {if $parser->active}checked{/if}/> <label for="active_checkbox">Активен</label><br>			
		</div>
	</div> 
    
    
    
 	<div>Последний запуск {$parser->lastrun}</div>
	<div id="column_left">
		<div class="block layer">
 
		
			<h2>Параметры парсера</h2>
				<li><label class=property>Комментарии</label><textarea name="description" class="simpla_inp"/>{$parser->description|escape}</textarea></li>
			<hr>
            <div id="tabs">
              <ul>
                <li><a href="#tabs-1">EXCEL Парсер</a><input type="radio" name="parser_type" value="excel" {if $parser->parser_type=='excel' || !$parser->parser_type}checked{/if} style="display:none"/></li>
                <li {if $parser->parser_type=='xml'}class="xml-active"{/if}><a href="#tabs-2">XML Парсер</a><input type="radio" name="parser_type" value="xml" {if $parser->parser_type=='xml'}checked{/if} style="display:none"/></li>
              </ul>
              <div id="tabs-1">
                <ul>
    				<li><label class=property>Файл</label>			
    				<select name="filename">
    					{foreach $pfiles as $file}
    					<option value="{$file}"{if $parser->filename == $file} selected {/if}>{$file}</option>
    					{/foreach}
    				</select>
    				<!-- <input name="filename" class="simpla_inp" type="text" 	value="{$parser->filename|escape}" /> -->
    				</li>
    				<li>
    				<label class=property></label>
    				 	<!-- <a href="{url module=ParserFileAdmin return=$smarty.server.REQUEST_URI}">Добавить файл</a> -->
    					<a href="#" onclick="pushit(); return false;">Загрузить файл</a>
    				</li>
     
    			<hr>
    				<h3>Поля</h3>
    			<hr>
    				<li><label class=property>Артикулы</label>					<input name="art" class="simpla_inp" type="text" 		value="{$parser->art|escape}" /></li>
    				<li><label class=property>Доп.артикул</label>				<input disabled name="art2" class="simpla_inp" type="text" 		value="{$parser->art2|escape}" /></li>
    				<li><label class=property>Обновление цен</label>			<select name="reprice" class="simpla_inp"><option {if !$parser->reprice}selected{/if} value="0">Нет</option><option {if $parser->reprice}selected{/if} value="1">Да</option> </select></li>							
    				<li><label class=property>Закупочные цены</label>			<input name="stockprice" class="simpla_inp" type="text" 		value="{$parser->stockprice|escape}" /></li>
    				<li><label class=property>Цены</label>						<input name="price" class="simpla_inp" type="text" 		value="{$parser->prices|escape}" /></li>
    				<li><label class=property>Обновление наличия</label>		<select name="restock" class="simpla_inp"><option {if !$parser->restock}selected{/if} value="0">Нет</option><option {if $parser->restock}selected{/if} value="1">Да</option> </select></li>							
    				<li><label class=property>Наличие</label>					<input name="nalichie" class="simpla_inp" type="text" 	value="{$parser->instock|escape}" /></li>
    				<li><label class=property>Ожидается</label>					<input name="ojidaetsa" class="simpla_inp" type="text" 	value="{$parser->byrequest|escape}" /></li>
                    {if $parser->id == 22 || $parser->id == 64 || $parser->id == 65}
                    <li><label class=property>Поставщик</label>					<input name="xls_vendor" class="simpla_inp" type="text" 	value="{$parser->xls_vendor|escape}" /></li>
                    {/if}
    			<hr>
    				<h3>Цены</h3>
    			<hr>	
    				<li><label class=property>Курс валюты прайса</label>		<input name="valuta" class="simpla_inp" type="text" 	value="{$parser->val|escape}" /></li>
    				{*<li><label class=property>Наценка (+ к цене товара)</label>		<input name="marja" class="simpla_inp" type="text" 		value="{unserialize($parser->margin)}" /></li>*}
    			<hr>
    				<h3>Синонимы</h3>
    			<hr>
    				<li><label class=property>Синоним наличия:</label>					<input name="vnalname" class="simpla_inp" type="text" 	value="{$parser->instockname|escape}" /></li>
    				<li><label class=property>Синоним отсутствия:</label>				<input name="nenalname" class="simpla_inp" type="text" 	value="{$parser->notinstockname|escape}" /></li>
    			</ul>
              </div>
              <div id="tabs-2">
                <ul class="xml-parse-settings">
                    <li>
                        <label class=property>Ссылка</label>
                        <input type="text" name="xmlurl" value="{$parser->xmlurl|escape}" class="xmlurl" />
                    </li>
                    <hr/>
    				    <h3>Поля</h3>
                    <hr/>
                    <li>
                        <label class=property><b style="text-decoration: underline;">Продукт</b></label>
                        <select name="xmlproduct" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlproduct}selected{/if}>{$tag}</option>
                            {/foreach}
                        </select> Обязательно!
                    </li>
                    <li>    
                        <label class=property>Артикул</label>
                        <select name="xmlart" >
                            <option value="">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlart}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlart=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlart_attr" class="xml-attr {if $parser->xmlart_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlart_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        
                        <input name="xmlart_xpath" class="xpath_input" type="text" value="{$parser->xmlart_attr}"/>
                    </li>
                    <li>
                        <label class="ios7-switch">
                            Обновление цен
                            <input type="checkbox" name="reprice" {if $parser->reprice}checked="checked"{/if} value="1"/>
                            <span style="margin-left: 70px;font-size: 18px;"></span>
                        </label>
                    </li>
                    <li>
                        <label class=property>Закупочные цены</label>			
                        <select name="xmlstockprice">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlstockprice}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlstockprice=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlstockprice_attr" class="xml-attr {if $parser->xmlstockprice_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlstockprice_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlstockprice_xpath" class="xpath_input" type="text" value="{$parser->xmlstockprice_attr}"/>
                    </li>
    				<li>
                        <label class=property>Цены</label>						
                        <select name="xmlprice">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlprice}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlprice=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlprice_attr" class="xml-attr {if $parser->xmlprice_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlprice_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlprice_xpath" class="xpath_input" type="text" value="{$parser->xmlprice_attr}"/>
                    </li>
    				<li>
                        <label class="ios7-switch">
                            Обновление наличия
                            <input type="checkbox" name="restock" {if $parser->restock}checked="checked"{/if} value="1"/>
                            <span style="margin-left: 43px;font-size: 18px;"></span>
                        </label>
                    </li>
                    <li>
                        <label class=property>Наличие</label>					
                        <select name="xmlstock">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlstock}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlstock=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlstock_attr" class="xml-attr {if $parser->xmlstock_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlstock_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlstock_xpath" class="xpath_input" type="text" value="{$parser->xmlstock_attr}"/>
                    </li>
    				<li>
                        <label class=property>Ожидается</label>					
                        <select name="xmlwaiting">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlwaiting}selected{/if}>{$tag}</option>
                            {/foreach}
                            <option value="xpath" {if $parser->xmlwaiting=='xpath'}selected{/if}>XPATH</option>
                        </select>
                        
                        <select name="xmlwaiting_attr" class="xml-attr {if $parser->xmlwaiting_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlwaiting_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                        <input name="xmlwaiting_xpath" class="xpath_input" type="text" value="{$parser->xmlwaiting_attr}"/>
                    </li>
                   	<hr/>
    				    <h3>Цены</h3>
                    <hr/>
                    <li>
                        <label class=property>Валюта прайса</label>		
                        <select name="xmlcurrency">
                            <option value="0">Выбрать тег</option>
                            {foreach $tags as $tag}
                            <option value="{$tag}" {if $tag==$parser->xmlcurrency}selected{/if}>{$tag}</option>
                            {/foreach}
                        </select>
                        
                        <select name="xmlcurrency_attr" class="xml-attr {if $parser->xmlcurrency_attr}display{/if}">
                            <option value="0">Выбрать атрибут</option>
                            {foreach $attributes as $attribute}
                            <option value="{$attribute}" {if $attribute==$parser->xmlcurrency_attr}selected{/if}>{$attribute}</option>
                            {/foreach}
                        </select>
                    </li>
                    <hr/>
    				    <h3>Синонимы</h3>
    			    <hr/>
    				<li>
                        <label class=property>Синоним наличия:</label>					
                        <input name="xmlvnalname" class="simpla_inp" type="text" value="{$parser->xmlvnalname|escape}" />
                    </li>
                    <li>
                        <label class=property>Синоним ожидается:</label>					
                        <input name="xmlojiname" class="simpla_inp" type="text" value="{$parser->xmlojiname|escape}" />
                    </li>
    				<li>
                        <label class=property>Синоним отсутствия:</label>				
                        <input name="xmlnenalname" class="simpla_inp" type="text" value="{$parser->xmlnenalname|escape}" />
                    </li>
                </ul> 
              </div>
            </div>
		</div>
	</div>
	<div id="column_right"> 
        <div class="block layer marga">
            <div class="activator">
                <label class="ios7-switch">           
                    <input type="checkbox" name="active_margin" {if $parser->active_margin}checked="checked"{/if} value="1"/>
                    <span id="check" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
                </label>
                <span style="font-size: 18px;font-weight: 600;">Наценка (+ к цене товара)</span>
            </div>
            
            <div class="margin-body {if $parser->active_margin}display{/if}">
                <ul class="marga-settings">
                    <h3></h3>
                    {assign 'margins' unserialize($parser->margin)}	
                    
                    {if $margins}
                        {foreach $margins['min'] as $i=>$min}
                        <li>
                            От <input name="marja[min][]" class="simpla_inp" type="text" value="{$min}" {if count( $margins['min'])>1}required{/if} /> 
                            до <input name="marja[max][]" class="simpla_inp" type="text" value="{$margins['max'][$i]}" {if count( $margins['min'])>1}required{/if} />
                            добавить <input name="marja[price][]" class="simpla_inp" type="text" value="{$margins['price'][$i]}" />
                            <select name="marja[mode][]">
                                <option value="1" {if $margins['mode'][$i]==1}selected{/if}>руб.</option>
                                <option value="2" {if $margins['mode'][$i]==2}selected{/if}>%</option>
                            </select>
                            <span class="add"{if $min@iteration!=1}style="display:none"{/if}><i class="dash_link" id="add_new_feature">Добавить интервал</i></span>
                            <span {if $min@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                        {/foreach}
                    {else}
                        <li>
                            От <input name="marja[min][]" class="simpla_inp" type="text" value="" /> 
                            до <input name="marja[max][]" class="simpla_inp" type="text" value="" />                                                          добавить <input name="marja[price][]" class="simpla_inp" type="text" value="" />
                            <select name="marja[mode][]">
                                <option value="1">руб.</option>
                                <option value="2">%</option>
                            </select>
                            <span class="add"><i class="dash_link" id="add_new_feature">Добавить интервал</i></span>
                            <span style='display:none;' class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                    {/if}
                </ul>
            </div>
        </div>
        
        <div class="block layer stockmarga">
            <div class="activator">
                <label class="ios7-switch">           
                    <input type="checkbox" name="active_stockmargin" {if $parser->active_stockmargin}checked="checked"{/if} value="1"/>
                    <span id="check" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
                </label>
                <span style="font-size: 16px;font-weight: 600;">Закупочная наценка (+ к закупочной цене товара)</span>
            </div>
            
            <div class="margin-body {if $parser->active_stockmargin}display{/if}">
                {*<h2>Закупочная наценка (+ к закупочной цене товара)</h2>*}
                <ul class="marga-settings_stock">
                    {assign 'stockmargins' unserialize($parser->stockmargin)}	
                    
                    {if $stockmargins}
                        {foreach $stockmargins['min'] as $i=>$smin}
                        <li>
                            От <input name="stockmarja[min][]" class="simpla_inp" type="text" value="{$smin}" {if count( $stockmargins['min'])>1}required{/if} /> 
                            до <input name="stockmarja[max][]" class="simpla_inp" type="text" value="{$stockmargins['max'][$i]}" {if count( $stockmargins['min'])>1}required{/if} />
                            добавить <input name="stockmarja[price][]" class="simpla_inp" type="text" value="{$stockmargins['price'][$i]}" />
                            <select name="stockmarja[mode][]">
                                <option value="1" {if $stockmargins['mode'][$i]==1}selected{/if}>руб.</option>
                                <option value="2" {if $stockmargins['mode'][$i]==2}selected{/if}>%</option>
                            </select>
                            <span class="add"{if $smin@iteration!=1}style="display:none"{/if}><i class="dash_link" id="add_new_interval_s">Добавить интервал</i></span>
                            <span {if $smin@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                        {/foreach}
                    {else}
                        <li>
                            От <input name="stockmarja[min][]" class="simpla_inp" type="text" value="" /> 
                            до <input name="stockmarja[max][]" class="simpla_inp" type="text" value="" />                                                          добавить <input name="stockmarja[price][]" class="simpla_inp" type="text" value="" />
                            <select name="stockmarja[mode][]">
                                <option value="1">руб.</option>
                                <option value="2">%</option>
                            </select>
                            <span class="add"><i class="dash_link" id="add_new_interval_s">Добавить интервал</i></span>
                            <span style='display:none;' class="delete"><i class="dash_link">Удалить</i></span>
                        </li>
                    {/if}
                </ul>
            </div>        
        </div>	
	</div>
	
	 
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
</form>
<form id="go" method="get">
		<input hidden name="module" value="ParsersAdmin">
		<input hidden name="goparse" id="chg" value="1">
		<input hidden name="prs" id="chg" value="{$parser->id}">
	<input class="button_green button_save" type="submit" name="" value="Запустить" />
</form>
<script>
$(function(){
    var tags = [{foreach $tags_w_attr as $tag},'{$tag}'{/foreach}];
    var selects = $('.xml-parse-settings select');
    selects.each(function(item, i){
        if($(this).val()=='xpath'){
            $(this).next().hide();
            $(this).siblings('.xpath_input').show();
        }
    });
    $('.xml-parse-settings select').on('change', function(){
        var this_select = $(this);
        if(this_select.val()=='xpath'){
            $(this_select).siblings('.xpath_input').show();
            //console.log($(this_select).siblings('.xpath_input'));
        }else{
            this_select.siblings('.xpath_input').hide();
        }
        console.log(this_select.val());
        if($.inArray(this_select.context.value, tags)==-1){
            this_select.next().find('option:first').attr('selected',1);
            this_select.next().hide();
        }else{
            this_select.next().show();
        }
    });
    
    
    
    $('#tabs').find('div input, select').attr('disabled', 1);
    var checked_tab = $('#tabs li.ui-state-default input:checked').parent().attr('aria-controls');
    
    $('#tabs').find('div#'+checked_tab+' input, div#'+checked_tab+' select').removeAttr('disabled');
    if($('#tabs .xml-active').length){ 
        $( "#tabs" ).tabs("option", "active", 1);
    }
    $('#tabs li.ui-state-default').on('click', function(){
        $(this).find('input').attr('checked', 1);
        $(this).parents('#tabs').find('div input, select').attr('disabled', 1);
        var tabname = $(this).attr('aria-controls');
        $(this).parents('#tabs').find('div#'+tabname+' input, div#'+tabname+' select').removeAttr('disabled');
    });
    
   $('#column_right .activator span#check').click(function(){
        $(this).parents('.block.layer').find('.margin-body').toggle();
    });
   
});
</script>