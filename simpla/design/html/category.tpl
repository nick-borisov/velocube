{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	<li class="active"><a href="index.php?module=CategoriesAdmin">Категории</a></li>
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{if $category->id}
{$meta_title = $category->name scope=parent}
{else}
{$meta_title = 'Новая категория' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}

{* On document load *}
{literal}
<script src="design/js/jquery/jquery.js"></script>
<script src="design/js/jquery/jquery-ui.min.js"></script>
<script src="design/js/autocomplete/jquery.autocomplete-min.js"></script>
<style>
.autocomplete-w1 { background:url(img/shadow.png) no-repeat bottom right; position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; }
.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:auto; min-width: 300px; overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
.autocomplete .selected { background:#F0F0F0; }
.autocomplete div { padding:2px 5px; white-space:nowrap; }
.autocomplete strong { font-weight:normal; color:#3399FF; }
</style>

<script>
$(function() {


	// Удаление изображений
	$(".images a.delete").click( function() {
		$("input[name='delete_image']").val('1');
		$(this).closest("ul").fadeOut(200, function() { $(this).remove(); });
		return false;
	});

	// Автозаполнение мета-тегов
	meta_title_touched = true;
	meta_keywords_touched = true;
	meta_description_touched = true;
	url_touched = true;
	
	if($('input[name="meta_title"]').val() == generate_meta_title() || $('input[name="meta_title"]').val() == '')
		meta_title_touched = false;
	if($('input[name="meta_keywords"]').val() == generate_meta_keywords() || $('input[name="meta_keywords"]').val() == '')
		meta_keywords_touched = false;
	if($('textarea[name="meta_description"]').val() == generate_meta_description() || $('textarea[name="meta_description"]').val() == '')
		meta_description_touched = false;
	if($('input[name="url"]').val() == generate_url() || $('input[name="url"]').val() == '')
		url_touched = false;
		
	$('input[name="meta_title"]').change(function() { meta_title_touched = true; });
	$('input[name="meta_keywords"]').change(function() { meta_keywords_touched = true; });
	$('textarea[name="meta_description"]').change(function() { meta_description_touched = true; });
	$('input[name="url"]').change(function() { url_touched = true; });
	
	$('input[name="name"]').keyup(function() { set_meta(); });
	  
});

function set_meta()
{
	/*if(!meta_title_touched)
		$('input[name="meta_title"]').val(generate_meta_title());
	if(!meta_keywords_touched)
		$('input[name="meta_keywords"]').val(generate_meta_keywords());
	if(!meta_description_touched)
		$('textarea[name="meta_description"]').val(generate_meta_description());*/
	if(!url_touched)
		$('input[name="url"]').val(generate_url());
}

function generate_meta_title()
{
	name = $('input[name="name"]').val();
	return name;
}

function generate_meta_keywords()
{
	name = $('input[name="name"]').val();
	return name;
}

function generate_meta_description()
{
	if(typeof(tinyMCE.get("description")) =='object')
	{
		description = tinyMCE.get("description").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ").replace(/^\s+|\s+$/g, '').substr(0, 512);
		return description;
	}
	else
		return $('textarea[name=description]').val().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ").replace(/^\s+|\s+$/g, '').substr(0, 512);
}

function generate_url()
{
	url = $('input[name="name"]').val();
	url = url.replace(/[\s]+/gi, '-');
	url = translit(url);
	url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();	
	return url;
}

function translit(str)
{
	var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")   
	var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")   
 	var res = '';
	for(var i=0, l=str.length; i<l; i++)
	{ 
		var s = str.charAt(i), n = ru.indexOf(s); 
		if(n >= 0) { res += en[n]; } 
		else { res += s; } 
    } 
    return res;  
}
</script>
 
{/literal}


{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Категория добавлена{elseif $message_success=='updated'}Категория обновлена{else}{$message_success}{/if}</span>
	<a class="link" target="_blank" href="../catalog/{$category->url}">Открыть категорию на сайте</a>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Категория с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$category->name|escape}"/> 
		<input name=id type="hidden" value="{$category->id|escape}"/> 
		<div class="checkbox">
			<input name=visible value='1' type="checkbox" id="active_checkbox" {if $category->visible}checked{/if}/> <label for="active_checkbox">Активна</label>
		</div>
		<div class="checkbox">
			<input name=metka value='1' type="checkbox" id="active_checkbox" {if $category->metka}checked{/if}/> <label for="active_checkbox">Метка</label>
		</div>
        <div class="checkbox">
			<input name=meta_generation value='1' type="checkbox" id="meta_generation" {if $category->meta_generation}checked{/if}/> <label for="meta_generation">Генерация мета</label>
        </div>    
        <div class="checkbox">
			<input name=hide_products value='1' type="checkbox" id="hide_products" {if $category->hide_products}checked{/if}/> <label for="hide_products">Не отображать продукты</label>
		</div>
	</div> 

	<div id="product_categories">
			<select name="parent_id">
				<option value='0'>Корневая категория</option>
				{function name=category_select level=0}
				{foreach from=$cats item=cat}
					{if $category->id != $cat->id}
						<option value='{$cat->id}' {if $category->parent_id == $cat->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$cat->name}</option>
						{category_select cats=$cat->subcategories level=$level+1}
					{/if}
				{/foreach}
				{/function}
				{category_select cats=$categories}
			</select>
	</div>
		
	<!-- Левая колонка свойств товара -->
	<div id="column_left">
			
		<!-- Параметры страницы -->
		<div class="block layer">
			<h2>Параметры страницы</h2>
			<ul>
				<li><label class=property>Имя в меню</label><input name="short_name" class="page_url" type="text" value="{$category->short_name|escape}" /></li>
                <li><label class=property>Имя для Яндекса</label><input name="market_category" class="page_url" type="text" value="{$category->market_category|escape}" /></li>
				<li><label class=property>Адрес</label><div class="page_url">/catalog/</div><input name="url" class="page_url" type="text" value="{$category->url|escape}" /></li>
				<li><label class=property>Заголовок</label><input name="meta_title" class="simpla_inp" type="text" value="{$category->meta_title|escape}" /></li>
				<li><label class=property>Ключевые слова</label><input name="meta_keywords" class="simpla_inp" type="text" value="{$category->meta_keywords|escape}" /></li>
				<li><label class=property>Описание</label><textarea name="meta_description" class="simpla_inp" />{$category->meta_description|escape}</textarea></li>
                <li><label class=property>Стоимость доставки</label><input class="set_interval" name="delivery_price" class="simpla_inp" type="number" value="{$category->delivery_price|escape}" min="100" step="10" /> руб. 
                <input type="checkbox" name="pricetochild" value="1" /><label>Применить для всех подкатегорий</label>
                </li>
			</ul>
<!--SEO-->	<h2 style="font-size:18px;">Список тегов используемые в заголовках и в описании для footer'а: <a href="#" id="meta_generate" title="Сгенерировать автоматически мета-данные и описание для футера"><img src="design/images/wand.png" alt="Сгенерировать автоматически мета-данные и описание для футера" title="Сгенерировать автоматически мета-данные и описание для футера"></a></h2>
<!--SEO-->	<ul>
<!--SEO-->		<li><label class=property>{literal}{skl}{/literal}</label>   <input name="skl"    class="simpla_inp" type="text" value="{$category->skl['skl']|escape}" />
<!--SEO-->			<label class=property>&nbsp;</label><div>Пример: <i>купить фотоаппарат</i></div></li>
<!--SEO-->		<li><label class=property>{literal}{skl_mn}{/literal}</label><input name="skl_mn" class="simpla_inp" type="text" value="{$category->skl['skl_mn']|escape}" />
<!--SEO-->			<label class=property>&nbsp;</label><div>Пример: <i>купить любые фотоаппараты</i></div></li>
<!--SEO-->		<li><label class=property>{literal}{skl_ov}{/literal}</label><input name="skl_ov" class="simpla_inp" type="text" value="{$category->skl['skl_ov']|escape}" />
<!--SEO-->			<label class=property>&nbsp;</label><div>Пример: <i>у нас склад фотоаппаратов</i></div></li>
<!--SEO-->	</ul>
{literal}
<script>
$(function(){
	$('#meta_generate').click(function() {
		
		$('#meta_generate img').attr('src', 'design/images/loader.gif');
		var error = false;
		var title = $('input[name=meta_title]');
		var keywords = $('input[name=meta_keywords]');
		var description = $('textarea[name=meta_description]');
		var skl = $('input[name=skl]');
		var skl_mn = $('input[name=skl_mn]');
		var skl_ov = $('input[name=skl_ov]');
		var description_footer = $('textarea[name=description_footer]');
		//alert(title.val().length);
		if(title.val() == '' || title.val().length < 2)
			var error = new Array('Введите в поле "Заголовок" название не менее 2 символов',title);
		if(skl.val() == '' || skl.val().length < 2)
			var error = new Array('Введите в поле "{skl}" название категории в единственном числе. Не менее 2 символов.',skl);
		if(skl_mn.val() == '' || skl_mn.val().length < 2)
			var error = new Array('Введите в поле "{skl_mn}" название категории в множественном числе. Не менее 2 символов.',skl_mn);
		if(skl_ov.val() == '' || skl_ov.val().length < 2)
			var error = new Array('Введите в поле "{skl_ov}" название категории в единственном числе. Не менее 2 символов.',skl_ov);
		
		if(error != false){
			alert(error[0]);
			error[1].focus();
			$('#meta_generate img').attr('src', 'design/images/wand.png');
			return false;
		}

		$.ajax({
 			 url: "ajax/seo.php",
 			 	data: {action:'category', cat_id: $('#name input[name="id"]').val(), cat_skl: skl.val(), cat_skl_mn:skl_mn.val(),cat_skl_ov:skl_ov.val()},
 			 	dataType: 'json',
  				success: function(data){
					title.val(data['title']);
					description.val(data['description']);
					keywords.val(data['keywords']);
					tinyMCE.get('description_footer').setContent(data['description_footer']);
					$(document).scrollTop( 0 );
					title.focus();
					title.val(title.val());
  				}
		});
		$('#meta_generate img').attr('src', 'design/images/wand.png');
		return false;
	});
    
    $('#desc_generate').click(function() {
		$('#desc_generate img').attr('src', 'design/images/loader.gif');
        var error = false;
        
		var skl = $('input[name=skl]');
		var skl_mn = $('input[name=skl_mn]');
		var skl_ov = $('input[name=skl_ov]');
		
		if(skl.val() == '' || skl.val().length < 2)
			var error = new Array('Введите в поле "{skl}" название категории в единственном числе. Не менее 2 символов.',skl);
		if(skl_mn.val() == '' || skl_mn.val().length < 2)
			var error = new Array('Введите в поле "{skl_mn}" название категории в множественном числе. Не менее 2 символов.',skl_mn);
		if(skl_ov.val() == '' || skl_ov.val().length < 2)
			var error = new Array('Введите в поле "{skl_ov}" название категории в единственном числе. Не менее 2 символов.',skl_ov);
		
		if(error != false){
			alert(error[0]);
			error[1].focus();
			$('#desc_generate img').attr('src', 'design/images/wand.png');
			return false;
		}
		$.ajax({
 			 url: "ajax/seo.php",
 			 	data: {action:'category_description', cat_id: $('#name input[name="id"]').val(), cat_skl: skl.val(), cat_skl_mn:skl_mn.val(),cat_skl_ov:skl_ov.val()},
 			 	dataType: 'json',
                cache: false,
  				success: function(data){

					tinyMCE.get('description').setContent(data['description']);
					//$(document).scrollTop( 0 );
					
  				}
		});
		$('#desc_generate img').attr('src', 'design/images/wand.png');
		return false;
	});
});    
</script>
{/literal}
		</div>
		<!-- Параметры страницы (The End)-->
		
 		{*
		<!-- Экспорт-->
		<div class="block">
			<h2>Экспорт товара</h2>
			<ul>
				<li><input id="exp_yad" type="checkbox" /> <label for="exp_yad">Яндекс Маркет</label> Бид <input class="simpla_inp" type="" name="" value="12" /> руб.</li>
				<li><input id="exp_goog" type="checkbox" /> <label for="exp_goog">Google Base</label> </li>
			</ul>
		</div>
		<!-- Свойства товара (The End)-->
		*}
			
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right">
		
        <div class="block layer costs">
            <div id="product_categories_lowcost">
                <label>Меньше этой суммы товары автоматически попадают в эту катерогию</label>
                <input type="number" name="lowcost_limit" class="lowcost" value="{$category->lowcost_limit}" min="0" />
    			<select name="lowcost_category">
    				<option value='0'>Выбрать категорию</option>
    				{function name=category_select_lowcost level=0}
    				{foreach from=$cats item=cat}	
    						<option value='{$cat->id}' {if $category->lowcost_category == $cat->id}selected{/if}>{section name=spl loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$cat->name}</option>
    						{category_select_lowcost cats=$cat->subcategories level=$level+1}
    				{/foreach}
    				{/function}
    				{category_select_lowcost cats=$categories}
    			</select>
    	   </div>
           <br /><br />
           <div id="product_categories_highcost">
                <label>Больше этой суммы товары автоматически попадают в эту катерогию</label>
                <input type="number" name="highcost_limit" class="highcost" value="{$category->highcost_limit}" min="0" />
    			<select name="highcost_category">
    				<option value='0'>Выбрать категорию</option>
    				{function name=category_select_highcost level=0}
    				{foreach from=$cats item=cat}	
    						<option value='{$cat->id}' {if $category->highcost_category == $cat->id}selected{/if}>{section name=sph loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$cat->name}</option>
    						{category_select_highcost cats=$cat->subcategories level=$level+1}
    				{/foreach}
    				{/function}
    				{category_select_highcost cats=$categories}
    			</select>
    	   </div>
           
           
           <br /><br />
           <h3>Категория-Распродажа</h3>
           <div id="product_categories_sale">
                <label>При добавлении старой цены товар автоматически попадает в эту катерогию</label>
    			<select name="sale_category">
    				<option value='0'>Выбрать категорию</option>
    				{function name=category_select_sale level=0}
    				{foreach from=$cats item=cat}	
    						<option value='{$cat->id}' {if $category->sale_category == $cat->id}selected{/if}>{section name=sph loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$cat->name}</option>
    						{category_select_sale cats=$cat->subcategories level=$level+1}
    				{/foreach}
    				{/function}
    				{category_select_sale cats=$categories}
    			</select>
    	   </div>
        </div>
        
        <div class="block layer">
            <h2>Фильтр для категории</h2><a href="{url module=FilterAdmin id=null return=$smarty.server.REQUEST_URI}" target="_blank" class="link">Создать новый</a>
            <select name="filter_id" class="form-control">
                <option value="0">Не выбран</option>
                {foreach $filters as $filter}
                <option value="{$filter->id}" {if $category->filter_id==$filter->id}selected{/if}>{$filter->name}</option>    
                {/foreach}
            </select>
        </div>
        	
		<!-- Изображение категории -->	
		<div class="block layer images">
			<h2>Изображение категории</h2>
			<input class='upload_image' name=image type=file>			
			<input type=hidden name="delete_image" value="">
			{if $category->image}
			<ul>
				<li>
					<a href='#' class="delete"><img src='design/images/cross-circle-frame.png'></a>
					<img src="../{$config->categories_images_dir}{$category->image}" alt="" />
				</li>
			</ul>
			{/if}
		</div>
	</div>
	<!-- Правая колонка свойств товара (The End)--> 

	<!-- Описагние категории -->
	<div class="block layer">
		<h2>Описание <a href="#" id="desc_generate" title="Сгенерировать автоматически описание"><img src="design/images/wand.png" alt="Сгенерировать автоматически описание" title="Сгенерировать автоматически описание"></a></h2><input id="generate_description" type="checkbox" name="generate_description" value="1" {if $category->generate_description}checked{/if} /><label for="generate_description">Генерировать из сео модуля</label>
		<textarea name="description" class="editor_large">{$category->description|escape}</textarea>
	</div>
	<!-- Описание категории (The End)-->
{*
<!--SEO--><!-- Описание Футер категории -->
<!--SEO--><div class="block layer">
<!--SEO-->		<h2>Описание для Footer`a</h2>
<!--SEO-->		<textarea name="description_footer" class="editor_large" id="description_footer">{$category->description_footer|escape}</textarea>
<!--SEO--></div>
<!--SEO--><!-- Описание футер категории (The End)-->
*}	
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	
</form>
<!-- Основная форма (The End) -->

