{* Вкладки *}
{capture name=tabs}
	<li class="active"><a href="index.php?module=ExportsAdmin">Все каналы</a></li>
 
{/capture}

{* Title *}
{$meta_title='Каналы экспорта' scope=parent}

{* Заголовок *}
<div id="header">
	<h1>Каналы экспорта</h1>
	<a class="add" href="{url module=ExportsChannelAdmin return=$smarty.server.REQUEST_URI}">Добавить канал</a>
 
</div>	
<!-- Заголовок (The End) -->

{if $exports}
<div id="main_list" class="categories">

	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
 
		{if $exports}
		<div id="list" class="sortable">
		
			{foreach $exports as $export}
			
		 
			
			<div class="{if !$export->active}invisible{/if} row {$export->color}">		
				<div class="tree_row">
					<input type="hidden" name="positions[{$export->id}]" value="{$export->position}">
					<!-- <div class="move cell" style="margin-left:{$level*20}px"><div class="move_zone"></div></div> -->
			 		<div class="checkbox cell">
						<input hidden type="checkbox" name="check[]" value="{$export->id}" />				
					</div>
					<div class="cellp">
						<a href="{url module=ExportsChannelAdmin id=$export->id return=$smarty.server.REQUEST_URI}">{$export->name|escape}</a> 	 			
					</div>					
 
					<div class="icons cell">					
						<a class="enable" title="Активна" href="#"></a>
						<a class="delete" title="Удалить" href="#"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			{/foreach}
	
		</div>
		{/if}
 
 		<span id="select">
		<select hidden name="action">
			<option value="enable">Сделать видимыми</option>
			<option value="disable">Сделать невидимыми</option>
			<option value="delete">Удалить</option>
		</select>
		<input hidden id="apply_action" class="button_green" type="submit" value="Применить">
		</form>
 
</div>
{else}
Нет каналов
{/if}
 

{literal}
<script>
$(function() {

 
 
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list input[type="checkbox"][name*="check"]').attr('checked'));
	});	

 

	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').attr('selected', true);
		$(this).closest("form").submit();
	});
	
		// включить 
	$("a.enable").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=disable]').attr('selected', true);
		$(this).closest("form").submit();
	});
	
			// !включить 
	$("a.disable").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=enable]').attr('selected', true);
		$(this).closest("form").submit();
	});

	
	// Подтвердить удаление
	$("form").submit(function() {
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});

});
</script>
{/literal}