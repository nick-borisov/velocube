{* Вкладки *}
{capture name=tabs}
	{if in_array('orders', $manager->permissions)}
	<li><a href="index.php?module=OrdersAdmin&status=all">Все</a></li>
		<li {if $order->status==0}class="active"{/if}><a href="index.php?module=OrdersAdmin&status=0">Новые</a></li>
		<li {if $order->status==1}class="active"{/if}><a href="index.php?module=OrdersAdmin&status=1">Приняты</a></li>
		<li {if $order->status==2}class="active"{/if}><a href="index.php?module=OrdersAdmin&status=2">Выполнены</a></li>
		<li {if $order->status==4}class="active"{/if}><a href="index.php?module=OrdersAdmin&status=4">Отменены</a></li>
		<li {if $order->status==5}class="active"{/if}><a href="index.php?module=OrdersAdmin&status=5">Проблемы</a></li>
		<li {if $order->status==3}class="active"{/if}><a href="index.php?module=OrdersAdmin&status=3">Удалены</a></li>
	{if $keyword}
	<li class="active"><a href="{url module=OrdersAdmin keyword=$keyword id=null label=null}">Поиск</a></li>
	{/if}
	{/if}
	{if in_array('labels', $manager->permissions)}
	<li><a href="{url module=OrdersLabelsAdmin keyword=null id=null page=null label=null}">Метки</a></li>
	{/if}
{/capture}

{* On document load *}
{literal}
<script src="design/js/jquery/datepicker/jquery.ui.datepicker-ru.js"></script>

<script>
$(function() {

	$('input[name="delivery_date"]').datepicker({
		regional:'ru'
	});
});
</script>
{/literal}	

{if $order->id}
{$meta_title = "Заказ №`$order->id`" scope=parent}
{else}
{$meta_title = 'Новый заказ' scope=parent}
{/if}

<!-- Основная форма -->
<form method=post id=order enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">

<div id="name">
	<input name=id type="hidden" value="{$order->id|escape}"/> 
	<h1>{if $order->id}Заказ №{$order->id|escape}{else}Новый заказ{/if}{if $order->smartbikes} Smart-bikes.ru{/if}
	<select class=status name="status">
		<option value='0' {if $order->status == 0}selected{/if}>Новый</option>
		<option value='1' {if $order->status == 1}selected{/if}>Принят</option>
		<option value='2' {if $order->status == 2}selected{/if}>Выполнен</option>
		<option value='4' {if $order->status == 4}selected{/if}>Отменен</option>
		<option value='5' {if $order->status == 5}selected{/if}>Проблемный</option>
		<option value='3' {if $order->status == 3}selected{/if}>Удален</option>
	</select>
	</h1>
	<a href="{url view=print id=$order->id}" target="_blank"><img src="./design/images/printer.png" name="export" title="Печать заказа"></a>
    <a title="Предпросмотр в новом окне" href="../order/{$order->url}" target="_blank"><img src="./design/images/monitor26.png" width="32px" title="Предпросмотр в новом окне"></a>

	<div id=next_order>
        
		{if $prev_order}
		<a class=prev_order href="{url id=$prev_order->id}">←</a>
		{/if}
		{if $next_order}
		<a class=next_order href="{url id=$next_order->id}">→</a>
		{/if}
	</div>
		
</div> 


{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='error_closing'}Нехватка товара на складе{else}{$message_error|escape}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{elseif $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='updated'}Заказ обновлен{elseif $message_success=='added'}Заказ добавлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}



<div id="order_details">
	<h2>Детали заказа <a href='#' class="edit_order_details"><img src='design/images/pencil.png' alt='Редактировать' title='Редактировать'></a></h2>
	
	<div id="user">
	<ul class="order_details">
		<li>
			<label class=property>IP</label>
			<div class="edit_order_detail view_order_detail">
			{$order->ip}
			</div>
		</li>
		<li>
			<label class=property>Дата</label>
			<div class="edit_order_detail view_order_detail">
			{$order->date} {$order->time}
			</div>
		</li>
		<li>
			<label class=property>Имя</label> 
			<div class="edit_order_detail" style='display:none;'>
				<input name="name" class="simpla_inp" type="text" value="{$order->name|escape}" />
			</div>
			<div class="view_order_detail">
				{$order->name|escape}
			</div>
		</li>
		<li>
			<label class=property>Паспортные данные</label> 
			<div class="edit_order_detail" style='display:none;'>
				<input name="passport" class="simpla_inp" type="text" value="{$order->passport|escape}" />
			</div>
			<div class="view_order_detail">
				{$order->passport|escape}
			</div>
		</li>
		<li>
			<label class=property>Email</label>
			<div class="edit_order_detail" style='display:none;'>
				<input name="email" class="simpla_inp" type="text" value="{$order->email|escape}" />
			</div>
			<div class="view_order_detail">
				<a href="mailto:{$order->email|escape}?subject=Заказ%20№{$order->id}">{$order->email|escape}</a>
			</div>
		</li>
		<li>
			{if $order->email2}
				<label class=property>Email2</label>
				<div class="edit_order_detail" style='display:none;'>
					<input name="email2" class="simpla_inp" type="text" value="{$order->email2|escape}" />
				</div>
				<div class="view_order_detail">
				{if $order->email2}
					<a href="mailto:{$order->email2|escape}?subject=Заказ%20№{$order->id}">{$order->email2|escape}</a>
				{/if}
				</div>
			{else}
				<label class="edit_order_detail property js-email2" style='display:none; margin-bottom: 10px'>Email2</label>
				<div class="edit_order_detail_email" style='display:none;'>
					<input name="email2" class="simpla_inp" type="text" value="{$order->email2|escape}" />
				</div>
			{/if}
		</li>
		<li>
			<label class=property>Телефон</label>
			<div class="edit_order_detail" style='display:none;'>
				<input name="phone" class="simpla_inp " type="text" value="{$order->phone|escape}" />
			</div>
			<div class="view_order_detail">
				{if $order->phone}
				<span class="ip_call" data-phone="+{$order->phone|escape}" target="_blank">+{$order->phone|escape}</span>{else}+{$order->phone|escape}{/if}
			</div>
			{if $order->phone2}
				<label class="property js-phone2">Телефон2</label>
                <div class="edit_order_detail" style='display:none;'>
    				<input name="phone2" class="simpla_inp " type="text" value="{$order->phone2|escape}" />
    			</div>
    			<div class="view_order_detail">
    				{if $order->phone2}
    				<span class="ip_call" data-phone="+{$order->phone2|escape}" target="_blank">+{$order->phone2|escape}</span>{else}+{$order->phone2|escape}{/if}
    			</div>
    		{else}
    			<label class="edit_order_detail property js-phone2" style='display:none; margin-bottom: 10px'>Телефон2</label>
    			<div class="edit_order_detail_phone " style='display:none;'>
    				<input name="phone2" class="simpla_inp " type="text" value="{$order->phone2|escape}" />
    			</div>
            {/if}
		</li>
		<li>
			<label class=property>Адрес <a href='http://maps.yandex.ru/' id=address_link target=_blank><img align=absmiddle src='design/images/map.png' alt='Карта в новом окне' title='Карта в новом окне'></a></label>
			<div class="edit_order_detail" style='display:none;'>
				<textarea name="address">{$order->address|escape}</textarea>
			</div>
			<div class="view_order_detail">
				{$order->address|escape}
			</div>
		</li>
		<li>
			<label class=property>Комментарий пользователя</label>
			<div class="edit_order_detail" style='display:none;'>
			<textarea name="comment">{$order->comment|escape}</textarea>
			</div>
			<div class="view_order_detail">
				{$order->comment|escape|nl2br}
			</div>
		</li>
		<li>
			<label class=property>Дата доставки</label>
			<div class="edit_order_detail" style='display:none;'>
				<input name="delivery_date" class="simpla_inp " type="text" value="{$order->delivery_date|escape}" />
			</div>
			<div class="view_order_detail">
			{if $order->delivery_date && $order->delivery_date != '0000-00-00' && $order->delivery_date != '1970-01-01'}
				{$order->delivery_date|escape}
				{/if}
			</div>
		</li>
		<li>
			<label class=property>Время доставки</label>
			<div class="edit_order_detail" style='display:none;'>
				<input name="delivery_time" class="simpla_inp " type="text" value="{$order->delivery_time|escape}" />
			</div>
			<div class="view_order_detail">
				{$order->delivery_time|escape}
			</div>
		</li>
		
		<li>
			<label class=property>Трек-номер</label>
			<div class="edit_order_detail" style='display:none;'>
				<input name="trnomer" class="simpla_inp " type="text" value="{$order->trnomer|escape}" />
			</div>
			<div class="view_order_detail">
				{$order->trnomer|escape}
			</div>
		</li>
		
		
		<li>
			<label class=property>Менеджер</label> 
			<div class="edit_order_detail">
		<select name="manager">
            <option value='' >Не указан</option>
       		{foreach $managers as $m}
            	{if $m->name}<option value='{$m->name|escape}' {if $m->name == $order->manager}selected{/if} >{$m->name|escape}</option>
                {else}
                <option value='{$m->login|escape}' {if $m->login == $order->manager}selected{/if} >{$m->login|escape}</option>
                {/if}
        	{/foreach}
		</select>
			</div>
		</li>						
	</ul>
	</div>

	
	{if $labels}
	<div class='layer'>
	<h2>Метка</h2>
	<!-- Метки -->
	<ul>
		{foreach $labels as $l}
		<li>
		<label for="label_{$l->id}">
		<input id="label_{$l->id}" type="checkbox" name="order_labels[]" value="{$l->id}" {if in_array($l->id, $order_labels)}checked{/if}>
		<span style="background-color:#{$l->color};" class="order_label"></span>
		{$l->name}
		</label>
		</li>
		{/foreach}
	</ul>
	<!-- Метки -->
	</div>
	{/if}

	
	<div class='layer'>
	<h2>Покупатель <a href='#' class="edit_user"><img src='design/images/pencil.png' alt='Редактировать' title='Редактировать'></a> {if $user}<a href="#" class='delete_user'><img src='design/images/delete.png' alt='Удалить' title='Удалить'></a>{/if}</h2>
		<div class='view_user'>
		{if !$user}
			Не зарегистрирован
		{else}
			<a href='index.php?module=UserAdmin&id={$user->id}' target=_blank>{$user->name|escape}</a> ({$user->email|escape})
		{/if}
		</div>
		<div class='edit_user' style='display:none;'>
		<input type=hidden name=user_id value='{$user->id}'>
		<input type=text id='user' class="input_autocomplete" placeholder="Выберите пользователя">
		</div>
	</div>
	

	
	<div class='layer'>
	<h2>Примечание <a href='#' class="edit_note"><img src='design/images/pencil.png' alt='Редактировать' title='Редактировать'></a></h2>
	<ul class="order_details">
		<li>
			<div class="edit_note" style='display:none;'>
				<label class=property>Ваше примечание (не видно пользователю)</label>
				<textarea name="note">{$order->note|escape}</textarea>
			</div>
			<div class="view_note" {if !$order->note}style='display:none;'{/if}>
				<label class=property>Ваше примечание (не видно пользователю)</label>
				<div class="note_text">{$order->note|escape}</div>
			</div>
		</li>
	</ul>
	</div>
		
</div>


<div id="purchases">
 
	<div id="list" class="purchases">
		{foreach from=$purchases item=purchase}
        
		<div class="row">
			<div class="image cell">
				<input type=hidden name=purchases[id][{$purchase->id}] value='{$purchase->id}'>
                {if $purchase->image}
                    <img class=product_icon src='{$purchase->image->filename|resize:35:35}'>
                {/if}

            </div>
			<div class="purchase_name cell">
                {if $purchase->product}
				<a class="related_product_name" href="index.php?module=ProductAdmin&id={$purchase->product->id}&return={$smarty.server.REQUEST_URI|urlencode}">{$purchase->product_name}</a>
				{else}
				{$purchase->product_name}				
				{/if}
				<div class='purchase_variant'>				
				<span class=edit_purchase style='display:none;'>
				<select name=purchases[variant_id][{$purchase->id}] {if $purchase->product->variants|count==1 && $purchase->variant_name == '' && $purchase->variant->sku == ''}style='display:none;'{/if}>					
		    	{if !$purchase->variant}<option price='{$purchase->price}' amount='{$purchase->amount}' value=''>{$purchase->variant_name|escape} {if $purchase->sku}(арт. {$purchase->sku}){/if}</option>{/if}
				{foreach $purchase->product->variants as $v}
					{if $v->stock>0 || $v->id == $purchase->variant->id}
					<option {if $v->sku}sku='{$v->sku}'{/if} price='{$v->price}' amount='{$v->stock}' value='{$v->id}' {if $v->id == $purchase->variant_id}selected{/if} >
					{$v->name}
					{if $v->sku}(арт. {$v->sku}){/if}
					</option>
					{/if}

				{/foreach}
				</select>
                {if $v->sku}<input type="hidden" name="purchases[sku][{$purchase->id}]" value="{$v->sku}" />{/if}
                {if !$purchase->auto_coupon && $purchase->apply_auto_coupon===null}
                    <input id="apply_auto_coupon" type="checkbox" name="purchases[apply_auto_coupon][{$purchase->id}]" value="1" />
                    <label for="apply_auto_coupon">Применить скидку (автокупон)</label>
                {/if}
				</span>
				<span class=view_purchase>
					{$purchase->variant_name} {if $purchase->sku}(арт. {$purchase->sku}){/if}			
				</span>
                {if $purchase->auto_coupon}
                <span> Автокупон 
                <b>{$coupons[$purchase->auto_coupon]->name}</b>
                {if $coupons[$purchase->auto_coupon]->active_margin}
                    {$margins = unserialize($coupons[$purchase->auto_coupon]->margin)}
                    {foreach $margins as $margin}
                        {if $margins['min'][0] && $margins['max'][0]}  
                            {foreach $margins['min'] as $i=>$min}
                                {if $purchase->variant->price > intval($margins['min'][$i]) && $purchase->variant->price < intval($margins['max'][$i])}
                                    {if intval($margins['mode'][$i])==1}
                                        {$autodiscount = $margins['price'][$i]}
                                        {$autodiscount_type = 'absolute'}
                                    {elseif intval($margins['mode'][$i])==2}
                                        {$autodiscount = $margins['price'][$i]}
                                        {$autodiscount_type = 'percentage'}
                                    {/if}
                                {/if}
                            {/foreach}
                        {/if}
                    {/foreach}
                    
                {else}
                    {$autodiscount = $coupons[$purchase->auto_coupon]->value}
                    {if $coupons[$purchase->auto_coupon]->type=='percentage'}
                        {$autodiscount_type = 'percentage'}
                    {elseif $coupons[$purchase->auto_coupon]->type=='absolute'}
                        {$autodiscount_type = 'absolute'}
                    {/if}
                {/if}
        		<input type=text name=auto_discount disabled value='{$autodiscount}' style="width: 50px"> 
                <select name="auto_discount_type" class=currency disabled>
                    <option value="1" {if $autodiscount_type=='percentage'}selected{/if}>%</option>
                    <option value="2" {if $autodiscount_type=='absolute'}selected{/if}>руб.</option>
                </select>
                {/if}
                </span>
				</div>
		
				
			</div>
            <div style="margin-top: 10px">
    			<div class="price cell">
                    {if $purchase->auto_coupon}<span class="" style="text-decoration: line-through;">{$purchase->dirty_price}</span> {$currency->sign}<br />{/if}
    				<span class=view_purchase>{$purchase->price}</span>
    				<span class=edit_purchase style='display:none;'>
    				<input type=text name=purchases[price][{$purchase->id}] value='{$purchase->price}' size=5>
    				</span>
    				{$currency->sign}
    				<span class=view_purchase style="float:left;margin-left:-10px;">опт. {$purchase->stockprice|escape}&nbsp;{$currency->sign}</span>
    				<span class=edit_purchase style='display:none;float:left;'>
    				<input name="purchases[stockprice][{$purchase->id}]" type="text"   value="{$purchase->stockprice|escape}">
    				</span>

    			</div>
    			<div class="amount cell">			
    				<span class=view_purchase>
    					{$purchase->amount} {$settings->units}
    				</span>
    				<span class=edit_purchase style='display:none;'>
    					{if $purchase->variant}
    					{math equation="min(max(x,y),z)" x=$purchase->variant->stock+$purchase->amount*($order->closed) y=$purchase->amount z=$settings->max_order_amount assign="loop"}
    					{else}
    					{math equation="x" x=$purchase->amount assign="loop"}
    					{/if}
    			        <select name=purchases[amount][{$purchase->id}]>
    						{section name=amounts start=1 loop=$loop+1 step=1}
    							<option value="{$smarty.section.amounts.index}" {if $purchase->amount==$smarty.section.amounts.index}selected{/if}>{$smarty.section.amounts.index} {$settings->units}</option>
    						{/section}
    			        </select>
    				</span>			
    			</div>
    
    			<div class="icons order cell">
                    {if $purchase->variant->store}
                        <a class="vstore" href="#" title=" {if $purchase->variant->store==1}склад Октябрьская{elseif $purchase->variant->store==2}магазин Сокольники{else}резерв{/if}"></a>
                    {/if}		
    				{if !$order->closed}
    					{if !$purchase->product}
    					<img src='design/images/error.png' alt='Товар был удалён' title='Товар был удалён' >
    					{elseif !$purchase->variant}
    					<img src='design/images/error.png' alt='Вариант товара был удалён' title='Вариант товара был удалён' >
    					{elseif $purchase->variant->stock < $purchase->amount}
    					<img src='design/images/error.png' alt='На складе остал{$purchase->variant->stock|plural:'ся':'ось'} {$purchase->variant->stock} товар{$purchase->variant->stock|plural:'':'ов':'а'}' title='На складе остал{$purchase->variant->stock|plural:'ся':'ось'} {$purchase->variant->stock} товар{$purchase->variant->stock|plural:'':'ов':'а'}'  >
    					{/if}
    				{/if}

    				
    				<a href='#' class="delete" title="Удалить"></a>		
    			</div>
            </div>
			<div class="clear"></div>
		</div>
		{/foreach}
		<div id="new_purchase" class="row" style='display:none;'>
			<div class="image cell">
				<input type=hidden name=purchases[id][] value=''>
				<img class=product_icon src=''>
			</div>
			<div class="purchase_name cell">
				<div class='purchase_variant'>				
					<select name=purchases[variant_id][] style='display:none;'></select>
				</div>
				<a class="purchase_name" href=""></a>
			</div>
			<div class="price cell">
				<input type=text name=purchases[price][] value='' size=5> {$currency->sign}
			</div>
			<div class="amount cell">
	        	<select name=purchases[amount][]></select>
			</div>
			<div class="icons cell">
				<a href='#' class="delete" title="Удалить"></a>	
			</div>
			<div class="clear"></div>
		</div>
	</div>

 	<div id="add_purchase" {if $purchases}style='display:none;'{/if}>
 		<input type=text name=related id='add_purchase' class="input_autocomplete" placeholder='Выберите товар чтобы добавить его'>
 	</div>
	{if $purchases}
	<a href='#' class="dash_link edit_purchases">редактировать покупки</a>
	{/if}


	{if $purchases}
	<div class="subtotal">
	Всего<b> {$subtotal} {$currency->sign}</b>
	</div>
	{/if}
   
    {*auto_discount*}
    {$auto_discount_price_total=0}
    {if $is_auto_coupon}
    <div class="block discount layer">
		<h2>Авто Купон</h2>
        {foreach $purchases as $purchase}
        {if $purchase->auto_coupon}
            <span style="border: 1px solid lightgreen; margin: 2px; padding: 4px;">
                <b>{$coupons[$purchase->auto_coupon]->name}</b>&nbsp;&nbsp;&nbsp;
            </span>
            {$auto_discount_price_total = $auto_discount_price_total + ($purchase->dirty_price-$purchase->price)*$purchase->amount}
            
        {/if}
        {/foreach}
        {$auto_discount_price_total}  {$currency->sign}
	</div>
    <div class="subtotal layer">
	С учетом автокупонов<b> {($subtotal - $auto_discount_price_total)|convert} {$currency->sign}</b>
	</div>
    {/if}
    {*/auto_discount/*}

	<div class="block discount layer">
		<h2>Скидка</h2>
		<input type=text name=discount value='{$order->discount}'> 
        <select name="discount_type" class=currency>
            <option value="1" {if $order->discount_type==1}selected{/if}>%</option>
            <option value="2" {if $order->discount_type==2}selected{/if}>руб.</option>
        </select>
	</div>

	<div class="subtotal layer">
	С учетом скидки<b> {if $order->discount_type==2}{($subtotal-$order->discount - $auto_discount_price_total)}
                       {else} {($subtotal-$subtotal*$order->discount/100 - $auto_discount_price_total)|round:0}
                       {/if}{$currency->sign}</b>
	</div> 
	
	<div class="block discount layer">
		<h2>Купон{if $order->coupon_code} ({$order->coupon_code}){/if}</h2>
		<input type=text name=coupon_discount value='{$order->coupon_discount}'> <span class=currency>{$currency->sign}</span>		
	</div>

	<div class="subtotal layer">
	С учетом купона<b> {if $order->discount_type==1}{($subtotal-$subtotal*$order->discount/100-$order->coupon_discount - $auto_discount_price_total)|round:0}
                        {else}   {($subtotal-$order->discount-$order->coupon_discount - $auto_discount_price_total)|round:2}
                        {/if}{$currency->sign}</b>
	</div> 
	
	<div class="block delivery">
		<h2>Доставка</h2>
				<select name="delivery_id">
				<option value="0">Не выбрана</option>
				{foreach $deliveries as $d}
				<option value="{$d->id}" {if $d->id==$delivery->id}selected{/if}>{$d->name}</option>
				{/foreach}
				</select>	
				<input type=text name=delivery_price value='{$order->delivery_price}'> <span class=currency>{$currency->sign}</span>
				<div class="separate_delivery">
					<input type=checkbox id="separate_delivery" name=separate_delivery value='1' {if $order->separate_delivery}checked{/if}> <label  for="separate_delivery">оплачивается отдельно</label>
				</div>
	</div>

	<div class="total layer">
	Итого<b> {$order->total_price|round:0} {$currency->sign}</b>
	</div>
		
		
	<div class="block payment">
		<h2>Оплата</h2>
				<select name="payment_method_id">
				<option value="0">Не выбрана</option>
				{foreach $payment_methods as $pm}
				<option value="{$pm->id}" {if $pm->id==$payment_method->id}selected{/if}>{$pm->name}</option>
				{/foreach}
				</select>
		
		<input type=checkbox name="paid" id="paid" value="1" {if $order->paid}checked{/if}> <label for="paid" {if $order->paid}class="green"{/if}>Заказ оплачен</label>	<br />
        <input type=checkbox name="allow_to_pay" id="allow_to_pay" value="1" {if $order->allow_to_pay}checked{/if}> <label for="allow_to_pay" {if $order->allow_to_pay}class="green"{/if}>Разрешить оплату</label>		
	</div>

 
	{if $payment_method}
	<div class="subtotal layer">
	К оплате<b> {$order->total_price|convert:$payment_currency->id} {$payment_currency->sign}</b>
	</div>
	{/if}


	<div class="block_save">
	<input type="checkbox" value="1" id="notify_user" name="notify_user">
	<label for="notify_user">Уведомить покупателя о состоянии заказа</label>
	<br>
	<input type="checkbox" value="1" id="notify_sms" name="notify_sms">
	<label for="notify_sms">Уведомить покупателя по SMS</label>
	
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	</div>
	<div class="block layer">
		<h2>Комментарии</h2>
		<div id="list" class="sortable">
			{foreach $order->comments as $comment}
			<div class="row">
		 		<div class="checkbox cell">
					<input type="checkbox" name="check[]" value="{$comment->id}"/>				
				</div>			
				<div class="name cell">
					<div class="comment_name">
					{$comment->name|escape}
					</div>
					<div class="comment_text">
					{$comment->text|nl2br}
					</div>					
					<div class="comment_info">
					Комментарий оставлен {$comment->date|date} в {$comment->date|time}
					</div>
				</div>

				<div class="clear"></div>
			</div>
			{/foreach}
							<div class="clear"></div><br>
	<input class="button_green button_save1" type="submit" name="delete_comment" value="Удалить выбранное" />
	<div class="clear"></div><br>			
		</div>		
		
		<h2>Оставить комментарий</h2>
		<textarea name="comm" class="editor_small" style="width:100%;height:100px;"></textarea>
	</div>
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
</div>


</form>
<!-- Основная форма (The End) -->


{* On document load *}
{literal}
<script src="design/js/autocomplete/jquery.autocomplete-min.js"></script>
<style>
.autocomplete-w1 { background:url(img/shadow.png) no-repeat bottom right; position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; }
.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:auto; min-width: 300px; overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
.autocomplete .selected { background:#F0F0F0; }
.autocomplete div { padding:2px 5px; white-space:nowrap; }
.autocomplete strong { font-weight:normal; color:#3399FF; }
</style>

<script>
$(function() {

	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();
	
	// Удаление товара
	$(document).on('click', ".purchases a.delete", function() {
		 $(this).closest(".row").fadeOut(200, function() { $(this).remove(); });
		 return false;
	});
 

	// Добавление товара 
	var new_purchase = $('.purchases #new_purchase').clone(true);
	$('.purchases #new_purchase').remove().removeAttr('id');
    $.ajaxSetup({cache: false}); 
	$("input#add_purchase").autocomplete({
  	serviceUrl:'ajax/add_order_product.php',
  	minChars:0,
  	noCache: true, 
  	onSelect:
  		function(value, data){
  			new_item = new_purchase.clone().appendTo('.purchases');
  			new_item.removeAttr('id');
  			new_item.find('a.purchase_name').html(data.fullname);
  			new_item.find('a.purchase_name').attr('href', 'index.php?module=ProductAdmin&id='+data.id);
  			
  			// Добавляем варианты нового товара
  			var variants_select = new_item.find('select[name*=purchases][name*=variant_id]');
			for(var i in data.variants)
  				variants_select.append("<option value='"+data.variants[i].id+"' price='"+data.variants[i].price+"' amount='"+data.variants[i].stock+"'>"+data.variants[i].name+"</option>");
  			console.log(data);
  			if(data.variants.length>1 || data.variants[0].name != '')
  				variants_select.show();
  				  				
			variants_select.bind('change', function(){change_variant(variants_select);});
				change_variant(variants_select);
  			
  			if(data.image)
  				new_item.find('img.product_icon').attr("src", data.image);
  			else
  				new_item.find('img.product_icon').remove();

			$("input#add_purchase").val(''); 
  			new_item.show();
  		},
		fnFormatResult:
			function(value, data, currentValue){
				var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
				var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
			}
  		
  });
  
  // Изменение цены и макс количества при изменении варианта
  function change_variant(element)
  {
		price = element.find('option:selected').attr('price');
		amount = element.find('option:selected').attr('amount');
        sku = element.find('option:selected').attr('sku');
		element.closest('.row').find('input[name*=purchases][name*=price]').val(price);
		element.closest('.row').find('input[name*=purchases][name*=sku]').val(sku);
		// 
		amount_select = element.closest('.row').find('select[name*=purchases][name*=amount]');
		selected_amount = amount_select.val();
		amount_select.html('');
		for(i=1; i<=amount; i++)
			amount_select.append("<option value='"+i+"'>"+i+" {/literal}{$settings->units}{literal}</option>");
		amount_select.val(Math.min(selected_amount, amount));


		return false;
  }
  
  
	// Редактировать покупки
	$("a.edit_purchases").click( function() {
		 $(".purchases span.view_purchase").hide();
		 $(".purchases span.edit_purchase").show();
		 $(".edit_purchases").hide();
		 $("div#add_purchase").show();
		 return false;
	});
  
	// Редактировать получателя
	$("div#order_details a.edit_order_details").click(function() {
		 $("ul.order_details .view_order_detail").hide();
		 $("ul.order_details .edit_order_detail").show();
		 return false;
	});


	$(".js-phone2").click(function() {
		 $(".edit_order_detail_phone").show();
		 return false;
	});
	$(".js-email2").click(function() {
		 $(".edit_order_detail_email").show();
		 return false;
	});
  
	// Редактировать примечание
	$("div#order_details a.edit_note").click(function() {
		 $("div.view_note").hide();
		 $("div.edit_note").show();
		 return false;
	});
  
	// Редактировать пользователя
	$("div#order_details a.edit_user").click(function() {
		 $("div.view_user").hide();
		 $("div.edit_user").show();
		 return false;
	});
	$("input#user").autocomplete({
		serviceUrl:'ajax/search_users.php',
		minChars:0,
		noCache: false, 
		onSelect:
			function(value, data){
				$('input[name="user_id"]').val(data.id);
			}
	});
  
	// Удалить пользователя
	$("div#order_details a.delete_user").click(function() {
		$('input[name="user_id"]').val(0);
		$('div.view_user').hide();
		$('div.edit_user').hide();
		return false;
	});

	// Посмотреть адрес на карте
	$("a#address_link").attr('href', 'http://maps.yandex.ru/?text='+$('#order_details textarea[name="address"]').val());
  
	// Подтверждение удаления
	$('select[name*=purchases][name*=variant_id]').bind('change', function(){change_variant($(this));});
	$("input[name='status_deleted']").click(function() {
		if(!confirm('Подтвердите удаление'))
			return false;	
	});

});

</script>

<style>
.ui-autocomplete{
background-color: #ffffff; width: 100px; overflow: hidden;
border: 1px solid #e0e0e0;
padding: 5px;
}
.ui-autocomplete li.ui-menu-item{
overflow: hidden;
white-space:nowrap;
display: block;
}
.ui-autocomplete a.ui-corner-all{
overflow: hidden;
white-space:nowrap;
display: block;
}
</style>
{/literal}

