{* Вкладки *}
{capture name=tabs}
		<li><a href="index.php?module=CommentsAdmin">Комментарии
				{if $new_comments_counter}<span class="rounded_counter_tab">{$new_comments_counter}</span>{/if}
			</a></li>
		<li><a href="index.php?module=FeedbacksAdmin">Обратная связь</a></li>
		<li class="active"><a href="index.php?module=YapAdmin">Отзывы с Я.Маркета</a></li>
        {* callbacks *}
	{if in_array('callbacks', $manager->permissions)}<li><a href="index.php?module=CallbacksAdmin">Заказ обратного звонка
			{if $new_callbacks_counter}<span class="rounded_counter_tab">{$new_callbacks_counter}</span>{/if}
		</a></li>
	<li><a href="index.php?module=CallbacksCheapAdmin">Нашли дешевле
			{if $new_callbackcheap_counter}<span class="rounded_counter_tab">{$new_callbackcheap_counter}</span>{/if}
		</a></li>{/if}
	{*/ callbacks *}
{/capture}

{* Title *}
{$meta_title='Отзывы Я.Маркета' scope=parent}

{* Заголовок *}
<div id="main_list">
	<form name="maga" method="get">
		<input name="module" value="YapAdmin" type="hidden">
		<input name="magaz" value="1" type="hidden">
		<input   class="button_green" type="submit" value="Импорт отзывов о магазине">
	</form>
	&nbsp;
	<form name="tovr" method="get">
		<input name="module" value="YapAdmin" type="hidden">
		<input name="tovar" value="1" type="hidden">
		<input   class="button_green" type="submit"  value="Импорт отзывов о товарах">
	</form>
</div>
