{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	<li class="active"><a href="index.php?module=BrandsAdmin">Бренды</a></li>
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{if $brand->id}
{$meta_title = $brand->name scope=parent}
{else}
{$meta_title = 'Новый бренд' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}


{* On document load *}
{literal}
<script>
$(function() {

	// Удаление изображений
	$(".images a.delete").click( function() {
		$("input[name='delete_image']").val('1');
		$(this).closest("ul").fadeOut(200, function() { $(this).remove(); });
		return false;
	});

	// Автозаполнение мета-тегов
	meta_title_touched = true;
	meta_keywords_touched = true;
	meta_description_touched = true;
	url_touched = true;
	
	if($('input[name="meta_title"]').val() == generate_meta_title() || $('input[name="meta_title"]').val() == '')
		meta_title_touched = false;
	if($('input[name="meta_keywords"]').val() == generate_meta_keywords() || $('input[name="meta_keywords"]').val() == '')
		meta_keywords_touched = false;
	if($('textarea[name="meta_description"]').val() == generate_meta_description() || $('textarea[name="meta_description"]').val() == '')
		meta_description_touched = false;
	if($('input[name="url"]').val() == generate_url() || $('input[name="url"]').val() == '')
		url_touched = false;
		
	$('input[name="meta_title"]').change(function() { meta_title_touched = true; });
	$('input[name="meta_keywords"]').change(function() { meta_keywords_touched = true; });
	$('input[textarea="meta_description"]').change(function() { meta_description_touched = true; });
	$('input[name="url"]').change(function() { url_touched = true; });
	
	$('input[name="name"]').keyup(function() { set_meta(); });
	
	function set_meta()
	{
		if(!meta_title_touched)
			$('input[name="meta_title"]').val(generate_meta_title());
		if(!meta_keywords_touched)
			$('input[name="meta_keywords"]').val(generate_meta_keywords());
		if(!meta_description_touched)
			$('textarea[name="meta_description"]').val(generate_meta_description());
		if(!url_touched)
			$('input[name="url"]').val(generate_url());
	}
	
	function generate_meta_title()
	{
		name = $('input[name="name"]').val();
		return name;
	}

	function generate_meta_keywords()
	{
		name = $('input[name="name"]').val();
		return name;
	}

	function generate_meta_description()
	{
		name = $('input[name="name"]').val();
		return name;
	}
		
	function generate_url()
	{
		url = $('input[name="name"]').val();
		url = url.replace(/[\s]+/gi, '-');
		url = translit(url);
		url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();	
		return url;
	}
	
	function translit(str)
	{
		var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")   
		var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")   
	 	var res = '';
		for(var i=0, l=str.length; i<l; i++)
		{ 
			var s = str.charAt(i), n = ru.indexOf(s); 
			if(n >= 0) { res += en[n]; } 
			else { res += s; } 
	    } 
	    return res;  
	}

});

</script>
 
{/literal}
{literal}
<script>
$(function(){
	$('#meta_generate').click(function() {
		
		$('#meta_generate img').attr('src', 'design/images/loader.gif');
		var error = false;
		var title = $('input[name=meta_title]');
		var keywords = $('input[name=meta_keywords]');
		var description = $('textarea[name=meta_description]');

		//alert(title.val().length);
		//if(title.val() == '' || title.val().length < 2)
		//	var error = new Array('Введите в поле "Заголовок" название не менее 2 символов',title);
		
		if(error != false){
			alert(error[0]);
			error[1].focus();
			$('#meta_generate img').attr('src', 'design/images/wand.png');
			return false;
		}

		$.ajax({
 			 url: "ajax/seo.php",
 			 	data: {action:'brand', brand_id: $('#name input[name="id"]').val(), brand: $('#name input[name="name"]').val()},
 			 	dataType: 'json',
  				success: function(data){
					title.val(data['title']);
					description.val(data['description']);
					keywords.val(data['keywords']);
					
					$(document).scrollTop( 0 );
					title.focus();
					title.val(title.val());
  				}
		});
		$('#meta_generate img').attr('src', 'design/images/wand.png');
		return false;
	});
    
    $('#desc_generate').click(function() {
		$('#desc_generate img').attr('src', 'design/images/loader.gif');
        var error = false;
		
		if(error != false){
			alert(error[0]);
			error[1].focus();
			$('#desc_generate img').attr('src', 'design/images/wand.png');
			return false;
		}
		$.ajax({
 			 url: "ajax/seo.php",
 			 	data: {action:'brand_description', brand_id: $('#name input[name="id"]').val(), brand: $('#name input[name="name"]').val()},
 			 	dataType: 'json',
                cache: false,
  				success: function(data){

					tinyMCE.get('description').setContent(data['description']);
					//$(document).scrollTop( 0 );
					
  				}
		});
		$('#desc_generate img').attr('src', 'design/images/wand.png');
		return false;
	});
});    
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Бренд добавлен{elseif $message_success=='updated'}Бренд обновлен{else}{$message_success}{/if}</span>
	<a class="link" target="_blank" href="../brands/{$brand->url}">Открыть бренд на сайте</a>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Бренд с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$brand->name|escape}"/> 
		<input name=id type="hidden" value="{$brand->id|escape}"/> 
		<div class="checkbox">
			
		</div>	
         <div class="checkbox">
			<input name=meta_generation value='1' type="checkbox" id="meta_generation" {if $brand->meta_generation}checked{/if}/> <label for="meta_generation">Генерация мета</label>
		</div>
	</div> 


 		
	<!-- Левая колонка свойств товара -->
	<div id="column_left">
			
		<!-- Параметры страницы -->
		<div class="block layer">
			<h2>Параметры страницы <a href="#" id="meta_generate" title="Сгенерировать автоматически мета-данные и описание для футера"><img src="design/images/wand.png" alt="Сгенерировать автоматически мета-данные и описание для футера" title="Сгенерировать автоматически мета-данные и описание для футера"></a></h2>
			<ul>
				<li><label class=property>Адрес</label><div class="page_url"> /brands/</div><input name="url" class="page_url" type="text" value="{$brand->url|escape}" /></li>
				<li><label class=property>Заголовок</label><input name="meta_title" class="simpla_inp" type="text" value="{$brand->meta_title|escape}" /></li>
				<li><label class=property>Ключевые слова</label><input name="meta_keywords" class="simpla_inp" type="text" value="{$brand->meta_keywords|escape}" /></li>
				<li><label class=property>Описание</label><textarea name="meta_description" class="simpla_inp" />{$brand->meta_description|escape}</textarea></li>
                <li><label class=property>Русское название</label><input name="rus_name" class="simpla_inp" type="text" value="{$brand->rus_name|escape}" /></li>
			</ul>
			<h2>Параметры бренда</h2>
			<ul>
				<li><label class=property>Исключить из фильтра</label>		<select name="in_filter" class="simpla_inp"><option {if !$brand->in_filter}selected{/if} value="0">Нет</option><option {if $brand->in_filter}selected{/if} value="1">Да</option> </select></li>										
				<li><label class=property>Не применять скидку</label>		<select name="nisale" class="simpla_inp"><option {if !$brand->nisale}selected{/if} value="0">Нет</option><option {if $brand->nisale}selected{/if} value="1">Да</option> </select></li>										
			</ul>
			
			
				

		</div>
		<!-- Параметры страницы (The End)-->
		
 		{*
		<!-- Экспорт-->
		<div class="block">
			<h2>Экспорт товара</h2>
			<ul>
				<li><input id="exp_yad" type="checkbox" /> <label for="exp_yad">Яндекс Маркет</label> Бид <input class="simpla_inp" type="" name="" value="12" /> руб.</li>
				<li><input id="exp_goog" type="checkbox" /> <label for="exp_goog">Google Base</label> </li>
			</ul>
		</div>
		<!-- Свойства товара (The End)-->
		*}
			
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right">
	
		<!-- Изображение категории -->	
		<div class="block layer images">
			<h2>Изображение бренда</h2>
			<input class='upload_image' name=image type=file>			
			<input type=hidden name="delete_image" value="">
			{if $brand->image}
			<ul>
				<li>
					<a href='#' class="delete"><img src='design/images/cross-circle-frame.png'></a>
					<img src="../{$config->brands_images_dir}{$brand->image}" alt="" />
				</li>
			</ul>
			{/if}
			Картинка должна быть 150 по ширине и 50 по высоте
		</div>
		
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	
	<!-- Описагние бренда -->
	<div class="block layer">
		<h2>Описание<a href="#" id="desc_generate" title="Сгенерировать автоматически описание"><img src="design/images/wand.png" alt="Сгенерировать автоматически описание" title="Сгенерировать автоматически описание"></a></h2><input id="generate_description" type="checkbox" name="generate_description" value="1" {if $brand->generate_description}checked{/if} /><label for="generate_description">Генерировать из сео модуля</label>
		<textarea name="description" class="editor_large">{$brand->description|escape}</textarea>
	</div>
	<!-- Описание бренда (The End)-->
	
	<!-- Описание бренда + категория -->
	{foreach $brand_in_categories as $category}
	<div class="block layer">
		<h2>Описание в категории {$category->name}:</h2>
		<textarea name="description_cat[{$category->id}]" class="editor_large">{$brand->description_in_categories[$category->id]|escape}</textarea>
	</div>
	{/foreach}
	<!-- Описание бренда + категория (The End)-->
	
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	
	
</form>
<!-- Основная форма (The End) -->

