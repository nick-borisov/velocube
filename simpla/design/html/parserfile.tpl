{* Вкладки *}
{capture name=tabs}
<!--	<li><a href="index.php?module=ProductsAdmin">Товары</a></li> -->
	<li><a href="index.php?module=ParsersAdmin">Все парсеры</a></li>
	{if ($parser->name)}
	<li class="active"><a href="index.php?module=ParserAdmin&id={$parser->id}">{$parser->name|escape}</a></li>
	{else}
	<li class="active"><a href="index.php?module=ParserFileAdmin">Добавить файл</a></li>
	{/if}
{/capture}
{if $parser->id}
{$meta_title = $parser->name scope=parent}
{else}
{$meta_title = 'Новый' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}


 

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Парсер цен добавлен{elseif $message_success=='updated'}Парсер обновлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Парсер с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}

 {if !$ok}
      <h2><p><b> Форма для загрузки файлов </b></p></h2>
      <form action="?module=ParserFileAdmin&return=%2Fsimpla%2Findex.php%3Fmodule%3DParsersAdmin" method="post" enctype="multipart/form-data">
      <input type="file" name="filename"><br> 
      <input type="submit" value="Загрузить"><br>
      </form>
 {else}
 {$ok}
 {/if}
 
 