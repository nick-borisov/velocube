{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	<li class="active"><a href="index.php?module=FeaturesAdmin">Свойства</a></li>
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{* Title *}
{$meta_title='Свойства' scope=parent}

{* Заголовок *}
<div id="header">
	<h1>Свойства</h1> 
	<a class="add" href="{url module=FeatureAdmin return=$smarty.server.REQUEST_URI}">Добавить свойство</a>
    <div id="expand" style="display: block;">
	<!-- Свернуть/развернуть варианты -->
    
	<a href="#" class="dash_link" id="expand_all" style="display:none;">Развернуть все свойства ↓</a>
	<a href="#" class="dash_link" id="roll_up_all" >Свернуть все свойства ↑</a>
	<!-- Свернуть/развернуть варианты (The End) -->
	</div>
</div>	

{if $features}
<div id="main_list" class="features">
	
	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">

		<div id="list" class="sortable">
			{foreach $features as $feature}
            {if $feature->separ}{assign 'separ_id' $feature->id}{/if}
			{if !$feature->separ}
			<div class="{if $feature->in_filter}in_filter{/if} {if $feature->yandex}yandex{/if} row usual separ_{$separ_id}">
				<input type="hidden" name="positions[{$feature->id}]" value="{$feature->position}">
				<div class="move cell"><div class="move_zone"></div></div>
		 		<div class="checkbox cell">
					<input type="checkbox" name="check[]" value="{$feature->id}" />				
				</div>
				<div class="cell">
					<a href="{url module=FeatureAdmin id=$feature->id return=$smarty.server.REQUEST_URI}">{$feature->name|escape}</a>
				</div>
				<div class="icons cell">
					<a title="Использовать в фильтре" class="in_filter" href='#' ></a>
					<a title="Удалить" class="delete" href='#' ></a>
			
        			{*<input type="checkbox" name="yandex" {if $feature->yandex}checked{/if} title="В яндекс">*}
                    <a class="yandex" href="#" title="В Яндекс"  ></a>
        		</div>
				<div class="clear"></div>
			</div>
			{else}
			<div class="row separ" style="background: #fff; text-align:center; text-decoration: none;"  data-sfid="{$feature->id}">
				<input type="hidden" name="positions[{$feature->id}]" value="{$feature->position}">
				<div class="move cell"><div class="move_zone"></div></div>
		 		<div class="checkbox cell">
					<input type="checkbox" name="check[]" value="{$feature->id}" />	
                    			
				</div>
				<div class="cell" style="width: 80%;">
					<a href="{url module=FeatureAdmin id=$feature->id return=$smarty.server.REQUEST_URI}">{$feature->name|escape}</a>
				</div>
				<div class="icons cell">
				 
					<a title="Удалить" class="delete" href='#' ></a>
				</div>
				<div class="clear"></div>
			</div>
			{/if} 
			{/foreach}
		</div>
		
		<div id="action">
		<label id="check_all" class="dash_link">Выбрать все</label>
	
		<span id="select">
		<select name="action">
			<option value="set_in_filter">Использовать в фильтре</option>
			<option value="unset_in_filter">Не использовать в фильтре</option>
			<option value="delete">Удалить</option>
            <option value="in_yandex">В Яндекс</option>
            <option value="out_yandex">Из Яндекса</option>
		</select>
		</span>
	
		<input id="apply_action" class="button_green" type="submit" value="Применить">
		</div>

	</form>

</div>
{else}
	Нет свойств
{/if}
 
 <!-- Меню -->
<div id="right_menu">
	
	<!-- Категории товаров -->
	{function name=categories_tree level=1}
	{if $categories}
		<ul id="my_list1" class="list-right level-{$level}{if $level > 1} drop{/if}">
			{if $categories[0]->parent_id == 0}
				<li class="{if !$category->id}selected{/if} no-border" style="margin-left:0;padding-left:0;"><a href="{url category_id=null brand_id=null}" style="border:0;">Все категории</a></li>	
			{/if}
			{foreach $categories as $c}
				<li category_id="{$c->id}" class="{if $category->id == $c->id}selected{else}droppable category{/if}{if $c->subcategories} dropdown{/if}">
					<a href='{url keyword=null brand_id=null page=null category_id={$c->id}}'>{$c->name}</a>
					{if $c->subcategories}
						<span class="icon icon-plus"></span>
					{/if}
					{categories_tree categories=$c->subcategories level=$level+1}
				</li>
				
			{/foreach}
		</ul>	 
	{/if}
	{/function}
	{categories_tree categories=$categories}
	<!-- Категории товаров (The End)-->
		
</div>
<!-- Левое меню  (The End) -->


{literal}
<script>
$(function() {

	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();
	
	// Сортировка списка
	/*$("#list").sortable({
		items:             ".row",
		tolerance:         "pointer",
		handle:            ".move_zone",
		axis: 'y',
		scrollSensitivity: 40,
		opacity:           0.7, 
		forcePlaceholderSize: true,
		
		helper: function(event, ui){		
			if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
			var helper = $('<div/>');
			$('input[type="checkbox"][name*="check"]:checked').each(function(){
				var item = $(this).closest('.row');
				helper.height(helper.height()+item.innerHeight());
				if(item[0]!=ui[0]) {
					helper.append(item.clone());
					$(this).closest('.row').remove();
				}
				else {
					helper.append(ui.clone());
					item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
				}
			});
			return helper;			
		},	
 		start: function(event, ui) {
  			if(ui.helper.children('.row').size()>0)
				$('.ui-sortable-placeholder').height(ui.helper.height());
		},
		beforeStop:function(event, ui){
			if(ui.helper.children('.row').size()>0){
				ui.helper.children('.row').each(function(){
					$(this).insertBefore(ui.item);
				});
				ui.item.remove();
			}
		},
		update:function(event, ui)
		{
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit(function() {
				colorize();
			});
		}
	});*/
    $(".sortable").sortable({
		items:".row",
		handle: ".move_zone",
		tolerance:"pointer",
		scrollSensitivity:40,
		opacity:0.7, 
		axis: "y",
		update:function()
		{
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit();
		}
	});
	
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', $('#list input[type="checkbox"][name*="check"]:not(:checked)').length>0);
	});	
	
	// Указать "в фильтре"/"не в фильтре"
	$("a.in_filter").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('in_filter')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'feature', 'id': id, 'values': {'in_filter': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(!state)
					line.removeClass('in_filter');
				else
					line.addClass('in_filter');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
     /*yandex ext*/
	$("a.yandex").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('yandex')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'feature', 'id': id, 'values': {'yandex': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('yandex');				
				else
					line.removeClass('yandex');
			},
			dataType: 'json'
		});	
		return false;	
	});	
    /*/yandex ext*/
    
	// Удалить
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').attr('selected', true);
		$(this).closest("form").submit();
	});
	
	// Подтверждение удаления
	$("form").submit(function() {
		if($('#list input[type="checkbox"][name*="check"]:checked').length>0)
			if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
				return false;	
	});
    
    $("div.separ").on('click', function(){
        var separ_id = $(this).data('sfid');
        $(".separ_"+separ_id).slideToggle('fast');
    });
	
    $('#expand_all').on('click', function(){
        $('div.usual').show();
        $('#roll_up_all').show();
        $(this).hide();
        return false;
    });
    $('#roll_up_all').on('click', function(){
        $('div.usual').hide();
        $('#expand_all').show();
        $(this).hide();
        return false;
    });
    $('#roll_up_all').click();
    if($('.category.dropdown li').hasClass('selected')){
        $('#expand_all').click();
    }
    
});
</script>
{/literal}