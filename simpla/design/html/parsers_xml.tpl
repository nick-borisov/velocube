{* Вкладки *}
{capture name=tabs}
<!--	<li><a href="index.php?module=ProductsAdmin">Товары</a></li> -->
	<li><a href="index.php?module=ParsersAdmin">Все парсеры</a></li>
	<li><a href="index.php?module=StratAdmin">Стратегия цен</a></li>
    <li class="active"><a href="index.php?module=ParsersImportXMLAdmin">Парсеры XML</a></li>
{/capture}

{* Title *}
{$meta_title='Парсеры XML' scope=parent}

{* Заголовок *}
<div id="header">
	<h1>Парсеры XML</h1>
	<a class="add" href="{url module=ParserImportXMLAdmin return=$smarty.server.REQUEST_URI}">Добавить парсер XML</a>
    <a class="add" href="{url module=ParsersImportXMLAdmin clear_xml_category=1 return=$smarty.server.REQUEST_URI}">Очистить категорию IMPORT_XML</a>
</div>	
<!-- Заголовок (The End) -->

{if $parsers}
<div id="main_list" class="categories">

	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
 
		{if $parsers}
		<div id="list" class="sortable parsers">
		
			{foreach $parsers as $parser}
			
		 
			
			<div class="{if !$parser->active}invisible{/if} row {$parser->color}">		
				<div class="tree_row">
					<input type="hidden" name="positions[{$parser->id}]" value="{$parser->position}">
					<!-- <div class="move cell" style="margin-left:{$level*20}px"><div class="move_zone"></div></div> -->
			 		<div class="checkbox cell">
						<input hidden type="checkbox" name="check[]" value="{$parser->id}" />				
					</div>
					<div class="cellp">
						<a href="{url module=ParserImportXMLAdmin id=$parser->id return=$smarty.server.REQUEST_URI}">{$parser->name|escape}<span>  (id) - {$parser->id}</span></a> 	 			
					</div>					
					<div class="cellr">
						<p align="right">{$parser->lastrun}</p>
					</div>
					
					
					
					<div class="icons cell">
                        {if $parser->parser_type=='xml'}
                        <span class="xml" title="YML Парсер"></span>
                        {/if}	
						<a class="preview" title="Показать все товары поставщика" target="_blank" href="http://velocube.ru/simpla/?vendor_id={$parser->id}"></a>
						<a class="enable" title="Активна" href="#"></a>
						<a class="hint" title="H" href="#">H<span>{$parser->description}</span></a>
						<a class="delete" title="Удалить" href="#"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			{/foreach}
	
		</div>
		{/if}
 
 		<span id="select">
		<select hidden name="action">
			<option value="enable">Сделать видимыми</option>
			<option value="disable">Сделать невидимыми</option>
			<option value="delete">Удалить</option>
		</select>
		<input hidden id="apply_action" class="button_green" type="submit" value="Применить">
		</form>
		
		<form id="go" method="get">
		<input hidden name="module" value="ParsersAdmin">
		<input hidden name="goclear" id="clr" value="1">
		<div id="action">
		<input id="apply_action" class="button_green" type="submit" value="Очистить всё">
		</form>
		<form id="go" method="get">
		<input hidden name="module" value="ParsersAdmin">
		<input hidden name="goparse" id="chg" value="1">
	
		<input id="apply_action" class="button_green" type="submit" value="Запустить все">
		</div>
		</form>
</div>
{else}
Нет парсеров
{/if}


<!-- Меню -->
<div id="right_menu">
	
 
	<ul>
	


		<li>Зеленый цвет - до 7 дней </li>
		<li>Желтый цвет - от 7 до 15 </li>
		<li>Красный цвет - от 15 до 30</li>
	</ul>
</div>

{literal}
<script>
$(function() {

 
 
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', 1-$('#list input[type="checkbox"][name*="check"]').prop('checked'));
	});	

 

	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').prop('selected', true);
		$(this).closest("form").submit();
	});
	
		// включить 
	$("a.enable").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=disable]').prop('selected', true);
		$(this).closest("form").submit();
	});
	
			// !включить 
	$("a.disable").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=enable]').prop('selected', true);
		$(this).closest("form").submit();
	});

	
	// Подтвердить удаление
	$("form").submit(function() {
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});

});
</script>
{/literal}