<!-- jQuery -->

{literal}
<script src="design/js/autocomplete/jquery.autocomplete-min.js"></script>
<script type="text/javascript" src="design/js/jquery.maskedinput-1.3.min.js"></script>
<script src="design/js/jquery.colorPicker.js"></script>
<script src="design/js/jquery.leanModal.min.js"></script>
 
<style>
	.autocomplete-suggestion { solid; background: #FFF !important; padding: 5px;}
	.autocomplete-w1 { position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0;/* width: 500px; */}
	.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:hidden;  overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
	.autocomplete .selected { background:#F0F0F0; }
	.autocomplete div { padding:2px 5px; white-space:nowrap; }
	.autocomplete strong { font-weight:normal; color:#3399FF; }
	</style>
<style>
	/*.autocomplete-w1 {background: #fff;}*/
	.autocomplete-w1 div {padding: 5px;}
</style>

<script>
jQuery(function(){
    jQuery(".mask246").mask("99");
	 
});

 
 

$(function() {

      $('.add_color').colorPicker();
		$("table.related_products").sortable({ items: 'tr' , axis: 'y',  cancel: '#header', handle: '.move_zone' });	
	// Удаление связанного товара
	$(".related_products a.delete").live('click', function() {
		 $(this).closest("div.row").fadeOut(200, function() { $(this).remove();console.log($(this)); });
		 return false;
	});
 
 	// Сортировка связанных товаров
	$(".sortable").sortable({
		items: "div.row",
		tolerance:"pointer",
		scrollSensitivity:40,
		opacity:0.7,
		handle: '.move_zone'
	});

	// Добавление связанного товара 
	var new_related_product = $('#new_related_product').clone(true);
	$('#new_related_product').remove().removeAttr('id');
 
	$("input#related_products").autocomplete({
		serviceUrl:'ajax/search_products.php',
		minChars:0,
		noCache: false, 
		onSelect:
			function(value, data){
				new_item = new_related_product.clone().appendTo('.related_products');
				new_item.removeAttr('id');
				new_item.find('a.related_product_name').html(data.fullname);
				new_item.find('a.related_product_name').attr('href', 'index.php?module=ProductAdmin&id='+data.id);
				new_item.find('input[name*="related_products"]').val(data.id);
				if(data.image)
					new_item.find('img.product_icon').attr("src", data.image);
				else
					new_item.find('img.product_icon').remove();
				$("#related_products").val(''); 
				new_item.show();
			},
		fnFormatResult:
			function(value, data, currentValue){
				var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
				var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
			}

	});
  });
</script>
{/literal} 
   
{* Вкладки *}
{capture name=tabs}
<!--	<li><a href="index.php?module=ProductsAdmin">Товары</a></li> -->
	<li><a href="index.php?module=ParsersAdmin">Все парсеры</a></li>
	<li><a href="index.php?module=StratAdmin">Стратегия цен</a></li>
	{if ($strat->name)}
	<li class="active"><a href="index.php?module=StratEditAdmin&id={$strat->id}">{$strat->name|escape}</a></li>
	{else}
	<li class="active"><a href="index.php?module=StratEditAdmin">Новая стратегия</a></li>
	{/if}
{/capture}
{if $strat->id}
{$meta_title = $strat->name scope=parent}
{else}
{$meta_title = 'Новая стратегия' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}
 
{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Добавлено{elseif $message_success=='updated'}Обновлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$strat->name|escape}"/> 
		<input name=id type="hidden" value="{$strat->id|escape}"/>
		<div class="checkbox">
			<input name=active value='1' type="checkbox" id="active_checkbox" {if $strat->active}checked{/if}/> <label for="active_checkbox">Активен</label><br>
		</div>
	</div> 
	<div id="column_left">
	
		<div class="block">
			<ul>
			<li><label class=property>Комментарий</label>
			<textarea name="description" class="description">{$strat->description}</textarea></li>
	
			<li><label class=property>Цвет</label> <input name="color" type="hidden" value="{$strat->color}" class="add_color" style="visibility: hidden; position: absolute;"></li>
			
			<li> <h3>Расписание</h3> </li>
			<li><label class=property>Дни недели</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day1 == "on"}checked{/if} name="day1"/><label for="sem">Понедельник</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day2 == "on"}checked{/if} name="day2"/><label for="sem">Вторник</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day3 == "on"}checked{/if} name="day3"/><label for="sem">Среда</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day4 == "on"}checked{/if} name="day4"/><label for="sem">Четверг</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day5 == "on"}checked{/if} name="day5"/><label for="sem">Пятница</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day6 == "on"}checked{/if} name="day6"/><label style="color:red" for="sem">Суббота</label></li>
			<li><label class=property></label><input type="checkbox" id="sem"  {if $strat->day7 == "on"}checked{/if} name="day7"/><label style="color:red" for="sem">Воскресение</label></li>

			<li><h4>Включение </h4></li>
			<li><label class=property>Часы</label><input name="starth" value="{if $strat->starth}{$strat->starth}{else}0{/if}" class="mask24"></li>
			<li><label class=property>Минуты</label><input name="startm" value="{if $strat->startm}{$strat->startm}{else}0{/if}" class="mask24"></li>
				
			<li><h4>Отключение </h4></li>
			<li><label class=property>Часы</label><input name="endh" value="{if $strat->endh}{$strat->endh}{else}23{/if}" class="mask24"></li>
			<li><label class=property>Минуты</label><input name="endm" value="{if $strat->endm}{$strat->endm}{else}59{/if}" class="mask24"></li>

			<li><h4>Параметры цены </h4></li>
			<li><label class=property>Меньше чем (maxprice)</label><input name="maxprice" value="{if $strat->maxprice}{$strat->maxprice}{else}{/if}"></li>
			<li><label class=property>Больше чем (minprice)</label><input name="minprice" value="{if $strat->minprice}{$strat->minprice}{else}{/if}"></li>
			
			</ul>
		</div>
	
		<div class="block layer">
 			<ul>
				<li>
				<h3>Параметры</h3>
				</li>
				<li><label class=property>Изменение цены (Руб)</label>		<input name="delta" class="simpla_inp" type="text" 		value="{$strat->delta|escape}" /></li>
				<li><label class=property>Изменение цены %</label>			<input name="deltap" class="simpla_inp" type="text" 	value="{$strat->deltap|escape}" /></li>
				<li><label class=property>Поставщик</label>		<select name="ven"><option {if $strat->ven == ""}selected{/if} value="IS NULL">Не указан</option>{foreach $vendors as $vn}<option {if $strat->ven == $vn->id}selected{/if} value="{$vn->id}">{$vn->name}</option>{/foreach}</select></li>
				<li><label class=property>Бренд</label>		<select name="brand"><option {if $strat->brand == ""}selected{/if} value="IS NULL">Не указан</option>{foreach $brands as $br}<option {if $strat->brand == $br->id}selected{/if} value="{$br->id}">{$br->name}</option>{/foreach}</select></li>
			</ul>
		</div>
		
 
<div class="block layer">
		 	<h2>Товары</h2>
			<div id=list class="sortable related_products">
				{foreach from=$related_products item=related_product}
				<div class="row">
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products[] value='{$related_product->id}'>
					<a href="{url module=ProductAdmin id=$related_product->id}">
						{if $related_product->images[0]}
						<img class=product_icon src='{$related_product->images[0]->filename|resize:35:35}'>
						{/if}
					</a>
					</div>
					<div class="name cell">
					<a href="{url module=ProductAdmin id=$related_product->id}">{$related_product->fullname}</a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
				{/foreach}
				<div id="new_related_product" class="row" style='display:none;'>
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products[] value=''>
					<img class=product_icon src=''>
					</div>
					<div class="name cell">
					<a class="related_product_name" href=""></a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<input type=text name=related id='related_products' class="input_autocomplete" placeholder='Выберите товар чтобы добавить его'>
		</div>
 
	 
	</div>
	<div id="column_right"> 	
	</div>
	<div class="block layer">
	 
	</div>
	 
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
</form>