{* Вкладки *}
{capture name=tabs}
	<li><a href="index.php?module=ProductsAdmin">Товары</a></li>
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	<li class="active"><a href="index.php?module=SeoAdmin">SEO</a></li>
{/capture}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}

{* Title *}
{$meta_title='SEO оптимизация интернет-магазина' scope=parent}

	<div id="header">	
		<h1>SEO оптимизация</h1>
	</div>
	
	{if $message_error}
	<!-- Системное сообщение -->
	<div class="message message_error" style="height:auto;">
		<ul>
		{foreach $message_error as $error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		<a class="button" href="/simpla/index.php?module=ProductsAdmin">Вернуться</a>
	</div>
	<!-- Системное сообщение (The End)-->
	{/if}
	
{literal}
<script>
    $(function(){
        $( "#tabs" ).tabs();
        
       	var feature = $('#new_feature').clone(true);
    	$('#new_feature').remove().removeAttr('id');
    	$('#add_new_feature').click(function() {
    		$(feature).clone(true).appendTo('ul.new_features').fadeIn('slow').find("input[name*=new_feature_name]").focus();
    		return false;		
    	}); 
        
        var feature_brand = $('#new_feature_brand').clone(true);
    	$('#new_feature_brand').remove().removeAttr('id');
    	$('#add_new_feature_brand').click(function() {
    		$(feature_brand).clone(true).appendTo('ul.new_features_brand').fadeIn('slow').find("input[name*=new_feature_name]").focus();
    		return false;		
    	});
        
        // Удаление категории
    	$(".categories_templates_delete").bind('click', function() {
    		$(this).closest("tr").fadeOut(200, function() { $(this).remove(); });
    		return false;
    	});
        
        var product_template = $('#new_template_product').clone(true);
    	$('#new_template_product').remove().removeAttr('id');
    	$('#add_product_template').click(function() {
    		product_template.clone(true).appendTo('ul.new_features_p').fadeIn('slow').find("input[name*=new_feature_name]").focus();
    		return false;		
    	}); 
        // Удаление категории
    	$(".products_templates_delete").bind('click', function() {
    		$(this).closest("tr").fadeOut(200, function() { $(this).remove(); });
    		return false;
    	});
        
        //Remember active tab on reload page
        if($('#tabs .product_tab').length){ 
            $( "#tabs" ).tabs("option", "active", 0);
        }
        if($('#tabs .category_tab').length){ 
            $( "#tabs" ).tabs("option", "active", 1);
        }
        if($('#tabs .site_tab').length){ 
            $( "#tabs" ).tabs("option", "active", 4);
        }
        if($('#tabs .cat_brand_tab').length){ 
            $( "#tabs" ).tabs("option", "active", 2);
        }
        if($('#tabs .brand_tab').length){ 
            $( "#tabs" ).tabs("option", "active", 3);
        }
        //Send to process only active tab 
        $('#tabs li.ui-state-default').on('click', function(){
            $(this).find('input').attr('checked', 1);
        });
        
    });
</script>
<style>
	.message_error ul{
		margin:0;padding:0;
		float:left;
	}

	/*Стиль таблицы*/
	TABLE{
		margin:0;
		padding:0;
		border-collapse:collapse;
		border:0;
	}

	TABLE TR TH{
		padding:15px 5px 4px 0;
		width:150px;
		
		vertical-align:top;
		
		font:normal 12px Arial;
		color:#000000;
		text-align:left;
	}
	TABLE TR TH:first-child{
		width:250px;
		padding-left:10px;
		padding-right:10px;
	}
	
	/*Подсказка*/
	TABLE TR TH span{
		display:block;
		font:normal 11px Arial;
		color:#a0a0a0;
	}
	
	/*Примеры*/
	TABLE TR TD{
		font:normal 12px Arial;
		color:#a0a0a0;
		padding-bottom:10px;
	}
	
	/*Поля ввода*/
	TABLE * INPUT{
		width:345px;
	}
	TABLE * TEXTAREA{
		width:345px;
		height:105px;
	}
	
	/*Разукрашиваем*/
	TABLE.seo TR:nth-child(2n-1),
	TABLE.seo TR:nth-child(2n){
		background: #ebebeb
	}
	TABLE.seo TR:nth-child(4n-1),
	TABLE.seo TR:nth-child(4n){
		background: #f3f3f3;
	}
	TABLE.seo TR:nth-child(2n){
		border-bottom:solid 1px #d0d0d0;
	}
	
	.tplNum{
		padding:5px;
		font:normal 16px Arial;
		color:#4D4D4D;
		border-top:1px dashed #4D4D4D;
	}
	
</style>
{/literal}
	<form method=post id=product enctype="multipart/form-data" style="width:100%">
	<input type=hidden name="session_id" value="{$smarty.session.id}">
		
		<input class="button_green button_save" type="submit" name="" value="Сохранить" />
		<input class="button_green button_save" type="submit" name="go_update_meta" value="Обновить мета" />
        <input class="button_green button_save" type="submit" name="go_update_description" value="Обновить описания" />
        <br><br><br>
        <div id="tabs">
            <ul>
                <li {if $tab_type == 'product'}class="product_tab"{/if}><a href="#tabs-2">Товар</a><input type="radio" name="tab_type" value="product" {if $tab_type == 'product' || !$tab_type}checked{/if} style="display:none"/></li>
                <li {if $tab_type == 'category'}class="category_tab"{/if}><a href="#tabs-3">Категория</a><input type="radio" name="tab_type" {if $tab_type == 'category'}checked{/if} value="category"  style="display:none"/></li>
                <li {if $tab_type == 'cat_brand'}class="cat_brand_tab"{/if}><a href="#tabs-4">Категория+Бренд</a><input type="radio" name="tab_type" value="cat_brand" {if $tab_type == 'cat_brand'}checked{/if} style="display:none"/></li>
                <li {if $tab_type == 'brand'}class="brand_tab"{/if}><a href="#tabs-5">Бренд</a><input type="radio" name="tab_type" value="brand" {if $tab_type == 'brand'}checked{/if} style="display:none"/></li>
                <li {if $tab_type == 'site'}class="site_tab"{/if}><a href="#tabs-1">Магазин</a><input type="radio" name="tab_type" value="site" {if $tab_type == 'site'}checked{/if} style="display:none"/></li>
                
            </ul>
            <div id="tabs-1">		
        		<table class="seo" style="width:100%;border:1px dashed silver;">		
                    <tr>
        				<th colspan=2><h3>Настройка мета для Интернет-магазина</h3></th>
        				<th rowspan="4" style="width:200px;border:1px solid silver;padding:5px;">
        					{literal}<div>
        					Список используемых тегов: 
                            <ul style="padding-left:5px;">
                                <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                
                                <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{all_cities}</strong> - ВСЕ областные центры в которые осуществляется доставка;</li>
                            </ul>
                            {/literal}  
        					</div>
        				</th>
        			</tr>
        			<tr>
        				<th>Краткое название интернет-магазина:</th>
        				<th><input type="text" name="name" value="{$seo->name}"></th>
        			</tr>
        			<tr>
        				<td></td>
        				<td>Пример: интерет-магазин ТеплоТорг</td>
        			</tr>
        <!-- SEPARATOR -->
        			<tr>
        				<th>Город в котором находится магазин:</th>
        				<th><input type="text" name="officeCity" value="{$seo->officeCity}"></th>
        			</tr>
        			<tr>
        				<td></td>
        				<td>Пример: Москва</td>
        			</tr>
        			<tr>
        				<th>Контактные телефоны:<span style="color:red;">Указываются в мета данных.</span></th>
        				<th><input type="text" name="contactTel" value="{$seo->contactTel}"></th>
        			</tr>
        			<tr>
        				<td></td>
        				<td>Пример: (090) 000-11-22; (090) 111-22-33</td>
        			</tr>
        <!-- SEPARATOR -->
        			<!--<tr>
        				<th>Города в которых есть представительства:<span>Указываются через запятую.</span></th>
        				<th><textarea name="officeCities" value="{$seo->officesCity}"></textarea></th>
        			</tr>
        			<tr>
        				<td></td>
        				<td> Пример: Белгород, Нижний Новгород, Пенза</td>
        			</tr>-->
        <!-- SEPARATOR -->
        			<tr>
        				<th>Города вашего региона (областного центра) в которые производится доставка:<span style="color:red;">В случайном порядке будет выводиться от 5-7 городов в футере в описании.</span><span>Указываются через запятую.</span></th>
        				<th><textarea name="regionCity">{$seo->regionCity}</textarea></th>
        			</tr>
        			<tr>
        				<td></td>
        				<td>Пример: Домодедово, Железнодорожный, Подольск, Балашиха</td>
        			</tr>
        <!-- SEPARATOR -->
        			<tr>
        				<th>Основные регионы, областные центры в которые производится доставка:<span style="color:red;">В случайном порядке будет выводиться от 5-7 областных центров в футере в описании.</span><span>Указываются через запятую.</span></th>
        				<th><textarea name="regions">{$seo->regions}</textarea></th>
        			</tr>
        			<tr>
        				<td></td>
        				<td colspan=2>Пример: Амур, Курск, Брянск, Волгоград, Воронеж</td>
        			</tr>
        		</table>
        <!--<p class="add_template"><span>Добавить шаблон для футера</span></p>-->
            </div>
            <div id="tabs-2">
                <table style="width:100%;border:1px dashed silver;">
        			<tr>
        				<th colspan="2"><h3>Настройка мета для товаров</h3></th>
        				<th rowspan="5" style="width:200px;border:1px solid silver;padding:5px;">
        					{literal}
                            <div  style="width: 250px">
            					Список используемых тегов в описании: 
                                <ul style="padding-left:5px;">
                                    <li><strong style="color:red">{name}</strong> - название товара;</li>
                                    <li><strong style="color:red">{type_prefix}</strong> - Тип товара;</li>
                                    <li><strong style="color:red">{fullname}</strong> - Полное название товара;</li> 
                                    <li><strong style="color:red">{l-name}</strong> - название товара с маленькой буквы;</li>
                                    <li><strong style="color:red">{l-type_prefix}</strong> - Тип товара с маленькой буквы;</li>
                                    <li><strong style="color:red">{variant}</strong> - название варианта товара;</li>
                                    <li><strong style="color:red">{price}</strong> - наименьшая цена товара;</li>
                                    <li><strong style="color:red">{brand}</strong> - название бренда;</li>
                                    <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>  
                                    <li><strong style="color:red">{category}</strong> - название категории;</li>
                                    <li><strong style="color:red">{l-category}</strong> - название категории с маленькой буквы;</li>
                                    <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                    <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                    <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                    <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{all_cities}</strong> - ВСЕ областные центры в которые осуществляется доставка;</li>
                                </ul>
                                {/literal}  
                                <div style="margin-top: 20px;">
                                    Если пустое поле "Описание товара", то модуль пройдеться только по товарам, которые лежат в категориях у которых есть шаблон описания.
                                    <br />
                                    Если не пустое поле "Описание товара", то модуль пройдеться по всем товарам, и если нет шаблона, то применит это поле, если есть, то шаблон.
                                </div>
        					</div>
        				</th>
        			</tr>
                    
        			<tr>
        				<th>Заголовок в товарах:</th>
                    </tr>
                    <tr>
        				<th><textarea class="" name="title_product">{$seo->title_product}</textarea></th>
        			</tr>
        			<tr>
        				<th>МЕТА описание товаре:</th>
                    </tr>
                    <tr>
        				<th><textarea class="" name="description_product">{$seo->description_product}</textarea></th>
        			</tr>
        			<tr>
        				<th>Ключевые слова в товаре:</th>
                    </tr>
                    <tr>
        				<th><textarea class="" name="keywords_product">{$seo->keywords_product}</textarea></th>
        			</tr>
                   {* <tr>
        				<th>Описание товара:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="editor_small" name="full_description_product">{$seo->full_description_product}</textarea></th>
        			</tr>*}
                    <tr>
        				<th>Описание доставки:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="editor_small" name="delivery_description">{$seo->delivery_description}</textarea></th>
        			</tr>
        		</table>
                
                <ul class="new_features_p">
    				<li id="new_template_product">
                        <table>
                        <th>
                            <select name="products_templates[]">
        						{function name=products_templates level=0}
        						{foreach from=$categories item=category}
                                <option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
                                    {products_templates categories=$category->subcategories selected_id=$selected_id  level=$level+1}
        						{/foreach}
        						{/function}
        						{products_templates categories=$categories}
        					</select>
                        </th>    
                        <th><textarea name="des_products_templates[]"></textarea></th>
                        </table>
                    </li>
    			</ul>
                
                <div class="des_cat_templates">
                    <span class="add"><i class="dash_link" id="add_product_template">Добавить новый шаблон</i></span>
                </div>
                
                <table>
                {if $seo->products_templates}{assign 'products_templates' unserialize($seo->products_templates)}{/if}
                {if $seo->des_products_templates}{assign 'des_products_templates' unserialize($seo->des_products_templates)}{/if}
                
                {foreach $products_templates as $key=>$pt}
                    <tr>
                        <th colspan="2">
                            <a class="spoiler" onclick="$(this).next().slideToggle();" href="javascript://"><b>Свойства &downarrow;</b></a>
                            <div class="features_list">
                            
                            {foreach $features[$pt] as $feature}
                                {if !$feature->separ}
                                    <strong style="color:red">{ldelim}f_{$feature->id}{rdelim}</strong> - {$feature->name} &nbsp;&nbsp;&nbsp;
                                {/if}
                            {/foreach}
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <select name="products_templates[]">
        						{products_templates categories=$categories selected_id=intval($pt)}
        					</select>
                            <span style="" class="products_templates_delete"><i class="dash_link">Удалить</i></span>
                        </th> 
                    </tr>
                    <tr>
        				
                        <th><textarea class="editor_small" name="des_products_templates[]">{$des_products_templates[$key]}</textarea></th>
                    </tr>
                {/foreach}
                </table>
                <!-- SEPARATOP-->
                <a class="spoiler" onclick="$('#product_one').slideToggle('slow');" href="javascript://">RANDOM Шаблоны описаний для товаров <span class="arrow">&downarrow;</span></a>

                <div id="product_one" style="display: none;">
                    <table style="width:100%;border:1px dashed silver;">
                        <!-- SEPARATOR -->
            			<tr>
            				<th>Шаблон описания товара:<span style="color:red;">Если у товара нет описания, то в случайном порядке будет подставляться шаблон.</span></th>
            				<th></th>
            			</tr>
            			{literal}
            			<tr>
            				<th colspan=3>Список используемых тегов в описании: 
                                <ul style="padding-left:5px;">
                                    <li><strong style="color:red">{name}</strong> - название товара;</li>
                                    <li><strong style="color:red">{type_prefix}</strong> - Тип товара;</li>
                                    <li><strong style="color:red">{fullname}</strong> - Полное название товара;</li> 
                                    <li><strong style="color:red">{l-name}</strong> - название товара с маленькой буквы;</li>
                                    <li><strong style="color:red">{l-type_prefix}</strong> - Тип товара с маленькой буквы;</li>
                                    <li><strong style="color:red">{variant}</strong> - название варианта товара;</li>
                                    <li><strong style="color:red">{price}</strong> - наименьшая цена товара;</li>
                                    <li><strong style="color:red">{brand}</strong> - название бренда;</li>
                                    <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>  
                                    <li><strong style="color:red">{category}</strong> - название категории;</li>
                                    <li><strong style="color:red">{l-category}</strong> - название категории с маленькой буквы;</li>
                                    <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                    <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                    <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                    <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{all_cities}</strong> - ВСЕ областные центры в которые осуществляется доставка;</li>
                                </ul>
            				</th>
            			</tr>
            			{/literal}
            			{section name=foop start=0 loop=10 step=1}
            			<tr><td colspan=2 class="tplNum">Шаблон №{$smarty.section.foop.index+1}</td></tr>
            			<tr>
            				<td colspan=2><textarea name="product_descriptions[{$smarty.section.foop.index}]" class="editor">{$seo->product_descriptions[$smarty.section.foop.index]}</textarea></td>
            			</tr>
            			{/section}
                    </table>
                </div>
            </div>
            <div id="tabs-3">
                <table style="width:100%;border:1px dashed silver;">
        			<tr>
        				<th colspan=2><h3>Настройка мета для категории</h3></th>
        				<th rowspan="4" style="width:200px;border:1px solid silver;padding:5px;">
        					{literal}<div>
        					Список используемых тегов в описании: 
                            <ul style="padding-left:5px;">
                                <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                <li><strong style="color:red">{brand}</strong> - название бренда;</li>
                                <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>
                                <li><strong style="color:red">{skl}</strong> - фотоаппарат;</li>
                                <li><strong style="color:red">{skl_mn}</strong> - фотоаппараты;</li>
                                <li><strong style="color:red">{skl_ov}</strong> - фотоаппаратов;</li>
                                <li><strong style="color:red">{skl_ov}</strong> - фотоаппаратов;</li>
                                <li><strong style="color:red">{l-skl}</strong> - фотоаппарат c маленькой буквы;</li>
                                <li><strong style="color:red">{l-skl_mn}</strong> - фотоаппараты c маленькой буквы;</li>
                                <li><strong style="color:red">{l-skl_ov}</strong> - фотоаппаратов c маленькой буквы;</li>
                                <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{category}</strong> - название категории;</li>
                                <li><strong style="color:red">{l-category}</strong> - название категории с маленькой буквы;</li>
                            </ul>
        					</div>{/literal}
        				</th>
        			</tr>
        			<tr>
        				<th>Заголовок в категории:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="title_category">{$seo->title_category}</textarea></th>
        			</tr>
        			<tr>
        				<th>МЕТА описание категории:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="description_category">{$seo->description_category}</textarea></th>
        			</tr>
        			<tr>
        				<th>Ключевые слова в категории:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="keywords_category">{$seo->keywords_category}</textarea></th>
        			</tr>
                    {*<tr>
        				<th>Описание товара:</th>
        				<th><textarea name="full_description_category">{$seo->full_description_category}</textarea></th>
        			</tr>*}
        		</table>
                
                <ul class=new_features>
    				<li id=new_feature>
                        <table>
                        <th>
                            <select name="categories_templates[]">
        						{function name=category_select level=0}
        						{foreach from=$categories item=category}
                                <option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
                                    {category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
        						{/foreach}
        						{/function}
        						{category_select categories=$categories}
        					</select>
                        </th>    
                        <th><textarea class="editor_small" name="des_categories_templates[]"></textarea></th>
                        </table>
                    </li>
    			</ul>
                
                <div class="des_cat_templates">
                    <span class="add"><i class="dash_link" id="add_new_feature">Добавить новый шаблон</i></span>
                </div>
                
                <table>
                
                {assign 'categories_templates' unserialize($seo->categories_templates)}
                {assign 'des_categories_templates' unserialize($seo->des_categories_templates)}
                
                {foreach $categories_templates as $key=>$ct}
                    <tr>
                        <th>
                            <select name="categories_templates[]">
        						{category_select categories=$categories selected_id=intval($ct)}
        					</select>
                            <span style="" class="categories_templates_delete"><i class="dash_link">Удалить</i></span>
                        </th>
                    </tr>
                    <tr>
        				 
                        <th><textarea class="editor_small" name="des_categories_templates[]">{$des_categories_templates[$key]}</textarea></th>
                    </tr>
                {/foreach}
                </table>
                
                <!-- SEPARATOP-->
                <a class="spoiler" onclick="$('#one').slideToggle('slow');" href="javascript://">RANDOM Шаблоны описаний для категорий <span class="arrow">&downarrow;</span></a>

                <div id="one" style="display: none;">
                    <table style="width:100%;border:1px dashed silver;">
                        <!-- SEPARATOR -->
            			<tr>
            				<th>Шаблон описания категории:<span style="color:red;">Если у категории нет описания, то в случайном порядке будет подставляться шаблон.</span></th>
            				<th></th>
            			</tr>
            			{literal}
            			<tr>
            				<th colspan=3>Список используемых тегов в описании: 
                                <ul style="padding-left:5px;">
                                    <li><strong style="color:red">{skl}</strong> - фотоаппарат;</li>
                                    <li><strong style="color:red">{skl_mn}</strong> - фотоаппараты;</li>
                                    <li><strong style="color:red">{skl_ov}</strong> - фотоаппаратов;</li>
                                    <li><strong style="color:red">{l-skl}</strong> - фотоаппарат c маленькой буквы;</li>
                                    <li><strong style="color:red">{l-skl_mn}</strong> - фотоаппараты c маленькой буквы;</li>
                                    <li><strong style="color:red">{l-skl_ov}</strong> - фотоаппаратов c маленькой буквы;</li>
                                    <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                    <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                </ul>
            				<span style="color:black;font-size:12px;color:silver;">Пример: У нашей фирмы большой склад {skl_ov}. Все {skl_mn} сертифицрованы в нашей стране и имеют официальную гарантию.
            				Если вы решили купить {skl}, но у Вас есть вопросы, обращайтесь к нашим менеджерам.</span>
            				</th>
            			</tr>
            			{/literal}
            			{section name=foo start=0 loop=10 step=1}
            			<tr><td colspan=2 class="tplNum">Шаблон №{$smarty.section.foo.index+1}</td></tr>
            			<tr>
            				<td colspan=2><textarea name="descriptions[{$smarty.section.foo.index}]" class="editor">{$seo->descriptions[$smarty.section.foo.index]}</textarea></td>
            			</tr>
            			{/section}
                    </table>
                </div>	
            </div>
            
            <div id="tabs-4">
                <table style="width:100%;border:1px dashed silver;">
        			<tr>
        				<th colspan=2><h3>Настройка мета для категории + бренд</h3></th>
        				<th rowspan="4" style="width:200px;border:1px solid silver;padding:5px;">
        					{literal}<div>
        					Список используемых тегов в описании: 
                            <ul style="padding-left:5px;">
                                <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                <li><strong style="color:red">{brand}</strong> - название бренда;</li>
                                <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>
                                <li><strong style="color:red">{skl}</strong> - фотоаппарат;</li>
                                <li><strong style="color:red">{skl_mn}</strong> - фотоаппараты;</li>
                                <li><strong style="color:red">{skl_ov}</strong> - фотоаппаратов;</li>
                                <li><strong style="color:red">{l-skl}</strong> - фотоаппарат c маленькой буквы;</li>
                                <li><strong style="color:red">{l-skl_mn}</strong> - фотоаппараты c маленькой буквы;</li>
                                <li><strong style="color:red">{l-skl_ov}</strong> - фотоаппаратов c маленькой буквы;</li>
                                <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{category}</strong> - название категории;</li>
                                <li><strong style="color:red">{l-category}</strong> - название категории с маленькой буквы;</li>
                                <li><strong style="color:red">{brand}</strong> - название бренда ;</li>
                            </ul>
        					</div>{/literal}
        				</th>
        			</tr>
        			<tr>
        				<th>Заголовок в категории:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="title_category_brand">{$seo->title_category_brand}</textarea></th>
        			</tr>
        			<tr>
        				<th>МЕТА описание категории:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="description_category_brand">{$seo->description_category_brand}</textarea></th>
        			</tr>
        			<tr>
        				<th>Ключевые слова в категории:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="keywords_category_brand">{$seo->keywords_category_brand}</textarea></th>
        			</tr>
                    {*<tr>
        				<th>Описание :</th>
                    </tr>
                    <tr>
        				<th><textarea name="full_description_category_brand">{$seo->full_description_category_brand}</textarea></th>
        			</tr>*}
        		</table>
                
                <ul class=new_features_brand>
    				<li id=new_feature_brand>
                        <table>
                        <th>
                            <select name="categories_brand_templates[]">
        						
        						{category_select categories=$categories}
        					</select>
                        </th>    
                        <th><textarea class="editor_small" name="des_categories_brand_templates[]"></textarea></th>
                        </table>
                    </li>
    			</ul>
                
               {**} <div class="des_cat_templates">
                    <span class="add"><i class="dash_link" id="add_new_feature_brand">Добавить новый шаблон</i></span>
                </div>
                
                <table>
                
                {assign 'categories_brand_templates' unserialize($seo->categories_brand_templates)}
                {assign 'des_categories_brand_templates' unserialize($seo->des_categories_brand_templates)}
                
                {foreach $categories_brand_templates as $key=>$cbt}
                    <tr>
                        <th>
                            <select name="categories_brand_templates[]">
        						{category_select categories=$categories selected_id=intval($cbt)}
        					</select>
                            <span style="" class="categories_templates_delete"><i class="dash_link">Удалить</i></span>
                        </th>
                    </tr>
                    <tr>
        				 
                        <th><textarea class="editor_small" name="des_categories_brand_templates[]">{$des_categories_brand_templates[$key]}</textarea></th>
                    </tr>
                {/foreach}
                </table>
                <!-- SEPARATOP-->
                <a class="spoiler" onclick="$('#one_brand').slideToggle('slow');" href="javascript://">RANDOM Шаблоны описаний для категории+бренд <span class="arrow">&downarrow;</span></a>

                <div id="one_brand" style="display: none;">
                    <table style="width:100%;border:1px dashed silver;">
                        <!-- SEPARATOR -->
            			<tr>
            				<th>Шаблон описания категории:<span style="color:red;">Если у категории нет описания, то в случайном порядке будет подставляться шаблон.</span></th>
            				<th></th>
            			</tr>
            			{literal}
            			<tr>
            				<th colspan=3>Список используемых тегов в описании: 
                                <ul style="padding-left:5px;">
                                    <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                    <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                    <li><strong style="color:red">{brand}</strong> - название бренда;</li>
                                    <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>
                                    <li><strong style="color:red">{skl}</strong> - фотоаппарат;</li>
                                    <li><strong style="color:red">{skl_mn}</strong> - фотоаппараты;</li>
                                    <li><strong style="color:red">{skl_ov}</strong> - фотоаппаратов;</li>
                                    <li><strong style="color:red">{l-skl}</strong> - фотоаппарат c маленькой буквы;</li>
                                    <li><strong style="color:red">{l-skl_mn}</strong> - фотоаппараты c маленькой буквы;</li>
                                    <li><strong style="color:red">{l-skl_ov}</strong> - фотоаппаратов c маленькой буквы;</li>
                                    <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                    <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{category}</strong> - название категории;</li>
                                    <li><strong style="color:red">{l-category}</strong> - название категории с маленькой буквы;</li>
                                </ul>
            				    <span style="color:black;font-size:12px;color:silver;">Пример: У нашей фирмы большой склад {skl_ov}. Все {skl_mn} сертифицрованы в нашей стране и имеют официальную гарантию.
            				        Если вы решили купить {skl}, но у Вас есть вопросы, обращайтесь к нашим менеджерам.
                                </span>
            				</th>
            			</tr>
            			{/literal}
            			{section name=random_brand start=0 loop=10 step=1}
            			<tr><td colspan=2 class="tplNum">Шаблон №{$smarty.section.random_brand.index+1}</td></tr>
            			<tr>
            				<td colspan=2><textarea name="descriptions_brand[{$smarty.section.random_brand.index}]" class="editor">{$seo->descriptions_brand[$smarty.section.random_brand.index]}</textarea></td>
            			</tr>
            			{/section}
                    </table>
                </div>	
             </div>
             
             <div id="tabs-5">
                <table style="width:100%;border:1px dashed silver;">
        			<tr>
        				<th colspan=2><h3>Настройка мета для брендов</h3></th>
        				<th rowspan="4" style="width:200px;border:1px solid silver;padding:5px;">
        					{literal}<div>
        					Список используемых тегов в описании: 
                            <ul style="padding-left:5px;">
                                <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                <li><strong style="color:red">{brand}</strong> - название бренда;</li>
                                <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>
                                <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                            </ul>
        					</div>{/literal}
        				</th>
        			</tr>
        			<tr>
        				<th>Заголовок в бренде:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="title_brand">{$seo->title_brand}</textarea></th>
        			</tr>
        			<tr>
        				<th>МЕТА описание бренда:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="description_brand">{$seo->description_brand}</textarea></th>
        			</tr>
        			<tr>
        				<th>Ключевые слова в бренде:</th>
                    </tr>
                    <tr>
        				
        				<th><textarea class="" name="keywords_brand">{$seo->keywords_brand}</textarea></th>
        			</tr>
                    {*<tr>
        				<th>Описание товара:</th>
        				<th><textarea name="full_description_category">{$seo->full_description_category}</textarea></th>
        			</tr>*}
        		</table>
                
                {*<ul class=new_features_b>
    				<li id=new_feature_b>
                        <table>
                        <th>
                            <select name="brands_templates[]">
        						{function name=category_select level=0}
        						{foreach from=$categories item=category}
                                <option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
                                    {category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
        						{/foreach}
        						{/function}
        						{category_select categories=$categories}
        					</select>
                        </th>    
                        <th><textarea class="editor_small" name="des_categories_templates[]"></textarea></th>
                        </table>
                    </li>
    			</ul>
                
                <div class="des_cat_templates">
                    <span class="add"><i class="dash_link" id="add_new_feature">Добавить новый шаблон</i></span>
                </div>
                
                <table>
                
                {assign 'categories_templates' unserialize($seo->categories_templates)}
                {assign 'des_categories_templates' unserialize($seo->des_categories_templates)}
                
                {foreach $categories_templates as $key=>$ct}
                    <tr>
                        <th>
                            <select name="categories_templates[]">
        						{category_select categories=$categories selected_id=intval($ct)}
        					</select>
                            <span style="" class="categories_templates_delete"><i class="dash_link">Удалить</i></span>
                        </th>
                    </tr>
                    <tr>
        				 
                        <th><textarea class="editor_small" name="des_categories_templates[]">{$des_categories_templates[$key]}</textarea></th>
                    </tr>
                {/foreach}
                </table>
                *}
                <!-- SEPARATOP-->
                <a class="spoiler" onclick="$('#brand_one').slideToggle('slow');" href="javascript://">RANDOM Шаблоны описаний для брендов <span class="arrow">&downarrow;</span></a>

                <div id="brand_one" style="">
                    <table style="width:100%;border:1px dashed silver;">
                        <!-- SEPARATOR -->
            			<tr>
            				<th>Шаблон описания бренда:<span style="color:red;">Если у бренда нет описания, то в случайном порядке будет подставляться шаблон.</span></th>
            				<th></th>
            			</tr>
            			{literal}
            			<tr>
            				<th colspan=3>Список используемых тегов в описании: 
                                <ul style="padding-left:5px;">
                                    <li><strong style="color:red">{shopname}</strong> - название магазина;</li>
                                    <li><strong style="color:red">{phones}</strong> - телефоны магазина;</li>
                                    <li><strong style="color:red">{brand}</strong> - название бренда;</li> 
                                    <li><strong style="color:red">{rus_brand}</strong> - название бренда на русском;</li>   
                                    <li><strong style="color:red">{shopcity}</strong> - город в котором находится магазин;</li>
                                    <li><strong style="color:red">{region_cities}</strong> - города области в которые осуществляется доставка;</li>
                                    <li><strong style="color:red">{regions}</strong> - областные центры в которые осуществляется доставка;</li>
                                </ul>
            				</th>
            			</tr>
            			{/literal}
            			{section name=foob start=0 loop=10 step=1}
            			<tr><td colspan=2 class="tplNum">Шаблон №{$smarty.section.foob.index+1}</td></tr>
            			<tr>
            				<td colspan=2><textarea name="brand_descriptions[{$smarty.section.foob.index}]" class="editor">{$seo->brand_descriptions[$smarty.section.foob.index]}</textarea></td>
            			</tr>
            			{/section}
                    </table>
                </div>	
            </div>
        </div>
	
		<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	</form>