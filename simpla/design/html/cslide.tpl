<link href="design/css/slides.css" rel="stylesheet" type="text/css" />
{capture name=tabs}
	<li><a href="index.php?module=SlidesAdmin">Слайдер</a></li>
	<li  class="active"><a href="index.php?module=CSlidesAdmin">Слайдер в категориях</a></li>
{/capture}

{if $slide->id}
{$meta_title = $slide->name scope=parent}
{else}
{$meta_title = 'Новый слайд' scope=parent}
{/if}

{* On document load *}
{literal}
<script>
$(function() {

	// Удаление изображений
	$("a.button_slide_delete").click( function() {
		$("input[name='delete_image']").val('1');
		$(this).closest("ul").fadeOut(200, function() { $(this).remove(); });
		return false;
	});

});
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Слайд добавлен{elseif $message_success=='updated'}Слайд обновлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

<!-- Основная форма -->
<form method=post  enctype="multipart/form-data">
	
	<input type=hidden name="session_id" value="{$smarty.session.id}">
		
	<ul class="slide">
		<li>
			<div><h2>{if $slide->id}Редактирование слайда{else}Добавление слайда{/if}</h2></div>
		</li>
		<li>
			<label>Название:</label>
			<div>
				<input class="name" name=name type="text" value="{$slide->name|escape}"/> 
				<input name=id type="hidden" value="{$slide->id|escape}"/> 
				<input name=url type="hidden" value="{$slide->url|escape}"/> 
			</div>
		</li>
	 
		<li>
			<label>Категория:</label>
			<div>			 
					<select name="cat">
						{function name=category_select level=0}
						{foreach from=$categories item=category}
								<option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
								{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
						{/foreach}
						{/function}
						{category_select categories=$categories selected_id=$slide->cat}
					</select>
			</div>
		</li>
		
		<li>
			<label>Бренд:</label>
			<div>
				<select name="brand">
					<option value='0' {if !$slide->brand}selected{/if} brand_name=''>Не указан</option>
						{foreach from=$brands item=brand}
							<option value='{$brand->id}' {if $slide->brand == $brand->id}selected{/if} brand_name='{$brand->name|escape}'>{$brand->name|escape}</option>
						{/foreach}
				</select>
			</div>
		</li>

		<li>
			<label>Положение:</label>
			<div>			 
					<select name="pos">					 
								<option value='' {if !$slide->pos}selected{/if}>По середине</option>
								<option value='1' {if $slide->pos == 1}selected{/if}>В левом блоке</option> 
					</select>
			</div>
		</li>
		
 
		<li>
			<label>Изображение:</label>
			<div>
				<ul>
					<li>
						<input class='upload_image' name=image type=file value="test">			
						<input type=hidden name="delete_image" value="">
						<input class="button_slide" type="submit" name="" value="{if $slide->image}Обновить{else}Загрузить{/if}" />
						
					</li>
					{if $slide->image}
					<ul>

						<li>
							<div class="tip">Файл: {$config->root_url}/{$slide->image}</div>
							<a href='#' class="button_slide_delete">Удалить</a>
						</li>
						<li class="image">
							
							<img src="../{$slide->image}" alt="" />
						</li>
						
					</ul>
					{/if}
				</ul>
			</div>
		</li>
		<li>
			<label>Описание:</label>
			<div>
				<textarea name="description" class="description">{$slide->description|escape}</textarea>
				<div class="tip">Описание выводится внизу изображения. Можно использовать html/css/js</div>
			</div>
		</li>
		<li>
			<input class="button_slide" type="submit" name="" value="Сохранить" />
		</li>
	</ul>

</form>
<!-- Основная форма (The End) -->