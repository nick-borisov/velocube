{* Вкладки *}
{capture name=tabs}
	{if in_array('users', $manager->permissions)}<li><a href="index.php?module=UsersAdmin">Покупатели</a></li>{/if}
	{if in_array('groups', $manager->permissions)}<li><a href="index.php?module=GroupsAdmin">Группы</a></li>{/if}
	<li {if !$smarty.get.auto_discount}class="active"{/if}><a href="index.php?module=CouponsAdmin">Купоны</a></li>
    <li {if $smarty.get.auto_discount}class="active"{/if}><a href="index.php?module=CouponsAdmin&auto_discount=1">Скидки</a></li>
{/capture}

{* Title *}
{$meta_title='Купоны' scope=parent}
		
{* Заголовок *}
<div id="header">
	{if $coupons_count}
	<h1>{$coupons_count} {$coupons_count|plural:'купон':'купонов':'купона'}</h1>
	{else}
	<h1>Нет купонов</h1>
	{/if}
	<a class="add" href="{url module=CouponAdmin return=$smarty.server.REQUEST_URI}">Новый купон</a>
</div>	

{if $coupons}
<div id="main_list">
	
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->

	<form id="form_list" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
	
		<div id="list">
			{foreach $coupons as $coupon}
			<div class="{if $coupon->valid}green{/if} {if (!$coupon->ad_active && $coupon->auto_discount) || (!$coupon->active && !$coupon->auto_discount)}invisible{/if} row">
		 		<input type="hidden" name="positions[{$coupon->id}]" value="{$coupon->position}"/>
                <div class="move cell"><div class="move_zone"></div></div>
                <div class="checkbox cell">
					<input type="checkbox" name="check[]" value="{$coupon->id}"/>				
				</div>
				<div class="coupon_name cell">			 	
	 				<a href="{url module=CouponAdmin id=$coupon->id return=$smarty.server.REQUEST_URI}">{$coupon->name} {if !$coupon->auto_discount}<b>{$coupon->code}</b>{/if}</a>
				</div>
				<div class="coupon_discount cell">			 	
	 				Скидка {$coupon->value*1} {if $coupon->type=='absolute'}{$currency->sign}{else}%{/if}<br>
	 				{if $coupon->min_order_price>0}
	 				<div class="detail">
	 				Для заказов от {$coupon->min_order_price|escape} {$currency->sign}
	 				</div>
	 				{/if}
				</div>
				<div class="coupon_details cell">			 	
					{if $coupon->single}
	 				<div class="detail">
	 				Одноразовый
	 				</div>
	 				{/if}
	 				{if $coupon->usages>0}
	 				<div class="detail">
	 				Использован {$coupon->usages|escape} {$coupon->usages|plural:'раз':'раз':'раза'}
	 				</div>
	 				{/if}
	 				{if $coupon->expire}
	 				<div class="detail">
	 				{if $smarty.now|date_format:'%Y%m%d' <= $coupon->expire|date_format:'%Y%m%d'}
	 				Действует до {$coupon->expire|date}
	 				{else}
	 				Истёк {$coupon->expire|date}
	 				{/if}
	 				</div>
	 				{/if}
				</div>
				<div class="icons cell">
                    <a class="enable" data-field="{if $coupon->auto_dicsount}ad_active{else}active{/if}" title="Активен" href="#"></a>
					<a href='#' class=delete></a>
				</div>
				<div class="name cell" style='white-space:nowrap;'>
					
	 				
				</div>
				<div class="clear"></div>
			</div>
			{/foreach}
		</div>
		
	
		<div id="action">
		<label id="check_all" class="dash_link">Выбрать все</label>
	
		<span id="select">
		<select name="action">
			<option value="delete">Удалить</option>
		</select>
		</span>
	
		<input id="apply_action" class="button_green" type="submit" value="Применить">
		
		</div>
				
	</form>	

	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->
       	
</div>
{/if}

<div id="right_menu">  
    <!-- Категории товаров -->
	{function name=categories_tree level=1}
	{if $categories}
		<ul id="my_list1" class="list-right level-{$level}{if $level > 1} drop{/if}">
			{if $categories[0]->parent_id == 0}
				<li class="{if !$category->id}selected{/if} no-border" style="margin-left:0;padding-left:0;"><a href="{url category_id=null brand_id=null}" style="border:0;">Все категории</a></li>	
			{/if}
			{foreach $categories as $c}
				<li category_id="{$c->id}" class="{if $category->id == $c->id}selected{else}droppable category{/if}{if $c->subcategories} dropdown{/if}">
					<a href='{url keyword=null brand_id=null page=null category_id={$c->id}}'>{$c->name}</a>
					{if $c->subcategories}
						<span class="icon icon-plus"></span>
					{/if}
					{categories_tree categories=$c->subcategories level=$level+1}
				</li>
				
			{/foreach}
		</ul>	 
	{/if}
	{/function}
	{categories_tree categories=$categories}
	<!-- Категории товаров (The End)-->
	<p class='otstup'>&nbsp;</p>
    
    {if $brands}
	<!-- Бренды -->
	<ul id="my_list2">
		<li {if !$brand->id}class="selected"{/if}><a href="{url brand_id=null}">Все бренды</a></li>
		{foreach $brands as $b}
		<li brand_id="{$b->id}" {if $brand->id == $b->id}class="selected"{else}class="droppable brand"{/if}><a href="{url keyword=null page=null brand_id=$b->id}">{$b->name}</a></li>
		{/foreach}
	</ul>
	<p class='otstup'>&nbsp;</p>
	<!-- Бренды (The End) -->
	{/if}
</div> 

{* On document load *}
{literal}

<script>
$(function() {

	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();
    
    	// Сортировка списка
	$("#list").sortable({
		items:             ".row",
		tolerance:         "pointer",
		handle:            ".move_zone",
		scrollSensitivity: 40,
		opacity:           0.7, 
		
		helper: function(event, ui){		
			if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
			var helper = $('<div/>');
			$('input[type="checkbox"][name*="check"]:checked').each(function(){
				var item = $(this).closest('.row');
				helper.height(helper.height()+item.innerHeight());
				if(item[0]!=ui[0]) {
					helper.append(item.clone());
					$(this).closest('.row').remove();
				}
				else {
					helper.append(ui.clone());
					item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
				}
			});
			return helper;			
		},	
 		start: function(event, ui) {
  			if(ui.helper.children('.row').size()>0)
				$('.ui-sortable-placeholder').height(ui.helper.height());
		},
		beforeStop:function(event, ui){
			if(ui.helper.children('.row').size()>0){
				ui.helper.children('.row').each(function(){
					$(this).insertBefore(ui.item);
				});
				ui.item.remove();
			}
		},
		update:function(event, ui)
		{
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit(function() {
				colorize();
			});
		}
	});

	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list input[type="checkbox"][name*="check"]').attr('checked'));
	});	
    
    // Показать автокупон
	$("a.enable").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('invisible')?1:0;
        var values       = {};
        values[icon.data('field')] = state;
        console.log(line.hasClass('invisible'));
        icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'coupon', 'id': id, 'values': values, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('invisible');
				else
					line.addClass('invisible');				
			},
			dataType: 'json'
		});	
		return false;	
	});


	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest(".row").find('input[type="checkbox"][name*="check"]').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').prop('selected', true);
		$(this).closest("form").submit();
	});
		
	// Подтверждение удаления
	$("form").submit(function() {
		if($('#list input[type="checkbox"][name*="check"]:checked').length>0)
			if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
				return false;	
	});
});

</script>
{/literal}