{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
	<li class="active"><a href="index.php?module=FiltersAdmin">Фильтры</a></li>
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{if $feature->id}
{$meta_title = $filter->name scope=parent}
{else}
{$meta_title = 'Новый фильтр' scope=parent}
{/if}

{* Подключаем Tiny MCE 
{include file='tinymce_init.tpl'}
*}
{* On document load *}
{literal}
<script>
$(function() {
    /*$('.taggroup-method').on('click', function(){
        $('.taggroupselect .taggroup-method').removeClass('active');
        $(this).addClass('active');
        //$(this).find('input').    
    });*/
    
    $(document).on('click', '.roll-onoff label', function(){
        $(this).parent().find('input').removeAttr('checked');
        $(this).find('input').attr('checked', 1);
    });
    
    $('select.features_select').on('change', function(){
        var f_id = $('select.features_select :selected').val();
        var f_name = $('select.features_select :selected').text();
        var f_type = $('select.features_select :selected').data('type');
        
        $('#filter_features').append('<li class="dd-item dd3-item" data-id="'+f_id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content">'+f_name+'<div class="type"><span class="label label-default">'+f_type+'</span></div><div class="controllinks"><code class="opcity50">'+f_id+'</code><a class="delete-item" href="#"><i class="fa fa-times"></i></a><a class="toggle-item light-on" href="#"></a><input type="hidden" id="in_filter_'+f_id+'" name="filter_features[id][]" value="'+f_id+'"></div><div class="roll-onoff"><div data-toggle="buttons" class="btn-group"><label class="btn btn-default4 btn-xs on "><input type="radio" value="1" name="expand['+f_id+']">  Раскрыть</label><label class="btn btn-default4 btn-xs off active"><input type="radio" value="0" name="expand['+f_id+']" checked> Свернуть</label></div></div></div></li>');
    });
    
    $(document).on('click', '.delete-item', function(e){
        e.preventDefault();
        $(this).parents('li').remove();
        console.log($(this));
    });
    
    $('form').submit(function(){
        console.log($(this).serialize());
        //return false;
    });
    
    $("#filter_features").sortable({
		items:             ".dd-item",
		tolerance:         "pointer",
		handle:            ".move_zone",
		scrollSensitivity: 40,
		opacity:           0.7, 
		
		/*helper: function(event, ui){		
			if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
			var helper = $('<div/>');
			$('input[type="checkbox"][name*="check"]:checked').each(function(){
				var item = $(this).closest('.row');
				helper.height(helper.height()+item.innerHeight());
				if(item[0]!=ui[0]) {
					helper.append(item.clone());
					$(this).closest('.row').remove();
				}
				else {
					helper.append(ui.clone());
					item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
				}
			});
			return helper;			
		},*/	
 		start: function(event, ui) {
  			if(ui.helper.children('.dd-item').size()>0)
				$('.ui-sortable-placeholder').height(ui.helper.height());
		},
		beforeStop:function(event, ui){
			if(ui.helper.children('.dd-item').size()>0){
				ui.helper.children('.dd-item').each(function(){
					$(this).insertBefore(ui.item);
				});
				ui.item.remove();
			}
		},
		update:function(event, ui)
		{
			$("#filter_features input[name*='check']").attr('checked', false);
			$("#filter_features").ajaxSubmit(function() {
				colorize();
			});
		}
	});
});
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Фильтр добавлен{elseif $message_success=='updated'}Фильтр обновлён{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{$message_error}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}

<!-- Основная форма -->
<form method=post id=product>

	<div id="name">
		<input class="name" name=name type="text" value="{$filter->name|escape}"/> 
		<input name=id type="hidden" value="{$filter->id|escape}"/> 
	</div> 

	<!-- Левая колонка свойств товара -->
	<div id="column_left">
			
		<!-- Категории -->	
		<div class="block">
            <ul id="filter_features" class="dd-list property-list ui-sortable">
                {foreach $filter_features as $ff}
                <li class="dd-item dd3-item" data-id="{$ff->feature_id}">
                    <div class="dd-handle dd3-handle move_zone ui-sortable-handle"></div>
                    <div class="dd3-content">{$features[$ff->feature_id]->name}
                        <div class="type">
                            <span class="label label-default">{$features[$ff->feature_id]->mode}</span>
                        </div>
                        <div class="controllinks">
                            <code class="opcity50">{$ff->feature_id}</code>
                            <a class="delete-item" href="#">
                                <i class="fa fa-times"></i>
                            </a>
                            <a class="toggle-item light-on" href="#"></a>
                            <input type="hidden" id="in_filter_{$ff->feature_id}" name="filter_features[id][]" value="{$ff->feature_id}"/>
                        </div>
                        <div class="roll-onoff">
							<div data-toggle="buttons" class="btn-group">
								<label class="btn btn-default4 btn-xs on {if $ff->expand}active{/if}">
									<input type="radio" value="1" name="expand[{$ff->feature_id}]" {if $ff->expand}checked{/if}/>  Раскрыть
								</label>
								<label class="btn btn-default4 btn-xs off {if !$ff->expand}active{/if}">
									<input type="radio" value="0" name="expand[{$ff->feature_id}]" {if !$ff->expand}checked{/if}> Свернуть
								</label>
							</div>
                		</div>
                    </div>
                </li>
                {/foreach}
            </ul>
			<select name="features" class="features_select">
                {foreach $features as $feature}
                <option value="{$feature->id}" data-type="{$feature->mode}">{$feature->name}</option>
                {/foreach}
            </select>
            
		</div>
        
        
        
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right" class="filter">
		
		<!-- Параметры страницы -->
		<div class="block">
			<h2>Настройки фильтра</h2>
			<ul>
                <li><input type=checkbox name=visible id=in_filter {if $filter->visible}checked{/if} value="1"> <label for=in_filter>Активен</label></li>
				
			</ul>
		</div>
		<!-- Параметры страницы (The End)-->
		<input type=hidden name='session_id' value='{$smarty.session.id}'>
		
		<div style="float: right;">
            <input class="button_green" type="submit" name="" value="Сохранить" />
        </div>
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	

</form>
<!-- Основная форма (The End) -->