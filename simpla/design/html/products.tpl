{* Вкладки *}
{capture name=tabs}
	<li class="active"><a href="{url module=ProductsAdmin keyword=null category_id=null brand_id=null filter=null page=null}">Товары</a></li>
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('pages', $manager->permissions)}<li><a href="index.php?module=MetadataPagesAdmin">Метаданные страниц</a></li>{/if}
    
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{* Title *}
{if $category}
	{$meta_title=$category->name scope=parent}
{else}
	{$meta_title='Товары' scope=parent}
{/if}

{* Поиск *}
<form method="get">
<div id="search">
	<input type="hidden" name="module" value="ProductsAdmin">
	<input class="search" type="text" name="keyword" value="{$keyword|escape}" />
	<input class="search_button" type="submit" value=""/>
</div>
</form>
	
{* Заголовок *}
<div id="header">	
	{if $products_count}
		{if $category->name || $brand->name}
			<h1>{$category->name} {$brand->name} ({$products_count} {$products_count|plural:'товар':'товаров':'товара'})</h1>
		{elseif $keyword}
			<h1>{$products_count|plural:'Найден':'Найдено':'Найдено'} {$products_count} {$products_count|plural:'товар':'товаров':'товара'}</h1>
		{else}
			<h1>{$products_count} {$products_count|plural:'товар':'товаров':'товара'}</h1>
		{/if}		
	{else}
		<h1>Нет товаров</h1>
	{/if}
	<a class="add" href="{url module=ProductAdmin return=$smarty.server.REQUEST_URI}">Добавить товар</a>
</div>	

<label>Дата с:&nbsp;</label><input type=text style="width:100px;" name=filter[date_from] value=''>&nbsp;
<label>По:&nbsp;</label><input type=text style="width:100px;" name=filter[date_to] value=''>&nbsp;
<label>User:&nbsp;</label><select name="manager" style="margin-right: 5px;">  <option value="all">Все</option>{foreach $managers as $m}<option value-"{$m->login}">{$m->login}</option>{/foreach}
&nbsp;&nbsp;
<input id="start" class="button_green" type="submit" value="Экспорт товаров">
<div id='progressbar'></div>
<div id="main_list">
	
	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->
		
	{if $products}
    
	<div id="expand">
	<!-- Свернуть/развернуть варианты -->
    
	<a href="#" class="dash_link" id="expand_all">Развернуть все варианты ↓</a>
	<a href="#" class="dash_link" id="roll_up_all" style="display:none;">Свернуть все варианты ↑</a>
	<!-- Свернуть/развернуть варианты (The End) -->
	</div>

	{* Основная форма *}
	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
	
		<div id="list">
		{foreach $products as $product}
		<div class="{if !$product->visible}invisible{/if} {if $product->featured}featured{/if} {if $product->new}new{/if} {if $product->warranty}warranty{/if} {if $product->seller_warranty}s_warranty{/if} {if $product->store}store{/if} {if $product->pickup}pickup{/if} {if $product->ttoday}ttoday{/if} {if $product->yandex}yandex{/if} {if $product->k50}k50{/if} {if $product->priceru}priceru{/if} {if $product->own_text}own_text{/if} row">
			<input type="hidden" name="positions[{$product->id}]" value="{$product->position}"/>
			<div class="move cell"><div class="move_zone"></div></div>
	 		<div class="checkbox cell">
				<input type="checkbox" name="check[]" value="{$product->id}"/>				
			</div>
			<div class="image cell">
				{$image = $product->images|@first}
				{if $image}
				<a href="{url module=ProductAdmin id=$product->id return=$smarty.server.REQUEST_URI}"><img src="{$image->filename|escape|resize:55:55}" id="image_{$product@iteration}" class="magnific-image" large-img="{$image->filename|resize:600}"/></a>
				{/if}
			</div>
			<div class="name product_name cell" >
			 	
                  <div class="icons cell">
                    
    				<a class="preview"   title="Предпросмотр в новом окне" href="../products/{$product->url}" target="_blank"></a>			
    				<a class="enable"    title="Активен"                 href="#"></a>
    				<a class="featured"  title="Рекомендуемый"           href="#"></a>			
    				<a class="duplicate" title="Дублировать"             href="#"></a>
    				<a class="delete"    title="Удалить"                 href="#"></a>
    				
    								<!--a class="new"    	title="Новинка"                 href="#">N</a-->
    				<a class="yandex"    title="В Яндекс"                href="#"></a>
                    <a class="priceru"    title="В Price.ru"                href="#"></a>
    				<!--<a class="google"    title="В Гугл"                href="#"></a>-->
    				<a class="k50"    title="В K50.ru"                href="#"></a>
    				<a class="warranty"    	title="Гарантия производителя" href="#"></a>	
    				<a class="ttoday"    	title="Товар дня"                 href="#">T</a>
    				<a class="own_text"    	title="Cвой текст" href="#">OT</a>
                    <a class="seller_warranty"    	title="Гарантия продавца" href="#"></a>
                    			
    				{*<a class="store"    	title="Розничный магазин" href="#"></a>	
                    <a class="pickup"    	title="Забрать самостоятельно" href="#"></a>*}	
    
    			</div>
                <a href="{url module=ProductAdmin id=$product->id return=$smarty.server.REQUEST_URI}" style="margin-bottom: 10px;display: block;">{$product->type_prefix|escape} {$bbrands[$product->brand_id]->name|escape} {$product->name|escape}</a>
                
			 	<div class="variants">
    			 	<ul>
    				{foreach $product->variants as $variant}
        				<li {if !$variant@first}class="variant" style="display:none;"{/if}>
        					
                            <input type="text" class="price" name="sku[{$variant->id}]" title="Артикул" value="{$variant->sku}" style="background-color: lightgrey;" />  
                            
        					<input class="price {if $variant->compare_price>0}compare_price{/if}" type="text" name="price[{$variant->id}]" value="{$variant->admprice}" {if $variant->compare_price>0}title="Старая цена &mdash; {$variant->compare_price} {$currency->sign}"{/if} />{if $variant->strat == 1}<b style="color:red">СТ</b> {/if}{$currency->sign}
                            <input name="compare_price[{$variant->id}]" type="text" class="price" title="Старая цена"  value="{$variant->compare_price}" style="background-color: rgba(249, 135, 135, 0.56);" /> 
                            	{if in_array('stockprice', $manager->permissions)}<input name="stockprice[{$variant->id}]" type="text" class="price" title="Оптовая цена"  value="{$variant->stockprice}" style="background-color: rgba(105, 180, 105, 0.69);" />{/if}  					
        					<input class="stock" type="text" name="stock[{$variant->id}]" value="{if $variant->infinity}∞{else}{$variant->stock}{/if}" title="{$settings->units}"/>   
                            <i title="{$variant->name|escape}">{$variant->name|escape}</i>
                            <span class="variant_viewed">({$variant->viewed})</span>
        				</li>
    				{/foreach}
                    {$variants_num = $product->variants|count}
        				{if $variants_num>1}
            				<div class="expand_variant">
                            
                            {if $variant->mprice > 0}
                            
            					<a target="_blank" href="{$variant->murl}">Карточка</a> <span>Мин: <b style="color:orange">{$variant->mprice}</b> {$currency->sign}</span>
            				{/if}
                            
            				<a class="dash_link expand_variant" href="#">{$variants_num} {$variants_num|plural:'вариант':'вариантов':'варианта'} ↓</a>
            				<a class="dash_link roll_up_variant" style="display:none;" href="#">{$variants_num} {$variants_num|plural:'вариант':'вариантов':'варианта'} ↑</a>
            				
                            </div>
        				
                        {/if}
    				</ul>
                </div>
			</div>
			<div class="clear"></div>
		</div>
		{/foreach}
		</div>

		<div id="action">
			<label id="check_all" class="dash_link">Выбрать все</label>
		
			<span id="select">
			<select name="action">
				<option value="enable">Сделать видимыми</option>
				<option value="disable">Сделать невидимыми</option>
				<option value="change_price">Изменить цену</option>
				<option value="change_oldprice">Изменить старую цену</option>
				<option value="del_oldprice">Убрать старую цену</option>
				<option value="change_sklad">Изменить Наличие на складе</option>

				<option value="set_featured">Сделать рекомендуемым</option>
				<option value="unset_featured">Отменить рекомендуемый</option>
				<option value="set_new">Сделать новинкой</option>
				
				<option value="unset_new">Отменить новинку</option>
				<option value="set_warranty">Гарантия производителя</option>
				<option value="unset_warranty">Нет гарантии производителя</option>
                <option value="set_seller_warranty">Гарантия продавца</option>
				<option value="unset_seller_warranty">Нет гарантии продавца</option>
				<option value="set_yandex">В Яндекс</option>
				<option value="unset_yandex">Из Яндекса</option>
				<option value="set_k50">В K50.ru</option>
				<option value="unset_k50">Из K50.ru</option>
                <option value="set_priceru">В Price.ru</option>
				<option value="unset_priceru">Из Price.ru</option>								
				<option value="duplicate">Создать дубликат</option>
				{if $pages_count>1}
				<option value="move_to_page">Переместить на страницу</option>
				{/if}
				{if $categories|count>1}
				<option value="move_to_category">Переместить в категорию</option>
				{/if}
				{if $categories|count>1}
				<option value="move_to_subcategory">Добавить дополнительно в категорию</option>
				{/if}
				{if $brands|count>0}
				<option value="move_to_brand">Указать бренд</option>
				{/if}
				{if $vendors|count>0}
				<option value="move_to_vendor">Указать поставщика</option>
				{/if}
				<option value="make_mark">Указать метку</option>
				<option value="delete">Удалить</option>
                <option value="sales_notes">Sales notes</option>
                <option value="free_delivery">Доставка бесплатно</option>
                <option value="smartbikes">В Smart-bikes.ru</option>
                <option value="unsmartbikes">Убарть галочку Smart-bikes.ru</option>
                
			</select>
			</span>

			<span id="make_mark">
			<select name="target_mark">
				{foreach $marks as $m}
				<option value="{$m->id}">{$m->name}</option>
				{/foreach}
			</select> 
			</span>
			
			
			<span id="move_to_page">
			<select name="target_page">
				{section target_page $pages_count}
				<option value="{$smarty.section.target_page.index+1}">{$smarty.section.target_page.index+1}</option>
				{/section}
			</select> 
			</span>
		
			<span id="move_to_category">
			<select name="target_category_c">
				{function name=category_select level=0}
				{foreach $categories as $category}
						<option value='{$category->id}'>{section sp $level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
						{category_select categories=$category->subcategories selected_id=$selected_id level=$level+1}
				{/foreach}
				{/function}
				{category_select categories=$categories}
			</select> 
			</span>
			
			<span id="move_to_subcategory">
			<select name="target_category_s">
				{function name=category_select level=0}
				{foreach $categories as $category}
						<option value='{$category->id}'>{section sp $level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
						{category_select categories=$category->subcategories selected_id=$selected_id level=$level+1}
				{/foreach}
				{/function}
				{category_select categories=$categories}
			</select> 
			</span>
			
			<span id="change_price" style="display:none">
			на <input name="how_many" type="text" value="" size="2" />
            <select name="how_change">
                <option value="1">руб.</option>
                <option value="2">%</option>
            </select>
			Округление  
			<SELECT NAME="round" style="width:80px">
				<OPTION VALUE="none"> не округлять</OPTION>
				<OPTION VALUE="-3"{if $round==-3} selected{/if}>до тысяч</OPTION>
				<OPTION VALUE="-2"{if $round==-2} selected{/if}>до сотен</OPTION>
				<OPTION VALUE="-1"{if $round==-1} selected{/if}>до десятков</OPTION>
				<OPTION VALUE="0"{if $round==0 && $round!='none'} selected{/if}>до целых</OPTION>
				<OPTION VALUE="1"{if $round==1} selected{/if}>1 знак после запятой</OPTION>
				<OPTION VALUE="2"{if $round==2} selected{/if}>2 знак после запятой</OPTION>
			</SELECT>
			</span>
			
			<span id="change_oldprice" style="display:none">
			на <input name="percent" type="text" value="" size="2" />%
			или <input name="percent2" type="text" value="" size="2" /> Руб
			</span>
			
			<span id="change_sklad" style="display:none">
			на <SELECT NAME="sklad_id">
				<OPTION VALUE="1"{if $sklad_id==1} selected{/if}>В наличии</OPTION>
			 
				<OPTION VALUE="2"{if $sklad_id==2} selected{/if}>Под заказ</OPTION>
				<OPTION VALUE="4"{if $sklad_id==4} selected{/if}>Предзаказ</OPTION>
				<OPTION VALUE="3"{if $sklad_id==3} selected{/if}>Ожидается</OPTION>
			</SELECT>
			</span>
			
			<span id="move_to_brand">
			<select name="target_brand">
				<option value="0">Не указан</option>
				{foreach $all_brands as $b}
				<option value="{$b->id}">{$b->name}</option>
				{/foreach}
			</select> 
			</span>
			
			<span id="move_to_vendor">
			<select name="target_vendor">
				<option value="0">Не указан</option>
				{foreach $vendors as $v}
				<option value="{$v->id}">{$v->name}</option>
				{/foreach}
			</select> 
			</span>
            
            <span id="sales_notes">
                <input type="text" name="sales_notes" class="sales_notes"/>
            </span>
            
			<input id="apply_action" class="button_green" type="submit" value="Применить">		
		</div>
		{/if}
	</form>

	<!-- Листалка страниц -->
	{include file='pagination.tpl'}	
	<!-- Листалка страниц (The End) -->	

		
		<form id="list_form2" method="get">
			<input name="pricelab" value="1" type="hidden">
			<input id="apply_action" class="button_green" type="submit" value="Загрузить из Прайслаб">	
		</form>
	
</div>


<!-- Меню -->
<div id="right_menu">
	
	<!-- Фильтры -->
	<ul>
		<li {if !$filter && !$filter_d}class="selected"{/if}><a href="{url brand_id=null category_id=null keyword=null page=null filter=null filter_d=null}">Все товары</a></li>
		<li {if $filter=='featured'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='featured'}">Рекомендуемые</a></li>
		
		<li {if $filter=='ttoday'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='ttoday'}">Товар дня</a></li>

		<li {if $filter=='own_text'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='own_text'}">Свой текст</a></li>

		<li {if $filter=='in_stock'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='in_stock'}">В наличии</a></li>

		<li {if $filter=='out_of_stock'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='out_of_stock' filter_d=null}">Не в наличии</a></li>
		
		
		<li {if $filter=='new'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='new' filter_d=null}">Новинки</a></li>
		<li {if $filter=='discounted'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='discounted' filter_d=null}">Со скидкой</a></li>
        <li {if $filter=='discounted_rev'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='discounted_rev' filter_d=null}">С отрицательной скидкой</a></li>
		<li {if $filter=='yandex'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='yandex'}">В Яндексе</a></li>
		<li {if $filter=='card'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='card' filter_d=null}">С карточкой Яндекса</a></li>
        <li {if $filter=='sklad'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='sklad' filter_d=null}">На складе</a></li>
        <li {if $filter=='store'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='store' filter_d=null}">В магазине</a></li>
        <li {if $filter=='rezerv'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='rezerv' filter_d=null}">В резерве</a></li>
        <li {if $filter=='smartbikes'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='smartbikes' filter_d=null}">Smart-bikes.ru</a></li>
        <li {if $filter=='k50'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='k50' filter_d=null}">В K50.ru</a></li>
        <li {if $filter=='priceru'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter='priceru' filter_d=null}">В Price.ru</a></li>
        <li {if $filter_d=='disc_less5'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter_d='disc_less5'}">Cкидка менее 5%</a></li>
        <li {if $filter_d=='disc_more50'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null filter_d='disc_more50'}">Cкидка более 50%</a></li>

	</ul>
	<p class='otstup'>&nbsp;</p>
		Сортировка
	<ul>
		<li {if $sort=='viewed'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null sort=viewed}">По популярности</a></li>
		<li {if $sort=='position_adm'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null sort=position_adm}">По умолчанию</a></li>
        	<li {if $sort=='position'}class="selected"{/if}><a href="{url keyword=null brand_id=null category_id=null page=null sort=position}">С учетом заказов и склада</a></li>
        <li {if $sort=='priceadm'}class="selected"{/if}><a href="{url sort='priceadm'}" class="dash_link" >ЦЕНА по возрастанию</a></li>
        <li {if $sort=='priceadm_r'}class="selected"{/if}><a href="{url sort='priceadm_r'}" class="dash_link">ЦЕНА по убыванию</a></li>
	</ul>
	<p class='otstup'>&nbsp;</p>
	<!-- Фильтры -->


	<!-- Категории товаров -->
	{function name=categories_tree level=1}
	{if $categories}
		<ul id="my_list1" class="list-right level-{$level}{if $level > 1} drop{/if}">
			{if $categories[0]->parent_id == 0}
				<li class="{if !$category->id}selected{/if} no-border" style="margin-left:0;padding-left:0;"><a href="{url category_id=null brand_id=null}" style="border:0;">Все категории</a></li>	
			{/if}
			{foreach $categories as $c}
				<li category_id="{$c->id}" class="{if $category->id == $c->id}selected{else}droppable category{/if}{if $c->subcategories} dropdown{/if}">
					<a href='{url keyword=null brand_id=null page=null category_id={$c->id}}'>{$c->name}</a>
					{if $c->subcategories}
						<span class="icon icon-plus"></span>
					{/if}
					{categories_tree categories=$c->subcategories level=$level+1}
				</li>
				
			{/foreach}
		</ul>	 
	{/if}
	{/function}
	{categories_tree categories=$categories}
	<!-- Категории товаров (The End)-->
	<p class='otstup'>&nbsp;</p>
	
	{if $brands}
	<!-- Бренды -->
	<ul id="my_list2">
		<li {if !$brand->id}class="selected"{/if}><a href="{url brand_id=null}">Все бренды</a></li>
		{foreach $brands as $b}
		<li brand_id="{$b->id}" {if $brand->id == $b->id}class="selected"{else}class="droppable brand"{/if}><a href="{url keyword=null page=null brand_id=$b->id}">{$b->name}</a></li>
		{/foreach}
	</ul>
	<p class='otstup'>&nbsp;</p>
	<!-- Бренды (The End) -->
	{/if}
	
		{if $vendors}
	<!-- Бренды -->
	<ul id="my_list3">
		<li {if !$vendor->id}class="selected"{/if}><a href="{url vendor_id=null}">Все поставщики</a></li>
		{foreach $vendors as $v}
		<li vendor_id="{$v->id}" {if $vendor->id == $v->id}class="selected"{else}class="droppable vendor"{/if}><a href="{url keyword=null page=null vendor_id=$v->id}">{$v->name}</a></li>
		{/foreach}
	</ul>
	<p class='otstup'>&nbsp;</p>
	<!-- Бренды (The End) -->
	{/if}
	
    {if $sales_notes}
	<!-- sales_notes -->
	<ul id="my_list4">
		<li {if !$sales_note}class="selected"{/if}><a href="{url sales_note=null}">Все SALES NOTES</a></li>
		{foreach $sales_notes as $sl}
		<li data-sales_note="{$sl|escape}" {if $sales_note == $sl}class="selected"{else}class="sales_note"{/if}><a href="{url keyword=null page=null sales_note=$sl|escape}">{$sl|escape}</a></li>
		{/foreach}
        <li {if $sales_note=='empty'}class="selected"{/if}><a href="{url sales_note="empty"}">Пустой SALES NOTES</a></li>
	</ul>
	<p class='otstup'>&nbsp;</p>
	<!-- sales_notes (The End) -->
	{/if}
</div>
<!-- Меню  (The End) -->


{* On document load *}
{literal}
<script src="design/js/jquery/datepicker/jquery.ui.datepicker-ru.js"></script>
<script>

$(function() {


$('input[name="filter[date_from]"]').datepicker({
regional:'ru'
});

$('input[name="filter[date_to]"]').datepicker({
regional:'ru'
});

$('input#start').click(function() {
 
    	$("#progressbar").progressbar({ value: 0 });
 		
    
		do_export();
    
	});



function do_export(page)
	{
		page = typeof(page) != 'undefined' ? page : 1;
	var  date_from = $('input[name="filter[date_from]"]').val();
	var  date_to = $('input[name="filter[date_to]"]').val();
	var  manager = $('select[name="manager"]').val();
		$.ajax({
				url: "ajax/export_products.php",
				data: {page:page, date_to:date_to, date_from:date_from, manager:manager},
				dataType: 'json',
				success: function(data){
				
				if(data && !data.end)
				{
					$("#progressbar").progressbar({ value: 100*data.page/data.totalpages });
					do_export(data.page*1+1);
				}
				else
				{	
					$("#progressbar").hide('fast');
					window.location.href = 'files/export_csvproducts/products.csv';

				}
				},
				error:function(xhr, status, errorThrown) {	
				alert(errorThrown+'\n'+xhr.responseText);
			}  				
  				
		});
	
	}

	// Сортировка списка
	$("#list").sortable({
		items:             ".row",
		tolerance:         "pointer",
		handle:            ".move_zone",
		scrollSensitivity: 40,
		opacity:           0.7, 
		
		helper: function(event, ui){		
			if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
			var helper = $('<div/>');
			$('input[type="checkbox"][name*="check"]:checked').each(function(){
				var item = $(this).closest('.row');
				helper.height(helper.height()+item.innerHeight());
				if(item[0]!=ui[0]) {
					helper.append(item.clone());
					$(this).closest('.row').remove();
				}
				else {
					helper.append(ui.clone());
					item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
				}
			});
			return helper;			
		},	
 		start: function(event, ui) {
  			if(ui.helper.children('.row').size()>0)
				$('.ui-sortable-placeholder').height(ui.helper.height());
		},
		beforeStop:function(event, ui){
			if(ui.helper.children('.row').size()>0){
				ui.helper.children('.row').each(function(){
					$(this).insertBefore(ui.item);
				});
				ui.item.remove();
			}
		},
		update:function(event, ui)
		{
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit(function() {
				colorize();
			});
		}
	});
	

	// Изменение цены
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'change_price')
			$("span#change_price").show();
		else
			$("span#change_price").hide();
	});
	
		// Изменение старой цены
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'change_oldprice')
			$("span#change_oldprice").show();
		else
			$("span#change_oldprice").hide();
	});


	// Изменение склада
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'change_sklad')
			$("span#change_sklad").show();
		else
			$("span#change_sklad").hide();
	});


	// Перенос товара на другую страницу
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'move_to_page')
			$("span#move_to_page").show();
		else
			$("span#move_to_page").hide();
	});
	$("#pagination a.droppable").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			$(ui.helper).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(ui.draggable).closest("form").find('select[name="action"] option[value=move_to_page]').attr("selected", "selected");		
			$(ui.draggable).closest("form").find('select[name=target_page] option[value='+$(this).html()+']').attr("selected", "selected");
			$(ui.draggable).closest("form").submit();
			return false;	
		}		
	});


	// Перенос товара в другую категорию
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'move_to_category')
			$("span#move_to_category").show();
		else
			$("span#move_to_category").hide();
	});
	$("#right_menu .droppable.category").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			$(ui.helper).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(ui.draggable).closest("form").find('select[name="action"] option[value=move_to_category]').attr("selected", "selected");	
            $(ui.draggable).closest("form").find('select[name=target_category] option[value='+$(this).attr('category_id')+']').attr("selected", "selected");
			$(ui.draggable).closest("form").submit();
			return false;			
		}
	});
	
	// Перенос товара в другую категорию
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'move_to_subcategory')
			$("span#move_to_subcategory").show();
		else
			$("span#move_to_subcategory").hide();
	});
	$("#right_menu .droppable.category").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			$(ui.helper).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(ui.draggable).closest("form").find('select[name="action"] option[value=move_to_subcategory]').attr("selected", "selected");	
			$(ui.draggable).closest("form").find('select[name=target_category] option[value='+$(this).attr('category_id')+']').attr("selected", "selected");
			$(ui.draggable).closest("form").submit();
			return false;			
		}
	});


	// Перенос товара в другой бренд
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'move_to_brand')
			$("span#move_to_brand").show();
		else
			$("span#move_to_brand").hide();
	});
	$("#right_menu .droppable.brand").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			$(ui.helper).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(ui.draggable).closest("form").find('select[name="action"] option[value=move_to_brand]').attr("selected", "selected");			
			$(ui.draggable).closest("form").find('select[name=target_brand] option[value='+$(this).attr('brand_id')+']').attr("selected", "selected");
			$(ui.draggable).closest("form").submit();
			return false;			
		}
	});
	
		// Перенос товара в другой вендор
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'move_to_vendor')
			$("span#move_to_vendor").show();
		else
			$("span#move_to_vendor").hide();
	});
	$("#right_menu .droppable.vendor").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			$(ui.helper).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(ui.draggable).closest("form").find('select[name="action"] option[value=move_to_vendor]').attr("selected", "selected");			
			$(ui.draggable).closest("form").find('select[name=target_vendor] option[value='+$(this).attr('vendor_id')+']').attr("selected", "selected");
			$(ui.draggable).closest("form").submit();
			return false;			
		}
	});
	
			// Указать метку
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'make_mark')
			$("span#make_mark").show();
		else
			$("span#make_mark").hide();
    });
            
	// sales_notes
	$("#action select[name=action]").change(function() {
		if($(this).val() == 'sales_notes')
			$("span#sales_notes").show();
		else
			$("span#sales_notes").hide();
            
	});
	$("#right_menu .droppable.mark").droppable({
		activeClass: "drop_active",
		hoverClass: "drop_hover",
		tolerance: "pointer",
		drop: function(event, ui){
			$(ui.helper).find('input[type="checkbox"][name*="check"]').attr('checked', true);
			$(ui.draggable).closest("form").find('select[name="action"] option[value=make_mark]').attr("selected", "selected");			
			$(ui.draggable).closest("form").find('select[name=target_mark] option[value='+$(this).attr('mark')+']').attr("selected", "selected");
			$(ui.draggable).closest("form").submit();
			return false;			
		}
	});


	// Если есть варианты, отображать ссылку на их разворачивание
	if($("li.variant").size()>0)
		$("#expand").show();


	// Раскраска строк
	function colorize()
	{
		$("#list div.row:even").addClass('even');
		$("#list div.row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();


	// Показать все варианты
	$("#expand_all").click(function() {
		$("a#expand_all").hide();
		$("a#roll_up_all").show();
		$("a.expand_variant").hide();
		$("a.roll_up_variant").show();
		$(".variants ul li.variant").fadeIn('fast');
		return false;
	});


	// Свернуть все варианты
	$("#roll_up_all").click(function() {
		$("a#roll_up_all").hide();
		$("a#expand_all").show();
		$("a.roll_up_variant").hide();
		$("a.expand_variant").show();
		$(".variants ul li.variant").fadeOut('fast');
		return false;
	});

 
	// Показать вариант
	$("a.expand_variant").click(function() {
		$(this).closest("div.cell").find("li.variant").fadeIn('fast');
		$(this).closest("div.cell").find("a.expand_variant").hide();
		$(this).closest("div.cell").find("a.roll_up_variant").show();
		return false;
	});

	// Свернуть вариант
	$("a.roll_up_variant").click(function() {
		$(this).closest("div.cell").find("li.variant").fadeOut('fast');
		$(this).closest("div.cell").find("a.roll_up_variant").hide();
		$(this).closest("div.cell").find("a.expand_variant").show();
		return false;
	});

	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', $('#list input[type="checkbox"][name*="check"]:not(:checked)').length>0);
	});	

	// Удалить товар
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').prop('selected', true);
        //console.log($(this).closest("form"));//return false;
		$("form#list_form").submit();
	});
	
	// Дублировать товар
	$("a.duplicate").click(function() {
		$('#list input[type="checkbox"][name*="check"]').prop('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]').prop('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=duplicate]').prop('selected', true);
		$(this).closest("form").submit();
	});
	
	// Показать товар
	$("a.enable").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('invisible')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'visible': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('invisible');
				else
					line.addClass('invisible');				
			},
			dataType: 'json'
		});	
		return false;	
	});

	// Сделать хитом
	$("a.featured").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('featured')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'featured': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('featured');				
				else
					line.removeClass('featured');
			},
			dataType: 'json'
		});	
		return false;	
	});

	// Сделать хитом
	$("a.new").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('new')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'new': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('new');				
				else
					line.removeClass('new');
			},
			dataType: 'json'
		});	
		return false;	
	});
	
	
	
		// Товар дня
	$("a.ttoday").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('ttoday')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'ttoday': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('ttoday');				
				else
					line.removeClass('ttoday');
			},
			dataType: 'json'
		});	
		return false;	
	});

	// свой текст
	$("a.own_text").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('own_text')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'own_text': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('own_text');				
				else
					line.removeClass('own_text');
			},
			dataType: 'json'
		});	
		return false;	
	});
	

	$("a.warranty").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('warranty')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'warranty': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('warranty');				
				else
					line.removeClass('warranty');
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    $("a.seller_warranty").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('s_warranty')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'seller_warranty': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('s_warranty');				
				else
					line.removeClass('s_warranty');
			},
			dataType: 'json'
		});	
		return false;	
	});
    
	// Сделать хитом
	$("a.yandex").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('yandex')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'yandex': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('yandex');				
				else
					line.removeClass('yandex');
			},
			dataType: 'json'
		});	
		return false;	
	});	
    
    $("a.priceru").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('priceru')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'priceru': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('priceru');				
				else
					line.removeClass('priceru');
			},
			dataType: 'json'
		});	
		return false;	
	});	
	
		// Сделать хитом
	$("a.google").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('google')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'google': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('google');				
				else
					line.removeClass('google');
			},
			dataType: 'json'
		});	
		return false;	
	});	
    
    // Сделать хитом
	$("a.k50").click(function() {
		var icon        = $(this);
		var line        = icon.closest("div.row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('k50')?0:1;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'product', 'id': id, 'values': {'k50': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.addClass('k50');				
				else
					line.removeClass('k50');
			},
			dataType: 'json'
		});	
		return false;	
	});

	// Подтверждение удаления
	$("form").submit(function() {
	   console.log($('input[name="check[]"]:checked'));
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});
	
	
	// Бесконечность на складе
	$("input[name*=stock]").focus(function() {
		if($(this).val() == '∞')
			$(this).val('');
		return false;
	});
	$("input[name*=stock]").blur(function() {
		if($(this).val() == '')
			$(this).val('∞');
	});
});

</script>
<style>
	.ui-progressbar-value { background-image: url(design/images/progress.gif); background-position:left; border-color: #009ae2;}
	#progressbar{ clear: both; height:29px; }
	#result{ clear: both; width:100%;}
	#download{ display:none;  clear: both; }
</style>
{/literal}