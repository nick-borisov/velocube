{capture name=tabs}
	<li class="active"><a href="{url module=ProductsAdmin category_id=$product->category_id return=null brand_id=null id=null}">Товары</a></li>
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{if $product->id}

{$meta_title = sprintf('%s %s %s', $product->type_prefix, $bbrand->name, $product->name) scope=parent} 

{else}
{$meta_title = 'Новый товар' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}

{* product_videos *}
<script type="text/javascript" src="../js/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" href="../js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<style>
.new_video{ldelim}
	display:none;
{rdelim}
</style>
{* /product_videos *}

{* On document load *}
{literal}
<script src="design/js/autocomplete/jquery.autocomplete-min.js"></script>
<script src="design/js/jquery.colorPicker.js"></script>
<script src="design/js/jquery.leanModal.min.js"></script>
<style>
.autocomplete-w1 {position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; }
.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:auto; min-width: 300px; overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
.autocomplete .selected { background:#F0F0F0; }
.autocomplete div { padding:2px 5px; white-space:nowrap; }
.autocomplete strong { font-weight:normal; color:#3399FF; }
</style>

<script>
$(function() {
	
	/* product_videos */
	$('.videos .add').click(function(){
		$('.new_video').clone(false).appendTo('.videos .sortable').fadeIn('slow',function(){
			$(this).removeClass('new_video').find('input').focus()
		});
	});
	$(".videos .delete").on('click', function() {
		$(this).closest(".row").fadeOut(200, function() { $(this).remove(); });
		return false;
	});
	$('.iframe').fancybox();
	/* product_videos **/
	
    //if(1){
        //$('#meta_generate').click();
    //}
    $('.button_save').on('click', function(e){
        e.preventDefault();
        //if($('#generate_meta:checked').val() && (!$('textarea[name="meta_description"]').val() || !$('input[name="meta_title"]') || !$('input[name="meta_keywords"]'))){
            seo_meta_generate();
            setTimeout(function(){$('form#product').submit()}, 1000);
        //}else
        //    $('form#product').submit();
    });
    
	$("a[rel*=leanModal]").leanModal();


	$('.marksinput').bind('change', function(){

     var n = $('.marks input:checkbox:checked');
     var itog = '';
     
     n.each(function(i, el){
					
					itog = itog + $(el).val() + '/';
					});
      itog = itog.substr(0, itog.length-1);
     $('#mks').val(itog);
     return false;

	});

	// Добавление категории
	$('#product_categories .add').click(function() {
		$("#product_categories ul li:last").clone(false).appendTo('#product_categories ul').fadeIn('slow').find("select[name*=categories]:last").focus();
		$("#product_categories ul li:last span.add").hide();
		$("#product_categories ul li:last span.delete").show();
		return false;		
	});

	// Удаление категории
	$(document).on('click', "#product_categories .delete", function() {
		$(this).closest("li").fadeOut(200, function() { $(this).remove(); });
		return false;
	});

	// Сортировка вариантов
	$("#variants_block").sortable({ items: '#variants ul' , axis: 'y',  cancel: '#header', handle: '.move_zone' });
	// Сортировка вариантов
	$("table.related_products").sortable({ items: 'tr' , axis: 'y',  cancel: '#header', handle: '.move_zone' });

	
	// Сортировка связанных товаров
	$(".sortable").sortable({
		items: "div.row",
		tolerance:"pointer",
		scrollSensitivity:40,
		opacity:0.7,
		handle: '.move_zone'
	});
		

	// Сортировка изображений
	$(".images ul").sortable({ tolerance: 'pointer'});

	// Удаление изображений
	$(".images a.delete").bind('click', function() {
		 $(this).closest("li").fadeOut(200, function() { $(this).remove(); });
		 return false;
	});
	// Загрузить изображение с компьютера
	$('#upload_image').click(function() {
		$("<input class='upload_image' name=images[] type=file multiple>").appendTo('div#add_image').focus().click();
	});
	// Или с URL
	$('#add_image_url').click(function() {
		$("<input class='remote_image' name=images_urls[] type=text value='http://'>").appendTo('div#add_image').focus().select();
	});
	
	// Добавление варианта
	var variant = $('#new_variant').clone(true).removeAttr('id');
	$('#new_variant').remove().removeAttr('id');
	$('#variants_block span.add').click(function() {
		if(!$('#variants_block').is('.single_variant'))
		{
			$(variant).clone(true).appendTo('#variants').fadeIn('slow').find("input[name*=variant][name*=name]").focus().closest('ul').find('.add_color').colorPicker({pickerDefault:""});
		}
		else
		{
			$('#variants_block .variant_name').show('slow');
			$('#variants_block').removeClass('single_variant');		
		}
		return false;		
	});
	
 
	// Удаление варианта
	$('a.del_variant').bind('click',function() {
		if($("#variants ul").size()>1)
		{
			$(this).closest("ul").fadeOut(200, function() { $(this).remove(); });
		}
		else
		{
			$('#variants_block .variant_name input[name*=variant][name*=name]').val('');
			$('#variants_block .variant_name').hide('slow');
			$('#variants_block').addClass('single_variant');
		}
		return false;
	});

	// Загрузить файл к варианту
	$('#variants_block a.add_attachment').click(function() {
		$(this).hide();
		$(this).closest('li').find('div.browse_attachment').show('fast');
		$(this).closest('li').find('input[name*=attachment]').attr('disabled', false);
		return false;		
	});
	
	// Удалить файл к варианту
	$('#variants_block a.remove_attachment').click(function() {
		closest_li = $(this).closest('li');
		closest_li.find('.attachment_name').hide('fast');
		$(this).hide('fast');
		closest_li.find('input[name*=delete_attachment]').val('1');
		closest_li.find('a.add_attachment').show('fast');
		return false;		
	});

    // Загрузить файл к варианту
    //////////////////////////////////
    var this_variant, ids;

    $('#variants_block .add_color').colorPicker({pickerDefault:""});

    $(document).on('click', '#variants_block a.add_images', function() {
        offset = $(this).offset();

        this_variant = $(this);
        image_id = $(this_variant).closest('li').find('input[type=hidden]').val();

        $('#popup_images').html('');
        $('#column_right .images ul').clone().appendTo('#popup_images');

        $('#popup_images input[type=hidden]').each(function(){
            
            var dom_id = $(this).attr('id');  
			var id = $(this).val()+''; 
            var label = $(this).next();
            //console.log(label.attr('for'));
            label.attr('for', label.attr('for')+'_m');	 
			if(id == image_id) 
                $('<input type="radio" value="' + $(this).val() + '" id="'+dom_id+'_m" checked="checked" />').insertAfter(this);
			else 
                $('<input type="radio" value="' + $(this).val() + '" id="'+dom_id+'_m" />').insertAfter(this);
			$(this).remove();
            
            //id = $(this).val()+'';
            //console.log(id);console.log(image_id);
            /*if(id === image_id){ 
                $('<input type="radio" name="image_id" value="' + $(this).val() + '" checked="checked" />').insertAfter(this);
                $(this_variant).closest('li').find('input[type=radio]').attr('cheched', true);
            }else $('<input type="radio" name="image_id" value="' + $(this).val() + '" />').insertAfter(this);
            $(this).remove();*/
        });

        $('#popup_images li').each(function() { $(this).find('a').remove(); });
        $('#popup_images ul').append('<li><input type="radio" name="image_id" value="" /></li>');

        $('#popup_images').css('top', (offset.top+28)+'px').css('left', (offset.left+0) + 'px').toggle();
        return false;
    });

    $(document).on('click', '#popup_images li', function() {
        var id = $(this).find('input[type="radio"]').val();
        //console.log( $(this).attr('checked'));
        
        if( $(this).find('input[type="radio"]').attr('checked')!=undefined ){
            
            console.log('here');
            $(this).find('input[type="radio"]').removeAttr('checked');
        }else
           $(this).find('input[type="radio"]').attr('checked', 1);     
        $(this_variant).closest('li').find('input[type=hidden]').val(id);
        if(id != '') $(this_variant).closest('li').find('img').attr('src', 'design/images/picture.png');
        else $(this_variant).closest('li').find('img').attr('src', 'design/images/picture_empty.png');
        $('#popup_images').toggle();
        
        //$('#variants_block a.add_images').click();
        $('#popup_images').hide();
        return false;
    });

    $('html').bind('click', function() {
        $('#popup_images').hide();

    });
    //////////////////////////////////

	$('#variants_block span.copy').click(function() {
    var v = $('#variants ul:first-child'),
        price = v.find('li.variant_price input').val(),
        old_price = v.find('li.variant_discount input').val(),
        stockprice = v.find('li.variant_stockprice input').val();
     
    $('#variants ul').each(function(){
        $(this).find('li.variant_price input').val(price);
        $(this).find('li.variant_discount input').val(old_price);
        $(this).find('li.variant_stockprice input').val(stockprice);
    });
    
});
	
	function show_category_features(category_id)
	{
		$('ul.prop_ul li').hide(); 
		if(categories_features[category_id] !== undefined)
		{
			$('ul.prop_ul li').filter(function(){return jQuery.inArray($(this).attr("feature_id"), categories_features[category_id])>-1;}).show();	
		}
	}
	// Изменение набора свойств при изменении категории
	$('select[name="categories[]"]:first').change(function() {
		show_category_features($("option:selected",this).val());
	});
	show_category_features($('select[name="categories[]"]:first option:selected').val());
	
	
	// Добавление нового свойства товара
	var feature = $('#new_feature').clone(true);
	$('#new_feature').remove().removeAttr('id');
	$('#add_new_feature').click(function() {
		$(feature).clone(true).appendTo('ul.new_features').fadeIn('slow').find("input[name*=new_feature_name]").focus();
		return false;		
	});
	
	// Подсказки для свойств
	$('input[name*="options"]').each(function(index) {
		f_id = $(this).closest('li').attr('feature_id');
		ac = $(this).autocomplete({
			serviceUrl:'ajax/options_autocomplete.php',
			minChars:0,
			params: {feature_id:f_id},
			noCache: false
		});
	});
		
	// Удаление связанного товара
	$(".related_products a.delete").bind('click', function() {
		 $(this).closest("div.row").fadeOut(200, function() { $(this).remove(); });
		 return false;
	});
 

	// Добавление связанного товара 
	var new_related_product = $('#new_related_product').clone(true);
	$('#new_related_product').remove().removeAttr('id');
 
	$("input#related_products").autocomplete({
		serviceUrl:'ajax/search_products.php',
		minChars:0,
		noCache: false, 
		onSelect:
			function(value, data){
				new_item = new_related_product.clone().appendTo('.related_products');
				new_item.removeAttr('id');
				new_item.find('a.related_product_name').html(data.name);
				new_item.find('a.related_product_name').attr('href', 'index.php?module=ProductAdmin&id='+data.id);
				new_item.find('input[name*="related_products"]').val(data.id);
				if(data.image)
					new_item.find('img.product_icon').attr("src", data.image);
				else
					new_item.find('img.product_icon').remove();
				$("#related_products").val(''); 
				new_item.show();
			},
		fnFormatResult:
			function(value, data, currentValue){
				var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
				var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
			}

	});
  

	// infinity
	$("input[name*=variant][name*=stock]").focus(function() {
		if($(this).val() == '∞')
			$(this).val('');
		return false;
	});

	$("input[name*=variant][name*=stock]").blur(function() {
		if($(this).val() == '')
			$(this).val('∞');
	});
	
	// Волшебные изображения
	name_changed = false;
	$("input[name=name]").change(function() {
		name_changed = true;
		images_loaded = 0;
	});	
	images_num = 8;
	images_loaded = 0;
	old_wizar_dicon_src = $('#images_wizard img').attr('src');
	$('#images_wizard').click(function() {
		
		$('#images_wizard img').attr('src', 'design/images/loader.gif');
		if(name_changed)
			$('div.images ul li.wizard').remove();
		name_changed = false;
		key = $('input[name=name]').val();
		$.ajax({
 			 url: "ajax/get_images.php",
 			 	data: {keyword: key, start: images_loaded},
 			 	dataType: 'json',
  				success: function(data){
    				for(i=0; i<Math.min(data.length, images_num); i++)
    				{
	    				image_url = data[i];
						$("<li class=wizard><a href='' class='delete'><img src='design/images/cross-circle-frame.png'></a><a href='"+image_url+"' target=_blank><img onerror='$(this).closest(\"li\").remove();' width=100 src='"+image_url+"' /><input name=images_urls[] type=hidden value='"+image_url+"'></a></li>").appendTo('div .images ul');
    				}
					$('#images_wizard img').attr('src', old_wizar_dicon_src);
					images_loaded += images_num;
  				}
		});
		return false;
	});
	
	// Волшебное описание
	name_changed = false;
	$("input[name=name]").change(function() {
		name_changed = true;
	});	
	old_prop_wizard_icon_src = $('#properties_wizard img').attr('src');
	$('#properties_wizard').click(function() {
		
		$('#properties_wizard img').attr('src', 'design/images/loader.gif');
		if(name_changed)
			$('div.images ul li.wizard').remove();
		name_changed = false;
		key = $('input[name=name]').val();
		$.ajax({
 			 url: "ajax/get_info.php",
 			 	data: {keyword: key},
 			 	dataType: 'json',
  				success: function(data){
  					if(data)
  					{
  						$('li#new_feature').remove();
	    				for(i=0; i<data.options.length; i++)
	    				{
	    					option_name = data.options[i].name;
	    					option_value = data.options[i].value;
							// Добавление нового свойства товара
							exists = false;
							if(!$('label.property:visible:contains('+option_name+')').closest('li').find('input[name*=options]').val(option_value).length)
							{
								f = $(feature).clone(true);
								f.find('input[name*=new_features_names]').val(option_name);
								f.find('input[name*=new_features_values]').val(option_value);
								f.appendTo('ul.new_features').fadeIn('slow');
							}
	   					}
	   					
   					}
					$('#properties_wizard img').attr('src', old_prop_wizard_icon_src);
					
				},
				error: function(xhr, textStatus, errorThrown){
                	alert("Error: " +textStatus);
           		}
		});
		return false;
	});
	

	// Автозаполнение мета-тегов
	meta_title_touched = true;
	meta_keywords_touched = true;
	meta_description_touched = true;
	url_touched = true;
    fullname_touched = true;
	
	if($('input[name="meta_title"]').val() == generate_meta_title() || $('input[name="meta_title"]').val() == '')
		meta_title_touched = false;
	if($('input[name="meta_keywords"]').val() == generate_meta_keywords() || $('input[name="meta_keywords"]').val() == '')
		meta_keywords_touched = false;
	if($('textarea[name="meta_description"]').val() == generate_meta_description() || $('textarea[name="meta_description"]').val() == '')
		meta_description_touched = false;
	if($('input[name="url"]').val() == generate_url() || $('input[name="url"]').val() == '')
		url_touched = false;
        
    if($('input[name="fullname"]').val() == generate_fullname() || $('input[name="fullname"]').val() == '  ')
		fullname_touched = false;    
		
	$('input[name="meta_title"]').change(function() { meta_title_touched = true; });
	$('input[name="meta_keywords"]').change(function() { meta_keywords_touched = true; });
	$('textarea[name="meta_description"]').change(function() { meta_description_touched = true; });
	$('input[name="url"]').change(function() { url_touched = true; });
	
	$('input[name="name"]').keyup(function() { set_meta(); });
    $('input[name="type_prefix"]').keyup(function() { set_meta(); });
	$('select[name="brand_id"]').change(function() { set_meta(); });
	$('select[name="categories[]"]').change(function() { set_meta(); });
	
});

function set_meta()
{
	if(!meta_title_touched)
		$('input[name="meta_title"]').val(generate_meta_title());
	if(!meta_keywords_touched)
		$('input[name="meta_keywords"]').val(generate_meta_keywords());
	if(!meta_description_touched)
		$('textarea[name="meta_description"]').val(generate_meta_description());
	if(!url_touched)
		$('input[name="url"]').val(generate_url());
    if(!fullname_touched)
		$('input[name="fullname"]').val(generate_fullname());    
}

function generate_meta_title()
{
	name = $('input[name="name"]').val();
	return name;
}

function generate_meta_keywords()
{
	name = $('input[name="name"]').val();
	result = name;
	brand = $('select[name="brand_id"] option:selected').attr('brand_name');
	if(typeof(brand) == 'string' && brand!='')
			result += ', '+brand;
	$('select[name="categories[]"]').each(function(index) {
		c = $(this).find('option:selected').attr('category_name');
		if(typeof(c) == 'string' && c != '')
    		result += ', '+c;
	}); 
	return result;
}

function generate_fullname()
{
	name = $('input[name="name"]').val();
    type_prefix = $('input[name="type_prefix"]').val();
	result = type_prefix;
	brand = $('select[name="brand_id"] option:selected').attr('brand_name');
	if(typeof(brand) == 'string' && brand!='')
			result += ' '+brand;
    result += ' '+name;        
	return result;
}

function generate_meta_description()
{
	if(typeof(tinyMCE.get("annotation")) =='object')
	{
		description = tinyMCE.get("annotation").getContent().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ").replace(/^\s+|\s+$/g, '').substr(0, 512);
		return description;
	}
	else
		return $('textarea[name=annotation]').val().replace(/(<([^>]+)>)/ig," ").replace(/(\&nbsp;)/ig," ").replace(/^\s+|\s+$/g, '').substr(0, 512);
}

function generate_url()
{
	url = $('input[name="type_prefix"]').val()+' '+$('select[name="brand_id"] option:selected').html()+' '+$('input[name="name"]').val();
	url = url.replace(/[\s]+/gi, '-');
	url = translit(url);
	url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();	
	return url;
}

function translit(str)
{
	var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")   
	var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")   
 	var res = '';
	for(var i=0, l=str.length; i<l; i++)
	{ 
		var s = str.charAt(i), n = ru.indexOf(s); 
		if(n >= 0) { res += en[n]; } 
		else { res += s; } 
    } 
    return res;  
}

</script>

<style>
.ui-autocomplete{
background-color: #ffffff; width: 100px; overflow: hidden;
border: 1px solid #e0e0e0;
padding: 5px;
}
.ui-autocomplete li.ui-menu-item{
overflow: hidden;
white-space:nowrap;
display: block;
}
.ui-autocomplete a.ui-corner-all{
overflow: hidden;
white-space:nowrap;
display: block;
}

</style>
{/literal}



{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Товар добавлен{elseif $message_success=='updated'}Товар изменен{else}{$message_success|escape}{/if}</span>
	<a class="link" target="_blank" href="../products/{$product->url}">Открыть товар на сайте</a>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Товар с таким адресом уже существует{elseif $message_error=='empty_name'}Введите название{else}{$message_error|escape}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">

 	<div id="name" class="p_second_bar">
        <div class="name">Тип товара</div>
        <input name="type_prefix" type="text" value="{$product->type_prefix|escape}" />
		<input name=id type="hidden" value="{$product->id|escape}"/> 
        <div class="galochki">
            <div class="verh">
        		<div class="checkbox">
        			<input name=visible value='1' type="checkbox" id="active_checkbox" {if $product->visible}checked{/if}/> <label for="active_checkbox">Активен</label>
        		</div>
        		<div class="checkbox">
        			<input name=featured value="1" type="checkbox" id="featured_checkbox" {if $product->featured}checked{/if}/> <label for="featured_checkbox">Реком.</label>
        		</div>
        		<div class="checkbox">
        			<input name=new value="1" type="checkbox" id="new_checkbox" {if $product->new}checked{/if}/> <label for="new_checkbox">Новинка</label>
        		</div>
        		<div class="checkbox">
        			<input name=yandex value="1" type="checkbox" id="yandex_checkbox" {if $product->yandex}checked{/if}/> <label for="yandex_checkbox">В Яндексе</label>
        		</div>				
        		<div class="checkbox">
        			<input name=warranty value="1" type="checkbox" id="yandex_warr" {if $product->warranty}checked{/if}/> <label for="yandex_warr">Гар. произв.</label>
        		</div>
                <div class="checkbox">
        			<input name=k50 value="1" type="checkbox" id="k50" {if $product->k50}checked{/if}/> <label for="k50">K50</label>
        		</div>
        		<div class="checkbox">
        			<input name=own_text value="1" type="checkbox" id="own_text" {if $product->own_text}checked{/if}/> <label for="own_text">свой текст</label>
        		</div>
                
                
            </div>
            <div class="niz">
                {*<div class="checkbox">
                    <input name="store" type="checkbox" id="yandex_store" {if $product->store}checked{/if} value="1"/>
                    <label for="yandex_store">Розничный магазин</label>
                </div> *}   
                <div class="checkbox">    
                    <input name="seller_warranty" type="checkbox" id="yandex_seller" {if $product->seller_warranty}checked{/if}  value="1"/>
                    <label for="yandex_seller">Гарантия от продавца</label>
                </div>
                <div class="checkbox">
        			<input name=smartbikes value="1" type="checkbox" id="smartbikes" {if $product->smartbikes}checked{/if}/> 
                    <label for="smartbikes">Smart-bikes.ru</label>
        		</div>
                <div class="checkbox">
        			<input name=generate_meta value="2" type="checkbox" id="generate_meta" {if $product->generate_meta!=2}checked{/if}/> 
                    <label for="generate_meta">Генерация мета</label>
        		</div>
                <div class="checkbox">
        			<input name="priceru" value="1" type="checkbox" id="priceru" {if $product->priceru}checked{/if}/> <label for="priceru">В Price.ru</label>
        		</div>
            </div>			
        </div>	
	</div> 
    
	<div class="p_second_bar">
        <span style="display: table-cell;width: 52px;">Модель </span>
		<input class="name" name=name type="text" value="{$product->name|escape}"/> 
        <input name="fullname" type="text" disabled class="fullname" value="{$product->fullname|escape}"/>
        
        
    </div> 
	<div id="product_brand" {if !$brands}style='display:none;'{/if}>
		<label>Бренд</label>
		<select name="brand_id">
            <option value='0' {if !$product->brand_id}selected{/if} brand_name=''>Не указан</option>
       		{foreach from=$brands item=brand}
            	<option value='{$brand->id}' {if $product->brand_id == $brand->id}selected{/if} brand_name='{$brand->name|escape}' rus_brand='{$brand->rus_name|escape}'>{$brand->name|escape}</option>
        	{/foreach}
		</select>
	</div>
	
	<div id="product_categories" {if !$categories}style='display:none;'{/if}>
		<label>Категория</label>
		<div>
			<ul>
				{foreach name=categories from=$product_categories item=product_category}
				<li>
					<select name="categories[]">
						{function name=category_select level=0}
						{foreach from=$categories item=category}
								<option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
								{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
						{/foreach}
						{/function}
						{category_select categories=$categories selected_id=$product_category->id}
					</select>
					<span {if not $smarty.foreach.categories.first}style='display:none;'{/if} class="add"><i class="dash_link">Дополнительная категория</i></span>
					<span {if $smarty.foreach.categories.first}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
				</li>
				{/foreach}		
			</ul>
		</div>
	</div>
	<div id="variants_block2">
	<ul>
 
		<li> 
			<div>Поставщик</div>
			<select name="vendor">
				<option value="">Не указан</option>
			{foreach $vendors as $ven}
				<option {if $ven->id == $product->vendor}selected{/if} value="{$ven->id}">{$ven->name}</option>
			{/foreach}
			</select>
            {if $product->last_parse}<span><b>Последнее обновление: </b>{$product->last_parse}</span>{/if}
		</li>
		
		<li>
			<div>Менеджер</div>
			<input name="manger" value="{$product->manager}" disabled>
		</li>
		
		<li>
			<div>Добавлен</div>
			<input name="created" value="{$product->created}" disabled>
		</li>
 
        <li>
        <div>Наличие на складе: </div>					
        	<select name="sklad_id">
        		<option value='0' {if $product->sklad_id == '0' or !$product->sklad_id}selected{/if}>По складу</option>		 
        		<option value='2' {if $product->sklad_id == '2'}selected{/if}>Под заказ</option>
        		<option value='4' {if $product->sklad_id == '4'}selected{/if}>Предзаказ</option>
        		<option value='3' {if $product->sklad_id == '3'}selected{/if}>Ожидается</option>
        	</select>  
        	</li> 
        <li>
            <div>Ожидается:</div>
            <input name="byrequest"  style="width:50px;"        type="text"   value="{$product->byrequest}" />  
        </li> 
        <li>
            <div>Рейтинг:</div><input name="rating"  style="width:50px;"        type="text"   value="{$product->rating}" />  </li> 
        <li>
            <div>Голосов:</div><input name="votes"  style="width:50px;"   type="text"   value="{$product->votes}" />  </li>  
        <li>
            <div>Текст. атрибут:</div><input name="atribut"         type="text"   value="{$product->atribut}" /> </li>  
        <li>
            <div>Количество покупок:</div>
            <input name="atribut" disabled type="text" value="{$product->hits}" style="width:35px"/> 
        </li>  
    </ul>
	</div>
 	<!-- Варианты товара -->
	<div id="variants_block" {assign var=first_variant value=$product_variants|@first}{if $product_variants|@count <= 1 && !$first_variant->name}class=single_variant{/if}>
		<ul id="header">
			<li class="variant_move"></li>
            <li class="variant_name">Вариант </li>
            <li class="variant_color"></li>
            <li class="variant_images"></li>
            <li class="variant_sku">Артикул</li>
			<li class="variant_price">Цена, {$currency->sign}</li>	
			<li class="variant_discount">Старая, {$currency->sign}</li>	
			{if in_array('stockprice', $manager->permissions)}<li class="variant_discount">Опт</li>{/if}
			<li class="variant_amount">Кол-во</li>
            <li class="variant_sklad-magaz">Склад/Магаз</li>
		</ul>
		<div id="variants">
		{foreach from=$product_variants item=variant}
		<ul>
			<li class="variant_move"><div class="move_zone"></div></li>
			<li class="variant_name">      <input name="variants[id][]"            type="hidden" value="{$variant->id|escape}" /><input name="variants[name][]" type="" value="{$variant->name|escape}" /> <a class="del_variant" href=""><img src="design/images/cross-circle-frame.png" alt="" /></a></li>
            <li class="variant_color"><input name="variants[color_code][]" type="hidden" value="{$variant->color_code|escape}" class="add_color" /></li>
            <li class="variant_images"><a href='#' class="add_images"><img src="design/images/picture{if !$variant->image_id}_empty{/if}.png" title="Привязать изображение" /></a> <input name="variants[image_id][]" type="hidden" value="{$variant->image_id|escape}" /></li>

            <li class="variant_sku">       <input name="variants[sku][]"           type="text"   value="{$variant->sku|escape}" />
						{if $variant->murl > ''}
					<br>
					<a target="_blank" href="{$variant->murl}">Карточка Я.Маркет</a>
					{/if}
			</li>
			<li class="variant_price">     <input name="variants[price][]"   {if $variant->strat == 1}style="color:red"{/if}      type="text"   value="{if $variant->admprice > 0}{$variant->admprice|escape}{else}{$variant->price|escape}{/if}" />
			{if $variant->mprice > 0}
					<br>
					<span>Мин: <b style="color:orange">{$variant->mprice}</b></span>
					{/if}
			</li>
			<li class="variant_discount">  <input name="variants[compare_price][]" type="text"   value="{if $variant->strat != 1}{$variant->compare_price|escape}{/if}" /></li>
			{if in_array('stockprice', $manager->permissions)}<li class="variant_discount variant_stockprice">  <input name="variants[stockprice][]" type="text"   value="{$variant->stockprice|escape}" /></li>{/if}
			<li class="variant_amount">    <input name="variants[stock][]"         type="text"   value="{if $variant->infinity || $variant->stock == ''}∞{else}{$variant->stock|escape}{/if}" />{$settings->units}</li>
			{*<li class="variant_download">
			
				{if $variant->attachment}
					<span class=attachment_name>{$variant->attachment|truncate:25:'...':false:true}</span>
					<a href='#' class=remove_attachment><img src='design/images/bullet_delete.png'  title="Удалить цифровой товар"></a>
					<a href='#' class=add_attachment style='display:none;'><img src="design/images/cd_add.png" title="Добавить цифровой товар" /></a>
				{else}
					<a href='#' class=add_attachment><img src="design/images/cd_add.png"  title="Добавить цифровой товар" /></a>
				{/if}
				<div class=browse_attachment style='display:none;'>
					<input type=file name=attachment[]>
					<input type=hidden name=delete_attachment[]>
				</div>
			
			</li>*}
            <li class="variant_slad-magaz">
                <select name="variants[store][]">
                    <option value="0" {if !$variant->store}selected{/if}>Не выбрано</option>
                    <option value="1" {if $variant->store==1}selected{/if}>Склад</option>
                    <option value="2" {if $variant->store==2}selected{/if}>Магазин</option>
                    <option value="3" {if $variant->store==3}selected{/if}>Резерв</option>
                </select>
            </li>
		</ul>
		{/foreach}		
		</div>
		<ul id=new_variant style='display:none;'>
			<li class="variant_move"><div class="move_zone"></div></li>
			<li class="variant_name"><input name="variants[id][]" type="hidden" value="" /><input name="variants[name][]" type="" value="" /><a class="del_variant" href=""><img src="design/images/cross-circle-frame.png" alt="" /></a></li>
            <li class="variant_color"><input name="variants[color_code][]" type="hidden" value="" class="add_color" /></li>
            <li class="variant_images"><a href='#' class="add_images"><img src="design/images/picture_empty.png" title="Привязать изображение" /></a> <input name="variants[image_id][]" type="hidden" value="" /></li>

            <li class="variant_sku"><input name="variants[sku][]" type="" value="" /></li>
			<li class="variant_price"><input  name="variants[price][]" type="" value="" /></li>
			<li class="variant_discount"><input name="variants[compare_price][]" type="" value="" /></li>
            {if in_array('stockprice', $manager->permissions)}<li class="variant_discount variant_stockprice">  <input name="variants[stockprice][]" type="text"   value="" /></li>{/if}
			<li class="variant_amount"><input name="variants[stock][]" type="" value="∞" />{$settings->units}</li>
			{*<li class="variant_download">
				<a href='#' class=add_attachment><img src="design/images/cd_add.png" alt="" /></a>
				<div class=browse_attachment style='display:none;'>
					<input type=file name=attachment[]>
					<input type=hidden name=delete_attachment[]>
				</div>
			</li>*}
            <li class="variant_slad-magaz">
                <select name="variants[store][]">
                    <option value="0" {if !$variant->store}selected{/if}>Не выбрано</option>
                    <option value="1" {if $variant->store==1}selected{/if}>Склад</option>
                    <option value="2" {if $variant->store==2}selected{/if}>Магазин</option>
                    <option value="3" {if $variant->store==3}selected{/if}>Резерв</option>
                </select>
            </li>
		</ul>

		<input class="button_green button_save" type="button" name="" value="Сохранить" />
		<span class="add" id="add_variant"><i class="dash_link">Добавить вариант</i></span>
		<span class="copy"><i class="dash_link">Скопировать цену</i></span>
 	</div>
	<!-- Варианты товара (The End)--> 
	
 	<!-- Левая колонка свойств товара -->
	<div id="column_left">
			
		<!-- Параметры страницы -->
		<div class="block layer">
			<h2>Параметры страницы</h2>
			<ul>
				<li><label class=property>Адрес</label><div class="page_url"> /products/</div><input name="url" class="page_url" type="text" value="{$product->url|escape}" /></li>
				<li><label class=property>Заголовок</label><input name="meta_title" class="simpla_inp" type="text" value="{$product->meta_title|escape}" /></li>
				<li><label class=property>Ключевые слова</label><input name="meta_keywords" class="simpla_inp" type="text" value="{$product->meta_keywords|escape}" /></li>
<!--SEO-->		<li><label class=property>Генерировать мета <a href="#" id="meta_generate" title="Сгенерировать автоматически мета-данные и описание для футера"><img src="design/images/wand.png" alt="Сгенерировать автоматически мета-данные и описание для футера" title="Сгенерировать автоматически мета-данные и описание для футера"></a></label></li>
				<li><label class=property>Описание</label><textarea name="meta_description" class="simpla_inp" />{$product->meta_description|escape}</textarea></li>
                <li><label class=property>Sales notes</label><input name="sales_notes" class="simpla_inp" type="text" value="{$product->sales_notes|escape}" maxlength="50"/></li>
			</ul>
		</div>
{literal}
<script>
	$('#meta_generate').on('click', function(e){
	   e.preventDefault();
	   seo_meta_generate();
	});
    
	function seo_meta_generate(){
		$('#meta_generate img').attr('src', 'design/images/loader.gif');
		var error = false;
		var name = $('input[name=name]').val();
		var brand = $('select[name="brand_id"] option:selected').attr('brand_name');
        var rus_brand = $('select[name="brand_id"] option:selected').attr('rus_brand');
        var fullname = $('input[name="fullname"]').val();
        var type_prefix = $('input[name="type_prefix"]').val();

		if(typeof(brand) != 'string' || brand=='')
			brand = '';
			
		var category = '';
		$('select[name="categories[]"]').each(function(index) {
			c = $(this).find('option:selected').attr('category_name');
			if(typeof(c) == 'string' && c != '')
				category = c;
		});
		
		var variants = ''; index = 0;var prices = [];
		$('.variant_name input[name="variants[name][]"]').each(function(){
		 
			if(index == 0)
			{
				variants = $(this).val();
				index = 1;
			}
			else
				variants += '_'+$(this).val();
            
            
                prices.push($(this).parent().parent().find('.variant_price input[name="variants[price][]"]').val());    

		});

        prices.sort(function(a,b){return a-b});
        var low_variant_price = prices[0];
        //console.log(low_variant_price);    
		
		var title = $('input[name=meta_title]');
		var keywords = $('input[name=meta_keywords]');
		var description = $('textarea[name=meta_description]');
		
		$.ajax({
 			url: "ajax/seo.php",
		 	data: {action:'product', name: name, title:title.val(), brand:brand, category:category, variants:variants, fullname: fullname, type_prefix: type_prefix, 'low_variant_price':low_variant_price, 'rus_brand':rus_brand},
		 	dataType: 'json',
			success: function(data){
				title.val(data['title']);
				keywords.val(data['keywords']);
				description.val(data['description']);
				//alert("Данные успешно сгенерированы");
			}
		});
		$('#meta_generate img').attr('src', 'design/images/wand.png');
		return false;
	};
    $(function(){
        $('#full_description_generate').on('click', function(e){
           e.preventDefault();
    	   seo_full_description_generate();
    	});
    });
    
    function seo_full_description_generate(){
		$('#full_description_generate img').attr('src', 'design/images/loader.gif');
		var error = false;
        var pid = $('#name input[name=id]').val();
		var name = $('input[name=name]').val();
		var brand = $('select[name="brand_id"] option:selected').attr('brand_name');
        var rus_brand = $('select[name="brand_id"] option:selected').attr('rus_brand');
        var fullname = $('input[name="fullname"]').val();
        var type_prefix = $('input[name="type_prefix"]').val();

		if(typeof(brand) != 'string' || brand=='')
			brand = '';
			
		var category = '';
		$('select[name="categories[]"]').each(function(index) {
			c = $(this).find('option:selected').attr('category_name');
			if(typeof(c) == 'string' && c != '')
				category = c;
		});
		
		var variants = ''; index = 0;var prices = [];
		$('.variant_name input[name="variants[name][]"]').each(function(){
		 
			if(index == 0)
			{
				variants = $(this).val();
				index = 1;
			}
			else
				variants += '_'+$(this).val();
            
            
                prices.push($(this).parent().parent().find('.variant_price input[name="variants[price][]"]').val());    

		});

        prices.sort(function(a,b){return a-b});
        var low_variant_price = prices[0];
        //console.log(low_variant_price);    
		
		//var title = $('input[name=meta_title]');
		//var keywords = $('input[name=meta_keywords]');
		//var full_description = $('textarea[name=body]');
		
		$.ajax({
 			url: "ajax/seo.php",
		 	data: {action:'product_description', pid: pid, name: name, brand:brand, category:category, variants:variants, fullname: fullname, type_prefix: type_prefix, 'low_variant_price':low_variant_price, 'rus_brand':rus_brand},
		 	dataType: 'json',
			success: function(data){
				//title.val(data['title']);
				//keywords.val(data['keywords']);
                if(typeof(tinyMCE.get("body")) =='object'){
                    tinyMCE.get("body").setContent(data.description);    
                }
                //console.log(data.description);
				//alert("Данные успешно сгенерированы");
			}
		});
		$('#full_description_generate img').attr('src', 'design/images/wand.png');
		return false;
	};
</script>
{/literal}
		<!-- Параметры страницы (The End)-->
		
		<!-- Свойства товара -->
		<script>
		var categories_features = new Array();
		{foreach from=$categories_features key=c item=fs}
		categories_features[{$c}]  = Array({foreach from=$fs item=f}'{$f}', {/foreach}0);
		{/foreach}
		</script>
		
		<div class="block layer" {if !$categories}style='display:none;'{/if}>
			<h2>Свойства товара
			<a href="#" id=properties_wizard><img src="design/images/wand.png" alt="Подобрать автоматически" title="Подобрать автоматически"/></a>
			</h2>
			
			<ul class="prop_ul">
				{foreach $features as $feature}
					{assign var=feature_id value=$feature->id}
                    
					<li feature_id={$feature_id} style='display:none;'>
                        <label class=property>{$feature->name}</label>
                        {$feature->prefix|escape}
                        {if $feature->strict}
                            <select name="options[{$feature_id}]" class="simpla_inp">
                                <option value="">Выбрать значение</option>
                                {foreach explode(',', $feature->allowed_values) as $fav}
                                <option value="{$fav}"{if $fav == $options.$feature_id->value}selected{/if}>{$fav}</option>
                                {/foreach}
                            </select>
                            
                        {else}
                            <input class="simpla_inp" type="text" name=options[{$feature_id}] value="{$options.$feature_id->value|escape}" />
                        {/if}
                        {$feature->postfix|escape}
                    </li>
				{/foreach}
			</ul>
			<!-- Новые свойства -->
			<ul class=new_features>
				<li id=new_feature><label class=property><input type=text name=new_features_names[]></label><input class="simpla_inp" type="text" name=new_features_values[] /></li>
			</ul>
			<span class="add"><i class="dash_link" id="add_new_feature">Добавить новое свойство</i></span>
			<input class="button_green button_save" type="button" name="" value="Сохранить" />			
		</div>
		
		<!-- Свойства товара (The End)-->
		
		{*
		<!-- Экспорт-->
		<div class="block">
			<h2>Экспорт товара</h2>
			<ul>
				<li><input id="exp_yad" type="checkbox" /> <label for="exp_yad">Яндекс Маркет</label> Бид <input class="simpla_inp" type="" name="" value="12" /> руб.</li>
				<li><input id="exp_goog" type="checkbox" /> <label for="exp_goog">Google Base</label> </li>
			</ul>
		</div>
		<!-- Свойства товара (The End)-->
		*}
			
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right">
		
		<!-- Изображения товара -->	
		<div class="block layer images">
			<h2>Изображения товара
			<a href="#" id=images_wizard><img src="design/images/wand.png" alt="Подобрать автоматически" title="Подобрать автоматически"/></a>
			
			</h2>
			<ul>{foreach from=$product_images item=image}<li>
					<a href='#' class="delete"><img src='design/images/cross-circle-frame.png'></a>
					
                    <input type=hidden name='images[]' value='{$image->id}' id="image_id_{$image->id}">
                    <label for="image_id_{$image->id}">
                        <img src="{$image->filename|resize:100:100}" alt="" class="magnific-image" id="image_{$image@iteration}" large-img="{$image->filename|resize:600}"/>
                    </label>
					
				</li>{/foreach}</ul>
			<span class=upload_image><i class="dash_link" id="upload_image">Добавить изображение</i></span> или <span class=add_image_url><i class="dash_link" id="add_image_url">загрузить из интернета</i></span>
			<div id=add_image></div>
			
		</div>
		
		{* product_videos *}
		<div class="block layer videos">
			<h2>Видео товара</h2>
			<div id=list class="sortable">
				{foreach $product_videos as $video}
					<div class="row">
						<div class="move cell">
							<div class="move_zone"></div>
						</div>
						<div class="name cell">
							<input name=videos[] value="{$video->link}" style="width:315px;margin-top:0; margin-right:5px;">
						</div>
						<div class="image cell">
							<a href="http://www.youtube.com/embed/{$video->vid}" class="iframe fancybox.iframe">
								<img src="http://img.youtube.com/vi/{$video->vid}/mqdefault.jpg" height="30" width="40"/>
							</a>
						</div>
						<div class="icons cell">
							<a href='#' class="delete"></a>
						</div>
						<div class="clear"></div>
					</div>
				{/foreach}
			</div>
			<br>
			Пример: https://www.youtube.com/watch?v=bVdTGoo97Ls
			<br>
			<br>
			<span class="add"><i class="dash_link">Добавить видео</i></span>
			<div class="row new_video">
				<div class="move cell">
					<div class="move_zone"></div>
				</div>
				<div class="name cell">
					<input name=videos[] value="" style="width:315px;margin-top:0; margin-right:5px;">
				</div>
				<div class="image cell"></div>
				<div class="icons cell">
					<a href='#' class="delete"></a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<input class="button_green button_save" type="submit" name="" value="Сохранить" />
		{* /product_videos *}

		<div class="block layer">
			<h2>Связанные товары</h2>
			<div id=list class="sortable related_products">
				{foreach from=$related_products item=related_product}
				<div class="row">
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products[] value='{$related_product->id}'>
					<a href="{url id=$related_product->id}">
					<img class=product_icon src='{$related_product->images[0]->filename|resize:35:35}'>
					</a>
					</div>
					<div class="name cell">
					<a href="{url id=$related_product->id}">{$related_product->name}</a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
				{/foreach}
				<div id="new_related_product" class="row" style='display:none;'>
					<div class="move cell">
						<div class="move_zone"></div>
					</div>
					<div class="image cell">
					<input type=hidden name=related_products[] value=''>
					<img class=product_icon src=''>
					</div>
					<div class="name cell">
					<a class="related_product_name" href=""></a>
					</div>
					<div class="icons cell">
					<a href='#' class="delete"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<input type=text name=related id='related_products' class="input_autocomplete" placeholder='Выберите товар чтобы добавить его'>
		</div>

		<div class="block layer">
			<h2><a href="#popup" id="go" name="popup" rel="leanModal">Метки</a></h2>
			<div id=list class="marks">
				<input type=hidden id="mks" name="marks" value="{foreach $product_marks as $pm}{$pm->mark_id}/{/foreach}"/>
				
				<div id="popup">
					
								
               	{foreach from=$marks item=mark}
				
				<input  class="marksinput" type=checkbox value="{$mark->id}" id="{$mark->id}" {foreach $product_marks as $pm}{if $mark->id == $pm->mark_id}checked="checked"{/if}{/foreach}/><label for="{$mark->id}">
				{$mark->name}</label>
				{/foreach}
				
				   
				</div>
			</div>	

		</div>

		<input class="button_green button_save" type="button" name="" value="Сохранить" />
		
	</div>
	<!-- Правая колонка свойств товара (The End)--> 

	<!-- Описагние товара -->
	<div class="block layer">
		<h2>Краткое описание</h2>
		<textarea name="annotation" class="editor_small">{$product->annotation|escape}</textarea>
	</div>
		
	<div class="block">		
        
		<h2>Полное  описание</h2>
        <label class=property>Генерировать описание <a href="#" id="full_description_generate" title="Сгенерировать автоматическое описание"><img src="design/images/wand.png" alt="Сгенерировать автоматическое описание" title="Сгенерировать автоматическое описание"></a></label>
        <input id="generate_description" type="checkbox" name="generate_description" value="1" {if $product->generate_description || !$product->id}checked{/if} /><label for="generate_description">Генерировать описания из сео модуля</label>
		<textarea name="body" class="editor_large">{$product->body|escape}</textarea>
	</div>
	
	<div class="block">		
		<h2>Видео</h2>
		<textarea name="video" class="editor_small">{$product->video|escape}</textarea>
	</div>
	
    <div class="block">		
		<h2>Описание для доставки</h2>
		<textarea name="delivery_description" class="editor_small">{$product->delivery_description|escape}</textarea>
	</div>
    
    
	<div class="block" style="display: none;">		
		<h2>Дата</h2>
		<input class="name" name=date type="text" value="{if $product->date == ''}{$smarty.now|date_format:'%Y-%m-%d'}{else}{$product->date}{/if}"/>
	</div>	
	<!-- Описание товара (The End)-->
	<input class="button_green button_save" type="button" name="" value="Сохранить" />
	
</form>
<!-- Основная форма (The End) -->

<div style="display: none; opacity: 0.5;" id="lean_overlay"></div>

<div id="popup_images"></div>