{* Вкладки *}
{capture name=tabs}
	<li class="active"><a href="index.php?module=CommentsAdmin">Комментарии
			{if $new_comments_counter}<span class="rounded_counter_tab">{$new_comments_counter}</span>{/if}
		</a></li>
	<li><a href="index.php?module=FeedbacksAdmin">Обратная связь</a></li>
	<li><a href="index.php?module=YapAdmin">Отзывы с Я.Маркета</a></li>
	{* callbacks *}
	{if in_array('callbacks', $manager->permissions)}<li><a href="index.php?module=CallbacksAdmin">Заказ обратного звонка
			{if $new_callbacks_counter}<span class="rounded_counter_tab">{$new_callbacks_counter}</span>{/if}
		</a></li>
	<li><a href="index.php?module=CallbacksCheapAdmin">Нашли дешевле
			{if $new_callbackcheap_counter}<span class="rounded_counter_tab">{$new_callbackcheap_counter}</span>{/if}
		</a></li>{/if}
	{*/ callbacks *}
{/capture}


{* Title *}
{$meta_title='Комментарий' scope=parent}




{* On document load *}
{literal}
<script src="design/js/jquery/datepicker/jquery.ui.datepicker-ru.js"></script>

<script>
$(function() {

	$('input[name="date"]').datepicker({
		regional:'ru'
	});
	
});

</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success == 'added'}Запись добавлена{elseif $message_success == 'updated'}Запись обновлена{/if}</span>
	<a target="_blank" class="link" href="{$config->root_url}/{if $comment->type == 'product'}products{elseif $comment->type == 'blog'}blog{elseif $comment->type == 'article'}article{/if}/{$comment->object->url}#comment_{$comment->id}">Открыть запись на сайте</a>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error == 'url_exists'}Запись с таким адресом уже существует{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$comment->name|escape}"/> 
		<input name=id type="hidden" value="{$comment->id|escape}"/>
		<input name="type" type="hidden" value="{$comment->type}"/>
		<input name=object_id type="hidden" value="{$comment->object_id|escape}"/>
		<input name=parent_id type="hidden" value="{$comment->parent_id|escape}"/> 
		<div class="checkbox">
			<input name=approved value='1' type="checkbox" id="active_checkbox" {if $comment->approved}checked{/if}/> <label for="active_checkbox">Одобрен</label>
		</div>
		<div class="checkbox">
			<input name=admin value='1' type="checkbox" id="active2_checkbox" {if $comment->admin}checked{/if}/> <label for="active2_checkbox">От админа</label>
		</div>
        <div class="checkbox">
			<input name=on_main_page value='1' type="checkbox" id="active_checkbox3" {if $comment->on_main_page}checked{/if}/> <label for="active_checkbox3">На главной</label>
		</div>
	</div> 

	<!-- Левая колонка свойств товара -->
	<div id="column_left">
			
		<!-- Параметры страницы -->
		<div class="block">
			<ul>
				<li><label class=property>Дата</label><input type=text name=date value='{$comment->date|date}'></li>
				<li><label class=property>IP</label><input type=text name=ip value='{$comment->ip}'></li>
			</ul>
		</div>
			
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right">
		<div class="block">
			<ul>
				<li><label class=property>Время</label><input type=text name=time value='{$comment->date|time}'></li>
			</ul>
		</div>			
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	
	<!-- Описагние товара -->
		
	<div class="block">
		<h2>Текст комментария</h2>
		<textarea name="text"  class='editor_small' style="width:100%;">{$comment->text|escape}</textarea>
	</div>
	<!-- Описание товара (The End)-->
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	
</form>
<!-- Основная форма (The End) -->
