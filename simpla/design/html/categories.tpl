{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	<li class="active"><a href="index.php?module=CategoriesAdmin">Категории</a></li>
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{* Title *}
{$meta_title='Категории' scope=parent}

{* Заголовок *}
<div id="header">
	<h1>Категории товаров</h1>
	<a class="add" href="{url module=CategoryAdmin return=$smarty.server.REQUEST_URI}">Добавить категорию</a>
    <a href="#" class="rolldown">Развернуть все</a>
    <a href="{url module=CategoriesAdmin count_cat_products=show}" class="">Показать кол-во товаров</a>
</div>	
<!-- Заголовок (The End) -->

{if $categories}
<div id="main_list" class="categories">

	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
		
		{function name=categories_tree level=0}
		{if $categories}
		<div id="list" class="sortable{if $level > 0} drop{/if}">
		
			{foreach $categories as $category}
			<div class="{if !$category->visible}invisible{/if} {if !$category->meta_generation}meta{/if} {if !$category->generate_description}description{/if} {if !$category->hide_products}hide_products{/if} row">		
				<div class="tree_row{if $category->subcategories} dropdown{/if}">
					<input type="hidden" name="positions[{$category->id}]" value="{$category->position}">
					{if $category->subcategories}
						<div class="cell icon-folder" style="margin-left:{$level*20}px"></div>
					{else}
						<div class="cell no-folder" style="margin-left:{$level*20}px"></div>
					{/if}
					<div class="move cell" ><div class="move_zone"></div></div>
			 		<div class="checkbox cell">
						<input type="checkbox" name="check[]" value="{$category->id}" />				
					</div>
					<div class="cell">
						<a href="{url module=CategoryAdmin id=$category->id return=$smarty.server.REQUEST_URI}">{$category->name|escape}</a> 	 			
					</div>
					<div class="icons cell">
						<a class="preview" title="Предпросмотр в новом окне" href="../catalog/{$category->url}" target="_blank"></a>				
						<a class="enable" title="Активна" href="#"></a>
						<a class="delete" title="Удалить" href="#"></a>
                        <a class="meta" title="Автогенерация метаданных" href="#"></a>
                        <a class="description" title="Автогенерация описания" href="#"></a>
                        <a class="hide_products" title="Не отображать продукты" href="#"></a>
					</div>
                    
                    {*yandex ext*}
                    <div class="icons cell">
                        {if $category->products_count}<span>( {$category->products_count} )</span>{/if}
            			<a class="yandex_c" data-to_yandex="1" href="javascript:;">В яндекс</a>
                        <a class="yandex_c" data-to_yandex="0" href="javascript:;">Из яндекса</a>
            		</div>
                    {*yandex ext/*}
                    
					<div class="clear"></div>
				</div>
				{categories_tree categories=$category->subcategories level=$level+1}
			</div>
			{/foreach}
	
		</div>
		{/if}
		{/function}
		{categories_tree categories=$categories}
		
		<div id="action">
		<label id="check_all" class="dash_link">Выбрать все</label>
		
		<span id="select">
		<select name="action">
			<option value="enable">Сделать видимыми</option>
			<option value="disable">Сделать невидимыми</option>
			<option value="delete">Удалить</option>
		</select>
		</span>
		
		<input id="apply_action" class="button_green" type="submit" value="Применить">
		
		</div>
	
	</form>
</div>
{else}
Нет категорий
{/if}

{literal}
<script>
$(function() {
    $('.rolldown').click(function(){
        if($(this).hasClass('up')){
            $(this).text('Развернуть все');
            $(this).removeClass('up');
        }else{
            $(this).text('Свернуть все');
            $(this).addClass('up');
        }
    });
    
	// Сортировка списка
	$(".sortable").sortable({
		items:".row",
		handle: ".move_zone",
		tolerance:"pointer",
		scrollSensitivity:40,
		opacity:0.7, 
		axis: "y",
		update:function()
		{
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit();
		}
	});
 
	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]:not(:disabled)').attr('checked', $('#list input[type="checkbox"][name*="check"]:not(:disabled):not(:checked)').length>0);
	});	

	// Показать категорию
	$("a.enable").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('invisible')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'category', 'id': id, 'values': {'visible': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('invisible');
				else
					line.addClass('invisible');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    // Автогенерация мета данных категории
	$("a.meta").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('meta')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'category', 'id': id, 'values': {'meta_generation': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('meta');
				else
					line.addClass('meta');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    // Автогенерация описания категории
	$("a.description").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('description')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'category', 'id': id, 'values': {'generate_description': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('description');
				else
					line.addClass('description');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
    // Не отображать товары в категории
	$("a.hide_products").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
		var state       = line.hasClass('hide_products')?1:0;
		icon.addClass('loading_icon');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'category', 'id': id, 'values': {'hide_products': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
				icon.removeClass('loading_icon');
				if(state)
					line.removeClass('hide_products');
				else
					line.addClass('hide_products');				
			},
			dataType: 'json'
		});	
		return false;	
	});
    
	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').attr('selected', true);
		$(this).closest("form").submit();
	});


    /*yandex ext*/
    $("a.yandex_c").click(function() {
		var icon        = $(this);
		var line        = icon.closest(".row");
		var id          = line.find('input[type="checkbox"][name*="check"]').val();
        var state = $(this).data('to_yandex');
		$.ajax({
			type: 'POST',
			url: 'ajax/update_object.php',
			data: {'object': 'category_yandex', 'id': id, 'values': {'to_yandex': state}, 'session_id': '{/literal}{$smarty.session.id}{literal}'},
			success: function(data){
                line.find('.tree_row a.yandex_c.success_yandex').removeClass('success_yandex');
                line.find('.tree_row a.yandex_c.fail_yandex').removeClass('fail_yandex');
                if (data == -1) {
                    line.find('.tree_row a.yandex_c[data-to_yandex="' + state + '"]').addClass('fail_yandex');
                } else if (data) {
                    line.find('.tree_row a.yandex_c[data-to_yandex="' + state + '"]').addClass('success_yandex');
				} else {
                    line.find('.tree_row a.yandex_c[data-to_yandex="' + state + '"]').removeClass('success_yandex');
				}
			},
			dataType: 'json'
		});	
		return false;	
	});
    /*/yandex ext*/

	
	// Подтвердить удаление
	$("form").submit(function() {
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});

});
</script>
{/literal}