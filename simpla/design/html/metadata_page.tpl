{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	{if in_array('features', $manager->permissions)}<li><a href="index.php?module=FeaturesAdmin">Свойства</a></li>{/if}
        <li class="active"><a href="index.php?module=MetadataPagesAdmin">Метаданные страниц</a></li>
{/capture}

{if $metadata_page->id}
{$meta_title = $metadata_page->name scope=parent}
{else}
{$meta_title = 'Новая страница' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}


{* On document load *}
{literal}
<script>
$(function() {


	// Автозаполнение мета-тегов
	url_touched = true;
	
	if($('input[name="url"]').val() == generate_url() || $('input[name="url"]').val() == '')
		url_touched = false;

	$('input[name="url"]').change(function() { url_touched = true; });
	
	$('input[name="name"]').keyup(function() { set_meta(); });
	
	function set_meta()
	{
		if(!url_touched)
			$('input[name="url"]').val(generate_url());
	}
		
	function generate_url()
	{
		url = $('input[name="name"]').val();
		url = url.toLowerCase();	
		return url;
	}
	

});

</script>
 
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}страница добавлена{elseif $message_success=='updated'}страница обновлена{else}{$message_success}{/if}</span>
	<a class="link" target="_blank" href="{$metadata_page->url|escape}">Открыть страницу на сайте</a>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}страница с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
    <form method=post id=product>
        <input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$metadata_page->name|escape}"/> 
		<input name=id type="hidden" value="{$metadata_page->id|escape}"/> 
	</div> 
	

	<!-- Левая колонка свойств товара -->
	<div id="column_left_all_width">
			
		<!-- Параметры страницы -->
		<div class="block layer">
			<h2>Параметры страницы</h2>
			<ul>
				<li style="width:100%;"><label class=property>Адрес (относительный)</label><input style="width:620px;" maxlength="255" name="url" class="page_url" type="text" value="{$metadata_page->url|escape}" /></li>
				<li style="width:100%;"><label class=property>Заголовок (title)</label><input style="width:620px;"  maxlength="500" name="meta_title" class="simpla_inp" type="text" value="{$metadata_page->meta_title|escape}" /></li>
				<li style="width:100%;"><label class=property>Ключевые слова (keywords)</label><input  maxlength="500" style="width:620px;"  name="meta_keywords" class="simpla_inp" type="text" value="{$metadata_page->meta_keywords|escape}" /></li>
				<li style="width:100%;"><label class=property>Описание (description)</label><textarea maxlength="500" style="width:620px;"  name="meta_description" class="simpla_inp" />{$metadata_page->meta_description|escape}</textarea></li>
                                <li style="width:100%;"><label class=property>Заголовок (h1)</label><input maxlength="255" style="width:620px;"  name="h1_title" class="simpla_inp" type="text" value="{$metadata_page->h1_title|escape}" /></li>                       
                        </ul>
		</div>
		<!-- Параметры страницы (The End)-->
		
			
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	

	
	<!-- Описагние страницаа -->
	<div class="block layer">
		<h2>Описание</h2>
		<textarea name="description" class="editor_large">{$metadata_page->description|escape}</textarea>
	</div>
	<!-- Описание страницы (The End)-->
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
	
	
</form>
<!-- Основная форма (The End) -->

