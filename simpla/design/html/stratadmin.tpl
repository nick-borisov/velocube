{* Вкладки *}
{capture name=tabs}
<!--	<li><a href="index.php?module=ProductsAdmin">Товары</a></li> -->
	<li><a href="index.php?module=ParsersAdmin">Все парсеры</a></li>
	<li  class="active"><a href="index.php?module=StratAdmin">Стратегия цен</a></li>

{/capture}

{* Title *}
{$meta_title='Стратегии' scope=parent}


{* Заголовок *}
<div id="header">
	<h1>Стратегии цен</h1>
	<a class="add" href="{url module=StratEditAdmin return=$smarty.server.REQUEST_URI}">Добавить стратегию цен</a>
 
</div>	
<!-- Заголовок (The End) -->

{if $strategs}
<div id="main_list" class="categories">

	<form id="list_form" method="post">
	<input type="hidden" name="session_id" value="{$smarty.session.id}">
 
		{if $strategs}
		<div id="list" class="sortable">
		
			{foreach $strategs as $strat}
			<div class="{if !$strat->active}invisible{/if} row">		
				<div class="tree_row" style="background: {$strat->color}">
			 		<input type="hidden" name="positions[{$strat->id}]" value="{$strat->position}">
					<div class="move cell"><div class="move_zone"></div></div>
					<div class="checkbox cell">
						<input hidden type="checkbox" name="check[]" value="{$strat->id}" />				
					</div>
					
					<div class="cellp">
						<a href="{url module=StratEditAdmin id=$strat->id return=$smarty.server.REQUEST_URI}">{$strat->name|escape}</a> 	 			
					</div>					
					
					<div class="icons cell">	
						<a class="enable" title="Активна" href="#"></a>
						<a class="delete" title="Удалить" href="#"></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			{/foreach}
	
		</div>
		{/if}
 
 		<span id="select">
		<select hidden name="action">
			<option value="enable">Сделать видимыми</option>
			<option value="disable">Сделать невидимыми</option>
			<option value="delete">Удалить</option>
		</select>
		<input hidden id="apply_action" class="button_green" type="submit" value="Применить">
		</form>
</div>
{else}
Нет стратегий
{/if}


<!-- Меню -->
<div id="right_menu">	

</div>

{literal}
<script>
$(function() {


	// Выделить все
	$("#check_all").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', 1-$('#list input[type="checkbox"][name*="check"]').attr('checked'));
	});	

 

	// Удалить 
	$("a.delete").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=delete]').attr('selected', true);
		$(this).closest("form").submit();
	});
	
		// включить 
	$("a.enable").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=disable]').attr('selected', true);
		$(this).closest("form").submit();
	});
	
			// !включить 
	$("a.disable").click(function() {
		$('#list input[type="checkbox"][name*="check"]').attr('checked', false);
		$(this).closest("div.row").find('input[type="checkbox"][name*="check"]:first').attr('checked', true);
		$(this).closest("form").find('select[name="action"] option[value=enable]').attr('selected', true);
		$(this).closest("form").submit();
	});

	
	// Подтвердить удаление
	$("form").submit(function() {
		if($('select[name="action"]').val()=='delete' && !confirm('Подтвердите удаление'))
			return false;	
	});

 

	// Сортировка списка
	$("#list").sortable({
		items:             ".row",
		tolerance:         "pointer",
		handle:            ".move_zone",
		scrollSensitivity: 40,
		opacity:           0.7, 
		forcePlaceholderSize: true,
		axis: 'y',
		
		helper: function(event, ui){		
			if($('input[type="checkbox"][name*="check"]:checked').size()<1) return ui;
			var helper = $('<div/>');
			$('input[type="checkbox"][name*="check"]:checked').each(function(){
				var item = $(this).closest('.row');
				helper.height(helper.height()+item.innerHeight());
				if(item[0]!=ui[0]) {
					helper.append(item.clone());
					$(this).closest('.row').remove();
				}
				else {
					helper.append(ui.clone());
					item.find('input[type="checkbox"][name*="check"]').attr('checked', false);
				}
			});
			return helper;			
		},	
 		start: function(event, ui) {
  			if(ui.helper.children('.row').size()>0)
				$('.ui-sortable-placeholder').height(ui.helper.height());
		},
		beforeStop:function(event, ui){
			if(ui.helper.children('.row').size()>0){
				ui.helper.children('.row').each(function(){
					$(this).insertBefore(ui.item);
				});
				ui.item.remove();
			}
		},
		update:function(event, ui)
		{
			$("#list_form input[name*='check']").attr('checked', false);
			$("#list_form").ajaxSubmit(function() {
				colorize();
			});
		}
	});

 
	// Раскраска строк
	function colorize()
	{
		$(".row:even").addClass('even');
		$(".row:odd").removeClass('even');
	}
	// Раскрасить строки сразу
	colorize();
 
   
});
 







</script>
{/literal}