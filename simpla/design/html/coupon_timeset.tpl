<div class="block layer">
    <h3>{$timeset_title}</h3>
	<ul class="coupon-schedule">
        {if $inner_timesets}
        {foreach $inner_timesets as $i=>$timeset}
		<li class="coupon-timeset">
            <div class="schedule-weekly i-bem schedule-weekly_js_inited">                          
                <div class="schedule-weekly__row">
                    <div class="schedule-weekly__selectors">
                        <span class="schedule-weekly__days">
                            с 
                            <select class="schStartDay" name="{$save_name}[from_day][]">
                                <option value="MONDAY" {if $timeset->from_day == "MONDAY"}selected{/if}>пн</option>
                                <option value="TUESDAY" {if $timeset->from_day == "TUESDAY"}selected{/if}>вт</option>
                                <option value="WEDNESDAY" {if $timeset->from_day == "WEDNESDAY"}selected{/if}>ср</option>
                                <option value="THURSDAY" {if $timeset->from_day == "THURSDAY"}selected{/if}>чт</option>
                                <option value="FRIDAY" {if $timeset->from_day == "FRIDAY"}selected{/if}>пт</option>
                                <option value="SATURDAY" {if $timeset->from_day == "SATURDAY"}selected{/if}>сб</option>
                                <option value="SUNDAY" {if $timeset->from_day == "SUNDAY"}selected{/if}>вс</option>
                            </select> 
                            по 
                            <select class="schStartDay" name="{$save_name}[to_day][]">
                                <option value="MONDAY" {if $timeset->to_day == "MONDAY"}selected{/if}>пн</option>
                                <option value="TUESDAY" {if $timeset->to_day == "TUESDAY"}selected{/if}>вт</option>
                                <option value="WEDNESDAY" {if $timeset->to_day == "WEDNESDAY"}selected{/if}>ср</option>
                                <option value="THURSDAY" {if $timeset->to_day == "THURSDAY"}selected{/if}>чт</option>
                                <option value="FRIDAY" {if $timeset->to_day == "FRIDAY"}selected{/if}>пт</option>
                                <option value="SATURDAY" {if $timeset->to_day == "SATURDAY"}selected{/if}>сб</option>
                                <option value="SUNDAY" {if $timeset->to_day == "SUNDAY"}selected{/if}>вс</option>
                            </select> 
                        </span>
                        <img src="design/images/i-clock.gif" valign="absmiddle" class="schedule-weekly__clock-icon">
                        <select class="s-sh" name="{$save_name}[from_hour][]">
                            <option value="0" {if $timeset->from_hour == 0}selected{/if}>00</option>
                            <option value="1" {if $timeset->from_hour == 1}selected{/if}>01</option>
                            <option value="2" {if $timeset->from_hour == 2}selected{/if}>02</option>
                            <option value="3" {if $timeset->from_hour == 3}selected{/if}>03</option>
                            <option value="4" {if $timeset->from_hour == 4}selected{/if}>04</option>
                            <option value="5" {if $timeset->from_hour == 5}selected{/if}>05</option>
                            <option value="6" {if $timeset->from_hour == 6}selected{/if}>06</option>
                            <option value="7" {if $timeset->from_hour == 7}selected{/if}>07</option>
                            <option value="8" {if $timeset->from_hour == 8}selected{/if}>08</option>
                            <option value="9" {if $timeset->from_hour == 9}selected{/if}>09</option>
                            <option value="10" {if $timeset->from_hour == 10}selected{/if}>10</option>
                            <option value="11" {if $timeset->from_hour == 11}selected{/if}>11</option>
                            <option value="12" {if $timeset->from_hour == 12}selected{/if}>12</option>
                            <option value="13" {if $timeset->from_hour == 13}selected{/if}>13</option>
                            <option value="14" {if $timeset->from_hour == 14}selected{/if}>14</option>
                            <option value="15" {if $timeset->from_hour == 15}selected{/if}>15</option>
                            <option value="16" {if $timeset->from_hour == 16}selected{/if}>16</option>
                            <option value="17" {if $timeset->from_hour == 17}selected{/if}>17</option>
                            <option value="18" {if $timeset->from_hour == 18}selected{/if}>18</option>
                            <option value="19" {if $timeset->from_hour == 19}selected{/if}>19</option>
                            <option value="20" {if $timeset->from_hour == 20}selected{/if}>20</option>
                            <option value="21" {if $timeset->from_hour == 21}selected{/if}>21</option>
                            <option value="22" {if $timeset->from_hour == 22}selected{/if}>22</option>
                            <option value="23" {if $timeset->from_hour == 23}selected{/if}>23</option>
                        </select> : 
                        <select class="s-sm" name="{$save_name}[from_minute][]">
                            <option value="00" {if $timeset->from_minute == "00"}selected{/if}>00</option>
                            <option value="10" {if $timeset->from_minute == "10"}selected{/if}>10</option>
                            <option value="20" {if $timeset->from_minute == "20"}selected{/if}>20</option>
                            <option value="30" {if $timeset->from_minute == "30"}selected{/if}>30</option>
                            <option value="40" {if $timeset->from_minute == "40"}selected{/if}>40</option>
                            <option value="50" {if $timeset->from_minute == "50"}selected{/if}>50</option>
                            <option value="59" {if $timeset->from_minute == "59"}selected{/if}>59</option>
                        </select>
                         - 
                        <select class="s-poh" name="{$save_name}[to_hour][]">
                            <option value="0" {if $timeset->to_hour == 0}selected{/if}>00</option>
                            <option value="1" {if $timeset->to_hour == 1}selected{/if}>01</option>
                            <option value="2" {if $timeset->to_hour == 2}selected{/if}>02</option>
                            <option value="3" {if $timeset->to_hour == 3}selected{/if}>03</option>
                            <option value="4" {if $timeset->to_hour == 4}selected{/if}>04</option>
                            <option value="5" {if $timeset->to_hour == 5}selected{/if}>05</option>
                            <option value="6" {if $timeset->to_hour == 6}selected{/if}>06</option>
                            <option value="7" {if $timeset->to_hour == 7}selected{/if}>07</option>
                            <option value="8" {if $timeset->to_hour == 8}selected{/if}>08</option>
                            <option value="9" {if $timeset->to_hour == 9}selected{/if}>09</option>
                            <option value="10" {if $timeset->to_hour == 10}selected{/if}>10</option>
                            <option value="11" {if $timeset->to_hour == 11}selected{/if}>11</option>
                            <option value="12" {if $timeset->to_hour == 12}selected{/if}>12</option>
                            <option value="13" {if $timeset->to_hour == 13}selected{/if}>13</option>
                            <option value="14" {if $timeset->to_hour == 14}selected{/if}>14</option>
                            <option value="15" {if $timeset->to_hour == 15}selected{/if}>15</option>
                            <option value="16" {if $timeset->to_hour == 16}selected{/if}>16</option>
                            <option value="17" {if $timeset->to_hour == 17}selected{/if}>17</option>
                            <option value="18" {if $timeset->to_hour == 18}selected{/if}>18</option>
                            <option value="19" {if $timeset->to_hour == 19}selected{/if}>19</option>
                            <option value="20" {if $timeset->to_hour == 20}selected{/if}>20</option>
                            <option value="21" {if $timeset->to_hour == 21}selected{/if}>21</option>
                            <option value="22" {if $timeset->to_hour == 22}selected{/if}>22</option>
                            <option value="23" {if $timeset->to_hour == 23}selected{/if}>23</option>
                        </select>
                         : 
                        <select class="s-pom" name="{$save_name}[to_minute][]">
                            <option value="00" {if $timeset->to_minute == "00"}selected{/if}>00</option>
                            <option value="10" {if $timeset->to_minute == "10"}selected{/if}>10</option>
                            <option value="20" {if $timeset->to_minute == "20"}selected{/if}>20</option>
                            <option value="30" {if $timeset->to_minute == "30"}selected{/if}>30</option>
                            <option value="40" {if $timeset->to_minute == "40"}selected{/if}>40</option>
                            <option value="50" {if $timeset->to_minute == "50"}selected{/if}>50</option>
                            <option value="59" {if $timeset->from_minute == "59"}selected{/if}>59</option>
                        </select>
                    </div>
                    <div class="schedule-weekly__controls">
                        <div class="pseudo-clicker pseudo-clicker__plus js-add_timeset"></div>
                        <div class="pseudo-clicker pseudo-clicker__remove pseudo-clicker__remove_disabled js-delete_timeset"></div>
                    </div>
                </div>
            </div>                
        </li>
        {/foreach}
        {else}
        <li class="coupon-timeset new">
            <div class="schedule-weekly i-bem schedule-weekly_js_inited">                          
                <div class="schedule-weekly__row">
                    <div class="schedule-weekly__selectors">
                        <span class="schedule-weekly__days">
                            с 
                            <select class="schStartDay" name="{$save_name}[from_day][]">
                                <option value="MONDAY" selected>пн</option>
                                <option value="TUESDAY">вт</option>
                                <option value="WEDNESDAY">ср</option>
                                <option value="THURSDAY">чт</option>
                                <option value="FRIDAY">пт</option>
                                <option value="SATURDAY">сб</option>
                                <option value="SUNDAY">вс</option>
                            </select> 
                            по 
                            <select class="schStartDay" name="{$save_name}[to_day][]">
                                <option value="MONDAY">пн</option>
                                <option value="TUESDAY">вт</option>
                                <option value="WEDNESDAY">ср</option>
                                <option value="THURSDAY">чт</option>
                                <option value="FRIDAY">пт</option>
                                <option value="SATURDAY">сб</option>
                                <option value="SUNDAY" selected>вс</option>
                            </select> 
                        </span>
                        <img src="design/images/i-clock.gif" valign="absmiddle" class="schedule-weekly__clock-icon">
                        <select class="s-sh" name="{$save_name}[from_hour][]">
                            <option value="0" selected>00</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                        </select> : 
                        <select class="s-sm" name="{$save_name}[from_minute][]">
                            <option value="00" selected="">00</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                            <option value="59">59</option>
                        </select>
                         - 
                        <select class="s-poh" name="{$save_name}[to_hour][]">
                            <option value="0">00</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23" selected>23</option>
                        </select>
                         : 
                        <select class="s-pom" name="{$save_name}[to_minute][]">
                            <option value="00" selected="">00</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                            <option value="59">59</option>
                        </select>
                    </div>
                    <div class="schedule-weekly__controls">
                        <div class="pseudo-clicker pseudo-clicker__plus js-add_timeset"></div>
                        <div class="pseudo-clicker pseudo-clicker__remove pseudo-clicker__remove_disabled js-delete_timeset"></div>
                    </div>
                    <div class="not saved">
                        <span>Не сохранено!</span>
                    </div>
                </div>
            </div>                
        </li>
        {/if}
	</ul>
</div>