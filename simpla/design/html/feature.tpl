{* Вкладки *}
{capture name=tabs}
	{if in_array('products', $manager->permissions)}<li><a href="index.php?module=ProductsAdmin">Товары</a></li>{/if}
	{if in_array('categories', $manager->permissions)}<li><a href="index.php?module=CategoriesAdmin">Категории</a></li>{/if}
	{if in_array('brands', $manager->permissions)}<li><a href="index.php?module=BrandsAdmin">Бренды</a></li>{/if}
	<li class="active"><a href="index.php?module=FeaturesAdmin">Свойства</a></li>
    {if in_array('filter', $manager->permissions)}<li><a href="index.php?module=FiltersAdmin">Фильтры</a></li>{/if}
	{if in_array('marks', $manager->permissions)}<li><a href="index.php?module=MarksAdmin">Метки</a></li>{/if}
	{if in_array('seo', $manager->permissions)}<li><a href="index.php?module=SeoAdmin">SEO</a></li>{/if}

{/capture}

{if $feature->id}
{$meta_title = $feature->name scope=parent}
{else}
{$meta_title = 'Новое свойство' scope=parent}
{/if}

<link rel="stylesheet" type="text/css" href="design/js/inputosaurus/inputosaurus.css" media="screen" />
<script src="design/js/inputosaurus/inputosaurus.js"></script>

{* On document load *}

{*NORMAL MULTIPLE CATEGORIES*}
<script src="design/js/jquery/jquery.sumoselect.js"></script>
<link rel="stylesheet" type="text/css" href="design/js/jquery/sumoselect.css" media="screen" />
{*NORMAL MULTIPLE CATEGORIES*}
{literal}
<script>
$(function() {
    
    $( "#tabs" ).tabs();
    
    $('input[name=allowed_values]').inputosaurus({
        activateFinalResult : true,
        autoCompleteSource : [{/literal}{foreach $feature_values as $tag}"{($tag|escape)}"{if !$tag@last},{/if}{/foreach}{literal}]
    });
    
    $('.taggroup-method').on('click', function(){
        $('.taggroupselect .taggroup-method').removeClass('active');
        $(this).addClass('active');    
        
        
    });
    //NORMAL MULTIPLE CATEGORIES
    $('.multiple_categories').SumoSelect({selectAll: true});
    //NORMAL MULTIPLE CATEGORIES
    
    $('#check_transfer').on('click', function(){
        $('#transfer_feature').toggle();
        if($('#transfer_feature select').prop('disabled'))
            $('#transfer_feature select, #transfer_feature input').prop('disabled', null);
        else
            $('#transfer_feature select, #transfer_feature input').prop('disabled', 'disabled');
    });
});
</script>
{/literal}

{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Свойство добавлено{elseif $message_success=='updated'}Свойство обновлено{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{$message_error}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}

<!-- Основная форма -->
<form method=post id=product>

	<div id="name">
		<input class="name" name=name type="text" value="{$feature->name|escape}"/> 
		<input name=id type="hidden" value="{$feature->id|escape}"/> 
	</div> 
    
    <div class="prefix">
        <label>Префикс </label>
        <i class="fa fa-info-circle" title="Если префикс указан то при фильтрации свойство будет отображаться в виде 'Ширина: 80 см', если же префикс не указан будет '80 см'"></i>
        <input type="text" name="prefix" value="{$feature->prefix|escape}"/>
        
        <label>Постфикс</label>
        <i class="fa fa-info-circle" title="Постфикс отображается при фильтрации, если он указан то будет писаться 'Ширина: 80 см', если нет то 'Ширина: 20'"></i>
        <input type="text" name="postfix" value="{$feature->postfix|escape}"/>
    </div>

	<!-- Левая колонка свойств товара -->
	<div id="column_left">
		<div id="tabs">
            <ul>
                <li><a href="#tabs-1">Категория</a></li>
                <li style="width: 170px!important;"><a href="#tabs-2">Привязанные значения</a></li>
                <li><a href="#tabs-3">Описание</a></li>
            </ul>
            <div id="tabs-1">
                <div class="block">
        			<h2>Использовать в категориях</h2>
    				<select class=multiple_categories multiple name="feature_categories[]" style="display: none">
    					{function name=category_select selected_id=$product_category level=0}
    					{foreach from=$categories item=category}
    							<option value='{$category->id}' {if in_array($category->id, $feature_categories)}selected{/if} category_name='{$category->single_name}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name}</option>
    							{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
    					{/foreach}
    					{/function}
    					{category_select categories=$categories}
    				</select>
        		</div>
            </div>
            <div id="tabs-2">
                <div class="block">
                    Начните вводить значение <input type="text" name="allowed_values" value="{$feature->allowed_values}"/>
                    <span style="margin-top: 20px; display: block"><b>Все значения:</b> {foreach $feature_values as $value}{$value}{if !$value@last}, {/if}{/foreach}</span>
                </div>
            </div>    
            <div id="tabs-3">
                <div class="block">
                    <h3>Описание</h3>
                    <textarea name="description" class="editor_small" style="width: 400px;">{$feature->description|escape}</textarea>
                </div>
            </div>
        </div>    	
		<!-- Категории -->	
        
        <div class="block" style="margin-top: 10px">
            <label class="ios7-switch">           
                <input type="checkbox" name="auto_discount"/>
                <span id="check_transfer" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
            </label>
            <span style="font-size: 16px;font-weight: 600;">Перемещение значений</span>
            
            <div id="transfer_feature" style="margin-top: 20px; display: none">
                <h2>Будьте предельно внимательны!</h2>
                <h3>В выбранной категории выбранное свойство будет подменено на текущее. Перемещение значений  из выбранного свойства в это (id : {$feature->id} "{$feature->name}")</h3>
                <h3>Из какого свойства перемещаем</h3>
           	    <select name="transfer_feature" class="features_select" style="width: 200px" disabled>
                    {foreach $features as $feature2}
                    <option value="{$feature2->id}" data-type="{$feature2->mode}">{$feature2->name} {if !$feature2->separ}(id : {$feature2->id}){/if}</option>
                    {/foreach}
                </select>
                <h3>В какой категории</h3>
                <select class=""  name="transfer_from_category" disabled>
    				{function name=category_select selected_id=$product_category level=0}
    				{foreach from=$categories item=category}
    						<option value='{$category->id}' {if in_array($category->id, $feature_categories)}selected{/if} category_name='{$category->single_name}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name}</option>
    						{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
    				{/foreach}
    				{/function}
    				{category_select categories=$categories}
    			</select>
            </div>
        </div>
        
	</div>
	<!-- Левая колонка свойств товара (The End)--> 
	
	<!-- Правая колонка свойств товара -->	
	<div id="column_right">
		
		<!-- Параметры страницы -->
		<div class="block">
			<h2>Настройки свойства</h2>
			<ul>
                <li><input type=checkbox name=in_filter id=in_filter {if $feature->in_filter}checked{/if} value="1"> <label for=in_filter>Использовать в фильтре</label></li>
				<li><input type=checkbox name=separ id=separ {if $feature->separ}checked{/if} value="1"> <label for=separ>Разделитель</label></li>
                <li><input type=checkbox name=strict id=strict {if $feature->strict}checked{/if} value="1"> <label for=strict>Строгая привязка значений</label></li>
                
                <li>
                    <h2>Отображение в фильтре</h2>
                    <div class="taggroupselect">
	                   <div class="taggroupselect-inline">
            				<div class="taggroup-method {if $feature->mode=='select'}active{/if}">
            				<div class="radio">
            					<label>
            						<input type="radio" name="mode" value="select" {if $feature->mode=='select'}checked{/if}>
                                    <div class="taggroup-image"><img src="http://minicart.su/system/templates//img/properties/list.png"></div>
            						<div class="taggroup-header">Выпадающий список</div>
            						<div class="taggroup-description">Позволяющий выбрать 1 значение из списка.</div>
                                    
            					</label>
                            </div>
                            </div>
            				<div class="taggroup-method {if $feature->mode=='checkbox'}active{/if}">
            				<div class="radio">
            					<label>
            						<input type="radio" name="mode" value="checkbox" {if $feature->mode=='checkbox'}checked{/if}>
                                    <div class="taggroup-image"><img src="http://minicart.su/system/templates//img/properties/checkbox.png"></div>
            						<div class="taggroup-header">Галочки (Чекбоксы)</div>
            						<div class="taggroup-description">Позволяют выбрать несколько значений из предложенных.</div>
                                    
            					</label>
                            </div>
                            </div>
            				<div class="taggroup-method {if $feature->mode=='radio'}active{/if}">
            				<div class="radio">
            					<label>
            						<input type="radio" name="mode" value="radio" {if $feature->mode=='radio'}checked{/if}>
                                    <div class="taggroup-image"><img src="http://minicart.su/system/templates//img/properties/radio.png"></div>
            						<div class="taggroup-header">Радиобаттон</div>
            						<div class="taggroup-description">Позволяет выбрать 1 значение из предложенных.</div>
                                    
            					</label>
                            </div>
                            </div>
            				<div class="taggroup-method {if $feature->mode=='range'}active{/if}">
            				<div class="radio">
            					<label>
            						<input type="radio" name="mode" value="range" {if $feature->mode=='range'}checked{/if}>
                                    <div class="taggroup-image"><img src="http://minicart.su/system/templates//img/properties/interval.png"></div>
            						<div class="taggroup-header">Диапазонный фильтр</div>
            						<div class="taggroup-description">Позволяет указать диапазон из предложенных численных значений.</div>
                                    
            					</label>
                            </div>
                            </div>
            				
            				<div class="taggroup-method {if $feature->mode=='logical'}active{/if}">
            				<div class="radio">
            					<label>
            						<input type="radio" name="mode" value="logical" {if $feature->mode=='logical'}checked{/if}>
                                    <div class="taggroup-image"><img src="http://minicart.su/system/templates//img/properties/logic.png"></div>
            						<div class="taggroup-header">Логический</div>
            						<div class="taggroup-description">Либо это значение есть либо нет. Отображается в виде одной галочки.</div>
                                    
            					</label>
                            </div>
                            </div>
                            
                            <div class="taggroup-method {if $feature->mode=='text'}active{/if}">
                				<div class="radio">
                					<label>
                						<input type="radio" name="mode" value="text" {if $feature->mode=='text'}checked{/if}>
                                        <div class="taggroup-image"><img src="http://minicart.su/system/templates//img/properties/text.png"></div>
                						<div class="taggroup-header">Текст</div>
                						<div class="taggroup-description">Просто текст, данное свойство не учавствует в фильтрации.</div>
                                        
                					</label>
                                </div>
                            </div>
                		</div>
                    </div>
                </li>
			</ul>
		</div>
		<!-- Параметры страницы (The End)-->
		<input type=hidden name='session_id' value='{$smarty.session.id}'>
		
		
	</div>
	<!-- Правая колонка свойств товара (The End)--> 
	<div style="float: right;">
        <input class="button_green" type="submit" name="" value="Сохранить" />
    </div>

</form>
<!-- Основная форма (The End) -->

