<!-- jQuery -->
<script src="//yandex.st/jquery/1.9.1/jquery.min.js"></script>
 
{* Вкладки *}
{capture name=tabs}
<!--	<li><a href="index.php?module=ProductsAdmin">Товары</a></li> -->
		<li><a href="index.php?module=ExportsAdmin">Все каналы</a></li>
	{if ($export->name)}
	<li class="active"><a href="index.php?module=ExportsChannelAdmin&id={$export->id}">{$export->name|escape}</a></li>
	{else}
	<li class="active"><a href="index.php?module=ExportsChannelAdmin">Новый канал</a></li>
	{/if}
{/capture}
{if $export->id}
{$meta_title = $export->name scope=parent}
{else}
{$meta_title = 'Новый канал' scope=parent}
{/if}

{* Подключаем Tiny MCE *}
{include file='tinymce_init.tpl'}

 
{if $message_success}
<!-- Системное сообщение -->
<div class="message message_success">
	<span>{if $message_success=='added'}Новый канал экспорта добавлен{elseif $message_success=='updated'}Канал обновлен{else}{$message_success}{/if}</span>
	{if $smarty.get.return}
	<a class="button" href="{$smarty.get.return}">Вернуться</a>
	{/if}
</div>
<!-- Системное сообщение (The End)-->
{/if}

{if $message_error}
<!-- Системное сообщение -->
<div class="message message_error">
	<span>{if $message_error=='url_exists'}Канал с таким адресом уже существует{else}{$message_error}{/if}</span>
	<a class="button" href="">Вернуться</a>
</div>
<!-- Системное сообщение (The End)-->
{/if}


<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data">
<input type=hidden name="session_id" value="{$smarty.session.id}">
	<div id="name">
		<input class="name" name=name type="text" value="{$export->name|escape}"/> 
		<input name=id type="hidden" value="{$export->id|escape}"/>
		<div class="checkbox">
			<input name=active value='1' type="checkbox" id="active_checkbox" {if $export->active}checked{/if}/> <label for="active_checkbox">Активен</label>
		</div>
	</div> 
 	 
	<div id="column_left">
		<div class="block layer">
 
			<h2>Параметры канала</h2>
				<li><label class="property">Ссылка</label><input name="url" class="simpla_inp" type="text" value=""></li>
				<li><label class="property">Иконка</label><input name="icon" class="simpla_inp" type="text" value=""></li>				
				
				<li><label class=property>Комментарии</label><textarea name="description" class="simpla_inp"/>{$export->description|escape}</textarea></li>
	 
 
 
		 
		</div>
	 
	</div>
	<div id="column_right"> 	
	</div>
	<div class="block layer">
	 
	</div>
	 
	<input class="button_green button_save" type="submit" name="" value="Сохранить" />
</form>