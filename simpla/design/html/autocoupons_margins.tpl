<div class="block layer marga">
    <div class="activator">
        <label class="ios7-switch">           
            <input type="checkbox" name="active_margin" {if $coupon->active_margin}checked="checked"{/if} value="1"/>
            <span id="check" style="margin-right: 5px;font-size: 25px;float: left;margin-bottom: -7px;"></span>
        </label>
        <span style="font-size: 18px;font-weight: 600;">Наценка (+ к цене товара)</span>
    </div>
    
    <div class="margin-body {if $coupon->active_margin}display{/if}">
        <ul class="marga-settings">
            <h3>{$timeset_title}</h3>
            {assign 'margins' unserialize($coupon->margin)}	
            
            {if $margins}
                {foreach $margins['min'] as $i=>$min}
                <li>
                    От <input name="marja[min][]" class="simpla_inp" type="text" value="{$min}" {if count( $margins['min'])>1}required{/if} /> 
                    до <input name="marja[max][]" class="simpla_inp" type="text" value="{$margins['max'][$i]}" {if count( $margins['min'])>1}required{/if} />
                    скидка <input name="marja[price][]" class="simpla_inp" type="text" value="{$margins['price'][$i]}" />
                    <select name="marja[mode][]">
                        <option value="1" {if $margins['mode'][$i]==1}selected{/if}>руб.</option>
                        <option value="2" {if $margins['mode'][$i]==2}selected{/if}>%</option>
                    </select>
                    <span class="add"{if $min@iteration!=1}style="display:none"{/if}><i class="dash_link" id="add_new_feature">Добавить интервал</i></span>
                    <span {if $min@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                </li>
                {/foreach}
            {else}
                <li>
                    От <input name="marja[min][]" class="simpla_inp" type="text" value="" /> 
                    до <input name="marja[max][]" class="simpla_inp" type="text" value="" />                                                          скидка <input name="marja[price][]" class="simpla_inp" type="text" value="" />
                    <select name="marja[mode][]">
                        <option value="1">руб.</option>
                        <option value="2">%</option>
                    </select>
                    <span class="add"><i class="dash_link" id="add_new_feature">Добавить интервал</i></span>
                    <span style='display:none;' class="delete"><i class="dash_link">Удалить</i></span>
                </li>
            {/if}
        </ul>
    </div>
</div>