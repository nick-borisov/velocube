<?PHP

require_once('api/Simpla.php');
// ini_set('display_errors',1);
 

class ParsersAdmin extends Simpla
{
	function fetch()
	{
		if($this->request->method('post'))
		{
			// Действия с выбранными
			$ids = $this->request->post('check');
			if(is_array($ids))
			switch($this->request->post('action'))
			{
			    case 'disable':
			    {
			    	foreach($ids as $id)
						$this->parsers->update_parser($id, array('active'=>0));    
					break;
			    }
			    case 'enable':
			    {
			    	foreach($ids as $id)
						$this->parsers->update_parser($id, array('active'=>1));    
			        break;
			    }
			    case 'delete':
			    {
			    	foreach($ids as $id)
						$this->parsers->delete_parser($id);    
			        break;
			    }
			}		
	  	
			// Сортировка
				$this->parsers->update_parser($ids[$i],array('active'=>1)); 

		}

		if($this->request->method('get') AND $this->request->get('goparse'))	
		{
		if ($this->request->get('prs'))
			$this->parsers->go_parse($this->request->get('prs'));
		else
			$this->parsers->go_parse();
		}
		
			if($this->request->method('get') AND $this->request->get('goclear'))	
		{
			$this->parsers->go_clear();
		}
  
		$parsers = $this->parsers->get_parsers();
		
		foreach($parsers as $parser)
		{
			
			$date = $parser->lastrun;
			$datenow2 = strtotime("$date GMT");
			$how = floor((time()-$datenow2) / 86400)  . "<br>";
			
		if ($how <= 7)
			$parser->color = "green";
		if ($how > 7 AND $how <= 14)
			$parser->color = "yellow";
		if ($how > 15)
			$parser->color = "red";
		}

		$this->design->assign('parsers', $parsers);
		return $this->design->fetch('parsers.tpl');
	}
}
