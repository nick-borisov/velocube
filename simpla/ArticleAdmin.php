<?PHP

require_once('api/Simpla.php');

class ArticleAdmin extends Simpla
{
	public function fetch()
	{
		if($this->request->method('post'))
		{
			$post->id = $this->request->post('id', 'integer');
			$post->name = $this->request->post('name');
			$post->date = date('Y-m-d', strtotime($this->request->post('date')));
			
			$post->visible = $this->request->post('visible', 'boolean');
			$post->category_id = $this->request->post('category_id', 'integer');

			$post->url = $this->request->post('url', 'string');
			$post->meta_title = $this->request->post('meta_title');
			$post->meta_keywords = $this->request->post('meta_keywords');
			$post->meta_description = $this->request->post('meta_description');
			
			$post->annotation = $this->request->post('annotation');
			$post->text = $this->request->post('body');

 			// Не допустить одинаковые URL разделов.
			if(($a = $this->articles->get_article($post->url)) && $a->id!=$post->id)
			{			
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{
				if(empty($post->id))
				{
	  				$post->id = $this->articles->add_article($post);
	  				$post = $this->articles->get_article($post->id);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->articles->update_article($post->id, $post);
  	    			$post = $this->articles->get_article($post->id);
					$this->design->assign('message_success', 'updated');
  	    		}	

   	    		
			}
		}
		else
		{
			$post->id = $this->request->get('id', 'integer');
			$post = $this->articles->get_article(intval($post->id));
		}

		if(empty($post->date))
			$post->date = date($this->settings->date_format, time());
 		
		$this->design->assign('post', $post);

		// Категории
		$articles_categories = $this->articles_categories->get_articles_categories_tree();
		$this->design->assign('articles_categories', $articles_categories);		
		      
 	  	return $this->design->fetch('article.tpl');
	}
}