<?php
//ini_set('display_errors',1);
require_once('api/Simpla.php');


############################################
# Class Category - Edit the good gategory
############################################
class ParserAdmin extends Simpla
{



  function fetch()
  {
		if($this->request->method('post'))
		{
		//die(print_r($_POST));
            $pareser = new StdClass();
            $parser->id = 				$this->request->post('id');
            $parser->name = 			$this->request->post('name');
            $parser->active = 			$this->request->post('active');
            $parser->parser_type =		$this->request->post('parser_type');
            $parser->reprice =			$this->request->post('reprice');
            $parser->restock =			$this->request->post('restock');
            
            if($parser->active_margin = $this->request->post('active_margin'))
                $parser->margin = serialize($this->request->post('marja'));
            if($parser->active_stockmargin = $this->request->post('active_stockmargin'))
                $parser->stockmargin = serialize($this->request->post('stockmarja'));
            
            if($parser->parser_type=='excel'){
    			$parser->filename = 		$this->request->post('filename');
    			$parser->art = 				$this->request->post('art');
    			$parser->art2 = 			$this->request->post('art2');
    			$parser->prices = 			$this->request->post('price');
    			$parser->stockprice = 		$this->request->post('stockprice');
    			$parser->instock = 			$this->request->post('nalichie');
    			$parser->val = 				$this->request->post('valuta');    			
    			$parser->instockname = 		$this->request->post('vnalname');
    			$parser->notinstockname = 	$this->request->post('nenalname');
    			$parser->byrequest =		$this->request->post('ojidaetsa');
    			$parser->description =		$this->request->post('description');
                $parser->xls_vendor =		$this->request->post('xls_vendor');
            }
            
            if($parser->parser_type=='xml'){ 
                $parser->xmlurl = $this->request->post('xmlurl');
                $parser->xmlproduct = $this->request->post('xmlproduct');
                $parser->xmlproduct_attr = $this->request->post('xmlproduct_attr');
                $parser->xmlart = $this->request->post('xmlart');
                $parser->xmlart_attr = $this->request->post('xmlart_attr');
                if($parser->xmlart=='xpath'){
                    $parser->xmlart_attr = $this->request->post('xmlart_xpath');
                }
               
                $parser->xmlprice = $this->request->post('xmlprice');
                $parser->xmlprice_attr = $this->request->post('xmlprice_attr');
                if($parser->xmlprice=='xpath'){
                    $parser->xmlprice_attr = $this->request->post('xmlprice_xpath');
                }
                $parser->xmlstockprice = $this->request->post('xmlstockprice');
                $parser->xmlstockprice_attr = $this->request->post('xmlstockprice_attr');
                if($parser->xmlstockprice=='xpath'){
                    $parser->xmlstockprice_attr = $this->request->post('xmlstockprice_xpath');
                }
                $parser->xmlstock = $this->request->post('xmlstock');
                $parser->xmlstock_attr = $this->request->post('xmlstock_attr');
                if($parser->xmlstock=='xpath'){
                    $parser->xmlstock_attr = $this->request->post('xmlstock_xpath');
                }
                $parser->xmlwaiting = $this->request->post('xmlwaiting');
                $parser->xmlwaiting_attr = $this->request->post('xmlwaiting_attr');
                if($parser->xmlwaiting=='xpath'){
                    $parser->xmlwaiting_attr = $this->request->post('xmlwaiting_xpath');
                }
                $parser->xmlcurrency = $this->request->post('xmlcurrency');
                $parser->xmlcurrency_attr = $this->request->post('xmlcurrency_attr');
                $parser->xmlvnalname = $this->request->post('xmlvnalname');
                $parser->xmlojiname = $this->request->post('xmlojiname');
                $parser->xmlnenalname = $this->request->post('xmlnenalname');
            }
            
			if(empty($parser->id))
			{
				if (empty($parser->name)) $parser->name = "Не указан";
  				$parser->id = $this->parsers->add_parser($parser);
				$this->design->assign('message_success', 'added');
  			}
    		else
    		{
    			$this->parsers->update_parser($parser->id, $parser);
				$this->design->assign('message_success', 'updated');
    		}	


	 		$parser = $this->parsers->get_parser($parser->id); 	
		}
		else
		{
			if ($pid = $this->request->get('id')) {
				$parser = $this->parsers->get_parser($pid);
			}
		}
        
        if($parser->xmlurl){
            $string = file_get_contents($parser->xmlurl);
            //var_dump($string);
            $xml = simplexml_load_string($string); 
            if($xml){
                $tags = array_unique(array_map(function ($e) { return $e->getName(); }, $xml->xpath('//*')));
                $this->design->assign('tags', $tags);
            
                $tags_w_attr = array_unique(array_map(function ($e) { return $e->getName(); }, $xml->xpath('//*[@*]')));
                $this->design->assign('tags_w_attr', $tags_w_attr);
    
                $attributes = array_unique(array_map(function ($e) { return $e->getName(); }, $xml->xpath('//@*')));
                $this->design->assign('attributes', $attributes);
                
                foreach($tags as $tag){
                    if(!in_array($tag, $tags_w_attr)){
                        //var_dump($parser->{$tag.'_attr'});
                    }
                }
            }
        }   
		
		$pfiles = $this->parsers->get_files();
		
 		$this->design->assign('parser', $parser);
		$this->design->assign('pfiles', $pfiles);
 
		return  $this->design->fetch('parser.tpl');
	}
}