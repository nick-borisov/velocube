<?php

require_once('api/Simpla.php');


############################################
# Class Mark - Edit the good mark
############################################
class MarkAdmin extends Simpla
{
  
  
  function fetch()
  {
		$mark = new stdClass;
		if($this->request->method('post'))
		{
			$mark->id = $this->request->post('id', 'integer');
			
			$mark->name = $this->request->post('name');
			$mark->category = $this->request->post('category');
			$mark->visible = $this->request->post('visible', 'boolean');

			$mark->url = $this->request->post('url', 'string');
			$mark->meta_title = $this->request->post('meta_title');
			$mark->meta_keywords = $this->request->post('meta_keywords');
			$mark->meta_description = $this->request->post('meta_description');
			
			$mark->description = $this->request->post('description');
	
			// Не допустить одинаковые URL разделов.
			if(($c = $this->marks->get_mark($mark->url)) && $c->id!=$mark->id)
			{			
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{
				if(empty($mark->id))
				{
	  				$mark->id = $this->marks->add_mark($mark);
					$this->design->assign('message_success', 'added');

	  			}
  	    		else
  	    		{
  	    			$this->marks->update_mark($mark->id, $mark);
					$this->design->assign('message_success', 'updated');
				
  	    		}

  	    		$mark = $this->marks->get_mark(intval($mark->id));
			}
		}
		else
		{
			$mark->id = $this->request->get('id', 'integer');
			$mark = $this->marks->get_mark($mark->id);
		}
		

		$categories = $this->categories->get_categories_tree();
        $category = $this->categories->get_category(intval($mark->category));
		$this->design->assign('cat', $category);
		$this->design->assign('categories', $categories);

		$this->design->assign('mark', $mark);
		
		return  $this->design->fetch('mark.tpl');
	}
}