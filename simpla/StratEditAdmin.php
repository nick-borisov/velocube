<?php

require_once('api/Simpla.php');


############################################
# Class Category - Edit the good gategory
############################################
class StratEditAdmin extends Simpla
{



  function fetch()
  {
		if($this->request->method('post'))
		{
			$strat->id				= 			$this->request->post('id');
			$strat->name 			= 			$this->request->post('name');
			$strat->starth 			= 			$this->request->post('starth');
			$strat->startm 			= 			$this->request->post('startm');
			$strat->day1 			= 			$this->request->post('day1');
			$strat->day2 			= 			$this->request->post('day2');
			$strat->day3 			= 			$this->request->post('day3');
			$strat->day4 			= 			$this->request->post('day4');
			$strat->day5 			= 			$this->request->post('day5');
			$strat->day6 			= 			$this->request->post('day6');
			$strat->day7 			= 			$this->request->post('day7');
			
			$strat->endh			= 			$this->request->post('endh');
			$strat->endm			= 			$this->request->post('endm');		 
			$strat->active 			= 			$this->request->post('active');
			$strat->delta 			= 			$this->request->post('delta');
			$strat->deltap 			= 			$this->request->post('deltap');
			$strat->ven 			= 			$this->request->post('ven');
			$strat->brand 			= 			$this->request->post('brand');
			$strat->description 	= 			$this->request->post('description');
			$strat->color 			= 			$this->request->post('color');
			$strat->minprice 		= 			$this->request->post('minprice');
			$strat->maxprice		= 			$this->request->post('maxprice');
			
			//die($strat->day7);

			if (!isset($strat->day1)) $strat->day1 = " ";
			if (!isset($strat->day2)) $strat->day2 = " ";
			if (!isset($strat->day3)) $strat->day3 = " ";
			if (!isset($strat->day4)) $strat->day4 = " ";
			if (!isset($strat->day5)) $strat->day5 = " ";
			if (!isset($strat->day6)) $strat->day6 = " ";
			if (!isset($strat->day7)) $strat->day7 = " ";
			
				if(empty($strat->id))
				{
					if (empty($strat->name)) $strat->name = "�� ������";
	  				$strat->id = $this->parsers->add_strat($strat);
					$this->design->assign('message_success', 'added');
	  			}
  	    		else
  	    		{
  	    			$this->parsers->update_strat($strat->id, $strat);
					$this->design->assign('message_success', 'updated');
  	    		}	  
		 		$strat = $this->parsers->get_strat($strat->id);			  	
				
				// ��������� ������
			if(is_array($this->request->post('related_products')))
			{
				foreach($this->request->post('related_products') as $p)
				{
					$rp[$p]->strat_id = $strat->id;
					$rp[$p]->related_id = $p;
				}
				$related_products = $rp;
				
					// ��������� ������
	   	    		$query = $this->db->placehold('DELETE FROM __strat_related_products WHERE strat_id=?', $strat->id);
	   	    		$this->db->query($query);
	 	  		    if(is_array($related_products))
		  		    {
		  		    	$pos = 0;
		  		    	foreach($related_products  as $i=>$related_product)
	   	    				$this->parsers->add_strat_related_product($strat->id, $related_product->related_id, $pos++);
	  	    		}
					
					
				
			}
		}
		else
		{
			if ($pid = $this->request->get('id')) 
				{
					$strat = $this->parsers->get_strat($pid);				 				 
				}
		}
		
		$related_products = $this->parsers->get_strat_related_products(array('strat_id'=>$strat->id));
				if(!empty($related_products))
		{
			foreach($related_products as &$r_p)
				$r_products[$r_p->related_id] = &$r_p;
			$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
			foreach($temp_products as $temp_product)
				$r_products[$temp_product->id] = $temp_product;
		
			$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
			foreach($related_products_images as $image)
			{
				$r_products[$image->product_id]->images[] = $image;
			}
		}
		
		  		// ��� ������
			
		$brands = $this->brands->get_brands(array("order"=>"1"));
		$this->design->assign('brands', $brands);
  
		
		$vendors = $this->parsers->get_parsers();
		$this->design->assign('vendors', $vendors);
 	
		$this->design->assign('related_products', $related_products);		
 		$this->design->assign('strat', $strat);

		return  $this->design->fetch('stratedit.tpl');
	}
}