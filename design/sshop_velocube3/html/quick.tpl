{strip}
<div class="product_quickview product_single product form_modal">

	<div class="form_modal_row">
	
		<div class="relative">
		
			<div class="product_sku text_gray2 text_5{if !$product->variant->sku} hidden{/if}">
				Артикул: <span class="value">{$product->variant->sku}</span>
			</div>

			<div class="product_name text_4 text_black" data-product_name="{$product->fullname|escape}">
				<span class="text_2 text_bold">{$product->fullname|escape}</span>
			</div>
			
			<div class="rating inline_block_middle" >
				
				<span class="rating_starOff">
					{*Рейтинг на основе отзывов*}
					{if $product->comcon > 0}
						<span class="rating_starOn" style="width:{$product->comrate*85/5|string_format:'%.0f'}px"></span>
					{*Рейтинг, заданный в админке*}
					{elseif $product->rating > 0}
						<span class="rating_starOn" style="width:{$product->rating*85/5|string_format:'%.0f'}px"></span>
					{else}
						<span class="rating_starOn"></span>
					{/if}
				</span>
			</div>
							
		</div>
		
	</div>
	
	<div class="form_modal_row">
	<div class="row">
	
		<div class="col_xs_12 col_md_6 col_lg_7">

			<div id="product_gallery_modal" class="product_gallery product_image text_center" data-product_image="{$product->image->filename|resize:200:200}">
						
				{* Прикрепленные видео к товару *}
				{*foreach $product->videos as $video}
					<div class="product_gallery_item" data-thumb="design/{$settings->theme|escape}/images/icon_video.svg">
						<div class="product_gallery_video relative">
							<iframe class="absolute" src="https://www.youtube.com/embed/{$video->vid}" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				{/foreach*}
				
				{* Галерея изображений *}
				{foreach $product->images as $i=>$image}
					<div class="product_gallery_item{if $product->videos|count > 0 && $image@first} product_gallery_first{/if}" data-thumb="{$image->filename|resize:60:40}">
						<a class="fancybox_gallery" href="{$image->filename|resize:800:600}" data-fancybox-group="gallery">
							<img src="{$image->filename|resize:600:400}" alt="{$product->name|escape}" />
						</a>
					</div>
				{/foreach}
				
			</div>
			
		</div>
		
		<div class="product_info col_xs_12 col_md_6 col_lg_5">
			<form class="variants" action="/cart">
				
				<div id="product_stock" class="product_stock inline_block_middle text_bold text_6 uppercase">
					{if $product->variant->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
					{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span> 
					{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span> 
					{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается {if $product->byrequest}( {$product->byrequest} ){/if} 
					{else}<span class="text_blue">В наличии</span> 
					{/if}
				</div>
					
				<span class="compare_link inline_block_middle text_5 text_gray2 text_right">
					{if $compare->ids && in_array($product->id, $compare->ids)}
						<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
						<span class="compare_link_text inline_block_middle">в сравнении</span>
					{else}
						<a href="/compare?id={$product->id}" class="compare_add" data-id="{$product->id}" data-key="compare" data-informer="1" data-result-text="в сравнении">
							<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
							<span class="compare_link_text inline_block_middle">сравнить</span>
						</a>
					{/if}
				</span>
						
				{* Контейнер с ценами *}
				<div class="price_container{if $product->variant->price == 0} hidden{/if}">
				
					{* Старая цена *}
					<div class="price old_price text_3 text_bold text_gray2{if !$product->variant->compare_price && !$user->discount &&	 !$product->variant->coupon_discount_price} hidden{/if}">
						<span class="value">
							{*Если есть старая цена, то выводим ее*}
							{if $product->variant->compare_price}
								{$product->variant->compare_price|convert}
							{*Если старой цены нет, но есть автоскидка или скидка пользователя, то выводим обычную цену вместо старой*}
							{elseif $product->variant->coupon_discount_price || $user->discount}
								{$product->variant->price|convert}
							{/if}
						</span>&nbsp;
						{$currency->sign|escape}
					</div>
					
					{* Разница между старой и новой ценой *}
					<span class="price discount_price discount_price_shop text_white text_5{if !$product->variant->compare_price} hidden{/if}">
						-&nbsp;<span class="value">{($product->variant->compare_price - $product->variant->price)|convert}</span>&nbsp;
						{$currency->sign|escape}
						<span class="discount_price_title hidden_xs hidden_sm animated">Акция магазина</span>
					</span>
					
					{* Скидка группы пользователя *}
					<span class="price discount_price discount_price_user text_white text_5{if !$user->discount} hidden{/if}">
						-&nbsp;<span class="value">{($product->variant->price * $user_group->discount/100)|convert}</span>&nbsp;
						{$currency->sign|escape}
						<span class="discount_price_title hidden_xs hidden_sm animated">Скидка постоянному покупателю</span>
					</span>

					{* Авто скидка по купону *}
					<span class="price discount_price discount_price_coupon text_white text_5{if !$product->variant->coupon_discount_price} hidden{/if}">
						-&nbsp;<span class="value">{$product->variant->coupon_discount_price|convert}</span>&nbsp;
						{$currency->sign|escape}
						<span class="discount_price_title hidden_xs hidden_sm animated">Скидка при покупке сейчас</span>
					</span>
					
					{* Итоговая цена *}
					<div class="price final_price text_bold text_black">
						<span class="value">{($product->variant->price - $product->variant->coupon_discount_price - ($product->variant->price * $user_group->discount/100))|convert}</span>&nbsp;
						<span class="currency">{$currency->sign|escape}</span>
					</div>
                   
				</div>
				
				<div class="variants_container{if $product->variants|count<2} hidden{/if}">
					{foreach $product->variants as $v}
						<div class="variant">
							<input id="product_{$v->id}" class="hidden" name="variant" value="{$v->id}" type="radio" 
								
								{if $product->variant->id==$v->id}checked {/if} 
								
								{if $v->sku}data-v_sku='{$v->sku}'{/if}
								
								{if $v->name}data-v_name="1"{/if}
								
								data-final_price="{($v->price - $v->coupon_discount_price - ($v->price * $user_group->discount/100))|convert}" 
					
								{if $v->compare_price}
									data-old_price="{$v->compare_price|convert}"
								{elseif $v->coupon_discount_price || $user->discount}
									data-old_price="{$v->price|convert}"
								{/if} 
								
								{if $v->compare_price}
									data-discount_price_shop="{($v->compare_price - $v->price)|convert}"
								{/if}
								
								{if $user_group->discount}
									data-discount_price_user="{($v->price * $user_group->discount/100)|convert}"
								{/if}  
								
								{if $v->coupon_discount_price}
									data-coupon_id="{$v->coupon_id}"
									data-discount_price_coupon="{$v->coupon_discount_price|convert}"
								{/if}
										
								data-stock='{if $v->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
											{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span>
											{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span>
											{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается
											{else}<span class="text_blue">В наличии</span>{/if}' 

                            />
							<label for="product_{$v->id}" class="variant_name checkbox_label">{$v->name}</label>
						</div>
					{/foreach}
				</div>
					
				<div class="button_container{if $product->variant->price == 0} hidden{/if}">
					
					<div class="clear">
						<div class="product_amount amount text_nowrap fleft">
							<span class="amount_trigger minus text_gray2 text_bold">-</span>
							<input class="amount_count form_input" type="tel" name="amount" value="1" data-max="{$product->variant->stock}">
							<span class="amount_trigger plus text_gray2 text_bold">+</span>
						</div>
						<button class="product_button fright button" type="submit">
							<span class="button_item icon_text">Купить</span>
							<span class="button_item icon hidden_xs">{include file="svg.tpl" svgId="icon_cart"}</span>
						</button>
					</div>
						
				</div>
			
			</form>
			
		</div>
	
	</div>
	</div>
	
</div>
{/strip}