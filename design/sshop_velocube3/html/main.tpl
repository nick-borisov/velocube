{strip}
{$wrapper = 'index.tpl' scope=parent}
{$canonical="" scope=parent}

{* Слайдер на главной *}
{*get_slides var=slide}
{if $slide}	
	<div class="main_slider_container relative">
		<div id="main_slider" class="main_slider">
		{foreach $slide as $s}
			{if $s->image}
				<div class="main_slider_item">
					{if $s->url}<a href="{$s->url}">{/if}
						<img src="{$s->image}" alt="{$s->name}" title="{$s->name}" />
					{if $s->url}</a>{/if}
				</div>
			{/if}
		{/foreach}
		</div>
		<div id="main_slider_control" class="main_slider_control container absolute"></div>
	</div>
{/if*}

<aside class="advantage section">
	<div class="advantage_inner container row flex_col" style="margin-top: 0;">

		{*Товар дня*}
		{get_ttoday_products_plugin var=today_products limit=1}
		{if $today_products}
			{foreach $today_products as $t_product}
				<div class="advantage_item today_product product col_xs_12 col_sm_6 col_md_4 col_lg_3 flex_item">
					{include file="product_block.tpl" product=$t_product sale_timer="1"}
				</div>
			{/foreach}
		{/if}
		{*/Товар дня*}
		
		<ul class="advantage_list flex_col flex_content_justify flex_item col_xs_12 col_sm_6 col_md_8 col_lg_9 no_padding">
			<li class="advantage_item col_xs_12 col_lg_4 flex_item">
				<div class="advantage_item_inner shadow_box relative animated">
					<div class="advantage_item_content shadow_box_item flex flex_wrap flex_align_center">
						<div class="advantage_item_icon col_xs_4 col_lg_12 flex_item text_right">
							<img src="design/{$settings->theme}/images/advantage_image_1.png" alt="" title="">
						</div>
						<div class="advantage_item_text col_xs_8 col_lg_12 flex_item">
							<div class="advantage_item_title text_medium text_black">
								<div class="text_2">Скидка 3% - </div>
								<div class="text_3">каждому</div>
							</div>
							<div class="advantage_item_annotation text_5 text_gray hidden_sm hidden_md">Зарегистрируйся и получи личную скидку в 3%</div>
							<a href="aktsii-i-skidki" class="link text_pink text_5">Подробнее</a>
						</div>
					</div>
				</div>
			</li>
			<li class="advantage_item col_xs_12 col_lg_4 flex_item">
				<div class="advantage_item_inner shadow_box relative animated">
					<div class="advantage_item_content shadow_box_item flex flex_wrap flex_align_center">
						<div class="advantage_item_icon col_xs_4 col_lg_12 flex_item text_right">
							<img src="design/{$settings->theme}/images/advantage_image_2.png" alt="" title="">
						</div>
						<div class="advantage_item_text col_xs_8 col_lg_12 flex_item">
							<div class="advantage_item_title text_medium text_black">
								<div class="text_2">Доставка - </div>
								<div class="text_3">быстрая и выгодная</div>
							</div>
							<div class="advantage_item_annotation text_5 text_gray hidden_sm hidden_md">Самая быстрая и выгодная доставка для Вас</div>
							<a href="dostavka" class="link text_pink text_5">Подробнее</a>
						</div>
					</div>
				</div>
			</li>
			<li class="advantage_item col_xs_12 col_lg_4 flex_item">
				<div class="advantage_item_inner shadow_box relative animated">
					<div class="advantage_item_content shadow_box_item flex flex_wrap flex_align_center">
						<div class="advantage_item_icon col_xs_4 col_lg_12 flex_item text_right">
							<img src="design/{$settings->theme}/images/advantage_image_3.png" alt="" title="">
						</div>
						<div class="advantage_item_text col_xs_8 col_lg_12 flex_item">
							<div class="advantage_item_title text_medium text_black">
								<div class="text_2">Выбор - </div>
								<div class="text_3">огромный</div>
							</div>
							<div class="advantage_item_annotation text_5 text_gray hidden_sm hidden_md">Большой выбор велосипедов и запчастей к ним</div>
							<a href="catalog/velosport" class="link text_pink text_5">Подробнее</a>
						</div>
					</div>
				</div>
			</li>
		</ul>		

	</div>
	
	{get_brands var=all_brands}
	{if $all_brands}
		<div class="brand_carousel_container container">
			<div class="section_title">
				<div class="title text_2">Бренды</div>
				<a class="section_title_link link_inverse text_gray2 text_5" href="/all_brands">Показать все бренды</a>
			</div>
			<div id="brand_carousel" class="brand_carousel">
				{foreach $all_brands as $b}	
					{if $b@index <= 20}
					<div class="brand_carousel_item text_center">
						<a data-brand="{$b->id}" href="brands/brand-{$b->url}">
							{if $b->image}
								<span class="brand_carousel_image"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></span>
							{else}
								<span class="brand_carousel_name inline_block_middle uppercase text_bold">{$b->name|escape}</span>
							{/if}		
						</a>
					</div>
					{/if}
				{/foreach}
			</div>
		</div>
	{/if}
</aside>

{* Популярные товары *}
{get_pop_products var=popular_products limit=10}
{if $popular_products}
<section class="shadow_section">
	<div class="container row">
		<div class="carousel_container">
			<h2 class="title">Популярные товары</h2>
			<div class="products grid products_carousel">
				{foreach $popular_products as $p_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$p_product}
					</div>
				{/foreach}
			</div>
		</div>
	</div>
</section>
{/if}

<div class="section">
	<div class="container">
	
		{*function name=categories_images}
		{if $categories}
			<section class="frontpage_category">
				<h2 class="title">Категории</h2>
				<div class="frontpage_category_list flex flex_wrap row">
				{foreach $categories as $c}
					{if $c->visible}
						<a class="frontpage_category_item text_center flex_item col_xs_4" href="catalog/{$c->url}" >
							<span class="frontpage_category_image"{if $c->image} style="background-image:url({$config->categories_images_dir}{$c->image});"{/if}></span>
							<span class="frontpage_category_name text_6 uppercase text_black">{$c->name|escape}</span>
						</a>
					{/if}
				{/foreach}
				</div>
			</section>
		{/if}
		{/function}
		{categories_images categories=$categories*}
	
		{* Новинки *}
		{get_new_products var=new_products limit=10}
		{if $new_products}
		{*<div class="separator"></div>*}
		<section class="carousel_container">
			<h2 class="title">Новинки</h2>
			<div class="products grid products_carousel row">
				{foreach $new_products as $n_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$n_product}
					</div>
				{/foreach}
			</div>
		</section>
		{/if}
		
		{* Рекомендуемые товары *}
		{get_featured_products var=featured_products limit=10}
		{if $featured_products}
		<div class="separator"></div>
		<section class="featured_products carousel_container">
			<div class="section_title">
				<h2 class="title">Хиты продаж</h2>
				<a class="section_title_link link_inverse text_5 text_gray2" href="/hits">Все хиты продаж</a>
			</div>
			<div class="products grid products_carousel row">
				{foreach $featured_products as $f_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$f_product}
					</div>
				{/foreach}
			</div>
		</section>
		{/if}
	
	</div>
</div>

<section class="shadow_section">
	<div class="container">
	
		<div class="section_title">
			<h2 class="title">Последние новости</h2>
			<a class="section_title_link link text_5 text_gray2" href="/blog">Показать все новости</a>
		</div>
		
		{* Выбираем в переменную $last_posts последние записи *}
		{get_posts var=last_posts limit=3}
		{if $last_posts}
			<div class="posts object_list row flex flex_wrap">
				{foreach $last_posts as $post}
					<article class="post object_list_item flex_item col_xs_12 col_sm_6 col_md_4">
						<div class="post_inner object_list_inner shadow_box relative animated">
							<a class="post_image shadow_box_item text_center{if !$post->image} post_image_empty{/if}" href="blog/{$post->url}">
								{if $post->image}
									<img src="{$post->image|resize:300:150:false:$config->resized_blog_dir}" alt="{$post->name|escape}" title="{$post->name|escape}"/>
								{/if}
							</a>
							<div class="post_text shadow_box_item">
								<h3 class="post_title text_4 text_normal"><a href="blog/{$post->url}" data-post="{$post->id}">{$post->name|escape}</a></h3>
								{if $post->annotation}<div class="post_annotation text_gray text_5">{$post->annotation|strip_tags|truncate:80}</div>{/if}
							</div>
							<div class="post_footer shadow_box_item no_border flex flex_align_center flex_content_justify">
								<a class="post_link text_pink text_5" href="blog/{$post->url}">
									<span class="link_inverse inline_block">Подробнее</span>&nbsp;
									<i class="inline_block_middle">{include file="svg.tpl" svgId="icon_arrow_right" width="12px" height="12px"}</i>
								</a>
								<time class="text_5 text_gray2" datetime="{$post->date}">{$post->date|date}</time>
							</div>
						</div>
					</article>
				{/foreach}
				{*
				<div class="object_list_item flex_item col_xs_12 col_sm_12 col_md_4">
					<div id="vk_groups" class="shadow_box object_list_inner"></div>
				</div>
				*}
			</div>
		{/if}
	</div>
</section>

{if $smarty.session.admin}
{get_comments var=main_comments limit=5}
{if $main_comments}
<section class="fp-reviews section section--dark">
	<div class="container">
		<h2 class="title text-1">Отзывы клиентов</h2>
		<div class="fp-reviews__content flex flex--align_center flex--center">
			
			<div class="col-6">
				<div id="fp-reviews__list" class="fp-reviews__list">
					{foreach $main_comments as $comment}
					{if $comment->approved}
						<div class="fp-reviews__item">
							<div class="fp-reviews__item-header text-3 text-bold clear">
								<div class="fp-reviews__name">{$comment->name}{if $comment->site_url} - <span class="text-blue">{$comment->site_url}</span>{/if}</div>
								<div class="fp-reviews__date text-right">{$comment->date|date}</div>
							</div>
							{$comment->text|escape|nl2br}
						</div>
					{/if}
					{/foreach}
				</div>
			</div>
		</div>
		<div class="text-center">
			<a class="button" href="/comments"><span class="button__content">Все отзывы</span></a>
		</div>
	</div>
</section>
{/if}

{/if}

<section class="section">
	<div class="container">
		<img src="design/{$settings->theme}/images/main_footer_slide.jpg" alt="" title="">
		{if $page->body}
			<div class="separator"></div>
			<div class="spoiler user_content text_5 text_gray">{$page->body}</div>
		{/if}
	</div>
</section>

{*
<div class="news">
	<h2>Новости и акции</h2>
	{get_posts var=last_posts limit=5}
	{if $last_posts}
	<ul>
		{foreach $last_posts as $post}
			<li data-post="{$post->id}">{$post->date|date} <a href="blog/{$post->url}">{$post->name|escape}</a></li>
		{/foreach}
	</ul>
	{/if}
</ul>
<a href="blog">Еще новости и акции</a>
</div>
*}

{*literal}<script>


$('a.quick_viev').live('click', function(){
    var pro_id = this.getAttribute('rel');

    $.arcticmodal({
    type: 'ajax',
    url: 'ajax/quick.php',
    ajax: {
        type: 'GET',
        cache: false,
        data: {id: pro_id},
        dataType: 'json',
        success: function(data, el, response) {
          
           data.body.html(response);
        }
    }
	});
});



</script>{/literal*}

{/strip}