{strip}
{$canonical="/products/{$product->url}" scope=parent}

<main class="main" itemscope itemtype="http://schema.org/Product">

	<div id="product" class="product product_single shadow_section">
		<div class="container">
		
		
			
			<meta itemprop="brand" content="{$brand->name|escape}"/>
			
			<div class="row">
			
				<div class="col_lg_9">{include file="path.tpl"}</div>
				
				<div class="product_share col_lg_3">
					<div class="product_share_title inline_block_middle text_bold text_gray">Поделиться:</div>
					<div class="product_share_count inline_block_middle ya-share2" data-services="vkontakte,facebook,gplus,odnoklassniki" data-counter=""></div>
				</div>

				<div class="product_main col_xs_12 col_md_7 col_lg_8">
					<div class="shadow_box">

						<div class="product_toolbar">
						
							<div class="relative">
						
								<div class="product_sku text_gray2 text_5{if !$product->variant->sku} hidden{/if}">
									Артикул: <span class="value">{$product->variant->sku}</span>
								</div>

								<h1 class="product_name text_4 text_black" data-product="{$product->id}" itemprop="name" data-product_name="{$product->fullname|escape}">
									<span class="product_prefix text_6 uppercase">{$product->type_prefix|escape}</span>
									<span class="text_2 text_bold">{$brand->name|escape} {$product->name|escape}</span>
								</h1>

								{* Суммарный рейтинг товара *}
								<div class="rating inline_block_middle" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
								
									<meta itemprop="ratingValue" content="{$product->rating}" />
									<meta itemprop="reviewCount" content="{$product->votes}" />
									
									<span class="rating_starOff">
										<span class="rating_starOn" style="width:{if $product->rating > 0}{$product->rating*85/5|string_format:'%.0f'}{else}0{/if}px"></span>
									</span>
									
									<span class="rating_text text_5 text_gray2">
										{if $product->rating > 0}
											Рейтинг: <span class="rating_value text_bold text_gray">{$product->rating}</span> 
											&nbsp;/&nbsp;
											<a id="show_review_link" href="#product_comment" class="link pseudo_link text_blue anchor_link">{$product->comcon|string_format:"%.0f"} {$product->comcon|plural:'отзыв':'отзывов':'отзыва'}</a>
										{else}
											<a id="show_review_link" href="#product_comment" class="link pseudo_link text_blue anchor_link">Оставить отзыв</a>
										{/if}
									</span>

								</div>
								{* /Суммарный рейтинг товара *}
							
							</div>
							
						</div>
						
						
						<div id="product_gallery" class="product_gallery product_image text_center" data-product_image="{$product->image->filename|resize:200:200}">
						
							{* Прикрепленные видео к товару *}
							{foreach $product->videos as $video}
								<div class="product_gallery_item" data-thumb="design/{$settings->theme|escape}/images/icon_video.svg">
									<div class="product_gallery_video relative">
										<iframe class="absolute" src="https://www.youtube.com/embed/{$video->vid}" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							{/foreach}
						
							{* Галерея изображений *}
							{foreach $product->images as $i=>$image}
								<div class="product_gallery_item{if $product->videos|count > 0 && $image@first} product_gallery_first{/if}" data-thumb="{$image->filename|resize:60:40}">
									<a class="fancybox_gallery" href="{$image->filename|resize:800:600}" data-fancybox-group="gallery">
										<img src="{$image->filename|resize:600:400}" alt="{$product->fullname|escape}" />
									</a>
								</div>
							{/foreach}
							
						</div>
						
						<div class="product_service visible_lg_flex row text_6 text_gray2">
							<div class="product_service_item col_md_4 flex_item">
								<i class="icon product_service_icon">{include file="svg.tpl" svgId="icon_free_delivery" width="40px" height="40px"}</i>
								<span class="icon_text product_service_text">
									Закажите на сумму от&nbsp;
									<span class="product_service_note text_bold text_nowrap"><span class="text_2">{$settings->interval_price}</span> рублей</span>&nbsp;
									и мы доставим бесплатно!
								</span>
							</div>
							<div class="product_service_item col_md_4 flex_item">
								<i class="icon product_service_icon">{include file="svg.tpl" svgId="icon_clock" width="40px" height="40px"}</i>
								<span class="icon_text product_service_text">
									Подтвердим ваш заказ в рабочее время в течение&nbsp;
									<span class="product_service_note text_2 text_bold text_nowrap">10 минут !</span>
								</span>
							</div>
							<div class="product_service_item col_md_4 flex_item">
								<i class="icon product_service_icon">{include file="svg.tpl" svgId="icon_gift" width="40px" height="40px"}</i>
								<span class="icon_text product_service_text">
									Оформите заказ через сайт и мы сделаем для Вас&nbsp;
									<span class="product_service_note text_2 text_bold text_nowrap">Скидку!</span>
								</span>
							</div>
						</div>

					</div>
				</div>
				
				<div class="product_info col_xs_12 col_md_5 col_lg_4">
					<form class="variants shadow_box" action="/cart" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					
						<meta itemprop="acceptedPaymentMethod" content="CreditCard" />
						<meta itemprop="priceCurrency" content="RUB" />
						<meta itemprop="availability" content="http://schema.org/InStock" />
						
						<div class="shadow_box_item">
						
							<div id="product_stock" class="product_stock inline_block_middle text_bold text_6 uppercase">
								{if $product->variant->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
								{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span> 
								{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span> 
								{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается {if $product->byrequest}( {$product->byrequest} ){/if} 
								{else}<span class="text_blue">В наличии</span> 
								{/if}
							</div>
								
							<span class="compare_link inline_block_middle text_5 text_gray2 text_right">
								{if $compare->ids && in_array($product->id, $compare->ids)}
									<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
									<span class="compare_link_text inline_block_middle">в сравнении</span>
								{else}
									<a href="/compare?id={$product->id}" class="compare_add" data-id="{$product->id}" data-key="compare" data-informer="1" data-result-text="в сравнении">
										<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
										<span class="compare_link_text inline_block_middle">сравнить</span>
									</a>
								{/if}
							</span>
							
						</div>
						
						{* Контейнер с ценами *}
						<div class="price_container shadow_box_item{if $product->variant->price == 0} hidden{/if}">
						
							{* Старая цена *}
							<div class="price old_price text_3 text_bold text_gray2{if !$product->variant->compare_price && !$user->discount &&	 !$product->variant->coupon_discount_price} hidden{/if}">
								<span class="value">
									{*Если есть старая цена, то выводим ее*}
									{if $product->variant->compare_price}
										{$product->variant->compare_price|convert}
									{*Если старой цены нет, но есть автоскидка или скидка пользователя, то выводим обычную цену вместо старой*}
									{elseif $product->variant->coupon_discount_price || $user->discount}
										{$product->variant->price|convert}
									{/if}
								</span>&nbsp;
								{$currency->sign|escape}
							</div>
							
							{* Разница между старой и новой ценой *}
							<span class="price discount_price discount_price_shop text_white text_5{if !$product->variant->compare_price} hidden{/if}">
								-&nbsp;<span class="value">{($product->variant->compare_price - $product->variant->price)|convert}</span>&nbsp;
								{$currency->sign|escape}
								<span class="discount_price_title hidden_xs hidden_sm animated">Акция магазина</span>
							</span>
							
							{* Скидка группы пользователя *}
							<span class="price discount_price discount_price_user text_white text_5{if !$user->discount} hidden{/if}">
								-&nbsp;<span class="value">{($product->variant->price * $user_group->discount/100)|convert}</span>&nbsp;
								{$currency->sign|escape}
								<span class="discount_price_title hidden_xs hidden_sm animated">Скидка постоянному покупателю</span>
							</span>

							{* Авто скидка по купону *}
							<span class="price discount_price discount_price_coupon text_white text_5{if !$product->variant->coupon_discount_price} hidden{/if}">
								-&nbsp;<span class="value">{$product->variant->coupon_discount_price|convert}</span>&nbsp;
								{$currency->sign|escape}
								<span class="discount_price_title hidden_xs hidden_sm animated">Скидка при покупке сейчас</span>
							</span>
							
							{* Итоговая цена *}
							<div class="price final_price text_bold text_black">
								<span class="value">{($product->variant->price - $product->variant->coupon_discount_price - ($product->variant->price * $user_group->discount/100))|convert}</span>&nbsp;
								<span class="currency">{$currency->sign|escape}</span>
							</div>
                           
						</div>
						
						<div class="variants_container shadow_box_item{if $product->variants|count<2 && !$product->variant->name} hidden{/if}">
							{foreach $product->variants as $v}
								<div class="variant">
									<input id="product_{$v->id}" class="hidden" name="variant" value="{$v->id}" type="radio" 
										
										{if $product->variant->id==$v->id}checked {/if} 
										
										{if $v->sku}data-v_sku='{$v->sku}'{/if}
										
										{if $v->name}data-v_name="1"{/if}
										
										data-final_price="{($v->price - $v->coupon_discount_price - ($v->price * $user_group->discount/100))|convert}" 
							
										{if $v->compare_price}
											data-old_price="{$v->compare_price|convert}"
										{elseif $v->coupon_discount_price || $user->discount}
											data-old_price="{$v->price|convert}"
										{/if} 
										
										{if $v->compare_price}
											data-discount_price_shop="{($v->compare_price - $v->price)|convert}"
										{/if}
										
										{if $user_group->discount}
											data-discount_price_user="{($v->price * $user_group->discount/100)|convert}"
										{/if}  
										
										{if $v->coupon_discount_price}
											data-coupon_id="{$v->coupon_id}"
											data-discount_price_coupon="{$v->coupon_discount_price|convert}"
										{/if}
												
										data-stock='{if $v->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
													{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span>
													{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span>
													{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается
													{else}<span class="text_blue">В наличии</span>{/if}' 
													
										data-delivery='<span id="tomorrow_osn"></span>'
										
										data-delivery_self='<span class="samovyvoz{if ($v->stock == 0 || $product->sklad_id != 0 || !$v->store || $v->store==3)}_no{elseif intval(date('H', time())) >= 19}_next_day{/if}"></span>'

                                    />
									<label for="product_{$v->id}" class="variant_name checkbox_label">{$v->name}</label>
								</div>
							{/foreach}
						</div>
							
						<div class="shadow_box_item button_container{if $product->variant->price == 0} hidden{/if}">
							
							<div class="clear">
								<div class="product_amount amount text_nowrap fleft">
									<span class="amount_trigger minus text_gray2 text_bold">-</span>
									<input class="amount_count form_input" type="tel" name="amount" value="1" data-max="{$product->variant->stock}">
									<span class="amount_trigger plus text_gray2 text_bold">+</span>
								</div>
								<button class="product_button fright button" type="submit">
									<span class="button_item icon_text">Купить</span>
									<span class="button_item icon hidden_md">{include file="svg.tpl" svgId="icon_cart"}</span>
								</button>
							</div>
							
							<a class="oneclick_link link pseudo_link text_gray fancybox" href="#oneclick_form">Купить быстро в 1 клик</a>
								
						</div>
						
					</form>
					
					<div id="product_delivery_container" class="product_delivery_container clear text_center">
						<div class="product_delivery col_xs_6">
							<span id="tomorrow_osn"></span> 
						</div>
						<div class="product_delivery_self col_xs_6">
							<span class="samovyvoz{if ($product->variant->stock == 0 || $product->sklad_id != 0 || !$product->variant->store || $product->variant->store==3)}_no{elseif intval(date("H", time())) >= 19}_next_day{/if}"></span>
						</div>
					</div>
				    	
 				</div> 
	
			</div>
		</div>
	</div>
	
	{*если все варианты товара не в наличии, то перед описание выводим похожие товары*}
	{if !$available && $similar_products}
		<section class="section similar_products">
			<div class="container row">
				<h2 class="title">Похожие товары</h2>
				<div class="products grid products_carousel">
					{foreach $similar_products as $s_product}
						<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
							{include file='product_block.tpl' product=$s_product}
						</div>
					{/foreach}
				</div>
			</div>
		</section>
	{/if}
	
	<div id="product_tab" class="product_tab tab_container section relative">
		
		<div class="tab_header text_center text_white hidden_xs hidden_sm">
			<div class="container flex flex_content_justify">
				{if $product->body}<a href="#product_description" class="tab_link text_3 text_bold uppercase animated">Описание</a>{/if}
				{if $product->features}<a href="#product_features" class="tab_link text_3 text_bold uppercase animated">Характеристики</a>{/if}
				{*if $product->video}<a href="#product_video" class="tab_link text_3 text_bold uppercase animated">Видео обзор</a>{/if*}
				<a href="#product_comment" class="tab_link text_3 text_bold uppercase animated">Отзывы</a>
				{if $product->delivery_description}<a href="#product_delivery_description" class="tab_link text_3 text_bold uppercase animated">Доставка</a>{/if}
			</div>
		</div>
		
		<div class="tab_body container">
		
			{if $product->body}
				<a href="#product_description" class="tab_link relative text_3 text_bold uppercase animated hidden_md hidden_lg">
					<span class="icon icon_more">
						<span class="icon_more_line line_1"></span>
						<span class="icon_more_line line_2"></span>
					</span>
					<span class="icon_text">Описание</span>
				</a>
				<div id="product_description" class="tab">
					<div class="tab_inner user_content clear" itemprop="description">{$product->body}</div>
					{if !$product->features || ($product->body && $product->features)}
						<div class="near_products flex flex_align_center row text_5 text_gray2 hidden_xs hidden_sm">
							{if $prev_product->url}
								<div class="near_products_link near_products_prev flex_item col_xs_auto">
									<a class="flex flex_align_center" href="products/{$prev_product->url}">
										<span class="icon">{include file="svg.tpl" svgId="icon_arrow_left"}</span>
										<span class="icon_text">{$prev_product->name|escape}</span>
									</a>
								</div>
							{/if}
							{if $next_product->url}
								<div class="near_products_link near_products_next flex_item col_xs_auto">
									<a class="flex flex_align_center" href="products/{$next_product->url}">
										<span class="icon_text">{$next_product->name|escape}</span>
										<span class="icon">{include file="svg.tpl" svgId="icon_arrow_right"}</span>
									</a>
								</div>
							{/if}
						</div>
					{/if}
				</div>
			{/if}
			
			{if $product->features}
				<a href="#product_features" class="tab_link relative text_3 text_bold uppercase animated hidden_md hidden_lg">
					<span class="icon icon_more">
						<span class="icon_more_line line_1"></span>
						<span class="icon_more_line line_2"></span>
					</span>
					<span class="icon_text">Характеристики</span>
				</a>
				<div id="product_features" class="tab">
					<div class="tab_inner">
						<ul class="product_feature">
							{foreach $product->features as $f}
								<li class="product_feature_item">
									<div class="product_feature_name text_blue2 uppercase text_bold">{$f->name}</div>
									<div class="product_feature_value">{$f->value}</div>
								</li>
							{/foreach}
						</ul>
						<br>
						<div class="text_5 text_gray">Фирма-производитель оставляет за собой право на внесение изменений в конструкцию, дизайн и комплектацию приборов без предварительного уведомления. Во избежание недоразумений при покупке приборов уточняйте информацию о комплектации, наличию и цене у менеджера магазина.</div>
					</div>
					{if !$product->body}
						<div class="near_products flex flex_align_center row text_5 text_gray2 hidden_xs hidden_sm">
							{if $prev_product->url}
								<div class="near_products_link near_products_prev flex_item col_xs_auto">
									<a class="flex flex_align_center" href="products/{$prev_product->url}">
										<span class="icon">{include file="svg.tpl" svgId="icon_arrow_left"}</span>
										<span class="icon_text">{$prev_product->name|escape}</span>
									</a>
								</div>
							{/if}
							{if $next_product->url}
								<div class="near_products_link near_products_next flex_item col_xs_auto">
									<a class="flex flex_align_center" href="products/{$next_product->url}">
										<span class="icon_text">{$next_product->name|escape}</span>
										<span class="icon">{include file="svg.tpl" svgId="icon_arrow_right"}</span>
									</a>
								</div>
							{/if}
						</div>
					{/if}
				</div>
			{/if}
			
			{*if $product->video}
				<a href="#product_video" class="tab_link relative text_3 text_bold uppercase animated hidden_md hidden_lg">
					<span class="icon icon_more">
						<span class="icon_more_line line_1"></span>
						<span class="icon_more_line line_2"></span>
					</span>
					<span class="icon_text">Видео обзор</span>
				</a>
				<div id="product_video" class="tab">
					<div class="tab_inner clear">{$product->video}</div>
				</div>
			{/if*}
			
			<a href="#product_comment" class="tab_link relative text_3 text_bold uppercase animated hidden_md hidden_lg">
				<span class="icon icon_more">
					<span class="icon_more_line line_1"></span>
					<span class="icon_more_line line_2"></span>
				</span>
				<span class="icon_text">Отзывы</span>
			</a>
			<div id="product_comment" class="tab">
				<div class="tab_inner clear">{include file="comment_block.tpl"}</div>
			</div>
			
			{if $product->delivery_description}
				<a href="#product_delivery_description" class="tab_link relative text_3 text_bold uppercase animated hidden_md hidden_lg">
					<span class="icon icon_more">
						<span class="icon_more_line line_1"></span>
						<span class="icon_more_line line_2"></span>
					</span>
					<span class="icon_text">Доставка</span>
				</a>
				<div id="product_delivery_description" class="tab">
					<div class="tab_inner user_content clear">{$product->delivery_description}</div>
				</div>
			{/if}
			
		</div>

	</div>
	
</main>

{get_browsed_products var=browsed_products limit=10}
{if $related_products || $similar_products || $browsed_products}

<section class="section related_products">
	<div class="container">
	
		{if $related_products}
			<div class="separator"></div>
			<h2 class="title">Так же советуем посмотреть</h2>
			<div class="products grid products_carousel">
				{foreach $related_products as $r_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$r_product}
					</div>
				{/foreach}
			</div>
		{/if}

		{if $available && $similar_products}
			<div class="separator"></div>
			<h2 class="title">Похожие товары</h2>
			<div class="products grid products_carousel">
				{foreach $similar_products as $s_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$s_product}
					</div>
				{/foreach}
			</div>
		{/if}
		
		{if $browsed_products}
			<div class="separator"></div>
			<h2 class="title">Вы недавно смотрели</h2>
			<div class="products grid products_carousel">
				{foreach $browsed_products as $b_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$b_product}
					</div>
				{/foreach}
			</div>
		{/if}
		
	</div>
</section>
{/if}

{*

<section class="shadow_section">
	<div class="container">
		<h2 class="title">{$product->fullname|escape}{if $brand} от {$brand->name|escape}{/if} - как купить?</h2>
		<div class="spoiler user_content">
			<p>Добро пожаловать в Velocube - лучший магазин спортивных товаров и комплектующих в столице!</p>
			<p>Покупать у нас комфортно - мы позаботились о том, чтобы удовлетворить каждого нашего клиента и ввели 2 способа покупки. Первый - для тех, кто хочет быть уверенным в каждом шаге и хочет получить дополнительные гарантии или оставить свои комментарии для неординарного заказа. Он подразумевает традиционную регистрацию и покупку через личный кабинет.</p>
			<p>Второй способ - для тех, кто не любит ждать и хочет получить свой заказ как можно быстрее. Речь идет об уникальной системе One Click, которая позволяет оформить заказ посредством всего одного клика! Для того, чтобы <strong	>купить {$product->fullname|lower|capitalize} от {$brand->name|escape}</strong> у нас, достаточно просто положить выбранную позицию в корзину, использовав специальную кнопку под названием товара на странице. Далее нужно заполнить простую форму, оставив нам свои данные, необходимые для дальнейшей работы, после чего наши сотрудники свяжутся с Вами для уточнения деталей оплаты Вашей покупки.</p>
			<p>Для тех, кто ценит скорость и комфорт интернет шопинга превыше всего, у нас работает оплата при помощи пластиковых карт  - самый быстрый и проверенный способ для современного пользователя.</p>
			<p>Продажа спортивного оборудования от Velocube осуществляются по всей территории Россиийской Федерации.</p>
			<p>Для жителей Москвы доступны два способа получения своего заказа -  доставка лично в руки, посредством курьерской службы, и самовывоз, а жителям регионов мы предлагаем получить {$product->fullname|lower|capitalize} от {$brand->name|escape}, воспользовавшись услугами междугородних компаний-перевозчиков и предварительно оплатив свою покупку в полном объеме.</p><p>Работать с нами не только удобно, но и выгодно - стоимость в интернет-магазине Velocube заметно дешевле, чем в розничных магазинах и на специализированных рынках. Все позиции, представленные в нашем каталоге, имеют сертификаты подлинности от производителей, а потому, покупая у нас, Вы экономите на цене, но не на качестве!</p>
			
			<p>Ждем Ваших звонков!</p>
		</div>
	</div>
</section>
*}

{/strip}