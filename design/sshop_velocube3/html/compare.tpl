﻿{strip}
{get_prods var=products get_session_products=compare category_id=$smarty.get.category data_features=1 data_categories=1}

{if $products|count>0}

	{foreach $products as $product}
		{foreach $product->options as $o}
			{$compare_features[{$o->feature_id}] = ['id'=>{$o->feature_id},'name'=>{$o->name}]}
			{$compare_products[{$o->product_id}][{$o->feature_id}] = {$o->value}}
			{$products_categories[$product->category->id] = $product->category}
		{/foreach}
	{/foreach}

	<main class="main section">
		<div class="container">
		
		{* Хлебные крошки *}
		{include file="path.tpl"}

		<h1 class="page_title text_black" data-page="{$page->id}">{$page->header|escape}</h1>

		{if $products_categories}
		<div class="compare_filter text_5 hidden_xs">
			<a class="compare_filter_item button button_small button_transparent{if !$smarty.get.category} active{/if}" href="/compare">Все категории</a>
			{foreach $products_categories as $c}
				<a href="{url params=[category=>$c->id]}" class="compare_filter_item button button_small button_transparent{if $c->id == $smarty.get.category} active{/if}">{$c->name|escape}</a>
			{/foreach}
		</div>
		{/if}
		
		<div class="compare_container">
		<table class="compare_table table">
			<tr>
				<td></td>
				{foreach $products as $p}
				<td>
					<div class="compare_product_image text_center"><a href="products/{$p->url}"><img src="{$p->image->filename|resize:200:200}" alt="{$p->fullname|escape}"/></a></div>
					<a class="compare_product_name text_black" href="products/{$p->url}">{$p->fullname}</a>
					<div class="compare_price price text_gray text_2">{$p->variant->price|convert} <span class="currency">{$currency->sign|escape}</span></div>
					{$category=$smarty.get.category}
					{if $p|count==1}{$category=null}{/if}
					<a href="{url params=[category=>$category, id=>$p->id, remove=>$p->id]}" class="compare_remove link pseuso_link text_red" data-id="{$p->id}" data-key="compare" data-informer="1" >удалить</a>
				</td>
				{/foreach}
			</tr>
			{foreach $compare_features as $f}
			<tr>
				<td>{$f.name}</td>
				{foreach $products as $p}
				<td>{if {$compare_products.{$p->id}.{$f.id}}}{$compare_products.{$p->id}.{$f.id}}{else}-{/if}</td>
				{/foreach}
			</tr>
			{/foreach}
		</table>
		</div>
	</div>
	</main>

{else}
	<div class="main page_empty container flex flex_align_center flex_content_center">
		<div class="shadow_box">
			<div class="page_empty_text shadow_box_item text_2 text_bold">В сравнении нет товаров</div>
		</div>
	</div>
{/if}


{/strip}