{strip}
{* Канонический адрес страницы *}
{if $category && $brand}
{$canonical="/catalog/{$category->url}/{$brand->url}" scope=parent}
{elseif $category && $mark}
{$canonical="/catalog/{$category->url}/tag/{$mark->url}" scope=parent}
{elseif $category}
{$canonical="/catalog/{$category->url}" scope=parent}
{elseif $brand}
{$canonical="/brands/{$brand->url}" scope=parent}
{elseif $keyword}
{$canonical="/products?keyword={$keyword|escape}" scope=parent}
{else}
{$canonical="/products" scope=parent}
{/if}

<main class="main section">
	<div class="container">
	
		{* Хлебные крошки *}
		{include file="path.tpl"}
		
		<div class="page_toolbar">
			
			{* Слайдер в категории *}
			{get_cslides var=slide cat=$category->id brand=$brand->id}
			{if $slide}	
				<div id="products_slider" class="products_slider">
				{foreach $slide as $sl}
					<div>
						<img src="{$sl->image}" alt="{$sl->name}" title="{$sl->name}"> 
					</div>
				{/foreach}
				</div>
			{/if}
			{* /Слайдер в категории *}
		
			{* Заголовок страницы *}
			<h1 class="text_black">
				{if $keyword}
					Поиск {$keyword|escape}
				{elseif $page}
					{$page->name|escape}
				{else}
					{if !$mark}{$category->name|escape}{/if} {$brand->name|escape} {$mark->name|escape} {$keyword|escape}
				{/if}
			</h1>
			{* /Заголовок страницы *}
			
			{* Подкатегории *}
			{if $cat_child}
			<ul class="subcategories_list row hidden_xs hidden_sm">
				{foreach $cat_child as $ch}
					{if $ch->visible && !$ch->metka}
						<li class="subcategories_item col_md_4 col_lg_3">
							<a class="subcategories_link shadow_box animated" href="/catalog/{$ch->url}">
								<span class="subcategories_image inline_block_middle">{if $ch->image}<img src="{$config->categories_images_dir}{$ch->image}" alt="{$ch->name|escape}">{/if}</span>
								<span class="subcategories_name inline_block_middle">
								{if $ch->short_name}
									{$ch->short_name|escape}
								{else}
									{$ch->name|escape}
								{/if}
								</span>
							</a>
						</li>
					{/if}
				{/foreach}
			</ul>
			{/if}
			{* /Подкатегории *}
			
			{* Бренды категории *}
			{if $category->brands}
			<div class="brand_carousel_container">
				<div class="section_title">
					<div class="text_2 title">Бренды</div>
					<a class="section_title_link link_inverse absolute text_gray2 text_5" href="/all_brands">Показать все бренды</a>
				</div>
				<div id="brand_carousel" class="brand_carousel">
					{foreach name=brands item=b from=$category->brands}
						{if $b@index <= 20 && !$b->disabled && !$b->in_filter}
						<div class="brand_carousel_item text_center">
							<a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}">
								{if $b->image}
									<span class="brand_carousel_image"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></span>
								{else}
									<span class="brand_carousel_name inline_block_middle uppercase text_bold">{$b->name|escape}</span>
								{/if}		
							</a>
						</div>
						{/if}
					{/foreach}
				</div>
			</div>
			{/if}
			
		</div>
		
		{if $products}
		
			<div class="products_toolbar row">
			
				{if $products && ($category->brands || $features)}
				<div class="products_toolbar_item col_xs_12 col_sm_5 col_md_4 col_lg_3">
					<span id="filter_trigger" class="button button_block button_transparent">Фильтр товаров</span>
				</div>
				{/if}
			
				{*Переключение вида*}
				<div id="change_view" class="change_view products_toolbar_item text_blue2 col_sm_3 hidden_xs">
					<span class="change_view_icon icon grid animated" title="Вид плиткой" data-view="grid">
						{include file="svg.tpl" svgId="icon_grid"}
					</span>
					<span class="change_view_icon icon list animated" title="Вид списком" data-view="list">
						{include file="svg.tpl" svgId="icon_list"}
					</span>
				</div>
			
				{*Сортировка*}
				<div class="sort products_toolbar_item fright col_xs_12 col_sm_4">
					<div class="sort_title text_gray2 text_5">Сортировать товары:</div>
					<select onchange="location = this.value;">
						<option {if $sort=='position'}selected{/if} value="{url sort=position page=null}"> по умолчанию</option>
						<option {if $sort=='viewed'}selected{/if} value="{url sort=viewed page=null}">по популярности</option>
                        <option {if $sort=='name'}selected{/if} value="{url sort=name page=null}">по названию</option>
						<option {if $sort=='price_r'}selected{/if} value="{url sort=price_r page=null}">сначала дорогие</option>
                        <option {if $sort=='price'}selected{/if} value="{url sort=price page=null}">сначала дешевые</option>
						{*<option {if $sort=='name_r'}selected{/if} value="{url sort=name_r page=null}">по названию я..А</option>*}
					</select>
				</div>
			
			</div>
	
			<div class="row">

				{*Фильтр в категории*}
				{if $products && ($category->brands || $features)}
				<aside id="products_sidebar" class="products_sidebar sidebar col_md_4 col_lg_3 animated">
					{include file="filter.tpl"}
					<input type="hidden" id="f_cat" value="{$category->id}">
					<input type="hidden" id="f_mark" value="{$mark->id}">
					<input type="hidden" id="f_brand" value="{$brand->id}">
					<input type="hidden" id="f_word" value="{$keyword}">
				</aside>
				{/if}
				
				{*Товары*}
				<div class="products_content {if $products && ($category->brands || $features)}col_md_8 col_lg_9 animated{else}col_sm_12{/if}">		
					<ul id="products" class="products object_list row animated">
						{foreach $products as $product}
							<li class="product object_list_item col_xs_12 col_sm_6 col_md_4 col_lg_3">
								{include file='product_block.tpl'}
							</li>
						{/foreach}
					</ul>
					{if $is_more}
						<button id="products_load_more" class="products_load_more button" type="button" data-page="{if $smarty.get.page}{$smarty.get.page + 1}{else}2{/if}">
							<span class="button_item icon">{include file="svg.tpl" svgId="icon_arrow_circle"}</span>
							<span class="button_item icon_text">Показать еще</span>
						</button>
                        
					{/if}
                    {include file="pagination.tpl"}	
				</div>
			
			</div>
			
			{get_browsed_products var=browsed_products limit=10}
			{if $browsed_products}
				<div class="separator"></div>
				<div class="carousel_container">
					<h2 class="title">Вы недавно смотрели</h2>
					<div class="products grid products_carousel">
						{foreach $browsed_products as $b_product}
							<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
								{include file='product_block.tpl' product=$b_product}
							</div>
						{/foreach}
					</div>
				</div>
			{/if}
			
		
	{* Описание категории *}
			 {if $current_page_num==1 && ($page->body || $category->description || $brand->description_in_categories[$category->id] || $brand->description )}
				<div class="separator"></div>
					<div class="spoiler seo_text user_content">
						{$page->body}
						{if $brand && $category && $brand->description_in_categories[$category->id]}
							{$brand->description_in_categories[$category->id]}
						{elseif $brand && $brand->description}
							{$brand->description}
						{elseif $category->description}
							{$category->description}
						{/if}
					</div>
				</div>
			{/if} 
		
		{*Шаблон вывода пустой категории*}
		{else}
			<div class="page_empty flex flex_align_center flex_content_center">
				<div class="shadow_box">
					<div class="page_empty_text shadow_box_item text_center">
						<div class="text_2 text_bold">Товары не найдены</div>
						<br>
						<a href="/" class="button button_block">Вернуться на главную</a>
					</div>
				</div>
			</div>
		{/if}
		
	</div>
</main>

{/strip}