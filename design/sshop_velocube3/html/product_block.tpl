{strip}

{* Создаем переменные сегодняшней даты и даты создания + 10дней *}
{$date_created = date('Y.m.d', strtotime($product->date)+60*60*24*10)}
{$date_now = $smarty.now|date_format:"%Y.%m.%d"}

{* Добавляем проверку для таймера обратного отсчета.
   Если для како-то группы товаров нужно вывести таймер, в инклуде добавляем sale_timer="1"
*}
{if $sale_timer != 1}{assign 'sale_timer' 0}{/if}
		
{* Создаем массивы для цен *}
{assign 'final_price_array' []}
{assign 'old_price_array' []}

{if $user_group->discount}
    {foreach $product->variants as $k=>$v}
        {if $v->price>1}
    		{if $v->compare_price}
    			{$old_price_array[$k] = $v->compare_price|convert}
    		{else}
                {$old_price_array[$k] = $v->price|convert}
    		{/if}
    		
            {if $v->coupon_discount_price}
                {$final_price_array[$k] = ($v->price - ($v->price*$user_group->discount/100) - $v->coupon_discount_price)|convert}
            {else}
                {$final_price_array[$k] = ($v->price - ($v->price*$user_group->discount/100))|convert}
            {/if}
		{/if}
    {/foreach}
{else}
    {$stock_filtering=0}
    {foreach $product->variants as $k=>$v}
        {if $v->stock>0}
            {$stock_filtering=1}
        {/if}
    {/foreach}
    {foreach $product->variants as $k=>$v}
        {if $stock_filtering}
            {if $v->price>1 && $v->stock>0}
            	{if $v->compare_price}
        			{$old_price_array[$k] = $v->compare_price|convert}
                {elseif $v->coupon_discount_price}
                    {$old_price_array[$k] = $v->price|convert}
        		{/if}
                
                {if $v->coupon_discount_price}
                    {$final_price_array[$k] = ($v->price - $v->coupon_discount_price)|convert}
                {else}
                    {$final_price_array[$k] = $v->price|convert}
                {/if}
      		{/if}
        {else}
            {if $v->price>1}
            	{if $v->compare_price}
        			{$old_price_array[$k] = $v->compare_price|convert}
                {elseif $v->coupon_discount_price}
                    {$old_price_array[$k] = $v->price|convert}
        		{/if}
                
                {if $v->coupon_discount_price}
                    {$final_price_array[$k] = ($v->price - $v->coupon_discount_price)|convert}
                {else}
                    {$final_price_array[$k] = $v->price|convert}
                {/if}
      		{/if}
        {/if}
    {/foreach}
{/if}

{* Сортируем цены от минимума к максимуму *}
{assign 'final_price_sort' asort($final_price_array)}
{assign 'old_price_sort' asort($old_price_array)}

{* Забираем первые элементы массивов, они же минимальные цены среди вариантов, следующим образом
   reset($final_price_array) - для итоговой цены
   reset($old_price_array) - для старой цены 
   reset($sale_percent_array) - для разницы
*}

<div class="products_inner object_list_inner shadow_box animated">

	<div class="product_image shadow_box_item relative text_center" data-product_image="{$product->image->filename|resize:200:200}">
	
		{if $smarty.session.admin}
			<div class="view_count absolute text_5 text_white">
				<i class="icon">{include file="svg.tpl" svgId="icon_eye" width="20px" height="20px"}</i>
				<span class="icon_text no_margin">&nbsp;{$product->variants[0]->viewed}</span>
			</div>
		{/if}
	
		{if $product->ttoday && $sale_timer == 1}
			<span class="product_sticker product_sticker_today absolute uppercase">
				<span class="product_sticker_text"><span class="text_medium text_5">Товар</span><br><span class="text_2 text_bold">дня</span></span>
			</span>	
		{elseif $product->featured && $smarty.get.module != 'MainView'}
			<span class="product_sticker product_sticker_hit absolute uppercase">
				<span class="product_sticker_text"><span class="text_1 text_bold">Хит</span><br><span class="text_medium text_6">продаж</span></span>
			</span>
		{elseif ( $date_now <= $date_created ) && $smarty.get.module != 'MainView'}
			<span class="product_sticker product_sticker_new absolute uppercase">
				<span class="product_sticker_text"><span class="text_1 text_bold">New</span><br><span class="text_medium text_6">новинка</span></span>
			</span>	
		{elseif reset($sale_percent_array) > 0}
			<span class="product_sticker product_sticker_today absolute uppercase">
				<span class="product_sticker_text">
					<span class="text_medium text_6">Скидка</span><br>
					<span class="text_2 text_bold">
						-{reset($sale_percent_array)}%
					</span>
				</span>
			</span>	
		{/if}
			
		{if $sale_timer == 1}
			<div class="product_timer timer absolute text_center">
				<span class="timer_title">осталось</span>
				<span class="number days timer_item"></span>
				<span class="number hours timer_item"></span>
				<span class="number minutes timer_item"></span>
			</div>
		{/if}
		
		<a class="products_image_link relative" href="products/{$product->url}">
			{if $product->image}
				<img src="{$product->image->filename|resize:200:200}" alt="{$product->fullname|escape}" title=""/>
			{else}
				<img src="{'nophoto.png'|resize:200:200}" alt="" title=""/>
			{/if}
			{*При ховере показываем другие изображения товара*}
			{if $product->images|count>1}
				{if $product->images|count > 4}
                {*print_r($product)*}
					<span class="products_image_gallery_count absolute animated" {count($product->images)}>
						Еще {math equation="x - 4" x=count($product->images)} фото
					</span>
				{/if}
				<span class="products_image_gallery absolute animated">
					{foreach $product->images as $i=>$image}
						<span class="products_image_gallery_item{if $image@first} hover{/if}" data-src="{$image->filename|resize:200:200}"></span>
						{if $image@iteration == 4}{break}{/if}
					{/foreach}
				</span>	
			{/if}
		</a>
	</div>
	
	<div class="products_info shadow_box_item">
		<a class="product_name text_black" href="products/{$product->url}" data-product="{$product->id}" data-product_name="{$product->fullname|escape}">{$product->fullname|escape}</a>

		{*Рейтинг*}
		<div class="rating inline_block_middle">
			<span class="rating_starOff">	
				<span class="rating_starOn" style="width:{if $product->rating > 0}{$product->rating*85/5|string_format:'%.0f'}{else}0{/if}px"></span>
			</span>
		</div>

		<span class="compare_link inline_block_middle text_5 text_gray2 text_right">
			{if $compare->ids && in_array($product->id, $compare->ids)}
				<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
				<span class="compare_link_text inline_block_middle">в сравнении</span>
			{else}
				<a href="/compare?id={$product->id}" class="compare_add" data-id="{$product->id}" data-key="compare" data-informer="1" data-result-text="в сравнении">
					<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
					<span class="compare_link_text inline_block_middle">сравнить</span>
				</a>
			{/if}
		</span>
	
	</div>
	

	<form class="products_price{if $product->variants|count == 1} variants{/if} flex flex_content_justify flex_align_center shadow_box_item" action="/cart">
	
		<div class="price_container">
			{if reset($old_price_array) > 0}<div class="price old_price text_gray2" old="{$old_price_array[reset(array_keys($final_price_array))]}">{$old_price_array[reset(array_keys($final_price_array))]}</div>{/if}
			<div class="price final_price text_gray text_2">
				<span class="value">{reset($final_price_array)}</span>&nbsp;
				<span class="currency">{$currency->sign|escape}</span>
			</div>
		</div>
		
		{*если у товара один вариант, даем пользователю купить его сразу*}
		{if $product->variants|count == 1}
			{/strip}
			<input id="variant_{$product->variant->id}" class="hidden" name="variant" value="{$product->variant->id}" type="radio" checked
				
				{if $product->variant->compare_price}
					data-old_price="{reset($old_price_array)}"
				{elseif $user->discount}
					data-old_price="{reset($final_price_array)}"
				{/if} 
				
				data-final_price="{reset($final_price_array)}"
                
                {if $product->variant->compare_price}
					data-discount_price_shop="{($product->variant->compare_price - $product->variant->price)|convert}"
				{/if}
				
				{if $user_group->discount}
					data-discount_price_user="{($product->variant->price * $user_group->discount/100)|convert}"
				{/if}  
				
				{if $product->variant->coupon_discount_price}
					data-coupon_id="{$product->variant->coupon_id}"
					data-discount_price_coupon="{$product->variant->coupon_discount_price|convert}"
				{/if}
			/>
			{strip}
			<button class="button" type="submit" onclick="yaCounter21197344.reachGoal('addcart');">{include file="svg.tpl" svgId="icon_cart"}</button>
			
		{*иначе при клике подгржаем детали товара для выбора варианта*}
		{else}
			<span class="product_ajax_link button" data-prod_id="{$product->id}" onclick="yaCounter21197344.reachGoal('quick_viev');">{include file="svg.tpl" svgId="icon_cart"}</span>
		{/if}
		
	</form>

	
	{* В категории и поиске выводим первые 12 свойств товара *}
	{if count($product->options) && ( $products || $keyword ) && !$browsed_products}
	<div class="products_detail shadow_box_item text_5 animated hidden_xs">
        <ul class="products_feature">
        {foreach $product->options as $opt}
            {if $opt@iteration < 13}
            <li class="products_feature_item">
				<span class="products_feature_name text_gray2">{$opt->name}:</span>&nbsp;
				<span class="products_feature_value text_black">{$opt->value}</span>
			</li>
            {/if}
        {/foreach}
        </ul>
	</div>
	{/if}

</div>
{/strip}