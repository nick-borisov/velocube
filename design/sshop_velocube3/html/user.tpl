{strip}
{* Шаблон страницы зарегистрированного пользователя *}

<main class="main section">

	<div class="container">
	
	{include file="path.tpl"}
	
	<h1 class="page_title">Личный кабинет</h1>

	<div class="row">
	
		{if $orders}
			<div class="orders_history col_lg_7 offset_lg_1">
				<h2 class="form_title">История заказов</h2>
				<table class="table table_colored text_5">
					<tr class="hidden_xs">
						<td>Дата</td>
						<td>Заказ</td>
						<td>Статус</td>
						<td>Ваш менеджер</td>
						<td></td>
					</tr>
					{foreach name=orders item=order from=$orders}
						<tr>
							<td class="text_gray"><span class="visible_xs_inline">Дата:&nbsp;</span>{$order->date|date}</td> 
							<td><span class="visible_xs_inline">Заказ:&nbsp;</span>№{$order->id}</td>
							<td>
								<span class="visible_xs_inline">Статус:&nbsp;</span>
								{if $order->paid == 1}		<span class="text_green">оплачен</span>
								{elseif $order->status == 0}<span class="text_blue">ждет обработки</span>
								{elseif $order->status == 1}<span class="text_blue">в обработке</span>
								{elseif $order->status == 2}<span class="text_green">выполнен</span>
								{elseif $order->status == 4}<span class="text_blue2">отменен</span>
								{elseif $order->status == 5}<span class="text_red">проблемный</span>
								{elseif $order->status == 3}<span class="text_red">удален</span>{/if}	
							</td>
							<td class="hidden_xs">{$order->manager}</td>
							<td><a class="button button_small button_light" href='order/{$order->url}'>Подробнее</a></td>
						</tr>
					{/foreach}
				</table>
			</div>
		{/if}

		<form class="account_form {if $orders}col_lg_4{else}form_single{/if}" method="post">
			<div class="shadow_box">
				<div class="account_form_inner shadow_box_item">
					<h2 class="form_title">Регистрационные данные</h2>
					{if $error}
					<div class="notice notice_alert">
						{if $error == 'empty_name'}Введите имя
						{elseif $error == 'empty_email'}Введите email
						{elseif $error == 'empty_password'}Введите пароль
						{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
						{else}{$error}{/if}
					</div>
					{/if}
					<div class="form_group">
						<label for="account_name" class="form_label">Имя:</label>
						<input id="account_name" class="form_input" data-format=".+" data-notice="Введите имя" value="{$name|escape}" name="name" maxlength="255" type="text" />
					</div>
					<div class="form_group">
						<label for="account_surname" class="form_label">Фамилия:</label>
						<input id="account_surname" class="form_input" type="text" name="surname" value="{$surname|escape}" maxlength="255"/>	
					</div>
					<div class="form_group">
						<label for="account_email" class="form_label">Адрес электронной почты:</label>
						<input id="account_email" class="form_input" value="{$email|escape}" name="email" type="email" maxlength="255" data-format="email" data-notice="Введите email"/>
					</div>
					<div class="change_password form_group">
						<label for="account_password" class="form_label">Изменить пароль:</label>
						<input id="account_password" class="form_input" value="" name="password" type="password" placeholder="Введите новый пароль"/>
					</div>
					<input type="submit" class="button button_large button_block" value="Сохранить">
				</div>
			</div>
		</form>

	</div>

	</div>
</main>

{/strip}