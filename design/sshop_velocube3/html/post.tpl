{strip}
{* Страница отдельной записи блога *}

{* Канонический адрес страницы *}
{$canonical="/blog/{$post->url}" scope=parent}

<article class="main section">
	<div class="container">
	
		{* Хлебные крошки *}
		{include file="path.tpl"}

		<h1 class="page_title" data-post="{$post->id}">{$post->name|escape}</h1>
		<div class="user_content">
			{$post->text}
		</div>
		
		<div class="post_date text_gray2">Дата публикации: <time datetime="{$post->date}">{$post->date|date}</time></div>
		
		<div class="separator hidden_xs"></div>

		<div class="post_navigation flex row hidden_xs">
			{if $prev_post}
				<div class="col_sm_6 flex_item text_nowrap">
					<a class="text_pink text_5" href="blog/{$prev_post->url}">
						<i class="inline_block">{include file="svg.tpl" svgId="icon_arrow_left" width="12px" height="12px"}</i>&nbsp;
						<span class="link_inverse inline_block text_wrap">{$prev_post->name}</span>
					</a>
				</div>
			{/if}
			{if $next_post}
				<div class="col_sm_6 text_right text_nowrap">
					<a class="text_pink text_5" href="blog/{$next_post->url}">
						<span class="link_inverse inline_block text_wrap">{$next_post->name}</span>&nbsp;
						<i class="inline_block">{include file="svg.tpl" svgId="icon_arrow_right" width="12px" height="12px"}</i>
					</a>
				</div>
			{/if}
		</div>
	
	</div>
</article>
{/strip}