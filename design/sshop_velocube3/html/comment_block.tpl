{strip}


	{function name=comments_tree}	
	{if $comments}
	
		{if $smarty.get.module == 'ProductView'}
		<div class="rating_detail_container row flex">
			<div class="rating_detail_col flex_item col_xs_12 col_md_6">
				<div class="rating_detail_title">
					<span class="text_black">Общий рейтинг:</span>&nbsp;
                    {assign 'word_mark' ""}
                    {if $product->rating > 1 && $product->rating <= 2 }{$word_mark='Неудовлетворительно'}
                    {elseif $product->rating > 2 && $product->rating <= 3}{$word_mark='Удовлетворительно'}
                    {elseif $product->rating > 3 && $product->rating <= 4}{$word_mark='Хорошо'}
                    {elseif $product->rating > 4}{$word_mark='Отлично'}
                    {/if}
					<span class="text_bold uppercase {if $product->rating > 1 && $product->rating <= 2 }text_red{elseif $product->rating > 2 && $product->rating <= 3}text_pink{elseif $product->rating > 3}text_yellow{else}text_gray{/if}">{$word_mark}</span>
					<br>
					<span class="rating_starOff">
						<span class="rating_starOn" style="width:{$product->rating*85/5|string_format:'%.0f'}px"></span>
					</span>&nbsp;
					<span class="inline_block_middle text_2 text_bold text_black">({$product->votes|string_format:"%.0f"})</span>
				</div>
				
				<ul class="rating_detail_list">
					<li class="rating_detail_item flex flex_align_center">
						<div class="rating_detail_item_title text_black">
							<span class="text_3 text_bold">5</span>&nbsp;
							<span class="text_6">звезд</span>
						</div>
						<div class="rating_detail_item_line rating_5 relative">
							<div class="rating_detail_item_scale" style="width: {$comments_rate_count[5]*100/$product->votes|string_format:'%.0f'}%"></div>
						</div>
						<div class="rating_detail_item_count text_bold text_blue2">({$comments_rate_count[5]|default:'0'})</div>
					</li>
					<li class="rating_detail_item flex flex_align_center">
						<div class="rating_detail_item_title text_black">
							<span class="text_3 text_bold">4</span>&nbsp;
							<span class="text_6">звезды</span>
						</div>
						<div class="rating_detail_item_line rating_4 relative">
							<div class="rating_detail_item_scale" style="width: {$comments_rate_count[4]*100/$product->votes|string_format:'%.0f'}%"></div>
						</div>
						<div class="rating_detail_item_count text_bold text_blue2">({$comments_rate_count[4]|default:'0'})</div>
					</li>
					<li class="rating_detail_item flex flex_align_center">
						<div class="rating_detail_item_title text_black">
							<span class="text_3 text_bold">3</span>&nbsp;
							<span class="text_6">звезды</span>
						</div>
						<div class="rating_detail_item_line rating_3 relative">
							<div class="rating_detail_item_scale" style="width: {$comments_rate_count[3]*100/$product->votes|string_format:'%.0f'}%"></div>
						</div>
						<div class="rating_detail_item_count text_bold text_blue2">({$comments_rate_count[3]|default:'0'})</div>
					</li>
					<li class="rating_detail_item flex flex_align_center">
						<div class="rating_detail_item_title text_black">
							<span class="text_3 text_bold">2</span>&nbsp;
							<span class="text_6">звезды</span>
						</div>
						<div class="rating_detail_item_line rating_2 relative">
							<div class="rating_detail_item_scale" style="width: {$comments_rate_count[2]*100/$product->votes|string_format:'%.0f'}%"></div>
						</div>
						<div class="rating_detail_item_count text_bold text_blue2">({$comments_rate_count[2]|default:'0'})</div>
					</li>
					<li class="rating_detail_item flex flex_align_center">
						<div class="rating_detail_item_title text_black">
							<span class="text_3 text_bold">1</span>&nbsp;
							<span class="text_6">звезда</span>
						</div>
						<div class="rating_detail_item_line rating_1 relative">
							<div class="rating_detail_item_scale" style="width: {$comments_rate_count[1]*100/$product->votes|string_format:'%.0f'}%"></div>
						</div>
						<div class="rating_detail_item_count text_bold text_blue2">({$comments_rate_count[1]|default:'0'})</div>
					</li>
				</ul>
			</div>
			<div class="rating_detail_col col_md_6 flex_item hidden_xs hidden_sm">
				<div class="rating_detail_add text_black text_center flex_col flex_content_center">
					Оставьте свой отзыв о товаре:
					<div class="rating_detail_add_title text_bold text_3">{$product->type_prefix|escape} {$product->name|escape}</div>
					<div><a href="#comment_form" class="button button_large button_light anchor_link">оставить свой отзыв</a></div>
				</div>
			</div>
		</div>
		{/if}
		
		<ul class="comment_list">    
		{foreach $comments as $comment}
            {if $comment->approved}        
  {*<a name="comment_{$comment->id}"></a>*}
			<li class="comment_list_item">	
				<div class="comment_list_header row">
					<div class="comment_list_name col_sm_9 col_md_10">
						<span class="text_black text_bold">{if $comment->name}{$comment->name|escape}{else}Анонимный пользователь{/if}</span>
						{*}
						<span class="comment_list_checking text_label text_label_alert">
							<span class="text_6">проверяется</span>
						</span>
						{/if*}
					</div>
					<div class="comment_list_rating col_sm_3 col_md_2">
						<span class="rating_starOff">
							{if $smarty.get.module == 'ProductView'}
								<span class="rating_starOn" style="width:{$comment->rating*85/5|string_format:'%.0f'}px"></span>
							{else}
								<span class="rating_starOn" style="width:{$comment->rate*85/5|string_format:'%.0f'}px"></span>
							{/if}
						</span>
					</div>
					
				</div>
				<div class="comment_list_body row"> 
					<div class="col_sm_3 col_md_2">
						<div class="inline_block text_center">
							<span class="comment_list_photo hidden_xs"></span>
							<time class="text_gray2 text_6" datetime="{$comment->date}">{$comment->date|date}</time>
						</div>
					</div>
					<div class="col_sm_9 col_md_10">
						{if $comment->text}
							<div class="comment_list_text">
								<div class="user_content text_5">{$comment->text|escape|nl2br}</div>
							</div>
						{/if}
						{if $comment->dost}
							<div class="comment_list_text">
								<div class="text_bold text_green">Достоинства:</div>
								<div class="user_content text_5">{$comment->dost|escape|nl2br}</div>
							</div>
						{/if}
					
						{if $comment->nedost}
							<div class="comment_list_text">
								<div class="text_bold text_red">Недостатки:</div>
								<div class="user_content text_5">{$comment->nedost|escape|nl2br}</div>
							</div>
						{/if}
					</div>
					
				</div>
				{* comments_tree comments = $comment->subcomments *}
			</li>
            {/if}            
		{/foreach}
		</ul>
	{else}
		<div class="title text_2 text_blue2">
			Ваш отзыв будет первым
		</div>
	{/if}
	{/function}
	{comments_tree comments = $comments}

	<form id="comment_form" class="comment_form" method="post">

		{if $error}
		<div class="notice notice_alert">
			<div class="notice_title">Внимание!</div>
			{if $error=='empty_name'}
				Введите имя
			{elseif $error=='empty_comment'}
				Введите комментарий
            {elseif $error=='captcha'}
				Отметьте галочку "я не робот"
			{/if}
		</div>
		{/if}
		
		<input type="hidden" name="id" value="{$product->id}" />
		<input id="parent" type="hidden" name="parent_id" value="0"/>
		<input id="admin" type="hidden" name="admin" value="{if $smarty.session.admin}1{else}0{/if}"/>
		
		<div class="row">
		
			<div class="col_md_3 hidden_xs hidden_sm text_black">
			
				<div class="text_3 text_bold">Написать отзыв</div>
				<br>
				Внимание! Публикация производится только после предварительной модерации
				{*
				<br><br>
				<a href="#" class="link text_blue">Ознакомится с правилами модерации</a>
				<br><br>
				<a href="#" class="link text_blue">Как написать полезный отзыв</a>
				*}
			</div>
		
			<div class="col_md_8 offset_md_1">
		
				<div class="form_group">
					<input class="form_input" type="text" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя" placeholder="Ваше имя"/>
				</div>
				
				{if $smarty.get.module == 'ProductView' || $smarty.get.module == 'CommentsView'}
				<div id="product_{$product->id}" class="add_rating rating_large form_group">
					<input id="add_rating" type="hidden" name="rating" value="5" />
					
					<div class="row">
						<div class="col_xs_4 text_3 text_black text_bold">
							{if $smarty.get.module == 'ProductView'}
								Оцените товар
							{else}
								Оцените магазин
							{/if}
						</div>
						<div class="col_xs_8 text_right">
							<span class="rating_starOff">
								<span class="rating_starOn"></span>
							</span>
						</div>
					</div>
				</div>
				{/if}
				
				<div class="form_group">
					<textarea class="form_input" name="text" rows="4" placeholder="Комментарий" data-format=".+" data-notice="Введите комментарий">{$comment_text}</textarea>
				</div>
				
				<div class="row">
				
					{if $smarty.get.module == 'ProductView' || $smarty.get.module == 'CommentsView'}
						<div id="comment_advanced" style="display: none;">
							<div class="form_group col_sm_6">
								<textarea class="form_input" name="dost" rows="4" placeholder="Достоинства">{$comment_dost}</textarea>
							</div>
							<div class="form_group col_sm_6">
								<textarea class="form_input" name="nedost" rows="4" placeholder="Недостатки">{$comment_nedost}</textarea>
							</div>
						</div>
					{/if}
					
					<div class="form_group col_xs_12 text_center">
						<a href="#comment_advanced" class="link pseudo_link text_pink text_pink trigger trigger_anchor" data-trigger_default_text="Написать достоинства и недостатки" data-trigger_active_text="Скрыть достоинства и недостатки" data-trigger_action="slide">Написать достоинства и недостатки</a>
					</div>
                    
					<div class="form_group col_xs_12 text_center">
						<div id="recaptcha_comment" class="inline_block"></div>        
						<input id="g-recaptcha_comment" class="recaptcha_callback" type="text" name="g-recaptcha-response" value="" data-format=".+" data-notice="Отметьте галочку"/>
					</div>
					
					<div class="col_xs_12 col_lg_6 offset_lg_3 text_center">
						<input class="button button_block button_large button_light" type="submit" name="comment" value="Отправить отзыв"/>
					</div>
					
				</div>
			
			</div>
		
		</div>

		

	</form>
	
	
{/strip}