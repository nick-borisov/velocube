{* Информера корзины (отдаётся аяксом) *}
<span class="header_informer_title text_5 text_center text_gray hidden_xs hidden_sm">Моя корзина</span>
<span class="header_informer_button button button_block trigger close_outside">
	<span class="button_item icon">
		{include file="svg.tpl" svgId="icon_cart"}
	</span>
	<span class="button_item icon_text">
		{$cart->total_products}
	</span>
</span>

<div class="header_informer_content trigger_content absolute">

	{foreach $cart->purchases as $purchase}
		<div class="header_informer_item">
			<div class="header_informer_col image text_center">
				<a href="products/{$purchase->product->url}">
					{$image = $purchase->product->images|first}
					{if $image}
						<img src="{$image->filename|resize:70:70}" alt="{$purchase->product->name|escape}">
					{else}
						<img src="{'nophoto.png'|resize:70:70}" alt="{$purchase->product->name|escape}"/>
					{/if}
				</a>
			</div>
			<div class="header_informer_col name text_bold">
				<a href="products/{$purchase->product->url}">{$purchase->product->fullname|escape}</a>
			</div>
   
			<div class="header_informer_col remove text_right">
				<span class="remove_link icon icon_link text_gray3" onclick="remove_cart_informer('{$purchase->variant->id}')" title="Удалить">
					{include file="svg.tpl" svgId="icon_close" width="20px" height="20px"}
				</span>
			</div>
		</div>
	{/foreach}
	
	<div class="header_informer_total">
		{if $cart->total_products>0}
			<div class="header_informer_price header_informer_col col_xs_6">
				<div class="text_6 text_gray2 uppercase">итого к оплате</div>
				<div class="text_1 text_black"><span class="text_bold">{$cart->total_price|convert}</span> <span class="currency">{$currency->sign|escape}</span></div>
			</div>
			<div class="header_informer_col col_xs_6 text_right">
				<a href="./cart" class="button">Оформить</a>
			</div>
		{else}
			<div class="header_informer_col text_center">
				<span class="text_bold uppercase text_gray2">В корзине пусто</span>
			</div>
		{/if}
	</div>
</div>