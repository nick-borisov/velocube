{strip}
{*Товары*}
<ul id="products" class="products object_list row animated">
	{foreach $products as $product}
		<li class="product object_list_item col_xs_12 col_sm_6 col_md_4 col_lg_3">
			{include file='product_block.tpl'}
		</li>
	{/foreach}
</ul>
{if $is_more}
	<button id="products_load_more" class="products_load_more button" type="button" data-page="{$next_page}">
		<span class="button_item icon">{include file="svg.tpl" svgId="icon_arrow_circle"}</span>
		<span class="button_item icon_text">Показать еще</span>
	</button>
{/if}	
{include file="pagination.tpl"}
{/strip}