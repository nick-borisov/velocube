{strip}
{* Шаблон текстовой страницы *}

{* Канонический адрес страницы *}
{$canonical="/{$page->url}" scope=parent}

<main class="main section">
	<div class="container">
	
		{* Хлебные крошки *}
		{include file="path.tpl"}

		<h1 class="page_title" data-page="{$page->id}">{$page->header|escape}</h1>
		
		{*Страница с брендами*}
		{if $page->id == 11}	
		{get_brands var=all_brands}
								
			{if $all_brands}
	
				<div class="brand_list object_list row">
					{foreach $all_brands as $b}	
						<div class="brand_list_item object_list_item col_xs_6 col_md_4 col_lg_3">
							<div class="shadow_box object_list_inner animated">
								<a class="brand_list_link shadow_box_item" data-brand="{$b->id}" href="brands/{$b->url}">
									<span class="brand_list_image text_center">{if $b->image}<img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}">{/if}</span>
									<span class="text_5 text_gray">{$b->name|escape}</span>	
								</a>
							</div>
						</div>
					{/foreach}
				</div>
			{/if}
		
		{*Страница с хитами продаж*}
		{elseif $page->id == 12}
		{get_featured_products var=featured_products}
		
			{if $featured_products}
				<div id="products" class="products grid row">
					{foreach $featured_products as $f_product}
						<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
							{include file='product_block.tpl' product=$f_product}
						</div>
					{/foreach}
				</div>
			{/if}
		
		{else}

			<div class="user_content">{$page->body}</div>
		
		{/if}
		
	</div>
</main>
{/strip}