// Аяксовая корзина
$(document).on('submit', 'form.variants', function(e) {
	e.preventDefault();
	var button = $(this).find('.button[type="submit"]'),
		variant = $(this).find('input[name=variant]:checked'),
		variantName = variant.closest('.variant').find('.variant_name'),
		amount = 1;
	if ( $(this).find('select[name=variant]').length ) {
		variant = $(this).find('select');
	}
	if( $(this).find('input[name=amount]').length ) {
		amount = $(this).find('input[name=amount]').val();
	}
	$.ajax({
		url: "ajax/cart.php",
		data: {variant: variant.val(), amount: amount, auto_coupon:variant.data('coupon_id')},
		dataType: 'json',
		success: function(data){
			$('#cart_informer').html(data);
			if ( button.attr('data-result-text') ) {
				button.val(button.attr('data-result-text'));
			}
		}
	});
	$('#informer_notice').find('.informer_notice_title').html('Товар добавлен в корзину');
	$('#informer_notice').find('.informer_notice_name').html( $(this).closest('.product').find('.product_name').data('product_name') );
	$('#informer_notice').find('.informer_notice_image img').attr('src', $(this).closest('.product').find('.product_image').data('product_image') );
	if( typeof(variant.data('v_name')) != 'undefined' ) {
		$('#informer_notice').find('.informer_notice_variant').show().html( variantName.html() );
	} else {
		$('#informer_notice').find('.informer_notice_variant').hide();
	}
	if( typeof(variant.data('old_price')) != 'undefined' ) {
		$('#informer_notice').find('.old_price').show().html( variant.data('old_price') );
	} else {
		$('#informer_notice').find('.old_price').hide();
	}
	$('#informer_notice').find('.final_price .value').html( variant.data('final_price') );
	$('#informer_notice').find('.informer_notice_submit').attr('href', '/cart').html('Перейти в корзину');
	$.fancybox({
		href: '#informer_notice',
		padding: 0
	});		
	return false;
});
 
/* Добавление в сравнение */
$(document).on('click', '.compare_add', function(e) {
	e.preventDefault();
    var href 	 = $(this),
		key      = $(this).data('key'),
		id       = $(this).data('id'),
		informer = $(this).data('informer'),
		variant  = $(this).closest('.product').find('input[name=variant]:checked');
	$.ajax({
		url: "ajax/product_to_session.php",
		data: {key: key, id: id, i:informer},
		dataType: 'json',
		success: function(data) {
            $('#compare_informer').html(data.informer);
			if( href.data('result-text') ) {
				href.parent().html(href.data('result-text'));
			}
		}
	});
	$('#informer_notice').find('.informer_notice_title').html('Товар добавлен в сравнение');
	$('#informer_notice').find('.informer_notice_name').html( $(this).closest('.product').find('.product_name').data('product_name') );
	$('#informer_notice').find('.informer_notice_image img').attr('src', $(this).closest('.product').find('.product_image').data('product_image') );
	if( variant.length && typeof( variant.data('old_price') ) != 'undefined' ) {
		$('#informer_notice').find('.old_price').show().html( variant.data('old_price') );
	} else {
		$('#informer_notice').find('.old_price').hide();
	}
	if( variant.length && typeof( variant.data('final_price') ) != 'undefined' ) {
		$('#informer_notice').find('.final_price .value').html( variant.data('final_price') );
	} else {
		$('#informer_notice').find('.final_price .value').html( $(this).closest('.product').find('.final_price .value').html() );
	}
	$('#informer_notice').find('.informer_notice_submit').attr('href', '/compare').html('Перейти в сравнение');
	$.fancybox({
		href: '#informer_notice',
		padding: 0
	});
	return false;
});
/* /Добавление в сравнение */


/* Удаление на странице сравнения */
$(document).on('click', '.compare_remove', function(e) {
	e.preventDefault();
	var href     = $(this),
		key      = $(this).data('key'),
		id       = $(this).data('id'),
		informer = $(this).data('informer');
	$.ajax({
		url: "ajax/remove_product.php",
		data: {key: key, id: id, i:informer},
		dataType: 'json',
		success: function(data)        {
            $('#compare_informer').html(data.informer);
			if(href.data('result-text')) {
				href.parent().html(href.data('result-text'));
			}
			location.reload();
		}
	});
	return false;
});
/* /Удаление на странице сравнения */

/* Ajax превью товара */
$(document).on('click', '.product_ajax_link', function(){
	var prod_id = $(this).data('prod_id');
	$.ajax({
		url: 'ajax/quick.php',
		data: {id: prod_id},
		dataType: 'json',
		cache: false,
		success: function(data) {
			$.fancybox({
				content: data,
				padding: 0
			});
			if($('#product_gallery_modal').length) {
				$('#product_gallery_modal').slick({
					dots: true,
					arrows: false,
					infinite: false
				});
			}
		}
	})
});

/* oneclick buy */
$(document).on('click', '#oneclick_buy', function() {
	var variant = $('#product form.variants').find('input[name=variant]:checked').val();
	if( !$('#oneclick_name').val() || !$('#oneclick_phone').val() ) { 
		$('#oneclick_form .notice').slideDown(100).delay(5000).slideUp(300);
		return false;
	}
	$.ajax({
		type: "post",
		url: "/ajax/oneclick.php",
		data: {amount: 1, variant: variant, name: $('#oneclick_name').val() , phone: $('#oneclick_phone').val() , comment: $('#oneclick_comment').val() },
		dataType: 'json',
		success: function(data) {
			$.fancybox({
				padding: 0,
				'href': '#success_notice'
			});
		}
	});
	return false;   
});

/* Удаление товаров из карт ифнормера */
function remove_cart_informer(remove_id){
	$.ajax({
		url: "ajax/cart_update.php",
		data: { 'remove_id':remove_id},
		success: function(data){
			if(data){
				$('#cart_informer').html(data.informer);
                $('#cart_informer').addClass('active');
                $('#cart_informer .header_informer_button').addClass('active');
                $('#cart_informer .header_informer_content').show();
			}
		}
	});
}