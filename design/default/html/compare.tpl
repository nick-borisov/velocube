{*
  Template name: Сравнение товаров

*}


  <!-- Хлебные крошки /-->
  <div id="path">
    <a href="./">Главная</a>
    → Сравнение товаров           
  </div>
<!-- Хлебные крошки #End /-->

  <h1>Сравнение товаров</h1>
{if $products}
<div id="product_params">
<table>
<tr>
<td> 

</td>
{foreach from=$products item=product}
<td class="product">
<!-- Описание товара /-->
		<!-- Фото товара -->
		{if $product->image}
		<div class="image">
			<a href="products/{$product->url}"><img src="{$product->image->filename|resize:100:100}" alt="{$product->name|escape}"/></a>
		</div>
		{/if}
		<!-- Фото товара (The End) -->

		<!-- Название товара -->
		<h3><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h3>
		<!-- Название товара (The End) -->
                    <div class="testRater_{$product->id} clr" id="rating_{$product->id}" class="stat">
                      <div class="statVal">
                        <span class="rater">
                          <span class="rater-starsOff" style="width:80px;">
                            <span class="rater-starsOn" style="width:{$product->rating*80/5|string_format:"%.0f"}px"></span>
                          </span><br>
                          <span class="rater-rating">{$product->rating|string_format:"%.1f"}</span>
                          &#160;(голосов <span class="rater-rateCount">{$product->votes|string_format:"%.0f"}</span>)
                        </span>
                      </div>		
  <br>
  <a href='compare/remove/{$product->url}'>Убрать</a>

</td>
{/foreach} 
</tr>
{foreach from=$compare_features name=features item=property}
<tr class="bord {if $smarty.foreach.features.iteration%2 == 0}gray{/if}">
<td class="feat"> 
		<b>{$property->name}: </b>
</td>
{foreach from=$products item=product}

<td> 
	{foreach $product->features as $f}
{if $f->name == $property->name}
		<span>{$f->value}</span>
{/if}
	
	{/foreach}
</td>
{/foreach} 
</tr>
{/foreach} 
<tr>
<td class="feat"> 
<b>Описание: </b>		
</td>
{foreach from=$products item=product}
<td class="feat"> 


{$product->annotation}

</td>
{/foreach} 
</tr>
<tr>
<td class="feat"> 
		
</td>
{foreach from=$products item=product}
<td> 
		{if $product->variants|count > 0}
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					<input id="featured_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $v@first}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
				</td>
				<td>
					{if $v->name}<label class="variant_name" for="featured_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>
			{/foreach}
			</table>
			<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>
		</form>
		<!-- Выбор варианта товара (The End) -->
		{else}
			Нет в наличии
		{/if}
</td>
{/foreach}
</tr>
</table>
</div>





{else}
Нет товаров для сравнения
{/if}
<!-- Описание товара #End/-->
<!-- Товар  #End /-->


