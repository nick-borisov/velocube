{if $cart->total_products>0}
	<b>В <a href="#data"  id="inline" >корзине</a>
	{$cart->total_products} {$cart->total_products|plural:'товар':'товаров':'товара'}
	на {$cart->total_price|convert} {$currency->sign|escape}</b>
{else}
	<b>Корзина пуста</b>
{/if}
<div style="display:none"><div id="data">
{if $cart->total_products>0}

          <div class="heading">
            <h2>Корзина</h2>
</div>
          <div class="content">
          
            <table class="cart">
              <tbody>
     {foreach from=$cart->purchases item=purchase}
                <tr>
                  <td class="image">
      {$image = $purchase->product->images|first}
      {if $image}
      <a href="products/{$purchase->product->url}"><img alt="Spicylicious" src="{$image->filename|resize:40:40}"/></a>
      {/if}
      </td>
                  <td class="name"><a href="products/{$purchase->product->url}">{$purchase->product->name|escape}</a>
                    <div>{$purchase->variant->name|escape}</div></td>
	<td class="amount">
		<select name="amounts[{$purchase->variant->id}]" onchange="update_cart('{$purchase->variant->id}',$(this).val());" class="amounts" id="{$purchase->variant->id}">
			{section name=amounts start=1 loop=$purchase->variant->stock+1 step=1}
			<option variant_id='{$purchase->variant->id}' value="{$smarty.section.amounts.index}" {if $purchase->amount==$smarty.section.amounts.index}selected{/if}>{$smarty.section.amounts.index} {$settings->units}</option>
			{/section}
		</select>
	</td>
                  <td class="total" id="amount_{$purchase->variant->id}"><b>{($purchase->variant->price*$purchase->amount)|convert}&nbsp;{$currency->sign}</b></td>
                  <td class="remove">
    <form class="cart_mini" method="get">
      <input type=hidden name=variant_id value='{$purchase->variant->id}'>
      <input type="submit" class="remove_cart" />
                   </form>
</td>
                </tr>
    {/foreach}
               </tbody>
            </table>
            <table class="total">
              <tbody>
    {if $cart->discount}
    <tr>
                  <td align="right"><b>Скидка</b></td>
                  <td align="right"><b>{$cart->discount}&nbsp;%</b></td>
                </tr>
    {/if}
                <tr>
                  <td align="right"><b>Итого</b></td>
                  <td align="right"><b>{$cart->total_price|convert}&nbsp;{$currency->sign}</b></td>
                </tr>
              </tbody>
            </table>
            <div class="checkout">
            <a class="button"  href="javascript:$.fancybox.close();">Продолжить покупки</a>
            <a class="button" href="/cart"><span>Оформить заказ</span></a>
            </div>
          </div>
       
{else}
 <div class="heading">
            <h4>Корзина</h4>
            <span id="cart_total">Ваша корзина пуста</span> </div>
<div class="checkout">
            <a  class="button" href="javascript:$.fancybox.close();">Продолжить покупки</a>
</div>
{/if}

</div></div>
	<script>
	$(function() {
		// Зум картинок
		$("#inline").fancybox();
	});
	</script>		