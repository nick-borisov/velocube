{* Страница отдельной записи блога *}

<!-- Заголовок /-->
<h1 data-post="{$post->id}">{$post->name|escape}</h1>
<p>{$post->date|date}</p>

<!-- Тело поста /-->
{$post->text}

<!-- Соседние записи /-->
<div id="back_forward">
	{if $prev_post}
		←&nbsp;<a class="prev_page_link" href="blog/{$prev_post->url}">{$prev_post->name}</a>
	{/if}
	{if $next_post}
		<a class="next_page_link" href="blog/{$next_post->url}">{$next_post->name}</a>&nbsp;→
	{/if}
</div>

<!-- Комментарии -->
<div id="comments">

	<h2>Комментарии</h2>
			{* Рекурсивная функция вывода дерева категорий *}
			{function name=comments_tree}	
{if $comments}
	<!-- Список с комментариями -->
	<ul class="comment_list">
		{foreach $comments as $comment}

		<a name="comment_{$comment->id}"></a>
		<li {if $comment->admin == 1}class="admin"{/if}>
			<!-- Имя и дата комментария-->
			<div class="comment_header">	
				{$comment->name|escape}<i>{$comment->date|date}, {$comment->date|time}</i>
				{if !$comment->approved}ожидает модерации</b>{/if}
			</div>
			<!-- Имя и дата комментария (The End)-->
			
			<!-- Комментарий -->
			{$comment->text|escape|nl2br}
			<br><a  href="javascript:show_form('{$comment->id}');">Ответить</a>
			<!-- Комментарий (The End)-->
					</li>
			{if $comment->subcomments}		
			{comments_tree comments = $comment->subcomments}
			{/if}

		{/foreach}
	</ul>
	<!-- Список с комментариями (The End)-->
	{else}
	<p>
		Пока нет комментариев
	</p>
{/if}
{/function}
{comments_tree comments = $comments}

<br><a  href="javascript:show_form('0');">Оставить комментарий</a>	
<div style="display:none"><div id="comment_form">	
	<!--Форма отправления комментария-->	
	<form class="comment_form" method="post">
		<h2>Написать комментарий</h2>
		{if $error}
		<div class="message_error">
			{if $error=='captcha'}
			Неверно введена капча
			{elseif $error=='empty_name'}
			Введите имя
			{elseif $error=='empty_comment'}
			Введите комментарий
			{/if}
		</div>
		{/if}
		<input type="hidden" id="parent" name="parent_id" value="0"/>
		<input type="hidden" id="admin" name="admin" value="{if $smarty.session.admin == 'admin'}1{else}0{/if}"/>
		<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий">{$comment_text}</textarea><br />
		<div>
		<label for="comment_name">Имя</label>
		<input class="input_name" type="text" id="comment_name" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя"/><br />

		<input class="button" type="submit" name="comment" value="Отправить" />
		
		<label for="comment_captcha">Число</label>
		<div class="captcha"><img src="captcha/image.php?{math equation='rand(10,10000)'}" alt='captcha'/></div> 
		<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>
		
		</div>
	</form>
	<!--Форма отправления комментария (The End)-->
</div></div>	
</div>
<!-- Комментарии (The End) -->