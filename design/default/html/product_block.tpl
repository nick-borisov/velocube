{* Шаблон блока в списке товаров *}

	<!-- Товар-->
	<li class="product">
		
		<!-- Фото товара -->
		{if $product->image}
		<div class="image">
		<div class="icons">
		{if $product->featured}
		<img src="design/{$settings->theme|escape}/images/icon-hit.png" title="Хит продаж!" alt="Хит продаж!"/>
		{/if}
		{if $product->new}
		<img src="design/{$settings->theme|escape}/images/icon-new.png" title="Новинка!" alt="Новинка!"/>
		{/if}
		{if $product->variant->compare_price > 0}
		<img src="design/{$settings->theme|escape}/images/icon-sale.png" title="Суперцена!" alt="Суперцена!"/>
		{/if}
		</div>
			<a href="products/{$product->url}"><img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/></a>
		</div>
		{/if}
		<!-- Фото товара (The End) -->

		<div class="product_info">
		<!-- Название товара -->
		<h3 class="{if $product->featured}featured{/if}"><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h3>
		<!-- Название товара (The End) -->
                    <div class="testRater_{$product->id} clr" id="rating_{$product->id}" class="stat">
                      <div class="statVal">
                        <span class="rater">
                          <span class="rater-starsOff" style="width:80px;">
                            <span class="rater-starsOn" style="width:{$product->rating*80/5|string_format:"%.0f"}px"></span>
                          </span>
                          <span class="rater-rating">{$product->rating|string_format:"%.1f"}</span>
                          &#160;(голосов <span class="rater-rateCount">{$product->votes|string_format:"%.0f"}</span>)
                        </span>
                      </div>
                      </div>
      {literal}
    <script type="text/javascript">
      $(function() { $('.testRater_{/literal}{$product->id}{literal}').rater({ postHref: '/ajax/rating.php' }); });
    </script>
    {/literal}<br /> 
		<!-- Описание товара -->
		<div class="annotation">{$product->annotation}</div>
		<!-- Описание товара (The End) -->
		
		{if $product->variants|count > 0}
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					<input id="variants_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $v@first}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
				</td>
				<td>
					{if $v->name}<label class="variant_name" for="variants_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>
			{/foreach}
			</table>
			<input type="hidden" name="mode" value="add"/>
			<input type="submit" class="button" value="в корзину" data-result-text="добавлено"/>
		</form>
		<!-- Выбор варианта товара (The End) -->
		{else}
			Нет в наличии
		{/if}
		<div class="clear"></div>
		<br>
		{if $smarty.session.compared_products && in_array($product->url, $smarty.session.compared_products)}
		<a  href='compare/'>В сравнении</a>
		{else}
		<a class="compare" href='compare/{$product->url}'>Сравнить</a>		
		{/if}
		 &nbsp;|&nbsp;
		 {if $wishlist}
		 <a href='wishlist/remove/{$product->url}'>Удалить</a>
		 {elseif $wishlist_products && in_array($product->url, $wishlist_products)}
		  <a  href='wishlist'>В избранном</a>
		  {else}
		<a class="wishlist" href='wishlist/{$product->url}'>В избранное</a>
		{/if}
		</div>

	</li>
	<!-- Товар (The End)-->