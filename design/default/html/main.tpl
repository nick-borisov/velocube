{* Главная страница магазина *}

{* Для того чтобы обернуть центральный блок в шаблон, отличный от index.tpl *}
{* Укажите нужный шаблон строкой ниже. Это работает и для других модулей *}
{$wrapper = 'index.tpl' scope=parent}

{* Заголовок страницы *}
<h1>{$page->header}</h1>

{* Тело страницы *}
{$page->body}

{* Рекомендуемые товары *}
{get_products var=featured_products featured=1 limit=3 sort=random}
{if $featured_products}
<!-- Список товаров-->
<a href="hits" class="more">Все рекомендуемые</a>
<h1>Рекомендуемые товары</h1>
<ul class="tiny_products">

	{foreach $featured_products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
			
</ul>
{/if}


{* Новинки *}
{get_products var=new_products new=1 limit=3}
{if $new_products}
<a href="new" class="more">Все новинки</a>
<h1>Новинки</h1>
<!-- Список товаров-->
<ul class="tiny_products">

	{foreach $new_products as $product}

{include file='product_block.tpl'}

	{/foreach}
			
</ul>
{/if}	


{* Акционные товары *}
{get_products var=discounted_products discounted=1 limit=9}
{if $discounted_products}
<a href="sale" class="more">Все акционные</a>
<h1>Акционные товары</h1>
<!-- Список товаров-->
<ul class="tiny_products">

	{foreach $discounted_products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
			
</ul>
{/if}	


{* Последние поступления *}
{get_products var=last_products sort=created limit=3}
{if $last_products}

<h1>Последние поступления</h1>
<!-- Список товаров-->
<ul class="tiny_products">

	{foreach $last_products as $product}

{include file='product_block.tpl'}

	{/foreach}
			
</ul>
{/if}