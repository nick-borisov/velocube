{* Список товаров *}

<!-- Хлебные крошки /-->
<div id="path">
	<a href="/">Главная</a>
	{if $articles_category}
	{foreach from=$articles_category->path item=cat}
	→ <a href="articles/{$cat->url}">{$cat->name|escape}</a>
	{/foreach}  
	{/if}
</div>
<!-- Хлебные крошки #End /-->


{* Заголовок страницы *}
{if $page}
<h1>{$page->name|escape}</h1>
{else}
<h1>{$articles_category->name|escape}</h1>
{/if}


{* Описание страницы (если задана) *}
{$page->body}

{if $current_page_num==1}
{* Описание категории *}
{$articles_category->description}
{/if}




<!--Каталог товаров-->
{if $posts}

{* Сортировка *}
{if $posts|count>1}
<div class="sort">
	Сортировать по 
	<a {if $sort=='position'} class="selected"{/if} href="{url articles_sort=position page=null}">умолчанию</a>
	<a {if $sort=='date'}    class="selected"{/if} href="{url articles_sort=date page=null}">дате</a>
	<a {if $sort=='name'}     class="selected"{/if} href="{url articles_sort=name page=null}">названию</a>
</div>
{/if}


{include file='pagination.tpl'}


<!-- Список товаров-->
<ul class="products">

	{foreach $posts as $post}
	<!-- Товар-->
	<li class="product">

		<h3><a data-post="{$post->id}" href="article/{$post->url}">{$post->name|escape}</a></h3>
		<p>{$post->date|date}</p>
		<p>{$post->annotation}</p>

		
	</li>
	<!-- Товар (The End)-->
	{/foreach}
			
</ul>

{include file='pagination.tpl'}	
<!-- Список товаров (The End)-->

{else}
Статьи не найдены
{/if}	
<!--Каталог товаров (The End)-->