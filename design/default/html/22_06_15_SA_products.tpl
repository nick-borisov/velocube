{* Список товаров *}
	<script src="design/{$settings->theme}/js/cookies.js"></script>
<!-- Хлебные крошки /-->
<div id="path">
	<a href="/">Главная</a>
	{if $category}
	{foreach from=$category->path item=cat}
	→ <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
	{/foreach}  
	{if $brand}
	→ <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
	{/if}
	{elseif $brand}
	→ <a href="brands/{$brand->url}">{$brand->name|escape}</a>
	{elseif $keyword}
	→ Поиск
	{elseif $page}
	→ {$page->name|escape}	
	{/if}
</div>
<!-- Хлебные крошки #End /-->


{* Заголовок страницы *}
{if $keyword}
<h1>Поиск {$keyword|escape}</h1>
{elseif $page}
<h1>{$page->name|escape}</h1>
{else}
<h1>{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h1>
{/if}


{* Описание страницы (если задана) *}
{$page->body}

{if $current_page_num==1}
{* Описание категории *}
{$category->description}
{/if}

{* Фильтр по брендам *}
{if $category->brands}
<div id="brands">
	<a href="catalog/{$category->url}" {if !$brand->id}class="selected"{/if}>Все бренды</a>
	{foreach name=brands item=b from=$category->brands}
		{if $b->image}
		<a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></a>
		{else}
		<a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}" {if $b->id == $brand->id}class="selected"{/if}>{$b->name|escape}</a>
		{/if}
	{/foreach}
</div>
{/if}

{* Описание бренда *}
{$brand->description}

{if !$wishlist}
<form method="get" action="{url page=null}">
{if $features}

<div id="features">

<table >
	{foreach $features as $f}
	<tr>
	<td class="feature_name" data-feature="{$f->id}">
		<b>{$f->name}:</b>
	</td>
    </tr><tr>
	<td class="feature_values">
      <ul style="list-style: none; margin: 0; padding: 0">

      {foreach $f->options as $k=>$o}
        <li><input type="checkbox" name="{$f->id}[]" {if $filter_features.{$f->id} && in_array($o->value,$filter_features.{$f->id})}checked="checked"{/if} value="{$o->value|escape}" />{$o->value|escape}</li>
      {/foreach}
      </ul>
	</td>
	</tr>
	{/foreach}
</table>

</div>
{/if}


{literal}
<script>
jQuery(document).ready(function(){


/* слайдер цен */

jQuery("#slider").slider({
{/literal}
	min: {if $min_cost}{$min_cost|string_format:'%d'}{else}{$min_price|convert|string_format:'%d'}{/if},
	max: {if $max_cost}{$max_cost|string_format:'%d'}{else}{$max_price|convert|string_format:'%d'}{/if},
	values: [{if $min_cost}{$min_cost|string_format:'%d'}{else}{$min_price|convert|string_format:'%d'}{/if},{if $max_cost}{$max_cost|string_format:'%d'}{else}{$max_price|convert|string_format:'%d'}{/if}],
{literal}	
	range: true,
	stop: function(event, ui) {
		jQuery("input#minCost").val(jQuery("#slider").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#slider").slider("values",1));
		
    },
    slide: function(event, ui){
		jQuery("input#minCost").val(jQuery("#slider").slider("values",0));
		jQuery("input#maxCost").val(jQuery("#slider").slider("values",1));
    }
});

});
</script>
{/literal}
	<div class="main clear">
		
			<div class="formCost">
				<label for="minCost">Цена: от</label> <input type="text" id="minCost" name="min_cost" value="{if $min_cost}{$min_cost|string_format:'%d'}{else}{$min_price|convert|string_format:'%d'}{/if}"/>
				<label for="maxCost">до</label> <input type="text" id="maxCost" name="max_cost" value="{if $max_cost}{$max_cost|string_format:'%d'}{else}{$max_price|convert|string_format:'%d'}{/if}"/>
			</div>
			<div class="sliderCont">
					<div id="slider"></div>
			</div>
			<div class="clear"></div>

	</div>
			<input type="submit" class="button" value="Подобрать" />
			<input type="button" class="button" value="Сбросить цены" style="clear:none;" onclick="$('input#minCost').val('{$min_price|convert}');$('input#maxCost').val('{$max_price|convert}');$(this).closest('form').submit();" />
			<input type="reset" class="button" style="clear:none;" value="Сбросить все" onclick="$('input#minCost').val('{$min_price|convert}');$('input#maxCost').val('{$max_price|convert}');$(this).closest('form').find('input[type=checkbox]').prop('checked', false);;$(this).closest('form').submit();" />	
</form>	
<div class="clear"></div>
<br>
{/if}
<!--Каталог товаров-->
{if $products}

{* Сортировка *}
{if $products|count>1}
<div class="sort">
<ul class="product_view">
	<li id="product_view_grid" class="current"></li>
	<li id="product_view_list"></li>
</ul>	
{if !$wishlist} 
	<label for="selectPrductSort">Сортировать по</label>	
<select onchange="location.href = this.options[this.selectedIndex].value;" id="selectPrductSort">
<option value="{url sort=position page=null}"{if $sort=='position'} selected{/if}>умолчанию</option>
<option value="{url sort=price page=null}"{if $sort=='price'} selected{/if}>цене</option>
<option value="{url sort=name page=null}"{if $sort=='name'} selected{/if}>названию</option>
<option value="{url sort=rating page=null}"{if $sort=='rating'} selected{/if}>рейтингу</option>
</select>	
{/if}
</div>
{/if}
<div class="clear"></div>

{include file='pagination.tpl'}
<div class="clear"></div>

<!-- Список товаров-->
<ul id="product_list" class="tiny_products">

	{foreach $products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
			
</ul>

{include file='pagination.tpl'}	
<!-- Список товаров (The End)-->
 <script src="/js/jquery.rater.js" type="text/javascript"></script>
{else}
Товары не найдены
{/if}	
<!--Каталог товаров (The End)-->