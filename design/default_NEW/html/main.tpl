{$wrapper = 'index.tpl' scope=parent}
{include file='slider.tpl'}
<ul class="smBanner">
	<li><a rel="nofollow" href="http://velocube.ru/catalog/velosipedy?min_price=&max_price=&discounted=1"><img src="files/1.png" title="Распродажа велосипедов" alt="Распродажа велосипедов"></a></li>
	<li><a rel="nofollow" href="http://velocube.ru/catalog/batuty?min_price=&max_price=&discounted=1"><img src="files/2.png" title="Распродажа батутов" alt="Распродажа батутов"></a></li>
	<li><a rel="nofollow" href="http://velocube.ru/user/register"><img src="files/3.png" title="" alt=""></a></li>
</ul>

{get_brands var=all_brands}
{if $all_brands}
<ul class="carousel">
	{foreach $all_brands as $b}
	<li>
		{if $b->image}
		<a href="brands/{$b->url}"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></a>
		{else}
		<a href="brands/{$b->url}">{$b->name}</a>
		{/if}
	</li>
	{/foreach}
</ul>
{/if}


{literal}
<script type="text/javascript">
$(document).ready(function(){
	var slider = $('.slider').bxSlider({
		mode: 'horizontal',
		auto: true,
		autoHover: true,
		pause: 5000,
		pager: false,
		controls: false
	});
	$('.control-prev').click(function(){
		slider.goToPrevSlide();
		return false;
	});
	$('.control-next').click(function(){
		slider.goToNextSlide();
		return false;
	});
	var carousel = $('.carousel').bxSlider({
		mode: 'horizontal',
		slideWidth: 240,
		slideMargin: 0,

		maxSlides: 5,
		moveSlides: 1,
		infiniteLoop: false,
		pager: false,
		auto: true,
		autoHover: true,
		pause: 2000
	});
	$('.rec_prev').click(function(){
		slider_cat.goToPrevSlide();
		return false;
	});
	$('.rec_next').click(function(){
		slider_cat.goToNextSlide();
		$(window).trigger("scroll");
		return false;
	});
});	
</script>
{/literal}



<h1>{$page->header}</h1>

{* Тело страницы *}
{$page->body}