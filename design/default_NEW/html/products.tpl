{* Список товаров *}
{if $features}
    {assign 'mt' $meta_title}
    {assign 'md' $meta_description}
    {foreach $features as $f}
        {foreach $f->options as $o}
            {if $smarty.get.$f@key == $o->value}
                {assign 'mt' $f->name|cat:' - '|cat:$o->value|cat:' / '|cat:$mt}
                {assign 'md' $f->name|cat:' - '|cat:$o->value|cat:'. '|cat:$md}
            {/if}       
        {/foreach}
    {/foreach}
    {$meta_title = $mt scope=parent}
    {$meta_description = $md scope=parent}
{/if}
	<script src="design/{$settings->theme}/js/cookies.js"></script>
<!-- Хлебные крошки /-->
<div id="path">
	<a href="/">Главная</a>
	{if $category}
	{foreach from=$category->path item=cat}
	→ <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
	{/foreach}  
	{if $brand}
	→ <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
	{/if}
	{elseif $brand}
	→ <a href="brands/{$brand->url}">{$brand->name|escape}</a>
	{elseif $keyword}
	→ Поиск
	{elseif $page}
	→ {$page->name|escape}	
	{/if}
</div>
<!-- Хлебные крошки #End /-->


{* Заголовок страницы *}
{if $keyword}
<h1>Поиск {$keyword|escape}</h1>
{elseif $page}
<h1>{$page->name|escape}</h1>
{else}
<h1>{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h1>
{/if}




<div id="compare_informer">
			{if $compare_products>0}
	В {if $compare_products>1}<a href="/compare/">сравнении</a>{else}сравнении{/if}
	{$compare_products} {$compare_products|plural:'товар':'товаров':'товара'}
{else}
	В сравнении нет товаров
{/if}
</div>

<!--Каталог товаров-->
{if $products}
<div class="sorts">
{* Сортировка *}
{if $products|count>1}
<div class="sort">
<ul class="product_view">
	<li id="product_view_grid" class="current"></li>
	<li id="product_view_list"></li>
</ul>	
{if !$wishlist} 
	<label for="selectPrductSort">Сортировать по</label>	
<select onchange="location.href = this.options[this.selectedIndex].value;" id="selectPrductSort">
<option value="{url sort=position page=null}"{if $sort=='position'} selected{/if}>умолчанию</option>
<option value="{url sort=price page=null}"{if $sort=='price'} selected{/if}>цене</option>
<option value="{url sort=name page=null}"{if $sort=='name'} selected{/if}>названию</option>
<option value="{url sort=rating page=null}"{if $sort=='rating'} selected{/if}>рейтингу</option>
</select>	
{/if}
</div>
{/if}


{include file='pagination.tpl'}
</div>
<div class="clear"></div>

<!-- Список товаров-->
<ul id="product_list">

	{foreach $products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
			
</ul>

{include file='pagination.tpl'}	
<!-- Список товаров (The End)-->
 <script src="/js/jquery.rater.js" type="text/javascript"></script>
{else}
Товары не найдены
{/if}	
<!--Каталог товаров (The End)-->

{if $current_page_num==1}{if !$brand}{if $category->description}{if $page->body}{$page->body}{else}{$category->description}{/if}{/if}{/if}{/if}

{if $brand}{if $current_page_num==1}{if $page->body}{$page->body}{else}{$brand->description}{/if}{/if}{/if}

