{* Шаблон блока в списке товаров *}

	<!-- Товар-->
	<li class="product">
		<!-- Название товара (The End) -->
                    <div class="testRater_{$product->id} clr rating" id="rating_{$product->id}" class="stat">
                      <div class="statVal">
                        <span class="rater">
                          <span class="rater-starsOff" style="width:80px;">
                            <span class="rater-starsOn" style="width:{$product->rating*80/5|string_format:"%.0f"}px"></span>
                          </span>
                          <span class="rater-rating">{$product->rating|string_format:"%.1f"}</span>
                          <!--&#160;(голосов <span class="rater-rateCount">{$product->votes|string_format:"%.0f"}</span>)-->
                        </span>
                      </div>
                      </div>
	<!-- Название товара -->
		<h3 class="title"><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h3>
		
		
	
	
	
		<!-- Фото товара -->
		{if $product->image}
		<div class="image">
		<div class="icons">
		{if $product->new}
		<img src="design/{$settings->theme|escape}/images/icon-new.png" title="Новинка!" alt="Новинка!"/>
		{/if}
		{if $product->variant->compare_price > 0}
		<img src="design/{$settings->theme|escape}/images/icon-sale.png" title="Суперцена!" alt="Суперцена!"/>
		{/if}
		{if $product->featured}
		<img src="design/{$settings->theme|escape}/images/icon-hit.png" title="Хит продаж!" alt="Хит продаж!"/>
		{/if}		
		</div>
			<a href="products/{$product->url}"><img src="{$product->image->filename|resize:228:165}" alt="{$product->name|escape}"/></a>
		</div>
		{/if}
		<!-- Фото товара (The End) -->
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
<div class="product_info">
		
		<!-- Описание товара -->
		<div class="annotation">{$product->annotation}</div>
		<!-- Описание товара (The End) -->
		
		{if $product->variants|count > 0}
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
		
		
		
		
		
		
		
		

{* Это если вариант один и с именем*}
{if $product->variants|count==1}

<div class="price">
	<span{if $product->variants[0]->compare_price} class="hotsale-price"{/if}>{$product->variants[0]->price|convert} {$currency->sign|escape}</span>
	{if $product->variants[0]->compare_price}<s>{$product->variants[0]->compare_price|convert} {$currency->sign|escape}</s>{/if}
</div>	
<div class="ver"></div>
	<select name="variant" style='display:none;'>
		{foreach $product->variants as $v}
		<option value="{$v->id}" {if $v->compare_price > 0}compare_price="{$v->compare_price|convert}"{/if} price="{$v->price|convert}">
		
		</option>
		{/foreach}
	</select>


{else}
<div class="price">
	<span{if $product->variants[0]->compare_price} class="hotsale-price"{/if}>{$product->variants[0]->price|convert} {$currency->sign|escape}</span>
	{if $product->variants[0]->compare_price}<s>{$product->variants[0]->compare_price|convert} {$currency->sign|escape}</s>{/if}
</div>	
{* А это если вариантов несколько *}
	<div class="ver"><select name="variant" >
		{foreach $product->variants as $v}
		<option value="{$v->id}" {if $v->compare_price > 0}compare_price="{$v->compare_price|convert}"{/if} price="{$v->price|convert}">
		{$v->name}
		</option>
		{/foreach}
	</select>
{/if}	
		
		
		
		
		
		
		
		
		
		
		
		
			<input type="hidden" class="cr_pr" name="price" value="{($product->variant->price/10)|convert}"/>
			<input type="hidden" class="url" name="url" value="cart?variant={$product->variant->id}&credit=1"/>
			<input type="hidden" name="mode" value="add"/>
						{*if $product->variant->price>=15000}
			<div class="clear"></div>
			В кредит <a class="cred_zoom" href="javascript:void(0);" onclick="$('#p_v').html($(this).closest('form').find('.cr_pr').val());$('#pr_cred').attr('href',$(this).closest('form').find('.url').val());" ><span class="pr">{($product->variant->price/10)|convert}</span> {$currency->sign|escape}</a><br /><br />
			<div class="clear"></div>

			{/if*}
			<input type="submit" onclick="yaCounter21197344.reachGoal('addcart1');yaCounter21197344.reachGoal('addcart');" class="button" value="в корзину" data-result-text="добавлено"/>

		</form>
		<!-- Выбор варианта товара (The End) -->
		{else}
			Нет в наличии
		{/if}
		<div class="clear"></div>
		<br>
		<!--div class="bottom">
		
		 &nbsp;|&nbsp;
		 {if $wishlist}
		 <a rel="nofollow" href='wishlist/remove/{$product->url}'>Удалить</a>
		 {elseif $wishlist_products && in_array($product->url, $wishlist_products)}
		  <a rel="nofollow" href='wishlist'>В избранном</a>
		  {else}
		<a rel="nofollow" class="wishlist" onclick="yaCounter21197344.reachGoal('addwishlist');" href='wishlist/{$product->url}'>В избранное</a>
		{/if}
		</div-->
		</div>
    <div class="comp_box">
	{if $smarty.session.compared_products && in_array($product->url, $smarty.session.compared_products)}
		<a rel="nofollow" href='compare/'>В сравнении</a>
		{else}
		<a rel="nofollow" onclick="yaCounter21197344.reachGoal('addcompare');" class="compare" href='compare/{$product->url}'>Сравнить</a>		
	{/if}
	</div>
	</li>
	<!-- Товар (The End)-->