{* Страница товара *}
 <script src="design/{$settings->theme|escape}/js/easySlider1.7.js" type="text/javascript"></script>
<!-- Хлебные крошки /-->
<div id="path">
	<a href="./">Главная</a>
	{foreach from=$category->path item=cat}
	> <a href="catalog/{$cat->url}">{$cat->name|escape}</a>
	{/foreach}
	{if $brand}
	> <a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a>
	{/if}
	>  {$product->name|escape}                
</div>
<!-- Хлебные крошки #End /-->
 <!-- Соседние товары /-->
    <div id="back_forward">
        {if $prev_product}
            ?&nbsp;<a class="prev_page_link" href="products/{$prev_product->url}">{$prev_product->name|escape}</a>
        {/if}
        {if $next_product}
            <a class="next_page_link" href="products/{$next_product->url}">{$next_product->name|escape}</a>&nbsp;?
        {/if}
    </div>

<h1 data-product="{$product->id}">{$product->name|escape}</h1>

<div class="product">

	<!-- Большое фото -->
	{if $product->image}
	<div class="image">
			<div class="icons">
		{if $product->featured}
		<img src="design/{$settings->theme|escape}/images/icon-hit.png" title="Хит продаж!" alt="Хит продаж!"/>
		{/if}
		{if $product->new}
		<img src="design/{$settings->theme|escape}/images/icon-new.png" title="Новинка!" alt="Новинка!"/>
		{/if}
		{if $product->variant->compare_price > 0}
		<img src="design/{$settings->theme|escape}/images/icon-sale.png" title="Суперцена!" alt="Суперцена!"/>
		{/if}
		</div>
		<a href="{$product->image->filename|resize:800:600:w}" class="zoom" data-rel="group"><img src="{$product->image->filename|resize:500:300}" alt="{$product->product->name|escape}" /></a>
		
		
<!-- Дополнительные фото продукта -->
	{if $product->images|count>1}
	<div class="images">

            <div id="gallery-slider">
                <ul>
                    <li>
		{* cut удаляет первую фотографию, если нужно начать 2-й - пишем cut:2 и тд *}
		{foreach $product->images|cut as $i=>$image}
			<a href="{$image->filename|resize:800:600:w}" class="zoom" data-rel="group"><img src="{$image->filename|resize:95:95}" alt="{$product->name|escape}" /></a>
		{if $image@iteration%4 == 0 && !$image@last}
		</li><li>
		{/if}  
		{/foreach}
                                                </li>                            
                </ul>
            </div>

	</div>
	{/if}
	<!-- Дополнительные фото продукта (The End)-->
	</div>
	{/if}
	<!-- Большое фото (The End)-->

	
	<!-- Описание товара -->
	<div class="description">
                    <div class="testRater_{$product->id} clr" id="rating_{$product->id}" class="stat">
                      <div class="statVal">
                        <span class="rater">
                          <span class="rater-starsOff" style="width:80px;">
                            <span class="rater-starsOn" style="width:{$product->rating*80/5|string_format:"%.0f"}px"></span>
                          </span>
                          <span class="rater-rating">{$product->rating|string_format:"%.1f"}</span>
                          &#160;(голосов <span class="rater-rateCount">{$product->votes|string_format:"%.0f"}</span>)
                        </span>
                      </div> 
    </div>
      {literal}
    <script type="text/javascript">
      $(function() { $('.testRater_{/literal}{$product->id}{literal}').rater({ postHref: '/ajax/rating.php' }); });
    </script>
    {/literal}
   <br>
	<b>{$product->sklad}</b>
	<br>
	<b>{$product->atribut}</b>	
	<div class="clear"></div>
	<br>
		{if $smarty.session.compared_products && in_array($product->url, $smarty.session.compared_products)}
		<a rel="nofollow" href='compare/'>В сравнении</a>
		{else}
		<a rel="nofollow" class="compare" onclick="yaCounter21197344.reachGoal('addcompare');" href='compare/{$product->url}'>Сравнить</a>		
		{/if}	 
	<br><br>	
		{$product->annotation}
		{if $product->variants|count > 0}
                
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					<input id="product_{$v->id}" name="variant" value="{$v->id}" type="radio" onclick="$(this).closest('form').find('span.pr').html('{($v->price/10)|convert}');$(this).closest('form').find('a.cred').attr('href','cart?variant={$v->id}&credit=1');" class="variant_radiobutton" {if $product->variant->id==$v->id}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
				</td>
				<td>
					{if $v->name}<label class="variant_name" for="product_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>
			{/foreach}
			</table>
			<input type="hidden" name="mode" value="add"/>
			<input type="submit" class="button" onclick="yaCounter21197344.reachGoal('addcart2');yaCounter21197344.reachGoal('addcart');" value="в корзину" data-result-text="добавлено"/>
			<div style="clear:both;"></div>
						{*if $product->variant->price>=15000}
						<table>
			<tr class="variant">
				<td>первый взнос:
					</td>
				<td>

					<span class="price"><span class="pr">{($product->variant->price/10)|convert}</span> <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>	
			</table>					
			<a class="cred" href="cart?variant={$product->variant->id}&credit=1" ></a>
			<div style="clear:both;"></div>
			{/if*}
		</form>
		<!-- Выбор варианта товара (The End) -->
 

                <!-- Блок «Поделиться» -->
        <script type="text/javascript" src="//yandex.st/share/share.js"
charset="utf-8"></script>
<div class="yashare-auto-init" data-yashareL10n="ru"
 data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj"

></div>
        <!-- Блок «Поделиться» (The End)-->
        
        <!-- Кнопка мне нравится вконтакте -->
{literal}
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?99"></script>

<script type="text/javascript">
  VK.init({apiId: 3852449, onlyWidgets: true});
</script>

<!-- Put this div tag to the place, where the Like block will be -->
<div id="vk_like"></div>
<script type="text/javascript">
VK.Widgets.Like("vk_like", {type: "full"});
</script>
{/literal}
<!-- Кнопка мне нравится вконтакте (The End) -->

		{else}
			Нет в наличии

					{if $product->variant2}
					<BR>{if $product->variant2->compare_price > 0}<span class="compare_price">{$product->variant2->compare_price|convert}</span>{/if}
					<span class="price">{$product->variant2->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
					{/if}
		
		{/if}
			
	</div>
	<!-- Описание товара (The End)-->
{$no_id = $product->id}
	

	

	<!-- Соседние товары /-->
	<div id="back_forward">
		<!--{if $prev_product}
			<&nbsp;<a class="prev_page_link" href="products/{$prev_product->url}">{$prev_product->name|escape}</a>
		{/if}
		{if $next_product}
			<a class="next_page_link" href="products/{$next_product->url}">{$next_product->name|escape}</a>&nbsp;>
		{/if}--
	</div>
	
</div>
<!-- Описание товара (The End)-->

<div class="section">
<script src="design/{$settings->theme}/js/tabs.js"></script>
	<ul class="tabs">
		<li class="current">Описание</li>
		{if $product->features}
		<li>Характеристики</li>
		{/if}
		{if $product->video}
		<li>Видео</li>
		{/if}
		<li>Отзывы ({$comments|count})</li>
	</ul>

	<div class="box visible">
	{$product->body}
	</div>
	{if $product->features}
	<div class="box product">
	<ul class="features">
	{foreach $product->features as $f}
	<li>
		<label>{$f->name}</label>
		<span>{$f->value}</span>
	</li>
	{/foreach}
	</ul>
	</div>
	{/if}
	{if $product->video}
	<div class="box">
	{$product->video}
	</div>
	{/if}
	<div class="box">
<!-- Комментарии -->
<div id="comments">

	<h2>Комментарии</h2>
			{* Рекурсивная функция вывода дерева категорий *}
			{function name=comments_tree}	
{if $comments}
	<!-- Список с комментариями -->
	<ul class="comment_list">
		{foreach $comments as $comment}

		<a name="comment_{$comment->id}"></a>
		<li {if $comment->admin == 1}class="admin"{/if}>
			<!-- Имя и дата комментария-->
			<div class="comment_header">	
				{$comment->name|escape}<i>{$comment->date|date}, {$comment->date|time}</i>
				{if !$comment->approved}ожидает модерации</b>{/if}
			</div>
			<!-- Имя и дата комментария (The End)-->
			
			<!-- Комментарий -->
			{$comment->text|escape|nl2br}
			<br><a  href="javascript:show_form('{$comment->id}');">Ответить</a>
			<!-- Комментарий (The End)-->
					</li>
			{if $comment->subcomments}		
			{comments_tree comments = $comment->subcomments}
			{/if}

		{/foreach}
	</ul>
	<!-- Список с комментариями (The End)-->
	{else}
	<p>
		Пока нет комментариев
	</p>
{/if}
{/function}
{comments_tree comments = $comments}

<br><a  href="javascript:show_form('0');">Оставить комментарий</a>	
<div style="display:none"><div id="comment_form">	
	<!--Форма отправления комментария-->	
	<form class="comment_form" method="post">
		<h2>Написать комментарий</h2>
		{if $error}
		<div class="message_error">
			{if $error=='captcha'}
			Неверно введена капча
			{elseif $error=='empty_name'}
			Введите имя
			{elseif $error=='empty_comment'}
			Введите комментарий
			{/if}
		</div>
		{/if}
		<input type="hidden" id="parent" name="parent_id" value="0"/>
		<input type="hidden" id="admin" name="admin" value="{if $smarty.session.admin == 'admin'}1{else}0{/if}"/>
		<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий">{$comment_text}</textarea><br />
		<div>
		<label for="comment_name">Имя</label>
		<input class="input_name" type="text" id="comment_name" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя"/><br />

		<input class="button" type="submit" name="comment" value="Отправить" />
		
		<label for="comment_captcha">Число</label>
		<div class="captcha"><img src="captcha/image.php?{math equation='rand(10,10000)'}" alt='captcha'/></div> 
		<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>
		
		</div>
	</form>
	<!--Форма отправления комментария (The End)-->
</div></div>	
</div>
<!-- Комментарии (The End) -->
	</div>
				
</div>	





{* Связанные товары *}
{if $related_products}
<h2>Так же советуем посмотреть</h2>
<!-- Список каталога товаров-->
<ul class="tiny_products">
	{foreach $related_products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
</ul>
{/if}


{if $brand}
{get_products var=brand_products brand_id=$brand->id no_id=$no_id limit=3 sort=random}
{if $brand_products}
<!-- Список товаров-->

<h2>Товары того же бренда (случайные)</h2>
<ul class="tiny_products">

	{foreach $brand_products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
			
</ul>
{/if}
{/if}

{get_products var=cat_products category_id=$category->id  no_id=$no_id limit=3}
{if $cat_products}
<!-- Список товаров-->

<h2>Товары той же категории (первые)</h2>
<ul class="tiny_products">

	{foreach $cat_products as $product}
	
{include file='product_block.tpl'}

	{/foreach}
			
</ul>
{/if}

 <script src="/js/jquery.rater.js" type="text/javascript"></script>
{literal}
<script>
$(function() {
	// Раскраска строк характеристик
	$(".features li:even").addClass('even');

	// Зум картинок
	$("a.zoom").fancybox({ 'hideOnContentClick' : true });


$('#gallery-slider').easySlider({prevId:'prevBtn',prevText:'',nextId:'nextBtn',nextText:''});
var sliderItems=$('#gallery-slider ul li');
if(sliderItems.length<2){$('#gallery-slider').parent().find('#nextBtn a').hide();}

});
</script>
{/literal}
