
    <h1>{if $page->header}{$page->header}{else}Карта сайта{/if}</h1>
    {$page->body}
<div id="catalog_menu">
    {if $pages}
    <h2>Страницы</h2>
    <div class="pages">
        <ul>
    {foreach $pages as $p}
    {if $p->visible}
            <li><a href="{$p->url}">{$p->header}</a></li>
     {/if}          
    {/foreach}
        </ul>
    </div>
    {/if}

    {if $posts}
    <h2>Блог</h2>
    <div class="posts">
        <ul>
    {foreach $posts as $p}
            <li><a href="{$p->url}">{$p->name}</a></li>   
    {/foreach}
        </ul>
    </div>
    {/if}
    
    {if $articles}
    <h2>Статьи</h2>
    <div class="posts">
        <ul>
    {foreach $articles as $p}
            <li><a href="{$p->url}">{$p->name}</a></li>   
    {/foreach}
        </ul>
    </div>
    {/if}    

    {if $cats}
    <div class="cats">
    <h2>Товары</h2>
    {function name=cat_prod}
    {if $prod}
        <ul class="product">
        {foreach $prod as $p}
            <li><a href="products/{$p->url}">{$p->name}</a></li>
        {/foreach}
        </ul>
    {/if}
    {/function}    
    
    {function name=cat_tree}
    {if $cats}
        <ul>
        {foreach $cats as $c}
            {* Показываем только видимые категории *}
            {if $c->visible}
                <li><a href="catalog/{$c->url}">{$c->name}</a>
                    {cat_tree cats=$c->subcategories}
                    {cat_prod prod=$c->products}
                </li>
            {/if}
        {/foreach}
        </ul>
    {/if}
    {/function}
    
    {cat_tree cats=$cats} 
    </div>
    {/if}
    
</div>
