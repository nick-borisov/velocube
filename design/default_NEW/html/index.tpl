<!DOCTYPE html>
<html>
<head>
	<base href="{$config->root_url}"/>
	<title>{$meta_title|escape}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="{$meta_description|escape}" />
	<meta name="keywords" content="{$meta_keywords|escape}" />
	<meta name="viewport" content="width=1024" />
	{if $canonical}<link rel="canonical" href="{$canonical}" />{/if}
	
	
	{* Стили *}
	<link href="design/{$settings->theme|escape}/css/style.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="design/{$settings->theme|escape}/css/cart.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="design/{$settings->theme|escape}/css/main.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="design/{$settings->theme|escape}/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	
	{* JQuery *}
	<script src="js/jquery/jquery.js"  type="text/javascript"></script>
	{* Слайдер *}
	<script src="js/jquery.bxslider.min.js"  type="text/javascript"></script>
	
	{* Всплывающие подсказки для администратора *}
	{if $smarty.session.admin == 'admin'}
	<script src ="js/admintooltip/admintooltip.js" type="text/javascript"></script>
	<link   href="js/admintooltip/css/admintooltip.css" rel="stylesheet" type="text/css" /> 
	{/if}
	
	{* Увеличитель картинок *}
	<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" href="js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
	
	{* Ctrl-навигация на соседние товары *}
	<script type="text/javascript" src="js/ctrlnavigate.js"></script>           
	
	{* Аяксовая корзина *}
	<script src="design/{$settings->theme}/js/jquery-ui.min.js"></script>
	<script src="design/{$settings->theme}/js/ajax_cart.js"></script>


	<script type="text/javascript" src="design/{$settings->theme}/js/jquery.ui-slider.js"></script>
	<script type="text/javascript" src="design/{$settings->theme}/js/jquery.main.js"></script>
	<script type="text/javascript" src="design/{$settings->theme}/js/filter.min.js"></script>
	<script type="text/javascript" src="design/{$settings->theme}/js/jquery.jcarousel.min.js"></script>
	
	{* js-проверка форм *}
	<script src="/js/baloon/js/baloon.js" type="text/javascript"></script>
	<link   href="/js/baloon/css/baloon.css" rel="stylesheet" type="text/css" /> 
	
	{* Автозаполнитель поиска *}
	{literal}
	<script src="js/autocomplete/jquery.autocomplete-min.js" type="text/javascript"></script>
	<style>
	.autocomplete-w1 { position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; }
	.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:auto;  overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
	.autocomplete .selected { background:#F0F0F0; }
	.autocomplete div { padding:2px 5px; white-space:nowrap; }
	.autocomplete strong { font-weight:normal; color:#3399FF; }
	</style>	
	<script>
	$(function() {

	$('#mycarousel').jcarousel({
		scroll: 5,
		wrap: 'circular',
		auto: 5,
		animation: 1500
	});

		//  Автозаполнитель поиска
		$(".input_search").autocomplete({
			serviceUrl:'ajax/search_products.php',
			minChars:1,
			noCache: false, 
			onSelect:
				function(value, data){
					 $(".input_search").closest('form').submit();
				},
			fnFormatResult:
				function(value, data, currentValue){
					var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
					var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
	  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
				}	
		});
	});
	</script>        
	{/literal}
        
</head>
<body>
<div id="top_background">
	<div id="top">
		<ul id="menu">
		{foreach $pages as $p}
			{* Выводим только страницы из первого меню *}
			{if $p->menu_id == 1}
			<li {if $page && $page->id == $p->id}class="selected"{/if}><a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a></li>
			{/if}
		{/foreach}
		</ul>
		<div id="account">
		{if $user}
			<span id="username">
				<a href="user">{$user->name}</a>{if $group->discount>0}, ваша скидка &mdash; {$group->discount}%{/if}</span>
				<a id="logout" href="user/logout">выйти</a>
		{else}                                  
				<a id="register" href="user/register">Регистрация</a> / <a id="login" href="user/login">Вход</a> или
				<!-- Ulogin --><script src="http://ulogin.ru/js/ulogin.js"></script>
				<div id="uLogin" data-ulogin="display=small;fields=first_name,last_name,email{*,phone,city*};providers=vkontakte,facebook,odnoklassniki,mailru;hidden=other;redirect_uri=http://velocube.ru/user/login"></div><!-- Ulogin end -->
		{/if}
		</div>
	</div>
</div>
<div id="header">
	<div id="logo">
		<a href="/"><img src="design/{$settings->theme|escape}/images/logo.png" title="{$settings->site_name|escape}" alt="{$settings->site_name|escape}"/></a>
	</div>
	<div id="search">
		<form action="products">
			<input class="input_search" type="text" name="keyword" value="{$keyword|escape}" placeholder="Поиск товара"/>
			<input class="button_search" value="" type="submit" />
		</form>
	</div>
	<div id="contact">
		<span id="phone">8 (499) 653-88-38</span>
		<div id="address">с 9-00 до 22-00 без выходных!</div>
	</div>
	<div class="basket_block">
		<noindex>
			{include file='cart_informer.tpl'}
		</noindex>
	</div>
	{function name=categories_tree}
		{if $categories}
			{foreach $categories as $c}
				{* Показываем только видимые категории *}
				{if !$c->metka}
				{if $c->visible}
				<li>
				{if $c->image}<!--img src="{$config->categories_images_dir}{$c->image}" alt="{$c->name}"-->{/if}
					<a {if $category->id == $c->id}class="selected"{/if} href="catalog/{$c->url}">{$c->name}{if $c->products_count}({$c->products_count}){/if}</a>
					{if $c->subcategories}						
					<ul class="dir">{categories_tree categories=$c->subcategories}</ul>
					{/if}
				</li>
				{/if}
			{/foreach}
			
			{/if}
			{/function}
	<ul id="nav" class="dropdown dropdown-linear dropdown-columnar">
		{categories_tree categories=$categories}
	</ul>
</div>
<div id="main">
	<div id="{if $module=='ProductsView'}content{else}contents{/if}">
		{$content}
	</div>
	{if $module=='ProductsView'}
	<div id="left">
		{if $category->brands || $features}
		<div id="filter">
			{include file='filter.tpl'}
		</div>
		{/if}
		
		{get_browsed_products var=browsed_products limit=20}
		{if $browsed_products}
		<h3>Вы просматривали:</h3>
		<ul id="browsed_products">
			{foreach $browsed_products as $browsed_product}
			<li>
				<a href="products/{$browsed_product->url}"><img src="{$browsed_product->image->filename|resize:50:50}" alt="{$browsed_product->name}" title="{$browsed_product->name}"></a>
			</li>
			{/foreach}
		</ul>
		{/if}

		<br /><a href="http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://market.yandex.ru/shop/219626/reviews"><img src="http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2507/*http://grade.market.yandex.ru/?id=219626&action=image&size=3" border="0" width="200" height="125" alt="Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете" /></a>
	</div>
	{/if}
</div>
<div id="footer">
	<ul id="footer_menu">
	{foreach $pages as $p}
		{* Выводим только страницы из первого меню *}
		{if $p->menu_id == 3}
		<li {if $page && $page->id == $p->id}class="selected"{/if}>
			<a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a>
		</li>
		{/if}
	{/foreach}
	</ul>
	<br />Интернет-магазин <a href="http://velocube.ru/">Velocube.ru</a>, © 2013-2014
    <div style="color: #AAA">
      	Информация, изложенная на сайте, не является публичной офертой, согласно статьи 437 Гражданского кодекса РФ, поскольку не содержит все необходимые существенные условия договора. О наличии товара, его цене, стране изготовителе, комплектации и основных потребительских свойствах уточняйте при оформлении заказа у наших операторов. Договор купли-продажи считается заключенным с момента оплаты товара и получения документов, подтверждающих покупку.
    </div>
        	</div>
	<!-- Футер (The End)-->
     <div style="display:none;"> 
      <div id="data2">
      <h2>Купить в кредит</h2>
      <div class="text">
      Получите кредит, не выходя из дома! Просто положите товар в корзину, а при оформлении отметьте, что хотите приобрести его в кредит. Заполните короткую анкету и менеджер Yes Credit сообщит вам решение банка. Никаких посещений банка, договор вам доставит курьер!
      </div>
      <span class="product">
      первый взнос:<span class="price"><span id="p_v"></span><span class="currency">{$currency->sign|escape}</span></span>
      </span>
      <a href="#"  id="pr_cred"> </a>
      </div>
      </div>    
{literal}
<script>
$(function() {

	// Зум картинок
	//$("a.cred_zoom").fancybox({ 'hideOnContentClick' : false });

});
</script>      
<script type='text/javascript'>
	(function() {
		var s = document.createElement('script');
		s.type ='text/javascript';
		s.id = 'supportScript';
		s.charset = 'utf-8';
		s.async = true;
		s.src = 'https://lcab.sms-uslugi.ru/support/support.js?h=d923f2b224ec6f3f48958c7e30945c64';
		var sc = document.getElementsByTagName('script')[0];
		
		var callback = function(){
			/*
				Здесь вы можете вызывать API. Например, чтобы изменить отступ по высоте:
				supportAPI.setSupportTop(200);
			*/
		};
		
		s.onreadystatechange = s.onload = function(){
			var state = s.readyState;
			if (!callback.done && (!state || /loaded|complete/.test(state))) {
				callback.done = true;
				callback();
			}
		};
		
		if (sc) sc.parentNode.insertBefore(s, sc);
		else document.documentElement.firstChild.appendChild(s);
	})();
</script>
<!-- Yandex.Metrika counter --><script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script><script type="text/javascript">try { var yaCounter21197344 = new Ya.Metrika({id:21197344, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true,params:window.yaParams||{ }}); } catch(e) { }</script>{/literal}<noscript><div><img src="//mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body>
</html>