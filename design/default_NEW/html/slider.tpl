{get_slides var=slide}
{if $slide}	
<div class="main-slider-wrapper">
	<div class="main-slider">
		<ul class="slider">
		{foreach $slide as $s}
				{if $s->image}
					<li>{if $s->url}<a href="{$s->url}">{/if}
					<img src="{$s->image}" alt="" {if $s->description}title="#slide_{$s->id}"{/if} />
					{if $s->url}</a>{/if}</li>
				{/if}
		{/foreach}
		</ul>
	</div>
	<i class="icon control-prev"></i>
	<i class="icon control-next"></i>
</div>
{/if}