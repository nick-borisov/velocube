$('a.cred_zoom').live('click', function(e) {
             $.fancybox({
             'href'			: '#data2'
             });  
    	return false;

});
// Аяксовая корзина
$('form.variants').live('submit', function(e) {
	e.preventDefault();
	button = $(this).find('input[type="submit"]');
    string = $(this).serialize();
	$.ajax({
		url: "ajax/cart.php",
		data: string,
		dataType: 'json',
		success: function(data){
			$('#cart_informer').html(data);
			if(button.attr('data-result-text'))
				button.val(button.attr('data-result-text'));
             $.fancybox({
             'href'			: '#data'
             });        
		}
	});
	var o1 = $(this).offset();
	var o2 = $('#cart_informer').offset();
	var dx = o1.left - o2.left;
	var dy = o1.top - o2.top;
	var distance = Math.sqrt(dx * dx + dy * dy);
	$(this).closest('.product').find('.image img').effect("transfer", { to: $("#cart_informer"), className: "transfer_class" }, distance);	
	$('.transfer_class').html($(this).closest('.product').find('.image').html());
	$('.transfer_class').find('img').css('height', '100%');
	return false;
});

$('form.cart_mini').live('submit', function(e) {
e.preventDefault();
	button = $(this).find('input[type="submit"]');
	$.ajax({
		url: "ajax/cart.php",
		data: {variant: $(this).find('input[name="variant_id"]').val(),'mode':'remove'},
		dataType: 'json',
		success: function(data){
			$('#cart_informer').html(data);
			$('#cart').html(data);
             $.fancybox({
             'href'			: '#data'
             });
               
		}
	});
  $.fancybox.showActivity();
return false;
});

function update_cart(variant_id,amount)
{

	$.ajax({
		url: "ajax/cart.php",
		data: {'variant':variant_id,'amount':amount,'mode':'update'},
		success: function(data){
		if(data){
			$('#cart_informer').html(data);

             $.fancybox({
             'href'			: '#data'
             });
      
        }
		}
	});
  $.fancybox.showActivity();
}
function show_form(parent_id)
{
$('#parent').val(parent_id);
             $.fancybox({
             'href'			: '#comment_form'
             });
}
$('a.compare').live('click', function(e) {
      href = $(this).attr('href');
          $('#compare_informer').load(href+' #compare_informer');
          $(this).removeClass('compare');
          $(this).attr('href', 'compare/');
    			$(this).html("В сравнении");          
    	var o1 = $(this).offset();
    	var o2 = $('#compare_informer').offset();
    	var dx = o1.left - o2.left;
    	var dy = o1.top - o2.top;
    	var distance = Math.sqrt(dx * dx + dy * dy);
    	$(this).closest('.product').find('.image img').effect("transfer", { to: $("#compare_informer"), className: "transfer_class" }, distance);
    	$('.transfer_class').html($(this).closest('.product').find('.image').html());
    	$('.transfer_class').find('img').css('height', '100%');
    	return false;

});

$(function() {
	// Раскраска строк характеристик
	//$(".features li:even").addClass('even');

	// Зум картинок
	//$("a.zoom").fancybox({ 'hideOnContentClick' : true });


//$('#gallery-slider').easySlider({prevId:'prevBtn',prevText:'',nextId:'nextBtn',nextText:''});
//var sliderItems=$('#gallery-slider ul li');
//if(sliderItems.length<2){$('#gallery-slider').parent().find('#nextBtn a').hide();}

});