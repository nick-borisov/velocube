$('form.variants').live('submit', function(e) {
	e.preventDefault();
	button = $(this).find('input[type="submit"]');
	if($(this).find('input[name=variant]:checked').size()>0)
		variant = $(this).find('input[name=variant]:checked').val();
	if($(this).find('select[name=variant]').size()>0)
		variant = $(this).find('select').val();
	$.ajax({
		url: "ajax/cart.php",
		data: {variant: variant},
		dataType: 'json',
		success: function(data){
			$('.basket_block').html(data);
			if(button.attr('data-result-text'))
				button.val(button.attr('data-result-text'));
		}
	});
	var o1 = $(this).offset();
	var o2 = $('.basket_block').offset();
	var dx = o1.left - o2.left;
	var dy = o1.top - o2.top;
	var distance = Math.sqrt(dx * dx + dy * dy);
	$(this).closest('.product').find('.image img').effect("transfer", { to: $(".basket_block"), className: "transfer_class" }, distance);	
	$('.transfer_class').html($(this).closest('.product').find('.image').html());
	$('.transfer_class').find('img').css('height', '100%');
	return false;
});


function show_form(parent_id)
{
$('#parent').val(parent_id);
             $.fancybox({
             'href'			: '#comment_form'
             });
}
$('a.compare').live('click', function(e) {
      href = $(this).attr('href');
          $('#compare_informer').load(href+' #compare_informer');
          $(this).removeClass('compare');
          $(this).attr('href', 'compare/');
    			$(this).html("В сравнении");          
    	var o1 = $(this).offset();
    	var o2 = $('#compare_informer').offset();
    	var dx = o1.left - o2.left;
    	var dy = o1.top - o2.top;
    	var distance = Math.sqrt(dx * dx + dy * dy);
    	$(this).closest('.product').find('.image img').effect("transfer", { to: $("#compare_informer"), className: "transfer_class" }, distance);
    	$('.transfer_class').html($(this).closest('.product').find('.image').html());
    	$('.transfer_class').find('img').css('height', '100%');
    	return false;

});