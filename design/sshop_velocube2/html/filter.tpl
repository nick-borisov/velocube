{strip}
<form class="filter sidebar_item" method="get" action="catalog/{$category->url}{if $mark}/tag/{$mark->url}{/if}">

	<div class="filter_title text_2 text_white text_bold visible_xs visible_sm">
		Фильтр товаров
		<span id="filter_close" class="filter_close icon icon_link fright text_red">{include file="svg.tpl" svgId="icon_close" width="20px" height="20px"}</span>
	</div>
	
	<div class="filter_content absolute">

		{* Фильтр по цене *}
		<div class="price_filter filter_item trigger_container">
		
			<div class="filter_item_title relative text_black">
				Цена&nbsp;
				<span class="filter_item_trigger absolute trigger fright icon icon_more active" data-trigger_action="slide">
					<span class="icon_more_line line_1"></span>
					<span class="icon_more_line line_2"></span>
				</span>
			</div>
			
			<input type="hidden" name="mark_id" value="{$mark->id}">
			<input type="hidden" name="cat_id" value="{$category->id}">
			<input type="hidden" name="sort" value="{$sort}">
			<input type="hidden" name="keyword" value="{$keyword}">
			<input type="hidden" name="page" value="1">
			
			<div class="filter_item_content trigger_content">
			
				<div id="slider_price"></div>
				
				<div class="input_price flex flex_content_justify flex_align_center">
					<div>
						<input id="min_price" class="form_input" type="text" data-min_price="{if $smarty.get.min_price}{$smarty.get.min_price}{else}{$max_min_price->min_price|convert:null:false|floor}{/if}" name="min_price" value="{if $smarty.get.min_price}{$smarty.get.min_price}{else}{$max_min_price->min_price|convert:null:false|floor}{/if}" autocomplete="off">
					</div>
					<div class="input_price_separator text_bold text_blue2 text_5">&ndash;</div>
					<div>
						<input id="max_price" class="form_input" type="text" data-max_price="{if $smarty.get.max_price}{$smarty.get.max_price}{else}{$max_min_price->max_price|convert:null:false|ceil}{/if}" name="max_price" value="{if $smarty.get.max_price}{$smarty.get.max_price}{else}{$max_min_price->max_price|convert:null:false|ceil}{/if}" autocomplete="off">
					</div>
				</div>
				
			</div>
			
		</div>
		{* /Фильтр по цене *}
		
		{* Фильтр по статусам *}
		<div class="filter_status filter_item">
			<ul class="filter_list">
				<li class="filter_list_item">
					<input id="in_stock" class="hidden" type="checkbox" name="in_stock" value="1"{if $in_stock->checked} checked{/if}>
					<label for="in_stock" class="checkbox_label text_5">Только в наличии <span class="filter_count">{if !$in_stock->checked}({$in_stock->count}){/if}</span></label>
				</li>
				<li class="filter_list_item">
					<input id="discounted" class="hidden" type="checkbox" name="discounted" value="1"{if $discounted->checked} checked{elseif $discounted->disabled} disabled{/if}>
					<label for="discounted" class="checkbox_label text_5">Товары со скидкой <span class="filter_count">{if !$discounted->checked}({$discounted->count}){/if}</span></label>
				</li>
				<li class="filter_list_item">
					<input id="featured" class="hidden" type="checkbox" name="featured" value="1"{if $featured->checked} checked{elseif $featured->disabled} disabled{/if}>
					<label for="featured" class="checkbox_label text_5 text_black">Хит продаж <span class="filter_count">{if !$featured->checked}({$featured->count}){/if}</span></label>
				</li>
			</ul>
		</div>
		{* /Фильтр по статусам *}
		
		{* Фильтр по брендам *}
		{if $category->brands}
		<div class="filter_brand filter_item trigger_container{if $category->brands|count < 2} hidden{/if}">
			<div class="filter_item_title relative text_black">
				Бренды 
				<span class="filter_item_trigger absolute trigger fright icon icon_more active" data-trigger_action="slide">
					<span class="icon_more_line line_1"></span>
					<span class="icon_more_line line_2"></span>
				</span>
			</div>
			<div class="filter_item_content trigger_content">
				<ul class="filter_list{if $category->brands|count > 5} filter_list_large{/if}">
					{foreach $category->brands as $b}
						{if !$b->in_filter}
						<li class="filter_list_item{if $b->checked} filter_selected{elseif $b->count == 0} filter_disabled{/if}">
							<input id="brand_{$b->id}" class="hidden" type="checkbox" name="brand_id[]" value="{$b->id}"{if $b->checked} checked{elseif $b->disabled} disabled{/if}>
							<label for="brand_{$b->id}" class="checkbox_label text_5 text_black">{$b->name|escape} {if !$b->checked}<span class="filter_count">({$b->count})</span>{/if}</label>
						</li>
						{/if}
					{/foreach}
				</ul>
				{if $category->brands|count > 5}<span class="filter_showall link pseudo_link text_pink text_5">Показать все</a>{/if}
			</div>
		</div>
		{/if}	
		{* /Фильтр по брендам *}
		
		{* Фильтр по свойстам *}
		{if $features}
			{foreach $features as $f}
			<div class="filter_feature filter_item trigger_container{if $f->options|count < 2} hidden{/if}{if $filter->features[$f->id]->expand} active{/if}">
				<div class="filter_item_title relative text_black">
					{$f->name} 
					<span class="filter_item_trigger absolute trigger fright icon icon_more{if $filter->features[$f->id]->expand} active{/if}" data-trigger_action="slide">
						<span class="icon_more_line line_1"></span>
						<span class="icon_more_line line_2"></span>
					</span>
				</div>
				<div class="filter_item_content trigger_content"{if !$filter->features[$f->id]->expand} style="display: none;"{/if}>
                {*{if $f->mode == "range"}
                    надо будет сделать
                {/if}*}
                {if $f->mode == "select"}
        			<select name="{$f->id}[]}">
        				<option value="">Все</option>
        				{foreach $f->options as $k=>$o}
                           {if !$o->disabled}
        				   <option value="{$o->value}" {if $o->checked} selected{elseif $o->disabled} disabled{/if} >{$o->value|escape}{if !$o->checked}({$o->count}){/if}</option>
                           {/if}
        				{/foreach}
        			</select>		  
        		{/if}
                {if $f->mode == "checkbox" || !$f->mode}
					<ul class="filter_list{if $f->options|count > 5} filter_list_large{/if}">
						{foreach $f->options as $k=>$o}
						<li class="filter_list_item{if $o->checked} filter_selected{/if}">
							<input id="option_{$f->id}_{$k}" class="hidden" type="checkbox" name="{$f->id}[]" value="{$o->value|escape}"{if $o->checked} checked{elseif $o->disabled} disabled{/if}>
							<label for="option_{$f->id}_{$k}" class="checkbox_label text_5 text_black">{$o->value|escape} {if !$o->checked}<span class="filter_count">({$o->count})</span>{/if}</label>
						</li>
						{/foreach}
					</ul>
					{if $f->options|count > 5}<span class="filter_showall link pseudo_link text_pink text_5">Показать все</a>{/if}
                {/if}
                {if $f->mode == "radio"}
                   <ul class="filter_list{if $f->options|count > 5} filter_list_large{/if}">
                        {foreach $f->options as $k=>$o}
						<li class="filter_list_item{if $o->checked} filter_selected{/if}">
							<input id="option_{$f->id}_{$k}" class="hidden" type="radio" name="{$f->id}[]" value="{$o->value|escape}" {if $o->checked}checked{assign var="exists_selected" value=true}{elseif $o->disabled}disabled{/if}/>
							<label for="option_{$f->id}_{$k}" class="checkbox_label radio text_5 text_black">{$o->value|escape} {if !$o->checked}<span class="filter_count">({$o->count})</span>{/if}</label>
						</li>
				        {/foreach}
					</ul>
					{if $f->options|count > 5}<span class="filter_showall link pseudo_link text_pink text_5">Показать все</a>{/if}
                {/if}
                {if $f->mode == "logical"}
        			{$tag_id = 0}{$logical_value = ''}
        			{foreach $f->options as $k=>$o}
        				{if $o->value == "да"}{$tag_id = $o->value}{/if}
                        {if $o->checked}{$logical_value = $o->value}{/if}
        			{/foreach}
        			<ul class="filter_list">
        				<li class="filter_list_item">
        					<input id="logical_{$f->id}" class="hidden" type="checkbox" name="{$f->id}" value="{$tag_id}"{if $logical_value} checked{/if}/>
							<label for="logical_{$f->id}" class="checkbox_label text_5 text_black">{$f->name}</label>
        				</li>
        			</ul>
        		{/if} 
                </div>
            </div>     
			{/foreach}
		{/if}
		{* /Фильтр по свойстам *}
		
	</div>

	<div class="filter_submit absolute">
		<input class="button" type="submit" value="Применить">
	</div>
	<div class="filter_reset absolute">
		<a class="button button_transparent" href="catalog/{$category->url}{if $mark}/tag/{$mark->url}{/if}">Очистить все</a>
	</div>
</form>
{/strip}