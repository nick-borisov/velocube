{strip}
<div class="path text_bold text_gray3 hidden_xs hidden_sm">
	<span class="path_item path_item_first" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		<a class="path_link" itemprop="url" href="./" title="Вернуться на главную"><span itemprop="title">Главная</span></a>
	</span>
	
	{if $smarty.get.module == 'ProductView'}
		{foreach from=$category->path item=cat}
			<span class="path_item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a class="path_link" href="catalog/{$cat->url}" itemprop="url"><span itemprop="title">{$cat->name|escape}</span></a>
			</span>
		{/foreach}
		{if $brand}
			<span class="path_item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a class="path_link" href="catalog/{$cat->url}/{$brand->url}" itemprop="url"><span itemprop="title">{$brand->name|escape}</span></a>
			</span>
		{/if}
		<span class="path_item">
			{$product->fullname|escape}
		</span>
		
	{elseif $smarty.get.module == 'ProductsView'}
		{if $category} 
			{foreach from=$category->path item=cat}
				{* Делаем последний пункт не ссылкой *}
				{if !$brand && $cat@last}
					<span class="path_item">
						{$cat->name|escape}
					</span>
				{else}
					<span class="path_item" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
						<a class="path_link" itemprop="url" href="catalog/{$cat->url}"><span itemprop="title">{$cat->name|escape}</span></a>
					</span>
				{/if}
			{/foreach}
			{if $brand}
				<span class="path_item">{$brand->name|escape}</span>
			{/if}
		{elseif $brand}
			<span class="path_item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a class="path_link" href="brands/{$brand->url}" itemprop="url"><span itemprop="title">{$brand->name|escape}</span></a>
			</span>
		{elseif $keyword}
			<span class="path_item">Поиск</span>
		{elseif $mark}
			<span class="path_item">{$mark->name|escape}</span>
		{else}
			<span class="path_item">Все товары</span>
		{/if}
		
	{elseif $smarty.get.module == 'BlogView'}
		{if $post}
			<span class="path_item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a class="path_link" itemprop="url" href="blog"><span itemprop="title">Новости</span></a>
			</span>
			<span class="path_item">{$post->name|escape}</span>
		{else}
			<span class="path_item">Новости</span>
		{/if}
	{elseif $smarty.get.module == 'RegisterView'}
		<span class="path_item">Регистрация нового пользователя</span>
	{elseif $smarty.get.module == 'LoginView'}
		<span class="path_item">Вход и Регистрация</span>
	{elseif $smarty.get.module == 'UserView'}
		<span class="path_item">Личный кабинет</span>
	{elseif $smarty.get.module == 'CartView'}
		<span class="path_item">Корзина</span>
	{else}
		<span class="path_item">{$page->name|escape}</span>
	{/if}
</div>
{/strip}