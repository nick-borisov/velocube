{* Шаблон письма пользователю о заказе *}

{$subject = "Заказ №`$order->id`" scope=parent}
<h1 style="font-weight:normal;font-family:arial;">
	<a href="{$config->root_url}/order/{$order->url}">Ваш заказ №{$order->id}</a>
	на сумму {$order->total_price|convert:$currency->id}&nbsp;{$currency->sign}
	{if $order->paid == 1}оплачен{else}еще не оплачен{/if},
	{if $order->status == 0}ждет обработки{elseif $order->status == 1}в обработке{elseif $order->status == 2}выполнен{elseif $order->status == 4}
				отменен	
			{elseif $order->status == 5}
				проблемный{elseif $order->status == 3}
				удален{/if}	
</h1>
{if $order->status == 2}
<center>
<b>Благодарим за обращение в наш интернет-магазин!
<div style="text-align: center;"><b>Дорогой ПОКУПАТЕЛЬ!</b></div>
<div>&nbsp;</div>
<div>&nbsp; &nbsp;В целях повышения качества обслуживания клиентов наша компания проводит <b>АКЦИЮ</b>!</div>
<div>&nbsp; Опубликуйте отзыв на Яндекс-Маркете и получите <font color="#FF0000"><b>200 рублей</b></font> на номер мобильного телефона. Для того чтобы принять участие в акции, необходимо продублировать в текстовом формате отзыв и номер Заказа, на эл.почту velocube@yandex.ru.</div>
<div>&nbsp; В письме укажите также номер мобильного телефона и наименование любого из приобретенных по данному заказу товара. Просим Вас отнестись с пониманием к тому, что акция является <b>разовой</b>, а сам отзыв должен соответствовать правилам публикации на Яндекс-Маркете.</div>
<div>Спасибо за покупку и удачного Вам дня!</div>
<div>Команда Велокуб.</div>
 <a href="http://market.yandex.ru/shop/219626/reviews/add?retpath=http%3A%2F%2Fmarket.yandex.ru%2Fshop%2F219626%2Freviews">Оставить отзыв</a><br>
Спасибо! Команда Velocube.ru
 </b>
</center>
{/if}
<table cellpadding="6" cellspacing="0" style="border-collapse: collapse;">
	<tr>
		<td style="padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Статус
		</td>
		<td style="padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{if $order->status == 0}
				ждет обработки      
			{elseif $order->status == 1}
				в обработке
			{elseif $order->status == 2}
				выполнен
			{elseif $order->status == 4}
				<font color="#FF0000"><b>отменен</b></font>	
			{elseif $order->status == 5}
				проблемный
			{elseif $order->status == 3}
				удален										
			{/if}
		</td>
	</tr>
	<tr>
		<td style="padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Оплата
		</td>
		<td style="padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{if $order->paid == 1}
			<font color="green">оплачен</font>
			{else}
			не оплачен
			{/if}
		</td>
	</tr>
	{if $order->name}
	<tr>
		<td style="padding:6px; width:170; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Имя, фамилия
		</td>
		<td style="padding:6px; width:330; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->name|escape}
		</td>
	</tr>
	{/if}
	{if $order->email}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Email
		</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->email|escape}
		</td>
	</tr>
	{/if}
	{if $order->phone}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Телефон
		</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			+{$order->phone|escape}{if $order->phone2}, +{$order->phone2|escape}{/if}
		</td>
	</tr>
	{/if}
	{if $order->address}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Адрес доставки
		</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->address|escape}
		</td>
	</tr>
	{/if}
	{if $order->comment}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Комментарий
		</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->comment|escape|nl2br}
		</td>
	</tr>
	{/if}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Дата
		</td>
		<td style="padding:6px; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->date|date} {$order->date|time}
		</td>
	</tr>
	{if $order->delivery_date && $order->delivery_date !='1970-01-01' && $order->delivery_date != '0000-00-00'}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Дата доставки
		</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->delivery_date}
			{if $order->delivery_time}
			<br />{$order->delivery_time}
			{/if}
		</td>
	</tr>
	{/if}
	{if $order->trnomer}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">Трек-номер</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">{$order->trnomer}</td>
	</tr>
	{/if}
    {if $order->manager}
	<tr>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">Ваш менеджер</td>
		<td style="padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">{$order->manager}</td>
	</tr>
	{/if}
	
	
	
</table>

<h1 style="font-weight:normal;font-family:arial;">Вы заказали:</h1>

<table cellpadding="6" cellspacing="0" style="border-collapse: collapse;">

	{foreach name=purchases from=$purchases item=purchase}
	<tr>
		<td align="center" style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$image = $purchase->product->images[0]}
			<a href="{$config->root_url}/products/{$purchase->product->url}"><img border="0" src="{$image->filename|resize:50:50}"></a>
		</td>
		<td style="padding:6px; width:250; padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			<a href="{$config->root_url}/products/{$purchase->product->url}">{$purchase->product_name}</a>
			{$purchase->variant_name}
			{if $order->paid && $purchase->variant->attachment}
			<br>
			<a href="{$config->root_url}/order/{$order->url}/{$purchase->variant->attachment}"><font color="green">Скачать {$purchase->variant->attachment}</font></a>
			{/if}
		</td>
		<td align=right style="padding:6px; text-align:right; width:150; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$purchase->amount} {$settings->units} &times; {$purchase->price|convert:$currency->id}&nbsp;{$currency->sign}
		</td>
	</tr>
	{/foreach}
	
	{if $order->discount}
	<tr>
		<td style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;"></td>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Скидка
		</td>
		<td align=right style="padding:6px; text-align:right; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->discount}&nbsp;%
		</td>
	</tr>
	{/if}

	{if $order->coupon_discount>0}
	<tr>
		<td style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;"></td>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			Купон {$order->coupon_code}
		</td>
		<td align=right style="padding:6px; text-align:right; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			&minus;{$order->coupon_discount}&nbsp;{$currency->sign}
		</td>
	</tr>
	{/if}

	{if $delivery && !$order->separate_delivery}
	<tr>
		<td style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;"></td>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;">
			{$delivery->name}
		</td>
		<td align="right" style="padding:6px; text-align:right; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;">
			{$order->delivery_price|convert:$currency->id}&nbsp;{$currency->sign}
		</td>
	</tr>
	{/if}
	
	<tr>
		<td style="padding:6px; width:100; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;"></td>
		<td style="padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;font-weight:bold;">
			Итого
		</td>
		<td align="right" style="padding:6px; text-align:right; width:170; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;font-weight:bold;">
			{$order->total_price|convert:$currency->id}&nbsp;{$currency->sign}
		</td>
	</tr>
</table>

<br>
Вы всегда можете проверить состояние заказа по ссылке:<br>
<a href="{$config->root_url}/order/{$order->url}">{$config->root_url}/order/{$order->url}</a>
<br>