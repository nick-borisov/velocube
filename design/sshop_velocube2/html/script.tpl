{strip}
{*Кнопка загрузить больше продуктов*}
{if $smarty.get.module == 'ProductsView'} 
{literal}
	<script>   
    $(document).on('click', '#products_load_more', function(){
        var page = $(this).data('page'),
			url = '{/literal}{url}{literal}',
			button = $(this),
			products = $('#products');
            
            if(url.indexOf('?')==-1){
                url += '?page='+page+'&load_more=1';
            }else{
                url += '&page='+page+'&load_more=1';
            }
        button.addClass('active');
        products.addClass('loading');
        $.ajax({
            'method': 'GET',
            'url': url,
            'dataType': 'html',
            'success': function(response){
                button.removeClass('active');
                products.removeClass('loading');
                $('.products_content').html(response);
				/*Fallback для вида плитка/списка*/
				$('.change_view_icon.active').trigger('click');
            }
        });
    });
	</script>
{/literal}
{/if}

{*Виджет группы VK*}
{*if $smarty.get.module == 'MainView'}
	<script src="http://vk.com/js/api/openapi.js"></script>
	{literal}
		<script>
			VK.Widgets.Group("vk_groups", {mode: 0, width: "auto", height: "350", color1: 'FFFFFF', color2: '356cd2', color3: '356cd2'}, 66539861);
		</script>
	{/literal}
{/if*}

{*social-registration*}
{if $smarty.get.module == 'LoginView' || $smarty.get.module == 'RegisterView'}
<script src="//ulogin.ru/js/ulogin.js"></script>
{literal}
	<script>
		function get_ulogin(token)	{
			$.ajax({
				url: 'http://ulogin.ru/token.php',
				type: 'GET',
				dataType:'jsonp',
				async:false,
				data: {'token': token, 'host': encodeURIComponent({/literal}'{$smarty.server.SERVER_NAME}'{literal})},
				success: function (data)
				{
					var user_data = $.parseJSON(data);
					$("#ulogin_first_name").val(user_data.first_name);
					$("#ulogin_last_name").val(user_data.last_name);
					$("#ulogin_email").val(user_data.email);
					$("#ulogin_phone").val(user_data.phone);
					$("#ulogin_password").val(user_data.uid);
					document.ulogin_form.submit();
				}
			});
		}
	</script>
{/literal}
{/if}
{*/social-registration*}

{if $smarty.get.module == 'ProductsView'}
	<script src="design/{$settings->theme}/js/jquery.ui-slider.js"></script>
	{literal}
	<script>
	!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);
	</script>
	{/literal}
	{literal}
		<script>
			if( $("#slider_price").length ){
		
			var sliderRangeMin = $('#min_price').data('min_price'),
				sliderRangeMax = $('#max_price').data('max_price'),
				sliderCurrentMin = $('#min_price').val(),
				sliderCurrentMax = $('#max_price').val();
				
			$("#slider_price").slider({
				range: true,
				min: sliderRangeMin,
				max: sliderRangeMax,
				values: [sliderCurrentMin, sliderCurrentMax],
				slide: function( event, ui ) {
					/*
					$( "#pr_min" ).html( ui.values[ 0 ] );
					$( "#pr_max" ).html( ui.values[ 1 ] );
					*/
					$( "#min_price" ).val( ui.values[ 0 ] );
					$( "#max_price" ).val( ui.values[ 1 ] );
				}
			});
			//$('#slider_price').draggable();
		}
		</script>
	{/literal}
	{*<script src="design/{$settings->theme}/js/filter.min.js"></script>*}
	{*literal}
	<script>
		$(window).load(function(){
			$('.nonselected').click(function(e){
			   // $(this).toggle();
			   // $(this).next().toggle();
			});
		/*	
		var cat = $('#f_cat').val();   
		var mark = $('#f_mark').val(); 
		var brand = $('#f_brand').val(); 

		   $.ajax({
				type: 'POST',
				url: "ajax/filter.php",
				data: {cat : cat, mark : mark, brand : brand},
				dataType: 'json',
				beforeSend:function(){$('#filter').html('<div class="loading"></div>');},
				success: function(data){
					$('#filter').html(data);
					
				}
			});
		  });

		 $('.apply').live('click', function() {
			   var form_data = $('#form_filter').serializeArray();
				$.ajax({
				type: 'POST',
				url: "ajax/update_products.php",
				data: form_data,
				dataType: 'json',
				success: function(data){
					$('#prods').html(data);
					$('.popover').hide();
					
				}
			});
				return false
		});
		
		$(document).ready(function(){
			var cat = $('#f_cat').val();   
			var mark = $('#f_mark').val(); 
			var brand = $('#f_brand').val(); 
			var word = $('#f_word').val(); 
					$.ajax({
					type: 'POST',
					url: "ajax/update_products.php",
					data: { brand_id:brand, cat_id:cat, mark_id:mark, keyword:word },
					dataType: 'json',
					success: function(data){
						$('#prods').html(data);
						$('.popover').hide();
						
					}
				});
			});
		*/
	</script>
	
	<script >
		function show_hide (id) {
			if (document.getElementById("hidden_" + id).style.display == "none") {
				document.getElementById("hidden_" + id).style.display = "block";
				document.getElementById("lnk_" + id).innerHTML = "популярные";
			} else {
				document.getElementById("hidden_" + id).style.display = "none";
				document.getElementById("lnk_" + id).innerHTML = "все";
				document.getElementById("hiddens_" + id).style.display = "none";
			}
		}

		function show_hides (id) {
			if (document.getElementById.style.display == "none") {
				document.getElementById("hiddens_" + id).style.display = "block";
			} else {
				document.getElementById("hiddens_" + id).style.display = "none";
			}
		}


		function setTable(what){
		if(document.getElementById(what).style.display=="none"){
		document.getElementById(what).style.display="block";
		document.getElementById("i" + what).className = "arr";
		}
		else if(document.getElementById(what).style.display=="block"){
		document.getElementById(what).style.display="none";
		document.getElementById("i" + what).className = "arr off";

		}
		} 
		</script>
	{/literal*}
{/if}

{/strip}

{if $smarty.get.module == 'ProductView'}
{literal}            
<script>
var holidays = [{/literal}{$settings->calendar_days}{literal}];
console.log(holidays);

function clock() {
    var d = new Date();

    month=new Array("января", "февраля", "марта", "апреля", "мая", "июня",
    "июля", "августа", "сентября", "октября", "ноября", "декабря");
    days=new Array("воскресенье", "понедельник", "вторник", "среда",
    "четверг", "пятница", "суббота");
    
    var new_date = d.getTime()+(24+6)*60*60*1000;/*заказ после 18:00 переходит на следующий день, т.е  + 6 часов*/
    var d_of_w_t = new Date(new_date);
    d_of_w_t = d_of_w_t.getDay();
    
    if(d_of_w_t>6){
        d_of_w_t = d_of_w_t-6; 
        new_date = d.getTime()+(48+6)*60*60*1000;
    }else if(d_of_w_t==0){
        d_of_w_t = 1;
        new_date = d.getTime()+(72+6)*60*60*1000;
    }
    
    var dd = new Date(new_date);
        
    var t_day = dd.getDate();
    var t_month = dd.getMonth()+1;
    
    if (t_day <= 9) t_day = "0" + t_day;
    if (t_month <= 9) t_month = "0" + t_month;
    
    console.log('d_of_w_t '+d_of_w_t);
    
    /* --------- */
    
    var new_date_t_osn = d.getTime()+(24+6)*60*60*1000; 
       
    var d_of_w_t_osn = get_day_osn(new_date_t_osn);/*new Date(new_date_t_osn);*/
    
    var i=1;
    var day_of_week = dayOfWeek(new_date_t_osn);
    console.log('xx'+new_date_t_osn);
    while($.inArray(get_format_date_osn(new_date_t_osn), holidays)!=-1){
        new_date_t_osn = d.getTime()+((24*i)+6)*60*60*1000; 
        get_day_osn(new_date_t_osn);
        console.log('new_date_t_osn--'+new_date_t_osn);
        i++;
    }
    
    /*console.log(d_d_date);*/
    
    /* --------- */
    
    var new_date_at_osn = d.getTime()+(48+6)*60*60*1000; 
       
    var d_of_w_at_osn = get_afday_osn(new_date_at_osn);//new Date(new_date_t_osn);
    
    var i=1;
    var day_of_week = dayOfWeek(new_date_at_osn);
    console.log('yy'+new_date_at_osn);
    while($.inArray(get_format_date_osn(new_date_at_osn), holidays)!=-1){
        new_date_at_osn = d.getTime()+((24*i)+6)*60*60*1000; 
        get_afday_osn(new_date_at_osn);
        console.log('new_date_at_osn--'+new_date_at_osn);
        i++;
    }
	
	/* --------- */
    
    var new_date_at_osn = d.getTime()+(48+6)*60*60*1000;    
    var d_of_w_at_osn = new Date(new_date_at_osn);
        d_of_w_at_osn = d_of_w_at_osn.getDay();
        
    if(d_of_w_at_osn>5){
        var div_at = 9 - d_of_w_at_osn;
        d_of_w_at_osn = 2;
        new_date_at_osn = d.getTime()+((24*div_at)+6)*60*60*1000;    
    }else if(!d_of_w_at_osn){
        d_of_w_at_osn = 2;
        new_date_at_osn = d.getTime()+((24*4)+6)*60*60*1000;
    }

    var dd_at = new Date(new_date_at_osn);
        
    var at_day = dd_at.getDate();
    var at_month = dd_at.getMonth()+1;

    if (at_day <= 9) at_day = "0" + at_day;
    if (at_month <= 9) at_month = "0" + at_month;
    
    date_tom = days[d_of_w_t] + ', ' + t_day + '.' + t_month;
    
    date_aftom = days[d_of_w_at_osn] + ', ' + at_day + '.' + at_month;
    
    console.log('d_of_w_t_osn '+d_of_w_t_osn+' '+'date_tom_osn '+date_tom_osn);
    {/literal}
    var paththeme = '/design/{$settings->theme}/';
    /*console.log(paththeme);*/
    {literal}
    jQuery("span#tomorrow").html('<img src="'+paththeme+'images/product_delivery_'+d_of_w_t+'.png" width="78px" title="'+date_tom+'"/>');
    jQuery("span#tomorrow_osn").html('<img src="'+paththeme+'images/product_delivery_'+get_day_osn(new_date_t_osn)+'.png" width="78px"  title="'+date_tom_osn+'"/>');
    jQuery("span#aftertomorrow_osn").html('<img src="'+paththeme+'images/product_delivery_'+get_afday_osn(new_date_at_osn)+'.png" width="78px"  title="'+date_aftom+'"/>');

}

function get_day_osn(time){
    var d_of_w_t_osn = new Date(time);
        d_of_w_t_osn = d_of_w_t_osn.getDay();
        
    if(d_of_w_t_osn>5){
        var div_t = 7 - d_of_w_t_osn;
        d_of_w_t_osn = 1;
        time = get_time_osn(time, div_t);/*d.getTime()+((24*div_t)+6)*60*60*1000;*/
    }else if(!d_of_w_t_osn){
        d_of_w_t_osn = 1;
        time = get_time_osn(time, 4);/*d.getTime()+((24*4)+6)*60*60*1000;*/
    }
    /*var DATE = get_format_date_osn(time);*/
    return d_of_w_t_osn;
}
function get_afday_osn(time){
    var d_of_w_t_osn = new Date(time);
        d_of_w_t_osn = d_of_w_t_osn.getDay();
        
    if(d_of_w_t_osn>5){
        var div_t = 7 - d_of_w_t_osn;
        d_of_w_t_osn = 1;
        time = get_time_osn(time, div_t);/*d.getTime()+((24*div_t)+6)*60*60*1000;*/
    }else if(!d_of_w_t_osn){
        d_of_w_t_osn = 1;
        time = get_time_osn(time, 4);/*d.getTime()+((24*4)+6)*60*60*1000;*/
    }
    /*var DATE = get_format_date_osn(time);*/
    return d_of_w_t_osn;
}

function get_time_osn(time, div){
    return new_date_t_osn = time+((24*div)+6)*60*60*1000;  
}
function get_format_date_osn(time){
    var d_d = new Date(time);
        
    var t_day_osn = d_d.getDate();
    var t_month_osn = d_d.getMonth()+1;
    
    if (t_day_osn <= 9) t_day_osn = "0" + t_day_osn;
    if (t_month_osn <= 9) t_month_osn = "0" + t_month_osn;
    return date_tom_osn = t_day_osn+'.'+t_month_osn+'.'+d_d.getFullYear();
}
function dayOfWeek(time){
    var d1 = new Date(time);
    return d1.getDate();
}
clock();
</script>       
{/literal}  
{/if}

{if $smarty.get.module == 'CartView'}
{literal}
<script>
	/*order_on_one_page*/
	$(document).ready(function(){
		$("#deliveries .active input[type=radio]").click();
		
		$(document).on('change', '.bill_list input[type=radio]', function(){
			if( $(this).closest('.bill_item').hasClass('active') ){
				return true;
			}
			$(this).closest('.bill_list').find('.bill_item').removeClass('active');
			$(this).closest('.bill_item').addClass('active');
			$(this).closest('.bill_list').find('.bill_description').slideUp();
			$(this).closest('.bill_item').find('.bill_description').slideDown();
		});
		
		$('#cart_fast_order_link').click(function(){   
			var variant;
			var form_variants = $(this).closest("form[name='cart']").find("input[name*='amount']");
			//console.log(form_obj);
			form_variants.each(function(i, elem){
				//console.log(elem); 
				$("#for_export_inputs").append(elem);
			});
            var form_auto_coupons = $(this).closest("form[name='cart']").find("input[name*='auto_coupon']");
            form_auto_coupons.each(function(i, elem){
				$("#for_export_inputs").append(elem);
			});
		}); 
	});    

	function change_payment_method($id) {
		$("#payment_"+$id+" .active input[type=radio]").attr('checked','checked');   
		$("#payments .bill_list").addClass('hidden');
		$("#payment_"+$id).removeClass('hidden');
	}
	/*/order_on_one_page*/
	
	/* Очищаем поле второго телефона, при его отмене */
	$(document).on('click', '#second_phone_link.active', function(){
		$('#cart_second_phone').val('');
	});
	
	$("input[name='coupon_code']").keypress(function(event){
		if(event.keyCode == 13){
			$("input[name='name']").attr('data-format', '');
			$("input[name='email']").attr('data-format', '');
			$("input[name='phone']").attr('data-format', '');
			document.cart.submit();
		}
	});
	if ( $('.coupon_form').find('.notice').length ) {
		$('.coupon_form').show();
	}

</script>
{/literal}
{/if}

{if $smarty.get.module == 'OrderView'}
{literal}
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
var yaParams = {
order_id: "{/literal}{$order->id}{literal}",
order_price: {/literal}{$order->total_price}{literal}, 
currency: "RUR",
exchange_rate: 1,
goods: 
[
{/literal}{foreach $purchases as $purchase}{literal}
{
name: "{/literal}{$purchase->product_name|escape}{literal}", 
price: {/literal}{$purchase->price}{literal},
quantity: {/literal}{$purchase->amount}{literal}
} {/literal}
{if $purchase@last} {else},{/if}
{/foreach}{literal}
]
};
</script>



<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter21197344 = new Ya.Metrika({id:21197344,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,params:window.yaParams||{ }});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
{/literal}
{/if}