{* Страница заказа *}

{$meta_title = "Ваш заказ №`$order->id`" scope=parent}

<main class="main shadow_section">
	<div class="container">

		<section class="order_notice_section shadow_box order_section">
			<div class="order_section_inner">
				<h1 class="title">Ваш заказ №{$order->id} 
					{if $order->paid}<span class="text_green">оплачен</span>,{/if}
					{if $order->status == 0}ждет обработки
					{elseif $order->status == 1}находится в обработке
					{elseif $order->status == 2}выполнен
					{elseif $order->status == 4}<span class="text_red">отменен</span>
					{elseif $order->status == 5}<span class="text_red">проблемный</span>
					{elseif $order->status == 3}<span class="text_red">удален</span>{/if}	
				</h1>
				{*if $order->paid}
					<div class="text_center text_2">
					<span class="text_label text_label_success">оплачен</span>
					</div>
				{/if*}
				{if $order->status == 0}
					<div class="order_info text_gray">
						Уважаемый(ая) {$order->name|escape}, благодарим за ваш заказ.<br>
						В скором времени наш менеджер свяжется с Вами по телефону +{$order->phone|escape} для уточнения деталей. 
					</div>
				{/if}
			</div>
		</section>
		
		<div class="flex flex_wrap row">

			{if !$order->isfastorder}
			<section class="order_payment_section col_xs_12 col_md_6 flex_item">
				<div class="order_section shadow_box">
					<div class="order_section_inner">
					
						{* Изменение способа оплаты *}
						{if $payment_methods && !$payment_method && $order->total_price>0}	
							<h2 class="title">Как будете оплачивать?</h2>
							<form class="change_payment_form" method="post">
								{foreach $payment_methods as $payment_method}
									<div class="form_group">
										<input class="hidden" type="radio" name="payment_method_id" value="{$payment_method->id}" {if $payment_method@first}checked{/if} id="payment_{$payment_method->id}">
										<label class="checkbox_label radio" for="payment_{$payment_method->id}">{$payment_method->name}</label>
									</div>
								{/foreach}
								<input type="submit" class="button button_block button_large" value="Применить">
							</form>	
							
						{* Выбранный способ оплаты *}
						{elseif !$order->paid}
							<h2 class="title">Способ оплаты &ndash; {$payment_method->name}</h2>
							
							{if $payment_method->description}<div class="order_info text_gray">{$payment_method->description}</div>{/if}
							{if !$order->paid}
								<form class="change_payment_form" method="post"><input type="submit" class="link text_green text_5" name="reset_payment_method" value="Изменить способ оплаты"></form>
							{/if}
							
							<div class="order_payment_price text_bold">
								<div class="text_5 text_gray2 uppercase">К оплате:</div>
								<div class="price text_1 text_black">{$order->total_price|convert} <span class="currency">{$currency->sign}</span></div>
							</div>
							
							{* Выбранный способ оплаты *}
							{if $payment_method}
                                {if ($payment_method->allow_to_pay && $order->allow_to_pay) || !$payment_method->allow_to_pay}
								<div class="checkout_form">
									{* Форма оплаты, генерируется модулем оплаты *}
									{checkout_form order_id=$order->id module=$payment_method->module}
								</div>
                                {else}
                                <div class="selected-payment-text">
                                    <div class="alert alert-warning">Для оплаты заказа подтвердите заказ у менеджера</div>
                                </div>
                                {/if}
							{/if}
                        {elseif $order->paid}
							<h2 class="title">Способ оплаты &ndash; {$payment_method->name}</h2>
							{if $payment_method->description}<div class="order_info text_gray">{$payment_method->description}</div>{/if}
							<div class="order_payment_price text_bold">
								<div class="text_5 text_gray2 uppercase">Сумма заказа:</div>
								<div class="price text_1 text_black">{$order->total_price|convert} <span class="currency">{$currency->sign}</span></div>
							</div>
						{/if}
						
					</div>
				</div>
			</section>
			
			<section class="order_delivery_section col_xs_12 col_md_6 flex_item">
				<div class="shadow_box order_section">
					<div class="order_section_inner">
						<h2 class="title">Способ доставки &ndash; {$delivery->name}</h2>
						{if $delivery->description}<div class="order_info text_gray">{$delivery->description}</div>{/if}
					</div>
				</div>
			</section>
			{/if}

			<section class="order_detail col_xs_12 col_md_6 flex_item">
				<div class="order_section shadow_box">
				
					<div class="order_section_inner">
						<h2 class="title">Детали заказа</h2>
						<ul class="order_detail_list text_5">
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Дата заказа:</div>
								<div class="order_detail_value">{$order->date|date} в {$order->date|time}</div>
							</li>
							{if $order->name}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Имя{if $order->surname|escape}, Фамилия{/if}:</div>
								<div class="order_detail_value">{$order->name|escape}{if $order->surname|escape}, {$order->surname|escape}{/if}</div>
							</li>
							{/if}
							{if $order->email && !$order->isfastorder}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Адрес электронной почты:</div>
								<div class="order_detail_value">{$order->email|escape}</div>
							</li>
							{/if}
							{if $order->phone}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Телефон:</div>
								<div class="order_detail_value">+{$order->phone|escape}{if $order->phone2}, +{$order->phone2|escape}{/if}</div>
							</li>
							{/if}
							{if $order->address && !$order->isfastorder}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Адрес доставки:</div>
								<div class="order_detail_value">{$order->address|escape}</div>
							</li>
							{/if}
							{if $order->manager}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Ваш менеджер:</div>
								<div class="order_detail_value">{$order->manager}</div>
							</li>
							{/if}
							{if $order->delivery_date && !$order->isfastorder}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Дата доставки:</div>
								<div class="order_detail_value">{$order->delivery_date}{if $order->delivery_time} {$order->delivery_time}{/if}</div>
							</li>
							{/if}
							{if $order->trnomer}
							<li class="order_detail_item">
								<div class="order_detail_name text_gray2">Трек-номер:</div>
								<div class="order_detail_value">{$order->trnomer}</div>
							</li>
							{/if}
							{if $order->comment}
							<div class="order_detail_item">
								<div class="order_detail_name text_gray2">Комментарий:</div>
								<div class="order_detail_value">{$order->comment|escape|nl2br}</div>
							</div>
							{/if}
						</ul>
					</div>
					
				</div>
			</section>

			{* Список покупок *}
			<section class="order_product_section col_md_6 flex_item">
				<div class="order_section shadow_box">
					<div class="order_section_inner">
			
						<h2 class="title">Заказанные товары</h2>
						
						{foreach $purchases as $purchase}
							<div class="order_product_item shadow_box_item relative flex flex_wrap flex_align_center">
							
								{* Изображение товара *}
								<div class="cart_product_image flex_item col_xs_6 col_sm_3 hidden_md text_center">
									{$image = $purchase->product->images|first}
									{if $image}
									<a href="products/{$purchase->product->url}"><img src="{$image->filename|resize:130:90}" alt="{$purchase->product->name|escape}"></a>
									{/if}
								</div>
									
								{* Название товара *}
								<div class="cart_product_name flex_item col_xs_6 col_sm_5 col_md_6 col_lg_4 text_black">
									<a href="products/{$purchase->product->url}" class="text_bold">{$purchase->product->fullname|escape}</a>
									{if $purchase->variant->name}
										<div class="text_6 text_blue">{$purchase->variant->name|escape}</div>
									{/if}
								</div>
								
								{* Количество *}
								<div class="cart_product_amount flex_item col_xs_6 col_sm_2 col_md_3 col_lg_2 text_nowrap text_5">
									x{$purchase->amount} {$settings->units}
								</div>
                                
								{* Цена *}
								<div class="cart_product_price flex_item col_xs_6 col_sm_2 col_md_3 col_lg_3 text_right">
									<span class="price text_3 text_black text_bold">
                                    {if $purchase->auto_coupon}
                                    
                                        {(($purchase->price - $purchase->coupon_discount_price - ($purchase->dirty_price * $order->discount/100))*$purchase->amount)|convert}
                                    {else}
                                        {$purchase_price = 0}
                                        {if $order->discount_type == 1}
                                            {$purchase_price = (($purchase->price - ($purchase->price * $order->discount/100))*$purchase->amount)}
                                        {elseif $order->discount_type == 2}
                                            {$purchase_price = (($purchase->price - $order->discount)*$purchase->amount)}
                                        {else}
                                            {$purchase_price = $purchase->price}
                                        {/if}
                                    
                                        {$purchase_price|convert}
                                    {/if}
                                     <span class="currency">{$currency->sign}</span></span>
								</div>
											
							</div>
						{/foreach}
							
						{assign 'total_price_clear' $order->total_price+$order->coupon_discount+$group_discount}
						{if $order->coupon_discount>0}
							<div class="order_total shadow_box_item row text_5 text_bold text_gray2">
								<div class="col_xs_6 uppercase">Скидка по купону:</div>
								<div class="col_xs_6 text_right">&minus;{$order->coupon_discount|convert}&nbsp;{$currency->sign}</div>
							</div>
						{/if}	
						{* Если стоимость доставки входит в сумму заказа *}
						{if !$order->separate_delivery && $order->delivery_price>0}
							<div class="order_total shadow_box_item row text_5 text_bold text_gray2 ">
								<div class="col_xs_6 uppercase">{$delivery->name|escape}:</div>
								<div class="col_xs_6 text_right">{$order->delivery_price|convert}&nbsp;{$currency->sign}</div>
							</div>
						{/if}
						<div class="order_total shadow_box_item row text_5 text_bold text_gray2 ">
							<div class="col_xs_6 uppercase">Итого:</div>
							<div class="col_xs_6 text_right">{$order->total_price|convert}&nbsp;{$currency->sign}</div>
						</div>	
						{* Если стоимость доставки не входит в сумму заказа *}
						{if $order->separate_delivery}
							<div class="order_total shadow_box_item row text_5 text_bold text_gray2">
								<div class="col_xs_6 uppercase">{$delivery->name|escape}</div>
								<div class="col_xs_6 text_right">{$order->delivery_price|convert}&nbsp;{$currency->sign}</div>
							</div>
						{/if}
						
					</div>

				</div>				
			</section>

		</div>
		
	</div>
</main>










{* Скидка, если есть 
{if $order->discount > 0}
<tr>
	<th class="image"></th>
	<th class="name">скидка</th>
	<th class="price"></th>
	<th class="amount"></th>
	<th class="price">
		{$order->discount}&nbsp;%
	</th>
</tr>
{/if}
*}
{* Купон, если есть 
{if $order->coupon_discount > 0}
<tr>
	<th class="image"></th>
	<th class="name">купон</th>
	<th class="price"></th>
	<th class="amount"></th>
	<th class="price">
		&minus;{$order->coupon_discount|convert}&nbsp;{$currency->sign}
	</th>
</tr>
{/if}
*}
{* Если стоимость доставки входит в сумму заказа 
{if !$order->separate_delivery && $order->delivery_price>0}
<tr>
	<td class="image"></td>
	<td class="name">{$delivery->name|escape}</td>
	<td class="price"></td>
	<td class="amount"></td>
	<td class="price">
		{$order->delivery_price|convert}&nbsp;{$currency->sign}
	</td>
</tr>
{/if}
*}
{* Итого 
<tr>
	<th class="image"></th>
	<th class="name">итого</th>
	<th class="price"></th>
	<th class="amount"></th>
	<th class="price">
		{$order->total_price|convert}&nbsp;{$currency->sign}
	</th>
</tr>
*}
{* Если стоимость доставки не входит в сумму заказа 
{if $order->separate_delivery}
<tr>
	<td class="image"></td>
	<td class="name">{$delivery->name|escape}</td>
	<td class="price"></td>
	<td class="amount"></td>
	<td class="price">
		{$order->delivery_price|convert}&nbsp;{$currency->sign}
	</td>
</tr>
{/if}
*}