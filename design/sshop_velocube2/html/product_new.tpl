<div class="product_main col_xs_12 col_md_8">
	<div class="shadow_box">

		<div class="product_toolbar">
			<h1 class="product_name text_4 text_black" data-product="{$product->id}" itemprop="name" data-product_name="{$product->fullname|escape}">
				<span class="product_prefix text_6 uppercase">{$product->type_prefix|escape}</span>
				<span class="text_2 text_bold">{$brand->name|escape} {$product->name|escape}</span>
			</h1>

			<div class="rating inline_block_middle" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
				
				<span class="rating_starOff">
					<span class="rating_starOn" style="width:{if $product->rating > 0}{$product->rating*85/5|string_format:'%.0f'}{else}0{/if}px"></span>
				</span>
				
				<span class="rating_text text_5 text_gray2">
					{if $product->rating > 0}
						Рейтинг: <span class="rating_value text_bold text_gray">{$product->rating}</span> 
						&nbsp;/&nbsp;
						<a id="show_review_link" href="#product_comment" class="link pseudo_link text_blue anchor_link">{$product->comcon|string_format:"%.0f"} {$product->comcon|plural:'отзыв':'отзывов':'отзыва'}</a>
					{else}
						<a id="show_review_link" href="#product_comment" class="link pseudo_link text_blue anchor_link">Оставить отзыв</a>
					{/if}
				</span>

				<meta itemprop="ratingValue" content="{$product->rating}" />
				<meta itemprop="reviewCount" content="{$product->votes}" />
			</div>
		</div>
		
		
		<div id="product_gallery" class="product_gallery product_image text_center" data-product_image="{$product->image->filename|resize:200:200}">
		
			{* Прикрепленные видео к товару *}
			{foreach $product->videos as $video}
				<div class="product_gallery_item" data-thumb="design/{$settings->theme|escape}/images/icon_video.svg">
					<div class="product_gallery_video relative">
						<iframe class="absolute" src="https://www.youtube.com/embed/{$video->vid}" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			{/foreach}
			
			{* Галерея изображений *}
			{foreach $product->images as $i=>$image}
				<div class="product_gallery_item{*if $product->videos|count > 0 && $image@first} product_gallery_first{/if*}" data-thumb="{$image->filename|resize:60:40}">
					<a class="fancybox_gallery" href="{$image->filename|resize:800:600}" data-fancybox-group="gallery">
						<img src="{$image->filename|resize:600:400}" alt="{$product->fullname|escape}" />
					</a>
				</div>
			{/foreach}
			
		</div>
		
		<div class="product_service visible_lg_flex row text_6 text_gray2">
			<div class="product_service_item flex flex_align_center">
				<i class="icon product_service_icon">{include file="svg.tpl" svgId="icon_free_delivery" width="40px" height="40px"}</i>
				<span class="icon_text product_service_text">
					Закажите на сумму от&nbsp;
					<span class="product_service_note text_bold text_nowrap"><span class="text_2">{$settings->interval_price}</span> рублей</span>&nbsp;
					и мы доставим бесплатно!
				</span>
			</div>
			<div class="product_service_item flex flex_align_center">
				<i class="icon product_service_icon">{include file="svg.tpl" svgId="icon_clock" width="40px" height="40px"}</i>
				<span class="icon_text product_service_text">
					Подтвердим ваш заказ в рабочее время в течение&nbsp;
					<span class="product_service_note text_2 text_bold text_nowrap">10 минут !</span>
				</span>
			</div>
			<div class="product_service_item flex flex_align_center">
				<i class="icon product_service_icon">{include file="svg.tpl" svgId="icon_gift" width="40px" height="40px"}</i>
				<span class="icon_text product_service_text">
					Оформите заказ через сайт и мы сделаем для Вас&nbsp;
					<span class="product_service_note text_2 text_bold text_nowrap">Скидку!</span>
				</span>
			</div>
		</div>

	</div>
</div>

<div class="product_info col_xs_12 col_md_4">
	<form class="variants shadow_box" action="/cart" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	
		<meta itemprop="acceptedPaymentMethod" content="CreditCard" />
		<meta itemprop="priceCurrency" content="RUB" />
		<meta itemprop="availability" content="http://schema.org/InStock" />
		
		<div class="shadow_box_item">
		
			<div id="product_stock" class="product_stock inline_block_middle text_bold text_6 uppercase">
				{if $product->variant->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
				{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span> 
				{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span> 
				{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается {if $product->byrequest}( {$product->byrequest} ){/if} 
				{else}<span class="text_blue">В наличии</span> 
				{/if}
			</div>
				
			<span class="compare_link inline_block_middle text_5 text_gray2 text_right">
				{if $compare->ids && in_array($product->id, $compare->ids)}
					<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
					<span class="compare_link_text inline_block_middle">в сравнении</span>
				{else}
					<a href="/compare?id={$product->id}" class="compare_add" data-id="{$product->id}" data-key="compare" data-informer="1" data-result-text="в сравнении">
						<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
						<span class="compare_link_text inline_block_middle">сравнить</span>
					</a>
				{/if}
			</span>
			
		</div>
		
		<div class="price_container shadow_box_item text_bold{if $product->variant->price == 0} hidden{/if}">

			<div class="old_price text_gray3{if $product->variant->compare_price == 0} hidden{/if}">
				<span class="value">{$product->variant->compare_price|convert}</span>
			</div>
			<div class="price text_black">
				<span class="value">{$product->variant->price|convert}</span>&nbsp;
				<span class="currency">{$currency->sign|escape}</span>
			</div>
            {if $product->variant->coupon_discount_price && $smarty.session.admin}
            <div>Купить сейчас за {$product->variant->auto_coupon_price|convert} <span class="currency">{$currency->sign|escape}</div>
            {/if}
            {if $user->group_id}
            <div class="group_discount shadow_box_item">
               <div class="">Скидка пользователя {$user_group->discount} % </div>
                <div class="price_discount"> - <span class="value">{($product->variant->price * $user_group->discount/100)|convert}</span> <span class="currency">{$currency->sign|escape}</span></div>
                <div class="ready_price text_black">
					<span class="value">{($product->variant->price - ($product->variant->price * $user_group->discount/100))|convert}</span>&nbsp;
					<span class="currency">{$currency->sign|escape}</span>
				</div>
            </div>
            {/if}
		</div>
		
		<div class="variants_container shadow_box_item{if $product->variants|count<2} hidden{/if}">
			{foreach $product->variants as $v}
				<div class="variant">
					<input id="product_{$v->id}" class="hidden" name="variant" value="{$v->id}" type="radio" 
					{if $v->name}data-v_name='{$v->name}'{/if}
					data-price="{$v->price|convert}" 
                    
                    {if $user_group->discount}data-ready_price="{($v->price - ($v->price * $user_group->discount/100))|convert}" data-discount_price="{($v->price * $user_group->discount/100)|convert}" data-discount="{$user_group->discount}"{/if}  
                    {if $v->coupon_discount_price}
                        data-auto_coupon_price="{$v->auto_coupon_price}"
                        data-coupon_id="{$v->coupon_id}"
                        data-coupon_discount_price="{$v->coupon_discount_price}"
                    {/if}
                    
					{if $v->compare_price}data-oldprice="{$v->compare_price|convert}"{/if} 
					data-stock='{if $v->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
					{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span>{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span>{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается {if $product->byrequest}( {$product->byrequest} ){/if}{else}<span class="text_blue">В наличии</span>{/if}' {if $product->variant->id==$v->id}checked {/if} 
                    data-dostavka='{if $product->variant->store==1}<span id="tomorrow_osn"></span>
                        {*if $v->price > $settings->interval_price} - бесплатно{else} - {$category->delivery_price} руб.{/if*}
                                    {else}
                                        {if $product->variant->price > $settings->interval_tom_price}<span id="tomorrow_osn"></span>{* - бесплатно*}
                                        {elseif $product->variant->price > $settings->interval_price}<span id="tomorrow_osn"></span> {*- {intval($category->delivery_price) * intval($settings->interval_delivery_mult)} руб.<br />
                                        <span id="aftertomorrow_osn"></span>{* - бесплатно *}
                                        {else}<span id="tomorrow_osn"></span>{* - {intval($category->delivery_price) * intval($settings->interval_delivery_mult)} руб.<br />
                                        <span id="aftertomorrow_osn"></span>{* - {$category->delivery_price} руб. *}
                                        {/if}
                                    {/if}'
                    data-samovyvoz='<span class="samovyvoz{if ($v->stock == 0 || $product->sklad_id != 0 || !$v->store || $v->store==3)}_no{else}{if intval(date("H", time())) >= 19}_next_day{/if}{/if}"></span>'
                    />
					<label for="product_{$v->id}" class="checkbox_label">{$v->name}</label>
				</div>
			{/foreach}
		</div>
			
		<div class="shadow_box_item button_container{if $product->variant->price == 0} hidden{/if}">
			
			<div class="relative">
				<button class="button button_block" type="submit">
					<span class="button_item icon_text">Купить</span>
					<span class="button_item icon">{include file="svg.tpl" svgId="icon_cart"}</span>
				</button>
				
				<div class="product_amount amount absolute text_bold text_center text_gray animated">
					<input class="amount_count" type="tel" name="amount" value="1" data-max="{$product->variant->stock}">
					{*
					<span class="amount_trigger minus absolute"></span>
					<span class="amount_trigger plus absolute"></span>
					*}
				</div>
			</div>
			
			<a class="oneclick_link link pseudo_link text_gray fancybox" href="#oneclick_form">Купить быстро в 1 клик</a>
				
		</div>
		{*
        <div class="block-delivery">
            <div class="prod_dostavka">
                {if $v->store==1} <span id="tomorrow_osn"></span> 
                {else}
                    {if $v->price > $settings->interval_tom_price} <span id="tomorrow_osn"></span>
                    {elseif $v->price > $settings->interval_price} <span id="tomorrow_osn"></span>
                    {else} <span id="tomorrow_osn"></span>
                    {/if}
                {/if}
            </div>
            
            <div class="prod_samovyvoz">
                <span class="samovyvoz{if ($product->variant->stock == 0 || $product->sklad_id != 0 || !$product->variant->store || $product->variant->store==3)}_no{else}{if intval(date("H", time())) >= 19}_next_day{/if}{/if}"></span>
            </div>
            
        </div>
		*}
	</form>
    	
</div> 