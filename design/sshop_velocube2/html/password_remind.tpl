{* Письмо пользователю для восстановления пароля *}

{* Канонический адрес страницы *}
{$canonical="/user/password_remind" scope=parent}

{$meta_title = "Восстановление пароля" scope=parent}

<main class="main shadow_section">
	<div class="page_notice_container container flex flex_align_center flex_content_center">

		<div class="shadow_box">
			<div class="shadow_box_item">
			
			{if $email_sent}
				<h1 class="title text_2">Вам отправлено письмо</h1>
				<div class="text_gray">На {$email|escape} отправлено письмо для восстановления пароля.</div>
			
			{else}

				<form class="form form_single" method="post">
					<h1 class="form_group text_2 text_black text_center text_bold">Напоминание пароля</h1>
					{if $error}
					<div class="notice notice_alert">
						{if $error == 'user_not_found'}Пользователь не найден
						{else}{$error}{/if}
					</div>
					{/if}
					<div class="form_group">
						<label class="form_label">Введите email, который вы указывали при регистрации</label>
						<input class="form_input" type="text" name="email" data-format="email" data-notice="Введите email" value="{$email|escape}"  maxlength="255"/>
					</div>
					<input type="submit" class="button button_large button_block" value="Вспомнить" />
				</form>
			{/if}
			</div>
		</div>

</main>