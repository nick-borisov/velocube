{* Информер обратного звонка *}
{if $callback_id}
	<div class="callback_success form_modal text_center">
        <div class="form_modal_row form_modal_title text_2 text_bold text_center text_green">Ваша заявка принята</div>
		<div class="form_modal_row text_gray">
			<div class="form_group">В ближайшее время с вами свяжется наш менеджер!</div>
			<button type="button" class="button" onclick='$.fancybox.close();return false;'>Закрыть</button>
		</div>
    </div>
{else}
	<div class="callback_error form_modal text_center">
        <div class="form_modal_row form_modal_title text_2 text_bold text_center text_red">При отправке возникла ошибка</div> 
		<div class="form_modal_row text_gray">
			<div class="form_group">Пожалуйста, попробуйте позже</div>
			<button type="button" class="button" onclick='$.fancybox.close();return false;'>Закрыть</button>
		</div>
    </div> 
{/if}
