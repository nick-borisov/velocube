{strip}
<main class="main section">
	<div class="container">
		
		{include file="path.tpl"}
		
		<h1 class="page_title text_black">{$page->header}</h1>
		
		{if $page->body}
			<div class="user_content">{$page->body}</div>
			<br><br>
		{/if}
		
		{include file="comment_block.tpl"}
		
	</div>
</main>
{/strip}