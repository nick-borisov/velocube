{strip}
{* Страница регистрации *}

{* Канонический адрес страницы *}
{$canonical="/user/register" scope=parent}

{$meta_title = "Регистрация" scope=parent}

<main class="main shadow_section">
	<div class="container">
	
		{* Хлебные крошки *}
		{include file="path.tpl"}
		
		<div class="auth_container row">
		
			<div class="auth_col col_xs_12">
				<div class="auth_inner shadow_box">	
					<div class="auth_content shadow_box_item">

						<form class="form_single" method="post">

							<h1 class="form_group text_2 text_black text_center text_bold">Регистрация</h1>

							{if $error}
							<div class="notice notice_alert">
								<div class="notice_title">Внимание!</div>
								{if $error == 'empty_name'}Введите имя
								{elseif $error == 'empty_email'}Введите адрес электронной почты
								{elseif $error == 'empty_password'}Введите пароль
								{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
								{else}{$error}{/if}
							</div>
							{/if}

							<div class="form_group">
								<label for="register_name" class="form_label">Имя: *</label>
								<input id="register_name" class="form_input" type="text" name="name" value="{$name|escape}" maxlength="255" data-format=".+" data-notice="Введите имя"/>
							</div>

							<div class="form_group">
								<label for="register_surname" class="form_label">Фамилия:</label>
								<input id="register_surname" class="form_input" type="text" name="surname" value="{$surname|escape}" maxlength="255" data-format=".+" data-notice="Введите фамилию"/>
							</div>
							
							<div class="form_group">
								<label for="register_email" class="form_label">Адрес электронной почты: *</label>
								<input id="register_email" class="form_input" type="text" name="email" value="{$email|escape}" maxlength="255" data-format=".+" data-notice="Введите адрес электронной почты"/>
							</div>
							
							<div class="form_group">
								<label for="register_phone" class="form_label">Номер телефона:</label>
								<input id="register_phone" class="phone_mask form_input" type="text" name="phone" value="{$phone|escape}" maxlength="255" data-format=".+" data-notice="Введите номер телефона"/>
							</div>	
							
							<div class="form_group">
								<label for="register_password" class="form_label">Пароль: *</label>
								<input class="form_input" type="password" name="password" value="" autocomplete="off" data-format=".+" data-notice="Введите пароль"/>
							</div>

							<div class="form_group">
								<input type="submit" class="button button_block button_large" name="register" value="Зарегистрироваться">
							</div>
							
							<div class="text_gray2 text_5">* - обязательные поля для заполнения</div>

						</form>
						
					</div>
				</div>
			</div>
			
			<section class="auth_col col_xs_12">
				<div class="auth_inner shadow_box">
					<div class="auth_content shadow_box_item">
						<h2 class="title text_black text_bold text_left">Войти используя аккаунт социальной сети</h2>
						<div id="uLogin" class="flex" data-ulogin="display=buttons;fields=first_name,last_name,email,phone;callback=get_ulogin;redirect_uri=">
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_fb.svg" alt="" title="" data-uloginbutton="facebook"/></span>
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_tw.svg" alt="" title="" data-uloginbutton="twitter"/></span>
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_gp.svg" alt="" title="" data-uloginbutton="googleplus"/></span>
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_vk.svg" alt="" title="" data-uloginbutton="vkontakte"/></span>
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_ok.svg" alt="" title="" data-uloginbutton="odnoklassniki"/></span>
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_ya.svg" alt="" title="" data-uloginbutton="yandex"/></span>
							<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_mr.svg" alt="" title="" data-uloginbutton="mailru"/></span>
						</div>
					</div>
				</div>
			</section>
		
		</div>

	</div>
	
</main>

{get_browsed_products var=browsed_products limit=10}
{if $browsed_products}
	<section class="section">
		<div class="container">
			<h2 class="title">Вы недавно смотрели</h2>
			<div class="products grid products_carousel">
			{foreach $browsed_products as $b_product}
				<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
					{include file='product_block.tpl' product=$b_product}
				</div>
			{/foreach}
			</div>
		</div>
	</section>
{/if}

{/strip}