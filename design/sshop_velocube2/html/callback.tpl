{strip}
<div id="callback_form">
    <form id="mail_form" class="form_modal" method="post">
	
		<div class="form_modal_row form_modal_title text_2 text_bold text_center">Заказ обратного звонка</div>

		<div class="form_modal_row">
		
			<div class="form_group">
				<label for="callback_name" class="form_label">Ваше имя: *</label>
				<input id="callback_name" class="form_input" type="text" name="name" value="" required/>
			</div>
			<div class="form_group">
				<label for="callback_phone" class="form_label">Номер телефона: *</label>
				<input id="callback_phone" class="form_input phone_mask" type="tel" name="phone" value="" maxlength="255" required/>
			</div>
			
			{if $settings->callback_message}
				<div class="form_group">
					<label for="callback_message" class="form_label">Сообщение:</label> 
					<textarea id="callback_message" class="form_input" name="message" rows="3" placeholder="Сообщение"></textarea>
				</div>
			{/if}
			{if $settings->callback_calltime}
			<div class="form_group">	
				<input id="call_intime" class="hidden" type="checkbox" name="call_intime" value="1"/>
				<label for="call_intime" class="checkbox_label">Назначить время звонка:</label>
			</div>
			<div id="callback_time_choose" class="callback_time_container form_group" style="display: none;">
				<div class="flex flex_content_justify flex_align_center">
					<div>
						<select class="callback_time callback_time_from" name="time_from" disabled="disabled">
							{section name=calltime_from loop=$settings->calltime_to start=$settings->calltime_from}
							<option value="{$smarty.section.calltime_from.index}">{$smarty.section.calltime_from.index}:00</option>
							{/section}
						</select>
					</div>
					<div class="callback_time_separator"></div>
					<div>
						<select class="callback_time callback_time_to" name="time_to" disabled="disabled">
							{section name=calltime_to loop=$settings->calltime_to+1 start=$settings->calltime_from+1}
							<option value="{$smarty.section.calltime_to.index}">{$smarty.section.calltime_to.index}:00</option>
							{/section}
						</select>
					</div>
				</div>
			</div>  
			{/if}
			
		</div>
		
		<div class="form_modal_row">
			<input class="button button_block button_large" type="submit" name="callback" value="Заказать"/>
		</div>
    </form>
</div>
{/strip}