$(function () {
	$('.sliders').slider({
    	range: true,
    	create: function () {
        	id = $(this).attr('id');
        	$(this).slider('option', 'step', parseFloat($(this).attr('rel')));
        	$(this).slider('option', 'min', parseFloat($('#min' + id).attr('rel')));
        	$(this).slider('option', 'max', parseFloat($('#max' + id).attr('rel')));
        	$(this).slider('option', 'values', [parseFloat($('#min' + id).val()), parseFloat($('#max' + id).val())]);
    	},
    	slide: function (event, ui) {
        	id = $(this).attr('id');
        	after_comma = $('#amount' + id).attr('rel');
        	$('#amount' + id).html(ui.values[0].toFixed(after_comma) + ' &mdash; ' + ui.values[1].toFixed(after_comma));
        	$('#min' + id).val(ui.values[0]);
        	$('#max' + id).val(ui.values[1]);
    	}
	});
});