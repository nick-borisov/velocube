$(document).ready(function(){
	
	
	/* Спойлер */
	if ( $('.spoiler').length ) {
		
		var spoiler_height_min = 150,
			spoiler_text_show  = 'Показать весь текст',
			spoiler_text_hide  = 'Свернуть текст';
			
		/* Генерируем новую html разметку */
		$('.spoiler').each(function(){
		
			var spoiler_text = $(this).html(),
				spoiler_link = '<button type="button" class="spoiler_link button button_small">Показать текст</button>';
				
			$(this).html('<div class="spoiler_text"><div class="spoiler_inner">' + spoiler_text + '</div></div>' + spoiler_link);
			
		});
		
		function spoiler_resize(){
			
			var spoiler = $(this).find('.spoiler_text'),
				spoiler_link = $(this).find('.spoiler_link'),
				spoiler_height_max = $(this).find('.spoiler_inner').outerHeight();
				
			if(	spoiler_height_max > spoiler_height_min ){
				spoiler.addClass('short').height(spoiler_height_min);
				spoiler_link.html(spoiler_text_show).show();
				
			} else {	
				spoiler.removeClass('short').height(spoiler_height_max);
				spoiler_link.html(spoiler_text_hide).hide();
				
			}
			
		}
	
		/* Обрабатываем каждый спойлер на странице */
		$('.spoiler').each(spoiler_resize);
		$(window).resize(function(){
			$('.spoiler').each(spoiler_resize);
		});
		
		$(document).on('click', '.spoiler_link', function(){	
		
			var spoiler_link = $(this),
				spoiler = $(this).prev(),
				spoiler_height_max = spoiler.find('.spoiler_inner').outerHeight();
				
			spoiler.toggleClass('short');
				
			if( spoiler.hasClass('short') ){
				spoiler.height(spoiler_height_min);
				spoiler_link.html(spoiler_text_show);
				
			} else {                  
				spoiler.height(spoiler_height_max);
				spoiler_link.html(spoiler_text_hide);

			}
			
		});
	}
	/* /Спойлер */
	
	
	/* Триггеры */
	$(document).on('click', '.trigger', function(e){
		e.preventDefault();
		
		var trigger_parent = '',
			trigger_content = $(this).next();
		
		if( $(this).closest('.trigger_container').length ) {
			trigger_content = $(this).closest('.trigger_container').find('.trigger_content').first();
		} else if ( $(this).hasClass('trigger_anchor') ) {
			trigger_content = $($.attr(this, 'href'));
		}
		
		if( trigger_content.hasClass('trigger_content') ) {
			trigger_parent = trigger_content.closest('.trigger_container');
		}
		
		$(this).toggleClass('active');
		
		if (trigger_parent.length) {
			trigger_parent.toggleClass('active');
		} else {
			trigger_content.toggleClass('active');
		}
		
		if ( typeof($(this).data('trigger_action')) != 'undefined' && $(this).data('trigger_action') == 'slide' ) {
			trigger_content.slideToggle();
		} else {
			trigger_content.fadeToggle();
		}
		
		if ( typeof($(this).data('trigger_active_text')) != 'undefined' && typeof($(this).data('trigger_default_text')) != 'undefined' ) {
			$(this).html( $(this).hasClass('active') ? $(this).data('trigger_active_text') : $(this).data('trigger_default_text') );
		}
		
	});

	/* Закрываем триггеры с классом close_outside по клику в не их области */
	$('body').bind('click', function(e) {
		
		var trigger = $('.trigger.close_outside.active'),
			trigger_parent = $('.trigger_container.close_outside.active'),
			trigger_content = trigger_parent.find('.trigger_content');
			
		if( trigger.length && ($(e.target).closest(trigger).length == 0) && ($(e.target).closest(trigger_content).length == 0) ) {
			trigger.removeClass('active');
			trigger_parent.removeClass('active');
			trigger_content.fadeOut();
			$('body.js_hide_scroll').removeClass('js_hide_scroll');
		}

	});
	
	/* При клике на триггер меню категорий скроллим страницу до ее начала (эта хрень нужна из-за фиксированной шапки при скролле) */
	$(document).on('click', '#category_menu_trigger', function(){
		if ( $(this).hasClass('active') ) {
			$('html, body').animate( { scrollTop: $(this).offset().top }, 300 );
			if ( $(window).width() < 992 ) {
				$('.category_menu_container').css('height', $(window).height() - $(this).outerHeight());
			}
		} else {
			$('.category_menu_container').css('height', '');
		}
		if ( $(window).width() < 992 ) {
			$('body').toggleClass('js_hide_scroll');
		}
	});
	
	/* /Триггеры */
	

	/* Табы */
	if ( $('.tab_container').length ) {
		
		/* Обрабатываем каждый контейнер с табами на странице */
		$('.tab_container').each(function() {
		
			/* Если у ссылки на таб есть класс active, то открываем соответствующий класс */
			if( $(this).find('.tab_link.active').length ) {
				$( $(this).find('.tab_link.active').attr("href") ).show();
				
			/* В противном случае открываем первый таб */
			} else {
				$(this).find('.tab_header .tab_link').first().addClass('active');
				$(this).find('.tab_body .tab_link').first().addClass('active');
				$(this).find('.tab').first().show();
			}   
			
		}); 
	
		/* Событие для табов на больших экранах, одновременно может быть открыт только один таб */
		$(document).on('click', '.tab_header .tab_link', function(e){
			e.preventDefault();
			
			/* Запрещаем повторное событие по тому же элементу */
			if($(this).hasClass('active')){
				return true;
			}
			
			/* Так как фактически ссылок на один таб у нас несколько, то отлавливаем не $(this), а $(this).attr("href"), чтобы передавать активное состояние всем ссылкам, с одинаковым якорем */
			var tab_container = $(this).closest('.tab_container'),
				tab_active = $(this).attr("href");
				
			tab_container.find('.tab').hide();
			tab_container.find('.tab_link').removeClass('active');
			tab_container.find('.tab_link[href="'+ tab_active +'"]').addClass('active');
			$(tab_active).fadeIn();
			
			/* Перестраиваем slick карусель '.products_carousel' в табах */
			if ($(tab_active).find('.products_carousel').length){
				$(tab_active).find(carousel).slick('setPosition');
			}
		});
	
		/* Событие для табов на малых экранах, нет ограничения на одновременно открытые табы */
		$(document).on('click', '.tab_body .tab_link', function(e){
			e.preventDefault();
			
			var tab_container = $(this).closest('.tab_container'),
				tab_active = $(this).attr("href");
			
			tab_container.find('.tab_link[href="'+ tab_active +'"]').toggleClass('active');
			$(tab_active).slideToggle();
			
			/* Перестраиваем slick карусель '.products_carousel' в табах */
			if ($(tab_active).find('.products_carousel').length){
				$(tab_active).find(carousel).slick('setPosition');
			}
		});
	
		/* Обратная совместимость, для перехода между малыми и большими экранами */
		/* Была идея переписать это и начальный foreach по феншую и так, чтобы в случае нескольких активных табов оставался первый выбранный, а не первый из списка, но мне впадлу, поэтому будет вот такой костыль */
		$(window).resize(function(){
			if ( $(window).width() > 767 ) {
				$('.tab_container').each(function() {				
					if( $(this).find('.tab_header .tab_link.active').length != 1 ) {
						$(this).find('.tab_link').removeClass('active');
						$(this).find('.tab_header .tab_link').first().addClass('active');
						$(this).find('.tab_body .tab_link').first().addClass('active');
						$(this).find('.tab').hide();
						$(this).find('.tab').first().show();
					}   
				}); 
			}
		});
		
	}
	/* /Табы */
	
	
	/* Слайдер на главной */
	if($('#main_slider').length) {
		$('#main_slider').slick({
			fade: true,
			dots: true,
			autoplay: true,
			autoplaySpeed: 2000,
			prevArrow: '<button type="button" class="slick-prev"></button>',
			nextArrow: '<button type="button" class="slick-next"></button>',
			appendArrows: $('#main_slider_control'),
			appendDots: $('#main_slider_control'),
			responsive: [{
				breakpoint: 767,
				settings: {
					arrows: false
				}
			}]
		});
	}
	/* /Слайдер на главной */
	
	/* Слайдер фото в товаре */
	if($('#product_gallery').length) {
		$('#product_gallery').slick({
			dots: true,
			arrows: false,
			infinite: false,
			customPaging: function(slider, i) {
				var title = $(slider.$slides[i]).data('thumb');
				return '<span class="product_gallery_control animated"><img src="' + title + '"/></span>';
			}
		});
		/* if ( $('.product_gallery_first').length ) {
			$('#product_gallery').slickGoTo('.product_gallery_first');
		} */
	}
	/* /Слайдер фото в товаре */
	
	/* Слайдер баннеров в категории */
	if($('#products_slider').length) {
		$('#products_slider').slick({
			dots: true,
			arrows: false,
			infinite: false
		});
	}
	/* Слайдер баннеров в категории */
	
	/* Карусель брендов */
	if($('#brand_carousel').length) {
		$('#brand_carousel').slick({
			speed: 500,
			slidesToShow: 5,
			slidesToScroll: 4,	
			dots: false,
			prevArrow: '<button type="button" class="slick-prev"></button>',
			nextArrow: '<button type="button" class="slick-next"></button>',
			responsive: [{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 3
				}
			},{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},{
				breakpoint: 544,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});
	}
	/* /Карусель брендов */
	
	/* Карусель товаров */
	if($('.products_carousel').length) {
		var carousel = $('.products_carousel').slick({
			speed: 500,
			slidesToShow: 4,
			slidesToScroll: 4,
			dots: true,
			prevArrow: '<button type="button" class="slick-prev"></button>',
			nextArrow: '<button type="button" class="slick-next"></button>',
			responsive: [{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},{
				breakpoint: 544,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false
				}
			}]
		});
	}
	/* /Карусель товаров */
	
	
	/* Плавный переход по якорю */
	$(document).on('click', '.anchor_link', function(e){
		e.preventDefault();
		var target = $(this).attr("href");
		
		$('body, html').animate({
			scrollTop: $(target).offset().top - 120
		}, 2000);
		
		/* Отдельно обрабатываем ссылку на отзывы в табах
		   Если блок с отзывами закрыт, то открываем его */
		if ( $(this).attr('id', 'show_review_link') && $('#product_tab .tab_header .tab_link[href="'+ target +'"]:not(.active)') ){
			$('#product_tab .tab_header .tab_link[href="'+ target +'"]').trigger('click');
		}	
		
	});
	
	
	/* Маска для телефона */
	$('.phone_mask').mask("+7(999)9999999");
	
	/*callback*/
	
	$(document).on('click', 'input[name="call_intime"]', function(){
        $('#callback_time_choose').slideToggle();
        $('#callback_time_choose select').attr('disabled', false);
    }); 
	
    $(document).on('change', 'select[name="time_from"]', function(){
        var val = $(this).val();
		
		/* Прячем и показываем актуальные варианты для выбора в селекте time_to */
		$('select[name="time_to"] :contains("'+val+'")').hide().prevAll().hide();
		$('select[name="time_to"] :contains("'+val+'")').nextAll().show();
		
		/* Выбираем следующий вариант после выбранного в time_from */
        $('select[name="time_to"] :contains("'+val+'")').next().attr('selected', 'selected');
    });  
    $(document).on('submit', '#mail_form', function(e){
		e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'ajax/callback.php',
            data: $('#mail_form').serialize(),
            dataType: 'json',
            success: function(data){
                $.fancybox({
					content: data,
					padding: 0
				});
            },error: function(s1,s2,s3){
				console.log(s1,s2,s3);
			}
        });
    });
	/* /callback */
	
	/* Одиночный fancybox */
	$('.fancybox').fancybox({
		padding: 0
	});
	
	
	/* Галлерея fancybox */
	$('.fancybox_gallery').fancybox({
		padding: 0,
		prevEffect	: 'fade',
		nextEffect	: 'fade'
	});
	
	
	/* Меню категорий */
	
	/* Выставляем начальные параметры для пунктов в категории меню */
	$('.category_menu_item').each(function(){
		var category_name = $(this).find('.category_menu_name.level_2').text();
		
		/* Всем родителям выбранного элемента задаем активное состояние */
		if ( $(this).hasClass('active') ) {
			$(this).parents('.category_menu_item').addClass('active').find('.category_menu_icon').addClass('active');
		}
		
		/* Добавляем ссылку для показа всего дерева категорий (включая 4-й уровень), если выполнены условия */
		if ( $(this).hasClass('level_2') && $(this).find('.category_menu_list.level_4').length ) {
			$(this).append('<span class="category_menu_showall link text_pink text_5">Все '+ category_name +'</span>');
		}
		
		/* Отображаем дерево категорий, в котором есть выбранный элемент */
		if ( $(window).width() < 992 ) {
			$('.category_menu_item.active').find('.category_menu_list.level_2').show();
		}
		
	});
	
	/* По клику разворачиваем полное дерево категорий в конкретном пункте меню */
	$(document).on('click', '.category_menu_showall', function(){
		
		var parent_category = $(this).closest('.category_menu_item'),
			parent_category_name = parent_category.find('.category_menu_name.level_2'),
			parent_category_list = $(this).closest('.category_menu_list');
		
		$(this).hide();
		
		/* Сначала убираем прозрачноть всем пунктам меню */
		parent_category_list.children().animate({opacity: '0'}, 300);
		
		/* Потом с задержкой скрываем соседние пункты меню и плавно увеличиваем прозрачность текщего пункта */
		window.setTimeout(function(){ 
			
			parent_category_name.prepend('<span class="category_menu_back animated" title="Назад"></span>');
			parent_category.addClass('expanded').animate({opacity: '1'}, 300).siblings().hide(); 
			$('.category_menu_bg').css('height', parent_category_list.outerHeight());
		}, 300 );
		/* Вся эта карусель с прозрачностью нужна, чтобы скрыть трансформацию */
	});
	
	/* Возвращаем меню в дефолтное состояние */
	$(document).on('click', '.category_menu_back', function(){
		
		var parent_category = $(this).closest('.category_menu_item'),
			parent_category_name = parent_category.find('.category_menu_name.level_2'),
			parent_category_list = $(this).closest('.category_menu_list'),
			parent_category_showall = parent_category.find('.category_menu_showall');
			
		$(this).remove();
		
		/* Сначала убираем прозрачноть пункту меню */
		parent_category.animate({opacity: '0'}, 300);
		
		/* Затем с задержкой восстанавливаем дефолтное состояние */
		window.setTimeout(function(){ 
			parent_category_showall.show();
			parent_category.removeClass('expanded').siblings().show(); 
			parent_category_list.children().animate({opacity: '1'}, 300);
			$('.category_menu_bg').css('height', parent_category_list.outerHeight());
		}, 300 );
		
	});
	
	function category_menu() {
		
		/* Цепляем события и эффекты на пункты меню для больших экранов */
		if ( $(window).width() >= 992 ) {
			
			/* Создаем фоновую подложку */
			if( $('.category_menu_bg').length == 0) {
				$('.category_menu').prepend('<div class="category_menu_bg animated"></div>');
			}
			
			/* Пилим появление меню по клику */
			$(document).on('mouseenter', '.category_menu_item.level_1', function(){
				if( $(this).hasClass('hover') ) {
					return true;
				}
				/* Задаем высоту для темной подложки */
				if( $('.category_menu_container.hover').length == 0 ){
					$('.category_menu_container').addClass('hover');
					
					$('.category_menu_container').css('height', $(document).height() - $('.header').outerHeight());
					
				}
				
				$(this).addClass('hover');
				$(this).find('.category_menu_list.level_2').show();	
				
				/* Изменяем высоту подложки в зависимости от выбранного пункта меню */
				if ( $(this).find('.category_menu_list.level_2').length ) {
					$('.category_menu_bg').css('height', $(this).find('.category_menu_list.level_2').outerHeight());
				} else {
					$('.category_menu_bg').css('height', '');
				}
			});	
				
			$(document).on('mouseleave', '.category_menu_item.level_1', function(){
				$(this).removeClass('hover');
			});
			
			/* Сбрасываем все эффекты, если курсор не на меню */
			$(document).on('mouseleave', '.category_menu_list.level_1', function(){
				$('.category_menu_container').removeClass('hover').css('height', '');
				$('.category_menu_bg').css('height', '');
				$('.category_menu_list.level_2').hide();
			});

		/* Сбрасываем все эффекты и события для малых экранов */
		} else {
			if( $('.category_menu_bg').length) {
				$('.category_menu_bg').remove();
			}
			$('.category_menu_item.level_1.active').find('.category_menu_list.level_2').show();
			$('.category_menu_container.hover').removeClass('hover').css('height', '');
			
			$(document).off('mouseenter mouseleave', '.category_menu_item.level_1');
			$(document).off('mouseleave', '.category_menu_list.level_1');
		}
	}
	
	category_menu();
	$(window).on('resize', category_menu);
	
	/* /Меню категорий */
	
	
	/* Показываем доп фото по ховеру в превью товаров */
	if ( $('.products_image_gallery').length ) {
		
		/* Меняем урл фото по ховеру */
		$(document).on('mouseenter', '.products_image_gallery_item', function(){
			
			var image_parent 	= $(this).closest('.products_image_link'),
				image_box		= image_parent.find('img'),
				image_current 	= $(this).data('src');
			
			$(this).addClass('hover');
			image_box.attr('src', image_current);
			
			/* Если курсор остановился на последней фото, но у товара они есть еще, то показываем количество не просмотренных */
			if ( $(this).is(':last-child') && image_parent.find('.products_image_gallery_count').length ) {
				image_parent.find('.products_image_gallery_count').addClass('show_count');
			}
		});
		
		$(document).on('mouseleave', '.products_image_gallery_item', function(){
			
			var image_parent = $(this).closest('.products_image_link');
			
			$(this).removeClass('hover');
			
			/* Если курсор ушел с последнего фото, то скрываем количество не просмотренных */
			if ( $(this).is(':last-child') && image_parent.find('.products_image_gallery_count').length ) {
				image_parent.find('.products_image_gallery_count').removeClass('show_count');
			}
		});

		/* Если курсор ушел с области галереи, то восстанавливаем дефолтное состояние */
		$(document).on('mouseleave', '.products_image_gallery', function(){
			
			var image_parent 	= $(this).closest('.products_image_link'),
				image_box		= image_parent.find('img'),
				image_original 	= $(this).closest('.product').find('.product_image').data('product_image');
				
			$(this).children().removeClass('hover');
			image_box.attr('src', image_original);
			
			if ( $(this).find('.products_image_gallery_count').length ) {
				$(this).find('.products_image_gallery_count').removeClass('show_count');
			}
				
		});
			
	}
	
	/*Изменение данных при смене варианта*/
	$(document).on('change', 'form.variants input[name="variant"]', function(){
	 
		var old_price 	 = $(this).closest('form.variants').find('.old_price .value'),
			final_price	 = $(this).closest('form.variants').find('.final_price .value'),
			discount_price_shop	  = $(this).closest('form.variants').find('.discount_price_shop .value'),
			discount_price_user	  = $(this).closest('form.variants').find('.discount_price_user .value'),
			discount_price_coupon = $(this).closest('form.variants').find('.discount_price_coupon .value'),
			sku		 	  = $(this).closest('.product').find('.product_sku .value'),      
			stock	 	  = $(this).closest('form.variants').find('#product_stock'),      
            delivery,
			delivery_self;

		/*Изменяем цену при смене варианта*/
		if ( $(this).data('final_price') != 0 && $(this).data('final_price') != '0,00' ) {
			final_price.html($(this).data('final_price'));
            $(this).closest('form.variants').find('.price_container').removeClass('hidden');
			$(this).closest('form.variants').find('.button_container').removeClass('hidden');	
		} else {
			$(this).closest('form.variants').find('.price_container').addClass('hidden');
			$(this).closest('form.variants').find('.button_container').addClass('hidden');
		}
		
		/*Показываем артикул варианта, если есть*/
		if(	typeof($(this).data('v_sku')) != 'undefined' ) {
			sku.html($(this).data('v_sku'));
			sku.parent().removeClass('hidden');
		} else {
			sku.parent().addClass('hidden');
		}
		
		/*Показываем старую цену варианта, если есть*/
		if(	typeof($(this).data('old_price')) != 'undefined' ) {
			old_price.html($(this).data('old_price'));
			old_price.parent().removeClass('hidden');
		} else {
			old_price.parent().addClass('hidden');
		}
		
		/*Показываем разницу между старой и новой ценой*/
		if(	typeof($(this).data('discount_price_shop')) != 'undefined' ) {
			discount_price_shop.html($(this).data('discount_price_shop'));
			discount_price_shop.parent().removeClass('hidden');
		} else {
			discount_price_shop.parent().addClass('hidden');
		}
		
		/*Показываем скидку пользователя, если есть*/
		if(	typeof($(this).data('discount_price_user')) != 'undefined' ) {
			discount_price_user.html($(this).data('discount_price_user'));
			discount_price_user.parent().removeClass('hidden');
		} else {
			discount_price_user.parent().addClass('hidden');
		}

		/*Показываем авто скидку, если есть*/
		if(	typeof($(this).data('discount_price_coupon')) != 'undefined' ) {
			discount_price_coupon.html($(this).data('discount_price_coupon'));
			discount_price_coupon.parent().removeClass('hidden');
		} else {
			discount_price_coupon.parent().addClass('hidden');
		}
		
		/*Изменяем доступное кол-во при смене варианта*/
		stock.html($(this).data('stock'));
		/* console.log($(this).data('stock')); */
		
		/* Выводим иконки доставки у товара */
		if(	typeof($(this).data('delivery')) != 'undefined' ) {
			delivery 	  = $(this).closest('.product_single').find('.product_delivery'),
			delivery_self = $(this).closest('.product_single').find('.product_delivery_self');
			delivery.html($(this).data('delivery'));
			delivery_self.html($(this).data('delivery_self'));
			clock();
        }
	});
	
	
	/* Выбор количества */
	$(document).on('click', '.amount_trigger', function(){
		
		var $input = $(this).closest('.amount').find('.amount_count'), 
			max_val = parseInt($input.data('max')), 
			curr_val = parseInt($input.val());
			
		if(	$(this).hasClass('plus') ) {
			$input.val(Math.min(max_val,(curr_val+1))).trigger('change');
			
		} else if( $(this).hasClass('minus') ) {
			$input.val(Math.max(1,(curr_val-1))).trigger('change');
		}
		
		/* Изменение количества товара во всплывающей корзине */
		/* if($(this).closest('#popup_cart').length) {
			$.ajax({
				url: "ajax/cart_amount.php",
				data: {variant_update: $input.data('id'), amount: $input.val()},
				dataType: 'json',
				success: function(data){	
					console.log(data);
					$('#cart_informer').html(data);
					$.fancybox({
						padding: 0,
						href: '#popup_cart'
					});
				}
			});
		} */
		
	});
	/* /Выбок количества */
	
	
	/* Переключение вида товаров в категории */
	if( $('#products').length ){
		if(sessionStorage){
			var viewTtype;
			if(!sessionStorage.view){
				sessionStorage.view = "grid";
				$('#products').addClass(sessionStorage.view);
			} else {
				viewTtype = sessionStorage.view;
				$('.change_view_icon[data-view="' + sessionStorage.view + '"]').addClass('active');
				$('#products').addClass(viewTtype);
			}
			$(document).on('click', '.change_view_icon', function(){
				sessionStorage.view = $(this).data('view');
				$('.change_view_icon').removeClass('active');
				$('#products').removeClass('grid');
				$('#products').removeClass('list');
				$('#products').addClass(sessionStorage.view);
				$(this).addClass('active');
			});
		} else {
			$('#products').show();
		}
	}
	/* /Переключение вида товаров в категории */

	
	/* Раскрываем фильтр в категории по клику */
	$(document).on('click', '#filter_trigger', function(){
		$(this).toggleClass('active');
		$('#products_sidebar').toggleClass('active');
		$('.products_content').toggleClass('filtered');
		if ( $(window).width() < 768 ) {
			$('body').toggleClass('js_hide_scroll');
		}
	});
	
	/* Прячем фильтр на малых экранах */
	$(document).on('click', '#filter_close', function(){
		$('#products_sidebar').removeClass('active');
		$('.products_content').removeClass('filtered');
		$('body').removeClass('js_hide_scroll');
	});
	
	/* Прячем большой список значений фильтра под спойлер */
	$(document).on('click', '.filter_showall', function(){
		$(this).toggleClass('active');
		$(this).closest('.filter_item').find('.filter_list').toggleClass('filter_list_large');
		
		if( $(this).hasClass('active') ){	
			$(this).html('Свернуть');
		} else {                  
			$(this).html('Показать все');
		}
	});
	
	/* Открываем фильтр при загрузке, если есть выбранные параметры */
	if ( $(window).width() >= 768 && $('.filter_selected').length) {
		$('#products_sidebar').addClass('active');
		$('.products_content').addClass('filtered');
		$('#filter_trigger').addClass('active');
	}
	
	/* Делаем фильтр по свойству открытым, если выбрано хотя бы одно значение */
	$('.filter_feature').each(function(){
		if ( ( $(this).find('.filter_selected').length || $(this).find('select').val() > 0 ) && !$(this).hasClass('active') ) {
			$(this).addClass('active');
			$(this).find('.filter_item_trigger').addClass('active');
			$(this).find('.filter_item_content').show();
		}
	});
	
	/* Кнопка Вверх */
	$(document).on('click', '#moveup', function(){
		$('html, body').animate({scrollTop: 0}, 800);
	});
	
	/* Изменения при скролле и ресайзе */
	function scroll_resize_change() {
		 
		/* Fallback для перехода между диапазонами с открытым фильтром */
		if ( $(window).width() >= 768 && $('body').hasClass('js_hide_scroll') && $('#products_sidebar').hasClass('active') ) {
			$('body').removeClass('js_hide_scroll');
		} else if ( $(window).width() < 768 && $('body').not('.js_hide_scroll') && $('#products_sidebar').hasClass('active') ) {
			$('body').addClass('js_hide_scroll');
		}
		
		/* Fallback для перехода между диапазонами с открытым меню */
		if ( $(window).width() >= 992 && $('body').hasClass('js_hide_scroll') && $('#category_menu_trigger').hasClass('active') ) {
			$('body').removeClass('js_hide_scroll');
		} else if ( $(window).width() < 992 && $('body').not('.js_hide_scroll') && $('#category_menu_trigger').hasClass('active') ) {
			$('body').addClass('js_hide_scroll');
		}
		
		/* Обрабатываем отдельные элементы при скролле, после того как проехали первый экран */
		if ( $(document).scrollTop() > ( $(window).height() * 2 ) ) {
			
			/* Кнопка Вверх */
			if ( $(window).width() >= 1170 ) {
				$('#moveup').fadeIn();
			}
			
			/* Плавное выезжание фиксированной шапки */
			if ( $('#header').hasClass('absolute') && $('.category_menu_item.hover').length == 0 ) {
				$('#header').addClass('header_moving');
				window.setTimeout(function(){ 
					$('#header').addClass('fixed').removeClass('absolute header_moving'); 
				}, 300 );
			}	
			
		} else {
			
			/* Плавное уезжание фиксированной шапки */
			if ( $('#header').hasClass('fixed') ) {
				$('#header').addClass('header_moving');
				window.setTimeout(function(){ 
					$('#header').addClass('absolute').removeClass('fixed header_moving'); 
				}, 300 );
			}	
			
			/* Прячем кнопку Вверх */
			if ( $(window).width() >= 1170 ) {
				$('#moveup').fadeOut();
			}
			
		}
		
		/* Прячем кнопку Вверх, еще раз, так чтоб наверняка:) */
		if ( $(window).width() < 1170 && $('#moveup').css('display') != 'none') {
			$('#moveup').hide();
		}
		
	}
	
	scroll_resize_change();	
	$(window).on('scroll resize', scroll_resize_change);
	
	/* /Изменения при скролле и ресайзе */

	
});