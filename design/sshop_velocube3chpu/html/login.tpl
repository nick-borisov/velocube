{strip}
{* Страница входа пользователя *}

{* Канонический адрес страницы *}
{$canonical="/user/login" scope=parent}

{$meta_title = "Вход" scope=parent}
   
<main class="main shadow_section">
	<div class="container">
	
	{* Хлебные крошки *}
	{include file="path.tpl"}
	
	<div class="auth_container row">
	
		<div class="auth_col col_xs_12 col_md_6">
			<div class="auth_inner shadow_box">
				<form class="auth_content shadow_box_item" method="post">
				
					<h1 class="form_group text_2 text_black text_center text_bold">Авторизация на сайте</h1>
					
					{if $error}
					<div class="notice notice_alert">
						<div class="notice_title">Внимание!</div>
						{if $error == 'login_incorrect'}Неверный логин или пароль
						{elseif $error == 'user_disabled'}Ваш аккаунт еще не активирован.
						{else}{$error}{/if}
					</div>
					{/if}

					<div class="form_group">
						<label for="logn_email" class="form_label">Адрес электронной почты:</label>
						<input id="logn_email" class="form_input" type="text" name="email" value="{$email|escape}" maxlength="255" data-format="email" data-notice="Введите email"/>
					</div>
					
					<div class="form_group">
						<label for="login_password" class="form_label">Пароль:</label>
						<input id="login_password" class="form_input" type="password" name="password"  value="" autocomplete="off" data-format=".+" data-notice="Введите пароль"/>
					</div>
					
					<div class="form_group">
						<a class="link text_5 text_gray2" href="user/password_remind">Забыли пароль?</a>
					</div>
					
					<input type="submit" class="button button_block button_large" name="login" value="Войти">
				
				</form>
			</div>
		</div>
		
		<section class="auth_col col_xs_12 col_md_6">
			<div class="auth_inner shadow_box">
				<div class="auth_content shadow_box_item">
					<h2 class="form_group text_2 text_black text_center text_bold">Регистрация</h2>
					<ul class="register_advantage_icons text_nowrap text_center form_group">
						<li class="register_advantage_item item_1 relative inline_block_middle">
							<span class="register_advantage_icon inline_block">
								{include file="svg.tpl" svgId="icon_clock" width="40px" height="40px"}
							</span>
							<div class="register_advantage_name relative text_5 uppercase text_bold">Быстро</div>
						</li>
						<li class="register_advantage_item item_2 relative inline_block_middle">
							<span class="register_advantage_icon inline_block">
								{include file="svg.tpl" svgId="icon_couch" width="40px" height="40px"}
							</span>
							<div class="register_advantage_name relative text_5 uppercase text_bold">Удобно</div>
						</li>
						<li class="register_advantage_item item_3 relative inline_block_middle">
							<span class="register_advantage_icon inline_block">
								{include file="svg.tpl" svgId="icon_touch" width="40px" height="40px"}
							</span>
							<div class="register_advantage_name relative text_5 uppercase text_bold">Легко</div>
						</li>
					</ul>
					<ul class="register_advantage_list ul_list form_group text_5 text_gray2">
						<li>Используйте введённые ранее данные</li>
						<li>Отслеживайте статус заказа</li>
						<li>Получайте скидку от 5 до 15 %</li>
						<li>Накапливайте и тратьте бонусы</li>
						<li>Сохраняйте историю заказов</li>
					</ul>
					<a href="/user/register" class="button button_block button_large button_light">Зарегистрироваться</a>
				</div>
			</div>
		</section>
		
		<section class="auth_col col_xs_12">
			<div class="auth_inner shadow_box">
				<div class="auth_content shadow_box_item">
					<h2 class="form_group text_2 text_black text_bold">Войти используя аккаунт социальной сети</h2>
					<div id="uLogin" class="flex" data-ulogin="display=buttons;fields=first_name,last_name,email,phone;callback=get_ulogin;redirect_uri=">
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_fb.svg" alt="" title="" data-uloginbutton="facebook"/></span>&nbsp;
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_tw.svg" alt="" title="" data-uloginbutton="twitter"/></span>&nbsp;
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_gp.svg" alt="" title="" data-uloginbutton="googleplus"/></span>&nbsp;
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_vk.svg" alt="" title="" data-uloginbutton="vkontakte"/></span>&nbsp;
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_ok.svg" alt="" title="" data-uloginbutton="odnoklassniki"/></span>&nbsp;
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_ya.svg" alt="" title="" data-uloginbutton="yandex"/></span>&nbsp;
						<span class="auth_link inline_block animated"><img src="design/{$settings->theme}/images/social_button_mr.svg" alt="" title="" data-uloginbutton="mailru"/></span>
					</div>
				</div>
			</div>
		</section>

	</div>

	</div>
</main>

{get_browsed_products var=browsed_products limit=10}
{if $browsed_products}
	<section class="section">
		<div class="container">
			<h2 class="title">Вы недавно смотрели</h2>
			<div class="products grid products_carousel">
			{foreach $browsed_products as $b_product}
				<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
					{include file='product_block.tpl' product=$b_product}
				</div>
			{/foreach}
			</div>
		</div>
	</section>
{/if}
{/strip}