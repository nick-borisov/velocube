{strip}

{* Список записей блога *}

{* Канонический адрес страницы *}
{$canonical="/blog" scope=parent}

<main class="main section">
	<div class="container">
	
	{* Хлебные крошки *}
	{include file="path.tpl"}
		
	<h1 class="page_title">{$page->name}</h1>

	<div class="posts row flex flex_wrap object_list">
		{foreach $posts as $post}
		<article class="post object_list_item flex_item col_xs_12 col_sm_6 col_md_4">
			<div class="post_inner object_list_inner shadow_box relative animated">
				<a class="post_image shadow_box_item text_center{if !$post->image} post_image_empty{/if}" href="blog/{$post->url}">
					{if $post->image}
						<img src="{$post->image|resize:300:150:false:$config->resized_blog_dir}" alt="{$post->name|escape}" title="{$post->name|escape}"/>
					{/if}
				</a>
				<div class="post_text shadow_box_item">
					<h3 class="post_title text_4 text_normal"><a href="blog/{$post->url}" data-post="{$post->id}">{$post->name|escape}</a></h3>
					{if $post->annotation}<div class="post_annotation text_gray text_5">{$post->annotation|strip_tags}</div>{/if}
				</div>
				<div class="post_footer shadow_box_item no_border flex flex_align_center flex_content_justify">
					<a class="post_link text_pink text_5" href="blog/{$post->url}">
						<span class="link_inverse inline_block">Подробнее</span>&nbsp;
						<i class="inline_block_middle">{include file="svg.tpl" svgId="icon_arrow_right" width="12px" height="12px"}</i>
					</a>
					<time class="text_5 text_gray2" datetime="{$post->date}">{$post->date|date}</time>
				</div>
			</div>
		</article>
		{/foreach}
	</div>

	{include file='pagination2.tpl'}
	
	</div>
</main>
{/strip}