{strip}
{* Страница с формой обратной связи *}

{* Канонический адрес страницы *}
{$canonical="/{$page->url}" scope=parent}

<main class="main section">
	<div class="container">
	
	{* Хлебные крошки *}
	{include file="path.tpl"}

	<h1 class="page_title">{$page->name|escape}</h1>

	<div class="user_content">{$page->body}</div>
	
	<div class="separator"></div>
	
	
	<form id="feedback_form" class="form_single" method="post">
		<h2 class="title">Обратная связь</h2>
		{if $message_sent}
			<div class="notice notice_success">{$name|escape}, ваше сообщение отправлено.</div>
		{else}
		
			{if $error}
			<div class="notice notice_alert">
				{if $error=='empty_name'}
					Введите имя
				{elseif $error=='empty_email'}
					Введите email
                {elseif $error=='captcha'}
					Неверная капча
				{elseif $error=='empty_text'}
					Отметьте галочку "я не робот"
				{/if}
			</div>
			{/if}
		
			<div class="form_group">
				<label for="feedback_name" class="form_label">Ваше имя: *</label>
				<input id="feedback_name" class="form_input" type="text" value="{$name|escape}" name="name" maxlength="255" data-format=".+" data-notice="Введите имя"/>
			</div>
			
			<div class="form_group">
				<label for="feedback_email" class="form_label">Ваг email: *</label>
				<input id="feedback_email" class="form_input" type="text" value="{$email|escape}" name="email" maxlength="255" data-format="email" data-notice="Введите email"/>
			</div>
			<div class="form_group">
				<label for="feedback_message" class="form_label">Сообщение: *</label>
				<textarea id="feedback_message" class="form_input" name="message" class="form_input" data-format=".+" data-notice="Введите сообщение">{$message|escape}</textarea>
			</div>
			<div class="form_group text_center">
				<div id="recaptcha_feedback" class="inline_block"></div>        
				<input id="g-recaptcha_feedback" class="recaptcha_callback" type="text" name="g-recaptcha-response" value="" data-format=".+" data-notice="Отметьте галочку"/>
			</div>
			<input class="button button_block button_large" type="submit" name="feedback" value="Отправить" />
            
		{/if}
	</form>

	</div>
</main>
{/strip}