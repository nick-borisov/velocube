{* Информер сравнения *}
<span class="header_informer_title text_5 text_center text_gray hidden_xs hidden_sm">В сравнении</span>
{if $session->count>0}
	<a class="header_informer_button button button_dark button_block" href="/compare">
{else}
	<span class="header_informer_button button button_dark button_block">
{/if}
	<span class="button_item icon">
		{include file="svg.tpl" svgId="icon_compare"}
	</span>
	<span class="button_item icon_text">
		{if $session->count>0}{$session->count}{else}0{/if}
	</span>
{if $session->count>0}
	</a>
{else}
	</span>
{/if}