
    <!-- Хлебные крошки /-->
<div id="path">
  <a href="/">Главная</a> → Все бренды
</div>
<!-- Хлебные крошки #End /-->
<div class="content corner5">
  {* Заголовок страницы *}
<h1>Все бренды</h1> 


  <ul id="allbrands">
  <li>
  {if $brands} 
  {assign 'letter' '0-9'}
  {foreach name=brands item=b from=$brands}
    {if $letter != $b->letter}
    {if !$smarty.foreach.brands.first}</ul></li>{/if}
    {assign 'letter' $b->letter}
    <li><span id="{$letter}">{if in_array($letter, array('0','1','2','3','4','5','6','7','8','9'))}0-9{else}{$letter}{/if}</span><ul>
    {/if}
    <li><a class="brand_nav" href='brands/{$b->url}'>{$b->name|escape}</a></li>

   
  {/foreach}
  </ul></li>
  {/if}
 </ul><div class="clear"><!-- /--></div> 
<!-- Фильтр по брендам #End /-->

              

 <div class="clear"></div>
</div>


