{strip}
<div class="product-cheap trigger_container close_outside animated">
	<span class="product-cheap__close icon text_red animated">{include file="svg.tpl" svgId="icon_close" width="18px" height="18px"}</span>
	<form id="product-cheap-form" class="product-cheap__form shadow_box trigger_content" method="post">
		<input type="hidden" name="callback" value="1" />
		<input type="hidden" name="product_id" value="{$product->id}" />
		<div class="shadow_box_item text_2 text_bold text_center">Нашли этот товар дешевле?</div>
		<div class="shadow_box_item">
			<div class="form_group">
				<label for="callback_cheap_name" class="form_label">Ваше имя: *</label>
				<input id="callback_name" class="form_input" type="text" name="name" value="" required/>
			</div>
			<div class="form_group">
				<label for="callback_cheap_phone" class="form_label">Номер телефона: *</label>
				<input id="callback_cheap_phone" class="form_input phone_mask" type="tel" name="phone" value="" maxlength="255" required/>
			</div>
			<input id="hidphone" class="" type="hidden" name="name_asbf" value="" maxlength="255" />
			<div class="form_group">
				<label for="callback_cheap_phone" class="form_label">Ссылка на более дешевый товар: *</label>
				<input id="callback_cheap_phone" class="form_input" type="url" name="message" value="" maxlength="255" required/>
			</div>
		</div>
		<div class="shadow_box_item">
			<button class="button button_block button_large" type="submit">Отправить</button>
		</div>
		<div class="shadow_box_item text_5 text_gray">
			Если вы хотите приобрести данный товар и нашли в другом интернет-магазине цену ниже, пришлите нам ссылку, <span class="underline">сделаем предложение лучше.</span>
		</div>
	</form>
</div>
{/strip}