{strip}
{$wrapper = 'index.tpl' scope=parent}
{$canonical="" scope=parent}

<div class="promo-section section">
	<div class="promo-section__inner container row">
		{* Слайдер на главной *}
		{get_slides var=slide}
		{if $slide}	
		<div class="main-slider col_xs_12 col_lg_9">
			<div class="js-main-slider">
			{foreach $slide as $s}
				{if $s->image}
					<div class="main-slider__item">
						{if $s->url}<a href="{$s->url}">{/if}
							<img src="{$s->image}" alt="{$s->name}" title="{$s->name}" />
						{if $s->url}</a>{/if}
					</div>
				{/if}
			{/foreach}
			</div>
		</div>  
		{/if}
		{*Товар дня*}
		{* {get_ttoday_products_plugin var=today_products limit=1} *}
		{if $product_today}
			{* {foreach $today_products as $t_product} *}
				<div class="product product--today col_xs_12 col_sm_6 col_lg_3">
					{include file="product_block.tpl" product=$product_today sale_timer="1"}
				</div>
			{* {/foreach} *}
		{/if}
		{*/Товар дня*}
		<div class="separator col_md_12 visible_lg"></div>
		{*Преимущества*}
		<div class="advantage-list col_xs_12 col_sm_6 col_lg_12">
			<div class="row">
				<div class="advantage-list__item col_lg_4">
					<div class="advantage-list__item-inner shadow_box relative animated">
						<div class="advantage-list__item-content shadow_box_item flex flex_wrap flex_align_center">
							<div class="advantage-list__item-icon col_xs_4 col_lg_12 text_right">
								<img src="design/{$settings->theme}/images/advantage_image_1.png" alt="" title="">
							</div>
							<div class="advantage-list__item-text col_xs_8 col_lg_12">
								<div class="text_medium text_black">
									<div class="text_2">Скидка 3% - </div>
									<div class="text_3">каждому</div>
								</div>
								<div class="advantage-list__item-annotation text_5 text_gray hidden_sm hidden_md">Зарегистрируйся и получи личную скидку в 3%</div>
								<a href="aktsii-i-skidki" class="link text_pink text_5">Подробнее</a>
							</div>
						</div>
					</div>
				</div>
				<div class="advantage-list__item col_lg_4">
					<div class="advantage-list__item-inner shadow_box relative animated">
						<div class="advantage-list__item-content shadow_box_item flex flex_wrap flex_align_center">
							<div class="advantage-list__item-icon col_xs_4 col_lg_12 text_right">
								<img src="design/{$settings->theme}/images/advantage_image_2.png" alt="" title="">
							</div>
							<div class="advantage-list__item-text col_xs_8 col_lg_12 flex_item">
								<div class="text_medium text_black">
									<div class="text_2">Доставка - </div>
									<div class="text_3">быстрая и выгодная</div>
								</div>
								<div class="advantage-list__item-annotation text_5 text_gray hidden_sm hidden_md">Самая быстрая и выгодная доставка для Вас</div>
								<a href="dostavka" class="link text_pink text_5">Подробнее</a>
							</div>
						</div>
					</div>
				</div>
				<div class="advantage-list__item col_lg_4">
					<div class="advantage-list__item-inner shadow_box relative animated">
						<div class="advantage-list__item-content shadow_box_item flex flex_wrap flex_align_center">
							<div class="advantage-list__item-icon col_xs_4 col_lg_12 text_right">
								<img src="design/{$settings->theme}/images/advantage_image_3.png" alt="" title="">
							</div>
							<div class="advantage-list__item-text col_xs_8 col_lg_12">
								<div class="text_medium text_black">
									<div class="text_2">Выбор - </div>
									<div class="text_3">огромный</div>
								</div>
								<div class="advantage-list__item-annotation text_5 text_gray hidden_sm hidden_md">Большой выбор велосипедов и запчастей к ним</div>
								<a href="catalog/velosport" class="link text_pink text_5">Подробнее</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		{*Все бренды*}
		{get_brands var=all_brands}
		{if $all_brands}
		<div class="brand_carousel_container col_xs_12">
			<div class="section_title">
				<div class="title text_2">Бренды</div>
				<a class="section_title_link link_inverse text_gray2 text_5" href="/all_brands">Показать все бренды</a>
			</div>
			<div id="brand_carousel" class="brand_carousel">
				{foreach $all_brands as $b}	
					{if $b@index <= 20}
					<div class="brand_carousel_item text_center">
						<a data-brand="{$b->id}" href="brands/{$b->url}">
							{if $b->image}
								<span class="brand_carousel_image"><img src="{$b->image|resize:150:60:false:'brand'}" alt="{$b->name|escape}"></span>
							{else}
								<span class="brand_carousel_name inline_block_middle uppercase text_bold">{$b->name|escape}</span>
							{/if}		
						</a>
					</div>
					{/if}
				{/foreach}
			</div>
		</div>
		{/if}		
	</div>
</div>

{* Популярные товары *}
{get_pop_products var=popular_products limit=10}
{if $popular_products}
<section class="shadow_section">
	<div class="container row">
		<div class="carousel_container">
			<h2 class="title">Популярные товары</h2>
			<div class="products grid products_carousel">
				{foreach $popular_products as $p_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$p_product}
					</div>
				{/foreach}
			</div>
		</div>
	</div>
</section>
{/if}

<div class="section">
	<div class="container">
	
		{*function name=categories_images}
		{if $categories}
			<section class="frontpage_category">
				<h2 class="title">Категории</h2>
				<div class="frontpage_category_list flex flex_wrap row">
				{foreach $categories as $c}
					{if $c->visible}
						<a class="frontpage_category_item text_center flex_item col_xs_4" href="catalog/{$c->url}" >
							<span class="frontpage_category_image"{if $c->image} style="background-image:url({$config->categories_images_dir}{$c->image});"{/if}></span>
							<span class="frontpage_category_name text_6 uppercase text_black">{$c->name|escape}</span>
						</a>
					{/if}
				{/foreach}
				</div>
			</section>
		{/if}
		{/function}
		{categories_images categories=$categories*}
	
		{* Новинки *}
		{get_new_products var=new_products limit=10}
		{if $new_products}
		{*<div class="separator"></div>*}
		<section class="carousel_container">
			<h2 class="title">Новинки</h2>
			<div class="products grid products_carousel row">
				{foreach $new_products as $n_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$n_product}
					</div>
				{/foreach}
			</div>
		</section>
		{/if}
		
		{* Рекомендуемые товары *}
		{get_featured_products var=featured_products limit=10}
		{if $featured_products}
		<div class="separator"></div>
		<section class="featured_products carousel_container">
			<div class="section_title">
				<h2 class="title">Хиты продаж</h2>
				<a class="section_title_link link_inverse text_5 text_gray2" href="/hits">Все хиты продаж</a>
			</div>
			<div class="products grid products_carousel row">
				{foreach $featured_products as $f_product}
					<div class="product col_xs_12 col_sm_6 col_md_4 col_lg_3">
						{include file='product_block.tpl' product=$f_product}
					</div>
				{/foreach}
			</div>
		</section>
		{/if}
	
	</div>
</div>


<section class="shadow_section">
	<div class="container">
		<div class="section_title">
			<div class="title text_2">Последние новости</div>
			<a class="section_title_link link text_5 text_gray2" href="/blog">Показать все новости</a>
		</div>
		{* Выбираем в переменную $last_posts последние записи *}
		{get_posts var=last_posts limit=3 sort='date'}
		{if $last_posts}
			<div class="posts object_list row flex flex_wrap">
				{foreach $last_posts as $post}
					<article class="post object_list_item flex_item col_xs_12 col_sm_6 col_md_4">
						<div class="post_inner object_list_inner shadow_box relative animated">
							{*
							<a class="post_image shadow_box_item text_center{if !$post->image} post_image_empty{/if}" href="blog/{$post->url}">
								{if $post->image}
									<img src="{$post->image|resize:300:150:false:$config->resized_blog_dir}" alt="{$post->name|escape}" title="{$post->name|escape}"/>
								{/if}
							</a>
							*}
							<div class="post_text shadow_box_item">
								<h3 class="post_title text_4 text_normal"><a href="blog/{$post->url}" data-post="{$post->id}">{$post->name|escape}</a></h3>
								{*if $post->annotation}<div class="post_annotation text_gray text_5">{$post->annotation|strip_tags|truncate:80}</div>{/if*}
							</div>
							<div class="post_footer shadow_box_item no_border flex flex_align_center flex_content_justify">
								<a class="post_link text_pink text_5" href="blog/{$post->url}">
									<span class="link_inverse inline_block">Подробнее</span>&nbsp;
									<i class="inline_block_middle">{include file="svg.tpl" svgId="icon_arrow_right" width="12px" height="12px"}</i>
								</a>
								<time class="text_5 text_gray2" datetime="{$post->date}">{$post->date|date}</time>
							</div>
						</div>
					</article>
				{/foreach}
				{*
				<div class="object_list_item flex_item col_xs_12 col_sm_12 col_md_4">
					<div id="vk_groups" class="shadow_box object_list_inner"></div>
				</div>
				*}
			</div>
		{/if}
		
		<div class="separator"></div>
		
		<div class="section_title">
			<div class="title text_2">Последние статьи</div>
			<a class="section_title_link link text_5 text_gray2" href="/articles">Показать все статьи</a>
		</div>
		{* Выбираем в переменную $last_posts последние записи *}
		{get_articles var=last_articles limit=3}
		{if $last_posts}
			<div class="posts object_list row flex flex_wrap">
				{foreach $last_articles as $article}
					<article class="post object_list_item flex_item col_xs_12 col_sm_6 col_md_4">
						<div class="post_inner object_list_inner shadow_box relative animated">
							<a class="post_image shadow_box_item text_center{if !$article->image} post_image_empty{/if}" href="article/{$article->url}">
								{if $article->image}
									<img src="{$article->image|resize:300:150:false:$config->resized_blog_dir}" alt="{$article->name|escape}" title="{$article->name|escape}"/>
								{/if}
							</a>
							<div class="post_text shadow_box_item">
								<h3 class="post_title text_4 text_normal"><a href="article/{$article->url}" data-post="{$article->id}">{$article->name|escape}</a></h3>
								{if $article->annotation}<div class="post_annotation text_gray text_5">{$article->annotation|strip_tags|truncate:80}</div>{/if}
							</div>
							<div class="post_footer shadow_box_item no_border flex flex_align_center flex_content_justify">
								<a class="post_link text_pink text_5" href="blog/{$article->url}">
									<span class="link_inverse inline_block">Подробнее</span>&nbsp;
									<i class="inline_block_middle">{include file="svg.tpl" svgId="icon_arrow_right" width="12px" height="12px"}</i>
								</a>
								<time class="text_5 text_gray2" datetime="{$article->date}">{$article->date|date}</time>
							</div>
						</div>
					</article>
				{/foreach}
			</div>
		{/if}
	</div>
</section>

{get_comments var=main_comments limit=5}
{if $main_comments}
<section class="main_comment section">
	<div class="container">
		<div class="section_title">
			<h2 class="title">Отзывы клиентов</h2>
			<a class="section_title_link link text_5 text_gray2" href="/comments">Все отзывы</a>
		</div>
		<ul id="main_comment" class="main_comment_list">
			{foreach $main_comments as $comment}
			{if $comment->approved}
				<li class="main_comment_item">	
					<div class="comment_list_header row">
						<div class="comment_list_name col_sm_9 col_md_10">
							<span class="text_black text_bold">{if $comment->name}{$comment->name|escape}{else}Анонимный пользователь{/if}</span>
						</div>
						<div class="comment_list_rating col_sm_3 col_md_2">
							<span class="rating_starOff">
								<span class="rating_starOn" style="width:{$comment->rate*85/5|string_format:'%.0f'}px"></span>
							</span>
						</div>
						
					</div>
					<div class="comment_list_body row"> 
						<div class="col_sm_3 col_md_2">
							<div class="inline_block text_center">
								<span class="comment_list_photo hidden_xs"></span>
								<time class="text_gray2 text_6" datetime="{$comment->date}">{$comment->date|date}</time>
							</div>
						</div>
						<div class="col_sm_9 col_md_10">
							{if $comment->text}
								<div class="comment_list_text">
									<div class="user_content text_5">{$comment->text|escape|nl2br}</div>
								</div>
							{/if}
							{if $comment->dost}
								<div class="comment_list_text">
									<div class="text_bold text_green">Достоинства:</div>
									<div class="user_content text_5">{$comment->dost|escape|nl2br}</div>
								</div>
							{/if}
							{if $comment->nedost}
								<div class="comment_list_text">
									<div class="text_bold text_red">Недостатки:</div>
									<div class="user_content text_5">{$comment->nedost|escape|nl2br}</div>
								</div>
							{/if}
						</div>
					</div>
				</li>
			{/if}
			{/foreach}
		</ul>
	</div>
</section>
{/if}

<section class="section">
	<div class="container">
		{*<img src="design/{$settings->theme}/images/main_footer_slide.jpg" alt="" title="">*}
		{if $page->body}
			{*<div class="separator"></div>*}
			<div class="spoiler user_content text_5 text_gray">{$page->body}</div>
		{/if}
	</div>
</section>

{/strip}