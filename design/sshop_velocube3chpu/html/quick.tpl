{strip}
<div class="product_quickview product_single product form_modal">

	<div class="form_modal_row">
	
		<div class="relative">
		
			<div class="product_sku text_gray2 text_5{if !$product->variant->sku} hidden{/if}">
				Артикул: <span class="value">{$product->variant->sku}</span>
			</div>

			<div class="product_name text_4 text_black" data-product_name="{$product->fullname|escape}">
				<span class="text_2 text_bold">{$product->fullname|escape}</span>
			</div>
			
			<div class="rating inline_block_middle" >
				
				<span class="rating_starOff">
					{*Рейтинг на основе отзывов*}
					{if $product->comcon > 0}
						<span class="rating_starOn" style="width:{$product->comrate*85/5|string_format:'%.0f'}px"></span>
					{*Рейтинг, заданный в админке*}
					{elseif $product->rating > 0}
						<span class="rating_starOn" style="width:{$product->rating*85/5|string_format:'%.0f'}px"></span>
					{else}
						<span class="rating_starOn"></span>
					{/if}
				</span>
			</div>
							
		</div>
		
	</div>
	
	<div class="form_modal_row">
	<div class="row">
	
		<div class="col_xs_12 col_md_6 col_lg_7">

			<div id="product_gallery_modal" class="product_gallery product_image text_center" data-product_image="{$product->image->filename|resize:200:200}">
						
				{* Прикрепленные видео к товару *}
				{*foreach $product->videos as $video}
					<div class="product_gallery_item" data-thumb="design/{$settings->theme|escape}/images/icon_video.svg">
						<div class="product_gallery_video relative">
							<iframe class="absolute" src="https://www.youtube.com/embed/{$video->vid}" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				{/foreach*}
				
				{* Галерея изображений *}
				{foreach $product->images as $i=>$image}
					<div class="product_gallery_item{if $product->videos|count > 0 && $image@first} product_gallery_first{/if}" data-thumb="{$image->filename|resize:60:40}">
						<a class="fancybox_gallery" href="{$image->filename|resize:800:600}" data-fancybox-group="gallery">
							<img src="{$image->filename|resize:600:400}" alt="{$product->name|escape}" />
						</a>
					</div>
				{/foreach}
				
			</div>
			
		</div>
		
		<div class="product_info col_xs_12 col_md_6 col_lg_5">
			<form class="variants" action="/cart">
				
				<div id="product_stock" class="product_stock inline_block_middle text_bold text_6 uppercase">
					{if $product->variant->stock > 0 && $product->variant->stock <= $settings->interval_min}
						<span class="text_orange">Заканчивается</span>
					{elseif $product->variant->stock == 0 && $product->sklad_id == 0 }
						<span class="text_red">Нет в наличии</span>
					{elseif $product->sklad_id == 2}
						<span class="text_pink">Под заказ</span> 
					{elseif $product->sklad_id == 4}
						<span class="text_pink">Предзаказ</span> 
					{elseif $product->sklad_id == 3}
						<span class="text_yellow">Ожидается {if $product->byrequest}( {$product->byrequest} ){/if} 
					{else}
						<span class="text_green">В наличии</span> 
					{/if}
				</div>
					
				<span class="compare_link inline_block_middle text_5 text_gray2 text_right">
					{if $compare->ids && in_array($product->id, $compare->ids)}
						<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
						<span class="compare_link_text inline_block_middle">в сравнении</span>
					{else}
						<a href="/compare?id={$product->id}" class="compare_add" data-id="{$product->id}" data-key="compare" data-informer="1" data-result-text="в сравнении">
							<i class="compare_link_icon inline_block_middle">{include file="svg.tpl" svgId="icon_compare" width="18px" height="18px"}</i>&nbsp;
							<span class="compare_link_text inline_block_middle">сравнить</span>
						</a>
					{/if}
				</span>
						
				{* Контейнер с ценами *}
				<div class="price-container price-container--expanded js-pricelist{if $product->variant->price == 0} hidden{/if}">
                
                    <div class="price old_price text_3 text_bold text_gray2 js-old-price-container{if !$product->variant->compare_price} hidden{/if}">
						<span class="js-old-price">
						    {$product->variant->compare_price|convert}
						</span>&nbsp;
						{$currency->sign|escape}
					</div>
                
                    <div class="price final_price text_bold text_black">
						<span class="js-base-price">{$product->variant->price|convert}</span>&nbsp;
						<span class="currency">{$currency->sign|escape}</span>
					</div>
                    
                    {assign 'discount_user' 0}
                    {assign 'discount_shop' 0}
                    {if $user_group->discount}
                        {$discount_user = $user_group->discount|string_format:"%d"}
                    {/if}
                    {if $product->variant->coupon_discount_price}
                        {$discount_shop = ($product->variant->coupon_discount_price / $product->variant->price * 100)}
                    {/if}
                    
                    {if $user_group->discount || $product->variant->coupon_discount_price}
                        <div class="sale-info">
                            <button class="sale-info__label" type="button">+Скидка <span class="sale-info__label-value">{$discount_user + $discount_shop}%</span> при покупке сейчас</button>
                            <div class="sale-info__detail">
                                <span class="sale-info__title">
                                    <span class="hidden_xs">Итоговая цена*</span>
                                    <span class="visible_xs_inline">Цена:</span>
                                </span>
                                <span class="sale-info__price price">
                                    <span class="js-final-price">
                                        {($product->variant->price - $product->variant->coupon_discount_price - ($product->variant->price * $discount_user / 100))|convert}
                                    </span>&nbsp;
                                    <span class="currency">{$currency->sign|escape}</span>
                                </span>
                                <div class="sale-info__description hidden_xs">
                                    {if $discount_user > 0}
                                        <div>{$discount_user}% личная скидка</div>
                                    {/if}
                                    {if $discount_shop > 0}
                                        <div>{$discount_shop}% скидка магазина</div>
                                    {/if}
                                </div>
                                <div class="sale-info__annotation hidden_xs">*Цена автоматически будет применена в корзине</div>
                            </div>
                        </div>
                    {/if}
                   
				</div>
				
				<div class="variants_container{if $product->variants|count<2} hidden{/if}">
					{foreach $product->variants as $v}
						<div class="variant">
							<input id="product_{$v->id}" class="hidden" name="variant" value="{$v->id}" type="radio" 
								
								{if $product->variant->id==$v->id}checked {/if} 
										
								{if $v->sku}data-sku='{$v->sku}'{/if}
								
								{if $v->name}data-v_name="1"{/if}
								
								data-pricelist="dropdown"
								
								data-price="{$v->price|convert|replace:' ':''}"
                                
                                {if $v->compare_price}
									data-old_price="{$v->compare_price|convert|replace:' ':''}"
								{/if} 
                                
                                data-discount_user="{$discount_user}"
                                
                                data-discount_shop="{$discount_shop}"
                                
								data-stock='{if $v->stock > 0 && $v->stock <= $settings->interval_min}<span class="text_orange">Заканчивается</span>
											{elseif $v->stock == 0 && $product->sklad_id == 0 }<span class="text_red">Нет в наличии</span>
											{elseif $product->sklad_id == 2}<span class="text_pink">Под заказ</span>
											{elseif $product->sklad_id == 4}<span class="text_pink">Предзаказ</span>
											{elseif $product->sklad_id == 3}<span class="text_yellow">Ожидается
											{else}<span class="text_green">В наличии</span>{/if}'

								{if $v->image_id}
								data-image_id="{$v->image_id}"
								{/if}

                            />
							<label for="product_{$v->id}" class="variant_name checkbox_label">{$v->name}</label>
						</div>
					{/foreach}
				</div>
					
				<div class="button_container{if $product->variant->price == 0} hidden{/if}">
					
					<div class="clear">
						<div class="product_amount amount text_nowrap fleft">
							<span class="amount_trigger minus text_gray2 text_bold">-</span>
							<input class="amount_count form_input" type="tel" name="amount" value="1" data-max="{$product->variant->stock}">
							<span class="amount_trigger plus text_gray2 text_bold">+</span>
						</div>
						<button class="product_button fright button" type="submit">
							<span class="button_item icon_text">Купить</span>
							<span class="button_item icon hidden_xs">{include file="svg.tpl" svgId="icon_cart"}</span>
						</button>
					</div>
						
				</div>
			
			</form>
			
		</div>
	
	</div>
	</div>
	
</div>
{/strip}