{strip}
{* Шаблон корзины *}

{$meta_title = "Корзина" scope=parent}
	
{if $cart->purchases}
	<main class="main shadow_section">
	<form id="cart_form" class="container" method="post" name="cart">
	
		{include file="path.tpl"}
		
		{* Товары в корзине *}
	
		<div class="cart-table shadow_box">
		
			<div class="cart-table__row cart-table__row--title shadow_box_item flex flex_align_center text_white hidden_xs hidden_sm hidden_md">
				<div class="cart-table__col col_xs_auto cart-table__image text_center">Фото</div>
				<div class="cart-table__col col_xs_auto cart-table__name">Наименование</div>
				<div class="cart-table__col col_xs_auto cart-table__stock">Наличие</div>
				<div class="cart-table__col col_xs_auto cart-table__amount text_center">Кол-во</div>
				<div class="cart-table__col col_xs_auto cart-table__price">Цена</div>
				<div class="cart-table__col col_xs_auto cart-table__sale">Скидка</div>
				<div class="cart-table__col col_xs_auto cart-table__price">Итого</div>
				<div class="cart-table__col col_xs_auto cart-table__remove"></div>
			</div>
			
			{foreach from=$cart->purchases item=purchase}
			<div class="cart-table__row cart-table__item shadow_box_item relative flex flex_align_center">
				
				{* Изображение товара *}
				<div class="cart-table__col cart-table__image col_xs_5 col_sm_4 col_md_2 text_center">
					{$variant_image = $purchase->product->images[$purchase->variant->image_id]}
					{if $variant_image}
						<a href="products/{$purchase->product->url}"><img src="{$variant_image->filename|resize:130:90}" alt="{$purchase->product->name|escape}"></a>
					{else}
						{$image = $purchase->product->images|first}
						<a href="products/{$purchase->product->url}"><img src="{$image->filename|resize:130:90}" alt="{$purchase->product->name|escape}"></a>
					{/if}
				</div>
					
				{* Название товара *}
				<div class="cart-table__col cart-table__name col_xs_6 col_sm_8 col_md_4">
					<a href="products/{$purchase->product->url}" class="cart_product_fullname text_bold text_black">{$purchase->product->fullname|escape}</a>
					{if $purchase->variant->name}
						<div class="text_6 text_gray">{$purchase->variant->name|escape}</div>
					{/if}
				</div>
				
				{*Наличие товара*}
				<div class="cart-table__col cart-table__stock text_nowrap col_xs_7 col_sm_8 col_md_3 offset_xs_5 offset_sm_4 offset_md_2 offset_lg_0">
				{if $purchase->variant->stock > 0 && $purchase->variant->stock <= $settings->interval_min}
					<div class="text_6 text_orange">
					    <span class="icon"><img src="design/{$settings->theme}/images/icon-stock-warning.svg" alt=""></span>
					    <span class="icon_text" style="margin-left: 5px;">Заканчивается</span>
                    </div>
				{elseif ($purchase->variant->stock == 0 && $purchase->sklad_id == 0) || $purchase->sklad_id == 1}
					<div class="text_6 text_red">
						<span class="icon" style="margin-top: -1px;">{include file="svg.tpl" svgId="icon_warning" width="16px" height="16px"}</span>
						<span class="icon_text">Нет в наличии</span>
					</div>
				{elseif $purchase->sklad_id == 2}
					<div class="text_6 text_pink">Под заказ</div> 
				{elseif $purchase->sklad_id == 4}
					<div class="text_6 text_pink">Предзаказ</div> 
				{elseif $purchase->sklad_id == 3}
					<div class="text_6 text_yellow">Ожидается {if $purchase->byrequest}( {$purchase->byrequest} ){/if} 
				{else}
					<div class="text_6 text_green">
						<span class="icon"><img src="design/{$settings->theme}/images/icon-stock-success.svg" alt=""></span>
						<span class="icon_text" style="margin-left: 5px;">В наличии</span>
					</div>
				{/if}
				</div>
				
				{* Количество *}
				<div class="cart-table__col cart-table__amount col_xs_5 col_sm_4 col_md_3 text_center">
					<div class="amount text_nowrap">
						<span class="amount_trigger minus text_gray2 text_bold">-</span>
						<input class="amount_count form_input" name="amounts[{$purchase->variant->id}]" type="tel" value="{$purchase->amount}" onchange="document.cart.submit();" data-max="{$purchase->variant->stock}"/>
						<span class="amount_trigger plus text_gray2 text_bold">+</span>
					</div>
				</div>
				
				<div class="cart-table__col cart-table__price col_xs_auto hidden_xs hidden_sm hidden_md">
					<div class="price text_3 text_bold text_black">
						{*Если есть старая цена, то выводим ее*}
						{if $purchase->variant->compare_price}
							<span class="value">{($purchase->variant->compare_price*$purchase->amount)|convert}</span>&nbsp;
							<span class="currency">{$currency->sign|escape}</span>
						{*Если старой цены нет, но есть автоскидка или скидка пользователя, то выводим обычную цену вместо старой*}
						{else}
							<span class="value">{($purchase->variant->price*$purchase->amount)|convert}</span>&nbsp;
							<span class="currency">{$currency->sign|escape}</span>
						{/if}
					</div>
				</div>
				
				{* Суммарная скидка по товару *}
				<div class="cart-table__col cart-table__sale col_xs_auto hidden_xs hidden_sm hidden_md">
					<div class="price text_3 text_bold text_gray2">
						{if $purchase->variant->compare_price}
							<span class="value">- {(( ($purchase->variant->compare_price - $purchase->variant->price) + ($purchase->variant->price * $user_group->discount/100) + $purchase->variant->coupon_discount_price )*$purchase->amount)|convert}</span>&nbsp;
							<span class="currency">{$currency->sign|escape}</span>
						{elseif $purchase->variant->coupon_discount_price || $user->discount}
							<span class="value">- {(( ($purchase->variant->price * $user_group->discount/100) + $purchase->variant->coupon_discount_price )*$purchase->amount)|convert}</span>&nbsp;
							<span class="currency">{$currency->sign|escape}</span>
						{/if}
						
						{if $purchase->variant->auto_coupon_price}
							<input type="hidden" name="auto_coupon[{$purchase->variant->id}]" value="{$purchase->variant->auto_coupon_id}" />
						{/if}
					</div>
				</div>

				{* Итоговая цена товара *}
				<div class="cart-table__col cart-table__price cart-table__price--final col_xs_7 col_sm_8 col_md_2">
					<div class="price text_3 text_bold text_black">
						<span class="value">{(($purchase->variant->price - $purchase->variant->coupon_discount_price - ($purchase->variant->price * $user_group->discount/100))*$purchase->amount)|convert}</span>&nbsp;
						<span class="currency">{$currency->sign|escape}</span>
					</div>
				</div>
				
				<div class="cart-table__col cart-table__remove col_md_1">
					<a class="icon text_red" href="cart/remove/{$purchase->variant->id}">{include file="svg.tpl" svgId="icon_close" width="16px" height="16px"}</a>
				</div>		
			</div>
			{/foreach}
			
			{if $cart->coupon_discount>0}
			<div class="cart_total_coupon shadow_box_item text_center text_black">
				Скидка по купону:&nbsp;&nbsp;
				<span class="text_3 text_bold">
					<span class="value">- {$cart->coupon_discount|convert}</span>&nbsp;
					<span class="currency">{$currency->sign}</span>
				</span>
			</div>
			{/if}
			
			{* Итоговая цена заказа *}
			<div class="cart_total_price shadow_box_item text_1 text_center text_bold text_black">
				Итого:&nbsp;
				<span class="value">{$cart->total_price|convert}</span>&nbsp;
				<span class="currency">{$currency->sign}</span>
			</div>
					
		</div>
		
		<div class="row">
			{* Блок с купоном *}
			<div class="coupon_container col_xs_12 col_sm_7 col_md_8 relative">
				<span class="coupon_link link pseudo_link text_pink text_5 trigger" data-trigger_action="slide">Есть купон на скидку?</span>
				<div class="coupon_form shadow_box text_left">
					<div class="shadow_box_item">
						<div class="row">
							{if $coupon_error}
								<div class="col_xs_12">
									<div class="notice notice_alert text_5">
										{if $coupon_error == 'invalid'}Купон недействителен{/if}
									</div>
								</div>
							{elseif $cart->coupon->min_order_price>0 && ($cart->coupon->min_order_price > $cart->total_price)}
								<div class="col_xs_12">
									<div class="notice text_5">купон <strong>{$cart->coupon->code|escape}</strong> действует для заказов от {$cart->coupon->min_order_price|convert} {$currency->sign}</div>
								</div>
							{elseif $smarty.session.bad_coupon}
								<div class="col_xs_12">
									<div class="notice notice_alert text_5">(для купона не выполнены условия)</div>
								</div>
							{elseif $cart->coupon_discount>0}
								<div class="col_xs_12">
									<div class="notice text_5">купон <strong>{$cart->coupon->code|escape}</strong> успешно применен</div>
								</div>
							{/if}
							<div class="form_group col_sm_6 col_md_8 col_lg_9">
								<input class="form_input" type="text" name="coupon_code" value="{$cart->coupon->code|escape}" placeholder="Введите код купона"/>
							</div>
							<div class="col_sm_6 col_md_4 col_lg_3">
								<input class="button button_block button_light" type="button" name="apply_coupon"  value="Применить" onclick="document.cart.submit();">
							</div>
						</div>
					</div>
				</div>	
			</div>
			
			{* Ссылка на экспресс заказ *}
			<div class="cart_fast_order col_xs_12 col_sm_5 col_md_4">
				<a id="cart_fast_order_link" class="link pseudo_link text_blue text_5 fancybox" href="#cart_fast_order_form">Нет времени на оформление?</a>		
			</div>
		
		</div>
		
		{* Блок оформления заказа *}
		<div class="cart_bill">
		
			{* Выбор доставки *}
			{if $deliveries}
				<div id="deliveries" class="cart_section">
					<h2 class="title text_blue2 text_bold text_left">Выберите способ доставки:</h2>
					<ul class="bill_list shadow_box">
						{foreach $deliveries as $delivery}
						<li class="bill_item shadow_box_item {if $delivery@first} active{/if}">
							<input id="delivery_{$delivery->id}" class="hidden" type="radio" name="delivery_id" value="{$delivery->id}" onclick="change_payment_method({$delivery->id})" {if $delivery_id==$delivery->id}checked{elseif $delivery@first}checked{/if}>
							
							<label for="delivery_{$delivery->id}" class="bill_title text_3 text_bold text_black checkbox_label radio">
								{$delivery->name}
									<span class="bill_tax">
									{if $cart->total_price < $delivery->free_from && $delivery->price>0}
										<span class="price">{$delivery->price|convert}</span>&nbsp;<span class="currency">{$currency->sign}</span>
									{elseif $cart->total_price >= $delivery->free_from}
										<span class="text_label text_label_success"><span class="text_5">бесплатно</span></span>
									{/if}
									</span>
							</label>
							{if $delivery->description}
								<div class="bill_description text_5 text_gray"{if $delivery@first} style="display: block;"{/if}>{$delivery->description}</div>
							{/if}
						</li>
						{/foreach}
					</ul>
				</div>
			{/if}
			{* /Выбор доставки *}
			
			{* Выбор оплаты *}
			<div id="payments" class="cart_section">
				<h2 class="title text_blue2 text_bold text_left">Выберите способ оплаты:</h2>
				{foreach $deliveries as $delivery}
					{if $delivery->payment_methods}             
						<ul id="payment_{$delivery->id}" class="bill_list shadow_box hidden">
							{foreach  $delivery->payment_methods as $payment_method}
								<li class="bill_item shadow_box_item{if $payment_method@first} active{/if}">
									<input id="payment_{$delivery->id}_{$payment_method->id}" class="hidden" {if $payment_method@first}checked{/if} type="radio" name="payment_method_id" value="{$payment_method->id}"/>
									<label for="payment_{$delivery->id}_{$payment_method->id}" class="bill_title text_3 text_bold text_black checkbox_label radio">
										{$payment_method->name}
										{*
										{$total_price_with_delivery = $cart->total_price}
										{if !$delivery->separate_payment && $cart->total_price < $delivery->free_from}
											{$total_price_with_delivery = $cart->total_price + $delivery->price}
										{/if}
										<span class="bill_tax text_3 text_black">
											<span class="price">{$total_price_with_delivery|convert:$payment_method->currency_id}</span>&nbsp;<span class="currency">{$all_currencies[$payment_method->currency_id]->sign}</span>
										</span>	
										*}
									</label>
									{if $payment_method->description}
										<div class="bill_description text_5 text_gray"{if $payment_method@first} style="display: block;"{/if}>{$payment_method->description}</div>
									{/if}
								</li>
							{/foreach}
						</ul>
					{/if}
				{/foreach}
			</div>
			{* /Выбор оплаты *}
		
			{* Блок с реквизитами *}
			<div class="cart_feedback shadow_box">
				
				<div class="form form_single">
				
					<h2 class="form_group text_2 text_black text_center text_bold">Укажите реквизиты получателя:</h2>
				
					{if $error}
						<div class="notice notice_alert">
							<div class="notice_title">Внимание!</div>
							{if $error == 'empty_name'}Введите имя{/if}
							{if $error == 'empty_email'}Введите email{/if}
							{if $error == 'empty_phone'}Введите телефон{/if}
                            {if $error == 'captcha'}Неверная капча{/if}
						</div>
					{/if}
					
					<div class="form_group">
						<label for="cart_name" class="form_label">Имя: *</label>
						<input id="cart_name" class="form_input" name="name" type="text" value="{$name|escape}" data-format=".+" data-notice="Введите имя"/>
					</div>
					
					<div class="form_group">
						<label for="cart_name" class="form_label">Фамилия:</label>
						<input id="cart_name" class="form_input" name="surname" type="text" value="{$surname|escape}"/>
					</div>
					
					<div id="passport_field" class="form_group" style="display: none;">
						<label for="cart_passport" class="form_label">Паспортные данные (для Транспортной компании):</label>
						<input id="cart_passport" class="form_input" name="passport" type="text" value="{$passport|escape}" />
					</div> 
					
					<div class="form_group">
						<label for="cart_email" class="form_label">Адрес электронной почты: *</label>
						<input id="cart_email" class="form_input" name="email" type="text" value="{$email|escape}" data-format="email" data-notice="Введите email" />
						<div class="text_gray2 text_5">На него будут отправляться письма со статусом заказа</div>
					</div>
					
					<div class="form_group">
						<label for="cart_phone" class="form_label">Номер телефона: *</label>
						<input id="cart_phone" class="form_input phone_mask" name="phone" type="text" value="{$phone|escape}" data-format=".+" data-notice="Введите телефон" />
					</div>
					
					<div class="form_group">
						<a id="second_phone_link" class="link pseudo_link text_pink text_5 trigger trigger_anchor" data-trigger_action="slide" data-trigger_active_text="Убрать второй телефон" data-trigger_default_text="Добавить второй телефон" href="#second_phone_field">Добавить второй телефон</a>
					</div>
					
					<div id="second_phone_field" class="form_group" style="display: none;">
						<label for="cart_second_phone" class="form_label">Второй номер телефона:</label>
						<input id="cart_second_phone" class="form_input phone_mask" name="phone2" type="text" value="{$phone2|escape}"/>
					</div>   					
					
					<div id="address_field" class="form_group">
						<label for="cart_address" class="form_label">Адрес доставки:</label>
						<input id="cart_address" class="form_input" name="address" type="text" value="{$address|escape}"/>
					</div>
					
					<div class="form_group">
						<label for="cart_comment" class="form_label">Комментарий к заказу: </label>
						<textarea id="cart_comment" class="form_input" name="comment" rows="3">{$comment|escape}</textarea>
					</div>
					
                    {*<div class="form_group">
                        <div id="recaptcha_cart"></div>        
                        <input id="g-recaptcha_cart" type="text" name="g-recaptcha-response" value="" required="required"/>
                    </div>*}
                        
					<div class="form_group text_center">
						<input type="hidden" name="checkout" value="0"/>
						<button class="button button_large js-cart-submit" type="submit" >
							<span class="button_text button_text_default">Оформить заказ</span>
							<span class="button_text button_text_loading hidden">
								<span class="button_item icon">{include file="svg.tpl" svgId="icon_arrow_circle"}</span>
								<span class="button_item icon_text">Оформляем заказ</span>
							</span>
						</button>
					</div>
					
					<div class="text_gray2 text_5">* - обязательные поля для заполнения</div>
					
				</div>
			</div>
			{* /Блок с реквизитами *}
			
		</div>
		{* /Блок оформления заказа *}
	
	</form>
	</main>
	
{else}
	<div class="main page_empty container flex flex_align_center flex_content_center">
		<div class="shadow_box">
			<h2 class="shadow_box_item text_center">Ничего не нашли?</h2>
			<div class="shadow_box_item">
				Возьмите купон на скидку 5% &ndash; <strong class="text_3 text_blue">Emptycart-5</strong><br>
				и попробуйте еще раз
			</div>
			<div class="shadow_box_item">
				<a class="button button_block button_large" href="/">Вернуться на главную</a>
			</div>
		</div>
	</div>
{/if}

{*cart_oneclick modal*}
<div class="hidden">
	<div id="cart_fast_order_form" class="form_modal">
				
		<div class="form_modal_row form_modal_title text_2 text_bold text_center">Экспресс заказ</div>
		<form name="fast-order" method="POST" action="/cart">
			<div class="form_modal_row">
				<input type="hidden" name="IsFastOrder" value="true">   
				<div id="for_export_inputs" style="display:none"></div>
				<div class="notice notice_alert" style="display: none;">Укажите ваше имя и номер телефона</div>
				<div class="form_group">
					<label for="oneclick_name" class="form_label">Ваше имя: *</label>
					<input id="oneclick_name" class="form_input" name="name" value="" type="text"/>
				</div>
				<div class="form_group">
					<label for="oneclick_phone" class="form_label">Номер телефона: *</label>
					<input id="oneclick_phone" class="form_input phone_mask" name="phone" value="" type="tel"/>
				</div>
			</div>
			<div class="form_modal_row">
				<button id="fastorder_buy" class="button button_block button_large" type="submit" name="IsFastOrder" value="Купить!">Купить!</button>	
			</div>
		</form>
	</div>
</div>
{*/cart_oneclick modal/*}

{/strip}