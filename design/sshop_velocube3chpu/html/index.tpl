{strip}
<!DOCTYPE html>
<html>
<head>
	<base href="{$config->root_url}/"/>
    <meta name="w1-verification" content="165372153651" />  
    
    <title>{$meta_title|escape} {* chpu_filter_extended *}{$filter_meta->title|escape}{* chpu_filter_extended /*}{if $current_page_num > 1} - страница {$current_page_num}{/if}</title>
    <meta name="description" content="{$meta_description|escape} {* chpu_filter_extended *}{$filter_meta->description|escape}{* chpu_filter_extended /*}" />
	{*SLAVA*}<meta name="keywords" content="{$meta_keywords|escape} {* chpu_filter_extended *}{$filter_meta->keywords|escape}{* chpu_filter_extended /*}" /> {*SLAVA*}
	{*
	{{if {$brand->name|escape} !="" && $module != 'ProductView'}
		<title>►{$category->name|escape} {$brand->name|escape} - купить в Москве | {$brand->name|escape} недорого в интернет-магазине Velocube.ru</title>
		<meta name="description" content="►Большой выбор товаров {$category->name|escape} {$brand->name|escape} от магазина «Velocube.ru».✔Бесплатная доставка по Москве!✔Большой ассортимент {$brand->name|escape} от известных брендов! ☎ Звоните:+7 (499) 653-88-38" />
	{elseif $module == 'ProductView'}       
		<title>►{$product->fullname} - купить в Москве | Цена на {$product->fullname} от Velocube.ru</title>
		<meta name="description" content="►{$product->fullname} в интернет-магазине Velocube.ru по недорогой цене.✔Бесплатная доставка!✔Подробное описание, технические характеристики, фотографии, отзывы покупателей. ☎Звоните:+7 (499) 653-88-38" />
	{elseif $module == 'ProductsView'}
		<title>►{$category->name|escape} - купить в Москве | {$category->name|escape} недорого в интернет-магазине Velocube.ru</title>
		<meta name="description" content="►{$category->name|escape} недорого от магазина «Velocube.ru».✔Бесплатная доставка по Москве!✔ Большой ассортимент {$category->name|escape} от известных брендов! ☎ Звоните:+7 (499) 653-88-38" />
	{else}
		<title>►{$meta_title|escape}</title>
		<meta name="description" content="►{$meta_description|escape}" />
	{/if}                           
	*}
	
	{if $module == 'ProductView'}
		<meta property="og:url" content="{$canonical}" />
		<meta property="og:type" content="product">
		<meta property="og:title" content="{*$product->fullname|escape*}{$meta_title|escape}" />
		<meta property="og:image" content="{$product->image->filename|resize:600:400}" />
		<meta property="og:description" content='{*$product->annotation*}{$meta_description|escape}' />
		<link rel="image_src" href="{$product->image->filename|resize:600:400}" />
	{/if}
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<link href="{$config->root_url}/favicon.ico" rel="icon" type="image/x-icon"/>
	<link href="{$config->root_url}/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	{* Канонический адрес страницы *}
	{if isset($canonical)}<link rel="canonical" href="{$canonical}"/>{/if}
	
	{* Стили *}
    <link href="design/{$settings->theme|escape}/css/reset.min.css" rel="stylesheet"/>
	<link href="design/{$settings->theme|escape}/css/grid.css" rel="stylesheet"/>
	<link href="design/{$settings->theme|escape}/css/ui.css" rel="stylesheet"/>
	<link href="design/{$settings->theme|escape}/css/style.css" rel="stylesheet"/> 
	<link href="design/{$settings->theme|escape}/css/responsive.css" rel="stylesheet"/> 
	{*Новогодняя тема*}
	{*<link href="design/{$settings->theme|escape}/css/ny.css" rel="stylesheet"/>*}
	
    {*<script src='https://www.google.com/recaptcha/api.js'></script>*}
</head>
<body>
	
	<header id="header" class="header absolute">
			
		{* Меню главных старниц *}
		<nav class="header_menu text_inverse">
		<div class="container row">
			<a href="#header_menu" class="header_menu_trigger button button_light button_small trigger trigger_anchor close_outside hidden_lg">Меню</a>
			<div class="header_menu_inner trigger_container close_outside animated">
				<ul id="header_menu" class="header_menu_list trigger_content flex_content_justify text_nowrap">
				
					<!-- noindex -->
					{if $user}
						<li class="header_account_item header_menu_item">
							<a class="header_menu_link text_yellow" href="user">Личный кабинет</a>
						</li>
						<li class="header_account_item header_menu_item">
							<a class="header_menu_link text_yellow" href="user/logout">Выйти</a>
						</li>
					{else}
						<li class="header_account_item header_menu_item">
							<a class="header_menu_link text_yellow" href="user/register">Регистрация</a>
						</li>
						<li class="header_account_item header_menu_item">
							<a class="header_menu_link text_yellow" href="user/login">Вход</a>
						</li>
					{/if}
					<!-- /noindex -->
					
					{foreach $pages as $p}
						{if $p->menu_id == 1}
							<li class="header_menu_item{if $page && $page->id == $p->id} active{/if}">
								<a class="header_menu_link" href="{$p->url}" data-page="{$p->id}">{$p->name|escape}</a>
							</li>
						{/if}
					{/foreach}
					
				</ul>
			</div>
		</div>
		</nav>
		
		<div class="header_content flex flex_wrap flex_align_center container row">
		
			<a href="./" class="header_logo col_xs_5 col_sm_4 col_md_auto flex_item">
				<img src="design/{$settings->theme|escape}/images/logo.svg" alt="Velocube.ru" title="Velocube.ru"/>
				{*Новогодний логотип*}
				{*<img src="design/{$settings->theme|escape}/images/ny/logo_ny.png" alt="Velocube.ru" title="Velocube.ru"/>*}
			</a>
			
			<div class="header_contact col_xs_7 col_sm_8 col_md_auto col_lg_3 flex_item text_nowrap text_center">
				<a href="tel:+74996538838" class="text_2">+7 (499) 653-88-38</a>
				<div class="text_6 text_gray2 uppercase">c 9:00 до 22:00 без выходных</div>
				<a href="#callback_form" class="callme_viewform link pseudo_link text_blue fancybox">Заказать звонок</a>
			</div>
			
			<div class="header_separator col_xs_12 flex_item hidden_md hidden_lg"></div>
			
			<a id="category_menu_trigger" class="category_menu_trigger col_xs_5 col_sm_4 col_md_auto flex_item text_nowrap trigger trigger_anchor close_outside animated" href="#category_menu">
				<span class="icon icon_menu">
					<span class="icon_menu_line line_1"></span>
					<span class="icon_menu_line line_2"></span>
					<span class="icon_menu_line line_3"></span>
				</span>
				<span class="icon_text text_blue2 text_5 uppercase text_bold animated">Каталог<br>товаров</span>
			</a>
		
			<div class="search_container absolute flex col_xs_3 col_sm_8 col_lg_4 flex_item trigger_container close_outside animated">
				{* Форма поиска *}
				<form class="search_form relative trigger_content" action="products">
					<input class="search_input form_input" type="text" name="keyword" placeholder="Поиск" value="" />
					<button class="search_submit absolute text_blue2" type="submit">
						{include file="svg.tpl" svgId="icon_search"}
					</button>
				</form>
				{* Триггер поиска *}
				<span class="search_trigger trigger close_outside visible_xs">
					<span class="search_open text_white animated">
						{include file="svg.tpl" svgId="icon_search"}
					</span>
					<span class="search_close text_red animated">
						{include file="svg.tpl" svgId="icon_close"}
					</span>
				</span>
			</div>
			
			<!-- noindex -->
			
			{* Информер сравнения *}
			{get_session_products key=compare}
			<div id="compare_informer" class="compare_informer header_informer static col_xs_auto flex_item">{include file='products_session_compare_informer.tpl' session=$compare}</div>
			
			{*Информер корзины*}
			<div id="cart_informer" class="cart_informer header_informer static trigger_container close_outside col_xs_auto flex_item">{include file='cart_informer.tpl'}</div>
			
			<!-- /noindex -->
			
		</div>
		
		{* Меню категорий *}
		<nav class="category_menu_container trigger_container close_outside absolute text_black">
			<div id="category_menu" class="category_menu trigger_content">
			{function name=categories_tree}
			{if $categories}
				<ul class="category_menu_list level_{$level}{if $level == 1} container{elseif $level == 2} trigger_content{/if}">
				{foreach $categories as $c}
					{if !$c->metka && $c->visible}
					<li class="category_menu_item level_{$level}{if $level == 1} animated{/if}{if $c->children|count > 1 && $level == 1} trigger_container{/if}{if $category->id == $c->id} active{/if}">
						<div class="category_menu_name level_{$level} relative {if $level == 1} animated{/if}{if $c->id == 524} category_menu_name--sale{/if}">
							{if $c->children|count > 1 && $level == 1}
								<span class="category_menu_icon level_{$level} absolute icon icon_link trigger hidden_lg{if $category->id == $c->id} active{/if}" data-trigger_action="slide">
									<span class="icon icon_more">
										<span class="icon_more_line line_1"></span>
										<span class="icon_more_line line_2"></span>
									</span>
								</span>
							{/if}
							<a class="category_menu_link level_{$level}{if $level > 1} link_inverse{/if}" href="catalog/{$c->url}">
								{if $c->short_name}
									{$c->short_name|escape}
								{else}
									{$c->name|escape}
								{/if}
							</a>
						</div>
						{categories_tree categories=$c->subcategories level=$level+1}
					</li>
					{/if}
				{/foreach}
				</ul>
			{/if}
			{/function}
			{categories_tree categories=$categories level=1}
		</nav>
		{* /Меню категорий *}
		
	</header>
	
	{$content}
	
	<footer class="footer text_inverse">
		<div class="footer_content">
			<div class="container row flex flex_wrap">
			
				<div class="footer_general col_xs_12 col_sm_8 col_md_9 flex_item text_5">

					<div class="footer_logo footer_content_item">
						<a href="./" class="footer_logo_image inline_block text_center">
							<img src="design/{$settings->theme|escape}/images/logo_dark.svg" alt="Velocube.ru" title="Velocube.ru"/>
							<span class="footer_sitename">
								Интернет магазин - <span class="text_white uppercase">Velocube.ru</span>
							</span>
						</a>
					</div>
					
					<ul class="footer_menu footer_content_item">
						{foreach $pages as $p}
						{if $p->menu_id == 3}
							<li class="footer_menu_item inline_block">
								<a class="footer_menu_link" href="{$p->url}">{$p->name|escape}</a>
							</li>
						{/if}
					{/foreach}
					</ul>

				</div>
				
				<div class="footer_contact footer_content_item flex_item col_xs_12 col_sm_auto">
					<div class="text_white text_3 text_bold">Контакты:</div>
					<ul class="footer_contact_list">
						<li class="footer_contact_item">
							<a class="text_2 text_yellow" href="tel:+84996538838">8 (499) 653-88-38</a>
							<div class="text_6 uppercase">c 9:00 до 22:00 без выходных</div>
						</li>
						<li class="footer_contact_item">
							г. Москва
							<div class="text_5">ул Октябрьская, д.80, стр6</div>
						</li>
						<li class="footer_contact_item">
							<a class="button button_small button_dark" href="/contact">Обратная связь</a>
						</li>
					</ul>
				</div>
				
				<div class="footer_payment footer_content_item flex_item col_xs_12 col_lg_6">
					<div class="text_3 text_white text_bold">Мы принимаем к оплате:</div>
					<ul class="footer_payment_list">
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_sberbank" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_abank" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_visa" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_mastercard" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_webmoney" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_qiwi" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_yandex" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_mailru" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_cash" width="74px" height="40px"}</i>
						</li>
						<li class="footer_payment_item inline_block">
							<i class="icon">{include file="svg.tpl" svgId="icon_payment_paypal" width="74px" height="40px"}</i>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
		
		<div class="footer_bottom text_5">
			<div class="container row">
				<div class="copyright footer_bottom_item col_xs_12 col_sm_6">&copy; 2013 - {$smarty.now|date_format:"%Y"}. Velocube.ru</div>
				<div class="footer_social footer_bottom_item col_xs_12 col_sm_6">
					Мы в социальных сетях
					<a class="icon icon_vk" href="https://vk.com/clubvelocube" target="_blank">
						{include file="svg.tpl" svgId="icon_vk" width="16px" height="16px"}
					</a>
					<i class="icon icon_fb disabled">
						{include file="svg.tpl" svgId="icon_fb" width="16px" height="16px"}
					</i>
					<i class="icon icon_tw disabled">
						{include file="svg.tpl" svgId="icon_tw" width="16px" height="16px"}
					</i>
				</div>
			</div>
		</div>
		
		<div id="moveup" class="moveup_link fixed">
			<div class="moveup_icon animated">
				<span class="moveup_line moveup_line_1 animated"></span>
				<span class="moveup_line moveup_line_2 animated"></span>
			</div>
			<div class="moveup_text text_5 text_black text_center uppercase animated">Наверх</div>
		</div>
		
	</footer>	
	
	<!-- noindex -->
	<div class="hidden">
	
		{*social-registration*}
		{if !$user && ($module == 'LoginView' || $module == 'RegisterView')}
			{*форма регистрации через соц.сети*}
			<form method="post" id="ulogin_form" name="ulogin_form" action="{$config->root_url}/user/login">
				<input type="hidden" name="form_type" value="ulogin">
				<input id="ulogin_first_name" type="text" name="first_name">
				<input id="ulogin_last_name" type="text" name="last_name">
				<input id="ulogin_email" type="text" name="email">
				<input id="ulogin_phone" type="text" name="phone">
				<input id="ulogin_password" type="text" name="password">
				<input type="submit" value="">
			</form>
			{*/форма регистрации через соц.сети*}
		{/if}
		{*/social-registration*}
		
		{if $smarty.get.module == 'ProductView'}
			
			{*oneclick buy*}
			<div id="oneclick_form" class="form_modal">
				<div class="form_modal_row form_modal_title text_2 text_bold text_center">Купить {$product->name|escape|rtrim}</div>
				<input id="oneclick_comment" class="hidden" value="в 1 клик..." type="text"/>
				<div class="form_modal_row">
					<div class="notice notice_alert" style="display: none;">Укажите ваше имя и номер телефона</div>
					<div class="form_group">
						<label for="oneclick_name" class="form_label">Ваше имя: *</label>
						<input id="oneclick_name" class="form_input" value="" type="text"/>
					</div>
					<div class="form_group">
						<label for="oneclick_phone" class="form_label">Номер телефона: *</label>
						<input id="oneclick_phone" class="form_input phone_mask" value="" type="tel"/>
					</div>
				</div>
				<div class="form_modal_row">
					<button id="oneclick_buy" class="button button_block button_large" type="submit" name="enter" onclick="yaCounter21197344.reachGoal('1click_order');" >Купить!</button>	
				</div>
			</div>
			{*/oneclick buy*}
			
		{/if}
		
		<div id="success_notice" class="form_modal text_center">
			<div class="form_modal_row form_modal_title text_2 text_bold text_center text_green">Ваша заявка принята</div>
			<div class="form_modal_row text_gray">
				<div class="form_group">В ближайшее время с вами свяжется наш менеджер!</div>
				<button type="button" class="button" onclick='$.fancybox.close();return false;'>Закрыть</button>
			</div>
		</div>
		
		
		<div id="informer_notice" class="form_modal">
			<div class="informer_notice_title form_modal_row form_modal_title text_2 text_bold text_center"></div>
			<div class="informer_notice_content form_modal_row">
				<div class="flex flex_align_center row">
					<div class="informer_notice_image flex_item col_sm_5 hidden_xs"><img src="/" alt="" title=""></div>
					<div class="flex_item col_xs_12 col_sm_6 offset_sm_1">
						<div class="informer_notice_name text_bold"></div>
						<div class="informer_notice_variant text_blue text_5"></div>
						<div class="informer_notice_price-container">
							<div class="informer_notice_price price old_price text_gray3 text_medium"></div>
							<div class="informer_notice_price price final_price text_1 text_bold">
								<span class="value"></span>&nbsp;
								<span class="currency">{$currency->sign|escape}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form_modal_row text_center">
				<div class="form_group">
					<a class="informer_notice_submit button button_black button_large" href="/"></a>
				</div>
				<button type="button" class="link pseudo_link text_gray" onclick='$.fancybox.close();return false;'>Продолжить покупки</button>
			</div>
		</div>
		
		{include file="callback.tpl"}
	</div>
	<!-- /noindex -->
	
{* JQuery *}
<script src="js/jquery/jquery.js"></script>
<script src="design/{$settings->theme}/js/jquery.mobile.custom.min.js"></script>

{* Слайдер *}
<script src="design/{$settings->theme}/js/slick.min.js"></script>

{* Аяксовая корзина *}
<script src="design/{$settings->theme}/js/ajax.js"></script>

<script src="design/{$settings->theme}/js/script.js"></script>

{*Рейтинг в отзывах*}
{if $smarty.get.module == 'ProductView' || $smarty.get.module == 'CommentsView'}
<script src="design/{$settings->theme}/js/rating.js"></script>
{literal}
<script>
	$(function() {
		$('.add_rating').rater();
	});
</script>
{/literal}
{/if}

{if $module == 'ProductView'}
	<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
	<script src="//yastatic.net/share2/share.js"></script>
{/if}

{include file="script.tpl"}

{* js-проверка форм *}
<script src="js/baloon/js/baloon.js"></script>
<link   href="js/baloon/css/baloon.css" rel="stylesheet" property="stylesheet"/> 	

{if $smarty.session.admin == 'admin'}
<script src ="js/admintooltip/admintooltip.js"></script>
<link   href="js/admintooltip/css/admintooltip.css" rel="stylesheet" property="stylesheet"/> 
{/if}	

<link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" property="stylesheet"/>
<script src="js/fancybox/jquery.fancybox.pack.js"></script>

{*Таймер обратного отсчета*}
<script src="design/{$settings->theme}/js/product_timer.js"></script>

{*Маска для телефона*}
<script src="design/{$settings->theme}/js/jquery.maskedinput.min.js"></script>

{* Автозаполнитель поиска *}
<script src="design/{$settings->theme}/js/jquery.autocomplete.min.js"></script>

{*RECAPTCHA*}
<script src="https://www.google.com/recaptcha/api.js?hl=ru&onload=onloadCallback&render=explicit" async defer></script>
{literal}
<script type="text/javascript">
    // var verifyCallback = function(response) {
    //     $('#callback_form #g-recaptcha_callback').val(response);
    // };
    var verifyComment = function(response) {
        $('#comment_form #g-recaptcha_comment').val(response);
    };
    var verifyRegister = function(response) {
        $('#register_form #g-recaptcha_register').val(response);
    };
    var verifyFeedback = function(response) {
        $('#feedback_form #g-recaptcha_feedback').val(response);
    };
    var verifyCart = function(response) {
        $('#cart_form #g-recaptcha_cart').val(response);
    };
    
    var recaptches = [];
    var onloadCallback = function() {
        /*if($('#recaptcha_callback').size()){ 
            recaptches.push(
                grecaptcha.render('recaptcha_callback', {
                    'sitekey' : '6Le3YhUUAAAAAMoOwD-poWZqMejuZ-YIkt88qvxn',
                    'callback' : verifyCallback,
                    'theme' : 'light' 
                })
            ); 
        }*/
        if($('#recaptcha_comment').size()){ 
            recaptches.push(
                grecaptcha.render('recaptcha_comment', {
                    'sitekey' : '6Le3YhUUAAAAAMoOwD-poWZqMejuZ-YIkt88qvxn',
                    'callback' : verifyComment,
                    'theme' : 'light' 
                })
            ); 
        }
        if($('#recaptcha_register').size()){ 
            recaptches.push(
                grecaptcha.render('recaptcha_register', {
                    'sitekey' : '6Le3YhUUAAAAAMoOwD-poWZqMejuZ-YIkt88qvxn',
                    'callback' : verifyRegister,
                    'theme' : 'light' 
                })
            ); 
        }
        if($('#recaptcha_feedback').size()){ 
            recaptches.push(
                grecaptcha.render('recaptcha_feedback', {
                    'sitekey' : '6Le3YhUUAAAAAMoOwD-poWZqMejuZ-YIkt88qvxn',
                    'callback' : verifyFeedback,
                    'theme' : 'light' 
                })
            ); 
        }
        if($('#recaptcha_cart').size()){ 
            recaptches.push(
                grecaptcha.render('recaptcha_cart', {
                    'sitekey' : '6Le3YhUUAAAAAMoOwD-poWZqMejuZ-YIkt88qvxn',
                    'callback' : verifyCart,
                    'theme' : 'light' 
                })
            ); 
        }
    }    
</script>
{/literal}	
{literal}
<script>
	(function() {
		var s = document.createElement('script');
		s.type ='text/javascript';
		s.id = 'supportScript';
		s.charset = 'utf-8';
		s.async = true;
		s.src = 'https://lcab.sms-uslugi.ru/support/support.js?h=9c98412cf1b010dbfb1fb1eecc44f39d';
		var sc = document.getElementsByTagName('script')[0];
		
		var callback = function(){
			/*
				Здесь вы можете вызывать API. Например, чтобы изменить отступ по высоте:
				supportAPI.setSupportTop(200);
			*/
		};
		
		s.onreadystatechange = s.onload = function(){
			var state = s.readyState;
			if (!callback.done && (!state || /loaded|complete/.test(state))) {
				callback.done = true;
				callback();
			}
		};
		
		if (sc) sc.parentNode.insertBefore(s, sc);
		else document.documentElement.firstChild.appendChild(s);
	})();

</script>
{/literal}

{if !$smarty.session.admin}

	{*копировалка*}
	<script src="design/{$settings->theme}/js/jquery.addtocopy.min.js"></script>

	{literal}
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter21197344 = new Ya.Metrika({
						id:21197344,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-80547125-1', 'auto');
	  ga('send', 'pageview');

	</script>

	{/literal}

	{if $module != 'OrderView'}
		{literal}
			<script>
			(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
			try {
			w.yaCounter21197344= new Ya.Metrika({id:21197344,
			webvisor:true,
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true,params:window.yaParams||{ }});
			} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
			</script>
			<noscript><div><img src="//mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		{/literal}
	{/if}

	<script src='//st.yagla.ru/js/y.c.js'></script>
	<script src="https://cdn.jsdelivr.net/stopsovetnik/latest/ss.min.js"></script>

{/if}

</body>
</html>
{/strip}