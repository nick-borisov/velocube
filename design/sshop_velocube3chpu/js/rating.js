$.fn.rater = function (options) {
    var opts = $.extend({}, $.fn.rater.defaults, options);
    return this.each(function () {
        var $this = $(this);
        var $on = $this.find('.rating_starOn');
        var $off = $this.find('.rating_starOff');
		
        opts.size = $on.height();
        if (opts.rating == undefined) opts.rating = $on.width() / opts.size;
        if (opts.id == undefined) opts.id = $this.attr('id');

        $off.mousemove(function (e) {
            var left = e.clientX - $off.offset().left;
            var width = $off.width() - ($off.width() - left);
            width = Math.ceil(width / (opts.size / opts.step)) * opts.size / opts.step;
            $on.width(width);
        }).hover(function (e) { $on.removeClass('rating_starHover'); }, function (e) {
            $on.removeClass('rating_starHover'); $on.width(opts.rating * opts.size);
        }).click(function (e) {
		    $on.addClass('rating_starHover');
		    var $rt = document.getElementById('add_rating');
            var r = Math.round($on.width() / $off.width() * (opts.units * opts.step)) / opts.step;
           
            $off.css('cursor', 'default'); $on.css('cursor', 'default');
            $rt.value = r;
        }).mouseleave(function (e) {  

			var $rt = document.getElementById('add_rating');
			$on.addClass('rating_starHover');
			$on.width($rt.value*30);     
			
		}).css('cursor', 'pointer'); $on.css('cursor', 'pointer');
    });
};

$.fn.rater.defaults = {
    postHref: location.href,
    units: 5,
    step: 1
};