(function($) {
$(function() {

	function createCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	function eraseCookie(name) {
		createCookie(name,"",-1);
	}





	$('.product_view').each(function(i) {
		var cookie = readCookie('tabCookie'+i);
		if (cookie) $(this).find('a').eq(cookie).addClass('current').siblings().removeClass('current')
			.parents('#content').find('#product_list').addClass('list_products').removeClass('tiny_products').eq(cookie).addClass('tiny_products').removeClass('list_products');
	})

	$('.product_view').delegate('a:not(.current)', 'click', function(i) {
		$(this).addClass('current').siblings().removeClass('current')
			.parents('#content').find('#product_list').removeClass('tiny_products').addClass('list_products').eq($(this).index()).addClass('tiny_products').removeClass('list_products')
			
				var cookie = readCookie('tabCookie'+i);
		if (cookie) $(this).find('#product_list').eq(cookie).removeClass('tiny_products').addClass('list_products').siblings().removeClass('list_products')
		
		
		
		var ulIndex = $('.product_view').index($(this).parents('.product_view'));
		eraseCookie('tabCookie'+ulIndex);
		createCookie('tabCookie'+ulIndex, $(this).index(), 365);
	})
	
	
	

	

})
})(jQuery)