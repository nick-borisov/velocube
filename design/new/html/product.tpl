
{$canonical="/products/{$product->url}" scope=parent}



{foreach from=$category->path item=cat}{assign var='last_path' value=$cat->url}{assign var='last_name' value=$cat->name}{assign var='last_id' value=$cat->id}{/foreach}

<div id="path">

<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
<a href="/">Главная</a>

{foreach from=$category->path item=cat} > <a itemprop="url" href="catalog/{$cat->url}"><span itemprop="title">{$cat->name|escape}</span></a>
{/foreach}

{if $brand} > <a itemprop="url" href="catalog/{$cat->url}/{$brand->url}"><span  itemprop="title">{$brand->name|escape}</span></a>{/if} 
>  {$product->name|escape} 
</div>
</span>

<div class="product" itemscope itemtype="http://schema.org/Product" >
	<div class="images">
        <meta itemprop="brand" content="{$brand->name|escape}" />
		<ul class="im">

		{if $product->images|count>0 AND $product->image->filename != ''}
			<li><a class="zoom" href="{$product->image->filename|resize:800:600}" rel='gal1' title="{$product->name|escape}"><img itemprop="image" src="{$product->image->filename|resize:300:200}" title="{$product->name|escape}" />{if $product->warranty}<div id="gp">&nbsp;</div>{/if}</a></li>
		{else}
		<li><a class="zoom" href="{"nophoto.png"|resize:800:600}" rel='gal1' title="{$product->name|escape}"><img itemprop="image" src="{"nophoto.png"|resize:300:200}" title="{$product->name|escape}" /></a></li>
		{/if}
		{if $product->images|count>1}
			{* cut удаляет первую фотографию, если нужно начать 2-й - пишем cut:2 и тд *}

			{foreach $product->images|cut as $i=>$image}<li><a class="zoom" href="{$image->filename|resize:800:600}" rel='gal1' data-fancybox-group="gallery"><img  src="{$image->filename|resize:300:200}" alt="{$product->name|escape}" />{if $product->warranty}<div id="gp">&nbsp;</div>{/if}</a></li>{/foreach}
		{/if}
		</ul>
		{if $product->images|count>0 AND $product->image->filename != ''}
		<div class="ims">
        {counter start=-1 skip=1 print=false}
        {foreach $product->images as $i=>$image}
			<a data-slide-index="{counter}" data-slide-index-click="{$i}" href=""><img src="{$image->filename|resize:50:50}" /></a>
		{/foreach}
		</div>
		{/if}
	</div>
	 
	<div class="description">
	<!-- Ретинг -->
		<div id="product_{$product->id}" class="raters" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		{if $product->rating != 0}
			<span class="rater-starsOff1" style="width:87px;">
				<span class="rater-starsOn1" style="width:{$product->rating*87/5|string_format:"%.0f"}px"></span>
			</span>
			<span class="test-text">
				<a rel="nofollow" data-product="{$product->id}" href="products/{$product->url}#comments"><span class="rater-rating" >{$product->votes|string_format:"%.0f"} {$product->votes|plural:'отзыв':'отзывов':'отзыва'}</span></a>
			</span>
                        
                        <meta itemprop="ratingValue" content="{$product->rating}" />
<meta itemprop="reviewCount" content="{$product->votes}" />

		{else}
					<span class="rater-starsOff1" style="width:87px;">
				<span class="rater-starsOn1" style="width:{$product->comrate/$product->comcon*87/5|string_format:"%.0f"}px"></span>
			</span>
			<span class="test-text">
				<a rel="nofollow" data-product="{$product->id}" href="products/{$product->url}#comments"><span class="rater-rating" >{$product->comcon|string_format:"%.0f"} {$product->comcon|plural:'отзыв':'отзывов':'отзыва'}</span></a>
			</span>
		
		{/if}
		</div>
	<!-- Рейтинг (The End) -->
	
    {if $compare->ids && in_array($product->id, $compare->ids)}
            <span class='mylist_add'>уже в списке</span>
            {else}
            <span class='mylist_add'><a href="/compare?id={$product->id}" class='addps' data-id='{$product->id}' data-key='compare' data-informer='1' data-result-text='уже в списке'>добавить к сравнению</a></span>
            {/if}
    
	<h1 data-product="{$product->id}" itemprop="name">{$product->name|escape}</h1>
	
		{$product->annotation}
		
		
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="acceptedPaymentMethod" content="CreditCard" />
		<meta itemprop="priceCurrency" content="RUB" />
		<meta itemprop="availability" content="http://schema.org/InStock" />

        <ul class="variants">
        {foreach $product->variants as $v}
			<li class="variant {if $product->variant->id==$v->id}checked{/if}">
				<div class="variant_img">
					<input id="product_{$v->id}" id-image="{$v->image->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton {if $v->color_code}hid{/if}" {if $product->variant->id==$v->id}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
                    {*if $v->image || ($v->color_code!=='' && $v->color_code!=='false')}<label{if $v->image}{else} style="background:{$v->color_code};"{/if} class="variant_name_color_box" title="{$v->name}" for="product_{$v->id}">{if $v->image}<img src="{$v->image->filename|resize:30:30}" alt="{$v->name}" />{/if}</label>{/if*}
                    {if $v->image || ($v->color_code!=='' && $v->color_code!=='false')}<label{if $v->color_code!=='' && $v->color_code!=='false'} style="background:{$v->color_code};"{/if} class="variant_name_color_box" title="{$v->name}" for="product_{$v->id}">{if $v->image && ($v->color_code=='' || $v->color_code=='false')}<img src="{$v->image->filename|resize:60:60}" alt="{$v->name}" />{/if}</label>{/if}
                    
                </div>
				<div class="variant_name_ss" style="{if ($v->stock && $product->sklad_id == 0 && $v->store)}width: 275px{/if}">
                    {if $v->name}<label class="variant_name" for="product_{$v->id}">{$v->name}</label>{/if}
                    
                    <div class="variant_price_ss">
                        <br />
                        <span>Цена:</span> 
    					{if $v->compare_price > 0 AND $v->stock > 0}<span class="compare_price" itemprop="price">{$v->compare_price|convert}</span>{/if}
    					<span class="price">{if $v->price > 0 AND ($product->sklad_id > 1 OR $v->stock > 0) }<span class="price_n" itemprop="price">{$v->price|convert}</span> <span class="currency">{$currency->sign|escape}</span>{/if}
    					{if $v->stock > 0}{$ve = 1}{/if}
    					{if $v->stock == 0 AND $product->sklad_id == 0 }<span style="color: red;"> Нет в наличии</span>
    					{*{elseif $v->stock <> 0}<span style="color: green;"> В наличии</span> *}
    					{elseif $product->sklad_id == 1}<span style="color: green;"> В наличии</span> 
    					{elseif $product->sklad_id == 2}<span style="color: blue;"> Под заказ</span> 
    					{elseif $product->sklad_id == 4}<span style="color: orange;"> Предзаказ</span> 
    					{elseif $product->sklad_id == 3}<span style="color: orange;"> Ожидается {if $product->byrequest} ( {$product->byrequest} ) {/if}</span>
    					{/if}
    					
    					</span>
				    </div>
                    
                    {if !($v->stock == 0 AND $product->sklad_id == 0)}
                    <div>
                    
                        <span>{if $v->store==1}Склад "Октябрьская"{elseif $v->store==2}Магазин "Сокольники"{else}Основной склад{/if}</span>
                        <small class="traffic_light_{if $v->stock > $settings->interval_max}green{elseif $v->stock > $settings->interval_min}yell{else}red{/if}"></small>
                        <span class="traffic_word">({if $v->stock > $settings->interval_max}много{elseif $v->stock > $settings->interval_min}достаточно{else}мало{/if})</span>
                    </div>
                    {/if}
    			 </div>
                 
                 
                {if !($v->stock == 0 AND $product->sklad_id == 0)}
                <div class="prod_dostavka">
                    {*<div>Доставка:</div>*}
                    
                    <span>
                    {if $v->store==1} <span id="tomorrow_osn"></span>
                        {*if $v->price > $settings->interval_price} - бесплатно{else} - {$category->delivery_price} руб.{/if*}
                    {else}
                        {if $v->price > $settings->interval_tom_price} <span id="tomorrow_osn"></span>{* - бесплатно*}
                        {elseif $v->price > $settings->interval_price} <span id="tomorrow_osn"></span> {*- {intval($category->delivery_price) * intval($settings->interval_delivery_mult)} руб.<br />
                        <span id="aftertomorrow_osn"></span>{* - бесплатно *}
                        {else} <span id="tomorrow_osn"></span>{* - {intval($category->delivery_price) * intval($settings->interval_delivery_mult)} руб.<br />
                        <span id="aftertomorrow_osn"></span>{* - {$category->delivery_price} руб. *}
                        {/if}
                    {/if}
                    </span>
                </div>
                
                <div class="samovyvoz{if ($v->stock == 0 || $product->sklad_id != 0 || !$v->store || $v->store==3)}_no{else}{if intval(date("H", time())) >= 19}_next_day{/if}{/if}"></div>
                {/if}
            </li> 
            
        {/foreach}
        </ul>
                
{literal}            
<script type="text/javascript">
var holidays = [{/literal}{$settings->calendar_days}{literal}];
console.log(holidays);
jQuery('li.variant').click(function(){
    jQuery('li.variant.checked').find('input[type="radio"]').removeAttr('checked');
    jQuery('li.variant.checked').removeClass('checked');
    jQuery(this).addClass('checked');
    var cVariant = jQuery(this).find('input[type="radio"]').attr('checked', 'checked');
    changeVariant(cVariant);
});
function clock() {
    var d = new Date();

    month=new Array("января", "февраля", "марта", "апреля", "мая", "июня",
    "июля", "августа", "сентября", "октября", "ноября", "декабря");
    days=new Array("воскресенье", "понедельник", "вторник", "среда",
    "четверг", "пятница", "суббота");
    
    var new_date = d.getTime()+(24+6)*60*60*1000;//заказ после 18:00 переходит на следующий день, т.е  + 6 часов
    var d_of_w_t = new Date(new_date);
    d_of_w_t = d_of_w_t.getDay();
    
    if(d_of_w_t>6){
        d_of_w_t = d_of_w_t-6; 
        new_date = d.getTime()+(48+6)*60*60*1000;
    }else if(d_of_w_t==0){
        d_of_w_t = 1;
        new_date = d.getTime()+(72+6)*60*60*1000;
    }
    
    var dd = new Date(new_date);
        
    var t_day = dd.getDate();
    var t_month = dd.getMonth()+1;
    
    if (t_day <= 9) t_day = "0" + t_day;
    if (t_month <= 9) t_month = "0" + t_month;
    
    console.log('d_of_w_t '+d_of_w_t);
    
    /////////////////////////////////////
    
    var new_date_t_osn = d.getTime()+(24+6)*60*60*1000; 
       
    var d_of_w_t_osn = get_day_osn(new_date_t_osn);//new Date(new_date_t_osn);
    
    var i=1;
    var day_of_week = dayOfWeek(new_date_t_osn);
    console.log('xx'+new_date_t_osn);
    while($.inArray(get_format_date_osn(new_date_t_osn), holidays)!=-1){
        new_date_t_osn = d.getTime()+((24*i)+6)*60*60*1000; 
        get_day_osn(new_date_t_osn);
        console.log('new_date_t_osn--'+new_date_t_osn);
        i++;
    }
    
    //console.log(d_d_date);
    
    ////////////////////////////////////////////
    
     var new_date_at_osn = d.getTime()+(48+6)*60*60*1000; 
       
    var d_of_w_at_osn = get_afday_osn(new_date_at_osn);//new Date(new_date_t_osn);
    
    var i=1;
    var day_of_week = dayOfWeek(new_date_at_osn);
    console.log('yy'+new_date_at_osn);
    while($.inArray(get_format_date_osn(new_date_at_osn), holidays)!=-1){
        new_date_at_osn = d.getTime()+((24*i)+6)*60*60*1000; 
        get_afday_osn(new_date_at_osn);
        console.log('new_date_at_osn--'+new_date_at_osn);
        i++;
    }
    //////////////////////////////////////////////
    /////////////////////////////////////////////
    
    var new_date_at_osn = d.getTime()+(48+6)*60*60*1000;    
    var d_of_w_at_osn = new Date(new_date_at_osn);
        d_of_w_at_osn = d_of_w_at_osn.getDay();
        
    if(d_of_w_at_osn>5){
        var div_at = 9 - d_of_w_at_osn;
        d_of_w_at_osn = 2;
        new_date_at_osn = d.getTime()+((24*div_at)+6)*60*60*1000;    
    }else if(!d_of_w_at_osn){
        d_of_w_at_osn = 2;
        new_date_at_osn = d.getTime()+((24*4)+6)*60*60*1000;
    }

    var dd_at = new Date(new_date_at_osn);
        
    var at_day = dd_at.getDate();
    var at_month = dd_at.getMonth()+1;

    if (at_day <= 9) at_day = "0" + at_day;
    if (at_month <= 9) at_month = "0" + at_month;
    
    date_tom = days[d_of_w_t] + ', ' + t_day + '.' + t_month;
    
    date_aftom = days[d_of_w_at_osn] + ', ' + at_day + '.' + at_month;
    
    console.log('d_of_w_t_osn '+d_of_w_t_osn+' '+'date_tom_osn '+date_tom_osn);
    {/literal}
    var paththeme = '/design/{$settings->theme}/';
    //console.log(paththeme);
    {literal}
    jQuery("span#tomorrow").html('<img src="'+paththeme+'images/delivery_'+d_of_w_t+'.png" width="78px" title="'+date_tom+'"/>');
    jQuery("span#tomorrow_osn").html('<img src="'+paththeme+'images/delivery_'+get_day_osn(new_date_t_osn)+'.png" width="78px"  title="'+date_tom_osn+'"/>');
    jQuery("span#aftertomorrow_osn").html('<img src="'+paththeme+'images/delivery_'+get_afday_osn(new_date_at_osn)+'.png" width="78px"  title="'+date_aftom+'"/>');

}

function get_day_osn(time){
    var d_of_w_t_osn = new Date(time);
        d_of_w_t_osn = d_of_w_t_osn.getDay();
        
    if(d_of_w_t_osn>5){
        var div_t = 7 - d_of_w_t_osn;
        d_of_w_t_osn = 1;
        time = get_time_osn(time, div_t);//d.getTime()+((24*div_t)+6)*60*60*1000;    
    }else if(!d_of_w_t_osn){
        d_of_w_t_osn = 1;
        time = get_time_osn(time, 4);//d.getTime()+((24*4)+6)*60*60*1000;
    }
    //var DATE = get_format_date_osn(time);
    return d_of_w_t_osn;
}
function get_afday_osn(time){
    var d_of_w_t_osn = new Date(time);
        d_of_w_t_osn = d_of_w_t_osn.getDay();
        
    if(d_of_w_t_osn>5){
        var div_t = 7 - d_of_w_t_osn;
        d_of_w_t_osn = 1;
        time = get_time_osn(time, div_t);//d.getTime()+((24*div_t)+6)*60*60*1000;    
    }else if(!d_of_w_t_osn){
        d_of_w_t_osn = 1;
        time = get_time_osn(time, 4);//d.getTime()+((24*4)+6)*60*60*1000;
    }
    //var DATE = get_format_date_osn(time);
    return d_of_w_t_osn;
}

function get_time_osn(time, div){
    return new_date_t_osn = time+((24*div)+6)*60*60*1000;  
}
function get_format_date_osn(time){
    var d_d = new Date(time);
        
    var t_day_osn = d_d.getDate();
    var t_month_osn = d_d.getMonth()+1;
    
    if (t_day_osn <= 9) t_day_osn = "0" + t_day_osn;
    if (t_month_osn <= 9) t_month_osn = "0" + t_month_osn;
    return date_tom_osn = t_day_osn+'.'+t_month_osn+'.'+d_d.getFullYear();
}
function dayOfWeek(time){
    var d1 = new Date(time);
    return d1.getDate();
}
clock();
</script>       
{/literal}            
			{if $ve != 0 OR $product->sklad_id != 0 }<input type="submit" onclick="yaCounter21197344.reachGoal('addcart');" class="button" value="В корзину" data-result-text="Добавлено"/>
			<a href="#oneclick" class="various oneclick" onclick="yaCounter21197344.reachGoal('1click');" style="line-height: 23px; margin-left: 12px;">Заказать в 1 клик</a>{/if}
            {*<div style="clear:both;padding-bottom: 5px;"></div>
            <div>{if $compare->ids && in_array($product->id, $compare->ids)}
            <span class='mylist_add'>уже в списке</span>
            {else}
            <span class='mylist_add'><a href="/compare?id={$product->id}" class='addps' data-id='{$product->id}' data-key='compare' data-informer='1' data-result-text='уже в списке'>добавить к сравнению</a></span>
            {/if}</div>*}
		
		</form>
		<!-- Выбор варианта товара (The End) -->               
		
		
	<div id="oneclick" class="window" style="display: none;">
        <div class="title">Купить {$product->name|escape|rtrim}</div>
        <ul>
            <li><span>Ваше имя:</span><input class="onename" value="" type="text"></li>
            <li><span>Телефон:</span><input class="onephone" value="" type="text"></li>
			<li style="display:none;"><span>Комментарий:</span><input class="onecomment" value="в 1 клик..." type="text"></li>
        </ul>
        <button type="submit" name="enter" onclick="yaCounter21197344.reachGoal('1click_order');" value="1" class="oneclickbuy button">Купить!</button>
	</div>
	{literal}
        <style>
                .button.various         { display: block; float: left; margin: 8px 0 0 2px; padding: 3px 10px; font-size: 13px; }
                #oneclick                       { width: 350px; font-size: 14px; }
                #oneclick .title        { font: 18px Arial; color: #000; margin-bottom: 7px; border-bottom: 1px solid #ccc; padding-bottom: 12px; }
                #oneclick p,
                #oneclick li            { display: block; padding: 6px 0; }
                #oneclick li span       { display: inline-block; width: 80px;  }
                #oneclick input         { width: 264px; }
                #oneclick button        { float: right; margin-top: 8px; }
            .hid {display: none;}

            
        </style>
        <script>

            function changeVariant(variant){
                var product_id = $(variant).attr('id-image');

                if($('a[data-slide-index-click='+$(variant).attr('id-image')+']').length>0)
                {
                    slider.goToSlide($('a[data-slide-index-click='+$(variant).attr('id-image')+']:eq(0)').index());
                }

            }



        $(function() {

            $('a[data-slide-index-click='+$('input[name=variant]:checked').attr('id-image')+']').click();

            $('a[data-slide-index-click]').click(function()
            {
                $('a[data-slide-index-click]').removeClass('active');
                $(this).addClass('active');
                if($('input[id-image='+$(this).attr('data-slide-index-click') +']').length >0) {
                    $('input[id-image='+$(this).attr('data-slide-index-click') +']:eq(0)').attr('checked','checked');
                }
            });

            $('input[name=variant]').change(function(){ changeVariant(this); });

                $(".various").fancybox({
                        helpers : {
                                overlay : {
                                        locked : false
                                }
                        },
						showNavArrows: 	true,
                        closeBtn: 		true,
                        fitToView: 		false,
                        autoSize: 		true,
                        minHeight: 		"0",
                        minWidth: 		"350",
                        scrolling: 		false,
                        closeClick      : false,
                        openEffect      : 'fade',
                        closeEffect     : 'none'
                });



                $('.oneclickbuy').click( function() {

                        if($('.variants').find('input[name=variant]:checked').size()>0) 
						variant = $('.variants input[name=variant]:checked').val();
                        
                        if( !$('.onename').val() || !$('.onephone').val() ) { 
                                alert("Заполните все поля!");
                                return false;
                        }

                        $.ajax({
                                type: "post",
                                url: "/ajax/oneclick.php",
                                data: {amount: 1, variant: variant, name: $('.onename').val() , phone: $('.onephone').val() , comment: $('.onecomment').val() },
                                dataType: 'json'
                        });
                        
                        $('.oneclick').hide(200);
                        $("#oneclick").html("<div class='title'>Спасибо за заказ!</div><p>В ближайшее время с вами свяжется наш менеджер!</p><button type='submit' class='button' onclick='$.fancybox.close();$(\".oneclick\").hide();return false;'>Закрыть!</button>");
                        
                        return false;
                        
                });

        });
        </script>
{/literal}
	</div>
	<!-- Описание товара (The End)-->



	{if $product->body}
        <br />
	<div class="h_2">Описание</div>
	<div itemprop="description">{$product->body}</div>
	{/if}
	
	{if $product->video}
	<div class="h_2">Видео обзор</div>
	{$product->video}
	{/if}
	
	{if $product->features}
	<!-- Характеристики товара -->
	<div class="h_2">Характеристики</div>
	<ul class="features">
	{foreach $product->features as $f}
	<li>
		<label>{$f->name}</label>
		<span>{$f->value}</span>
	</li>
	{/foreach}
 
	</ul>
		Фирма-производитель оставляет за собой право на внесение изменений в конструкцию, дизайн и комплектацию приборов без предварительного уведомления. Во избежание недоразумений при покупке приборов уточняйте информацию о комплектации, наличию и цене у менеджера магазина.<br>  <br>  
	<!-- Характеристики товара (The End)-->
	{/if}
</div>





 <style>
 
   r {
    text-align: right; /* Выравнивание по правому краю */
   }
  </style>


<!-- Соседние товары /-->
    <div id="back_forward">
        {if $prev_product}
            ←&nbsp;<a class="prev_page_link" href="products/{$prev_product->url}">{$prev_product->name|escape}</a>
        {/if}
        {if $next_product}

          <a class="next_page_link" href="products/{$next_product->url}">{$next_product->name|escape}</a>&nbsp;→

       {/if}
   
    </div>				

				<div style="clear: both;margin-bottom: 10px;background:#fff;" class="text">
                        <div style="width:1000px;margin: 0 auto;">
                        <div style="background:#fff;padding:20px">
                    
<h2 style="text-align: center;"><span style="font-size: large; " data-product="{$product->id}">{$product->name|escape} от {$brand->name|escape} - как купить?</span></h2>
<!-- Description Footer For {$product->name|escape} -->
{if $category AND $last_name}
<div id="category_description_foot">


<p>Добро пожаловать в Velocube - лучший магазин спортивных товаров и комплектующих в столице!</p>
<p>Покупать у нас комфортно - мы позаботились о том, чтобы удовлетворить каждого нашего клиента и ввели 2 способа покупки. Первый - для тех, кто хочет быть уверенным в каждом шаге и хочет получить дополнительные гарантии или оставить свои комментарии для неординарного заказа. Он подразумевает традиционную регистрацию и покупку через личный кабинет.</p>
<p>Второй способ - для тех, кто не любит ждать и хочет получить свой заказ как можно быстрее. Речь идет об уникальной системе One Click, которая позволяет оформить заказ посредством всего одного клика! Для того, чтобы <strong	>купить {$product->name|lower|capitalize} от {$brand->name|escape}</strong> у нас, достаточно просто положить выбранную позицию в корзину, использовав специальную кнопку под названием товара на странице. Далее нужно заполнить простую форму, оставив нам свои данные, необходимые для дальнейшей работы, после чего наши сотрудники свяжутся с Вами для уточнения деталей оплаты Вашей покупки.</p>
<p>Для тех, кто ценит скорость и комфорт интернет шопинга превыше всего, у нас работает оплата при помощи пластиковых карт  - самый быстрый и проверенный способ для современного пользователя.</p>
<p>Продажа спортивного оборудования от Velocube осуществляются по всей территории Россиийской Федерации.</p>
<p>Для жителей Москвы доступны два способа получения своего заказа -  доставка лично в руки, посредством курьерской службы, и самовывоз, а жителям регионов мы предлагаем получить {$product->name|lower|capitalize} от {$brand->name|escape}, воспользовавшись услугами междугородних компаний-перевозчиков и предварительно оплатив свою покупку в полном объеме.</p><p>Работать с нами не только удобно, но и выгодно - стоимость в интернет-магазине Velocube заметно дешевле, чем в розничных магазинах и на специализированных рынках. Все позиции, представленные в нашем каталоге, имеют сертификаты подлинности от производителей, а потому, покупая у нас, Вы экономите на цене, но не на качестве!</p>

<p>Если Вы еще не определились с выбором , то персонал нашего интернет-магазина поможет Вам подобрать {if $brand} <a href="catalog/{$last_path}/{$brand->url}/" title="{$last_name} {$brand->name|escape}" target=_blank>{$last_name|lower|capitalize} {$brand->name|escape}</a> или {/if} <a href="catalog/{$last_path}/" title="{$last_name}" target=_blank>{$last_name|lower|capitalize}</a> {if $brand}другой торговой марки{/if} исходя из Ваших предпочтений и пожеланий.</p>

<p>Ждем Ваших звонков!</p>


</div>
{/if}
<!-- END Description Footer For {$product->name|escape} -->




</div></div></div>

{if $related_products}
<div class="h_2">Так же советуем посмотреть</div>
<ul class="tiny_products">
	{foreach $related_products as $product}
		{include file='product_block.tpl'}
	{/foreach}
</ul>
{/if}

{if $similar_products}
<div class="h_2">Похожие товары</div>


<ul class="tiny_products">
	{foreach $similar_products as $product}
		{include file='product_block.tpl'}
	{/foreach}
</ul>
{/if}




<!-- Комментарии -->
<div id="comments"  style="clear:both">
	<div class="h_2">Комментарии</div>
		<div class="box">
			<!--Форма отправления комментария-->	
	<form class="comment_form" method="post" style="clear:both">
		<div class="h_2">Написать комментарий</div>
		{if $error}
		<div class="message_error">
			{if $error=='captcha'}
			Неверно введена капча
			{elseif $error=='empty_name'}
			Введите имя
			{elseif $error=='empty_comment'}
			Введите комментарий
			{/if}
		</div>
		{/if}
		<input type="hidden" id="parent" name="parent_id" value="0"/>
		<input type="hidden" id="admin" name="admin" value="{if $smarty.session.admin == 'admin'}1{else}0{/if}"/>
		
		<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий">{$comment_text}</textarea><br />
		<div>
		<label for="comment_name">Имя</label>
		<input class="input_name" type="text" id="comment_name" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя"/><br />

		<input type="hidden" name="id" value="{$product->id}" />
		<script src="{$config->root_url}/js/project.js"></script>
		
		{literal}
		<script type="text/javascript">
			$(function() {
				$('.testRater2').rater();
			});
		</script>
		{/literal}
		
		<div class="testRater2" id="product_{$product->id}">
			<span class="rater2">
				<label for="comment_rat" style="margin-top:0px">Оценка</label> 
				<input type="hidden" id="rt" name="rating" value="" />
				<span class="rater-starsOff" style="width:157px;">
					<span class="rater-starsOn" style="width:0"%.0f"}px"></span>
				</span>
			</span>
		</div>
		
		<label for="comment_captcha">Число</label>
		<div class="captcha"><img src="captcha/image.php?{math equation='rand(10,10000)'}" alt='captcha'/></div> 
		<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>
		
		<input class="button" type="submit" name="comment" value="Отправить" />
	
		</div>
	</form>
	
{$test2 = 'product_page' scope='parent'}	




	
		{if $comments}
	<!-- Список с комментариями -->
	<ul class="comment_list">
		{foreach $comments as $comment}
		<a name="comment_{$comment->id}"></a>
		<li>
			<!-- Имя и дата комментария-->
			<div class="comment_header">	
				{$comment->name|escape} <i>{$comment->date|date}, {$comment->date|time}</i>
				{if !$comment->approved}ожидает модерации</b>{/if}
			</div>
			<!-- Имя и дата комментария (The End)-->
			
			<!-- Комментарий -->
			 <div class="grade grade-{$comment->rate}"></div>
			
			{if $comment->text}
				<div class="head-plus"><b>Комментарии:</b></div>
				<div class="yc-text">{$comment->text|escape|nl2br}</div>
				<br>
				{/if}
				{if $comment->dost}
				<div class="head-plus"><b>Достоинства:</b></div>
				<div class="yc-text">{$comment->dost|escape|nl2br}</div>
				<br>
				{/if}
	 
			
				{if $comment->nedost}
				<div class="comment-wrap">
					<div class="head-minus"><b>Недостатки:</b></div>
					<div class="yc-text">{$comment->nedost|escape|nl2br}<br></div>
				</div>
				{/if}
			<!-- Комментарий (The End)-->
		</li>
		{/foreach}
	</ul>
	<!-- Список с комментариями (The End)-->
	{else}
	<p>
		Пока нет комментариев
	</p>
	{/if}
	<!--Форма отправления комментария (The End)-->
		</div>
		<div class="box"><!-- Комментарии Вконтакте -->
		{literal}
			<script type="text/javascript" src="//vk.com/js/api/openapi.js?99"></script>
			<script type="text/javascript">
				VK.init({apiId: 3852449, onlyWidgets: true});
			</script>
			
			<div id="vk_comments"></div>
			<script type="text/javascript">
				VK.Widgets.Comments("vk_comments", {limit: 20, width: "450", attach: "*"});
			</script>
		{/literal}
		<!-- Комментарии Вконтакте (The End) --></div>
</div>



{literal}
<script type="text/javascript">
	$(document).ready(function() {
		$('.zoom').fancybox();
	});
	
	var slider = $('.im').bxSlider({
		mode: 'fade',
		controls: false,
		pagerCustom: '.ims'
	});
	

(function($) {
$(function() {
	$('ul.tabs').on('click', 'li:not(.current)', function() {
		$(this).addClass('current').siblings().removeClass('current')
		.parents('div.section').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
	})
		
	var tabIndex = window.location.hash.replace('#tab','')-1;
	if (tabIndex != -1) $('ul.tabs li').eq(tabIndex).click();
		
	$('a[href*=#tab]').click(function() {
		var tabIndex = $(this).attr('href').replace(/(.*)#tab/, '')-1;
		$('ul.tabs li').eq(tabIndex).click();
	});
})
})(jQuery)
</script>
{/literal}

