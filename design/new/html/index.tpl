<!DOCTYPE html>
<html>
<head>
	<base href="{$config->root_url}/"/>
    <meta name="w1-verification" content="165372153651" />    
 
   
           
    <title>{$meta_title|escape}</title>
    <meta name="description" content="{$meta_description|escape}" />
	 

  
	
	
	{* Метатеги *}
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	
	<meta name="viewport" content="width=1024"/>
	
	{* Канонический адрес страницы *}
	{if isset($canonical)}<link rel="canonical" href="{$canonical}"/>{/if}
	
	{* Стили *}
    
    <link href="design/{$settings->theme|escape}/css/custom_slava.css?2" rel="stylesheet" type="text/css" media="screen"/>
	<link href="design/{$settings->theme|escape}/css/style.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="design/{$settings->theme|escape}/css/style22.css" rel="stylesheet" type="text/css" media="screen"/>
	
	{if $module == 'CommentsView'}
	<link href="design/{$settings->theme|escape}/css/yacomments.css" rel="stylesheet" type="text/css" media="screen"/>
	{/if}
	
	
	
    <link href="{$config->root_url}/favicon.ico" rel="icon" type="image/x-icon"/>
	<link href="{$config->root_url}/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	
	{* JQuery *}
	<script src="js/jquery/jquery.js"  type="text/javascript"></script>
	<script src="design/{$settings->theme}/js/jquery.maskedinput.js"></script>

	{* Всплывающие подсказки для администратора *}
	{if $smarty.session.admin == 'admin'}
	<script src ="js/admintooltip/admintooltip.js" type="text/javascript"></script>
	<link   href="js/admintooltip/css/admintooltip.css" rel="stylesheet" type="text/css" /> 
	{/if}
	
	{* Ctrl-навигация на соседние товары *}
	<script type="text/javascript" src="js/ctrlnavigate.js"></script>           
	
	{* Аяксовая корзина *}
	<script src="design/{$settings->theme}/js/jquery-ui.min.js"></script>
	<script src="design/{$settings->theme}/js/ajax_cart.js"></script>
	{*  Быстрый просмотр *}
	<script src="design/{$settings->theme}/js/jquery.arcticmodal-0.3.min.js"></script>
	<script src="design/{$settings->theme}/js/jquery.jqzoom-core.js"></script>
	
	{*копировалка*}
	<script src="design/{$settings->theme}/js/ac.js"></script>
	
	<link rel="stylesheet" href="design/{$settings->theme}/css/jquery.arcticmodal-0.3.css">
	<link rel="stylesheet" href="design/{$settings->theme}/css/simple.css">

	{if $module != 'MainView' && $module != 'CartView' && $module != 'OrderView'}<script src="{$config->root_url}/js/scroll.js"></script>{/if}
	
	{if $module == 'MainView'}<script src="{$config->root_url}/js/timer.js"></script>{/if}
	{* Сравнение   *}
	<script src="design/{$settings->theme}/js/product_to_session.js"></script>
	<script src="design/{$settings->theme}/js/remove_product.js"></script>	
	{* js-проверка форм *}
	<script src="js/baloon/js/baloon.js" type="text/javascript"></script>
	<link   href="js/baloon/css/baloon.css" rel="stylesheet" type="text/css" /> 
	
		{* Автозаполнитель поиска *}
	{literal}
	<script src="js/autocomplete/jquery.autocomplete-min.js" type="text/javascript"></script>
	<style>
	.autocomplete-suggestion { solid; background: #FFF !important; padding: 5px;}
	.autocomplete-w1 { position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; width: 500px; }
	.autocomplete { border:1px solid #999; background:#FFF; cursor:default; text-align:left; overflow-x:hidden;  overflow-y: auto; margin:-6px 6px 6px -6px; /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
	.autocomplete .selected { background:#F0F0F0; }
	.autocomplete div { padding:2px 5px; white-space:nowrap; }
	.autocomplete strong { font-weight:normal; color:#3399FF; }
	</style>	
	<script>
	$(function() {
	
		
		//  Автозаполнитель поиска
		$(".input_search").autocomplete({
			serviceUrl:'ajax/search_products.php',
			minChars:3,
			noCache: true, 
			onSelect:
				function(value, data){
				    console.log(data+' '+value);
                    $('input[name="keyword"].input_search').val(data.fullname);
                    $(".input_search").closest('form').submit();
                    
				},
			fnFormatResult:
				function(value, data, currentValue){
					var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');
					var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
	  				return (data.image?"<img align=absmiddle src='"+data.image+"'> ":'') + value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
				}	
		});
	});
	
	function wait_loader(){
        $('a.checkout').fancybox();
        $('a.checkout').click();
    }
	
	</script>
	{/literal}
		
			
			
			
			
<script type="text/javascript" src="design/{$settings->theme}/js/jquery.main.js"></script>
<script type="text/javascript" src="design/{$settings->theme}/js/jquery.ui-slider.js"></script>
<script type="text/javascript" src="design/{$settings->theme}/js/filter.min.js"></script>


<script type="text/javascript" src="design/{$settings->theme}/js/jquery.bxslider.min.js"></script>

<link href="design/{$settings->theme|escape}/css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript" src="design/{$settings->theme}/js/jquery.fancybox.pack.js"></script>

<script type="text/javascript" charset="utf-8" src="/callme/js/callme.js"></script>

</head>
<body>

{if $sticker}
<div id="sticker" style="background: none repeat scroll 0 0 #fff500;border: 2px solid #ffe000;border-radius: 6px;box-shadow: 0 0 100px rgba(0, 0, 0, 0.5);color: #000;font-size: 13px;left: 50%;margin: 0 0 0 -245px;padding: 15px 25px 15px 20px;position: fixed;top: -5px;width: 440px;z-index: 9999;">
	<div class="beono-flashmessage-text">
		{$sticker->text}
	</div>
	<a onclick="$('#sticker').remove()" style="color: #666;display: block;font-family: Verdana,Arial,Helvetica,sans-serif;font-size: 16px;font-weight: bold;line-height: 16px;position: absolute;right: 8px;text-decoration: none;top: 16px;cursor: pointer">×</a>
</div>
{/if}
 
<div id="header">
	<div id="wrapper">
        
		<div id="top">
                                   {if $test == 'products_page' || $test2 == 'product_page'}
        
         {/if}
			<div id="reg_menu">
				<ul id="menu">
				{foreach $pages as $p}
					{* Выводим только страницы из первого меню *}
					{if $p->menu_id == 1}
						<li {if $page && $page->id == $p->id}class="selected"{/if}>
							<a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a>
						</li>
					{/if}
				{/foreach}
				</ul>
             
				<div id="account">
				{if $user}
					<span id="username">
						<a href="user">{$user->name}</a>{if $group->discount>0}, ваша скидка &mdash; {$group->discount}%{/if}
					</span>
					<a id="logout" href="user/logout">выйти</a>
				{else}
					<a id="register" href="user/register">Регистрация</a> /
					<a id="login" href="user/login">Вход</a>
				{/if}
				</div>
			</div>
			
			<div id="logo"><a href="/" class="icon logo" title="{$settings->site_name|escape}"></a></div>	
			
			
			<div id="contact">          
				<span id="phone">8 (499) 653-88-38</span>
				<div id="address">С 9:00 ДО 22:00 БЕЗ ВЫХОДНЫХ!</div>
				<a class="callme_viewform" href="#"><em>Заказать обратный звонок</em></a>
			</div>
                         {if $test == 'products_page' || $test2 == 'product_page'}
               <!--/noindex-->
         {/if}
		</div>
                
      
                
		<div id="category_search">
			<div id="catalog" class="icon"><span class="nc"><i class="icon icon_cat"></i>КАТАЛОГ ТОВАРОВ</span>
				{* Рекурсивная функция вывода дерева категорий *}
				{function name=categories_tree}
					{if $categories}
					{foreach $categories as $c}
						{* Показываем только видимые категории *}
						{if !$c->metka}{if $c->visible}
						<li>
							<a {if $category->id == $c->id}class="selected"{/if} href="catalog/{$c->url}" data-category="{$c->id}"><i class="icon cat_{$c->id}"></i>{$c->name|escape}</a>
						
							{if $c->subcategories}<ul>{categories_tree categories=$c->subcategories}</ul>{/if}
						</li>
						{/if}{/if}
					{/foreach}			
				{/if}
			{/function}	
				<div id="category"{if $module=='MainView'} class="act"{/if}><ul>{categories_tree categories=$categories}</ul>
				
				{if $module=='MainView'}
				{get_ttoday_products_plugin var=get_ttoday_products_plugin limit=1}
				<div class="ttoday">
				{if $get_ttoday_products_plugin}
					{foreach $get_ttoday_products_plugin as $product}
						{foreach $product->variants as $v}
							{if  $v@first || $v->price<$pmin}
								{assign var="cpmin" value=$v->compare_price}
								{assign var="pmin" value=$v->price}
							{/if}
						{/foreach}
				<div class="image">
					<a href="products/{$product->url}"><img data-original="{$product->image->filename|resize:100:98}" src="{$product->image->filename|resize:100:98}" alt="{$product->name|escape}"/></a>
				</div>
				<div class="timers icon">
					<div class="names">Товар дня</div>
					<span class="timer">осталось
						<span class="number days"></span>
						<span class="measure measure-days">:</span>
						<span class="number hours"></span>
						<span class="measure measure-hours">:</span>
						<span class="number minutes"></span>
						<span class="measure measure-minutes"></span>
					</span>
				</div>
				<div class="name">
					<a href="products/{$product->url}"><h3>{$product->name|escape}</h3></a>
				</div>
				<div class="price">{$pmin|convert} {$currency->sign|escape}</div>				
						
						
					{/foreach}
				{/if}
					
				
				</div>
				{/if}
				</div>
			</div>
                        
       
                        
                        
                        
			<div id="search">
				<form action="products">
					<input class="input_search" type="text" name="keyword" placeholder="Введите наименование или артикул..." value="" onblur="if (this.value == '') { this.placeholder = 'Введите наименование или артикул...';this.style.background = '#F5DD7C';}" onfocus="if (this.value == '') { this.placeholder = 'Введите наименование или артикул...';this.style.background = '#fff';}" />
					<input class="button_search icon" value="" type="submit" />
				</form>
			</div>
			
			{get_session_products key=compare}
		    <div id="compare">
            {include file='products_session_compare_informer.tpl' session=$compare}
            </div>
			<div id="cart">{include file='cart_informer.tpl'}</div>
		</div>
	</div>
</div>
<div id="content">
	<div id="wrapper">
  
{$content}
             
            
	</div>
</div>	
<div id="footer">




	<div id="wrapper">
		<div id="social" class="icon"><span>Мы в социальных сетях —</span> <a href="https://vk.com/clubvelocube" target="_blank"><i class="vk icon"></i></a></div>
	</div>
	<div id="foot">
		<div id="wrapper">
			<div class="desc">© Все права защищены 2013 - 2015<br /> Интернет-магазин — Velocube.ru</div>
			
                         {if $test == 'products_page' || $test2 == 'product_page'} 
                                        
                        {/if}
                        
                        <ul class="menu">
			{foreach $pages as $p}
				{* Выводим только страницы из первого меню *}
				{if $p->menu_id == 1}
					<li {if $page && $page->id == $p->id}class="selected"{/if}>
						<a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a>
					</li>
				{/if}
			{/foreach}
			</ul>
			<div class="contact">
				<span class="phone">8 (499) 653-88-38</span>
				<div class="address">С 9:00 ДО 22:00 БЕЗ ВЫХОДНЫХ!</div>
			</div>
                        
                     
                                                
                       
			<div class="copyright">Информация, изложенная на сайте, не является публичной офертой, согласно статьи 437 Гражданского кодекса РФ, поскольку не содержит все необходимые существенные условия договора. О наличии товара, его цене, стране изготовителе, комплектации и основных потребительских свойствах уточняйте при оформлении заказа у наших операторов. Договор купли-продажи считается заключенным с момента оплаты товара и получения документов, подтверждающих покупку.<br /></div>
         </div>
         
         {if $test == 'products_page' || $test2 == 'product_page'}
             
         {/if}
         
	</div>
</div>

{literal}
<script type='text/javascript'>
	(function() {
		var s = document.createElement('script');
		s.type ='text/javascript';
		s.id = 'supportScript';
		s.charset = 'utf-8';
		s.async = true;
		s.src = 'https://lcab.sms-uslugi.ru/support/support.js?h=9c98412cf1b010dbfb1fb1eecc44f39d';
		var sc = document.getElementsByTagName('script')[0];
		
		var callback = function(){
			/*
				Здесь вы можете вызывать API. Например, чтобы изменить отступ по высоте:
				supportAPI.setSupportTop(200);
			*/
		};
		
		s.onreadystatechange = s.onload = function(){
			var state = s.readyState;
			if (!callback.done && (!state || /loaded|complete/.test(state))) {
				callback.done = true;
				callback();
			}
		};
		
		if (sc) sc.parentNode.insertBefore(s, sc);
		else document.documentElement.firstChild.appendChild(s);
	})();

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80547125-1', 'auto');
  ga('send', 'pageview');

</script>



<!-- Yandex.Metrika counter -->

<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter21197344 = new Ya.Metrika({id:21197344,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter 

<script type="text/javascript">var yaParams = {/*Здесь параметры визита*/};</script><script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script><script type="text/javascript">try { var yaCounter21197344 = new Ya.Metrika({id:21197344, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true,params:window.yaParams||{ }}); } catch(e) { }</script>{/literal}<noscript><div><img src="//mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
-->

<script type="text/javascript" src="design/{$settings->theme}/js/jquery.load.js"></script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=eGebGymC0byfz8eRp0yNWVwi9kc2l2KoxV7ofx/yRH6hdLNwJBoFIQbjN8To*Ko7RJkq7/Spee8FpST1VqQH4t7AtglwcVtov4ZcEZxV2EtIzG3QrQADlFnld4EXcEMy2avsz4tDzVCH7l9jfgTSibQEuQzD6DTzsRp7jk1Xv4M-';</script>

{if $module != 'OrderView'}
{literal}
<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter21197344= new Ya.Metrika({id:21197344,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,params:window.yaParams||{ }});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21197344" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

{/literal}
{/if}

<script src='//st.yagla.ru/js/y.c.js'></script>
<script src="https://cdn.jsdelivr.net/stopsovetnik/latest/ss.min.js"></script>
<!-- reenter.ru -->
<script type="text/javascript" src="//cdn.reenter.ru/825/m.js"></script>
<!-- reenter.ru -->



</body>
</html>