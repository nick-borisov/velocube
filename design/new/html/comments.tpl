

<div class="desc" style="  width: 100%; margin-left: 0px;">


<div id="page_title"><h1>{$page->header}</h1></div>
{if $page->body}
<div id="page_body">{$page->body}</div>
{/if}

{function name=comments_tree}	
{if $comments}
		{foreach $comments as $comment}
		<a name="comment_{$comment->id}"></a>
		<div class="yaComments {if $comment->admin == 1}admin{/if}">              
			<div class="header">
				<div class="author">{$comment->name|escape} {if !$comment->approved}(ожидает модерации){/if}</div>
				<div class="date">{$comment->date|date}{*$comment->date|time*}</div>
				<div class="grade grade-{$comment->rate}"></div>
			</div>
			<div class="comment-wrap"> 
				{if $comment->text}
				<div class="head-plus">Комментарии:</div>
				<div class="yc-text">{$comment->text|escape|nl2br}</div>
				<br>
				{/if}
				{if $comment->dost}
				<div class="head-plus">Достоинства:</div>
				<div class="yc-text">{$comment->dost|escape|nl2br}</div>
				<br>
				{/if}
				{*
				<a onclick="$('#parent').val({$comment->id});" class="button open-comment-form" href="#new_comment_form">
					<span>Ответить</span>		
				</a>
				*}
			
				{if $comment->nedost}
				<div class="comment-wrap">
					<div class="head-minus">Недостатки:</div>
					<div class="yc-text">{$comment->nedost|escape|nl2br}<br></div>
				</div>
			</div>
			{/if}
			{* comments_tree comments = $comment->subcomments *}
			
		</div>
		{/foreach}
	{else}
	<p>
		Пока нет комментариев
	</p>
{/if}
{/function}
{comments_tree comments = $comments}
<div class="add-comment">
	<a  onclick="$('#parent').val(0);" id="new_comment_tab_btn" class="button add-comment open-comment-form" style="padding: 10px 30px;font-size: 18px;" href="#new_comment_form">
		Напишите отзыв!
	</a>
</div>
<div style="display:none"><div id="new_comment_form">	
	<!--Форма отправления комментария-->	
	<form class="comment_form" method="post">
		<h2>Написать комментарий</h2>
		{if $error}
		<div class="message_error">
			{if $error=='captcha'}
			Неверно введена капча
			{elseif $error=='empty_name'}
			Введите имя
			{elseif $error=='empty_comment'}
			Введите комментарий
			{/if}
		</div>
		{/if}
		<input type="hidden" id="parent" name="parent_id" value="0"/>
		<input type="hidden" id="admin" name="admin" value="{if $smarty.session.admin == 'admin'}1{else}0{/if}"/>
		<textarea class="comment_textarea" id="comment_text" name="text" data-format=".+" data-notice="Введите комментарий">{$comment_text}</textarea><br />
		<div>
		<label for="comment_name">Имя</label>
		<input class="input_name" type="text" id="comment_name" name="name" value="{$comment_name}" data-format=".+" data-notice="Введите имя"/><br />

		<input class="button" type="submit" name="comment" value="Отправить" />
		
		<label for="comment_captcha">Число</label>
		<div class="captcha"><img src="captcha/image.php?{math equation='rand(10,10000)'}" alt='captcha'/></div> 
		<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>
		
		</div>
	</form>
	<!--Форма отправления комментария (The End)-->
</div></div>	
</div>
<!-- Комментарии (The End) -->
{include file='pagination.tpl'}
	{literal}
		<script>
		$(document).ready(function(){
			

			if (!!$.prototype.fancybox)
				$('.open-comment-form').fancybox({
					'autoSize' : false,
					'width' : 600,
					'height' : 'auto',
					'hideOnContentClick': false
				});

			$(document).on('click', '#id_new_comment_form .closefb', function(e){
				$.fancybox.close();
			});

			
		});
		</script>
	{/literal}