{* Шаблон корзины *}

{$meta_title = "Корзина" scope=parent}

<h1>
{if $cart->purchases}В корзине {$cart->total_products} {$cart->total_products|plural:'товар':'товаров':'товара'}
{else}Корзина пуста{/if}
</h1>
<div style="display:none">
    <div id="please_wait">Пожалуйста, подождите. Заказ оформляется...
        <div class="plz_wait_loader"></div>
    </div>
    
</div>
{if $cart->purchases}
<form method="post" name="cart">

{* Список покупок *}
<table id="purchases">

{foreach from=$cart->purchases item=purchase}
<tr>
	{* Изображение товара *}
	<td class="image">
		{$image = $purchase->product->images|first}
		{if $image}
		<a href="products/{$purchase->product->url}"><img src="{$image->filename|resize:50:50}" alt="{$purchase->product->name|escape}"></a>
		{/if}
	</td>
	
	{* Название товара *}
	<td class="name">
		<a href="products/{$purchase->product->url}">{$purchase->product->name|escape}</a>
		{$purchase->variant->name|escape}
		{if $purchase->variant->stock == 0}<p style="color:#F00">Товар под заказ</p>
		{else if $purchase->variant->stock == 1}<p style="color:#FF6C00">Последний товар</p>
		{else}<p style="color:#1DD446">В наличии</p>{/if}
	</td>

	{* Цена за единицу *}
	<td class="price">
		{($purchase->variant->price)|convert} {$currency->sign}
	</td>

	{* Количество *}
	<td class="amount">
		<input name="amounts[{$purchase->variant->id}]" type="text" value="{$purchase->amount}" onchange="document.cart.submit();" style="width:50px;text-align:center;" />
	</td>

	{* Цена *}
	<td class="price">
		{($purchase->variant->price*$purchase->amount)|convert}&nbsp;{$currency->sign}
	</td>
	
	{* Удалить из корзины *}
	<td class="remove">
		<a href="cart/remove/{$purchase->variant->id}">
		<img src="design/{$settings->theme}/images/delete.png" title="Удалить из корзины" alt="Удалить из корзины">
		</a>
	</td>
			
</tr>
{/foreach}
{if $user->discount}
<tr>
	<th class="image"></th>
	<th class="name">скидка</th>
	<th class="price"></th>
	<th class="amount"></th>
	<th class="price">
		{$user->discount}&nbsp;%
	</th>
	<th class="remove"></th>
</tr>
{/if}
{if $coupon_request}
<tr class="coupon">
	<th class="image"></th>
	<th class="name" colspan="3">Код купона или подарочного ваучера
		{if $coupon_error}
		<div class="message_error">
			{if $coupon_error == 'invalid'}Купон недействителен{/if}
            {*$related_products_stop = $this->coupons->get_related_products_stop(intval($cart->coupon->id));*}
		</div>
		{/if}
	
		<div>
		<input type="text" name="coupon_code" value="{$cart->coupon->code|escape}" class="coupon_code">
		</div>
        
		{if $cart->coupon->min_order_price>0}(купон {$cart->coupon->code|escape} действует для заказов от {$cart->coupon->min_order_price|convert} {$currency->sign}){/if}
        {if $smarty.session.bad_coupon}
            <br /><span class="bad_coupon">(для купона не выполнены условия)</span>
        {/if}
		<div>
		<input type="button" name="apply_coupon"  value="Применить купон" onclick="document.cart.submit();">
		</div>
	</th>
	<th class="price">
		{if $cart->coupon_discount>0}
		&minus;{$cart->coupon_discount|convert}&nbsp;{$currency->sign}
		{/if}
	</th>
	<th class="remove"></th>
</tr>

{literal}
<script>
$("input[name='coupon_code']").keypress(function(event){
	if(event.keyCode == 13){
		$("input[name='name']").attr('data-format', '');
		$("input[name='email']").attr('data-format', '');
		document.cart.submit();
	}
});
</script>
{/literal}

{/if}

<tr style="height: 50px;">
	<th class="image"></th>
	<th class="name"></th>
	<th class="price" colspan="4">
		Итого
		{$cart->total_price|convert}&nbsp;{$currency->sign}
	</th>
</tr>
</table>
<a class="checkout" href="#please_wait"></a><input type="submit" name="checkout" class="button" value="Оформить заказ" style="float: right"/>
{* Связанные товары *}
{*
{if $related_products}
<h2>Так же советуем посмотреть</h2>
<!-- Список каталога товаров-->
<ul class="tiny_products">
	{foreach $related_products as $product}
	<!-- Товар-->
	<li class="product">
		
		<!-- Фото товара -->
		{if $product->image}
		<div class="image">
			<a href="products/{$product->url}"><img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/></a>
		</div>
		{/if}
		<!-- Фото товара (The End) -->

		<!-- Название товара -->
		<h3><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h3>
		<!-- Название товара (The End) -->

		{if $product->variants|count > 0}
		<!-- Выбор варианта товара -->
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					{if $v->name}<label class="variant_name" for="related_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
				<td>
					<a href="cart?variant={$v->id}">в корзину</a>
				</td>
			</tr>
			{/foreach}
			</table>
		<!-- Выбор варианта товара (The End) -->
		{else}
			Нет в наличии
		{/if}


	</li>
	<!-- Товар (The End)-->
	{/foreach}
</ul>
{/if}
*}

<div class="checkout-form">
		<div class="checkout-form_user">
			<h2>Адрес получателя</h2>
			<div class="form cart_form">
			{if $error}
				<div class="message_error">
					{if $error == 'empty_name'}Введите имя{/if}
					{if $error == 'empty_email'}Введите email{/if}
                    {if $error == 'empty_phone'}Введите телефон{/if}
					{if $error == 'captcha'}Капча введена неверно{/if}
				</div>
			{/if}
			
			<div id="hideme">
				<label>Имя, фамилия</label>
				<input name="name" type="text" value="{$name|escape}" data-format=".+" data-notice="Введите имя"/>
				
                <div class="passport" style="display: none">
    				<label>Паспортные данные (для Транспортной компании)</label>
    				<input name="passport" type="text" value="{$passport|escape}" />
                </div> 
                
				<label>Email</label>
				<input name="email" type="text" value="{$email|escape}" data-format="email" data-notice="Введите email" />
				
				<label>Телефон</label>
				<input id="phonenumber" name="phone" type="text" value="{$phone|escape}" placeholder="+7xxxxxxxxxx" data-format=".+" data-notice="Введите телефон" />
                <span class="add">
                    <i class="dash_link" id="add_new_feature">Добавить телефон</i>
                </span>
                <span class="item_phone" style="display:none">
                    <input id="phonenumber2" type="text" value="{$phone2|escape}" placeholder="+7xxxxxxxxxx" />
                    <span class="delete" style="display:none"><i class="dash_link">Удалить</i></span>
                </span>    
                
                {*<input id=new_feature type=text style="float: right;margin-right: 145px;"/>
                
                
                    <input name="sms_phone[]" class="simpla_inp" type="text" value="{$phone|escape}" {if $phone@iteration>2}style="margin-left: 170px"{/if}/>
                    <span class="add"{if $phone@iteration!=1}style="display:none"{/if}><i class="dash_link" id="add_new_feature">Добавить телефон</i></span>
                    <span {if $phone@iteration==1}style='display:none;'{/if} class="delete"><i class="dash_link">Удалить</i></span>
                </span>*}
                
				
				<label>Адрес доставки</label>
				<input name="address" type="text" value="{$address|escape}"/>
				
				<label>Комментарий к&nbsp;заказу</label>
				<textarea name="comment" id="order_comment">{$comment|escape}</textarea>
			</div>
				<input type="submit" name="checkout" class="button" value="Оформить заказ">
			</div>
		</div>
		<div class="checkout-form_deliveries">
		{if $deliveries}
			<h2>Выберите способ доставки:</h2>
			<ul id="deliveries">
				{foreach $deliveries as $delivery}
				<li>
					<div class="checkbox"><input type="radio" name="delivery_id"  onclick="mkchange()" value="{$delivery->id}" {if $delivery_id==$delivery->id}checked{elseif $delivery@first}checked{/if} id="deliveries_{$delivery->id}"></div>
					
					<h3><label for="deliveries_{$delivery->id}">{$delivery->name} {if $cart->total_price < $delivery->free_from && $delivery->price>0}({$delivery->price|convert}&nbsp;{$currency->sign}){elseif $cart->total_price >= $delivery->free_from}(бесплатно){/if}</label></h3>
					
					<div class="description">{$delivery->description}</div>
				</li>
				{/foreach}
			</ul>
		{/if}
		</div>
	</div>
</form>
{else}
  В корзине нет товаров
{/if}
<script>
$(function(){
    $('#phonenumber').mask('+79999999999');
    $('#phonenumber2').mask('+79999999999');
    
    $('#deliveries li div.description').hide();
    $('input[name="delivery_id"]:checked').parents('li').find('.description').show().fadeIn(600);
    
    // Добавление нового свойства товара
	
	$('#add_new_feature').click(function() {
	   
       $('#phonenumber2').parent().show();
	   $('#phonenumber2').show().attr('name', 'phone2').focus();
       $('#add_new_feature').hide();
       $('.item_phone .delete').show();
	   return false;		
	});
    
    // Удаление категории
	$(".item_phone .delete").live('click', function() {
		$(this).parent(".item_phone").hide().fadeOut(200, function() { $('#phonenumber2').removeAttr('name'); });
        $('#add_new_feature').show();
		return false;
	});
});

</script>
{literal}
 
<script>
        function mkchange() {
            $('#deliveries li div.description').hide();
            $('input[name="delivery_id"]:checked').parents('li').find('.description').show().effect('slide','slow');
			if (document.getElementById("deliveries_6").checked)
			{
		 
				document.getElementById("hideme").style.display = "block";
				$('#hideme .passport').show();
			}
			else if (document.getElementById("deliveries_3").checked)
			{
				document.getElementById("hideme").style.display = "block";
				//$('#hideme').html( $('#hiddenblock').html() );
                $('#hideme .passport').show();
			}
			 else if (document.getElementById("deliveries_7").checked)
			{
				document.getElementById("hideme").style.display = "block";
				//$('#hideme').html( $('#hiddenblock').html() );
                $('#hideme .passport').show();
			}
			else if (document.getElementById("deliveries_8").checked)
			{
				document.getElementById("hideme").style.display = "block";
				//$('#hideme').html( $('#hiddenblock').html() );
                $('#hideme .passport').show();
			}
			else if (document.getElementById("deliveries_9").checked)
			{
				document.getElementById("hideme").style.display = "block";
				//$('#hideme').html( $('#hiddenblock').html() );
                $('#hideme .passport').show();
			}
			else if (document.getElementById("deliveries_5").checked)
			{
				document.getElementById("hideme").style.display = "block";
				//$('#hideme').html( $('#hiddenblock').html() );
                $('#hideme .passport').show();
			}
			else 
			{
				document.getElementById("hideme").style.display = "block";
				//$('#hideme').html( $('#hiddenblock_orig').html() );
                $('#hideme .passport').hide();
			}
			
		 	 
		}

</script>
{/literal}

<div id="hiddenblock" style="display:none">
				<label>Имя, фамилия</label>
				<input name="name" type="text" value="{$name|escape}" data-format=".+" data-notice="Введите имя"/>
                
                
				
				<label>Email</label>
				<input name="email" type="text" value="{$email|escape}" data-format="email" data-notice="Введите email" />
				
				<label>Телефон</label>
				<input name="phone" type="text" value="{$phone|escape}" />
				
				<label>Адрес доставки</label>
				<input name="address" type="text" value="{$address|escape}"/>
				
				<label>Комментарий к&nbsp;заказу</label>
				<textarea name="comment" id="order_comment">{$comment|escape}</textarea>
</div>

<div id="hiddenblock_orig" style="display:none">
				<label>Имя, фамилия</label>
				<input name="name" type="text" value="{$name|escape}" data-format=".+" data-notice="Введите имя"/>
				
                   
                
				<label>Email</label>
				<input name="email" type="text" value="{$email|escape}" data-format="email" data-notice="Введите email" />
				
				<label>Телефон</label>
				<input name="phone" type="text" value="{$phone|escape}" />
				
				<label>Адрес доставки</label>
				<input name="address" type="text" value="{$address|escape}"/>
				
				<label>Комментарий к&nbsp;заказу</label>
				<textarea name="comment" id="order_comment">{$comment|escape}</textarea>
			</div>