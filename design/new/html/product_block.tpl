<li class="products">
		{foreach $product->variants as $v}
			{if  $v@first || $v->price<$pmin}
				{assign var="cpmin" value=$v->compare_price}
			{/if}
		{/foreach}
		{if $cpmin}<div class="sales">Акция</div>{/if}
		
		{$date1=date('Y.m.d', strtotime($product->date)+60*60*24*10)}
		{$date2=$smarty.now|date_format:"%Y.%m.%d"}
		
		{if $date2 <= $date1}<div class="newss">Новинка</div>{/if}
	
	{if $smarty.session.admin == 'admin'}<div style="display:block;width:208px;height:21px;position:absolute;margin-top:190px;background:#000;text-align:center;color:#fff;z-index:2;">{$product->variants[0]->viewed}</div>{/if}
	
	
	{if $product->image}
	<div class="image">
		<a data-product="{$product->id}" href="products/{$product->url}"><img data-original="{$product->image->filename|resize:200:200}" src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/></a>
		{*<a onclick="yaCounter21197344.reachGoal('quick_viev');"  class="quick_viev button" rel="{$product->id}">Быстрый просмотр</a>*}
	</div>
	{else}
	<div class="image">
		<a data-product="{$product->id}" href="products/{$product->url}"><img src="{"nophoto.png"|resize:200:200}" alt="{$product->name|escape}"/></a>
		{*<a onclick="yaCounter21197344.reachGoal('quick_viev');"  class="quick_viev button" rel="{$product->id}">Быстрый просмотр</a>*}
	</div>
	
	{/if}
	

	<!-- Ретинг -->
		<div id="product_{$product->id}" class="raters">
		{if $product->rating != 0}
			<span class="rater-starsOff1" style="width:87px;">
				<span class="rater-starsOn1" style="width:{$product->rating*87/5|string_format:"%.0f"}px"></span>
			</span>
			<span class="test-text">
				<noindex><a rel="nofollow" data-product="{$product->id}" href="products/{$product->url}#comments"><span class="rater-rating">{$product->votes|string_format:"%.0f"} {$product->votes|plural:'отзыв':'отзывов':'отзыва'}</span></a></noindex>
			</span>
		{/if}
		</div>
	<!-- Рейтинг (The End) -->


	<a data-product="{$product->id}" href="products/{$product->url}">
	<div class="name">
		<div font-size: 14px;> {$product->name|escape}</div>
	</div>
	</a>
	
	
	
	<div class="annotation">{$product->annotation}</div>

	
{if $product->variants|count > 1}
	
	
	{foreach $product->variants as $v}
		{if (($pmin < 1) and ($v->price > 0) and ($v->stock > 0 or $product->sklad_id > 0))}
				{assign var="cpmin" value=$v->compare_price}
				{assign var="pmin" value=$v->price}
		{/if}
		
			{if  (($v->price<$pmin) and ($v->stock > 0 or $product->sklad_id > 0))}
				{assign var="cpmin" value=$v->compare_price}
				{assign var="pmin" value=$v->price}
			{/if}
			
		{/foreach}
		{if $pmin > 0}
			<div class="price{if $cpmin} sale{/if}">{$pmin|convert} {$currency->sign|escape}</div>	
		{/if}
	<a class="button" href="products/{$product->url}">Подробней</a>	
	
	{else}

	{if $v->stock == 0 AND $product->sklad_id == 0 }
	

	{else}
	<div class="price{if $product->variants[0]->compare_price > 0} sale{/if}">{$product->variants[0]->price|convert} {$currency->sign|escape}</div>
	<form class="variants" action="/cart">
		<input id="discounted_{$product->variants[0]->id}" name="variant" value="{$product->variants[0]->id}" type="radio" class="variant_radiobutton" checked style="display:none;"/>
		<input type="submit" onclick="yaCounter21197344.reachGoal('addcart');" class="button" value="В корзину" data-result-text="В корзине"/>
	</form>
	{/if}

{/if}

	

	 {if $compare->ids && in_array($product->id, $compare->ids)}
        <span class='mylist_add'>уже в списке</span>
        {else}
        <span class='mylist_add'><a href="/compare?id={$product->id}" class='addps' data-id='{$product->id}' data-key='compare' data-informer='1' data-result-text='уже в списке'>добавить к сравнению</a></span>
        {/if}
       
	
</li>