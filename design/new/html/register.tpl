{* Страница регистрации *}

{* Канонический адрес страницы *}
{$canonical="/user/register" scope=parent}

{$meta_title = "Регистрация" scope=parent}

<script src="design/new/js/jquery.maskedinput.js" type="text/javascript"></script>
{literal}
<script type="text/javascript">
jQuery(function($){
    jQuery(".phone").mask("+7 (999) 9999999");
});
</script> 
{/literal}

<h1>Регистрация</h1>

{if $error}
<div class="message_error">
	{if $error == 'empty_name'}Введите имя
	{elseif $error == 'empty_email'}Введите email
	{elseif $error == 'empty_password'}Введите пароль
	{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
	{elseif $error == 'captcha'}Неверно введена капча
	{else}{$error}{/if}
</div>
{/if}

<form class="form register_form" method="post">
	<label>Имя</label>
	<input type="text" name="name" data-format=".+" data-notice="Введите имя" value="{$name|escape}" maxlength="255" />

	<label>Фамилия</label>
	<input type="text" name="surname" data-format=".+" data-notice="Введите вашу фамилию" value="{$surname|escape}" maxlength="255" />	
	
	<label>Телефон</label>
	<input type="text" name="phone" class="phone" data-format=".+" data-notice="Введите ван телефон" value="{$phone|escape}" maxlength="20" />
	
	<label>Email</label>
	<input type="text" name="email" data-format="email" data-notice="Введите email" value="{$email|escape}" maxlength="255" />

    <label>Пароль</label>
    <input type="password" name="password" data-format=".+" data-notice="Введите пароль" value="" />

	<div class="captcha"><img src="captcha/image.php?{math equation='rand(10,10000)'}"/></div> 
	<input class="input_captcha" id="comment_captcha" type="text" name="captcha_code" value="" data-format="\d\d\d\d" data-notice="Введите капчу"/>

	<input type="submit" class="button" name="register" value="Зарегистрироваться">

</form>
