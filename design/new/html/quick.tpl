<div class="box-popup">
<div class="box-modal" style="width:auto; height:auto!important;"><div class="box-modal_close arcticmodal-close">X</div>



<div class="product">

	<div class="images" style="margin-right:20px;">
	
             <div class="main_img">
			<a href="{$product->image->filename|resize:1200:1200}" class="jqzoom" rel='gal1' title="{$product->name|escape}">
					<img src="{$product->image->filename|resize:300:300}" title="{$product->name|escape}">
				</a>
		
	</div>
		
		
	<div style="clear:both;"></div>
		
		<div class="ims">
		
				{foreach $product->images as $i=>$image}

					<a {if $image@index == 0}class="zoomThumbActive" {/if}href="#" onHover="javascript:void(0) return false;" rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$image->filename|resize:300:300}', largeimage: '{$image->filename|resize:1200:1200}' {literal}}{/literal}"><img src='{$image->filename|resize:50:50}' alt="{$product->name|escape}" /></a>

				{/foreach}
		
		</div>
	</div>
	<!-- Описание товара -->
	<div class="description" style="width:560px;">
<!-- Ретинг -->
		<div id="product_{$product->id}" class="raters">
		{if $product->rating != 0}
			<span class="rater-starsOff1" style="width:87px;">
				<span class="rater-starsOn1" style="width:{$product->rating*87/5|string_format:"%.0f"}px"></span>
			</span>
			<span class="test-text">
				<noindex><a rel="nofollow" data-product="{$product->id}" href="products/{$product->url}#comments"><span class="rater-rating">{$product->votes|string_format:"%.0f"} {$product->votes|plural:'отзыв':'отзывов':'отзыва'}</span></a></noindex>
			</span>
		{/if}
		</div>
	<!-- Рейтинг (The End) -->
	
	<h2 data-product="{$product->id}">{$product->name|escape}</h2>
	
		{$product->annotation}
		
		
		<!-- Выбор варианта товара -->
		<form class="variants" action="/cart">
			<table>
			{foreach $product->variants as $v}
			<tr class="variant">
				<td>
					<input id="product_{$v->id}" name="variant" value="{$v->id}" type="radio" class="variant_radiobutton" {if $product->variant->id==$v->id}checked{/if} {if $product->variants|count<2}style="display:none;"{/if}/>
				</td>
				<td>
					{if $v->name}<label class="variant_name" for="product_{$v->id}">{$v->name}</label>{/if}
				</td>
				<td>
					{if $v->compare_price > 0}<span class="compare_price">{$v->compare_price|convert}</span>{/if}
					<span class="price">{$v->price|convert} <span class="currency">{$currency->sign|escape}</span></span>
				</td>
			</tr>
			{/foreach}
			</table>
			<input type="submit" onclick="yaCounter21197344.reachGoal('add_quick_viev');" class="button" value="В корзину" data-result-text="Добавлено"/>
			
			
		</form>
		<!-- Выбор варианта товара (The End) -->

	{if $product->features}
	<!-- Характеристики товара -->
</br>
	<h2>Характеристики</h2>
	<ul class="features" style="margin-bottom:0px;">
	{foreach $product->features as $f name="goo"}
	{if $smarty.foreach.goo.iteration < 6}
	<li>
		<label>{$f->name}</label>
		<span>{$f->value}</span>
	</li>
	{/if}
	{/foreach}
	</ul>
	<!-- Характеристики товара (The End)-->
	{/if}	


<a data-product="{$product->id}" href="products/{$product->url}">Подробнее</a>
</div>
	


	

	
	<div style="clear:both;"></div>

	
	
</div>

</div>

</div>
<!-- Описание товара (The End)-->



{* Увеличитель картинок *}
{literal}

 <script>
     

              
	


 $('.jqzoom').jqzoom({
		 zoomType: 'reverse',
            lens: true,
            preloadImages: true,
            zoomWidth: 560,
            zoomHeight: 300,
            xOffset: 20,
            yOffset: 0,
            position: 'right',
            preloadText: 'Идет увеличение...',
            title: true,
            imageOpacity: 0.4, // для zoomType 'reverse'
            showEffect: 'show',
            hideEffect: 'fadeout',
            fadeinSpeed: 'slow', // Если для showEffect выбран 'fadein'
            fadeoutSpeed: 500 // Если для hideEffect выбран 'fadeout'
	});     
      
 </script>
{/literal}
