{* Шаблон страницы зарегистрированного пользователя *}
<script src="design/new/js/jquery.maskedinput.js" type="text/javascript"></script>
{literal}
<script type="text/javascript">
jQuery(function($){
    jQuery(".phone").mask("+7 (999) 9999999");
});
</script> 
{/literal}

<h1>{$user->name|escape}</h1>

{if $error}
<div class="message_error">
	{if $error == 'empty_name'}Введите имя
	{elseif $error == 'empty_email'}Введите email
	{elseif $error == 'empty_password'}Введите пароль
	{elseif $error == 'user_exists'}Пользователь с таким email уже зарегистрирован
	{else}{$error}{/if}
</div>
{/if}

<form class="form" method="post">
	<label>Имя</label>
	<input data-format=".+" data-notice="Введите имя" value="{$name|escape}" name="name" maxlength="255" type="text"/>
 
	<label>Email</label>
	<input data-format="email" data-notice="Введите email" value="{$email|escape}" name="email" maxlength="255" type="text"/>
	
	
	<label>Фамилия</label>
	<input type="text" name="surname" data-format=".+" data-notice="Введите вашу фамилию" value="{$surname|escape}" maxlength="255" />	
	
	<label>Телефон</label>
	<input type="text" name="phone" class="phone" data-format=".+" data-notice="Введите ван телефон" value="{$phone|escape}" maxlength="20" />	

	
	<label><a href='#' onclick="$('#password').show();return false;">Изменить пароль</a></label>
	<input id="password" value="" name="password" type="password" style="display:none;"/>
	<input type="submit" class="button" value="Сохранить">
</form>

{if $orders}
<p></p>
<h2>Ваши заказы</h2>
<ul id="orders_history">
{foreach name=orders item=order from=$orders}
	<li>
	{$order->date|date} <a href='order/{$order->url}'>Заказ №{$order->id}</a>
	{if $order->paid == 1}оплачен,{/if} 
	{if $order->status == 0}ждет обработки{elseif $order->status == 1}в обработке{elseif $order->status == 2}выполнен{/if}
    {if $order->manager}<div class="manager">Ваш личный менеджер: {$order->manager}</div>{/if}
	</li>
{/foreach}
</ul>
{/if}