{if $products}

	<script src="design/{$settings->theme}/js/cookies.js"></script>
{if $products|count>0}
<div class="sort" id="sort_block">Сортировать по <a class="go_sort {if $sort=='position'}selected{/if}" id="position" >умолчанию</a> <a class="go_sort {if $sort=='price'}selected{/if}" id="price" >цене</a><a class="go_sort {if $sort=='name'}selected{/if}" id="name">названию</a> <a class="go_sort {if $sort=='viewed'}selected{/if}" id="viewed" >популярности</a><div class="product_view">Выводить <a id="product_view_grid">картинками</a> или <a id="product_view_list">списком </a></div></div>



{literal}
<script>
$('.go_sort').bind('click', function(event){
	
  $('input[name=sort]').val(this.id);
  	var form_data = $('#form_filter').serializeArray();
		$.ajax({
		type: 'POST',
		url: "ajax/update_products.php",
		data: form_data,
		dataType: 'json',
		success: function(data){
			$('#prods').html(data);
			
		}
	});
});

$('.pagination a').bind('click', function(event){
	event.preventDefault();


	
  
if ($('#form_filter').length) {
  $('input[name=page]').val(this.id);
  	var form_data = $('#form_filter').serializeArray();
		$.ajax({
		type: 'POST',
		url: "ajax/update_products.php",
		data: form_data,
		dataType: 'json',
		success: function(data){
			 $('html, body').animate({ scrollTop: $('#product_list').offset().top - 100});
			$('#prods').html(data);
			
		}
	});

}
else {

	var page = this.id;
	var cat = $('#f_cat').val();   
var mark = $('#f_mark').val(); 
var brand = $('#f_brand').val(); 
var word = $('#f_word').val(); 
		$.ajax({
		type: 'POST',
		url: "ajax/update_products.php",
		data: { brand_id:brand, cat_id:cat, mark_id:mark, keyword:word, page:page },
		dataType: 'json',
		success: function(data){
			 $('html, body').animate({ scrollTop: $('#product_list').offset().top - 100});
			$('#prods').html(data);
			
			
		}
	});


}


});


$(function(){
	$(".image img").lazyload({
		effect: "fadeIn"
	});
});
$('a.quick_viev').live('click', function(){
    var pro_id = this.getAttribute('rel');

    $.arcticmodal({
    type: 'ajax',
    url: 'ajax/quick.php',
    ajax: {
        type: 'GET',
        cache: false,
        data: {id: pro_id},
        dataType: 'json',
        success: function(data, el, response) {
          
           data.body.html(response);
        }
    }
	});


});
</script>
{/literal}



{/if}

<ul id="product_list" class="tiny_products">
	{foreach $products as $product}
		{include file='product_block.tpl'}
	{/foreach}	
</ul>

{include file='pagination.tpl'}
{else}Товары не найдены{/if}
