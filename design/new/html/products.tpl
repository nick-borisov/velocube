 
<div id="left">{if $category->brands || $features}<div id="filter"><div class="loading"></div></div>{/if}
<input type="hidden" id="f_cat" value="{$category->id}">
<input type="hidden" id="f_mark" value="{$mark->id}">
<input type="hidden" id="f_brand" value="{$brand->id}">
<input type="hidden" id="f_word" value="{$keyword}">
{literal}<script type="text/javascript">
$(window).load(function(){
    $('.nonselected').click(function(e){
       // $(this).toggle();
       // $(this).next().toggle();
    });
    
var cat = $('#f_cat').val();   
var mark = $('#f_mark').val(); 
var brand = $('#f_brand').val(); 

   $.ajax({
		type: 'POST',
		url: "ajax/filter.php",
		data: {cat : cat, mark : mark, brand : brand},
		dataType: 'json',
		beforeSend:function(){$('#filter').html('<div class="loading"></div>');},
		success: function(data){
			$('#filter').html(data);
			
		}
	});
  });

 $('.apply').live('click', function() {
       var form_data = $('#form_filter').serializeArray();
		$.ajax({
		type: 'POST',
		url: "ajax/update_products.php",
		data: form_data,
		dataType: 'json',
		success: function(data){
			$('#prods').html(data);
			$('.popover').hide();
			
		}
	});
        return false
});
/*
$(document).ready(function(){

var cat = $('#f_cat').val();   
var mark = $('#f_mark').val(); 
var brand = $('#f_brand').val(); 
var word = $('#f_word').val(); 
		$.ajax({
		type: 'POST',
		url: "ajax/update_products.php",
		data: { brand_id:brand, cat_id:cat, mark_id:mark, keyword:word },
		dataType: 'json',
		success: function(data){
			$('#prods').html(data);
			$('.popover').hide();
			
		}
	});


});
*/


</script>{/literal}

{get_cslides var=slide cat=$category->id brand=$brandix pos=1}
{if $slide}	
<br>
{foreach $slide as $sl}
	<div id="pslideleft">
		<img  width="100%" src="{$sl->image}"> 
	</div>
{/foreach}
{/if}
</div>

<div id="main"{if $keyword} class="all"{/if}>

{* Канонический адрес страницы *}
{if $category && $brand}
{$canonical="/catalog/{$category->url}/{$brand->url}" scope=parent}
{elseif $category && $mark}
{$canonical="/catalog/{$category->url}/tag/{$mark->url}" scope=parent}
{elseif $category}
{$canonical="/catalog/{$category->url}" scope=parent}
{elseif $brand}
{$canonical="/brands/{$brand->url}" scope=parent}
{elseif $keyword}
{$canonical="/products?keyword={$keyword|escape}" scope=parent}
{else}
{$canonical="/products" scope=parent}
{/if}

<!-- {$category->id} b {$brand->id} -->
<!-- 23 / 14 -->
{get_cslides var=slide cat=$category->id brand=$brand->id}
{if $slide}	
	<div id="pslide">
		<img  width="100%" src="{$slide[0]->image}"> 
	</div>
{/if}

<script src="{$config->root_url}/design/{$settings->theme}/js/cookies.js"></script>

<div id="path">
<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
<a itemprop="url" href="/"><span itemprop="title">Главная</span></a>
{if $category} 
{foreach from=$category->path item=cat} 
› <a itemprop="url" href="catalog/{$cat->url}"> 
{if {$cat->name|escape} != {$category->name|escape}}
<span itemprop="title">{$cat->name|escape}</span></a>
{else}
{if {$brand->name|escape}==""}
</a>{$cat->name|escape}
{else}
<span itemprop="title">{$cat->name|escape}</span></a>
 {/if}
{/if}

{/foreach} 

{if $brand} > </a>{$brand->name|escape}{/if}
{if $mark} > </a>{$mark->name|escape}{/if}
{if $keyword} ></a> Поиск{/if}
{/if}
</span>

</div>

{if $keyword}<h1>Поиск {$keyword|escape}</h1>
{elseif $page}<h1>{$page->name|escape}</h1>
{else}<h1>{if !$mark}{$category->name|escape}{/if} {$brand->name|escape} {$mark->name|escape} {$keyword|escape}</h1>{/if}

{* Фильтр по меткам *}
{if $marks}
<div id="marks">	
	<!-- <a href="catalog/{$category->url}/" {if !$mark->id}class="selected"{/if}>Все метки</a> -->
	{foreach name=mks item=m from=$marks}
		{if $m->visible}
		<a data-mark="{$m->id}" href="catalog/{$category->url}/tag/{$m->url}" {if $m->id == $mark->id}class="selected"{/if}>{$m->name|escape}</a>
		{/if}
	{/foreach}
</div>
{/if}
{if $current_page_num==1}
{* Описание метки *}
{$mark->description}
{/if}

{if $products}
	<script src="design/{$settings->theme}/js/cookies.js"></script>
    {if $products|count>0}
        <div class="sortd">
        	Сортировать по 
        	<a {if $sort =='position_r'} class="selectedd" href="{url sort=position page=null}"{else}class="nonselected" href="{url sort=position_r page=null}"{/if}>умолчанию</a>
        	<a {if $sort=='price_r'}class="selectedd" href="{url sort=price page=null}" {else}class="nonselected" href="{url sort=price_r page=null}"{/if} >цене</a>
        	<a {if $sort=='name_r'}class="selectedd" href="{url sort=name page=null}"{else}class="nonselected" href="{url sort=name_r page=null}"{/if}>названию</a>
        	<a {if $sort=='ranked_r'}class="selectedd" href="{url sort=ranked page=null}"{else}class="nonselected" href="{url sort=ranked_r page=null}"{/if}>популярности</a>
        </div>
    {/if}
{/if}

<!--Подкатегории-->
{if $cat_child}
<ul class="cat_child">
   {foreach $cat_child as $ch}
   {if !$ch->metka}{if $ch->visible}
   <li>
   <a href="/catalog/{$ch->url}">
   <span class="c_img"><img src="{if $ch->image}{$config->categories_images_dir}{$ch->image}{else}data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC{/if}" alt="{$ch->name|escape}"></span><span class="c_text">{$ch->name|escape}</span>
   </a>
   </li>
   {/if}{/if}
   {/foreach}
 </ul>
{/if}

<!--Каталог товаров-->

{if $products}
<div id="prods">

<ul id="product_list" class="tiny_products">
	{foreach $products as $product}
		{include file='product_block.tpl'}
	{/foreach}	
</ul>

{include file='pagination.tpl'}
{else}Товары не найдены{/if}
</div>


{if $current_page_num==1}
<div style="float:left;">
{* Описание страницы (если задана) *}
{$page->body}


{* НОВОЕ Описание категории *}
{if $category->description!='' OR $brand->description_in_categories[$category->id]!='' OR $brand->description!=''}
<div id="category_description">
   {if $brand AND $category AND $brand->description_in_categories[$category->id]!=''}{$brand->description_in_categories[$category->id]}
   {elseif $brand AND $brand->description!=''}{$brand->description}
   {elseif $category->description!=''}{$category->description}{/if}
</div>
{/if}

{* Фильтр по брендам *}
{if $category->brands}
<div id="brands">
	<a href="catalog/{$category->url}" {if !$brand->id}class="selected"{/if}>Все бренды</a>
	{foreach name=brands item=b from=$category->brands}
		{if $b->image}
		<!--a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></a-->
		{/if}
		<a data-brand="{$b->id}" href="catalog/{$category->url}/{$b->url}" {if $b->id == $brand->id}class="selected"{/if}>{$b->name|escape}</a>
		
	{/foreach}
</div>
{/if}


</div>
{/if}

{$test = 'products_page' scope='parent'}



</div>

{* Описание категории в футере*}
{if $category->description_footer != ''}
<div id="category_description_foot">
	<div>
		<h2>{$category->name}{if $brand} {$brand->name}{/if}</h2>
		<div>{$category->description_footer}</div>{* Тело страницы *}
	</div>      
</div>
{/if}