{* Информера корзины (отдаётся аяксом) *}

{if $cart->total_products>0}
	<a href="./cart/"><i class="icon icon_cart"></i>В корзине <br />{$cart->total_products} {$cart->total_products|plural:'товар':'товаров':'товара'}</a>
{else}
	<i class="icon icon_cart"></i>В корзине <br />нет товаров
{/if}
