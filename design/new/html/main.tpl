{$wrapper = 'index.tpl' scope=parent}
{$canonical="" scope=parent}
{include file='slider.tpl'}

<ul class="smBanner">
	<li class="club">
		<a href="{$config->root_url}/user/register"><img alt="Регистрация на сайте" title="Регистрация на сайте" src="files/regclub.png" /></a>
	</li>
	<li class="dost">
		<a href="{$config->root_url}/dostavka"><img alt="Доставка товаров" title="Доставка товаров" src="files/dost.png" /></a>
	</li>
	<li class="cat">
		<a href="{$config->root_url}/catalog/velosipedy"><img alt="Каталог велосипедов" title="Каталог велосипедов" src="files/cat.png" /></a>
	</li>
</ul>

{* Рекомендуемые товары *}
{get_featured_products var=featured_products limit=20}
{if $featured_products}
<div class="reks">
<h2>Рекомендуемые товары</h2>
<i class="icon control-products-prev"></i>
<ul class="tiny_products">
	{foreach $featured_products as $product}
		{include file='product_block.tpl'}
	{/foreach}
</ul>
<i class="icon control-products-next"></i>
</div>
{/if}

{* Рекомендуемые товары *}
{get_pop_products var=popular_products limit=60}
{if $popular_products}
<div class="reks">
<h2>Популярные товары</h2>
<i class="icon control-products-prev1"></i>
<ul class="tiny_products2">
	{foreach $popular_products as $product}
		{include file='product_block.tpl'}
	{/foreach}
</ul>
<i class="icon control-products-next1"></i>
</div>
{/if}

<div class="news">
	<h2>Новости и акции</h2>
	{get_posts var=last_posts limit=5}
	{if $last_posts}
	<ul>
		{foreach $last_posts as $post}
			<li data-post="{$post->id}">{$post->date|date} <a href="blog/{$post->url}">{$post->name|escape}</a></li>
		{/foreach}
	</ul>
	{/if}
</ul>
<a href="blog">Еще новости и акции</a>
</div>

<div class="brands">
	<h2>Бренды</h2>
	{get_brands var=all_brands}
	{if $all_brands}
	<i class="icon control-barnd-prev"></i>
	<div style="float:left"><ul class="all_brands">
		{foreach $all_brands as $b}	
			<li>
			{if $b->image}
				<a href="brands/{$b->url}"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></a>
			{else}
				<a href="brands/{$b->url}">{$b->name}</a>
			{/if}
			</li>
		{/foreach}
	</ul>
	</div>
	<i class="icon control-barnd-next"></i>
	<div class="all"><a href="{$config->root_url}/all_brands">Все бренды</a></div>
	{/if}
{literal}<script>
var sliders = $('.all_brands').bxSlider({
	minSlides: 4,
	maxSlides: 4,
	slideWidth: 160,
	pager: false,
	controls: false
});

$('.control-barnd-prev').click(function(){
  sliders.goToPrevSlide();
  return false;
});

$('.control-barnd-next').click(function(){
  sliders.goToNextSlide();
  return false;
});


$('a.quick_viev').live('click', function(){
    var pro_id = this.getAttribute('rel');

    $.arcticmodal({
    type: 'ajax',
    url: 'ajax/quick.php',
    ajax: {
        type: 'GET',
        cache: false,
        data: {id: pro_id},
        dataType: 'json',
        success: function(data, el, response) {
          
           data.body.html(response);
        }
    }
	});
});

var sliders2 = $('.tiny_products').bxSlider({
	minSlides: 4,
	maxSlides: 4,
	moveSlides: 1,
	slideWidth: 225,
	pager: false,
	controls: false
});

$('.control-products-prev').click(function(){
  sliders2.goToPrevSlide();
  return false;
});

$('.control-products-next').click(function(){
  sliders2.goToNextSlide();
  $(window).trigger("scroll");
  return false;
});

var sliders3 = $('.tiny_products2').bxSlider({
	minSlides: 4,
	maxSlides: 4,
	moveSlides: 1,
	slideWidth: 225,
	pager: false,
	controls: false
});

$('.control-products-prev1').click(function(){
  sliders3.goToPrevSlide();
  return false;
});

$('.control-products-next1').click(function(){
  sliders3.goToNextSlide();
  $(window).trigger("scroll");
  return false;
});

</script>{/literal}
</div>

<div class="desc"><h1>{$page->header}</h1>{$page->body}</div>