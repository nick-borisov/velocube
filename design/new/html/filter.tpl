

<script type="text/javascript" src="design/{$settings->theme}/js/jquery.ui-slider.js"></script>
<script type="text/javascript" src="design/{$settings->theme}/js/filter.min.js"></script>

<noindex>
<form method="get" id="form_filter" action="catalog/{$category->url}{if $mark}/tag/{$mark->url}{/if}">



<div class="res">
		<a href="catalog/{$category->url}{if $mark}/tag/{$mark->url}{/if}">Сбросить все параметры</a>
	</div>
	<div class="price_filter">
		<h3>Цена:</h3>
		<input type="hidden" name="mark_id" value="{$mark->id}">
		<input type="hidden" name="cat_id" value="{$category->id}">
		<input type="hidden" name="sort" value="{$sort}">
		<input type="hidden" name="keyword" value="{$keyword}">
		<input type="hidden" name="page" value="1">
		<div class="price_slider">
			<div class="input_price">
				<label for="min_price">от</label>
				<input type="text" class="keypress" id="min_price" data-min-price="{if $smarty.get.min_price}{$smarty.get.min_price}{else}{$max_min_price->min_price|convert:null:false|floor}{/if}" name="min_price" value="{if $smarty.get.min_price}{$smarty.get.min_price}{else}{$max_min_price->min_price|convert:null:false|floor}{/if}" autocomplete="off">
				<label for="max_price">до</label>
				<input type="text" class="keypress" id="max_price" data-max-price="{if $smarty.get.max_price}{$smarty.get.max_price}{else}{$max_min_price->max_price|convert:null:false|ceil}{/if}" name="max_price" value="{if $smarty.get.max_price}{$smarty.get.max_price}{else}{$max_min_price->max_price|convert:null:false|ceil}{/if}" autocomplete="off">
				{$currency->sign|escape}{*<input id="price_submit" style="display: none;" type="submit" value="ОК">*}
            </div>
			<div id="slider_price" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content" data-slider-min-range="{$slider_max_min_price->min_price|convert:null:false|floor}" data-slider-max-range="{$slider_max_min_price->max_price|convert:null:false|ceil}">
			
				<div class="ui-slider-range ui-widget-header ui-widget-header-bar"></div>
				<div class="ui-slider-range ui-widget-header ui-widget-header-left ui-widget-header-hidden"></div>
				<div class="ui-slider-range ui-widget-header ui-widget-header-right ui-widget-header-hidden"></div>
			
			
                <div class="ui-slider-range ui-widget-header"></div>
                <a class="ui-slider-handle ui-state-default ui-state-left" href="#"></a>
                <a class="ui-slider-handle ui-state-default ui-state-right" href="#"></a>
            </div>
		</div>
	</div>
	<ul>
		<li>
			<input id="in_stock" type="checkbox" name="in_stock" value="1"{if $in_stock->checked} checked{/if}>
			<label for="in_stock"><span>Только в наличии</span> <i>{if !$in_stock->checked}({$in_stock->count}){/if}</i></label>
		</li>
		<li{if $discounted->disabled} class="disabled"{/if}>
			<input id="discounted" type="checkbox" name="discounted" value="1"{if $discounted->checked} checked{elseif $discounted->disabled} disabled{/if}>
			<label for="discounted"><span>Товары со скидкой</span> <i>{if !$discounted->checked}({$discounted->count}){/if}</i></label>
		</li>
		<li{if $featured->disabled} class="disabled"{/if}>
			<input id="featured" type="checkbox" name="featured" value="1"{if $featured->checked} checked{elseif $featured->disabled} disabled{/if}>
			<label for="featured"><span>Хит продаж</span> <i>{if !$featured->checked}({$featured->count}){/if}</i></label>
		</li>
	</ul>
</noindex>
	{if $brands}
	<ul>
		<h3>Бренды: <a id="lnk_1" onclick="show_hide(1)">все</a></h3>
		{foreach $brands as $b}
		{if !$b->in_filter}
		{if $b@index == 6}<div id="hidden_1" style="display: none;">{/if}
		<li{if $b->disabled} class="disabled"{/if}>
			<input id="brand_{$b->id}" type="checkbox" name="brand_id[]" value="{$b->id}"{if $b->checked} checked{elseif $b->disabled} disabled{/if}>
			<label for="brand_{$b->id}"><span>{$b->name|escape}</span> <i>{if !$b->checked}({$b->count}){/if}</i></label>
		</li>
		
		{/if}
		{/foreach}
		{if $b@index == 6}</div>{/if}
	</ul>
	{/if}
<noindex>	
	{if $features}
		{foreach $features as $f}
		<ul>
		<h3 onclick="setTable('{$f->id}');return true"><i id="i{$f->id}" class="arr off"></i><span>{$f->name}<span></h3>
		<div id="{$f->id}" style="display:none; margin-top: 10px;">
			{foreach $f->options as $k=>$o}
			<li{if $o->disabled} class="disabled"{/if}>
				<input id="option_{$f->id}_{$k}" type="checkbox" name="{$f->id}[]" value="{$o->value|escape}"{if $o->checked} checked{elseif $o->disabled} disabled{/if}>
				<label for="option_{$f->id}_{$k}"><span>{$o->value|escape}</span> <i>{if !$o->checked}({$o->count}){/if}</i></label>
			</li>
			{/foreach}
		</div>
		</ul>
		{/foreach}
	{/if}
	<span class="count_products_search"></span>
	<ul style="text-align:center;">
	<input class="button" type="submit" value="Показать">
	</ul>
	<div class="res2">
		<a href="catalog/{$category->url}{if $mark}/tag/{$mark->url}{/if}">Сбросить все параметры</a>
	</div>
</form>

           

</noindex>

{literal}<script type="text/javascript">
function show_hide (id) {
	if (document.getElementById("hidden_" + id).style.display == "none") {
		document.getElementById("hidden_" + id).style.display = "block";
		document.getElementById("lnk_" + id).innerHTML = "популярные";
	} else {
		document.getElementById("hidden_" + id).style.display = "none";
		document.getElementById("lnk_" + id).innerHTML = "все";
		document.getElementById("hiddens_" + id).style.display = "none";
	}
}

function show_hides (id) {
	if (document.getElementById.style.display == "none") {
		document.getElementById("hiddens_" + id).style.display = "block";
	} else {
		document.getElementById("hiddens_" + id).style.display = "none";
	}
}


function setTable(what){
if(document.getElementById(what).style.display=="none"){
document.getElementById(what).style.display="block";
document.getElementById("i" + what).className = "arr";
}
else if(document.getElementById(what).style.display=="block"){
document.getElementById(what).style.display="none";
document.getElementById("i" + what).className = "arr off";

}
} 
</script>{/literal}