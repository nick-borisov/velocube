<footer>
	<div class="foot-light">
		<div class="container">
			<div class="row">
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span>Webmarket</span></h3>
					</div>
					
					<p>Этот магазин является демонстрацией скрипта интернет-магазина <a href="http://simplacms.ru">Simpla</a>. Все материалы на этом сайте присутствуют исключительно в демострационных целях.</p>
				</div>
				
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span>Контакты</span></h3>
					</div>

					<p><i class="icon-skype"></i> 02.8780.09999 </p>
					<p><i class="icon-phone"></i> 02.8780.09999 </p>
					<p><i class="icon-envelope"></i> contact@kulerthemes.com </p>
					<p><i class="icon-glass"></i> 02.8780.09999 </p>
					<p><i class="icon-glass"></i> 02.8780.09999 </p>
				</div>
				
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span>Facebook</span></h3>
					</div>

					<div class="fill-iframe">
						<div class="fb-like-box" data-href="https://www.facebook.com/ProteusNet" data-width="270" data-height="200" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
					</div>
				</div>
				
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span class="light">Twitter</span></h3>
					</div>
					
					<a class="twitter-timeline"  href="https://twitter.com/primozcigler"  data-widget-id="361435057526800385">Tweets by @primozcigler</a>
					{literal}
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					{/literal}
				</div>
			</div>
		</div>
	</div>

	<div class="foot-dark">
		<div class="container">
			<div class="row">
				<!-- Menu 1 -->
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span class="light">Webmarket</span></h3>
					</div>
					<ul class="nav">
						{foreach $pages as $p}
							{if $p->menu_id == 1}
								<li><a data-page="{$p->id}" href="{$p->url}">{$p->name|escape}</a></li>
							{/if}
						{/foreach}
					</ul>
				</div>

				<!-- Menu 2 -->
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span class="light">Акции и скидки</span></h3>
					</div>
					<ul class="nav">
						<li><a href="#">Акции</a></li>
						<li><a href="#">Товары дня</a></li>
						<li><a href="#">Новинки</a></li>
						<li><a href="#">Уцененные товары</a></li>
						<li><a href="#">Скидки</a></li>
					</ul>
				</div>

				<!-- Menu 3 -->
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span class="light">Сервисы</span></h3>
					</div>
					
					<ul class="nav">
						<li><a href="#">Помощь</a></li>
						<li><a href="#">Новости</a></li>
						<li><a href="#">Обзоры</a></li>
						<li><a href="#">Пресс-центр</a></li>
						<li><a href="#">Сотрудничество</a></li>
						<li><a href="#">Реклама на сайте</a></li>
					</ul>
				</div>

				<!-- Menu 4 -->
				<div class="span3">
					<div class="push-down-20">
						<h3 class="skew_title"><span class="light">Мы в социальных сетях</span></h3>
					</div>
					<ul class="nav">
						<li><a href="#">foursquare</a></li>
						<li><a href="#">Google+</a></li>
						<li><a href="#">Twitter</a></li>
						<li><a href="#">Facebook</a></li>
						<li><a href="#">ВКонтакте</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="foot-last">
		<div class="container">
			<div class="row">
				<div class="span6">
					&copy; Copyright 2014. Адаптация для Simpla CMS — <a target="_blank" href="http://chocolatemol.es/">chocolate_moles</a>.
				</div>
				<div class="span6">
					<div class="pull-right">Webmarket HTML Шаблон от <a target="_blank" href="http://www.proteusthemes.com">ProteusThemes</a></div>
				</div>
			</div>
		</div>
	</div>
</footer>