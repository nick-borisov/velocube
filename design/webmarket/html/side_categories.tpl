{function name=side_categories_tree level=0}
{if $categories}
	<ul>
		{foreach $categories as $c}
			{if $c->visible}
				<li>
					<a {if $category->id == $c->id}class="active"{/if} href="catalog/{$c->url}" data-category="{$c->id}">
						{if $level!=0}
							<span class="icon-chevron-right"></span>
						{/if}
						
						{$c->name}
					</a>
					
					{if in_array($category->id, $c->children)}
					{side_categories_tree categories=$c->subcategories level=$level+1}
					{/if}
				</li>
			{/if}
		{/foreach}
	</ul>
{/if}
{/function}

<div class="sidebar-item">
	<div class="underlined">
		<h3><span class="light">Каталог товаров</span></h3>
	</div>
	
	<div class="side-menu">
		{side_categories_tree categories=$categories}
	</div>
</div>