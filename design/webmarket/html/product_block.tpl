<div class="product">
	<div class="title-box">
		<h4 class="light">
			<a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a>
		</h4>
		
		<div class="rating">
			<span class="rating-starsOff">
				<span class="rating-starsOn" style="width:{$product->rating*85/5|string_format:"%.0f"}px"></span>
			</span>
		</div>
	</div>
	
	<div class="picture">
		<a href="products/{$product->url}">
			{if $product->image}
			<img src="{$product->image->filename|resize:200:200}" alt="{$product->name|escape}"/>
			{else}
			<img src="design/{$settings->theme}/images/no_image.png" alt="{$product->name|escape}"/>
			{/if}
		</a>
		
		<div class="img-overlay">
			<div class="compare_box">
				{if $smarty.session.compared_products && in_array($product->url, $smarty.session.compared_products)}
					<a rel="nofollow" class="skew_btn" href='compare/'><i class="icon-signal"></i> В сравнении</a>
				{else}
					<a rel="nofollow" class="skew_btn compare" href='compare/{$product->url}'><i class="icon-signal"></i> Сравнить</a>		
				{/if}
			</div>
			
			<div class="wishlist_box">
				{if $wishlist}
					<a rel="nofollow" class="skew_btn" href='wishlist/remove/{$product->url}'><i class="icon-star"></i> Удалить</a>
				{elseif $wishlist_products && in_array($product->url, $wishlist_products)}
					<a rel="nofollow" class="skew_btn" href='wishlist'><i class="icon-star"></i> В избранном</a>
				{else}
					<a rel="nofollow" class="skew_btn wishlist" href='wishlist/{$product->url}'><i class="icon-star"></i> В избранное</a>
				{/if}
			</div>
		</div>
	</div>
	
	<form class="variants main-titles price-box" action="/cart">
		<h4 class="title">
			{if $product->variants|count > 0}
			<div class="skew_title">
				<span>
					<span class="striked red-clr">{if $product->variant->compare_price > 0}{$product->variant->compare_price|convert}{/if}</span> 

					<span class="v_price white-clr">{$product->variant->price|convert}</span> {$currency->sign|escape}
				</span>
			</div>
			{else}
			<span class="v_price out_of_stock">Нет в наличии</span>
			{/if}
		</h4>
		
		{if $product->variants|count > 0}
			<input id="variants_{$product->variant->id}" name="variant" value="{$product->variant->id}" type="radio" checked style="display:none;"/>

			<button class="buy_btn"><i class="icon-shopping-cart"></i> В корзину</button>
		{/if}
	</form>
	
	{if $product->variant->price>=15000}
		<div class="credit_box">
			В кредит <a class="credit_btn" href="#" data-price="{($product->variant->price/10)|convert}" data-url="cart?variant={$product->variant->id}&credit=1">{($product->variant->price/10)|convert} {$currency->sign|escape}</a>
		</div>
	{/if}
</div>