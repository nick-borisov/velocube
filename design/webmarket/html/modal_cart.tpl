{if $cart->total_products>0}
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="cartModalLabel"><span class="light">Корзина</span></h3>
</div>

<div class="modal-body">
	<table class="table table-items">
		{foreach from=$cart->purchases item=purchase}
		<tr>
			<td class="image">
				{$image = $purchase->product->images|first}
				{if $image}
					<a href="products/{$purchase->product->url}"><img alt="Spicylicious" src="{$image->filename|resize:40:40}"/></a>
				{/if}
			</td>
			
			<td class="name">
				<a href="products/{$purchase->product->url}">{$purchase->product->name|escape}</a>
				<div>{$purchase->variant->name|escape}</div>
			</td>
			
			<td class="qty">
				<select name="amounts[{$purchase->variant->id}]" onchange="update_cart('{$purchase->variant->id}',$(this).val());" class="amount" id="{$purchase->variant->id}">
				{section name=amounts start=1 loop=$purchase->variant->stock+1 step=1}
				<option variant_id='{$purchase->variant->id}' value="{$smarty.section.amounts.index}" {if $purchase->amount==$smarty.section.amounts.index}selected{/if}>{$smarty.section.amounts.index} {$settings->units}</option>
				{/section}
				</select>
			</td>
			
			<td class="price" id="total_cost_modal_{$purchase->variant->id}">{($purchase->variant->price*$purchase->amount)|convert}&nbsp;{$currency->sign}</td>
			
			<td class="delete">
				<a href="#" class="icon-remove-sign" onclick="remove_item_cart('{$purchase->variant->id}'); return false" title="Удалить"></a>
			</td>
		</tr>
		{/foreach}

		{if $cart->discount}
		<tr>
			<td class="image">&nbsp;</td>
			<td class="name">&nbsp;</td>
			<td class="qty">Скидка</td>
			<td class="price">{$cart->discount}&nbsp;%</td>
			<td class="delete">&nbsp;</td>
		</tr>
		{/if}
		<tr>
			<td class="image">&nbsp;</td>
			<td class="name">&nbsp;</td>
			<td class="qty">Итого</td>
			<td class="price" id="cart_total_modal">{$cart->total_price|convert}&nbsp;{$currency->sign}</td>
			<td class="delete">&nbsp;</td>
		</tr>
	</table>

	<div class="align-center">
		<a class="skew-btn tny" href="#" data-dismiss="modal" aria-hidden="true">Продолжить покупки</a>
		<a class="skew-btn tny" href="/cart"><span>Оформить заказ</span></a>
	</div>
</div>
{else}

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<h3 id="cartModalLabel"><span class="light">Ваша корзина пуста</span></h3>
</div>
<div class="modal-body align-center">
	
		<a class="skew-btn tny button" href="#" data-dismiss="modal" aria-hidden="true">Продолжить покупки</a>
	
</div>
{/if}


