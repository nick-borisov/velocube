{* Страница товара *}

{* Канонический адрес страницы *}
{$canonical="/products/{$product->url}" scope=parent}




<div class="container ">
	<div class="padding-top-30 blocks-spacer">
		<div class="row">
			<aside class="span3">
				{include file='side_categories.tpl'}
			</aside>
			
			<section class="span9">
				<div class="white-box blocks-spacer">
					<ul class="breadcrumb">
						<li><a href="./">Главная</a></li>
						{foreach from=$category->path item=cat}
							<li><span class="icon-chevron-right"></span></li>
							<li><a href="catalog/{$cat->url}">{$cat->name|escape}</a></li>
						{/foreach}
						{if $brand}
							<li><span class="icon-chevron-right"></span></li>
							<li><a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a></li>
						{/if}
						<li><span class="icon-chevron-right"></span></li>
						<li>{$product->name|escape}</li>
					</ul>

					<div class="product-preview padding-box">
						{if $product->image}
						<div class="picture">
							<img src="{$product->image->filename|resize:800:400}" alt="{$product->product->name|escape}" id="mainPreviewImg" />
						</div>
						{/if}
		
						{if $product->images|count>1}
						<div class="thumbs clearfix">
							{foreach $product->images as $i=>$image}
							<div class="thumb{if $image@first} active{/if}">
								<a href="#mainPreviewImg"><img src="{$image->filename|resize:800:400}" alt="{$product->name|escape}" /></a>
							</div>
							{/foreach}
						</div>
						{/if}
					</div>

					<div class="product-title ">
						<h1 class="name" data-product="{$product->id}">{$product->name|escape}</h1>
						
						<div class="testRater_{$product->id} clr" id="rating_{$product->id}" class="stat">
							<span class="rater-starsOff">
								<span class="rater-starsOn" style="width:{$product->rating*85/5|string_format:"%.0f"}px"></span>
							</span>
							
							<span class="rater-rating">{$product->rating|string_format:"%.1f"}</span>&#160;(голосов <span class="rater-rateCount">{$product->votes|string_format:"%.0f"}</span>)
						</div>

						<hr />
						
						<div class="meta">
							<span class="tag">
								<span class="striked red-clr">{if $product->variant->compare_price > 0}{$product->variant->compare_price|convert}{/if}</span> 
								
								{if $product->variants|count > 0}
								<span class="v_price">{$product->variant->price|convert}</span> {$currency->sign|escape}
								{else}
								<span class="v_price out_of_stock">Нет в наличии</span>
								{/if}
							</span>
						</div>
					</div>
					
					<div class="product-description padding-box">
						{$product->annotation}

						{if $product->variant->price>=15000}
						<p>Первый взнос при покупке в кредит: <a class="cred" href="cart?variant={$product->variant->id}&credit=1" >{($product->variant->price/10)|convert} {$currency->sign|escape}</a></p>
						{/if}
						
						<hr />

						{if $product->variants|count > 0}
						<form action="/cart" class="form form-inline clearfix variants">
							<div class="numbered">
								<input type="text" name="amount" value="1" maxlength="2" class="tiny-size" />
								<span class="clickable add-one icon-plus-sign-alt"></span>
								<span class="clickable remove-one icon-minus-sign-alt"></span>
							</div>
							&nbsp;
							<select name="variant" class="span3{if $product->variants|count==1 || !$product->variant->name} hidden{/if}">
								{foreach $product->variants as $v}
								<option value="{$v->id}" {if $v->compare_price > 0}data-compare_price="{$v->compare_price|convert}"{/if} data-price="{$v->price|convert}">{$v->name}</option>
								{/foreach}
							</select>
							
							<button class="skew-btn pull-right"><i class="icon-shopping-cart"></i> &nbsp; Добавить в корзину</button>
						</form>
						
						<hr />
						{/if}

						
			
						<div class="share-item push-down-20">
							<div class="row-fluid">
								<div class="span6 title">
									{if $smarty.session.compared_products && in_array($product->url, $smarty.session.compared_products)}
										<a rel="nofollow" href='compare/'><i class="icon-signal"></i> В сравнении</a>
									{else}
										<a rel="nofollow" class="compare" href='compare/{$product->url}'><i class="icon-signal"></i> Сравнить</a>		
									{/if}
									&nbsp;|&nbsp;
									{if $wishlist}
										<a rel="nofollow" href='wishlist/remove/{$product->url}'><i class="icon-star"></i> Удалить</a>
									{elseif $wishlist_products && in_array($product->url, $wishlist_products)}
										<a rel="nofollow" href='wishlist'><i class="icon-star"></i> В избранном</a>
									{else}
										<a rel="nofollow" class="wishlist" href='wishlist/{$product->url}'><i class="icon-star"></i> В избранное</a>
									{/if}
								</div>
								
								<div class="span6 title">
									Поделитесь с друзьями:
									
										<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
										<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj"></div>
									
								</div>
							</div>
						</div>

						<ul id="myTab" class="nav nav-tabs">
							{if $product->body}
							<li{if !$error} class="active"{/if}><a href="#tab-1" data-toggle="tab">Описание</a></li>
							{/if}
							
							{if $product->features}
							<li{if !$product->body && !$error} class="active"{/if}><a href="#tab-2" data-toggle="tab">Характеристики</a></li>
							{/if}
							
							<li{if (!$product->body && !$product->features) || $error} class="active"{/if}><a href="#tab-3" data-toggle="tab">Комментарии</a></li>
						</ul>
							
						<div class="tab-content push-down-10">
							{if $product->body}
							<div class="fade tab-pane{if !$error} in active{/if}" id="tab-1">
								{$product->body}
							</div>
							{/if}
							
							{if $product->features}
							<div class="fade tab-pane{if !$product->body && !$error} in active{/if}" id="tab-2">
								<table class="table table-striped table-bordered">
									{foreach $product->features as $f}
									<tr>
										<td>{$f->name}</td>
										<td>{$f->value}</td>
									</tr>
									{/foreach}
							   </table>
							</div>
							{/if}
							
							<div class="fade tab-pane{if (!$product->body && !$product->features) || $error} in active{/if}" id="tab-3">
								<section id="comments" class="comments-container">
									{if $comments}
										{foreach $comments as $comment}
										<div class="single-comment clearfix">
											<div class="avatar-container">
												<img src="design/{$settings->theme}/images/avatar_dummy.gif" alt="avatar" class="avatar"  />
											</div>
											<div class="comment-content">
												<div class="comment-inner">
													<cite class="author-name">
														<span class="light">{$comment->name|escape}</span>{if !$comment->approved}, ожидает модерации{/if}
													</cite>
													<div class="metadata">
														{$comment->date|date}, {$comment->date|time}
													</div>
													<div class="comment-text">
														{$comment->text|escape|nl2br}
													</div>
												</div>
											</div>
										</div>
										{/foreach}
									{/if}
									
									<h3 class="push-down-25"><span class="light">Написать</span> комментарий</h3>

									<form id="commentform" method="post" class="form form-inline form-comments">
										{if $error}
											<div class="alert alert-danger in fade">
												<button type="button" class="close" data-dismiss="alert">&times;</button>
												{if $error=='captcha'}
												Неверно введена капча
												{elseif $error=='empty_name'}
												Введите имя
												{elseif $error=='empty_comment'}
												Введите комментарий
												{/if}
											</div>
											
											<script>
												$(document).ready(function (){
													scroll2error();
												})
											</script>
										{/if}

										<p class="push-down-20">
											<input type="text" aria-required="true" tabindex="1" size="30" id="comment_name" name="name" value="{$comment_name}" required/>
											<label for="comment_name">Имя<span class="red-clr bold">*</span></label>
										</p>

										<p class="push-down-20" style="position: relative">
											<img src="captcha/image.php?{math equation='rand(10,10000)'}" alt='captcha' style="position: absolute; max-height: 33px; top: 2px; left: 161px; border-radius: 3px"/>
											<input type="text" aria-required="true" tabindex="2" size="30" id="comment_captcha" name="captcha_code" value="" required/>
											<label for="captcha_code">Число<span class="red-clr bold">*</span></label>
										</p>

										<p class="push-down-20">
											<textarea class="input-block-level" tabindex="3" rows="7" cols="70" id="comment" name="text" placeholder="Ваш коментарий ..." required>{$comment_text}</textarea>
										</p>

										<p>
											<button class="skew-btn" tabindex="4" name="comment" id="submit" >Отправить</button>
										</p>
									</form>
								 </section>
							</div>
						</div>
					</div>
				</div>
	
				{if $related_products}
				<div class="most-popular blocks-spacer">
					<div class="row">
						<div class="span9">
							<div class="main-titles double-lined align-center">
								<h2 class="skew_title"><span class="light">Так же советуем посмотреть</span></h2>
							</div>
						</div>
					</div>

					<div class="row popup-products blocks-spacer">
						{foreach $related_products as $product}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
						{if $product@iteration is div by 3}<div class="clearfix"></div>{/if}
						{/foreach}
					</div>
				</div>
				{/if}	

				{if $brand}
				{get_products var=brand_products brand_id=$brand->id no_id=$no_id limit=3 sort=random}
				{if $brand_products}
				<div class="most-popular">
					<div class="row">
						<div class="span9">
							<div class="main-titles double-lined align-center">
								<h2 class="skew_title"><span class="light">Товары того же бренда (случайные)</span></h2>
							</div>
						</div>
					</div>

					<div class="row popup-products blocks-spacer">
						{foreach $brand_products as $product}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
						{/foreach}
					</div>
				</div>
				{/if}
				{/if}

				{get_products var=cat_products category_id=$category->id  no_id=$no_id limit=3}
				{if $cat_products}
				<div class="most-popular">
					<div class="row">
						<div class="span9">
							<div class="main-titles double-lined align-center">
								<h2 class="skew_title"><span class="light">Товары той же категории (первые)</span></h2>
							</div>
						</div>
					</div>

					<div class="row popup-products blocks-spacer">
						{foreach $cat_products as $product}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
						{/foreach}
					</div>
				</div>
				{/if}	
			</section>
		</div>
	</div>
</div><!-- container -->