{* Шаблон текстовой страницы *}

{* Канонический адрес страницы *}
{$canonical="/{$page->url}" scope=parent}

<div class="container">
	<div class="padding-top-30 blocks-spacer">
		<div class="row">
			<div class="span12 white-box">
				<ul class="breadcrumb">
					<li>
						<a href="./">Главная</a>
					</li>
					<li><span class="icon-chevron-right"></span></li>
					<li>
						{$page->header|escape}
					</li>
				</ul>
				
				<div class="padding-box">
					<h1 class="main_heading" data-page="{$page->id}">
						{$page->header}
					</h1>
					
					{$page->body}
				</div>
			</div>
		</div>
	</div>
</div>