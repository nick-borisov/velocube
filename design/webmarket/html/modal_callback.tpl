<div id="callbackModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="callbackModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="callbackModalLabel"><span class="light">Обратный звонок</span></h3>
	</div>
	
	<div class="modal-body">
		<form method="post" id="callbackForm">
			<div class="control-group">
				<label class="control-label hidden shown-ie8" for="inputName">Имя</label>
				<div class="controls">
					<input type="text" name="name" class="input-block-level" id="inputName" placeholder="Имя" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label hidden shown-ie8" for="inputPassword">Телефон</label>
				<div class="controls">
					<input type="text" name="phone" class="input-block-level" id="inputPhone" placeholder="Телефон" required>
				</div>
			</div>
			<button type="submit" class="btn btn-primary input-block-level bold higher push-down-20">
				Перезвоните мне
			</button>
			<div id="callbackResult"></div>
		</form>
	</div>
</div>