<!DOCTYPE html>
<!--[if lt IE 8]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
	<base href="{$config->root_url}/" />
	<title>{$meta_title|escape}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$meta_description|escape}">
	<meta name="keywords"    content="{$meta_keywords|escape}">
	
	{* Канонический адрес страницы *}
	{if isset($canonical)}<link rel="canonical" href="{$config->root_url}{$canonical}"/>{/if}
	
	<!--  = Google Fonts =  -->
    <script type="text/javascript">
        WebFontConfig = {
            google : {
                families : ['Roboto+Condensed:300,400,700:latin,latin-ext,cyrillic', 'PT Serif:400,700:latin,cyrillic']
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
	
	<!-- Twitter Bootstrap -->
    <link href="design/{$settings->theme}/css/bootstrap.css" rel="stylesheet">
    <link href="design/{$settings->theme}/css/responsive.css" rel="stylesheet">
    <!-- Slider Revolution -->
    <link rel="stylesheet" href="design/{$settings->theme}/js/rs-plugin/css/settings.css" type="text/css"/>
    <!-- jQuery UI -->
	{if $module != 'CartView'}
    <link rel="stylesheet" href="design/{$settings->theme}/js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
	{/if}
    <!-- PrettyPhoto -->
    <link rel="stylesheet" href="design/{$settings->theme}/js/prettyphoto/css/prettyPhoto.css" type="text/css"/>
    <!-- main styles -->
    <link href="design/{$settings->theme}/css/style.css" rel="stylesheet">
	
	<!-- Custom scrollbars CSS -->
	<link href="design/{$settings->theme}/js/scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" />
	
	{* Раскоментируйте одну из тем если нужно *}
    <!-- <link href="design/{$settings->theme}/css/theme/grass-green.css" rel="stylesheet"> -->
    <!-- <link href="design/{$settings->theme}/css/theme/gray.css" rel="stylesheet"> -->
    <!-- <link href="design/{$settings->theme}/css/theme/oil-green.css" rel="stylesheet"> -->

    <!-- Modernizr -->
    <script src="design/{$settings->theme}/js/modernizr.custom.56918.js"></script>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="design/{$settings->theme}/images/apple-touch/144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="design/{$settings->theme}/images/apple-touch/114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="design/{$settings->theme}/images/apple-touch/72.png">
    <link rel="apple-touch-icon-precomposed" href="design/{$settings->theme}/images/apple-touch/57.png">
    <link rel="shortcut icon" href="design/{$settings->theme}/images/apple-touch/57.png">
</head>

<body class="pattern-13">
	<div class="master-wrapper">
		<header id="header">
			<div class="darker-row">
				<div class="container">
					<div class="row">
						<div class="span7">
							<div class="topmost-line">
								{foreach $pages as $p}
									{if $p->menu_id == 1}
									{if !$p@first}&nbsp; | &nbsp;{/if}
										<a data-page="{$p->id}" href="{$p->url}" class="gray-link">{$p->name|escape}</a>
									{/if}
								{/foreach}
							</div>

							<div class="topmost-line push-down-15">
								{if $currencies|count>1}
								<div class="lang-currency">
									<div class="dropdown js-selectable-dropdown">
										<a data-toggle="dropdown" class="selected" href="#"><span class="js-change-text">{$currency->name}</span> <b class="caret"></b></a>
										<ul class="dropdown-menu js-possibilities" role="menu" aria-labelledby="dLabel">
											{foreach from=$currencies item=c}
												{if $c->enabled} 
													<li><a href="{url currency_id=$c->id}">{$c->name|escape}</a></li>
												{/if}
											{/foreach}
										</ul>
									</div>
								</div>
								{/if}
									
								<span id="compare_informer_box">
									<span id="compare_informer">
										<a href="/compare/"><i class="icon-signal"></i> Сравнение ({$compare_products})</a>
									</span>
								</span>
								&nbsp;
								<span id="wishlist_informer_box">
									<span id="wishlist_informer">
										<a href="wishlist"><i class="icon-star"></i> Избранное ({$wishlist_products|count})</a>
									</span>
								</span>
								&nbsp;
								{if $user}
									Добро пожаловать <a href="user">{$user->name}</a>{if $group->discount>0}, ваша скидка &mdash; {$group->discount}%{/if} &nbsp; | &nbsp; <a href="user/logout">выйти</a>
								{else}
									<a href="#registerModal" role="button" data-toggle="modal"><i class="icon-check"></i> Регистрация</a>  &nbsp;
									<a href="#loginModal" role="button" data-toggle="modal"><i class="icon-user"></i> Вход</a> &nbsp; или &nbsp;                     
								
									<div id="uLogin" data-ulogin="display=small;fields=first_name,last_name,email{*,phone,city*};providers=vkontakte,facebook,odnoklassniki,mailru;hidden=other;redirect_uri=http://velocube.ru/user/login"></div>
								{/if}
							</div>
						</div>

						<div class="span2">
							<div class="topmost-line align-center">
								<p class="phone">+7(499) 653-88-38</p>
								<p class="work_time">9:00 - 21:00</p>
								<a href="#callbackModal" role="button" data-toggle="modal">Заказать обратный звонок</a>
							</div>
						</div>
						<div class="span3">
							<div class="cart-container" id="cart_informer">
								{include file='cart_informer.tpl'}
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="navbar navbar-static-top{if $module == 'MainView'} transparent{/if}" id="stickyNavbar">
			<div class="navbar-inner">
				<div class="container">
					<div class="row">
						<div class="span2">
							<div class="logo">
								<a href="/"><img src="design/{$settings->theme}/images/logo.png" alt="{$settings->site_name|escape}" width="257" height="76" /></a>
							</div>
						</div>
						
						<div class="span7">
							<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							
							<div class="nav-collapse collapse">
								<ul class="nav" id="mainNavigation">
									<li class="dropdown dropdown-megamenu">
										<a href="#" class="dropdown-toggle"> Мегаменю <b class="caret"></b> </a>

										
										
										{* Рекурсивная функция вывода дерева категорий *}
										{function name=categories_tree}
										{if $categories}
										<ul class="dropdown-menu megamenu container col-2">
											<li class="row">
											{foreach $categories as $c}
												{if $c->visible}
												
												{if $c@iteration != 3 && $c@iteration != 5}
												<div class="span3">
												{/if}
													<ul class="nav unstyled header">
														<li>
															<a class="menu-category-title{if $category->id == $c->id} selected{/if}" href="catalog/{$c->url}" data-category="{$c->id}">{$c->name|escape}</a>
															{if $c->image && $c->subcategories}<img class="menu-category-image float-right" src="{$config->categories_images_dir}{$c->image}" alt="{$c->name|escape}">{/if}
														</li>
													</ul>
													
													{if $c->subcategories}
														<ul class="nav unstyled list">
														{foreach $c->subcategories as $c2}
															{if $c->visible}
															<li><a {if $category->id == $c2->id}class="selected"{/if} href="catalog/{$c2->url}" data-category="{$c2->id}">{$c2->name|escape}</a></li>
															{/if}
														{/foreach}
														</ul>
													{/if}
												
												{if $c@iteration != 2 && $c@iteration != 4}
												</div>
												{/if}
												{/if}
											{/foreach}
											</li>
										</ul>
										{/if}
										{/function}

										{categories_tree categories=$categories}
									</li>
									
									<li class="dropdown">
										<a href="#" class="dropdown-toggle"> Меню <b class="caret"></b> </a>
										<ul class="dropdown-menu custom">
										{foreach $pages as $p}
											{if $p->menu_id == 1}
												<li class="dropdown{if $category->id == $c->id} active{/if}">
													<a data-page="{$p->id}" href="{$p->url}" class="gray-link">{$p->name|escape}</a>
												</li>
											{/if}
										{/foreach}
										</ul>
									</li>
								</ul>

								
							</div>
						</div>

						<div class="span3">
							<form class="navbar-form pull-right" action="products" method="get">
								<button type="submit"><span class="icon-search"></span></button>
								<input type="text" class="span1" name="keyword" id="navSearchInput" value="{$keyword|escape}" placeholder="Поиск">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		{$content}

		
		
		{include file='footer.tpl'}
		{include file='modal_login.tpl'}
		{include file='modal_register.tpl'}
		{include file='modal_forgot_password.tpl'}
		{include file='modal_credit.tpl'}
		{include file='modal_callback.tpl'}

		{* Cart modal *}
		<div id="cartModal" class="modal modal-large hide fade" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" aria-hidden="true">
			{include file='modal_cart.tpl'}
		</div>

	</div>

	<!--  = jQuery - CDN with local fallback =  -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
    if (typeof jQuery == 'undefined') {
        document.write('<script src="design/{$settings->theme}/js/jquery.min.js"><\/script>');
    }
    </script>
	
    <!-- JavaScript -->
    <!--  = FB =  -->
	
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=126780447403102";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <!--  = _ =  -->
    <script src="design/{$settings->theme}/js/underscore/underscore-min.js" type="text/javascript"></script>

    <!--  = Bootstrap =  -->
    <script src="design/{$settings->theme}/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  = Slider Revolution =  -->
    <script src="design/{$settings->theme}/js/rs-plugin/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
    <script src="design/{$settings->theme}/js/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>

    <!--  = CarouFredSel =  -->
    <script src="design/{$settings->theme}/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>

    <!--  = jQuery UI =  -->
	{if $module != 'CartView'}
    <script src="design/{$settings->theme}/js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	{/if}
    <script src="design/{$settings->theme}/js/jquery-ui-1.10.3/touch-fix.min.js" type="text/javascript"></script>

    <!--  = Isotope =  -->
    <script src="design/{$settings->theme}/js/isotope/jquery.isotope.min.js" type="text/javascript"></script>

    <!--  = PrettyPhoto =  -->
    <script src="design/{$settings->theme}/js/prettyphoto/js/jquery.prettyPhoto.js" type="text/javascript"></script>

    <!--  = Google Maps API =  -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="design/{$settings->theme}/js/goMap/js/jquery.gomap-1.3.2.min.js"></script>

	<!-- infinity scroll -->
	<script src="design/{$settings->theme}/js/infinite_scroll/jquery.infinitescroll.js"></script>
	<script src="design/{$settings->theme}/js/infinite_scroll/jquery.infinitescroll.manual.js"></script>
	
    <!--  = Custom JS =  -->
    <script src="design/{$settings->theme}/js/custom.js" type="text/javascript"></script>
	
	<!-- Аяксовая корзина -->
	<script src="design/{$settings->theme}/js/ajax_cart.js"></script>
	<script src="design/{$settings->theme}/js/filter.min.js"></script>

	<!-- Custom scroll -->
	<script src="design/{$settings->theme}/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

	{* Всплывающие подсказки для администратора *}
	{if $smarty.session.admin == 'admin'}
	<!--<script src ="design/{$settings->theme}/js/admintooltip/admintooltip.js" type="text/javascript"></script>
	<link   href="design/{$settings->theme}/js/admintooltip/css/admintooltip.css" rel="stylesheet" type="text/css" />-->
	{/if}
	
	<!-- uLogin -->
	<script src="http://ulogin.ru/js/ulogin.js"></script>
		
	<script src="js/jquery.rater.js"></script>
    <script>
      $(function() { $('.testRater_{$product->id}').rater({ postHref: '/ajax/rating.php' }); });
    </script>
		
	{* Автозаполнитель поиска *}
	{literal}
	<script src="js/autocomplete/jquery.autocomplete-min.js" type="text/javascript"></script>
	<style>
	
	</style>	

	<script>
	$(function() {
		//  Автозаполнитель поиска
		$("#navSearchInput").autocomplete({
			serviceUrl:'ajax/search_products.php',
			minChars:1,
			onSelect:
				function(value, data){
					 $("#navSearchInput").closest('form').submit();
				},
			fnFormatResult:
				function(value, data, currentValue, count){
				
				return (data.image?"<div class='image'><img src='" + data.image + "'></div>":'') + 
						"<span class='title roboto'>" + value + "</span>" + 
						"<div class='rating'><span class='rater-starsOff'><span class='rater-starsOn' style='width:" + (data.rating * 17) + "px'></span></span></div>" + 
						"<div class='annotation'>" + data.annotation + "</div>" ;
				}
		});
	});
	</script>
	{/literal}
	
{if $module == 'CartView'}
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script src="http://yes-credit.su/crem/js/crem.js" type="text/javascript"></script>
    <link href="http://yes-credit.su/crem/css/blizter.css" rel="stylesheet" type="text/css" />
	
	<input type=radio name=payment_method_id value='12' id="payment" style='display:none;' />
	
    <script>
		
		function show_credit() {
			$( "#payment" ).attr('checked','checked');
			yescreditmodul([
				{foreach from=$cart->purchases item=purchase}
					{ MODEL: "{$purchase->product->name|escape} {$purchase->variant->name|escape}", COUNT:"{$purchase->amount}", PRICE:"{($purchase->variant->price*$purchase->amount)|convert}"},
				{/foreach}
			],367782,"ORD{$last_order}")
		}
		
		{if $smarty.get.credit == 1}
		show_credit();
		{/if}
    </script>
{/if}
</body>
</html>