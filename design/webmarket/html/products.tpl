{* Список товаров *}

{* Канонический адрес страницы *}
{if $category && $brand}
{$canonical="/catalog/{$category->url}/{$brand->url}" scope=parent}
{elseif $category}
{$canonical="/catalog/{$category->url}" scope=parent}
{elseif $brand}
{$canonical="/brands/{$brand->url}" scope=parent}
{elseif $keyword}
{$canonical="/products?keyword={$keyword|escape}" scope=parent}
{else}
{$canonical="/products" scope=parent}
{/if}

{if $features}
    {assign 'mt' $meta_title}
    {assign 'md' $meta_description}
    {foreach $features as $f}
        {foreach $f->options as $o}
            {if $smarty.get.$f@key == $o->value}
                {assign 'mt' $f->name|cat:' - '|cat:$o->value|cat:' / '|cat:$mt}
                {assign 'md' $f->name|cat:' - '|cat:$o->value|cat:'. '|cat:$md}
            {/if}       
        {/foreach}
    {/foreach}
    {$meta_title = $mt scope=parent}
    {$meta_description = $md scope=parent}
{/if}

<div class="container">
	<div class="padding-top-30 blocks-spacer">
		<div class="row">
			{if $products}
			<aside class="span3 " id="filter">
				{if $category->brands || $features}
					{include file='filter.tpl'}
				{/if}
				
				{include file='side_categories.tpl'}
			</aside>
			
			<section class="span9">
				<div class="white-box push-down-20">
					<ul class="breadcrumb">
						<li><a href="/">Главная</a></li>
						{if $category}
						{foreach from=$category->path item=cat}
							<li><span class="icon-chevron-right"></span></li>
							<li><a href="catalog/{$cat->url}">{$cat->name|escape}</a></li>
						{/foreach}  
						{if $brand}
							<li><span class="icon-chevron-right"></span></li>
							<li><a href="catalog/{$cat->url}/{$brand->url}">{$brand->name|escape}</a></li>
						{/if}
						{elseif $brand}
							<li><span class="icon-chevron-right"></span></li> 
							<li><a href="brands/{$brand->url}">{$brand->name|escape}</a></li>
						{elseif $keyword}
							<li><span class="icon-chevron-right"></span></li>
							<li>Поиск</li>
						{/if}
					</ul>

					<div class="category-info">
						<div class="info">
							{if $keyword}
							<h1 class="main_heading">Поиск {$keyword|escape}</h1>
							{elseif $page}
							<h1 class="main_heading">{$page->name|escape}</h1>
							{else}
							<h1 class="main_heading">{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h1>
							{/if}
							
							{if $category->image && $category->description}
							<div class="image">
								<img src="{$config->categories_images_dir}{$category->image}" alt="{$c->name|escape}">
							</div>
							{/if}
							
							{* Описание страницы (если задана) *}
							{$page->body}

							{if $current_page_num==1}
							{* Описание категории *}
							{$category->description}
							{/if}
						</div>

						{if $category->subcategories}
						<div class="category-list clearfix">
							<h2>Уточните категорию</h2>

							<ul>
								{foreach $category->subcategories as $c}
									{if $c->visible}
										<li>
											<a class="skew-btn tny" href="catalog/{$c->url}" data-category="{$c->id}">{$c->name|escape}</a>
										</li>
									{/if}
								{/foreach}
							</ul>
						</div>
						{/if}
						
						<div class="product-filter clearfix">
							
							
							<div class="pull-right">
								<div class="form-inline sorting-by">
									<label for="selectPrductSort" class="black-clr">Сортировать по:</label>
								
									<select id="selectPrductSort" class="span3" onchange="location = this.value;">
										<option value="{url sort=position page=null}"{if $sort=='position'} selected{/if}>умолчанию</option>
										<option value="{url sort=price page=null}"{if $sort=='price'} selected{/if}>цене</option>
										<option value="{url sort=name page=null}"{if $sort=='name'} selected{/if}>названию</option>
										<option value="{url sort=rating page=null}"{if $sort=='rating'} selected{/if}>рейтингу</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>



			
				<div class="row popup-products">
					
						{foreach $products as $product}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
						{if $product@iteration is div by 3}<div class="clearfix"></div>{/if}
						{/foreach}
					</div>
				</div>
				
				

				<div class="row">
					<div class="span9 offset3">
						<div class="pagination">
							{include file = 'pagination.tpl'}
						</div>
					</div>
				</div>
			</section>
			
			{else}
			
			<section class="span12">
				<div class="underlined push-down-20">
					<div class="row">
						<div class="span12">
							<h3>{$category->name|escape} {$brand->name|escape} {$keyword|escape}</h3>
						</div>
					</div>
				</div>

				<div class="alert in fade">
					Товары не найдены
				</div>
			</section>
			{/if}	
		</div>			
	</div>			
</div>