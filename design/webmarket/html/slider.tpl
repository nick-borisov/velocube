<div class="fullwidthbanner-container">
	<div class="fullwidthbanner">
		<ul>
			<li data-transition="fade" data-slotamount="3">
				<img src="design/{$settings->theme}/slides/slide01_bg.jpg" alt="slider img" width="1400" height="377" />

				<div class="caption lfr easeInOutExpo "
					data-x="400" 
					data-y="202"
					data-speed="1000" 
					data-start="965"
					data-easing="easeInOutExpo">
					<img src="design/{$settings->theme}/slides/slide01_object01.png">
				</div>

				<div class="caption lfl easeInCirc " 
					data-x="400" 
					data-y="304" 
					data-speed="1000" 
					data-start="1723" 
					data-easing="easeInCirc">
					<img src="design/{$settings->theme}/slides/slide01_object02.png">
				</div>
				

				<div class="caption lfb easeInOutExpo " 
					data-x="400" 
					data-y="356"
					data-speed="1000"
					data-start="2358" 
					data-easing="easeInOutExpo">
					<img src="design/{$settings->theme}/slides/slide01_object03.png">
				</div>

				<div class="caption randomrotate easeOutSine "
					data-x="400"
					data-y="416"
					data-speed="1000" 
					data-start="3120"
					data-easing="easeOutSine">
					<img src="design/{$settings->theme}/slides/slide01_object04.png">
				</div>
			</li><!-- /slide -->

			<li data-transition="fade" data-slotamount="3">
				<img src="design/{$settings->theme}/slides/slide02_bg.jpg">
				
				<div class="caption lfr easeOutBack "
					data-x="1292"
					data-y="284"
					data-speed="1200" 
					data-start="3201"
					data-easing="easeOutBack">
					<img src="design/{$settings->theme}/slides/slide02_object05.png">
				</div>
				
				<div class="caption lfl easeOutElastic "
					data-x="225" 
					data-y="398"
					data-speed="1000"
					data-start="2099"
					data-easing="easeOutElastic">
					<img src="design/{$settings->theme}/slides/slide02_object03.png">
				</div>
				
				<div class="caption lfb easeInCubic " 
					data-x="400"
					data-y="87" 
					data-speed="1000" 
					data-start="648" 
					data-easing="easeInCubic">
					<img src="design/{$settings->theme}/slides/slide02_object01.png">
				</div>
				
				<div class="caption lfr easeOutQuad "
					data-x="483"
					data-y="123" 
					data-speed="1000"
					data-start="1242" 
					data-easing="easeOutQuad">
					<img src="design/{$settings->theme}/slides/slide02_object02.png">
				</div>
				
				<div class="caption lfr easeOutExpo " 
					data-x="1367" 
					data-y="141" 
					data-speed="1000" 
					data-start="2819" 
					data-easing="easeOutExpo">
					<img src="design/{$settings->theme}/slides/slide02_object04.png">
				</div>
			</li>
			
			<li data-transition="fade" data-slotamount="3" >
				<img src="design/{$settings->theme}/slides/slide03_bg.jpg">

				<div class="caption lfr easeOutQuart "
					data-x="400" 
					data-y="138"
					data-speed="1000" 
					data-start="789"
					data-easing="easeOutQuart">
					<img src="design/{$settings->theme}/slides/slide03_object01.png">
				</div>
				
				<div class="caption lfl easeInQuint "
					data-x="443"
					data-y="240"
					data-speed="1000"
					data-start="1761"
					data-easing="easeInQuint">
					<img src="design/{$settings->theme}/slides/slide03_object02.png">
				</div>
				
				<div class="caption randomrotate easeOutExpo "
					data-x="437" 
					data-y="311"
					data-speed="1000"
					data-start="2446"
					data-easing="easeOutExpo">
					<img src="design/{$settings->theme}/slides/slide03_object03.png">
				</div>
				
				<div class="caption lfb easeOutQuint " 
					data-x="543"
					data-y="373" 
					data-speed="1000" 
					data-start="3649" 
					data-easing="easeOutQuint">
					<img src="design/{$settings->theme}/slides/slide03_object04.png">
				</div>
			</li>
		</ul>
		
		<div class="tp-bannertimer"></div>
	</div>

	<div id="sliderRevLeft"><i class="icon-chevron-left"></i></div>
	<div id="sliderRevRight"><i class="icon-chevron-right"></i></div>
</div>