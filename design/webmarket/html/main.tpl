{* Главная страница магазина *}

{* Канонический адрес страницы *}
{$canonical="" scope=parent}

{include file = 'slider.tpl'}

<div class="container">
	<div class="row">
		<div class="span12 blocks-spacer">
			<div class="row">
				<div class="span4">
					<a href="#" class="banner">
						<img src="design/{$settings->theme}/banners/te-b1.jpg" alt="">
					</a>
				</div>
				<div class="span4">
					<a href="#" class="banner">
						<img src="design/{$settings->theme}/banners/te-b2.jpg" alt="">
					</a>
				</div>
				<div class="span4">
					<a href="#" class="banner">
						<img src="design/{$settings->theme}/banners/te-b3.jpg" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
	
	{get_featured_products var=featured_products}
	{if $featured_products}
	<div class="row featured-items blocks-spacer">
		<div class="span12">
			<div class="main-titles double-lined align-center">
				<h2 class="skew_title"><span class="light">Рекомендуемые товары</span></h2>
				<div class="arrows">
					<a href="#" class="icon-arrow-left" id="featuredItemsLeft"></a>
					<a href="#" class="icon-arrow-right" id="featuredItemsRight"></a>
				</div>
			</div>
		</div>

		<div class="span12 popup-products">
			<div class="carouFredSel" data-autoplay="false" data-nav="featuredItems">
				{foreach $featured_products as $product}
				{if $product@first || ($product@iteration - 1) is div by 4}
				<div class="slide">
					<div class="row">
					{/if}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
					{if $product@last || $product@iteration is div by 4}
					</div>
				</div>
				{/if}
				{/foreach}
			</div>
		</div>
	</div>
	{/if}
	
	{get_new_products var=new_products limit=8}
	{if $new_products}
	<div class="row featured-items blocks-spacer">
		<div class="span12">
			<div class="main-titles double-lined align-center">
				<h2 class="skew_title"><span class="light">Новые товары</span></h2>
				<div class="arrows">
					<a href="#" class="icon-arrow-left" id="newItemsLeft"></a>
					<a href="#" class="icon-arrow-right" id="newItemsRight"></a>
				</div>
			</div>
		</div>

		<div class="span12 popup-products">
			<div class="carouFredSel" data-autoplay="false" data-nav="newItems">
				{foreach $new_products as $product}
				{if $product@first || ($product@iteration - 1) is div by 4}
				<div class="slide">
					<div class="row">
					{/if}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
					{if $product@last || $product@iteration is div by 4}
					</div>
				</div>
				{/if}
				{/foreach}
			</div>
		</div>
	</div>
	{/if}
	
	{get_discounted_products var=discounted_products limit=8}
	{if $discounted_products}
	<div class="row featured-items blocks-spacer">
		<div class="span12">
			<div class="main-titles double-lined align-center">
				<h2 class="skew_title"><span class="light">Акционные товары</span></h2>
				<div class="arrows">
					<a href="#" class="icon-arrow-left" id="discountedItemsLeft"></a>
					<a href="#" class="icon-arrow-right" id="discountedItemsRight"></a>
				</div>
			</div>
		</div>

		<div class="span12 popup-products">
			<div class="carouFredSel" data-autoplay="false" data-nav="discountedItems">
				{foreach $discounted_products as $product}
				{if $product@first || ($product@iteration - 1) is div by 4}
				<div class="slide">
					<div class="row">
					{/if}
						<div class="span3">
							{include file = 'product_block.tpl'}
						</div>
					{if $product@last || $product@iteration is div by 4}
					</div>
				</div>
				{/if}
				{/foreach}
			</div>
		</div>
	</div>
	{/if}	


	{get_posts var=last_posts limit=3}
	{if $last_posts}
	<div class="row blocks-spacer">
		<div class="span12">
			<div class="main-titles double-lined align-center">
				<h2 class="skew_title"><span class="light">Блог</span></h2>
			</div>
		</div>
		
		{foreach $last_posts as $post}
		<div class="span4">
			<div class="white-box padding-box" data-post="{$post->id}">
				<div class="published">{$post->date|date}</div>
				<h6><a href="blog/{$post->url}">{$post->name|escape}</a></h6>
				<p>{$post->annotation}</p>
			</div>
		</div>
		{/foreach}
	</div>
	{/if}
	
	
	{get_brands var=all_brands}
	{if $all_brands}
	<div class="row">
		<div class="span12">
			<div class="main-titles double-lined align-center">
				<h2 class="skew_title"><span class="light">Наши Бренды</span></h2>
				<div class="arrows">
					<a href="#" class="icon-arrow-left" id="brandsLeft"></a>
					<a href="#" class="icon-arrow-right" id="brandsRight"></a>
				</div>
			</div>
		</div>
	</div>

	<div class="row blocks-spacer-last">
		<div class="span12">
			<div class="brands carouFredSel" data-nav="brands" data-autoplay="true">
				{foreach $all_brands as $b}	
					{if $b->image}
						<a href="brands/{$b->url}"><img src="{$config->brands_images_dir}{$b->image}" alt="{$b->name|escape}"></a>
					{else}
						<a href="brands/{$b->url}"><span>{$b->name}</span></a>
					{/if}
				{/foreach}
			</div>
		</div>
	</div> 
	{/if}
	
	<div class="row blocks-spacer-last">
		<div class="span4">
			<a href="#" class="banner">
				<img src="design/{$settings->theme}/banners/be-c1.jpg" alt="">
			</a>
		</div>
		<div class="span4">
			<a href="#" class="banner">
				<img src="design/{$settings->theme}/banners/be-c2.jpg" alt="">
			</a>
		</div>
		<div class="span4">
			<a href="#" class="banner">
				<img src="design/{$settings->theme}/banners/be-c3.jpg" alt="">
			</a>
		</div>
	</div>
	
	<div class="row blocks-spacer-last">
		<div class="span6">
			<ul class="social-icons">
				<li><a href="#" class="zocial-facebook"></a></li>
				<li><a href="#" class="zocial-twitter"></a></li>
				<li><a href="#" class="zocial-googleplus"></a></li>
				<li><a href="#" class="zocial-pinterest"></a></li>
				<li><a href="#" class="zocial-youtube"></a></li>
				<li><a href="#" class="zocial-rss"></a></li>
			</ul>
		</div>
		
		<div class="span6 align-right">
			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
				<form action="http://proteusthemes.us4.list-manage1.com/subscribe/post?u=ea0786485977f5ec8c9283d5c&amp;id=5dad3f35e9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form form-inline" target="_blank" novalidate>
					<div class="mc-field-group">
						<span class="month-push-right">Новостная рассылка</span>
						<input type="email" value="" placeholder="Введите ваш email" name="EMAIL" class="required email newsletter-input" id="mce-EMAIL">
						<button type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="skew-btn">Отправить</button>
					</div>
					
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>
				</form>
			</div>
			<!--End mc_embed_signup-->
		</div>
	</div>
</div>