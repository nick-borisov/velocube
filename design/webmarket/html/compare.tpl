<div class="container">
	<div class="padding-top-30 blocks-spacer">
		<div class="row">
			<div class="span12 white-box">
				<ul class="breadcrumb">
					<li><a href="./">Главная</a></li>
					<li><span class="icon-chevron-right"></span></li>
					<li>Сравнение товаров</li>
				</ul>
				
				<div class="padding-box">
					<h1 class="main_heading">
						Сравнение товаров
					</h1>

					{if $products}
					<table class="table table-striped table-bordered table-compare">
						<thead>
							<tr>
								<td>&nbsp;</td>
								
								{foreach from=$products item=product}
								<td class="product">
									{if $product->image}
									<div class="image">
										<a href="products/{$product->url}"><img src="{$product->image->filename|resize:100:100}" alt="{$product->name|escape}"/></a>
									</div>
									{/if}

									<h4 class="light"><a data-product="{$product->id}" href="products/{$product->url}">{$product->name|escape}</a></h4>
								
									<div class="testRater_{$product->id} clr" id="rating_{$product->id}" class="stat">
										<span class="rater-starsOff" style="width:85px;">
											<span class="rater-starsOn" style="width:{$product->rating*85/5|string_format:"%.0f"}px"></span>
										</span>
										
										<br>
										
										<a href='compare/remove/{$product->url}'>Убрать</a>
									</div>
								</td>
								{/foreach} 
							</tr>
						</thead>
						
						<tbody>
							{foreach from=$compare_features name=features item=property}
							<tr>
								<td> 
									<strong>{$property->name}: </strong>
								</td>

								{foreach from=$products item=product}
								<td> 
									{foreach $product->features as $f}
									{if $f->name == $property->name}
									<span>{$f->value}</span>
									{/if}

									{/foreach}
								</td>
								{/foreach} 
							</tr>
							{/foreach} 

							<tr>
								<td> 
									<strong>Описание: </strong>		
								</td>
								
								{foreach from=$products item=product}
									<td> 
										{$product->annotation}
									</td>
								{/foreach} 
							</tr>

							<tr>
								<td> 
									<strong>Цена: </strong>		
								</td>
								
								{foreach from=$products item=product}
									<td> 
										{if $product->variants|count > 0}
											<span class="striked gray-clr">{if $product->variant->compare_price > 0}{$product->variant->compare_price|convert}{/if}</span> 
											<span>{$product->variant->price|convert}</span> {$currency->sign|escape}
										{else}
											Нет в наличии
										{/if}
									</td>
								{/foreach} 
							</tr>
							
							<tr>
								<td>&nbsp;</td>
								
								{foreach from=$products item=product}
								<td class=""> 
									<form class="variants align-center no-margin" action="/cart">
										{if $product->variants|count > 0}
											<input id="variants_{$product->variant->id}" name="variant" value="{$product->variant->id}" type="radio" checked style="display:none;"/>

											<button class="skew-btn"><i class="icon-shopping-cart"></i> В корзину</button>
										{/if}
									</form>
								</td>
								{/foreach}
							</tr>
						</tbody>
					</table>
					{else}
					<div class="alert in fade push-down-40">
						Нет товаров для сравнения
					</div>
					{/if}
				</div>
			</div>
		</div>
	</div>
</div>


 

<div id="product_params">

</div>






<!-- Описание товара #End/-->
<!-- Товар  #End /-->


