<form method="get" class="sidebar-item sidebar-filters blocks-spacer" action="catalog/{$category->url}">
	<div class="underlined">
		<h3><span class="light">Фильтр</span> товаров</h3>
	</div>
	
	<!-- Price -->
	<div class="filter_box">
		<span class="filter_title">Цена</span>
		
		<div id="filterPrices" class="accordion-body">
			
				<div class="price_slider jqueryui-slider-container">
					<div id="slider_price" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content" data-slider-min-range="{$slider_max_min_price->min_price|convert:null:false|floor}" data-slider-max-range="{$slider_max_min_price->max_price|convert:null:false|ceil}">
					
						<div class="ui-slider-range ui-widget-header ui-widget-header-bar"></div>
						<div class="ui-slider-range ui-widget-header ui-widget-header-left ui-widget-header-hidden"></div>
						<div class="ui-slider-range ui-widget-header ui-widget-header-right ui-widget-header-hidden"></div>

						<div class="ui-slider-range ui-widget-header"></div>
						<a class="ui-slider-handle ui-state-default ui-state-left" href="#"></a>
						<a class="ui-slider-handle ui-state-default ui-state-right" href="#"></a>
					</div>
				</div>
			
		</div>

		<div class="accordion-inner align-center">
			<span>от</span>
			<input type="text" class="keypress max-val" id="min_price" data-min-price="{if $smarty.get.min_price}{$smarty.get.min_price}{else}{$max_min_price->min_price|convert:null:false|floor}{/if}" name="min_price" value="{if $smarty.get.min_price}{$smarty.get.min_price}{else}{/if}" autocomplete="off">
			<span>до</span>
			<input type="text" class="keypress min-val" id="max_price" data-max-price="{if $smarty.get.max_price}{$smarty.get.max_price}{else}{$max_min_price->max_price|convert:null:false|ceil}{/if}" name="max_price" value="{if $smarty.get.max_price}{$smarty.get.max_price}{else}{/if}" autocomplete="off">
			<span>{$currency->sign|escape}</span>
		</div>
	</div>
	
	<div class="filter_box">
		<span class="filter_title">Цена</span>
		
		<ul>
			<li{if $discounted->disabled} class="disabled"{/if}>
				<input id="discounted" type="checkbox" name="discounted" value="1"{if $discounted->checked} checked{elseif $discounted->disabled} disabled{/if}>
				<label for="discounted"><span>Акционные</span> <i>{if !$discounted->checked}({$discounted->count}){/if}</i></label>
			</li>
			<li{if $featured->disabled} class="disabled"{/if}>
				<input id="featured" type="checkbox" name="featured" value="1"{if $featured->checked} checked{elseif $featured->disabled} disabled{/if}>
				<label for="featured"><span>Рекомендуемые</span> <i>{if !$featured->checked}({$featured->count}){/if}</i></label>
			</li>
		</ul>
	</div>
	
	
	{if $category->brands}
	<div class="filter_box">
		<span class="filter_title">Бренды</span>
		
		<ul>
			{foreach $category->brands as $b}
			<li{if $b->disabled} class="disabled"{/if}>
				<input id="brand_{$b->id}" type="checkbox" name="brand_id[]" value="{$b->id}"{if $b->checked} checked{elseif $b->disabled} disabled{/if}>
				<label for="brand_{$b->id}"><span>{$b->name|escape}</span> <i>{if !$b->checked}({$b->count}){/if}</i></label>
			</li>
			{/foreach}
		</ul>
	</div>
	{/if}
	
	{if $features}
		{foreach $features as $f}
		<div class="filter_box">
			<span class="filter_title">{$f->name}</span>
			<ul>
				{foreach $f->options as $k=>$o}
				<li{if $o->disabled} class="disabled"{/if}>
					<input id="option_{$f->id}_{$k}" type="checkbox" name="{$f->id}[]" value="{$o->value|escape}"{if $o->checked} checked{elseif $o->disabled} disabled{/if}>
					<label for="option_{$f->id}_{$k}"><span>{$o->value|escape}</span> <i>{if !$o->checked}({$o->count}){/if}</i></label>
				</li>
				{/foreach}
			</ul>
		</div>
		{/foreach}
	{/if}
	<span class="count_products_search"></span>
	
	<div class="filter_buttons">
		<button><h3 class="skew_title"><span>Подобрать</span></h3></button>
		
		<a href="catalog/{$category->url}">Сбросить все</a>
	</div>
</form>