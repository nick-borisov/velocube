<div id="creditModal" class="modal modal-large hide fade" tabindex="-1" role="dialog" aria-labelledby="creditModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="creditModalLabel"><span class="light">Купить</span> в кредит</h3>
	</div>
	
	<div class="modal-body">
		<p>Получите кредит, не выходя из дома! Просто положите товар в корзину, а при оформлении отметьте, что хотите приобрести его в кредит. Заполните короткую анкету и менеджер Yes Credit сообщит вам решение банка. Никаких посещений банка, договор вам доставит курьер!</p>
		
		<p>Первый взнос: <span class="bold"><span id="p_v"></span> {$currency->sign|escape}</span></p>
		
		<p class="center-align push-down-0">
			<a href="#" class="skew-btn" id="pr_cred">Купить в кредит</a>
		</p>
	</div>
</div>
