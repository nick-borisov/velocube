// Аяксовая корзина
$(document).on('submit', 'form.variants', function(e) {
	e.preventDefault();
	button = $(this).find('input[type="submit"]');
	if($(this).find('input[name=variant]:checked').size()>0)
		variant = $(this).find('input[name=variant]:checked').val();
	if($(this).find('select[name=variant]').size()>0)
		variant = $(this).find('select').val();
		
	amount = $(this).find('input[name=amount]').val();	
	
	$.ajax({
		url: "ajax/cart.php",
		data: {variant: variant, amount: amount},
		dataType: 'json',
		success: function(data){
			$('#cart_informer').html(data.cart_informer);
			$('#cartModal').html(data.cartModal);
			
			if(button.attr('data-result-text'))
				button.val(button.attr('data-result-text'));

			$(".products-box.scroll").mCustomScrollbar({
				theme:"dark",
				scrollInertia: 250
			});
			
			$('#cartModal').modal()
		}
	});

	return false;
});

$(document).on('submit', 'form.cart_mini', function(e) {
	e.preventDefault();
	button = $(this).find('input[type="submit"]');
	$.ajax({
		url: "ajax/cart.php",
		data: {variant: $(this).find('input[name="variant_id"]').val(),'mode':'remove'},
		dataType: 'json',
		success: function(data){
			$('#cart_informer').html(data.cart_informer);
			$('#cartModal').html(data.cartModal);

			$(".products-box.scroll").mCustomScrollbar({
				theme:"dark",
				scrollInertia: 250
			});
			

			$('#cartModal').modal("show")
		}
	});
	
	
	
	return false;
});
$('.credit_btn').click(function(){
	$('#p_v').html( $(this).data('price') );
	$('#pr_cred').attr( 'href', $(this).data('url') );
	$('#creditModal').modal()
	return false
})