<?php

require_once('api/Simpla.php');

class Comepay extends Simpla
{
	public function checkout_form($order_id, $button_text = null)
	{
		if(empty($button_text))
			$button_text = 'Оплатить';

		$order = $this->orders->get_order((int)$order_id);
		$payment_method = $this->payment->get_payment_method($order->payment_method_id);
		$payment_currency = $this->money->get_currency(intval($payment_method->currency_id));
		$payment_settings = $this->payment->get_payment_settings($payment_method->id);

		$price = $this->money->convert($order->total_price, $payment_method->currency_id, false);

		$success_url = $this->config->root_url.'/order/'.$order->url;

		$fail_url = $this->config->root_url.'/order/'.$order->url;
		$phone = preg_replace('/[^\d]/', '', $order->phone);
		$phone = substr($phone, -min(10, strlen($phone)), 10);

		$button = '';
		if(isset($_GET['fail'])){
			$button .= '<div class="message_error">';
			if($_GET['fail']){
				$button .= 'Ошибка выставления счёта:';
				switch ($_GET['fail']) {
					case 5:$button .= 'Неверные параметры запроса';break;
	        		case 13:$button .= 'Сервер Comepay занят';break;
	        		case 150:$button .= 'Ошибка авторизации';break;
	        		case 210:$button .= 'Запрос не найден';break;
	        		case 241:$button .= 'Сумма меньше минимума';break;
	        		case 242:$button .= 'Превышена максимальная сумма для оплаты через Comepay кошелек (15000руб), обратитесь к администратору сайта.';break;
	        		case 300:$button .= 'Ошибка сервера';break;
	        		case 99999:$button .= 'Ошибка аутентификации';break;

					default:
						$button .= 'проблемы с сетью';
						break;
				}
			} else {
				$button .= 'Ошибка при проведении платежа';
			}
			$button .= '</div>';
		}
		if(isset($_GET['success'])){
			$button .='<div style="padding: 10px 20px 10px 20px;border: 1px solid #51a400;background-color: #d3ffa9;color: #2e5e00;font-size: 14px;">Спасибо за оплату заказа, статус заказа в скором времени обновится автоматически</div>';
		}


		$button .= '<form method="post" action="'.$this->config->root_url.'/payment/Comepay/create.php">
					<input type="hidden" name="order_id" value="'.$order->id.'">';
		if($payment_settings['comepay_payment_type']) {
			$button .= '<label>Укажите номер телефона для выставления счёта (в международном формате +79990000000)</label><br>
			<input type="text" name="phone" value="+7'.$phone.'"><br>';
		}
        if(!isset($_GET['success'])){
    		$button .= '
    		<input type="submit" class="checkout_button" value="'.$button_text.'">';
        }
		$button .= '</form>';




		return $button;
	}


}