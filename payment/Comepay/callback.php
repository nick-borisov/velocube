<?php
// Работаем в корневой директории
chdir ('../../');
require_once('api/Simpla.php');
$simpla = new Simpla();
function comepay_answer($code){
	header("Content-Type: text/xml");
	echo "<?xml version=\"1.0\"?><result><result_code>$code</result_code></result>";
	exit();
}
function check_comepay_sign($simpla, $settings){
	return isset($_SERVER['PHP_AUTH_USER'])?
            ($_SERVER['PHP_AUTH_USER']==$settings['comepay_login']&&$_SERVER['PHP_AUTH_PW']==$settings['comepay_callbak_password']):
            ($_SERVER['REMOTE_USER']=='Basic '.base64_encode($settings['comepay_login'].':'.$settings['comepay_callbak_password']));
}

$order_id		 	= intval($simpla->request->post('bill_id'));
$amount				= $simpla->request->post('amount');
$status = $simpla->request->post('status');
$result = '151';
////////////////////////////////////////////////
// Выберем заказ из базы
////////////////////////////////////////////////
$order = $simpla->orders->get_order($order_id);
if(empty($order))
	comepay_answer($result);

////////////////////////////////////////////////
// Выбираем из базы соответствующий метод оплаты
////////////////////////////////////////////////
$method = $simpla->payment->get_payment_method(intval($order->payment_method_id));
if(empty($method))
	comepay_answer($result);

$settings = unserialize($method->settings);
$payment_currency = $simpla->money->get_currency(intval($method->currency_id));


// Проверяем контрольную подпись
if(check_comepay_sign($simpla, $settings) && $status == 'paid'){
	$result = 0;
		// Установим статус оплачен
	$simpla->orders->update_order(intval($order->id), array('paid'=>1));

	// Отправим уведомление на email
	$simpla->notify->email_order_user(intval($order->id));
	$simpla->notify->email_order_admin(intval($order->id));

	// Спишем товары
	$simpla->orders->close(intval($order->id));

}
comepay_answer($result);
