<?php
	define('COMEPAY_TEST',0);
	function comepay_request($url,$data,$headers=array(), $method = 'PUT') {
		if(COMEPAY_TEST){
			$host = 'moneytest.comepay.ru';
			$port = '439';
		} else {
			$host = 'shop.comepay.ru';
			$port = '443';
		}

		$data = http_build_query($data);
		//$url = parse_url($url);
		$head = '';
		if (!empty($headers)){
			foreach($headers as $param=>$value){
				$head .= "{$value}\r\n";
			}
		}
		$fp = fsockopen("ssl://".$host, $port, $errno, $errstr, 30);
		$result = '';
		if (!$fp) {
		    echo "$errstr ($errno)<br />\n";
		    return false;
		} else {
		    $out = $method." {$url} HTTP/1.0\r\n";
		    $out .= "Host: {$host}\r\n";
		    $out .= $head;
		    //$out .= "Content-type: application/x-www-form-urlencoded\r\n";
		    if($data){
			    $out .='Content-Length: ' . strlen($data)."\r\n";
			}
		    $out .= "Connection: Close\r\n\r\n";
		    $out .= $data;
		    fwrite($fp, $out);
		    $headers = "";
			while(!feof($fp))
			{
				$line = fgets($fp, 4096);
				if($line == "\r\n")
				{
					break;
				}
				$headers .= $line;
			}

				while(!feof($fp))
					$result .= fread($fp, 4096);

		    fclose($fp);
		}
		if( strpos($result, "<h2>401")!==FALSE) {
			$result = json_encode(array('response'=>array('result_code'=>99999)));
		}
		return $result;
	}
	// Работаем в корневой директории
	chdir ('../../');

	// Подключаем симплу
	require_once('api/Simpla.php');
	$simpla = new Simpla();

	// Выбираем оплачиваемый заказ
	$order = $simpla->orders->get_order(intval($simpla->request->post('order_id')));

	if(empty($order))
		exit();

	// Выбираем из базы соответствующий метод оплаты
	$method = $simpla->payment->get_payment_method(intval($order->payment_method_id));
	if(empty($method))
		exit();
	// Настройки способа оплаты
	$payment_settings = unserialize($method->settings);
	$success_url = $simpla->config->root_url.'/order/'.$order->url.'?success';
	$fail_url = $simpla->config->root_url.'/order/'.$order->url.'?fail';
	$phone = preg_replace('/[^\d]/', '', $_POST['phone']);
	$phone = substr($phone, -min(10, strlen($phone)), 10);
	$price = $order->total_price;
	if($payment_settings['comepay_payment_type']) {
		$user = 'tel:+7'.$phone;
	} else {
		$user = 'bankcard';
	}
	$result = comepay_request('/api/prv/'.$payment_settings['comepay_prvid'].'/bills/'.$order->id,
		array(
		'user'=>$user,
		'amount'=>$price,
		'ccy'=>'RUB',
		'comment'=>'Оплата заказа №'.$order->id,
		'lifetime'=>date('Y-m-d\TH:i:s',strtotime('+168 hour')),
		'email'=>$order->email

		)
		, array(
	'Accept: text/json',
		'Authorization: Basic '.base64_encode($payment_settings['comepay_login'].':'.$payment_settings['comepay_password']),
		'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
		)
	);
	$result = json_decode($result);
	if($result && ($result->response->result_code==0 || $result->response->result_code==215)){
		if(COMEPAY_TEST) {
			$host = 'moneytest.comepay.ru:439';
		} else {
			$host = 'shop.comepay.ru';
		}

		$url =	"https://$host/Order/external/main.action?shop={$payment_settings['comepay_prvid']}&transaction={$order->id}&successUrl=".urlencode($success_url)."&failUrl=".urlencode($fail_url)."&terminalBackUrl=".urlencode($success_url);
	} else {
		$url = $fail_url.'='.$result->response->result_code;
	}
	header('Location: '.$url);
	exit();

