<?php
namespace WalletOne;

class W1ResultCms extends W1Result  {
  
  /**
   *
   * @var array
   */
  public $fieldsName = array();
  
  /**
   * The array with all message;
   * 
   * @var array 
   */
  public $messages = array();
  
  function __construct($params, $lang = 'ru') {
    if (!defined('w1PathImg')) {
      define('w1PathImg', '');
    }
    
    $defaultLang = 'ru';
    if($lang != 'ru'){
      $defaultLang = 'en';
    }
    
    include_once str_replace('\\', '/', __DIR__) . '/../lang/settings.'.$defaultLang.'.php';
    include str_replace('\\', '/', __DIR__) . '/../config.php';
    include_once 'w1.decoding.php';
    include_once('w1.helpers.php');
    $this->requiredFields = $config['result']['requiredFields'];
    $this->fieldsName = $config['result']['fieldsName'];
    $this->fieldsPreg = $config['result']['fieldsPreg'];
    if(!empty($params['WMI_ORDER_ID'])) {
      $this->orderPaymentId = $params['WMI_ORDER_ID'];
    }
    if(!empty($params['WMI_ORDER_STATE'])) {
      $this->orderState = mb_strtolower($params['WMI_ORDER_STATE']);
    }
    if(!empty($params['WMI_PAYMENT_NO'])) {
      $this->orderId = str_replace('_'.$_SERVER['HTTP_HOST'], '', $params['WMI_PAYMENT_NO']);
    }
    if(!empty($params['WMI_PAYMENT_TYPE'])) {
      $this->paymentType = $params['WMI_PAYMENT_TYPE'];
    }
    if(!empty($params['WMI_SIGNATURE'])) {
      $this->signature = rawurldecode($params['WMI_SIGNATURE']);
    }
    if(!empty($params['WMI_PAYMENT_AMOUNT'])) {
      $this->summ = $params['WMI_PAYMENT_AMOUNT'];
    }
  }
  
  /**
   * Get value of field.
   * 
   * @param string $name
   * @return string
   */
  public function getValue($name){
    $reflect = new \ReflectionClass(get_class($this));
    $property = $reflect->getProperty($name);
    return $property->getValue($this);
  }
  
  /**
   * To check the required fields.
   * 
   * @param array $fields
   * @return boolean
   */
  public function required(){
    $this->errors = array();
    $this->messages = array();
    if(!empty($this->requiredFields)){
      foreach ($this->requiredFields as $value) {
        if(property_exists($this, $value)) {
          $field = $this->getValue($value);
          if(empty($field)){
            $this->errors[] = sprintf(w1ErrorRequired, $this->fieldsName[$value]);
            Helpers::logging(sprintf(w1ErrorRequired, $this->fieldsName[$value]));
          }
        }
        else{
          $this->errors[] = sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]);
          Helpers::logging(sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]));
        }
      }
      if(!empty($this->errors)){
        $this->messages[] = w1ErrorEmptyFields;
        return false;
      }
    }
    return true;
  }
  
  /**
   * Validation fields.
   * 
   * @return boolean
   */
  public function validation() {
    $this->errors = array();
    $this->messages = array();
    if(!empty($this->fieldsPreg)){
      foreach ($this->fieldsPreg as $key => $value) {
        if(property_exists($this, $key)) {
          $val = $this->getValue($key);
          if (!empty($val) && preg_match($value, $val) == 0) {
            $this->errors[] = sprintf(w1ErrorPreg, $this->fieldsName[$key]);
            Helpers::logging(sprintf(w1ErrorPreg, $this->fieldsName[$key]).' '.$val);
          }
        }
      }
      if(!empty($this->errors)){
        return false;
      }
    }
    return true;
  }
  
  /**
   * Checking signature.
   * 
   * @param array $post
   * @param string $sign
   * @param string $method
   * @return boolean
   */
  public function checkSignature($post, $sign, $method){
    $this->errors = array();
    $this->messages = array();
    foreach ($post as $key => $value) {
      if ($key !== "WMI_SIGNATURE") {
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
          if ($key == 'WMI_DESCRIPTION' || $key == 'WMI_CUSTOMER_FIRSTNAME' || $key == 'WMI_CUSTOMER_LASTNAME' || $key == 'WMI_FAIL_URL' || $key == 'WMI_PAYMENT_NO' || $key == 'WMI_SUCCESS_URL') {
            $params[$key] = urldecode(Decoding::to_utf8($value));
          }
          else {
            $params[$key] = rawurldecode($value);
          }
        }
        else {
          $params[$key] = mb_convert_encoding(urldecode(Decoding::to_utf8($value)), "UTF-8", "windows-1251");
        }
      }
    }
    uksort($params, "strcasecmp");

    $values = implode('', $params);
    $values = mb_convert_encoding($values, "windows-1251", "UTF-8");
    $signature = base64_encode(pack("H*", call_user_func($method, $values . $sign)));
    if ($signature != rawurldecode($post['WMI_SIGNATURE'])) {
      $this->errors[] = sprintf(w1ErrorResultSignature, $this->orderId);
      Helpers::logging(sprintf(w1ErrorResultSignature, $this->orderId));
      Helpers::logging($signature . ' || ' . rawurldecode($post['WMI_SIGNATURE']));
    }
    if (!empty($this->errors)) {
      return false;
    }
    return true;
  }
  
  public function checkSignatureUniversal($post, $sign, $method, $typeUser = 'browser') {
    $this->errors = array();
    $this->messages = array();
    $postSignat = '';
    foreach ($post as $key => $value) {
      if ($typeUser == 'bot') {
        if ($key !== "WMI_SIGNATURE") {
          $params[$key] = $value;
        }
        else{
          $postSignat = $value;
        }
      }
      else {
        if ($key !== "WMI_SIGNATURE") {
          $params[$key] = mb_convert_encoding(urldecode(Decoding::to_utf8($value)), "UTF-8", "windows-1251");
        }
        else{
          $postSignat = mb_convert_encoding(urldecode(Decoding::to_utf8($value)), "UTF-8", "windows-1251");
        }
      }
      
    }
    uksort($params, "strcasecmp");
    $values = implode('', $params);
    if ($typeUser == 'browser') {
      $values = mb_convert_encoding($values, "windows-1251", "UTF-8");
    }
    $signature = base64_encode(pack("H*", call_user_func($method, $values . $sign)));
    if ($signature != $postSignat) {
      $this->errors[] = sprintf(w1ErrorResultSignature, $this->orderId);
      Helpers::logging(sprintf(w1ErrorResultSignature, $this->orderId) . ' from ' . $typeUser);
      Helpers::logging($signature . ' || ' . $postSignat);
    }
    if (!empty($this->errors)) {
      return false;
    }
    return true;
  }
  
}
