<?php
namespace WalletOne;

/**
 * Abstract class for payment processing result.
 * 
 */
abstract class W1Result {

  /**
   * The set of required invoce fields.
   * 
   * @var array
   */
  public $requiredFields = array();
  
  /**
   * The array for check to the regular expressions.
   * 
   * @var array
   */
  public $fieldsPreg = array();
  
  /**
   * Id order in payment system.
   * 
   * @var int
   */
  public $orderPaymentId;
  
  /**
   * Status payment order. 
   * 
   * @var string
   */
  public $orderState;
  
  /**
   * Order number in CMS when returning.
   * 
   * @var string
   */
  public $orderId;
  
  /**
   * Type payment in payment system.
   * 
   * @var string
   */
  public $paymentType;
  
  /**
   * Secret code the returned in the response.
   * 
   * @var string
   */
  public $signature;
  
  /**
   * Summ of order.
   * 
   * @var string
   */
  public $summ;

  /**
   * The array with all errors;
   * 
   * @var array 
   */
  public $errors = array();

  /**
   * Checks for required fields and cheks validation these fields.
   * 
   * @param array $param
   * 
   * @return boolean
   */
  abstract public function validation();
  
  /**
   * Checking signature.
   * 
   * @param array $post
   * @param string $sign
   * @param string $method
   * @return boolean
   */
  abstract public function checkSignature($param, $sign, $method);
  
  /**
   * Get value of field.
   * 
   * @param string $name
   * @return string
   */
  abstract public function getValue($name);
  
}
