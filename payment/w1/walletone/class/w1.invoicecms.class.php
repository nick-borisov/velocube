<?php
namespace WalletOne;

class W1InvoiceCms extends W1Invoice  {
  
  /**
   *
   * @var array
   */
  public $fieldsName = array();
  
  /**
   * The array with all message;
   * 
   * @var array 
   */
  public $messages = array();
  
  public $orderNumber;
      
  function __construct($params, $lang = 'ru') {
    if (!defined('w1PathImg')) {
      define('w1PathImg', '');
    }
    
    $defaultLang = 'ru';
    if($lang != 'ru'){
      $defaultLang = 'en';
    }
    include_once str_replace('\\', '/', __DIR__) . '/../lang/settings.'.$defaultLang.'.php';
    
    include str_replace('\\', '/', __DIR__) . '/../config.php';
    include_once('w1.helpers.php');
    
    $this->requiredFields = $config['invoce']['requiredFields'];
    $this->fieldsName = $config['invoce']['fieldsName'];
    $this->fieldsPreg = $config['invoce']['fieldsPreg'];
    
    $currencyDefault = $config['currencyDefault'];
    
    if(!empty($params['order_id'])) {
      $this->orderId = $params['order_id'];
    }
    if(!empty($params['order_number'])) {
      $this->orderNumber = $params['order_number'];
    }
    if(!empty($params['order_summ'])) {
      $this->summ = $params['order_summ'];
    }
    if(!empty($params['currency_option'])) {
      $this->currencyId = $params['currency_option'];
    }
    if(!empty($params['currency_default'])) {
      $currencyDefault = $params['currency_default'];
    }
    if(!empty($params['firstname'])) {
      $this->firstNameBuyer = $params['firstname'];
    }
    if(!empty($params['lastname'])) {
      $this->lastNameBuyer = $params['lastname'];
    }
    if(!empty($params['email'])) {
      $this->emailBuyer = $params['email'];
    }
    
    if(!empty($params['order_currency'])){
      if($params['order_currency'] == 'RUR') {
        $params['order_currency'] = 'RUB';
      }
      $currency_iso = array_search($params['order_currency'], $config['currencyCode']);
      if($currency_iso){
        if($currencyDefault == $config['currencyDefault']){
          $this->currencyId = $currency_iso;
        }
      }
      else{
        $this->currencyId = 643;
      }
    }
    else{
      $this->currencyId = 643;
    }
  }
  
  /**
   * Get value of field.
   * 
   * @param string $name
   * @return string
   */
  public function getValue($name){
    $reflect = new \ReflectionClass(get_class($this));
    $property = $reflect->getProperty($name);
    return $property->getValue($this);
  }
  
  /**
   * To check the required fields.
   * 
   * @param array $fields
   * @return boolean
   */
  public function required(){
    $this->errors = array();
    $this->messages = array();
    
    if(!empty($this->requiredFields)){
      foreach ($this->requiredFields as $value) {
        if(property_exists($this, $value)) {
          $field = $this->getValue($value);
          if(empty($field)){
            $this->errors[] = sprintf(w1ErrorRequired, $this->fieldsName[$value]);
            Helpers::logging(sprintf(w1ErrorRequired, $this->fieldsName[$value]));
          }
        }
        else{
          $this->errors[] = sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]);
          Helpers::logging(sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]));
        }
      }
      if(!empty($this->errors)){
        $this->messages[] = w1ErrorEmptyFields;
        return false;
      }
    }
    return true;
  }
  
  
  /**
   * Validation fields.
   * 
   * @return boolean
   */
  public function validation() {
    $this->errors = array();
    $this->messages = array();
    if(!empty($this->fieldsPreg)){
      foreach ($this->fieldsPreg as $key => $value) {
        if(property_exists($this, $key)) {
          $val = $this->getValue($key);
          if (!empty($val) && preg_match($value, $val) == 0) {
            $this->errors[] = sprintf(w1ErrorPreg, $this->fieldsName[$key]);
            Helpers::logging(sprintf(w1ErrorPreg, $this->fieldsName[$key]));
          }
        }
      }
      if(!empty($this->errors)){
        return false;
      }
    }
    return true;
  }
  
}
