<?php

namespace WalletOne;

/**
 * Abstract class for building of html.
 * 
 */
abstract class W1Html {
  
  /**
   * The getting the html code of payments system.
   * 
   * @param string $paymentName
   *  Name of type payment system
   * @param array $paymentActive
   *  Array of selected options.
   * @param string $filename
   *  Path to the file with payment systems.
   * @param string $inputName
   *  Name for begin name of input. 
   * 
   * @return string
   *  Return html code with checkboxes and labels.
   */
  abstract public function getHtmlPayments($paymentName, $paymentActive, $filename, $inputName);
  
  /**
   * Create forn for payment system.
   * 
   * @param array $fields
   * @param string $paymentUrl
   * @param string $canelUrl
   * 
   * @return string
   *  Return html form.
   */
  abstract public function createForm($fields, $paymentUrl, $canelUrl);
  
}
