<?php

namespace WalletOne;

class Helpers{
  /**
   * Errors and information are logged to a file.
   * 
   * @param string $text
   * @return boolean
   */
  public static function logging($text){
    if(is_array($text)){
      $text = self::arrayToStringInErrorsModels($text);
    }
    $text = date('Y-m-d h:i:s').' '.$text ."\n";
    //Create the directory.
    if(!file_exists(str_replace('\\', '/', __DIR__) . '/../logs/')){
      if(!mkdir(str_replace('\\', '/', __DIR__) . '/../logs/')){
        return false;
      }
    }
    if(!file_exists(str_replace('\\', '/', __DIR__) . '/../logs/' . date('Y-m'))){
      if(!mkdir(str_replace('\\', '/', __DIR__) . '/../logs/' . date('Y-m'))){
        return false;
      }
    }
    
    //Put the text to a file
    if (is_writable(str_replace('\\', '/', __DIR__) . '/../logs/' . date('Y-m'))) {
      file_put_contents(str_replace('\\', '/', __DIR__).'/../logs/'.date('Y-m').'/'.date('Y-m-d').'.txt', $text, FILE_APPEND);
    }
  }
  
  /**
   * 
   * 
   * @param array $params
   * @return string
   */
  public static function arrayToStringInErrorsModels($params) {
    $string = '';
    foreach ($params as $key => $value) {
      if(is_array($value)){
        $string .= $key.' || '.$value[0]."\n";
      }
      else{
        $string .= $key.' || '.$value."\n";
      }
    }
    return $string;
  }
}

