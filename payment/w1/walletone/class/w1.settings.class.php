<?php
namespace WalletOne;
/**
 * Abstract class for items of settings of module.
 * 
 */
abstract class W1Settings {
  
  /**
   * Id merchant in payment system Wallet One.
   * 
   * @var string 
   */
  public $merchantId;
  
  /**
   * A secret code generated selected methods in a private office Wallet One.
   * 
   * @var string
   */
  public $signature;
  
  /**
   * The chosen method of generating secret code (MD5/SHA1).
   * 
   * @var string
   */
  public $signatureMethod;
  
  /**
   * Default currency.
   * 
   * @var int
   */
  public $currencyId;
  
  /**
   * Currency code list.
   * 
   * @var array
   */
  public $currencyCode = array();
  
  /**
   * Currency name list.
   * 
   * @var array
   */
  public $currencyName = array();


  /**
   * A option the selected of default currency.
   * 
   * @var string
   */
  public $currencyDefault;
  
  /**
   * The order status after successful payment.
   * 
   * @var string
   */
  public $orderStatusSuccess;
  
  /**
   * The order status waiting payment.
   * 
   * @var string
   */
  public $orderStatusWaiting;
  
  /**
   * The order status after fail payment.
   * 
   * @var string
   */
  public $orderStatusFail;
  
  /**
   * A array of permitted payment systems.
   * 
   * @var string
   */
  public $paymentSystemEnabled;
  
  /**
   * A array of forbidden payment systems.
   * 
   * @var string
   */
  public $paymentSystemDisabled;
  
  /**
   * The set of required fields.
   * 
   * @var array
   */
  public $requiredFields = array();
  
  /**
   * The array for check to the regular expressions.
   * 
   * @var array
   */
  public $fieldsPreg = array();
  
  /**
   * The array with all errors;
   * 
   * @var array 
   */
  public $errors = array();
  
  /**
   * A path to the file with payment systems. 
   * 
   * @var string
   */
  public $filename;

  /**
   * To check the required fields.
   * 
   * @return boolean
   */
  abstract public function required();

  /**
   * Checks for required fields and cheks validation these fields.
   * 
   * @param array $param
   * 
   * @return boolean
   */
  abstract public function validation();
  
  /**
   * Get value of field.
   * 
   * @param string $name
   * @return string
   */
  abstract public function getValue($name);
  
}
