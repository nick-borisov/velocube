<?php
namespace WalletOne;

class W1SettingsCms extends W1Settings {

  /**
   * The array with all message;
   * 
   * @var array 
   */
  public $messages = array();

  public $signatureMethodArray;
  
  public $signatureMethodArrayPresta;
  
  public $currencyPresta;

  /**
   * Array of names fields.
   * 
   * @var array
   */
  public $fieldsName = array();

  function __construct($params = array(), $lang = 'ru') {
    if (!defined('w1PathImg')) {
      define('w1PathImg', '');
    }
    
    $defaultLang = 'ru';
    $this->filename = str_replace('\\', '/', __DIR__) . '/../files/payments_out.php';
    if ($lang != 'ru') {
      $defaultLang = 'en';
      $this->filename = str_replace('\\', '/', __DIR__) . '/../files/payments_en_out.php';
    }
    include_once str_replace('\\', '/', __DIR__) . '/../lang/settings.' . $defaultLang . '.php';

    include str_replace('\\', '/', __DIR__) . '/../config.php';
    
    include_once('w1.helpers.php');

    $this->signatureMethod = $config['signatureMethodDefault'];
    $this->signatureMethodArray = $config['signatureMethod'];
    $this->signatureMethodArrayPresta = $config['signatureMethodPresta'];
    $this->fieldsPreg = $config['settings']['fieldsPreg'];
    $this->currencyDefault = 0;
    $this->requiredFields = $config['settings']['requiredFields'];
    $this->currencyCode = $config['currencyCode'];
    $this->currencyName = $config['currencyName'];
    $this->currencyPresta = $config['currencyPresta'];
    $this->fieldsName = $config['settings']['fieldsName'];

    if (!empty($params['MERCHANT_ID'])) {
      $this->merchantId = $params['MERCHANT_ID'];
    }
    if (!empty($params['SIGNATURE'])) {
      $this->signature = $params['SIGNATURE'];
    }
    if (!empty($params['SIGNATURE_METHOD'])) {
      $this->signatureMethod = $params['SIGNATURE_METHOD'];
    }
    if (!empty($params['CURRENCY_ID'])) {
      $this->currencyId = $params['CURRENCY_ID'];
    }
    if (!empty($params['currency_default'])) {
      $this->currencyDefault = $params['currency_default'];
    }
    if (!empty($params['order_status_sucess'])) {
      $this->orderStatusSuccess = $params['order_status_sucess'];
    }
    if (!empty($params['order_status_waiting'])) {
      $this->orderStatusWaiting = $params['order_status_waiting'];
    }
    if (!empty($params['order_status_fail'])) {
      $this->orderStatusFail = $params['order_status_fail'];
    }
    if (!empty($params['PTENABLED'])) {
      if (is_array($params['PTENABLED'])) {
        $this->paymentSystemEnabled = array_diff($params['PTENABLED'], array(null, false, 0));
      }
      else {
        if (strpos($params['PTENABLED'], ','))
          $this->paymentSystemEnabled = explode(',', preg_replace('/\s+/', '', $params['PTENABLED']));
        else {
          $this->paymentSystemEnabled[0] = preg_replace('/\s+/', '', $params['PTENABLED']);
        }
      }
      sort($this->paymentSystemEnabled);
    }
    if (!empty($params['PTDISABLED'])) {
      if (is_array($params['PTDISABLED'])) {
        $this->paymentSystemDisabled = array_diff($params['PTDISABLED'], array(null, false, 0));
      }
      else {
        if (strpos($params['PTDISABLED'], ','))
          $this->paymentSystemDisabled = explode(',', preg_replace('/\s+/', '', $params['PTDISABLED']));
        else {
          $this->paymentSystemDisabled[0] = preg_replace('/\s+/', '', $params['PTDISABLED']);
        }
      }
      sort($this->paymentSystemDisabled);
    }
  }
  
  /**
   * Get value of field.
   * 
   * @param string $name
   * @return string
   */
  public function getValue($name){
    $reflect = new \ReflectionClass(get_class($this));
    $property = $reflect->getProperty($name);
    return $property->getValue($this);
  }

  /**
   * To check the required fields.
   * 
   * @param array $fields
   * @return boolean
   */
  public function required() {
    $this->errors = array();
    $this->messages = array();
    if (!empty($this->requiredFields)) {
      foreach ($this->requiredFields as $value) {
        if (property_exists($this, $value)) {
          $field = $this->getValue($value);
          if(empty($field) && $field !== 0){
            $this->errors[] = sprintf(w1ErrorRequired, $this->fieldsName[$value]);
            Helpers::logging(sprintf(w1ErrorRequired, $this->fieldsName[$value]));
          }
        }
        else {
          $this->errors[] = sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]);
          Helpers::logging(sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]));
        }
      }
      if (!empty($this->errors)) {
        $this->messages[] = w1ErrorActive;
        return false;
      }
    }
    return true;
  }

  /**
   * Validation fields.
   * 
   * @return boolean
   */
  public function validation() {
    $this->errors = array();
    $this->messages = array();
    if (!empty($this->fieldsPreg)) {
      foreach ($this->fieldsPreg as $key => $value) {
        if (property_exists($this, $key)) {
          $val = $this->getValue($key);
          if(!empty($val)){
            if(is_array($val)){
              foreach ($val as $v) {
                if(preg_match($value, $v) == 0){
                  $this->errors[] = sprintf(w1ErrorPreg, $this->fieldsName[$key]).' '.$v;
                  Helpers::logging(sprintf(w1ErrorPreg, $this->fieldsName[$key]).' '.$v);
                }
              }
            }
            elseif(preg_match($value, $val) == 0){
              $this->errors[] = sprintf(w1ErrorPreg, $this->fieldsName[$key]);
              Helpers::logging(sprintf(w1ErrorPreg, $this->fieldsName[$key]));
            }
          }
        }
      }
      if (!empty($this->errors)) {
        return false;
      }
    }
    return true;
  }

}