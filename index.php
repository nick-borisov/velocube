<?PHP
/**
 * Simpla CMS
 * 
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 */
//ini_set('display_errors', 1);
// Засекаем время
$time_start = microtime(true);
@ini_set('session.gc_maxlifetime', 864000); // 10 дней
@ini_set('session.cookie_lifetime', 864000); // 10 дней
//@ini_set('display_errors', 1); // 10 дней
//error_reporting(E_ALL);

session_start();



$urlred = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

if($urlred == 'http://velocube.ru/catalog/commencal-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/commencal');
exit();
}else if($urlred == 'http://velocube.ru/catalog/cronus-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/cronus');
exit();
}else if($urlred == 'http://velocube.ru/catalog/cube-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/cube');
exit();
}else if($urlred == 'http://velocube.ru/catalog/ghost-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/ghost');
exit();
}else if($urlred == 'http://velocube.ru/catalog/orbea-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/orbea');
exit();
}else if($urlred == 'http://velocube.ru/catalog/schwinn-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/schwinn');
exit();
}else if($urlred == 'http://velocube.ru/catalog/silverback-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/silverback');
exit();
}else if($urlred == 'http://velocube.ru/catalog/stark-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/stark');
exit();
}else if($urlred == 'http://velocube.ru/catalog/wheeler-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/wheeler');
exit();
}else if($urlred == 'http://velocube.ru/catalog/bulls-2014'){
header('HTTP/1.1 301 Moved Permanently');
header('Location: http://velocube.ru/catalog/velosipedy-2014-goda/bulls');
exit();
}

 

require_once('view/IndexView.php');

$view = new IndexView();


if(isset($_GET['logout']))
{
    header('WWW-Authenticate: Basic realm="Simpla CMS"');
    header('HTTP/1.0 401 Unauthorized');
	unset($_SESSION['admin']);
}

// Если все хорошо
if(($res = $view->fetch()) !== false)
{
	// Выводим результат
	header("Content-type: text/html; charset=UTF-8");	
	print $res;

	// Сохраняем последнюю просмотренную страницу в переменной $_SESSION['last_visited_page']
	if(empty($_SESSION['last_visited_page']) || empty($_SESSION['current_page']) || $_SERVER['REQUEST_URI'] !== $_SESSION['current_page'])
	{
		if(!empty($_SESSION['current_page']) && !empty($_SESSION['last_visited_page']) && $_SESSION['last_visited_page'] !== $_SESSION['current_page'])
			$_SESSION['last_visited_page'] = $_SESSION['current_page'];
		$_SESSION['current_page'] = $_SERVER['REQUEST_URI'];
	}		
}
else 
{ 
	// Иначе страница об ошибке
	header("http/1.0 404 not found");
	
	// Подменим переменную GET, чтобы вывести страницу 404
	$_GET['page_url'] = '404';
	$_GET['module'] = 'PageView';
	print $view->fetch();   
}